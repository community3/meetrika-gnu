/*
               File: PromptContratoTermoAditivo
        Description: Selecione Contrato Termo Aditivo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:37:20.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratotermoaditivo : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratotermoaditivo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratotermoaditivo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoTermoAditivo_Codigo ,
                           ref DateTime aP1_InOutContratoTermoAditivo_DataInicio )
      {
         this.AV7InOutContratoTermoAditivo_Codigo = aP0_InOutContratoTermoAditivo_Codigo;
         this.AV8InOutContratoTermoAditivo_DataInicio = aP1_InOutContratoTermoAditivo_DataInicio;
         executePrivate();
         aP0_InOutContratoTermoAditivo_Codigo=this.AV7InOutContratoTermoAditivo_Codigo;
         aP1_InOutContratoTermoAditivo_DataInicio=this.AV8InOutContratoTermoAditivo_DataInicio;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_134 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_134_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_134_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16ContratoTermoAditivo_DataInicio1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoTermoAditivo_DataInicio1", context.localUtil.Format(AV16ContratoTermoAditivo_DataInicio1, "99/99/99"));
               AV17ContratoTermoAditivo_DataInicio_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoTermoAditivo_DataInicio_To1", context.localUtil.Format(AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"));
               AV30ContratoTermoAditivo_DataFim1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoTermoAditivo_DataFim1", context.localUtil.Format(AV30ContratoTermoAditivo_DataFim1, "99/99/99"));
               AV31ContratoTermoAditivo_DataFim_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoTermoAditivo_DataFim_To1", context.localUtil.Format(AV31ContratoTermoAditivo_DataFim_To1, "99/99/99"));
               AV32ContratoTermoAditivo_DataAssinatura1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContratoTermoAditivo_DataAssinatura1", context.localUtil.Format(AV32ContratoTermoAditivo_DataAssinatura1, "99/99/99"));
               AV33ContratoTermoAditivo_DataAssinatura_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoTermoAditivo_DataAssinatura_To1", context.localUtil.Format(AV33ContratoTermoAditivo_DataAssinatura_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20ContratoTermoAditivo_DataInicio2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoTermoAditivo_DataInicio2", context.localUtil.Format(AV20ContratoTermoAditivo_DataInicio2, "99/99/99"));
               AV21ContratoTermoAditivo_DataInicio_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoTermoAditivo_DataInicio_To2", context.localUtil.Format(AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"));
               AV34ContratoTermoAditivo_DataFim2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContratoTermoAditivo_DataFim2", context.localUtil.Format(AV34ContratoTermoAditivo_DataFim2, "99/99/99"));
               AV35ContratoTermoAditivo_DataFim_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContratoTermoAditivo_DataFim_To2", context.localUtil.Format(AV35ContratoTermoAditivo_DataFim_To2, "99/99/99"));
               AV36ContratoTermoAditivo_DataAssinatura2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContratoTermoAditivo_DataAssinatura2", context.localUtil.Format(AV36ContratoTermoAditivo_DataAssinatura2, "99/99/99"));
               AV37ContratoTermoAditivo_DataAssinatura_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoTermoAditivo_DataAssinatura_To2", context.localUtil.Format(AV37ContratoTermoAditivo_DataAssinatura_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24ContratoTermoAditivo_DataInicio3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoTermoAditivo_DataInicio3", context.localUtil.Format(AV24ContratoTermoAditivo_DataInicio3, "99/99/99"));
               AV25ContratoTermoAditivo_DataInicio_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoTermoAditivo_DataInicio_To3", context.localUtil.Format(AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"));
               AV38ContratoTermoAditivo_DataFim3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoTermoAditivo_DataFim3", context.localUtil.Format(AV38ContratoTermoAditivo_DataFim3, "99/99/99"));
               AV39ContratoTermoAditivo_DataFim_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoTermoAditivo_DataFim_To3", context.localUtil.Format(AV39ContratoTermoAditivo_DataFim_To3, "99/99/99"));
               AV40ContratoTermoAditivo_DataAssinatura3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoTermoAditivo_DataAssinatura3", context.localUtil.Format(AV40ContratoTermoAditivo_DataAssinatura3, "99/99/99"));
               AV41ContratoTermoAditivo_DataAssinatura_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoTermoAditivo_DataAssinatura_To3", context.localUtil.Format(AV41ContratoTermoAditivo_DataAssinatura_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV43TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContrato_Numero", AV43TFContrato_Numero);
               AV44TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContrato_Numero_Sel", AV44TFContrato_Numero_Sel);
               AV47TFContratoTermoAditivo_DataInicio = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoTermoAditivo_DataInicio", context.localUtil.Format(AV47TFContratoTermoAditivo_DataInicio, "99/99/99"));
               AV48TFContratoTermoAditivo_DataInicio_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoTermoAditivo_DataInicio_To", context.localUtil.Format(AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"));
               AV53TFContratoTermoAditivo_DataFim = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoTermoAditivo_DataFim", context.localUtil.Format(AV53TFContratoTermoAditivo_DataFim, "99/99/99"));
               AV54TFContratoTermoAditivo_DataFim_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoTermoAditivo_DataFim_To", context.localUtil.Format(AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"));
               AV59TFContratoTermoAditivo_DataAssinatura = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoTermoAditivo_DataAssinatura", context.localUtil.Format(AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"));
               AV60TFContratoTermoAditivo_DataAssinatura_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoTermoAditivo_DataAssinatura_To", context.localUtil.Format(AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"));
               AV45ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contrato_NumeroTitleControlIdToReplace", AV45ddo_Contrato_NumeroTitleControlIdToReplace);
               AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace", AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace);
               AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace", AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace);
               AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace", AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace);
               AV71Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptContratoTermoAditivo";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptcontratotermoaditivo:[SendSecurityCheck value for]"+"ContratoTermoAditivo_Codigo:"+context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoTermoAditivo_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoTermoAditivo_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoTermoAditivo_DataInicio = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoTermoAditivo_DataInicio", context.localUtil.Format(AV8InOutContratoTermoAditivo_DataInicio, "99/99/99"));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA7L2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV71Pgmname = "PromptContratoTermoAditivo";
               context.Gx_err = 0;
               WS7L2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE7L2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311737210");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratotermoaditivo.aspx") + "?" + UrlEncode("" +AV7InOutContratoTermoAditivo_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV8InOutContratoTermoAditivo_DataInicio))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAINICIO1", context.localUtil.Format(AV16ContratoTermoAditivo_DataInicio1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO1", context.localUtil.Format(AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAFIM1", context.localUtil.Format(AV30ContratoTermoAditivo_DataFim1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO1", context.localUtil.Format(AV31ContratoTermoAditivo_DataFim_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA1", context.localUtil.Format(AV32ContratoTermoAditivo_DataAssinatura1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1", context.localUtil.Format(AV33ContratoTermoAditivo_DataAssinatura_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAINICIO2", context.localUtil.Format(AV20ContratoTermoAditivo_DataInicio2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO2", context.localUtil.Format(AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAFIM2", context.localUtil.Format(AV34ContratoTermoAditivo_DataFim2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO2", context.localUtil.Format(AV35ContratoTermoAditivo_DataFim_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA2", context.localUtil.Format(AV36ContratoTermoAditivo_DataAssinatura2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2", context.localUtil.Format(AV37ContratoTermoAditivo_DataAssinatura_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAINICIO3", context.localUtil.Format(AV24ContratoTermoAditivo_DataInicio3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO3", context.localUtil.Format(AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAFIM3", context.localUtil.Format(AV38ContratoTermoAditivo_DataFim3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO3", context.localUtil.Format(AV39ContratoTermoAditivo_DataFim_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA3", context.localUtil.Format(AV40ContratoTermoAditivo_DataAssinatura3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3", context.localUtil.Format(AV41ContratoTermoAditivo_DataAssinatura_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV43TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV44TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOTERMOADITIVO_DATAINICIO", context.localUtil.Format(AV47TFContratoTermoAditivo_DataInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOTERMOADITIVO_DATAINICIO_TO", context.localUtil.Format(AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOTERMOADITIVO_DATAFIM", context.localUtil.Format(AV53TFContratoTermoAditivo_DataFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOTERMOADITIVO_DATAFIM_TO", context.localUtil.Format(AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOTERMOADITIVO_DATAASSINATURA", context.localUtil.Format(AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO", context.localUtil.Format(AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_134", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_134), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV64DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV64DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV42Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV42Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOTERMOADITIVO_DATAINICIOTITLEFILTERDATA", AV46ContratoTermoAditivo_DataInicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOTERMOADITIVO_DATAINICIOTITLEFILTERDATA", AV46ContratoTermoAditivo_DataInicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOTERMOADITIVO_DATAFIMTITLEFILTERDATA", AV52ContratoTermoAditivo_DataFimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOTERMOADITIVO_DATAFIMTITLEFILTERDATA", AV52ContratoTermoAditivo_DataFimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOTERMOADITIVO_DATAASSINATURATITLEFILTERDATA", AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOTERMOADITIVO_DATAASSINATURATITLEFILTERDATA", AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV71Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOTERMOADITIVO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoTermoAditivo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOTERMOADITIVO_DATAINICIO", context.localUtil.DToC( AV8InOutContratoTermoAditivo_DataInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Caption", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Tooltip", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Cls", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtext_set", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includesortasc", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datainicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datainicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Sortedstatus", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includefilter", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datainicio_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filtertype", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filterisrange", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datainicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includedatalist", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datainicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Sortasc", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Sortdsc", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Cleanfilter", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Rangefilterto", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Searchbuttontext", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Caption", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Tooltip", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Cls", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtext_set", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtextto_set", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includesortasc", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datafim_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datafim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Sortedstatus", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includefilter", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datafim_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filtertype", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filterisrange", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datafim_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includedatalist", StringUtil.BoolToStr( Ddo_contratotermoaditivo_datafim_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Sortasc", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Sortdsc", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Cleanfilter", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Rangefilterto", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Searchbuttontext", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Caption", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Tooltip", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Cls", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtext_set", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includesortasc", StringUtil.BoolToStr( Ddo_contratotermoaditivo_dataassinatura_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratotermoaditivo_dataassinatura_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Sortedstatus", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includefilter", StringUtil.BoolToStr( Ddo_contratotermoaditivo_dataassinatura_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filtertype", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filterisrange", StringUtil.BoolToStr( Ddo_contratotermoaditivo_dataassinatura_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includedatalist", StringUtil.BoolToStr( Ddo_contratotermoaditivo_dataassinatura_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Sortasc", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Sortdsc", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Cleanfilter", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Rangefilterto", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Searchbuttontext", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Activeeventkey", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtext_get", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratotermoaditivo_datainicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Activeeventkey", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtext_get", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtextto_get", StringUtil.RTrim( Ddo_contratotermoaditivo_datafim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Activeeventkey", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtext_get", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptContratoTermoAditivo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptcontratotermoaditivo:[SendSecurityCheck value for]"+"ContratoTermoAditivo_Codigo:"+context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm7L2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoTermoAditivo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Termo Aditivo" ;
      }

      protected void WB7L0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_7L2( true) ;
         }
         else
         {
            wb_table1_2_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoTermoAditivo_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_134_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(145, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_134_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(146, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_134_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV43TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV43TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoTermoAditivo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_134_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV44TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV44TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoTermoAditivo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratotermoaditivo_datainicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratotermoaditivo_datainicio_Internalname, context.localUtil.Format(AV47TFContratoTermoAditivo_DataInicio, "99/99/99"), context.localUtil.Format( AV47TFContratoTermoAditivo_DataInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratotermoaditivo_datainicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratotermoaditivo_datainicio_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratotermoaditivo_datainicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratotermoaditivo_datainicio_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratotermoaditivo_datainicio_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratotermoaditivo_datainicio_to_Internalname, context.localUtil.Format(AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"), context.localUtil.Format( AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,150);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratotermoaditivo_datainicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratotermoaditivo_datainicio_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratotermoaditivo_datainicio_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratotermoaditivo_datainicio_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratotermoaditivo_datainicioauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname, context.localUtil.Format(AV49DDO_ContratoTermoAditivo_DataInicioAuxDate, "99/99/99"), context.localUtil.Format( AV49DDO_ContratoTermoAditivo_DataInicioAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,152);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratotermoaditivo_datainicioauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname, context.localUtil.Format(AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo, "99/99/99"), context.localUtil.Format( AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,153);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratotermoaditivo_datainicioauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratotermoaditivo_datafim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratotermoaditivo_datafim_Internalname, context.localUtil.Format(AV53TFContratoTermoAditivo_DataFim, "99/99/99"), context.localUtil.Format( AV53TFContratoTermoAditivo_DataFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratotermoaditivo_datafim_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratotermoaditivo_datafim_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratotermoaditivo_datafim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratotermoaditivo_datafim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratotermoaditivo_datafim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratotermoaditivo_datafim_to_Internalname, context.localUtil.Format(AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"), context.localUtil.Format( AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,155);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratotermoaditivo_datafim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratotermoaditivo_datafim_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratotermoaditivo_datafim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratotermoaditivo_datafim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratotermoaditivo_datafimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratotermoaditivo_datafimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratotermoaditivo_datafimauxdate_Internalname, context.localUtil.Format(AV55DDO_ContratoTermoAditivo_DataFimAuxDate, "99/99/99"), context.localUtil.Format( AV55DDO_ContratoTermoAditivo_DataFimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,157);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratotermoaditivo_datafimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratotermoaditivo_datafimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname, context.localUtil.Format(AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo, "99/99/99"), context.localUtil.Format( AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,158);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratotermoaditivo_datafimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratotermoaditivo_dataassinatura_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratotermoaditivo_dataassinatura_Internalname, context.localUtil.Format(AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"), context.localUtil.Format( AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,159);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratotermoaditivo_dataassinatura_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratotermoaditivo_dataassinatura_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratotermoaditivo_dataassinatura_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratotermoaditivo_dataassinatura_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 160,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratotermoaditivo_dataassinatura_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratotermoaditivo_dataassinatura_to_Internalname, context.localUtil.Format(AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"), context.localUtil.Format( AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,160);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratotermoaditivo_dataassinatura_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratotermoaditivo_dataassinatura_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratotermoaditivo_dataassinatura_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratotermoaditivo_dataassinatura_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratotermoaditivo_dataassinaturaauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname, context.localUtil.Format(AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate, "99/99/99"), context.localUtil.Format( AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,162);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname, context.localUtil.Format(AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo, "99/99/99"), context.localUtil.Format( AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,163);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'" + sGXsfl_134_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV45ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoTermoAditivo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOTERMOADITIVO_DATAINICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'" + sGXsfl_134_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Internalname, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,167);\"", 0, edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoTermoAditivo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOTERMOADITIVO_DATAFIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'" + sGXsfl_134_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Internalname, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", 0, edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoTermoAditivo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOTERMOADITIVO_DATAASSINATURAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'" + sGXsfl_134_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Internalname, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,171);\"", 0, edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoTermoAditivo.htm");
         }
         wbLoad = true;
      }

      protected void START7L2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Termo Aditivo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP7L0( ) ;
      }

      protected void WS7L2( )
      {
         START7L2( ) ;
         EVT7L2( ) ;
      }

      protected void EVT7L2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E117L2 */
                           E117L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E127L2 */
                           E127L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOTERMOADITIVO_DATAINICIO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E137L2 */
                           E137L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOTERMOADITIVO_DATAFIM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E147L2 */
                           E147L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E157L2 */
                           E157L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E167L2 */
                           E167L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E177L2 */
                           E177L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E187L2 */
                           E187L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E197L2 */
                           E197L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E207L2 */
                           E207L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E217L2 */
                           E217L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E227L2 */
                           E227L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E237L2 */
                           E237L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E247L2 */
                           E247L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E257L2 */
                           E257L2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_134_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_134_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_134_idx), 4, 0)), 4, "0");
                           SubsflControlProps_1342( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV70Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                           A316ContratoTermoAditivo_DataInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoTermoAditivo_DataInicio_Internalname), 0));
                           n316ContratoTermoAditivo_DataInicio = false;
                           A317ContratoTermoAditivo_DataFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoTermoAditivo_DataFim_Internalname), 0));
                           n317ContratoTermoAditivo_DataFim = false;
                           A318ContratoTermoAditivo_DataAssinatura = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoTermoAditivo_DataAssinatura_Internalname), 0));
                           n318ContratoTermoAditivo_DataAssinatura = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E267L2 */
                                 E267L2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E277L2 */
                                 E277L2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E287L2 */
                                 E287L2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datainicio1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO1"), 0) != AV16ContratoTermoAditivo_DataInicio1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datainicio_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO1"), 0) != AV17ContratoTermoAditivo_DataInicio_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datafim1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM1"), 0) != AV30ContratoTermoAditivo_DataFim1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datafim_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO1"), 0) != AV31ContratoTermoAditivo_DataFim_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_dataassinatura1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA1"), 0) != AV32ContratoTermoAditivo_DataAssinatura1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_dataassinatura_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1"), 0) != AV33ContratoTermoAditivo_DataAssinatura_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datainicio2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO2"), 0) != AV20ContratoTermoAditivo_DataInicio2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datainicio_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO2"), 0) != AV21ContratoTermoAditivo_DataInicio_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datafim2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM2"), 0) != AV34ContratoTermoAditivo_DataFim2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datafim_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO2"), 0) != AV35ContratoTermoAditivo_DataFim_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_dataassinatura2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA2"), 0) != AV36ContratoTermoAditivo_DataAssinatura2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_dataassinatura_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2"), 0) != AV37ContratoTermoAditivo_DataAssinatura_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datainicio3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO3"), 0) != AV24ContratoTermoAditivo_DataInicio3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datainicio_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO3"), 0) != AV25ContratoTermoAditivo_DataInicio_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datafim3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM3"), 0) != AV38ContratoTermoAditivo_DataFim3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_datafim_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO3"), 0) != AV39ContratoTermoAditivo_DataFim_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_dataassinatura3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA3"), 0) != AV40ContratoTermoAditivo_DataAssinatura3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratotermoaditivo_dataassinatura_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3"), 0) != AV41ContratoTermoAditivo_DataAssinatura_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV43TFContrato_Numero) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV44TFContrato_Numero_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratotermoaditivo_datainicio Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAINICIO"), 0) != AV47TFContratoTermoAditivo_DataInicio )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratotermoaditivo_datainicio_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAINICIO_TO"), 0) != AV48TFContratoTermoAditivo_DataInicio_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratotermoaditivo_datafim Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAFIM"), 0) != AV53TFContratoTermoAditivo_DataFim )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratotermoaditivo_datafim_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAFIM_TO"), 0) != AV54TFContratoTermoAditivo_DataFim_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratotermoaditivo_dataassinatura Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAASSINATURA"), 0) != AV59TFContratoTermoAditivo_DataAssinatura )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratotermoaditivo_dataassinatura_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO"), 0) != AV60TFContratoTermoAditivo_DataAssinatura_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E297L2 */
                                       E297L2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE7L2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm7L2( ) ;
            }
         }
      }

      protected void PA7L2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOTERMOADITIVO_DATAINICIO", "Data Inicio", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOTERMOADITIVO_DATAFIM", "Data Fim", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOTERMOADITIVO_DATAASSINATURA", "Data Assinatura", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOTERMOADITIVO_DATAINICIO", "Data Inicio", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOTERMOADITIVO_DATAFIM", "Data Fim", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOTERMOADITIVO_DATAASSINATURA", "Data Assinatura", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOTERMOADITIVO_DATAINICIO", "Data Inicio", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATOTERMOADITIVO_DATAFIM", "Data Fim", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATOTERMOADITIVO_DATAASSINATURA", "Data Assinatura", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1342( ) ;
         while ( nGXsfl_134_idx <= nRC_GXsfl_134 )
         {
            sendrow_1342( ) ;
            nGXsfl_134_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_134_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_134_idx+1));
            sGXsfl_134_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_134_idx), 4, 0)), 4, "0");
            SubsflControlProps_1342( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16ContratoTermoAditivo_DataInicio1 ,
                                       DateTime AV17ContratoTermoAditivo_DataInicio_To1 ,
                                       DateTime AV30ContratoTermoAditivo_DataFim1 ,
                                       DateTime AV31ContratoTermoAditivo_DataFim_To1 ,
                                       DateTime AV32ContratoTermoAditivo_DataAssinatura1 ,
                                       DateTime AV33ContratoTermoAditivo_DataAssinatura_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20ContratoTermoAditivo_DataInicio2 ,
                                       DateTime AV21ContratoTermoAditivo_DataInicio_To2 ,
                                       DateTime AV34ContratoTermoAditivo_DataFim2 ,
                                       DateTime AV35ContratoTermoAditivo_DataFim_To2 ,
                                       DateTime AV36ContratoTermoAditivo_DataAssinatura2 ,
                                       DateTime AV37ContratoTermoAditivo_DataAssinatura_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24ContratoTermoAditivo_DataInicio3 ,
                                       DateTime AV25ContratoTermoAditivo_DataInicio_To3 ,
                                       DateTime AV38ContratoTermoAditivo_DataFim3 ,
                                       DateTime AV39ContratoTermoAditivo_DataFim_To3 ,
                                       DateTime AV40ContratoTermoAditivo_DataAssinatura3 ,
                                       DateTime AV41ContratoTermoAditivo_DataAssinatura_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV43TFContrato_Numero ,
                                       String AV44TFContrato_Numero_Sel ,
                                       DateTime AV47TFContratoTermoAditivo_DataInicio ,
                                       DateTime AV48TFContratoTermoAditivo_DataInicio_To ,
                                       DateTime AV53TFContratoTermoAditivo_DataFim ,
                                       DateTime AV54TFContratoTermoAditivo_DataFim_To ,
                                       DateTime AV59TFContratoTermoAditivo_DataAssinatura ,
                                       DateTime AV60TFContratoTermoAditivo_DataAssinatura_To ,
                                       String AV45ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace ,
                                       String AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace ,
                                       String AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace ,
                                       String AV71Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF7L2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOTERMOADITIVO_DATAINICIO", GetSecureSignedToken( "", A316ContratoTermoAditivo_DataInicio));
         GxWebStd.gx_hidden_field( context, "CONTRATOTERMOADITIVO_DATAINICIO", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOTERMOADITIVO_DATAFIM", GetSecureSignedToken( "", A317ContratoTermoAditivo_DataFim));
         GxWebStd.gx_hidden_field( context, "CONTRATOTERMOADITIVO_DATAFIM", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOTERMOADITIVO_DATAASSINATURA", GetSecureSignedToken( "", A318ContratoTermoAditivo_DataAssinatura));
         GxWebStd.gx_hidden_field( context, "CONTRATOTERMOADITIVO_DATAASSINATURA", context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7L2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV71Pgmname = "PromptContratoTermoAditivo";
         context.Gx_err = 0;
      }

      protected void RF7L2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 134;
         /* Execute user event: E277L2 */
         E277L2 ();
         nGXsfl_134_idx = 1;
         sGXsfl_134_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_134_idx), 4, 0)), 4, "0");
         SubsflControlProps_1342( ) ;
         nGXsfl_134_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1342( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16ContratoTermoAditivo_DataInicio1 ,
                                                 AV17ContratoTermoAditivo_DataInicio_To1 ,
                                                 AV30ContratoTermoAditivo_DataFim1 ,
                                                 AV31ContratoTermoAditivo_DataFim_To1 ,
                                                 AV32ContratoTermoAditivo_DataAssinatura1 ,
                                                 AV33ContratoTermoAditivo_DataAssinatura_To1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20ContratoTermoAditivo_DataInicio2 ,
                                                 AV21ContratoTermoAditivo_DataInicio_To2 ,
                                                 AV34ContratoTermoAditivo_DataFim2 ,
                                                 AV35ContratoTermoAditivo_DataFim_To2 ,
                                                 AV36ContratoTermoAditivo_DataAssinatura2 ,
                                                 AV37ContratoTermoAditivo_DataAssinatura_To2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24ContratoTermoAditivo_DataInicio3 ,
                                                 AV25ContratoTermoAditivo_DataInicio_To3 ,
                                                 AV38ContratoTermoAditivo_DataFim3 ,
                                                 AV39ContratoTermoAditivo_DataFim_To3 ,
                                                 AV40ContratoTermoAditivo_DataAssinatura3 ,
                                                 AV41ContratoTermoAditivo_DataAssinatura_To3 ,
                                                 AV44TFContrato_Numero_Sel ,
                                                 AV43TFContrato_Numero ,
                                                 AV47TFContratoTermoAditivo_DataInicio ,
                                                 AV48TFContratoTermoAditivo_DataInicio_To ,
                                                 AV53TFContratoTermoAditivo_DataFim ,
                                                 AV54TFContratoTermoAditivo_DataFim_To ,
                                                 AV59TFContratoTermoAditivo_DataAssinatura ,
                                                 AV60TFContratoTermoAditivo_DataAssinatura_To ,
                                                 A316ContratoTermoAditivo_DataInicio ,
                                                 A317ContratoTermoAditivo_DataFim ,
                                                 A318ContratoTermoAditivo_DataAssinatura ,
                                                 A77Contrato_Numero ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV43TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV43TFContrato_Numero), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContrato_Numero", AV43TFContrato_Numero);
            /* Using cursor H007L2 */
            pr_default.execute(0, new Object[] {AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, lV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_134_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A74Contrato_Codigo = H007L2_A74Contrato_Codigo[0];
               A315ContratoTermoAditivo_Codigo = H007L2_A315ContratoTermoAditivo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
               A318ContratoTermoAditivo_DataAssinatura = H007L2_A318ContratoTermoAditivo_DataAssinatura[0];
               n318ContratoTermoAditivo_DataAssinatura = H007L2_n318ContratoTermoAditivo_DataAssinatura[0];
               A317ContratoTermoAditivo_DataFim = H007L2_A317ContratoTermoAditivo_DataFim[0];
               n317ContratoTermoAditivo_DataFim = H007L2_n317ContratoTermoAditivo_DataFim[0];
               A316ContratoTermoAditivo_DataInicio = H007L2_A316ContratoTermoAditivo_DataInicio[0];
               n316ContratoTermoAditivo_DataInicio = H007L2_n316ContratoTermoAditivo_DataInicio[0];
               A77Contrato_Numero = H007L2_A77Contrato_Numero[0];
               A77Contrato_Numero = H007L2_A77Contrato_Numero[0];
               /* Execute user event: E287L2 */
               E287L2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 134;
            WB7L0( ) ;
         }
         nGXsfl_134_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16ContratoTermoAditivo_DataInicio1 ,
                                              AV17ContratoTermoAditivo_DataInicio_To1 ,
                                              AV30ContratoTermoAditivo_DataFim1 ,
                                              AV31ContratoTermoAditivo_DataFim_To1 ,
                                              AV32ContratoTermoAditivo_DataAssinatura1 ,
                                              AV33ContratoTermoAditivo_DataAssinatura_To1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20ContratoTermoAditivo_DataInicio2 ,
                                              AV21ContratoTermoAditivo_DataInicio_To2 ,
                                              AV34ContratoTermoAditivo_DataFim2 ,
                                              AV35ContratoTermoAditivo_DataFim_To2 ,
                                              AV36ContratoTermoAditivo_DataAssinatura2 ,
                                              AV37ContratoTermoAditivo_DataAssinatura_To2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24ContratoTermoAditivo_DataInicio3 ,
                                              AV25ContratoTermoAditivo_DataInicio_To3 ,
                                              AV38ContratoTermoAditivo_DataFim3 ,
                                              AV39ContratoTermoAditivo_DataFim_To3 ,
                                              AV40ContratoTermoAditivo_DataAssinatura3 ,
                                              AV41ContratoTermoAditivo_DataAssinatura_To3 ,
                                              AV44TFContrato_Numero_Sel ,
                                              AV43TFContrato_Numero ,
                                              AV47TFContratoTermoAditivo_DataInicio ,
                                              AV48TFContratoTermoAditivo_DataInicio_To ,
                                              AV53TFContratoTermoAditivo_DataFim ,
                                              AV54TFContratoTermoAditivo_DataFim_To ,
                                              AV59TFContratoTermoAditivo_DataAssinatura ,
                                              AV60TFContratoTermoAditivo_DataAssinatura_To ,
                                              A316ContratoTermoAditivo_DataInicio ,
                                              A317ContratoTermoAditivo_DataFim ,
                                              A318ContratoTermoAditivo_DataAssinatura ,
                                              A77Contrato_Numero ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV43TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV43TFContrato_Numero), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContrato_Numero", AV43TFContrato_Numero);
         /* Using cursor H007L3 */
         pr_default.execute(1, new Object[] {AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, lV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To});
         GRID_nRecordCount = H007L3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP7L0( )
      {
         /* Before Start, stand alone formulas. */
         AV71Pgmname = "PromptContratoTermoAditivo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E267L2 */
         E267L2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV64DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV42Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOTERMOADITIVO_DATAINICIOTITLEFILTERDATA"), AV46ContratoTermoAditivo_DataInicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOTERMOADITIVO_DATAFIMTITLEFILTERDATA"), AV52ContratoTermoAditivo_DataFimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOTERMOADITIVO_DATAASSINATURATITLEFILTERDATA"), AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datainicio1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Inicio1"}), 1, "vCONTRATOTERMOADITIVO_DATAINICIO1");
               GX_FocusControl = edtavContratotermoaditivo_datainicio1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContratoTermoAditivo_DataInicio1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoTermoAditivo_DataInicio1", context.localUtil.Format(AV16ContratoTermoAditivo_DataInicio1, "99/99/99"));
            }
            else
            {
               AV16ContratoTermoAditivo_DataInicio1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datainicio1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoTermoAditivo_DataInicio1", context.localUtil.Format(AV16ContratoTermoAditivo_DataInicio1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datainicio_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Inicio_To1"}), 1, "vCONTRATOTERMOADITIVO_DATAINICIO_TO1");
               GX_FocusControl = edtavContratotermoaditivo_datainicio_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoTermoAditivo_DataInicio_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoTermoAditivo_DataInicio_To1", context.localUtil.Format(AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"));
            }
            else
            {
               AV17ContratoTermoAditivo_DataInicio_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datainicio_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoTermoAditivo_DataInicio_To1", context.localUtil.Format(AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datafim1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Fim1"}), 1, "vCONTRATOTERMOADITIVO_DATAFIM1");
               GX_FocusControl = edtavContratotermoaditivo_datafim1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30ContratoTermoAditivo_DataFim1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoTermoAditivo_DataFim1", context.localUtil.Format(AV30ContratoTermoAditivo_DataFim1, "99/99/99"));
            }
            else
            {
               AV30ContratoTermoAditivo_DataFim1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datafim1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoTermoAditivo_DataFim1", context.localUtil.Format(AV30ContratoTermoAditivo_DataFim1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datafim_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Fim_To1"}), 1, "vCONTRATOTERMOADITIVO_DATAFIM_TO1");
               GX_FocusControl = edtavContratotermoaditivo_datafim_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31ContratoTermoAditivo_DataFim_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoTermoAditivo_DataFim_To1", context.localUtil.Format(AV31ContratoTermoAditivo_DataFim_To1, "99/99/99"));
            }
            else
            {
               AV31ContratoTermoAditivo_DataFim_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datafim_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoTermoAditivo_DataFim_To1", context.localUtil.Format(AV31ContratoTermoAditivo_DataFim_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_dataassinatura1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Assinatura1"}), 1, "vCONTRATOTERMOADITIVO_DATAASSINATURA1");
               GX_FocusControl = edtavContratotermoaditivo_dataassinatura1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32ContratoTermoAditivo_DataAssinatura1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContratoTermoAditivo_DataAssinatura1", context.localUtil.Format(AV32ContratoTermoAditivo_DataAssinatura1, "99/99/99"));
            }
            else
            {
               AV32ContratoTermoAditivo_DataAssinatura1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_dataassinatura1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContratoTermoAditivo_DataAssinatura1", context.localUtil.Format(AV32ContratoTermoAditivo_DataAssinatura1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_dataassinatura_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Assinatura_To1"}), 1, "vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1");
               GX_FocusControl = edtavContratotermoaditivo_dataassinatura_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33ContratoTermoAditivo_DataAssinatura_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoTermoAditivo_DataAssinatura_To1", context.localUtil.Format(AV33ContratoTermoAditivo_DataAssinatura_To1, "99/99/99"));
            }
            else
            {
               AV33ContratoTermoAditivo_DataAssinatura_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_dataassinatura_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoTermoAditivo_DataAssinatura_To1", context.localUtil.Format(AV33ContratoTermoAditivo_DataAssinatura_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datainicio2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Inicio2"}), 1, "vCONTRATOTERMOADITIVO_DATAINICIO2");
               GX_FocusControl = edtavContratotermoaditivo_datainicio2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContratoTermoAditivo_DataInicio2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoTermoAditivo_DataInicio2", context.localUtil.Format(AV20ContratoTermoAditivo_DataInicio2, "99/99/99"));
            }
            else
            {
               AV20ContratoTermoAditivo_DataInicio2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datainicio2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoTermoAditivo_DataInicio2", context.localUtil.Format(AV20ContratoTermoAditivo_DataInicio2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datainicio_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Inicio_To2"}), 1, "vCONTRATOTERMOADITIVO_DATAINICIO_TO2");
               GX_FocusControl = edtavContratotermoaditivo_datainicio_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoTermoAditivo_DataInicio_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoTermoAditivo_DataInicio_To2", context.localUtil.Format(AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"));
            }
            else
            {
               AV21ContratoTermoAditivo_DataInicio_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datainicio_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoTermoAditivo_DataInicio_To2", context.localUtil.Format(AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datafim2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Fim2"}), 1, "vCONTRATOTERMOADITIVO_DATAFIM2");
               GX_FocusControl = edtavContratotermoaditivo_datafim2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34ContratoTermoAditivo_DataFim2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContratoTermoAditivo_DataFim2", context.localUtil.Format(AV34ContratoTermoAditivo_DataFim2, "99/99/99"));
            }
            else
            {
               AV34ContratoTermoAditivo_DataFim2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datafim2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContratoTermoAditivo_DataFim2", context.localUtil.Format(AV34ContratoTermoAditivo_DataFim2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datafim_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Fim_To2"}), 1, "vCONTRATOTERMOADITIVO_DATAFIM_TO2");
               GX_FocusControl = edtavContratotermoaditivo_datafim_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35ContratoTermoAditivo_DataFim_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContratoTermoAditivo_DataFim_To2", context.localUtil.Format(AV35ContratoTermoAditivo_DataFim_To2, "99/99/99"));
            }
            else
            {
               AV35ContratoTermoAditivo_DataFim_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datafim_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContratoTermoAditivo_DataFim_To2", context.localUtil.Format(AV35ContratoTermoAditivo_DataFim_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_dataassinatura2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Assinatura2"}), 1, "vCONTRATOTERMOADITIVO_DATAASSINATURA2");
               GX_FocusControl = edtavContratotermoaditivo_dataassinatura2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36ContratoTermoAditivo_DataAssinatura2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContratoTermoAditivo_DataAssinatura2", context.localUtil.Format(AV36ContratoTermoAditivo_DataAssinatura2, "99/99/99"));
            }
            else
            {
               AV36ContratoTermoAditivo_DataAssinatura2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_dataassinatura2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContratoTermoAditivo_DataAssinatura2", context.localUtil.Format(AV36ContratoTermoAditivo_DataAssinatura2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_dataassinatura_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Assinatura_To2"}), 1, "vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2");
               GX_FocusControl = edtavContratotermoaditivo_dataassinatura_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37ContratoTermoAditivo_DataAssinatura_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoTermoAditivo_DataAssinatura_To2", context.localUtil.Format(AV37ContratoTermoAditivo_DataAssinatura_To2, "99/99/99"));
            }
            else
            {
               AV37ContratoTermoAditivo_DataAssinatura_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_dataassinatura_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoTermoAditivo_DataAssinatura_To2", context.localUtil.Format(AV37ContratoTermoAditivo_DataAssinatura_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datainicio3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Inicio3"}), 1, "vCONTRATOTERMOADITIVO_DATAINICIO3");
               GX_FocusControl = edtavContratotermoaditivo_datainicio3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContratoTermoAditivo_DataInicio3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoTermoAditivo_DataInicio3", context.localUtil.Format(AV24ContratoTermoAditivo_DataInicio3, "99/99/99"));
            }
            else
            {
               AV24ContratoTermoAditivo_DataInicio3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datainicio3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoTermoAditivo_DataInicio3", context.localUtil.Format(AV24ContratoTermoAditivo_DataInicio3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datainicio_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Inicio_To3"}), 1, "vCONTRATOTERMOADITIVO_DATAINICIO_TO3");
               GX_FocusControl = edtavContratotermoaditivo_datainicio_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContratoTermoAditivo_DataInicio_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoTermoAditivo_DataInicio_To3", context.localUtil.Format(AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"));
            }
            else
            {
               AV25ContratoTermoAditivo_DataInicio_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datainicio_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoTermoAditivo_DataInicio_To3", context.localUtil.Format(AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datafim3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Fim3"}), 1, "vCONTRATOTERMOADITIVO_DATAFIM3");
               GX_FocusControl = edtavContratotermoaditivo_datafim3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38ContratoTermoAditivo_DataFim3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoTermoAditivo_DataFim3", context.localUtil.Format(AV38ContratoTermoAditivo_DataFim3, "99/99/99"));
            }
            else
            {
               AV38ContratoTermoAditivo_DataFim3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datafim3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoTermoAditivo_DataFim3", context.localUtil.Format(AV38ContratoTermoAditivo_DataFim3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_datafim_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Fim_To3"}), 1, "vCONTRATOTERMOADITIVO_DATAFIM_TO3");
               GX_FocusControl = edtavContratotermoaditivo_datafim_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39ContratoTermoAditivo_DataFim_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoTermoAditivo_DataFim_To3", context.localUtil.Format(AV39ContratoTermoAditivo_DataFim_To3, "99/99/99"));
            }
            else
            {
               AV39ContratoTermoAditivo_DataFim_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_datafim_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoTermoAditivo_DataFim_To3", context.localUtil.Format(AV39ContratoTermoAditivo_DataFim_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_dataassinatura3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Assinatura3"}), 1, "vCONTRATOTERMOADITIVO_DATAASSINATURA3");
               GX_FocusControl = edtavContratotermoaditivo_dataassinatura3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40ContratoTermoAditivo_DataAssinatura3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoTermoAditivo_DataAssinatura3", context.localUtil.Format(AV40ContratoTermoAditivo_DataAssinatura3, "99/99/99"));
            }
            else
            {
               AV40ContratoTermoAditivo_DataAssinatura3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_dataassinatura3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoTermoAditivo_DataAssinatura3", context.localUtil.Format(AV40ContratoTermoAditivo_DataAssinatura3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratotermoaditivo_dataassinatura_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Termo Aditivo_Data Assinatura_To3"}), 1, "vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3");
               GX_FocusControl = edtavContratotermoaditivo_dataassinatura_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41ContratoTermoAditivo_DataAssinatura_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoTermoAditivo_DataAssinatura_To3", context.localUtil.Format(AV41ContratoTermoAditivo_DataAssinatura_To3, "99/99/99"));
            }
            else
            {
               AV41ContratoTermoAditivo_DataAssinatura_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratotermoaditivo_dataassinatura_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoTermoAditivo_DataAssinatura_To3", context.localUtil.Format(AV41ContratoTermoAditivo_DataAssinatura_To3, "99/99/99"));
            }
            A315ContratoTermoAditivo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV43TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContrato_Numero", AV43TFContrato_Numero);
            AV44TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContrato_Numero_Sel", AV44TFContrato_Numero_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratotermoaditivo_datainicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Termo Aditivo_Data Inicio"}), 1, "vTFCONTRATOTERMOADITIVO_DATAINICIO");
               GX_FocusControl = edtavTfcontratotermoaditivo_datainicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFContratoTermoAditivo_DataInicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoTermoAditivo_DataInicio", context.localUtil.Format(AV47TFContratoTermoAditivo_DataInicio, "99/99/99"));
            }
            else
            {
               AV47TFContratoTermoAditivo_DataInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratotermoaditivo_datainicio_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoTermoAditivo_DataInicio", context.localUtil.Format(AV47TFContratoTermoAditivo_DataInicio, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratotermoaditivo_datainicio_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Termo Aditivo_Data Inicio_To"}), 1, "vTFCONTRATOTERMOADITIVO_DATAINICIO_TO");
               GX_FocusControl = edtavTfcontratotermoaditivo_datainicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFContratoTermoAditivo_DataInicio_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoTermoAditivo_DataInicio_To", context.localUtil.Format(AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"));
            }
            else
            {
               AV48TFContratoTermoAditivo_DataInicio_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratotermoaditivo_datainicio_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoTermoAditivo_DataInicio_To", context.localUtil.Format(AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Termo Aditivo_Data Inicio Aux Date"}), 1, "vDDO_CONTRATOTERMOADITIVO_DATAINICIOAUXDATE");
               GX_FocusControl = edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49DDO_ContratoTermoAditivo_DataInicioAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49DDO_ContratoTermoAditivo_DataInicioAuxDate", context.localUtil.Format(AV49DDO_ContratoTermoAditivo_DataInicioAuxDate, "99/99/99"));
            }
            else
            {
               AV49DDO_ContratoTermoAditivo_DataInicioAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49DDO_ContratoTermoAditivo_DataInicioAuxDate", context.localUtil.Format(AV49DDO_ContratoTermoAditivo_DataInicioAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Termo Aditivo_Data Inicio Aux Date To"}), 1, "vDDO_CONTRATOTERMOADITIVO_DATAINICIOAUXDATETO");
               GX_FocusControl = edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo", context.localUtil.Format(AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo, "99/99/99"));
            }
            else
            {
               AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo", context.localUtil.Format(AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratotermoaditivo_datafim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Termo Aditivo_Data Fim"}), 1, "vTFCONTRATOTERMOADITIVO_DATAFIM");
               GX_FocusControl = edtavTfcontratotermoaditivo_datafim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContratoTermoAditivo_DataFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoTermoAditivo_DataFim", context.localUtil.Format(AV53TFContratoTermoAditivo_DataFim, "99/99/99"));
            }
            else
            {
               AV53TFContratoTermoAditivo_DataFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratotermoaditivo_datafim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoTermoAditivo_DataFim", context.localUtil.Format(AV53TFContratoTermoAditivo_DataFim, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratotermoaditivo_datafim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Termo Aditivo_Data Fim_To"}), 1, "vTFCONTRATOTERMOADITIVO_DATAFIM_TO");
               GX_FocusControl = edtavTfcontratotermoaditivo_datafim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContratoTermoAditivo_DataFim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoTermoAditivo_DataFim_To", context.localUtil.Format(AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"));
            }
            else
            {
               AV54TFContratoTermoAditivo_DataFim_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratotermoaditivo_datafim_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoTermoAditivo_DataFim_To", context.localUtil.Format(AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratotermoaditivo_datafimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Termo Aditivo_Data Fim Aux Date"}), 1, "vDDO_CONTRATOTERMOADITIVO_DATAFIMAUXDATE");
               GX_FocusControl = edtavDdo_contratotermoaditivo_datafimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55DDO_ContratoTermoAditivo_DataFimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContratoTermoAditivo_DataFimAuxDate", context.localUtil.Format(AV55DDO_ContratoTermoAditivo_DataFimAuxDate, "99/99/99"));
            }
            else
            {
               AV55DDO_ContratoTermoAditivo_DataFimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratotermoaditivo_datafimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContratoTermoAditivo_DataFimAuxDate", context.localUtil.Format(AV55DDO_ContratoTermoAditivo_DataFimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Termo Aditivo_Data Fim Aux Date To"}), 1, "vDDO_CONTRATOTERMOADITIVO_DATAFIMAUXDATETO");
               GX_FocusControl = edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo", context.localUtil.Format(AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo", context.localUtil.Format(AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratotermoaditivo_dataassinatura_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Termo Aditivo_Data Assinatura"}), 1, "vTFCONTRATOTERMOADITIVO_DATAASSINATURA");
               GX_FocusControl = edtavTfcontratotermoaditivo_dataassinatura_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFContratoTermoAditivo_DataAssinatura = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoTermoAditivo_DataAssinatura", context.localUtil.Format(AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"));
            }
            else
            {
               AV59TFContratoTermoAditivo_DataAssinatura = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratotermoaditivo_dataassinatura_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoTermoAditivo_DataAssinatura", context.localUtil.Format(AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratotermoaditivo_dataassinatura_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Termo Aditivo_Data Assinatura_To"}), 1, "vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO");
               GX_FocusControl = edtavTfcontratotermoaditivo_dataassinatura_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFContratoTermoAditivo_DataAssinatura_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoTermoAditivo_DataAssinatura_To", context.localUtil.Format(AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"));
            }
            else
            {
               AV60TFContratoTermoAditivo_DataAssinatura_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratotermoaditivo_dataassinatura_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoTermoAditivo_DataAssinatura_To", context.localUtil.Format(AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Termo Aditivo_Data Assinatura Aux Date"}), 1, "vDDO_CONTRATOTERMOADITIVO_DATAASSINATURAAUXDATE");
               GX_FocusControl = edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate", context.localUtil.Format(AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate, "99/99/99"));
            }
            else
            {
               AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate", context.localUtil.Format(AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Termo Aditivo_Data Assinatura Aux Date To"}), 1, "vDDO_CONTRATOTERMOADITIVO_DATAASSINATURAAUXDATETO");
               GX_FocusControl = edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo", context.localUtil.Format(AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo, "99/99/99"));
            }
            else
            {
               AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo", context.localUtil.Format(AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo, "99/99/99"));
            }
            AV45ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contrato_NumeroTitleControlIdToReplace", AV45ddo_Contrato_NumeroTitleControlIdToReplace);
            AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace = cgiGet( edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace", AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace);
            AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace = cgiGet( edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace", AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace);
            AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace = cgiGet( edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace", AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_134 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_134"), ",", "."));
            AV66GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV67GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratotermoaditivo_datainicio_Caption = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Caption");
            Ddo_contratotermoaditivo_datainicio_Tooltip = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Tooltip");
            Ddo_contratotermoaditivo_datainicio_Cls = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Cls");
            Ddo_contratotermoaditivo_datainicio_Filteredtext_set = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtext_set");
            Ddo_contratotermoaditivo_datainicio_Filteredtextto_set = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtextto_set");
            Ddo_contratotermoaditivo_datainicio_Dropdownoptionstype = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Dropdownoptionstype");
            Ddo_contratotermoaditivo_datainicio_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Titlecontrolidtoreplace");
            Ddo_contratotermoaditivo_datainicio_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includesortasc"));
            Ddo_contratotermoaditivo_datainicio_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includesortdsc"));
            Ddo_contratotermoaditivo_datainicio_Sortedstatus = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Sortedstatus");
            Ddo_contratotermoaditivo_datainicio_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includefilter"));
            Ddo_contratotermoaditivo_datainicio_Filtertype = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filtertype");
            Ddo_contratotermoaditivo_datainicio_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filterisrange"));
            Ddo_contratotermoaditivo_datainicio_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Includedatalist"));
            Ddo_contratotermoaditivo_datainicio_Sortasc = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Sortasc");
            Ddo_contratotermoaditivo_datainicio_Sortdsc = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Sortdsc");
            Ddo_contratotermoaditivo_datainicio_Cleanfilter = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Cleanfilter");
            Ddo_contratotermoaditivo_datainicio_Rangefilterfrom = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Rangefilterfrom");
            Ddo_contratotermoaditivo_datainicio_Rangefilterto = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Rangefilterto");
            Ddo_contratotermoaditivo_datainicio_Searchbuttontext = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Searchbuttontext");
            Ddo_contratotermoaditivo_datafim_Caption = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Caption");
            Ddo_contratotermoaditivo_datafim_Tooltip = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Tooltip");
            Ddo_contratotermoaditivo_datafim_Cls = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Cls");
            Ddo_contratotermoaditivo_datafim_Filteredtext_set = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtext_set");
            Ddo_contratotermoaditivo_datafim_Filteredtextto_set = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtextto_set");
            Ddo_contratotermoaditivo_datafim_Dropdownoptionstype = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Dropdownoptionstype");
            Ddo_contratotermoaditivo_datafim_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Titlecontrolidtoreplace");
            Ddo_contratotermoaditivo_datafim_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includesortasc"));
            Ddo_contratotermoaditivo_datafim_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includesortdsc"));
            Ddo_contratotermoaditivo_datafim_Sortedstatus = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Sortedstatus");
            Ddo_contratotermoaditivo_datafim_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includefilter"));
            Ddo_contratotermoaditivo_datafim_Filtertype = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filtertype");
            Ddo_contratotermoaditivo_datafim_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filterisrange"));
            Ddo_contratotermoaditivo_datafim_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Includedatalist"));
            Ddo_contratotermoaditivo_datafim_Sortasc = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Sortasc");
            Ddo_contratotermoaditivo_datafim_Sortdsc = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Sortdsc");
            Ddo_contratotermoaditivo_datafim_Cleanfilter = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Cleanfilter");
            Ddo_contratotermoaditivo_datafim_Rangefilterfrom = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Rangefilterfrom");
            Ddo_contratotermoaditivo_datafim_Rangefilterto = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Rangefilterto");
            Ddo_contratotermoaditivo_datafim_Searchbuttontext = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Searchbuttontext");
            Ddo_contratotermoaditivo_dataassinatura_Caption = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Caption");
            Ddo_contratotermoaditivo_dataassinatura_Tooltip = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Tooltip");
            Ddo_contratotermoaditivo_dataassinatura_Cls = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Cls");
            Ddo_contratotermoaditivo_dataassinatura_Filteredtext_set = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtext_set");
            Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_set = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtextto_set");
            Ddo_contratotermoaditivo_dataassinatura_Dropdownoptionstype = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Dropdownoptionstype");
            Ddo_contratotermoaditivo_dataassinatura_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Titlecontrolidtoreplace");
            Ddo_contratotermoaditivo_dataassinatura_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includesortasc"));
            Ddo_contratotermoaditivo_dataassinatura_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includesortdsc"));
            Ddo_contratotermoaditivo_dataassinatura_Sortedstatus = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Sortedstatus");
            Ddo_contratotermoaditivo_dataassinatura_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includefilter"));
            Ddo_contratotermoaditivo_dataassinatura_Filtertype = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filtertype");
            Ddo_contratotermoaditivo_dataassinatura_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filterisrange"));
            Ddo_contratotermoaditivo_dataassinatura_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Includedatalist"));
            Ddo_contratotermoaditivo_dataassinatura_Sortasc = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Sortasc");
            Ddo_contratotermoaditivo_dataassinatura_Sortdsc = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Sortdsc");
            Ddo_contratotermoaditivo_dataassinatura_Cleanfilter = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Cleanfilter");
            Ddo_contratotermoaditivo_dataassinatura_Rangefilterfrom = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Rangefilterfrom");
            Ddo_contratotermoaditivo_dataassinatura_Rangefilterto = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Rangefilterto");
            Ddo_contratotermoaditivo_dataassinatura_Searchbuttontext = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratotermoaditivo_datainicio_Activeeventkey = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Activeeventkey");
            Ddo_contratotermoaditivo_datainicio_Filteredtext_get = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtext_get");
            Ddo_contratotermoaditivo_datainicio_Filteredtextto_get = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAINICIO_Filteredtextto_get");
            Ddo_contratotermoaditivo_datafim_Activeeventkey = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Activeeventkey");
            Ddo_contratotermoaditivo_datafim_Filteredtext_get = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtext_get");
            Ddo_contratotermoaditivo_datafim_Filteredtextto_get = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAFIM_Filteredtextto_get");
            Ddo_contratotermoaditivo_dataassinatura_Activeeventkey = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Activeeventkey");
            Ddo_contratotermoaditivo_dataassinatura_Filteredtext_get = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtext_get");
            Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_get = cgiGet( "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptContratoTermoAditivo";
            A315ContratoTermoAditivo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptcontratotermoaditivo:[SecurityCheckFailed value for]"+"ContratoTermoAditivo_Codigo:"+context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO1"), 0) != AV16ContratoTermoAditivo_DataInicio1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO1"), 0) != AV17ContratoTermoAditivo_DataInicio_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM1"), 0) != AV30ContratoTermoAditivo_DataFim1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO1"), 0) != AV31ContratoTermoAditivo_DataFim_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA1"), 0) != AV32ContratoTermoAditivo_DataAssinatura1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1"), 0) != AV33ContratoTermoAditivo_DataAssinatura_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO2"), 0) != AV20ContratoTermoAditivo_DataInicio2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO2"), 0) != AV21ContratoTermoAditivo_DataInicio_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM2"), 0) != AV34ContratoTermoAditivo_DataFim2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO2"), 0) != AV35ContratoTermoAditivo_DataFim_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA2"), 0) != AV36ContratoTermoAditivo_DataAssinatura2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2"), 0) != AV37ContratoTermoAditivo_DataAssinatura_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO3"), 0) != AV24ContratoTermoAditivo_DataInicio3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAINICIO_TO3"), 0) != AV25ContratoTermoAditivo_DataInicio_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM3"), 0) != AV38ContratoTermoAditivo_DataFim3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAFIM_TO3"), 0) != AV39ContratoTermoAditivo_DataFim_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA3"), 0) != AV40ContratoTermoAditivo_DataAssinatura3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3"), 0) != AV41ContratoTermoAditivo_DataAssinatura_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV43TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV44TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAINICIO"), 0) != AV47TFContratoTermoAditivo_DataInicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAINICIO_TO"), 0) != AV48TFContratoTermoAditivo_DataInicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAFIM"), 0) != AV53TFContratoTermoAditivo_DataFim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAFIM_TO"), 0) != AV54TFContratoTermoAditivo_DataFim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAASSINATURA"), 0) != AV59TFContratoTermoAditivo_DataAssinatura )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO"), 0) != AV60TFContratoTermoAditivo_DataAssinatura_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E267L2 */
         E267L2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E267L2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOTERMOADITIVO_DATAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOTERMOADITIVO_DATAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOTERMOADITIVO_DATAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratotermoaditivo_datainicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratotermoaditivo_datainicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratotermoaditivo_datainicio_Visible), 5, 0)));
         edtavTfcontratotermoaditivo_datainicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratotermoaditivo_datainicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratotermoaditivo_datainicio_to_Visible), 5, 0)));
         edtavTfcontratotermoaditivo_datafim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratotermoaditivo_datafim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratotermoaditivo_datafim_Visible), 5, 0)));
         edtavTfcontratotermoaditivo_datafim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratotermoaditivo_datafim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratotermoaditivo_datafim_to_Visible), 5, 0)));
         edtavTfcontratotermoaditivo_dataassinatura_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratotermoaditivo_dataassinatura_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratotermoaditivo_dataassinatura_Visible), 5, 0)));
         edtavTfcontratotermoaditivo_dataassinatura_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratotermoaditivo_dataassinatura_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratotermoaditivo_dataassinatura_to_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV45ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contrato_NumeroTitleControlIdToReplace", AV45ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratotermoaditivo_datainicio_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoTermoAditivo_DataInicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datainicio_Internalname, "TitleControlIdToReplace", Ddo_contratotermoaditivo_datainicio_Titlecontrolidtoreplace);
         AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace = Ddo_contratotermoaditivo_datainicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace", AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace);
         edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratotermoaditivo_datafim_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoTermoAditivo_DataFim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datafim_Internalname, "TitleControlIdToReplace", Ddo_contratotermoaditivo_datafim_Titlecontrolidtoreplace);
         AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace = Ddo_contratotermoaditivo_datafim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace", AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace);
         edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratotermoaditivo_dataassinatura_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoTermoAditivo_DataAssinatura";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_dataassinatura_Internalname, "TitleControlIdToReplace", Ddo_contratotermoaditivo_dataassinatura_Titlecontrolidtoreplace);
         AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace = Ddo_contratotermoaditivo_dataassinatura_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace", AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace);
         edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contrato Termo Aditivo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtContratoTermoAditivo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data Inicio", 0);
         cmbavOrderedby.addItem("2", "Data Fim", 0);
         cmbavOrderedby.addItem("3", "Data Assinatura", 0);
         cmbavOrderedby.addItem("4", "N� Contrato", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV64DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV64DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E277L2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV42Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContratoTermoAditivo_DataInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoTermoAditivo_DataFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV45ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratoTermoAditivo_DataInicio_Titleformat = 2;
         edtContratoTermoAditivo_DataInicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data de inicio da Vig�ncia", AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_DataInicio_Internalname, "Title", edtContratoTermoAditivo_DataInicio_Title);
         edtContratoTermoAditivo_DataFim_Titleformat = 2;
         edtContratoTermoAditivo_DataFim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data de termino da Vig�ncia", AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_DataFim_Internalname, "Title", edtContratoTermoAditivo_DataFim_Title);
         edtContratoTermoAditivo_DataAssinatura_Titleformat = 2;
         edtContratoTermoAditivo_DataAssinatura_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data de assinatura do Termo", AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_DataAssinatura_Internalname, "Title", edtContratoTermoAditivo_DataAssinatura_Title);
         AV66GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66GridCurrentPage), 10, 0)));
         AV67GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Contrato_NumeroTitleFilterData", AV42Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46ContratoTermoAditivo_DataInicioTitleFilterData", AV46ContratoTermoAditivo_DataInicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52ContratoTermoAditivo_DataFimTitleFilterData", AV52ContratoTermoAditivo_DataFimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData", AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E117L2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV65PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV65PageToGo) ;
         }
      }

      protected void E127L2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContrato_Numero", AV43TFContrato_Numero);
            AV44TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContrato_Numero_Sel", AV44TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E137L2( )
      {
         /* Ddo_contratotermoaditivo_datainicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_datainicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratotermoaditivo_datainicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datainicio_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_datainicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratotermoaditivo_datainicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datainicio_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_datainicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFContratoTermoAditivo_DataInicio = context.localUtil.CToD( Ddo_contratotermoaditivo_datainicio_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoTermoAditivo_DataInicio", context.localUtil.Format(AV47TFContratoTermoAditivo_DataInicio, "99/99/99"));
            AV48TFContratoTermoAditivo_DataInicio_To = context.localUtil.CToD( Ddo_contratotermoaditivo_datainicio_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoTermoAditivo_DataInicio_To", context.localUtil.Format(AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E147L2( )
      {
         /* Ddo_contratotermoaditivo_datafim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_datafim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratotermoaditivo_datafim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datafim_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_datafim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratotermoaditivo_datafim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datafim_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_datafim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFContratoTermoAditivo_DataFim = context.localUtil.CToD( Ddo_contratotermoaditivo_datafim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoTermoAditivo_DataFim", context.localUtil.Format(AV53TFContratoTermoAditivo_DataFim, "99/99/99"));
            AV54TFContratoTermoAditivo_DataFim_To = context.localUtil.CToD( Ddo_contratotermoaditivo_datafim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoTermoAditivo_DataFim_To", context.localUtil.Format(AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E157L2( )
      {
         /* Ddo_contratotermoaditivo_dataassinatura_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_dataassinatura_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratotermoaditivo_dataassinatura_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_dataassinatura_Internalname, "SortedStatus", Ddo_contratotermoaditivo_dataassinatura_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_dataassinatura_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratotermoaditivo_dataassinatura_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_dataassinatura_Internalname, "SortedStatus", Ddo_contratotermoaditivo_dataassinatura_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratotermoaditivo_dataassinatura_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFContratoTermoAditivo_DataAssinatura = context.localUtil.CToD( Ddo_contratotermoaditivo_dataassinatura_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoTermoAditivo_DataAssinatura", context.localUtil.Format(AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"));
            AV60TFContratoTermoAditivo_DataAssinatura_To = context.localUtil.CToD( Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoTermoAditivo_DataAssinatura_To", context.localUtil.Format(AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E287L2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV70Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 134;
         }
         sendrow_1342( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_134_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(134, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E297L2 */
         E297L2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E297L2( )
      {
         /* Enter Routine */
         AV7InOutContratoTermoAditivo_Codigo = A315ContratoTermoAditivo_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoTermoAditivo_Codigo), 6, 0)));
         AV8InOutContratoTermoAditivo_DataInicio = A316ContratoTermoAditivo_DataInicio;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoTermoAditivo_DataInicio", context.localUtil.Format(AV8InOutContratoTermoAditivo_DataInicio, "99/99/99"));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoTermoAditivo_Codigo,context.localUtil.Format( AV8InOutContratoTermoAditivo_DataInicio, "99/99/99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E167L2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E217L2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E177L2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E227L2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E237L2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E187L2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E247L2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E197L2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoTermoAditivo_DataInicio1, AV17ContratoTermoAditivo_DataInicio_To1, AV30ContratoTermoAditivo_DataFim1, AV31ContratoTermoAditivo_DataFim_To1, AV32ContratoTermoAditivo_DataAssinatura1, AV33ContratoTermoAditivo_DataAssinatura_To1, AV19DynamicFiltersSelector2, AV20ContratoTermoAditivo_DataInicio2, AV21ContratoTermoAditivo_DataInicio_To2, AV34ContratoTermoAditivo_DataFim2, AV35ContratoTermoAditivo_DataFim_To2, AV36ContratoTermoAditivo_DataAssinatura2, AV37ContratoTermoAditivo_DataAssinatura_To2, AV23DynamicFiltersSelector3, AV24ContratoTermoAditivo_DataInicio3, AV25ContratoTermoAditivo_DataInicio_To3, AV38ContratoTermoAditivo_DataFim3, AV39ContratoTermoAditivo_DataFim_To3, AV40ContratoTermoAditivo_DataAssinatura3, AV41ContratoTermoAditivo_DataAssinatura_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV43TFContrato_Numero, AV44TFContrato_Numero_Sel, AV47TFContratoTermoAditivo_DataInicio, AV48TFContratoTermoAditivo_DataInicio_To, AV53TFContratoTermoAditivo_DataFim, AV54TFContratoTermoAditivo_DataFim_To, AV59TFContratoTermoAditivo_DataAssinatura, AV60TFContratoTermoAditivo_DataAssinatura_To, AV45ddo_Contrato_NumeroTitleControlIdToReplace, AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace, AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace, AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace, AV71Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E257L2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E207L2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratotermoaditivo_datainicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datainicio_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datainicio_Sortedstatus);
         Ddo_contratotermoaditivo_datafim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datafim_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datafim_Sortedstatus);
         Ddo_contratotermoaditivo_dataassinatura_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_dataassinatura_Internalname, "SortedStatus", Ddo_contratotermoaditivo_dataassinatura_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 4 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratotermoaditivo_datainicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datainicio_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datainicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratotermoaditivo_datafim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datafim_Internalname, "SortedStatus", Ddo_contratotermoaditivo_datafim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratotermoaditivo_dataassinatura_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_dataassinatura_Internalname, "SortedStatus", Ddo_contratotermoaditivo_dataassinatura_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
         {
            tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOTERMOADITIVO_DATAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20ContratoTermoAditivo_DataInicio2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoTermoAditivo_DataInicio2", context.localUtil.Format(AV20ContratoTermoAditivo_DataInicio2, "99/99/99"));
         AV21ContratoTermoAditivo_DataInicio_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoTermoAditivo_DataInicio_To2", context.localUtil.Format(AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOTERMOADITIVO_DATAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24ContratoTermoAditivo_DataInicio3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoTermoAditivo_DataInicio3", context.localUtil.Format(AV24ContratoTermoAditivo_DataInicio3, "99/99/99"));
         AV25ContratoTermoAditivo_DataInicio_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoTermoAditivo_DataInicio_To3", context.localUtil.Format(AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV43TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContrato_Numero", AV43TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV44TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContrato_Numero_Sel", AV44TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV47TFContratoTermoAditivo_DataInicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratoTermoAditivo_DataInicio", context.localUtil.Format(AV47TFContratoTermoAditivo_DataInicio, "99/99/99"));
         Ddo_contratotermoaditivo_datainicio_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datainicio_Internalname, "FilteredText_set", Ddo_contratotermoaditivo_datainicio_Filteredtext_set);
         AV48TFContratoTermoAditivo_DataInicio_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoTermoAditivo_DataInicio_To", context.localUtil.Format(AV48TFContratoTermoAditivo_DataInicio_To, "99/99/99"));
         Ddo_contratotermoaditivo_datainicio_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datainicio_Internalname, "FilteredTextTo_set", Ddo_contratotermoaditivo_datainicio_Filteredtextto_set);
         AV53TFContratoTermoAditivo_DataFim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContratoTermoAditivo_DataFim", context.localUtil.Format(AV53TFContratoTermoAditivo_DataFim, "99/99/99"));
         Ddo_contratotermoaditivo_datafim_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datafim_Internalname, "FilteredText_set", Ddo_contratotermoaditivo_datafim_Filteredtext_set);
         AV54TFContratoTermoAditivo_DataFim_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFContratoTermoAditivo_DataFim_To", context.localUtil.Format(AV54TFContratoTermoAditivo_DataFim_To, "99/99/99"));
         Ddo_contratotermoaditivo_datafim_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_datafim_Internalname, "FilteredTextTo_set", Ddo_contratotermoaditivo_datafim_Filteredtextto_set);
         AV59TFContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoTermoAditivo_DataAssinatura", context.localUtil.Format(AV59TFContratoTermoAditivo_DataAssinatura, "99/99/99"));
         Ddo_contratotermoaditivo_dataassinatura_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_dataassinatura_Internalname, "FilteredText_set", Ddo_contratotermoaditivo_dataassinatura_Filteredtext_set);
         AV60TFContratoTermoAditivo_DataAssinatura_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoTermoAditivo_DataAssinatura_To", context.localUtil.Format(AV60TFContratoTermoAditivo_DataAssinatura_To, "99/99/99"));
         Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratotermoaditivo_dataassinatura_Internalname, "FilteredTextTo_set", Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOTERMOADITIVO_DATAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContratoTermoAditivo_DataInicio1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoTermoAditivo_DataInicio1", context.localUtil.Format(AV16ContratoTermoAditivo_DataInicio1, "99/99/99"));
         AV17ContratoTermoAditivo_DataInicio_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoTermoAditivo_DataInicio_To1", context.localUtil.Format(AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
            {
               AV16ContratoTermoAditivo_DataInicio1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoTermoAditivo_DataInicio1", context.localUtil.Format(AV16ContratoTermoAditivo_DataInicio1, "99/99/99"));
               AV17ContratoTermoAditivo_DataInicio_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoTermoAditivo_DataInicio_To1", context.localUtil.Format(AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
            {
               AV30ContratoTermoAditivo_DataFim1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratoTermoAditivo_DataFim1", context.localUtil.Format(AV30ContratoTermoAditivo_DataFim1, "99/99/99"));
               AV31ContratoTermoAditivo_DataFim_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratoTermoAditivo_DataFim_To1", context.localUtil.Format(AV31ContratoTermoAditivo_DataFim_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
            {
               AV32ContratoTermoAditivo_DataAssinatura1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContratoTermoAditivo_DataAssinatura1", context.localUtil.Format(AV32ContratoTermoAditivo_DataAssinatura1, "99/99/99"));
               AV33ContratoTermoAditivo_DataAssinatura_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContratoTermoAditivo_DataAssinatura_To1", context.localUtil.Format(AV33ContratoTermoAditivo_DataAssinatura_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
               {
                  AV20ContratoTermoAditivo_DataInicio2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoTermoAditivo_DataInicio2", context.localUtil.Format(AV20ContratoTermoAditivo_DataInicio2, "99/99/99"));
                  AV21ContratoTermoAditivo_DataInicio_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoTermoAditivo_DataInicio_To2", context.localUtil.Format(AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
               {
                  AV34ContratoTermoAditivo_DataFim2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContratoTermoAditivo_DataFim2", context.localUtil.Format(AV34ContratoTermoAditivo_DataFim2, "99/99/99"));
                  AV35ContratoTermoAditivo_DataFim_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContratoTermoAditivo_DataFim_To2", context.localUtil.Format(AV35ContratoTermoAditivo_DataFim_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
               {
                  AV36ContratoTermoAditivo_DataAssinatura2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContratoTermoAditivo_DataAssinatura2", context.localUtil.Format(AV36ContratoTermoAditivo_DataAssinatura2, "99/99/99"));
                  AV37ContratoTermoAditivo_DataAssinatura_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoTermoAditivo_DataAssinatura_To2", context.localUtil.Format(AV37ContratoTermoAditivo_DataAssinatura_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
                  {
                     AV24ContratoTermoAditivo_DataInicio3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoTermoAditivo_DataInicio3", context.localUtil.Format(AV24ContratoTermoAditivo_DataInicio3, "99/99/99"));
                     AV25ContratoTermoAditivo_DataInicio_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoTermoAditivo_DataInicio_To3", context.localUtil.Format(AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
                  {
                     AV38ContratoTermoAditivo_DataFim3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoTermoAditivo_DataFim3", context.localUtil.Format(AV38ContratoTermoAditivo_DataFim3, "99/99/99"));
                     AV39ContratoTermoAditivo_DataFim_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoTermoAditivo_DataFim_To3", context.localUtil.Format(AV39ContratoTermoAditivo_DataFim_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
                  {
                     AV40ContratoTermoAditivo_DataAssinatura3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoTermoAditivo_DataAssinatura3", context.localUtil.Format(AV40ContratoTermoAditivo_DataAssinatura3, "99/99/99"));
                     AV41ContratoTermoAditivo_DataAssinatura_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoTermoAditivo_DataAssinatura_To3", context.localUtil.Format(AV41ContratoTermoAditivo_DataAssinatura_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV47TFContratoTermoAditivo_DataInicio) && (DateTime.MinValue==AV48TFContratoTermoAditivo_DataInicio_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOTERMOADITIVO_DATAINICIO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV47TFContratoTermoAditivo_DataInicio, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV48TFContratoTermoAditivo_DataInicio_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV53TFContratoTermoAditivo_DataFim) && (DateTime.MinValue==AV54TFContratoTermoAditivo_DataFim_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOTERMOADITIVO_DATAFIM";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV53TFContratoTermoAditivo_DataFim, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV54TFContratoTermoAditivo_DataFim_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV59TFContratoTermoAditivo_DataAssinatura) && (DateTime.MinValue==AV60TFContratoTermoAditivo_DataAssinatura_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOTERMOADITIVO_DATAASSINATURA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV59TFContratoTermoAditivo_DataAssinatura, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV60TFContratoTermoAditivo_DataAssinatura_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV71Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV16ContratoTermoAditivo_DataInicio1) && (DateTime.MinValue==AV17ContratoTermoAditivo_DataInicio_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16ContratoTermoAditivo_DataInicio1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17ContratoTermoAditivo_DataInicio_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ! ( (DateTime.MinValue==AV30ContratoTermoAditivo_DataFim1) && (DateTime.MinValue==AV31ContratoTermoAditivo_DataFim_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV30ContratoTermoAditivo_DataFim1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV31ContratoTermoAditivo_DataFim_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ! ( (DateTime.MinValue==AV32ContratoTermoAditivo_DataAssinatura1) && (DateTime.MinValue==AV33ContratoTermoAditivo_DataAssinatura_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV32ContratoTermoAditivo_DataAssinatura1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV33ContratoTermoAditivo_DataAssinatura_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV20ContratoTermoAditivo_DataInicio2) && (DateTime.MinValue==AV21ContratoTermoAditivo_DataInicio_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20ContratoTermoAditivo_DataInicio2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21ContratoTermoAditivo_DataInicio_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ! ( (DateTime.MinValue==AV34ContratoTermoAditivo_DataFim2) && (DateTime.MinValue==AV35ContratoTermoAditivo_DataFim_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV34ContratoTermoAditivo_DataFim2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV35ContratoTermoAditivo_DataFim_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ! ( (DateTime.MinValue==AV36ContratoTermoAditivo_DataAssinatura2) && (DateTime.MinValue==AV37ContratoTermoAditivo_DataAssinatura_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV36ContratoTermoAditivo_DataAssinatura2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV37ContratoTermoAditivo_DataAssinatura_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV24ContratoTermoAditivo_DataInicio3) && (DateTime.MinValue==AV25ContratoTermoAditivo_DataInicio_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24ContratoTermoAditivo_DataInicio3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25ContratoTermoAditivo_DataInicio_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ! ( (DateTime.MinValue==AV38ContratoTermoAditivo_DataFim3) && (DateTime.MinValue==AV39ContratoTermoAditivo_DataFim_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV38ContratoTermoAditivo_DataFim3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV39ContratoTermoAditivo_DataFim_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ! ( (DateTime.MinValue==AV40ContratoTermoAditivo_DataAssinatura3) && (DateTime.MinValue==AV41ContratoTermoAditivo_DataAssinatura_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV40ContratoTermoAditivo_DataAssinatura3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV41ContratoTermoAditivo_DataAssinatura_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_7L2( true) ;
         }
         else
         {
            wb_table2_5_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_128_7L2( true) ;
         }
         else
         {
            wb_table3_128_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table3_128_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7L2e( true) ;
         }
         else
         {
            wb_table1_2_7L2e( false) ;
         }
      }

      protected void wb_table3_128_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_131_7L2( true) ;
         }
         else
         {
            wb_table4_131_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table4_131_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_128_7L2e( true) ;
         }
         else
         {
            wb_table3_128_7L2e( false) ;
         }
      }

      protected void wb_table4_131_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"134\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoTermoAditivo_DataInicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoTermoAditivo_DataInicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoTermoAditivo_DataInicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoTermoAditivo_DataFim_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoTermoAditivo_DataFim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoTermoAditivo_DataFim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoTermoAditivo_DataAssinatura_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoTermoAditivo_DataAssinatura_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoTermoAditivo_DataAssinatura_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoTermoAditivo_DataInicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoTermoAditivo_DataInicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoTermoAditivo_DataFim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoTermoAditivo_DataFim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoTermoAditivo_DataAssinatura_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoTermoAditivo_DataAssinatura_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 134 )
         {
            wbEnd = 0;
            nRC_GXsfl_134 = (short)(nGXsfl_134_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_131_7L2e( true) ;
         }
         else
         {
            wb_table4_131_7L2e( false) ;
         }
      }

      protected void wb_table2_5_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_134_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoTermoAditivo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_134_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_7L2( true) ;
         }
         else
         {
            wb_table5_14_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_7L2e( true) ;
         }
         else
         {
            wb_table2_5_7L2e( false) ;
         }
      }

      protected void wb_table5_14_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_7L2( true) ;
         }
         else
         {
            wb_table6_19_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_7L2e( true) ;
         }
         else
         {
            wb_table5_14_7L2e( false) ;
         }
      }

      protected void wb_table6_19_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_134_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoTermoAditivo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_7L2( true) ;
         }
         else
         {
            wb_table7_28_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table8_36_7L2( true) ;
         }
         else
         {
            wb_table8_36_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table8_36_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table9_44_7L2( true) ;
         }
         else
         {
            wb_table9_44_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table9_44_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_134_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_PromptContratoTermoAditivo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_63_7L2( true) ;
         }
         else
         {
            wb_table10_63_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table10_63_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table11_71_7L2( true) ;
         }
         else
         {
            wb_table11_71_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table11_71_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table12_79_7L2( true) ;
         }
         else
         {
            wb_table12_79_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table12_79_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_134_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", "", true, "HLP_PromptContratoTermoAditivo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_98_7L2( true) ;
         }
         else
         {
            wb_table13_98_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table13_98_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table14_106_7L2( true) ;
         }
         else
         {
            wb_table14_106_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table14_106_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table15_114_7L2( true) ;
         }
         else
         {
            wb_table15_114_7L2( false) ;
         }
         return  ;
      }

      protected void wb_table15_114_7L2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_7L2e( true) ;
         }
         else
         {
            wb_table6_19_7L2e( false) ;
         }
      }

      protected void wb_table15_114_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_dataassinatura3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_dataassinatura3_Internalname, context.localUtil.Format(AV40ContratoTermoAditivo_DataAssinatura3, "99/99/99"), context.localUtil.Format( AV40ContratoTermoAditivo_DataAssinatura3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_dataassinatura3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_dataassinatura3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_dataassinatura_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_dataassinatura_to3_Internalname, context.localUtil.Format(AV41ContratoTermoAditivo_DataAssinatura_To3, "99/99/99"), context.localUtil.Format( AV41ContratoTermoAditivo_DataAssinatura_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_dataassinatura_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_dataassinatura_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_114_7L2e( true) ;
         }
         else
         {
            wb_table15_114_7L2e( false) ;
         }
      }

      protected void wb_table14_106_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datafim3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datafim3_Internalname, context.localUtil.Format(AV38ContratoTermoAditivo_DataFim3, "99/99/99"), context.localUtil.Format( AV38ContratoTermoAditivo_DataFim3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datafim3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datafim3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datafim_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datafim_to3_Internalname, context.localUtil.Format(AV39ContratoTermoAditivo_DataFim_To3, "99/99/99"), context.localUtil.Format( AV39ContratoTermoAditivo_DataFim_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datafim_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datafim_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_106_7L2e( true) ;
         }
         else
         {
            wb_table14_106_7L2e( false) ;
         }
      }

      protected void wb_table13_98_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datainicio3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datainicio3_Internalname, context.localUtil.Format(AV24ContratoTermoAditivo_DataInicio3, "99/99/99"), context.localUtil.Format( AV24ContratoTermoAditivo_DataInicio3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datainicio3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datainicio3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datainicio_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datainicio_to3_Internalname, context.localUtil.Format(AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"), context.localUtil.Format( AV25ContratoTermoAditivo_DataInicio_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datainicio_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datainicio_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_98_7L2e( true) ;
         }
         else
         {
            wb_table13_98_7L2e( false) ;
         }
      }

      protected void wb_table12_79_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_dataassinatura2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_dataassinatura2_Internalname, context.localUtil.Format(AV36ContratoTermoAditivo_DataAssinatura2, "99/99/99"), context.localUtil.Format( AV36ContratoTermoAditivo_DataAssinatura2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_dataassinatura2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_dataassinatura2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_dataassinatura_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_dataassinatura_to2_Internalname, context.localUtil.Format(AV37ContratoTermoAditivo_DataAssinatura_To2, "99/99/99"), context.localUtil.Format( AV37ContratoTermoAditivo_DataAssinatura_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_dataassinatura_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_dataassinatura_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_79_7L2e( true) ;
         }
         else
         {
            wb_table12_79_7L2e( false) ;
         }
      }

      protected void wb_table11_71_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datafim2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datafim2_Internalname, context.localUtil.Format(AV34ContratoTermoAditivo_DataFim2, "99/99/99"), context.localUtil.Format( AV34ContratoTermoAditivo_DataFim2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datafim2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datafim2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datafim_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datafim_to2_Internalname, context.localUtil.Format(AV35ContratoTermoAditivo_DataFim_To2, "99/99/99"), context.localUtil.Format( AV35ContratoTermoAditivo_DataFim_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datafim_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datafim_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_71_7L2e( true) ;
         }
         else
         {
            wb_table11_71_7L2e( false) ;
         }
      }

      protected void wb_table10_63_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datainicio2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datainicio2_Internalname, context.localUtil.Format(AV20ContratoTermoAditivo_DataInicio2, "99/99/99"), context.localUtil.Format( AV20ContratoTermoAditivo_DataInicio2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datainicio2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datainicio2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datainicio_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datainicio_to2_Internalname, context.localUtil.Format(AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"), context.localUtil.Format( AV21ContratoTermoAditivo_DataInicio_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datainicio_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datainicio_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_63_7L2e( true) ;
         }
         else
         {
            wb_table10_63_7L2e( false) ;
         }
      }

      protected void wb_table9_44_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_dataassinatura1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_dataassinatura1_Internalname, context.localUtil.Format(AV32ContratoTermoAditivo_DataAssinatura1, "99/99/99"), context.localUtil.Format( AV32ContratoTermoAditivo_DataAssinatura1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_dataassinatura1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_dataassinatura1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_dataassinatura_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_dataassinatura_to1_Internalname, context.localUtil.Format(AV33ContratoTermoAditivo_DataAssinatura_To1, "99/99/99"), context.localUtil.Format( AV33ContratoTermoAditivo_DataAssinatura_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_dataassinatura_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_dataassinatura_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_44_7L2e( true) ;
         }
         else
         {
            wb_table9_44_7L2e( false) ;
         }
      }

      protected void wb_table8_36_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datafim1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datafim1_Internalname, context.localUtil.Format(AV30ContratoTermoAditivo_DataFim1, "99/99/99"), context.localUtil.Format( AV30ContratoTermoAditivo_DataFim1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datafim1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datafim1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datafim_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datafim_to1_Internalname, context.localUtil.Format(AV31ContratoTermoAditivo_DataFim_To1, "99/99/99"), context.localUtil.Format( AV31ContratoTermoAditivo_DataFim_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datafim_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datafim_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_36_7L2e( true) ;
         }
         else
         {
            wb_table8_36_7L2e( false) ;
         }
      }

      protected void wb_table7_28_7L2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Internalname, tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datainicio1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datainicio1_Internalname, context.localUtil.Format(AV16ContratoTermoAditivo_DataInicio1, "99/99/99"), context.localUtil.Format( AV16ContratoTermoAditivo_DataInicio1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datainicio1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datainicio1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_134_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratotermoaditivo_datainicio_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratotermoaditivo_datainicio_to1_Internalname, context.localUtil.Format(AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"), context.localUtil.Format( AV17ContratoTermoAditivo_DataInicio_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratotermoaditivo_datainicio_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtavContratotermoaditivo_datainicio_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_7L2e( true) ;
         }
         else
         {
            wb_table7_28_7L2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoTermoAditivo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoTermoAditivo_Codigo), 6, 0)));
         AV8InOutContratoTermoAditivo_DataInicio = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoTermoAditivo_DataInicio", context.localUtil.Format(AV8InOutContratoTermoAditivo_DataInicio, "99/99/99"));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7L2( ) ;
         WS7L2( ) ;
         WE7L2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117372845");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratotermoaditivo.js", "?20203117372845");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1342( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_134_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_134_idx;
         edtContratoTermoAditivo_DataInicio_Internalname = "CONTRATOTERMOADITIVO_DATAINICIO_"+sGXsfl_134_idx;
         edtContratoTermoAditivo_DataFim_Internalname = "CONTRATOTERMOADITIVO_DATAFIM_"+sGXsfl_134_idx;
         edtContratoTermoAditivo_DataAssinatura_Internalname = "CONTRATOTERMOADITIVO_DATAASSINATURA_"+sGXsfl_134_idx;
      }

      protected void SubsflControlProps_fel_1342( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_134_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_134_fel_idx;
         edtContratoTermoAditivo_DataInicio_Internalname = "CONTRATOTERMOADITIVO_DATAINICIO_"+sGXsfl_134_fel_idx;
         edtContratoTermoAditivo_DataFim_Internalname = "CONTRATOTERMOADITIVO_DATAFIM_"+sGXsfl_134_fel_idx;
         edtContratoTermoAditivo_DataAssinatura_Internalname = "CONTRATOTERMOADITIVO_DATAASSINATURA_"+sGXsfl_134_fel_idx;
      }

      protected void sendrow_1342( )
      {
         SubsflControlProps_1342( ) ;
         WB7L0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_134_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_134_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_134_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 135,'',false,'',134)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV70Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_134_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)134,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoTermoAditivo_DataInicio_Internalname,context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"),context.localUtil.Format( A316ContratoTermoAditivo_DataInicio, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoTermoAditivo_DataInicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)134,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoTermoAditivo_DataFim_Internalname,context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"),context.localUtil.Format( A317ContratoTermoAditivo_DataFim, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoTermoAditivo_DataFim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)134,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoTermoAditivo_DataAssinatura_Internalname,context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"),context.localUtil.Format( A318ContratoTermoAditivo_DataAssinatura, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoTermoAditivo_DataAssinatura_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)134,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOTERMOADITIVO_DATAINICIO"+"_"+sGXsfl_134_idx, GetSecureSignedToken( sGXsfl_134_idx, A316ContratoTermoAditivo_DataInicio));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOTERMOADITIVO_DATAFIM"+"_"+sGXsfl_134_idx, GetSecureSignedToken( sGXsfl_134_idx, A317ContratoTermoAditivo_DataFim));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOTERMOADITIVO_DATAASSINATURA"+"_"+sGXsfl_134_idx, GetSecureSignedToken( sGXsfl_134_idx, A318ContratoTermoAditivo_DataAssinatura));
            GridContainer.AddRow(GridRow);
            nGXsfl_134_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_134_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_134_idx+1));
            sGXsfl_134_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_134_idx), 4, 0)), 4, "0");
            SubsflControlProps_1342( ) ;
         }
         /* End function sendrow_1342 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContratotermoaditivo_datainicio1_Internalname = "vCONTRATOTERMOADITIVO_DATAINICIO1";
         lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO_RANGEMIDDLETEXT1";
         edtavContratotermoaditivo_datainicio_to1_Internalname = "vCONTRATOTERMOADITIVO_DATAINICIO_TO1";
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO1";
         edtavContratotermoaditivo_datafim1_Internalname = "vCONTRATOTERMOADITIVO_DATAFIM1";
         lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM_RANGEMIDDLETEXT1";
         edtavContratotermoaditivo_datafim_to1_Internalname = "vCONTRATOTERMOADITIVO_DATAFIM_TO1";
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM1";
         edtavContratotermoaditivo_dataassinatura1_Internalname = "vCONTRATOTERMOADITIVO_DATAASSINATURA1";
         lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA_RANGEMIDDLETEXT1";
         edtavContratotermoaditivo_dataassinatura_to1_Internalname = "vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1";
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContratotermoaditivo_datainicio2_Internalname = "vCONTRATOTERMOADITIVO_DATAINICIO2";
         lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO_RANGEMIDDLETEXT2";
         edtavContratotermoaditivo_datainicio_to2_Internalname = "vCONTRATOTERMOADITIVO_DATAINICIO_TO2";
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO2";
         edtavContratotermoaditivo_datafim2_Internalname = "vCONTRATOTERMOADITIVO_DATAFIM2";
         lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM_RANGEMIDDLETEXT2";
         edtavContratotermoaditivo_datafim_to2_Internalname = "vCONTRATOTERMOADITIVO_DATAFIM_TO2";
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM2";
         edtavContratotermoaditivo_dataassinatura2_Internalname = "vCONTRATOTERMOADITIVO_DATAASSINATURA2";
         lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA_RANGEMIDDLETEXT2";
         edtavContratotermoaditivo_dataassinatura_to2_Internalname = "vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2";
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContratotermoaditivo_datainicio3_Internalname = "vCONTRATOTERMOADITIVO_DATAINICIO3";
         lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO_RANGEMIDDLETEXT3";
         edtavContratotermoaditivo_datainicio_to3_Internalname = "vCONTRATOTERMOADITIVO_DATAINICIO_TO3";
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO3";
         edtavContratotermoaditivo_datafim3_Internalname = "vCONTRATOTERMOADITIVO_DATAFIM3";
         lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM_RANGEMIDDLETEXT3";
         edtavContratotermoaditivo_datafim_to3_Internalname = "vCONTRATOTERMOADITIVO_DATAFIM_TO3";
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM3";
         edtavContratotermoaditivo_dataassinatura3_Internalname = "vCONTRATOTERMOADITIVO_DATAASSINATURA3";
         lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA_RANGEMIDDLETEXT3";
         edtavContratotermoaditivo_dataassinatura_to3_Internalname = "vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3";
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratoTermoAditivo_DataInicio_Internalname = "CONTRATOTERMOADITIVO_DATAINICIO";
         edtContratoTermoAditivo_DataFim_Internalname = "CONTRATOTERMOADITIVO_DATAFIM";
         edtContratoTermoAditivo_DataAssinatura_Internalname = "CONTRATOTERMOADITIVO_DATAASSINATURA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoTermoAditivo_Codigo_Internalname = "CONTRATOTERMOADITIVO_CODIGO";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratotermoaditivo_datainicio_Internalname = "vTFCONTRATOTERMOADITIVO_DATAINICIO";
         edtavTfcontratotermoaditivo_datainicio_to_Internalname = "vTFCONTRATOTERMOADITIVO_DATAINICIO_TO";
         edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAINICIOAUXDATE";
         edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAINICIOAUXDATETO";
         divDdo_contratotermoaditivo_datainicioauxdates_Internalname = "DDO_CONTRATOTERMOADITIVO_DATAINICIOAUXDATES";
         edtavTfcontratotermoaditivo_datafim_Internalname = "vTFCONTRATOTERMOADITIVO_DATAFIM";
         edtavTfcontratotermoaditivo_datafim_to_Internalname = "vTFCONTRATOTERMOADITIVO_DATAFIM_TO";
         edtavDdo_contratotermoaditivo_datafimauxdate_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAFIMAUXDATE";
         edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAFIMAUXDATETO";
         divDdo_contratotermoaditivo_datafimauxdates_Internalname = "DDO_CONTRATOTERMOADITIVO_DATAFIMAUXDATES";
         edtavTfcontratotermoaditivo_dataassinatura_Internalname = "vTFCONTRATOTERMOADITIVO_DATAASSINATURA";
         edtavTfcontratotermoaditivo_dataassinatura_to_Internalname = "vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO";
         edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAASSINATURAAUXDATE";
         edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAASSINATURAAUXDATETO";
         divDdo_contratotermoaditivo_dataassinaturaauxdates_Internalname = "DDO_CONTRATOTERMOADITIVO_DATAASSINATURAAUXDATES";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratotermoaditivo_datainicio_Internalname = "DDO_CONTRATOTERMOADITIVO_DATAINICIO";
         edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE";
         Ddo_contratotermoaditivo_datafim_Internalname = "DDO_CONTRATOTERMOADITIVO_DATAFIM";
         edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE";
         Ddo_contratotermoaditivo_dataassinatura_Internalname = "DDO_CONTRATOTERMOADITIVO_DATAASSINATURA";
         edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoTermoAditivo_DataAssinatura_Jsonclick = "";
         edtContratoTermoAditivo_DataFim_Jsonclick = "";
         edtContratoTermoAditivo_DataInicio_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratotermoaditivo_datainicio_to1_Jsonclick = "";
         edtavContratotermoaditivo_datainicio1_Jsonclick = "";
         edtavContratotermoaditivo_datafim_to1_Jsonclick = "";
         edtavContratotermoaditivo_datafim1_Jsonclick = "";
         edtavContratotermoaditivo_dataassinatura_to1_Jsonclick = "";
         edtavContratotermoaditivo_dataassinatura1_Jsonclick = "";
         edtavContratotermoaditivo_datainicio_to2_Jsonclick = "";
         edtavContratotermoaditivo_datainicio2_Jsonclick = "";
         edtavContratotermoaditivo_datafim_to2_Jsonclick = "";
         edtavContratotermoaditivo_datafim2_Jsonclick = "";
         edtavContratotermoaditivo_dataassinatura_to2_Jsonclick = "";
         edtavContratotermoaditivo_dataassinatura2_Jsonclick = "";
         edtavContratotermoaditivo_datainicio_to3_Jsonclick = "";
         edtavContratotermoaditivo_datainicio3_Jsonclick = "";
         edtavContratotermoaditivo_datafim_to3_Jsonclick = "";
         edtavContratotermoaditivo_datafim3_Jsonclick = "";
         edtavContratotermoaditivo_dataassinatura_to3_Jsonclick = "";
         edtavContratotermoaditivo_dataassinatura3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoTermoAditivo_DataAssinatura_Titleformat = 0;
         edtContratoTermoAditivo_DataFim_Titleformat = 0;
         edtContratoTermoAditivo_DataInicio_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible = 1;
         tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible = 1;
         edtContratoTermoAditivo_DataAssinatura_Title = "Data de assinatura do Termo";
         edtContratoTermoAditivo_DataFim_Title = "Data de termino da Vig�ncia";
         edtContratoTermoAditivo_DataInicio_Title = "Data de inicio da Vig�ncia";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Jsonclick = "";
         edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Jsonclick = "";
         edtavTfcontratotermoaditivo_dataassinatura_to_Jsonclick = "";
         edtavTfcontratotermoaditivo_dataassinatura_to_Visible = 1;
         edtavTfcontratotermoaditivo_dataassinatura_Jsonclick = "";
         edtavTfcontratotermoaditivo_dataassinatura_Visible = 1;
         edtavDdo_contratotermoaditivo_datafimauxdateto_Jsonclick = "";
         edtavDdo_contratotermoaditivo_datafimauxdate_Jsonclick = "";
         edtavTfcontratotermoaditivo_datafim_to_Jsonclick = "";
         edtavTfcontratotermoaditivo_datafim_to_Visible = 1;
         edtavTfcontratotermoaditivo_datafim_Jsonclick = "";
         edtavTfcontratotermoaditivo_datafim_Visible = 1;
         edtavDdo_contratotermoaditivo_datainicioauxdateto_Jsonclick = "";
         edtavDdo_contratotermoaditivo_datainicioauxdate_Jsonclick = "";
         edtavTfcontratotermoaditivo_datainicio_to_Jsonclick = "";
         edtavTfcontratotermoaditivo_datainicio_to_Visible = 1;
         edtavTfcontratotermoaditivo_datainicio_Jsonclick = "";
         edtavTfcontratotermoaditivo_datainicio_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContratoTermoAditivo_Codigo_Jsonclick = "";
         edtContratoTermoAditivo_Codigo_Visible = 1;
         Ddo_contratotermoaditivo_dataassinatura_Searchbuttontext = "Pesquisar";
         Ddo_contratotermoaditivo_dataassinatura_Rangefilterto = "At�";
         Ddo_contratotermoaditivo_dataassinatura_Rangefilterfrom = "Desde";
         Ddo_contratotermoaditivo_dataassinatura_Cleanfilter = "Limpar pesquisa";
         Ddo_contratotermoaditivo_dataassinatura_Sortdsc = "Ordenar de Z � A";
         Ddo_contratotermoaditivo_dataassinatura_Sortasc = "Ordenar de A � Z";
         Ddo_contratotermoaditivo_dataassinatura_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratotermoaditivo_dataassinatura_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_dataassinatura_Filtertype = "Date";
         Ddo_contratotermoaditivo_dataassinatura_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_dataassinatura_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_dataassinatura_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_dataassinatura_Titlecontrolidtoreplace = "";
         Ddo_contratotermoaditivo_dataassinatura_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratotermoaditivo_dataassinatura_Cls = "ColumnSettings";
         Ddo_contratotermoaditivo_dataassinatura_Tooltip = "Op��es";
         Ddo_contratotermoaditivo_dataassinatura_Caption = "";
         Ddo_contratotermoaditivo_datafim_Searchbuttontext = "Pesquisar";
         Ddo_contratotermoaditivo_datafim_Rangefilterto = "At�";
         Ddo_contratotermoaditivo_datafim_Rangefilterfrom = "Desde";
         Ddo_contratotermoaditivo_datafim_Cleanfilter = "Limpar pesquisa";
         Ddo_contratotermoaditivo_datafim_Sortdsc = "Ordenar de Z � A";
         Ddo_contratotermoaditivo_datafim_Sortasc = "Ordenar de A � Z";
         Ddo_contratotermoaditivo_datafim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratotermoaditivo_datafim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datafim_Filtertype = "Date";
         Ddo_contratotermoaditivo_datafim_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datafim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datafim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datafim_Titlecontrolidtoreplace = "";
         Ddo_contratotermoaditivo_datafim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratotermoaditivo_datafim_Cls = "ColumnSettings";
         Ddo_contratotermoaditivo_datafim_Tooltip = "Op��es";
         Ddo_contratotermoaditivo_datafim_Caption = "";
         Ddo_contratotermoaditivo_datainicio_Searchbuttontext = "Pesquisar";
         Ddo_contratotermoaditivo_datainicio_Rangefilterto = "At�";
         Ddo_contratotermoaditivo_datainicio_Rangefilterfrom = "Desde";
         Ddo_contratotermoaditivo_datainicio_Cleanfilter = "Limpar pesquisa";
         Ddo_contratotermoaditivo_datainicio_Sortdsc = "Ordenar de Z � A";
         Ddo_contratotermoaditivo_datainicio_Sortasc = "Ordenar de A � Z";
         Ddo_contratotermoaditivo_datainicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratotermoaditivo_datainicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datainicio_Filtertype = "Date";
         Ddo_contratotermoaditivo_datainicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datainicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datainicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratotermoaditivo_datainicio_Titlecontrolidtoreplace = "";
         Ddo_contratotermoaditivo_datainicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratotermoaditivo_datainicio_Cls = "ColumnSettings";
         Ddo_contratotermoaditivo_datainicio_Tooltip = "Op��es";
         Ddo_contratotermoaditivo_datainicio_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetPromptContratoTermoAditivoFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Termo Aditivo";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''}],oparms:[{av:'AV42Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV46ContratoTermoAditivo_DataInicioTitleFilterData',fld:'vCONTRATOTERMOADITIVO_DATAINICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ContratoTermoAditivo_DataFimTitleFilterData',fld:'vCONTRATOTERMOADITIVO_DATAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURATITLEFILTERDATA',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratoTermoAditivo_DataInicio_Titleformat',ctrl:'CONTRATOTERMOADITIVO_DATAINICIO',prop:'Titleformat'},{av:'edtContratoTermoAditivo_DataInicio_Title',ctrl:'CONTRATOTERMOADITIVO_DATAINICIO',prop:'Title'},{av:'edtContratoTermoAditivo_DataFim_Titleformat',ctrl:'CONTRATOTERMOADITIVO_DATAFIM',prop:'Titleformat'},{av:'edtContratoTermoAditivo_DataFim_Title',ctrl:'CONTRATOTERMOADITIVO_DATAFIM',prop:'Title'},{av:'edtContratoTermoAditivo_DataAssinatura_Titleformat',ctrl:'CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'Titleformat'},{av:'edtContratoTermoAditivo_DataAssinatura_Title',ctrl:'CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'Title'},{av:'AV66GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E117L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E127L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratotermoaditivo_datainicio_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_datafim_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_dataassinatura_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOTERMOADITIVO_DATAINICIO.ONOPTIONCLICKED","{handler:'E137L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratotermoaditivo_datainicio_Activeeventkey',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'ActiveEventKey'},{av:'Ddo_contratotermoaditivo_datainicio_Filteredtext_get',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'FilteredText_get'},{av:'Ddo_contratotermoaditivo_datainicio_Filteredtextto_get',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratotermoaditivo_datainicio_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'SortedStatus'},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_datafim_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_dataassinatura_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOTERMOADITIVO_DATAFIM.ONOPTIONCLICKED","{handler:'E147L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratotermoaditivo_datafim_Activeeventkey',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'ActiveEventKey'},{av:'Ddo_contratotermoaditivo_datafim_Filteredtext_get',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'FilteredText_get'},{av:'Ddo_contratotermoaditivo_datafim_Filteredtextto_get',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratotermoaditivo_datafim_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'SortedStatus'},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_datainicio_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_dataassinatura_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOTERMOADITIVO_DATAASSINATURA.ONOPTIONCLICKED","{handler:'E157L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratotermoaditivo_dataassinatura_Activeeventkey',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'ActiveEventKey'},{av:'Ddo_contratotermoaditivo_dataassinatura_Filteredtext_get',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'FilteredText_get'},{av:'Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_get',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratotermoaditivo_dataassinatura_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'SortedStatus'},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_datainicio_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'SortedStatus'},{av:'Ddo_contratotermoaditivo_datafim_Sortedstatus',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E287L2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E297L2',iparms:[{av:'A315ContratoTermoAditivo_Codigo',fld:'CONTRATOTERMOADITIVO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A316ContratoTermoAditivo_DataInicio',fld:'CONTRATOTERMOADITIVO_DATAINICIO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutContratoTermoAditivo_Codigo',fld:'vINOUTCONTRATOTERMOADITIVO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoTermoAditivo_DataInicio',fld:'vINOUTCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E167L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E217L2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E177L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E227L2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E237L2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E187L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E247L2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E197L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E257L2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E207L2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'AV45ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace',fld:'vDDO_CONTRATOTERMOADITIVO_DATAASSINATURATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV43TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV44TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV47TFContratoTermoAditivo_DataInicio',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'Ddo_contratotermoaditivo_datainicio_Filteredtext_set',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'FilteredText_set'},{av:'AV48TFContratoTermoAditivo_DataInicio_To',fld:'vTFCONTRATOTERMOADITIVO_DATAINICIO_TO',pic:'',nv:''},{av:'Ddo_contratotermoaditivo_datainicio_Filteredtextto_set',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAINICIO',prop:'FilteredTextTo_set'},{av:'AV53TFContratoTermoAditivo_DataFim',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'Ddo_contratotermoaditivo_datafim_Filteredtext_set',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'FilteredText_set'},{av:'AV54TFContratoTermoAditivo_DataFim_To',fld:'vTFCONTRATOTERMOADITIVO_DATAFIM_TO',pic:'',nv:''},{av:'Ddo_contratotermoaditivo_datafim_Filteredtextto_set',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAFIM',prop:'FilteredTextTo_set'},{av:'AV59TFContratoTermoAditivo_DataAssinatura',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA',pic:'',nv:''},{av:'Ddo_contratotermoaditivo_dataassinatura_Filteredtext_set',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'FilteredText_set'},{av:'AV60TFContratoTermoAditivo_DataAssinatura_To',fld:'vTFCONTRATOTERMOADITIVO_DATAASSINATURA_TO',pic:'',nv:''},{av:'Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_set',ctrl:'DDO_CONTRATOTERMOADITIVO_DATAASSINATURA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoTermoAditivo_DataInicio1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO1',pic:'',nv:''},{av:'AV17ContratoTermoAditivo_DataInicio_To1',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoTermoAditivo_DataInicio2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO2',pic:'',nv:''},{av:'AV21ContratoTermoAditivo_DataInicio_To2',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoTermoAditivo_DataInicio3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO3',pic:'',nv:''},{av:'AV25ContratoTermoAditivo_DataInicio_To3',fld:'vCONTRATOTERMOADITIVO_DATAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV30ContratoTermoAditivo_DataFim1',fld:'vCONTRATOTERMOADITIVO_DATAFIM1',pic:'',nv:''},{av:'AV31ContratoTermoAditivo_DataFim_To1',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO1',pic:'',nv:''},{av:'AV32ContratoTermoAditivo_DataAssinatura1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA1',pic:'',nv:''},{av:'AV33ContratoTermoAditivo_DataAssinatura_To1',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV34ContratoTermoAditivo_DataFim2',fld:'vCONTRATOTERMOADITIVO_DATAFIM2',pic:'',nv:''},{av:'AV35ContratoTermoAditivo_DataFim_To2',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO2',pic:'',nv:''},{av:'AV36ContratoTermoAditivo_DataAssinatura2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA2',pic:'',nv:''},{av:'AV37ContratoTermoAditivo_DataAssinatura_To2',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO2',pic:'',nv:''},{av:'AV38ContratoTermoAditivo_DataFim3',fld:'vCONTRATOTERMOADITIVO_DATAFIM3',pic:'',nv:''},{av:'AV39ContratoTermoAditivo_DataFim_To3',fld:'vCONTRATOTERMOADITIVO_DATAFIM_TO3',pic:'',nv:''},{av:'AV40ContratoTermoAditivo_DataAssinatura3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA3',pic:'',nv:''},{av:'AV41ContratoTermoAditivo_DataAssinatura_To3',fld:'vCONTRATOTERMOADITIVO_DATAASSINATURA_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAFIM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOTERMOADITIVO_DATAASSINATURA3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratoTermoAditivo_DataInicio = DateTime.MinValue;
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratotermoaditivo_datainicio_Activeeventkey = "";
         Ddo_contratotermoaditivo_datainicio_Filteredtext_get = "";
         Ddo_contratotermoaditivo_datainicio_Filteredtextto_get = "";
         Ddo_contratotermoaditivo_datafim_Activeeventkey = "";
         Ddo_contratotermoaditivo_datafim_Filteredtext_get = "";
         Ddo_contratotermoaditivo_datafim_Filteredtextto_get = "";
         Ddo_contratotermoaditivo_dataassinatura_Activeeventkey = "";
         Ddo_contratotermoaditivo_dataassinatura_Filteredtext_get = "";
         Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContratoTermoAditivo_DataInicio1 = DateTime.MinValue;
         AV17ContratoTermoAditivo_DataInicio_To1 = DateTime.MinValue;
         AV30ContratoTermoAditivo_DataFim1 = DateTime.MinValue;
         AV31ContratoTermoAditivo_DataFim_To1 = DateTime.MinValue;
         AV32ContratoTermoAditivo_DataAssinatura1 = DateTime.MinValue;
         AV33ContratoTermoAditivo_DataAssinatura_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV20ContratoTermoAditivo_DataInicio2 = DateTime.MinValue;
         AV21ContratoTermoAditivo_DataInicio_To2 = DateTime.MinValue;
         AV34ContratoTermoAditivo_DataFim2 = DateTime.MinValue;
         AV35ContratoTermoAditivo_DataFim_To2 = DateTime.MinValue;
         AV36ContratoTermoAditivo_DataAssinatura2 = DateTime.MinValue;
         AV37ContratoTermoAditivo_DataAssinatura_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV24ContratoTermoAditivo_DataInicio3 = DateTime.MinValue;
         AV25ContratoTermoAditivo_DataInicio_To3 = DateTime.MinValue;
         AV38ContratoTermoAditivo_DataFim3 = DateTime.MinValue;
         AV39ContratoTermoAditivo_DataFim_To3 = DateTime.MinValue;
         AV40ContratoTermoAditivo_DataAssinatura3 = DateTime.MinValue;
         AV41ContratoTermoAditivo_DataAssinatura_To3 = DateTime.MinValue;
         AV43TFContrato_Numero = "";
         AV44TFContrato_Numero_Sel = "";
         AV47TFContratoTermoAditivo_DataInicio = DateTime.MinValue;
         AV48TFContratoTermoAditivo_DataInicio_To = DateTime.MinValue;
         AV53TFContratoTermoAditivo_DataFim = DateTime.MinValue;
         AV54TFContratoTermoAditivo_DataFim_To = DateTime.MinValue;
         AV59TFContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         AV60TFContratoTermoAditivo_DataAssinatura_To = DateTime.MinValue;
         AV45ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace = "";
         AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace = "";
         AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace = "";
         AV71Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV64DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV42Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ContratoTermoAditivo_DataInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoTermoAditivo_DataFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratotermoaditivo_datainicio_Filteredtext_set = "";
         Ddo_contratotermoaditivo_datainicio_Filteredtextto_set = "";
         Ddo_contratotermoaditivo_datainicio_Sortedstatus = "";
         Ddo_contratotermoaditivo_datafim_Filteredtext_set = "";
         Ddo_contratotermoaditivo_datafim_Filteredtextto_set = "";
         Ddo_contratotermoaditivo_datafim_Sortedstatus = "";
         Ddo_contratotermoaditivo_dataassinatura_Filteredtext_set = "";
         Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_set = "";
         Ddo_contratotermoaditivo_dataassinatura_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV49DDO_ContratoTermoAditivo_DataInicioAuxDate = DateTime.MinValue;
         AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo = DateTime.MinValue;
         AV55DDO_ContratoTermoAditivo_DataFimAuxDate = DateTime.MinValue;
         AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo = DateTime.MinValue;
         AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate = DateTime.MinValue;
         AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV70Select_GXI = "";
         A77Contrato_Numero = "";
         A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         A318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV43TFContrato_Numero = "";
         H007L2_A74Contrato_Codigo = new int[1] ;
         H007L2_A315ContratoTermoAditivo_Codigo = new int[1] ;
         H007L2_A318ContratoTermoAditivo_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         H007L2_n318ContratoTermoAditivo_DataAssinatura = new bool[] {false} ;
         H007L2_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         H007L2_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         H007L2_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         H007L2_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         H007L2_A77Contrato_Numero = new String[] {""} ;
         H007L3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratotermoaditivo__default(),
            new Object[][] {
                new Object[] {
               H007L2_A74Contrato_Codigo, H007L2_A315ContratoTermoAditivo_Codigo, H007L2_A318ContratoTermoAditivo_DataAssinatura, H007L2_n318ContratoTermoAditivo_DataAssinatura, H007L2_A317ContratoTermoAditivo_DataFim, H007L2_n317ContratoTermoAditivo_DataFim, H007L2_A316ContratoTermoAditivo_DataInicio, H007L2_n316ContratoTermoAditivo_DataInicio, H007L2_A77Contrato_Numero
               }
               , new Object[] {
               H007L3_AGRID_nRecordCount
               }
            }
         );
         AV71Pgmname = "PromptContratoTermoAditivo";
         /* GeneXus formulas. */
         AV71Pgmname = "PromptContratoTermoAditivo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_134 ;
      private short nGXsfl_134_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_134_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratoTermoAditivo_DataInicio_Titleformat ;
      private short edtContratoTermoAditivo_DataFim_Titleformat ;
      private short edtContratoTermoAditivo_DataAssinatura_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoTermoAditivo_Codigo ;
      private int wcpOAV7InOutContratoTermoAditivo_Codigo ;
      private int subGrid_Rows ;
      private int A315ContratoTermoAditivo_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int edtContratoTermoAditivo_Codigo_Visible ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratotermoaditivo_datainicio_Visible ;
      private int edtavTfcontratotermoaditivo_datainicio_to_Visible ;
      private int edtavTfcontratotermoaditivo_datafim_Visible ;
      private int edtavTfcontratotermoaditivo_datafim_to_Visible ;
      private int edtavTfcontratotermoaditivo_dataassinatura_Visible ;
      private int edtavTfcontratotermoaditivo_dataassinatura_to_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A74Contrato_Codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV65PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Visible ;
      private int tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV66GridCurrentPage ;
      private long AV67GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratotermoaditivo_datainicio_Activeeventkey ;
      private String Ddo_contratotermoaditivo_datainicio_Filteredtext_get ;
      private String Ddo_contratotermoaditivo_datainicio_Filteredtextto_get ;
      private String Ddo_contratotermoaditivo_datafim_Activeeventkey ;
      private String Ddo_contratotermoaditivo_datafim_Filteredtext_get ;
      private String Ddo_contratotermoaditivo_datafim_Filteredtextto_get ;
      private String Ddo_contratotermoaditivo_dataassinatura_Activeeventkey ;
      private String Ddo_contratotermoaditivo_dataassinatura_Filteredtext_get ;
      private String Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_134_idx="0001" ;
      private String AV43TFContrato_Numero ;
      private String AV44TFContrato_Numero_Sel ;
      private String AV71Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratotermoaditivo_datainicio_Caption ;
      private String Ddo_contratotermoaditivo_datainicio_Tooltip ;
      private String Ddo_contratotermoaditivo_datainicio_Cls ;
      private String Ddo_contratotermoaditivo_datainicio_Filteredtext_set ;
      private String Ddo_contratotermoaditivo_datainicio_Filteredtextto_set ;
      private String Ddo_contratotermoaditivo_datainicio_Dropdownoptionstype ;
      private String Ddo_contratotermoaditivo_datainicio_Titlecontrolidtoreplace ;
      private String Ddo_contratotermoaditivo_datainicio_Sortedstatus ;
      private String Ddo_contratotermoaditivo_datainicio_Filtertype ;
      private String Ddo_contratotermoaditivo_datainicio_Sortasc ;
      private String Ddo_contratotermoaditivo_datainicio_Sortdsc ;
      private String Ddo_contratotermoaditivo_datainicio_Cleanfilter ;
      private String Ddo_contratotermoaditivo_datainicio_Rangefilterfrom ;
      private String Ddo_contratotermoaditivo_datainicio_Rangefilterto ;
      private String Ddo_contratotermoaditivo_datainicio_Searchbuttontext ;
      private String Ddo_contratotermoaditivo_datafim_Caption ;
      private String Ddo_contratotermoaditivo_datafim_Tooltip ;
      private String Ddo_contratotermoaditivo_datafim_Cls ;
      private String Ddo_contratotermoaditivo_datafim_Filteredtext_set ;
      private String Ddo_contratotermoaditivo_datafim_Filteredtextto_set ;
      private String Ddo_contratotermoaditivo_datafim_Dropdownoptionstype ;
      private String Ddo_contratotermoaditivo_datafim_Titlecontrolidtoreplace ;
      private String Ddo_contratotermoaditivo_datafim_Sortedstatus ;
      private String Ddo_contratotermoaditivo_datafim_Filtertype ;
      private String Ddo_contratotermoaditivo_datafim_Sortasc ;
      private String Ddo_contratotermoaditivo_datafim_Sortdsc ;
      private String Ddo_contratotermoaditivo_datafim_Cleanfilter ;
      private String Ddo_contratotermoaditivo_datafim_Rangefilterfrom ;
      private String Ddo_contratotermoaditivo_datafim_Rangefilterto ;
      private String Ddo_contratotermoaditivo_datafim_Searchbuttontext ;
      private String Ddo_contratotermoaditivo_dataassinatura_Caption ;
      private String Ddo_contratotermoaditivo_dataassinatura_Tooltip ;
      private String Ddo_contratotermoaditivo_dataassinatura_Cls ;
      private String Ddo_contratotermoaditivo_dataassinatura_Filteredtext_set ;
      private String Ddo_contratotermoaditivo_dataassinatura_Filteredtextto_set ;
      private String Ddo_contratotermoaditivo_dataassinatura_Dropdownoptionstype ;
      private String Ddo_contratotermoaditivo_dataassinatura_Titlecontrolidtoreplace ;
      private String Ddo_contratotermoaditivo_dataassinatura_Sortedstatus ;
      private String Ddo_contratotermoaditivo_dataassinatura_Filtertype ;
      private String Ddo_contratotermoaditivo_dataassinatura_Sortasc ;
      private String Ddo_contratotermoaditivo_dataassinatura_Sortdsc ;
      private String Ddo_contratotermoaditivo_dataassinatura_Cleanfilter ;
      private String Ddo_contratotermoaditivo_dataassinatura_Rangefilterfrom ;
      private String Ddo_contratotermoaditivo_dataassinatura_Rangefilterto ;
      private String Ddo_contratotermoaditivo_dataassinatura_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtContratoTermoAditivo_Codigo_Internalname ;
      private String edtContratoTermoAditivo_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratotermoaditivo_datainicio_Internalname ;
      private String edtavTfcontratotermoaditivo_datainicio_Jsonclick ;
      private String edtavTfcontratotermoaditivo_datainicio_to_Internalname ;
      private String edtavTfcontratotermoaditivo_datainicio_to_Jsonclick ;
      private String divDdo_contratotermoaditivo_datainicioauxdates_Internalname ;
      private String edtavDdo_contratotermoaditivo_datainicioauxdate_Internalname ;
      private String edtavDdo_contratotermoaditivo_datainicioauxdate_Jsonclick ;
      private String edtavDdo_contratotermoaditivo_datainicioauxdateto_Internalname ;
      private String edtavDdo_contratotermoaditivo_datainicioauxdateto_Jsonclick ;
      private String edtavTfcontratotermoaditivo_datafim_Internalname ;
      private String edtavTfcontratotermoaditivo_datafim_Jsonclick ;
      private String edtavTfcontratotermoaditivo_datafim_to_Internalname ;
      private String edtavTfcontratotermoaditivo_datafim_to_Jsonclick ;
      private String divDdo_contratotermoaditivo_datafimauxdates_Internalname ;
      private String edtavDdo_contratotermoaditivo_datafimauxdate_Internalname ;
      private String edtavDdo_contratotermoaditivo_datafimauxdate_Jsonclick ;
      private String edtavDdo_contratotermoaditivo_datafimauxdateto_Internalname ;
      private String edtavDdo_contratotermoaditivo_datafimauxdateto_Jsonclick ;
      private String edtavTfcontratotermoaditivo_dataassinatura_Internalname ;
      private String edtavTfcontratotermoaditivo_dataassinatura_Jsonclick ;
      private String edtavTfcontratotermoaditivo_dataassinatura_to_Internalname ;
      private String edtavTfcontratotermoaditivo_dataassinatura_to_Jsonclick ;
      private String divDdo_contratotermoaditivo_dataassinaturaauxdates_Internalname ;
      private String edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Internalname ;
      private String edtavDdo_contratotermoaditivo_dataassinaturaauxdate_Jsonclick ;
      private String edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Internalname ;
      private String edtavDdo_contratotermoaditivo_dataassinaturaauxdateto_Jsonclick ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratotermoaditivo_datainiciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratotermoaditivo_datafimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratotermoaditivo_dataassinaturatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratoTermoAditivo_DataInicio_Internalname ;
      private String edtContratoTermoAditivo_DataFim_Internalname ;
      private String edtContratoTermoAditivo_DataAssinatura_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV43TFContrato_Numero ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratotermoaditivo_datainicio1_Internalname ;
      private String edtavContratotermoaditivo_datainicio_to1_Internalname ;
      private String edtavContratotermoaditivo_datafim1_Internalname ;
      private String edtavContratotermoaditivo_datafim_to1_Internalname ;
      private String edtavContratotermoaditivo_dataassinatura1_Internalname ;
      private String edtavContratotermoaditivo_dataassinatura_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContratotermoaditivo_datainicio2_Internalname ;
      private String edtavContratotermoaditivo_datainicio_to2_Internalname ;
      private String edtavContratotermoaditivo_datafim2_Internalname ;
      private String edtavContratotermoaditivo_datafim_to2_Internalname ;
      private String edtavContratotermoaditivo_dataassinatura2_Internalname ;
      private String edtavContratotermoaditivo_dataassinatura_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContratotermoaditivo_datainicio3_Internalname ;
      private String edtavContratotermoaditivo_datainicio_to3_Internalname ;
      private String edtavContratotermoaditivo_datafim3_Internalname ;
      private String edtavContratotermoaditivo_datafim_to3_Internalname ;
      private String edtavContratotermoaditivo_dataassinatura3_Internalname ;
      private String edtavContratotermoaditivo_dataassinatura_to3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratotermoaditivo_datainicio_Internalname ;
      private String Ddo_contratotermoaditivo_datafim_Internalname ;
      private String Ddo_contratotermoaditivo_dataassinatura_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratoTermoAditivo_DataInicio_Title ;
      private String edtContratoTermoAditivo_DataFim_Title ;
      private String edtContratoTermoAditivo_DataAssinatura_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_datainicio1_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_datafim1_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura1_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_datainicio2_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_datafim2_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura2_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_datainicio3_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_datafim3_Internalname ;
      private String tblTablemergeddynamicfilterscontratotermoaditivo_dataassinatura3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContratotermoaditivo_dataassinatura3_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext3_Jsonclick ;
      private String edtavContratotermoaditivo_dataassinatura_to3_Jsonclick ;
      private String edtavContratotermoaditivo_datafim3_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext3_Jsonclick ;
      private String edtavContratotermoaditivo_datafim_to3_Jsonclick ;
      private String edtavContratotermoaditivo_datainicio3_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext3_Jsonclick ;
      private String edtavContratotermoaditivo_datainicio_to3_Jsonclick ;
      private String edtavContratotermoaditivo_dataassinatura2_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext2_Jsonclick ;
      private String edtavContratotermoaditivo_dataassinatura_to2_Jsonclick ;
      private String edtavContratotermoaditivo_datafim2_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext2_Jsonclick ;
      private String edtavContratotermoaditivo_datafim_to2_Jsonclick ;
      private String edtavContratotermoaditivo_datainicio2_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext2_Jsonclick ;
      private String edtavContratotermoaditivo_datainicio_to2_Jsonclick ;
      private String edtavContratotermoaditivo_dataassinatura1_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_dataassinatura_rangemiddletext1_Jsonclick ;
      private String edtavContratotermoaditivo_dataassinatura_to1_Jsonclick ;
      private String edtavContratotermoaditivo_datafim1_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_datafim_rangemiddletext1_Jsonclick ;
      private String edtavContratotermoaditivo_datafim_to1_Jsonclick ;
      private String edtavContratotermoaditivo_datainicio1_Jsonclick ;
      private String lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratotermoaditivo_datainicio_rangemiddletext1_Jsonclick ;
      private String edtavContratotermoaditivo_datainicio_to1_Jsonclick ;
      private String sGXsfl_134_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratoTermoAditivo_DataInicio_Jsonclick ;
      private String edtContratoTermoAditivo_DataFim_Jsonclick ;
      private String edtContratoTermoAditivo_DataAssinatura_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutContratoTermoAditivo_DataInicio ;
      private DateTime wcpOAV8InOutContratoTermoAditivo_DataInicio ;
      private DateTime AV16ContratoTermoAditivo_DataInicio1 ;
      private DateTime AV17ContratoTermoAditivo_DataInicio_To1 ;
      private DateTime AV30ContratoTermoAditivo_DataFim1 ;
      private DateTime AV31ContratoTermoAditivo_DataFim_To1 ;
      private DateTime AV32ContratoTermoAditivo_DataAssinatura1 ;
      private DateTime AV33ContratoTermoAditivo_DataAssinatura_To1 ;
      private DateTime AV20ContratoTermoAditivo_DataInicio2 ;
      private DateTime AV21ContratoTermoAditivo_DataInicio_To2 ;
      private DateTime AV34ContratoTermoAditivo_DataFim2 ;
      private DateTime AV35ContratoTermoAditivo_DataFim_To2 ;
      private DateTime AV36ContratoTermoAditivo_DataAssinatura2 ;
      private DateTime AV37ContratoTermoAditivo_DataAssinatura_To2 ;
      private DateTime AV24ContratoTermoAditivo_DataInicio3 ;
      private DateTime AV25ContratoTermoAditivo_DataInicio_To3 ;
      private DateTime AV38ContratoTermoAditivo_DataFim3 ;
      private DateTime AV39ContratoTermoAditivo_DataFim_To3 ;
      private DateTime AV40ContratoTermoAditivo_DataAssinatura3 ;
      private DateTime AV41ContratoTermoAditivo_DataAssinatura_To3 ;
      private DateTime AV47TFContratoTermoAditivo_DataInicio ;
      private DateTime AV48TFContratoTermoAditivo_DataInicio_To ;
      private DateTime AV53TFContratoTermoAditivo_DataFim ;
      private DateTime AV54TFContratoTermoAditivo_DataFim_To ;
      private DateTime AV59TFContratoTermoAditivo_DataAssinatura ;
      private DateTime AV60TFContratoTermoAditivo_DataAssinatura_To ;
      private DateTime AV49DDO_ContratoTermoAditivo_DataInicioAuxDate ;
      private DateTime AV50DDO_ContratoTermoAditivo_DataInicioAuxDateTo ;
      private DateTime AV55DDO_ContratoTermoAditivo_DataFimAuxDate ;
      private DateTime AV56DDO_ContratoTermoAditivo_DataFimAuxDateTo ;
      private DateTime AV61DDO_ContratoTermoAditivo_DataAssinaturaAuxDate ;
      private DateTime AV62DDO_ContratoTermoAditivo_DataAssinaturaAuxDateTo ;
      private DateTime A316ContratoTermoAditivo_DataInicio ;
      private DateTime A317ContratoTermoAditivo_DataFim ;
      private DateTime A318ContratoTermoAditivo_DataAssinatura ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratotermoaditivo_datainicio_Includesortasc ;
      private bool Ddo_contratotermoaditivo_datainicio_Includesortdsc ;
      private bool Ddo_contratotermoaditivo_datainicio_Includefilter ;
      private bool Ddo_contratotermoaditivo_datainicio_Filterisrange ;
      private bool Ddo_contratotermoaditivo_datainicio_Includedatalist ;
      private bool Ddo_contratotermoaditivo_datafim_Includesortasc ;
      private bool Ddo_contratotermoaditivo_datafim_Includesortdsc ;
      private bool Ddo_contratotermoaditivo_datafim_Includefilter ;
      private bool Ddo_contratotermoaditivo_datafim_Filterisrange ;
      private bool Ddo_contratotermoaditivo_datafim_Includedatalist ;
      private bool Ddo_contratotermoaditivo_dataassinatura_Includesortasc ;
      private bool Ddo_contratotermoaditivo_dataassinatura_Includesortdsc ;
      private bool Ddo_contratotermoaditivo_dataassinatura_Includefilter ;
      private bool Ddo_contratotermoaditivo_dataassinatura_Filterisrange ;
      private bool Ddo_contratotermoaditivo_dataassinatura_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n316ContratoTermoAditivo_DataInicio ;
      private bool n317ContratoTermoAditivo_DataFim ;
      private bool n318ContratoTermoAditivo_DataAssinatura ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV45ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV51ddo_ContratoTermoAditivo_DataInicioTitleControlIdToReplace ;
      private String AV57ddo_ContratoTermoAditivo_DataFimTitleControlIdToReplace ;
      private String AV63ddo_ContratoTermoAditivo_DataAssinaturaTitleControlIdToReplace ;
      private String AV70Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoTermoAditivo_Codigo ;
      private DateTime aP1_InOutContratoTermoAditivo_DataInicio ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H007L2_A74Contrato_Codigo ;
      private int[] H007L2_A315ContratoTermoAditivo_Codigo ;
      private DateTime[] H007L2_A318ContratoTermoAditivo_DataAssinatura ;
      private bool[] H007L2_n318ContratoTermoAditivo_DataAssinatura ;
      private DateTime[] H007L2_A317ContratoTermoAditivo_DataFim ;
      private bool[] H007L2_n317ContratoTermoAditivo_DataFim ;
      private DateTime[] H007L2_A316ContratoTermoAditivo_DataInicio ;
      private bool[] H007L2_n316ContratoTermoAditivo_DataInicio ;
      private String[] H007L2_A77Contrato_Numero ;
      private long[] H007L3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46ContratoTermoAditivo_DataInicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ContratoTermoAditivo_DataFimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ContratoTermoAditivo_DataAssinaturaTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV64DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratotermoaditivo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H007L2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContratoTermoAditivo_DataInicio1 ,
                                             DateTime AV17ContratoTermoAditivo_DataInicio_To1 ,
                                             DateTime AV30ContratoTermoAditivo_DataFim1 ,
                                             DateTime AV31ContratoTermoAditivo_DataFim_To1 ,
                                             DateTime AV32ContratoTermoAditivo_DataAssinatura1 ,
                                             DateTime AV33ContratoTermoAditivo_DataAssinatura_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContratoTermoAditivo_DataInicio2 ,
                                             DateTime AV21ContratoTermoAditivo_DataInicio_To2 ,
                                             DateTime AV34ContratoTermoAditivo_DataFim2 ,
                                             DateTime AV35ContratoTermoAditivo_DataFim_To2 ,
                                             DateTime AV36ContratoTermoAditivo_DataAssinatura2 ,
                                             DateTime AV37ContratoTermoAditivo_DataAssinatura_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24ContratoTermoAditivo_DataInicio3 ,
                                             DateTime AV25ContratoTermoAditivo_DataInicio_To3 ,
                                             DateTime AV38ContratoTermoAditivo_DataFim3 ,
                                             DateTime AV39ContratoTermoAditivo_DataFim_To3 ,
                                             DateTime AV40ContratoTermoAditivo_DataAssinatura3 ,
                                             DateTime AV41ContratoTermoAditivo_DataAssinatura_To3 ,
                                             String AV44TFContrato_Numero_Sel ,
                                             String AV43TFContrato_Numero ,
                                             DateTime AV47TFContratoTermoAditivo_DataInicio ,
                                             DateTime AV48TFContratoTermoAditivo_DataInicio_To ,
                                             DateTime AV53TFContratoTermoAditivo_DataFim ,
                                             DateTime AV54TFContratoTermoAditivo_DataFim_To ,
                                             DateTime AV59TFContratoTermoAditivo_DataAssinatura ,
                                             DateTime AV60TFContratoTermoAditivo_DataAssinatura_To ,
                                             DateTime A316ContratoTermoAditivo_DataInicio ,
                                             DateTime A317ContratoTermoAditivo_DataFim ,
                                             DateTime A318ContratoTermoAditivo_DataAssinatura ,
                                             String A77Contrato_Numero ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [31] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contrato_Codigo], T1.[ContratoTermoAditivo_Codigo], T1.[ContratoTermoAditivo_DataAssinatura], T1.[ContratoTermoAditivo_DataFim], T1.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Numero]";
         sFromString = " FROM ([ContratoTermoAditivo] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV16ContratoTermoAditivo_DataInicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV16ContratoTermoAditivo_DataInicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV16ContratoTermoAditivo_DataInicio1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoTermoAditivo_DataInicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV17ContratoTermoAditivo_DataInicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV17ContratoTermoAditivo_DataInicio_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV30ContratoTermoAditivo_DataFim1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV30ContratoTermoAditivo_DataFim1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV30ContratoTermoAditivo_DataFim1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV31ContratoTermoAditivo_DataFim_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV31ContratoTermoAditivo_DataFim_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV31ContratoTermoAditivo_DataFim_To1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV32ContratoTermoAditivo_DataAssinatura1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV32ContratoTermoAditivo_DataAssinatura1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV32ContratoTermoAditivo_DataAssinatura1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV33ContratoTermoAditivo_DataAssinatura_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV33ContratoTermoAditivo_DataAssinatura_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV33ContratoTermoAditivo_DataAssinatura_To1)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV20ContratoTermoAditivo_DataInicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV20ContratoTermoAditivo_DataInicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV20ContratoTermoAditivo_DataInicio2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV21ContratoTermoAditivo_DataInicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV21ContratoTermoAditivo_DataInicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV21ContratoTermoAditivo_DataInicio_To2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV34ContratoTermoAditivo_DataFim2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV34ContratoTermoAditivo_DataFim2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV34ContratoTermoAditivo_DataFim2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV35ContratoTermoAditivo_DataFim_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV35ContratoTermoAditivo_DataFim_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV35ContratoTermoAditivo_DataFim_To2)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV36ContratoTermoAditivo_DataAssinatura2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV36ContratoTermoAditivo_DataAssinatura2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV36ContratoTermoAditivo_DataAssinatura2)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV37ContratoTermoAditivo_DataAssinatura_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV37ContratoTermoAditivo_DataAssinatura_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV37ContratoTermoAditivo_DataAssinatura_To2)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoTermoAditivo_DataInicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV24ContratoTermoAditivo_DataInicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV24ContratoTermoAditivo_DataInicio3)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV25ContratoTermoAditivo_DataInicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV25ContratoTermoAditivo_DataInicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV25ContratoTermoAditivo_DataInicio_To3)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV38ContratoTermoAditivo_DataFim3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV38ContratoTermoAditivo_DataFim3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV38ContratoTermoAditivo_DataFim3)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV39ContratoTermoAditivo_DataFim_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV39ContratoTermoAditivo_DataFim_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV39ContratoTermoAditivo_DataFim_To3)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV40ContratoTermoAditivo_DataAssinatura3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV40ContratoTermoAditivo_DataAssinatura3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV40ContratoTermoAditivo_DataAssinatura3)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV41ContratoTermoAditivo_DataAssinatura_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV41ContratoTermoAditivo_DataAssinatura_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV41ContratoTermoAditivo_DataAssinatura_To3)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV43TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV43TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV44TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV44TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV47TFContratoTermoAditivo_DataInicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV47TFContratoTermoAditivo_DataInicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV47TFContratoTermoAditivo_DataInicio)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV48TFContratoTermoAditivo_DataInicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV48TFContratoTermoAditivo_DataInicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV48TFContratoTermoAditivo_DataInicio_To)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContratoTermoAditivo_DataFim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV53TFContratoTermoAditivo_DataFim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV53TFContratoTermoAditivo_DataFim)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV54TFContratoTermoAditivo_DataFim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV54TFContratoTermoAditivo_DataFim_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV54TFContratoTermoAditivo_DataFim_To)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV59TFContratoTermoAditivo_DataAssinatura) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV59TFContratoTermoAditivo_DataAssinatura)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV59TFContratoTermoAditivo_DataAssinatura)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV60TFContratoTermoAditivo_DataAssinatura_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV60TFContratoTermoAditivo_DataAssinatura_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV60TFContratoTermoAditivo_DataAssinatura_To)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoTermoAditivo_DataInicio]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoTermoAditivo_DataInicio] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoTermoAditivo_DataFim]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoTermoAditivo_DataFim] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoTermoAditivo_DataAssinatura]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoTermoAditivo_DataAssinatura] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoTermoAditivo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H007L3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContratoTermoAditivo_DataInicio1 ,
                                             DateTime AV17ContratoTermoAditivo_DataInicio_To1 ,
                                             DateTime AV30ContratoTermoAditivo_DataFim1 ,
                                             DateTime AV31ContratoTermoAditivo_DataFim_To1 ,
                                             DateTime AV32ContratoTermoAditivo_DataAssinatura1 ,
                                             DateTime AV33ContratoTermoAditivo_DataAssinatura_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContratoTermoAditivo_DataInicio2 ,
                                             DateTime AV21ContratoTermoAditivo_DataInicio_To2 ,
                                             DateTime AV34ContratoTermoAditivo_DataFim2 ,
                                             DateTime AV35ContratoTermoAditivo_DataFim_To2 ,
                                             DateTime AV36ContratoTermoAditivo_DataAssinatura2 ,
                                             DateTime AV37ContratoTermoAditivo_DataAssinatura_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24ContratoTermoAditivo_DataInicio3 ,
                                             DateTime AV25ContratoTermoAditivo_DataInicio_To3 ,
                                             DateTime AV38ContratoTermoAditivo_DataFim3 ,
                                             DateTime AV39ContratoTermoAditivo_DataFim_To3 ,
                                             DateTime AV40ContratoTermoAditivo_DataAssinatura3 ,
                                             DateTime AV41ContratoTermoAditivo_DataAssinatura_To3 ,
                                             String AV44TFContrato_Numero_Sel ,
                                             String AV43TFContrato_Numero ,
                                             DateTime AV47TFContratoTermoAditivo_DataInicio ,
                                             DateTime AV48TFContratoTermoAditivo_DataInicio_To ,
                                             DateTime AV53TFContratoTermoAditivo_DataFim ,
                                             DateTime AV54TFContratoTermoAditivo_DataFim_To ,
                                             DateTime AV59TFContratoTermoAditivo_DataAssinatura ,
                                             DateTime AV60TFContratoTermoAditivo_DataAssinatura_To ,
                                             DateTime A316ContratoTermoAditivo_DataInicio ,
                                             DateTime A317ContratoTermoAditivo_DataFim ,
                                             DateTime A318ContratoTermoAditivo_DataAssinatura ,
                                             String A77Contrato_Numero ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [26] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoTermoAditivo] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV16ContratoTermoAditivo_DataInicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV16ContratoTermoAditivo_DataInicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV16ContratoTermoAditivo_DataInicio1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoTermoAditivo_DataInicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV17ContratoTermoAditivo_DataInicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV17ContratoTermoAditivo_DataInicio_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV30ContratoTermoAditivo_DataFim1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV30ContratoTermoAditivo_DataFim1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV30ContratoTermoAditivo_DataFim1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV31ContratoTermoAditivo_DataFim_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV31ContratoTermoAditivo_DataFim_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV31ContratoTermoAditivo_DataFim_To1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV32ContratoTermoAditivo_DataAssinatura1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV32ContratoTermoAditivo_DataAssinatura1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV32ContratoTermoAditivo_DataAssinatura1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV33ContratoTermoAditivo_DataAssinatura_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV33ContratoTermoAditivo_DataAssinatura_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV33ContratoTermoAditivo_DataAssinatura_To1)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV20ContratoTermoAditivo_DataInicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV20ContratoTermoAditivo_DataInicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV20ContratoTermoAditivo_DataInicio2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV21ContratoTermoAditivo_DataInicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV21ContratoTermoAditivo_DataInicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV21ContratoTermoAditivo_DataInicio_To2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV34ContratoTermoAditivo_DataFim2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV34ContratoTermoAditivo_DataFim2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV34ContratoTermoAditivo_DataFim2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV35ContratoTermoAditivo_DataFim_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV35ContratoTermoAditivo_DataFim_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV35ContratoTermoAditivo_DataFim_To2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV36ContratoTermoAditivo_DataAssinatura2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV36ContratoTermoAditivo_DataAssinatura2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV36ContratoTermoAditivo_DataAssinatura2)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV37ContratoTermoAditivo_DataAssinatura_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV37ContratoTermoAditivo_DataAssinatura_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV37ContratoTermoAditivo_DataAssinatura_To2)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoTermoAditivo_DataInicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV24ContratoTermoAditivo_DataInicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV24ContratoTermoAditivo_DataInicio3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV25ContratoTermoAditivo_DataInicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV25ContratoTermoAditivo_DataInicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV25ContratoTermoAditivo_DataInicio_To3)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV38ContratoTermoAditivo_DataFim3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV38ContratoTermoAditivo_DataFim3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV38ContratoTermoAditivo_DataFim3)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV39ContratoTermoAditivo_DataFim_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV39ContratoTermoAditivo_DataFim_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV39ContratoTermoAditivo_DataFim_To3)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV40ContratoTermoAditivo_DataAssinatura3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV40ContratoTermoAditivo_DataAssinatura3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV40ContratoTermoAditivo_DataAssinatura3)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV41ContratoTermoAditivo_DataAssinatura_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV41ContratoTermoAditivo_DataAssinatura_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV41ContratoTermoAditivo_DataAssinatura_To3)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV43TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV43TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV44TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV44TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV47TFContratoTermoAditivo_DataInicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV47TFContratoTermoAditivo_DataInicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV47TFContratoTermoAditivo_DataInicio)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV48TFContratoTermoAditivo_DataInicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV48TFContratoTermoAditivo_DataInicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV48TFContratoTermoAditivo_DataInicio_To)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContratoTermoAditivo_DataFim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV53TFContratoTermoAditivo_DataFim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV53TFContratoTermoAditivo_DataFim)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV54TFContratoTermoAditivo_DataFim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV54TFContratoTermoAditivo_DataFim_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV54TFContratoTermoAditivo_DataFim_To)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV59TFContratoTermoAditivo_DataAssinatura) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV59TFContratoTermoAditivo_DataAssinatura)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV59TFContratoTermoAditivo_DataAssinatura)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV60TFContratoTermoAditivo_DataAssinatura_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV60TFContratoTermoAditivo_DataAssinatura_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV60TFContratoTermoAditivo_DataAssinatura_To)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H007L2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
               case 1 :
                     return conditional_H007L3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007L2 ;
          prmH007L2 = new Object[] {
          new Object[] {"@AV16ContratoTermoAditivo_DataInicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17ContratoTermoAditivo_DataInicio_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30ContratoTermoAditivo_DataFim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV31ContratoTermoAditivo_DataFim_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32ContratoTermoAditivo_DataAssinatura1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33ContratoTermoAditivo_DataAssinatura_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20ContratoTermoAditivo_DataInicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21ContratoTermoAditivo_DataInicio_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34ContratoTermoAditivo_DataFim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35ContratoTermoAditivo_DataFim_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV36ContratoTermoAditivo_DataAssinatura2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37ContratoTermoAditivo_DataAssinatura_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoTermoAditivo_DataInicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25ContratoTermoAditivo_DataInicio_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV38ContratoTermoAditivo_DataFim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV39ContratoTermoAditivo_DataFim_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40ContratoTermoAditivo_DataAssinatura3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV41ContratoTermoAditivo_DataAssinatura_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV43TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV44TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV47TFContratoTermoAditivo_DataInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48TFContratoTermoAditivo_DataInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53TFContratoTermoAditivo_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFContratoTermoAditivo_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59TFContratoTermoAditivo_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60TFContratoTermoAditivo_DataAssinatura_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH007L3 ;
          prmH007L3 = new Object[] {
          new Object[] {"@AV16ContratoTermoAditivo_DataInicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17ContratoTermoAditivo_DataInicio_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30ContratoTermoAditivo_DataFim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV31ContratoTermoAditivo_DataFim_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV32ContratoTermoAditivo_DataAssinatura1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33ContratoTermoAditivo_DataAssinatura_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20ContratoTermoAditivo_DataInicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21ContratoTermoAditivo_DataInicio_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34ContratoTermoAditivo_DataFim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV35ContratoTermoAditivo_DataFim_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV36ContratoTermoAditivo_DataAssinatura2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV37ContratoTermoAditivo_DataAssinatura_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoTermoAditivo_DataInicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25ContratoTermoAditivo_DataInicio_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV38ContratoTermoAditivo_DataFim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV39ContratoTermoAditivo_DataFim_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40ContratoTermoAditivo_DataAssinatura3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV41ContratoTermoAditivo_DataAssinatura_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV43TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV44TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV47TFContratoTermoAditivo_DataInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48TFContratoTermoAditivo_DataInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53TFContratoTermoAditivo_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFContratoTermoAditivo_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59TFContratoTermoAditivo_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60TFContratoTermoAditivo_DataAssinatura_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007L2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007L2,11,0,true,false )
             ,new CursorDef("H007L3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007L3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 20) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                return;
       }
    }

 }

}
