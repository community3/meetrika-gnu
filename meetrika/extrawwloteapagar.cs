/*
               File: ExtraWWLoteAPagar
        Description:  Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:45:44.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class extrawwloteapagar : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public extrawwloteapagar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public extrawwloteapagar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavLote_status = new GXCombobox();
         dynavLote_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbLote_Status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vLOTE_AREATRABALHOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvLOTE_AREATRABALHOCODF52( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_140 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_140_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_140_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)));
               AV17DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
               AV50Lote_PrevPagamento1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Lote_PrevPagamento1", context.localUtil.Format(AV50Lote_PrevPagamento1, "99/99/99"));
               AV51Lote_PrevPagamento_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Lote_PrevPagamento_To1", context.localUtil.Format(AV51Lote_PrevPagamento_To1, "99/99/99"));
               AV18Lote_Numero1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_Numero1", AV18Lote_Numero1);
               AV19Lote_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Lote_Nome1", AV19Lote_Nome1);
               AV62Lote_NFe1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Lote_NFe1), 6, 0)));
               AV63Lote_DataNfe1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Lote_DataNfe1", context.localUtil.Format(AV63Lote_DataNfe1, "99/99/99"));
               AV64Lote_DataNfe_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Lote_DataNfe_To1", context.localUtil.Format(AV64Lote_DataNfe_To1, "99/99/99"));
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV52Lote_PrevPagamento2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Lote_PrevPagamento2", context.localUtil.Format(AV52Lote_PrevPagamento2, "99/99/99"));
               AV53Lote_PrevPagamento_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_PrevPagamento_To2", context.localUtil.Format(AV53Lote_PrevPagamento_To2, "99/99/99"));
               AV22Lote_Numero2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Numero2", AV22Lote_Numero2);
               AV23Lote_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_Nome2", AV23Lote_Nome2);
               AV65Lote_NFe2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Lote_NFe2), 6, 0)));
               AV66Lote_DataNfe2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Lote_DataNfe2", context.localUtil.Format(AV66Lote_DataNfe2, "99/99/99"));
               AV67Lote_DataNfe_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Lote_DataNfe_To2", context.localUtil.Format(AV67Lote_DataNfe_To2, "99/99/99"));
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV54Lote_PrevPagamento3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Lote_PrevPagamento3", context.localUtil.Format(AV54Lote_PrevPagamento3, "99/99/99"));
               AV55Lote_PrevPagamento_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Lote_PrevPagamento_To3", context.localUtil.Format(AV55Lote_PrevPagamento_To3, "99/99/99"));
               AV26Lote_Numero3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Lote_Numero3", AV26Lote_Numero3);
               AV27Lote_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
               AV68Lote_NFe3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Lote_NFe3), 6, 0)));
               AV69Lote_DataNfe3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Lote_DataNfe3", context.localUtil.Format(AV69Lote_DataNfe3, "99/99/99"));
               AV70Lote_DataNfe_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Lote_DataNfe_To3", context.localUtil.Format(AV70Lote_DataNfe_To3, "99/99/99"));
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV16Lote_Status = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_Status", AV16Lote_Status);
               AV73Lote_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73Lote_ContratadaCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV163Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV29DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
               AV28DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
               A596Lote_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A575Lote_Status = GetNextPar( );
               AV33Quantidade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Quantidade), 3, 0)));
               AV34QtdDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdDmn), 3, 0)));
               A569Lote_QtdeDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV35QtdPF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdPF", StringUtil.LTrim( StringUtil.Str( AV35QtdPF, 14, 5)));
               A573Lote_QtdePF = NumberUtil.Val( GetNextPar( ), ".");
               AV36ValorTotal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ValorTotal", StringUtil.LTrim( StringUtil.Str( AV36ValorTotal, 18, 5)));
               A572Lote_Valor = NumberUtil.Val( GetNextPar( ), ".");
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void GetLote_QtdePF( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A573Lote_QtdePF = 0;
         A1058Lote_ValorOSs = 0;
         /* Using cursor H00F52 */
         pr_default.execute(0, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) && ( H00F52_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  H00F52_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*H00F52_A512ContagemResultado_ValorPF[0]);
            A573Lote_QtdePF = (decimal)(A573Lote_QtdePF+A574ContagemResultado_PFFinal);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1058Lote_ValorOSs", StringUtil.LTrim( StringUtil.Str( A1058Lote_ValorOSs, 18, 5)));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      public override short ExecuteStartEvent( )
      {
         PAF52( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTF52( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423454453");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("extrawwloteapagar.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV17DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO1", context.localUtil.Format(AV50Lote_PrevPagamento1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO_TO1", context.localUtil.Format(AV51Lote_PrevPagamento_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NUMERO1", StringUtil.RTrim( AV18Lote_Numero1));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NOME1", StringUtil.RTrim( AV19Lote_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFE1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62Lote_NFe1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE1", context.localUtil.Format(AV63Lote_DataNfe1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE_TO1", context.localUtil.Format(AV64Lote_DataNfe_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO2", context.localUtil.Format(AV52Lote_PrevPagamento2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO_TO2", context.localUtil.Format(AV53Lote_PrevPagamento_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NUMERO2", StringUtil.RTrim( AV22Lote_Numero2));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NOME2", StringUtil.RTrim( AV23Lote_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFE2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65Lote_NFe2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE2", context.localUtil.Format(AV66Lote_DataNfe2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE_TO2", context.localUtil.Format(AV67Lote_DataNfe_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO3", context.localUtil.Format(AV54Lote_PrevPagamento3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_PREVPAGAMENTO_TO3", context.localUtil.Format(AV55Lote_PrevPagamento_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NUMERO3", StringUtil.RTrim( AV26Lote_Numero3));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NOME3", StringUtil.RTrim( AV27Lote_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NFE3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68Lote_NFe3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE3", context.localUtil.Format(AV69Lote_DataNfe3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_DATANFE_TO3", context.localUtil.Format(AV70Lote_DataNfe_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_140", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_140), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV128GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV129GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV163Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV29DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV28DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_FILTROCONSCONTADORFM", AV71SDT_FiltroConsContadorFM);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_FILTROCONSCONTADORFM", AV71SDT_FiltroConsContadorFM);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALOROSS", StringUtil.LTrim( StringUtil.NToC( A1058Lote_ValorOSs, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALORGLOSAS", StringUtil.LTrim( StringUtil.NToC( A1057Lote_ValorGlosas, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW1_Target", StringUtil.RTrim( Innewwindow1_Target));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEF52( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTF52( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("extrawwloteapagar.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ExtraWWLoteAPagar" ;
      }

      public override String GetPgmdesc( )
      {
         return " Lote" ;
      }

      protected void WBF50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_F52( true) ;
         }
         else
         {
            wb_table1_2_F52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'',false,'" + sGXsfl_140_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(184, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,184);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 185,'',false,'" + sGXsfl_140_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(185, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,185);\"");
         }
         wbLoad = true;
      }

      protected void STARTF52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Lote", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPF50( ) ;
      }

      protected void WSF52( )
      {
         STARTF52( ) ;
         EVTF52( ) ;
      }

      protected void EVTF52( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11F52 */
                              E11F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12F52 */
                              E12F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13F52 */
                              E13F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14F52 */
                              E14F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15F52 */
                              E15F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16F52 */
                              E16F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17F52 */
                              E17F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18F52 */
                              E18F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19F52 */
                              E19F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20F52 */
                              E20F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21F52 */
                              E21F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22F52 */
                              E22F52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOACTION'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "LOTE_QTDEDMN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOACTION'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "LOTE_QTDEDMN.CLICK") == 0 ) )
                           {
                              nGXsfl_140_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_140_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_140_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1402( ) ;
                              A596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( edtLote_Codigo_Internalname), ",", "."));
                              A562Lote_Numero = cgiGet( edtLote_Numero_Internalname);
                              A563Lote_Nome = StringUtil.Upper( cgiGet( edtLote_Nome_Internalname));
                              A564Lote_Data = context.localUtil.CToT( cgiGet( edtLote_Data_Internalname), 0);
                              A673Lote_NFe = (int)(context.localUtil.CToN( cgiGet( edtLote_NFe_Internalname), ",", "."));
                              n673Lote_NFe = false;
                              A674Lote_DataNfe = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataNfe_Internalname), 0));
                              n674Lote_DataNfe = false;
                              A857Lote_PrevPagamento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_PrevPagamento_Internalname), 0));
                              n857Lote_PrevPagamento = false;
                              AV61Action = cgiGet( edtavAction_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAction_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV61Action)) ? AV162Action_GXI : context.convertURL( context.PathToRelativeUrl( AV61Action))));
                              A565Lote_ValorPF = context.localUtil.CToN( cgiGet( edtLote_ValorPF_Internalname), ",", ".");
                              A569Lote_QtdeDmn = (short)(context.localUtil.CToN( cgiGet( edtLote_QtdeDmn_Internalname), ",", "."));
                              A573Lote_QtdePF = context.localUtil.CToN( cgiGet( edtLote_QtdePF_Internalname), ",", ".");
                              cmbLote_Status.Name = cmbLote_Status_Internalname;
                              cmbLote_Status.CurrentValue = cgiGet( cmbLote_Status_Internalname);
                              A575Lote_Status = cgiGet( cmbLote_Status_Internalname);
                              A572Lote_Valor = context.localUtil.CToN( cgiGet( edtLote_Valor_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23F52 */
                                    E23F52 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24F52 */
                                    E24F52 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOACTION'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25F52 */
                                    E25F52 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOTE_QTDEDMN.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26F52 */
                                    E26F52 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_areatrabalhocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV15Lote_AreaTrabalhoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO1"), 0) != AV50Lote_PrevPagamento1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO1"), 0) != AV51Lote_PrevPagamento_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_numero1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO1"), AV18Lote_Numero1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME1"), AV19Lote_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfe1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE1"), ",", ".") != Convert.ToDecimal( AV62Lote_NFe1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE1"), 0) != AV63Lote_DataNfe1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO1"), 0) != AV64Lote_DataNfe_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO2"), 0) != AV52Lote_PrevPagamento2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO2"), 0) != AV53Lote_PrevPagamento_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_numero2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO2"), AV22Lote_Numero2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME2"), AV23Lote_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfe2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE2"), ",", ".") != Convert.ToDecimal( AV65Lote_NFe2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE2"), 0) != AV66Lote_DataNfe2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO2"), 0) != AV67Lote_DataNfe_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO3"), 0) != AV54Lote_PrevPagamento3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_prevpagamento_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO3"), 0) != AV55Lote_PrevPagamento_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_numero3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO3"), AV26Lote_Numero3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME3"), AV27Lote_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_nfe3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE3"), ",", ".") != Convert.ToDecimal( AV68Lote_NFe3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE3"), 0) != AV69Lote_DataNfe3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lote_datanfe_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO3"), 0) != AV70Lote_DataNfe_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEF52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAF52( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavLote_status.Name = "vLOTE_STATUS";
            cmbavLote_status.WebTags = "";
            cmbavLote_status.addItem("", "Todos", 0);
            cmbavLote_status.addItem("B", "Stand by", 0);
            cmbavLote_status.addItem("S", "Solicitada", 0);
            cmbavLote_status.addItem("E", "Em An�lise", 0);
            cmbavLote_status.addItem("A", "Em execu��o", 0);
            cmbavLote_status.addItem("R", "Resolvida", 0);
            cmbavLote_status.addItem("C", "Conferida", 0);
            cmbavLote_status.addItem("D", "Rejeitada", 0);
            cmbavLote_status.addItem("H", "Homologada", 0);
            cmbavLote_status.addItem("O", "Aceite", 0);
            cmbavLote_status.addItem("P", "A Pagar", 0);
            cmbavLote_status.addItem("L", "Liquidada", 0);
            cmbavLote_status.addItem("X", "Cancelada", 0);
            cmbavLote_status.addItem("N", "N�o Faturada", 0);
            cmbavLote_status.addItem("J", "Planejamento", 0);
            cmbavLote_status.addItem("I", "An�lise Planejamento", 0);
            cmbavLote_status.addItem("T", "Validacao T�cnica", 0);
            cmbavLote_status.addItem("Q", "Validacao Qualidade", 0);
            cmbavLote_status.addItem("G", "Em Homologa��o", 0);
            cmbavLote_status.addItem("M", "Valida��o Mensura��o", 0);
            cmbavLote_status.addItem("U", "Rascunho", 0);
            if ( cmbavLote_status.ItemCount > 0 )
            {
               AV16Lote_Status = cmbavLote_status.getValidValue(AV16Lote_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_Status", AV16Lote_Status);
            }
            dynavLote_areatrabalhocod.Name = "vLOTE_AREATRABALHOCOD";
            dynavLote_areatrabalhocod.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("LOTE_PREVPAGAMENTO", "Pagamento", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_NUMERO", "N�mero", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_NFE", "NFe", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_DATANFE", "Data Nfe", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("LOTE_PREVPAGAMENTO", "Pagamento", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_NUMERO", "N�mero", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_NFE", "NFe", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_DATANFE", "Data Nfe", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("LOTE_PREVPAGAMENTO", "Pagamento", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_NUMERO", "N�mero", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_NFE", "NFe", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_DATANFE", "Data Nfe", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            GXCCtl = "LOTE_STATUS_" + sGXsfl_140_idx;
            cmbLote_Status.Name = GXCCtl;
            cmbLote_Status.WebTags = "";
            cmbLote_Status.addItem("B", "Stand by", 0);
            cmbLote_Status.addItem("S", "Solicitada", 0);
            cmbLote_Status.addItem("E", "Em An�lise", 0);
            cmbLote_Status.addItem("A", "Em execu��o", 0);
            cmbLote_Status.addItem("R", "Resolvida", 0);
            cmbLote_Status.addItem("C", "Conferida", 0);
            cmbLote_Status.addItem("D", "Rejeitada", 0);
            cmbLote_Status.addItem("H", "Homologada", 0);
            cmbLote_Status.addItem("O", "Aceite", 0);
            cmbLote_Status.addItem("P", "A Pagar", 0);
            cmbLote_Status.addItem("L", "Liquidada", 0);
            cmbLote_Status.addItem("X", "Cancelada", 0);
            cmbLote_Status.addItem("N", "N�o Faturada", 0);
            cmbLote_Status.addItem("J", "Planejamento", 0);
            cmbLote_Status.addItem("I", "An�lise Planejamento", 0);
            cmbLote_Status.addItem("T", "Validacao T�cnica", 0);
            cmbLote_Status.addItem("Q", "Validacao Qualidade", 0);
            cmbLote_Status.addItem("G", "Em Homologa��o", 0);
            cmbLote_Status.addItem("M", "Valida��o Mensura��o", 0);
            cmbLote_Status.addItem("U", "Rascunho", 0);
            if ( cmbLote_Status.ItemCount > 0 )
            {
               A575Lote_Status = cmbLote_Status.getValidValue(A575Lote_Status);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvLOTE_AREATRABALHOCODF52( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvLOTE_AREATRABALHOCOD_dataF52( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvLOTE_AREATRABALHOCOD_htmlF52( )
      {
         int gxdynajaxvalue ;
         GXDLVvLOTE_AREATRABALHOCOD_dataF52( ) ;
         gxdynajaxindex = 1;
         dynavLote_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavLote_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavLote_areatrabalhocod.ItemCount > 0 )
         {
            AV15Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavLote_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvLOTE_AREATRABALHOCOD_dataF52( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todas");
         /* Using cursor H00F53 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00F53_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00F53_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1402( ) ;
         while ( nGXsfl_140_idx <= nRC_GXsfl_140 )
         {
            sendrow_1402( ) ;
            nGXsfl_140_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_140_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_140_idx+1));
            sGXsfl_140_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_140_idx), 4, 0)), 4, "0");
            SubsflControlProps_1402( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV15Lote_AreaTrabalhoCod ,
                                       String AV17DynamicFiltersSelector1 ,
                                       DateTime AV50Lote_PrevPagamento1 ,
                                       DateTime AV51Lote_PrevPagamento_To1 ,
                                       String AV18Lote_Numero1 ,
                                       String AV19Lote_Nome1 ,
                                       int AV62Lote_NFe1 ,
                                       DateTime AV63Lote_DataNfe1 ,
                                       DateTime AV64Lote_DataNfe_To1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       DateTime AV52Lote_PrevPagamento2 ,
                                       DateTime AV53Lote_PrevPagamento_To2 ,
                                       String AV22Lote_Numero2 ,
                                       String AV23Lote_Nome2 ,
                                       int AV65Lote_NFe2 ,
                                       DateTime AV66Lote_DataNfe2 ,
                                       DateTime AV67Lote_DataNfe_To2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       DateTime AV54Lote_PrevPagamento3 ,
                                       DateTime AV55Lote_PrevPagamento_To3 ,
                                       String AV26Lote_Numero3 ,
                                       String AV27Lote_Nome3 ,
                                       int AV68Lote_NFe3 ,
                                       DateTime AV69Lote_DataNfe3 ,
                                       DateTime AV70Lote_DataNfe_To3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV16Lote_Status ,
                                       int AV73Lote_ContratadaCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV163Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV29DynamicFiltersIgnoreFirst ,
                                       bool AV28DynamicFiltersRemoving ,
                                       int A596Lote_Codigo ,
                                       String A575Lote_Status ,
                                       short AV33Quantidade ,
                                       short AV34QtdDmn ,
                                       short A569Lote_QtdeDmn ,
                                       decimal AV35QtdPF ,
                                       decimal A573Lote_QtdePF ,
                                       decimal AV36ValorTotal ,
                                       decimal A572Lote_Valor )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFF52( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "LOTE_NUMERO", StringUtil.RTrim( A562Lote_Numero));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "LOTE_NOME", StringUtil.RTrim( A563Lote_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOTE_DATA", context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NFE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOTE_NFE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATANFE", GetSecureSignedToken( "", A674Lote_DataNfe));
         GxWebStd.gx_hidden_field( context, "LOTE_DATANFE", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_PREVPAGAMENTO", GetSecureSignedToken( "", A857Lote_PrevPagamento));
         GxWebStd.gx_hidden_field( context, "LOTE_PREVPAGAMENTO", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALORPF", StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_QTDEPF", GetSecureSignedToken( "", context.localUtil.Format( A573Lote_QtdePF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "LOTE_QTDEPF", StringUtil.LTrim( StringUtil.NToC( A573Lote_QtdePF, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALOR", StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavLote_status.ItemCount > 0 )
         {
            AV16Lote_Status = cmbavLote_status.getValidValue(AV16Lote_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_Status", AV16Lote_Status);
         }
         if ( dynavLote_areatrabalhocod.ItemCount > 0 )
         {
            AV15Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavLote_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFF52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV163Pgmname = "ExtraWWLoteAPagar";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavQtddmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtddmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtddmn_Enabled), 5, 0)));
         edtavQtdpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdpf_Enabled), 5, 0)));
         edtavValortotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValortotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValortotal_Enabled), 5, 0)));
      }

      protected void RFF52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 140;
         /* Execute user event: E12F52 */
         E12F52 ();
         nGXsfl_140_idx = 1;
         sGXsfl_140_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_140_idx), 4, 0)), 4, "0");
         SubsflControlProps_1402( ) ;
         nGXsfl_140_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1402( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod ,
                                                 AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 ,
                                                 AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 ,
                                                 AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 ,
                                                 AV139ExtraWWLoteAPagarDS_7_Lote_numero1 ,
                                                 AV140ExtraWWLoteAPagarDS_8_Lote_nome1 ,
                                                 AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 ,
                                                 AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 ,
                                                 AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 ,
                                                 AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 ,
                                                 AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 ,
                                                 AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 ,
                                                 AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 ,
                                                 AV148ExtraWWLoteAPagarDS_16_Lote_numero2 ,
                                                 AV149ExtraWWLoteAPagarDS_17_Lote_nome2 ,
                                                 AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 ,
                                                 AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 ,
                                                 AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 ,
                                                 AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 ,
                                                 AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 ,
                                                 AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 ,
                                                 AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 ,
                                                 AV157ExtraWWLoteAPagarDS_25_Lote_numero3 ,
                                                 AV158ExtraWWLoteAPagarDS_26_Lote_nome3 ,
                                                 AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 ,
                                                 AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 ,
                                                 AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 ,
                                                 AV10GridState.gxTpr_Dynamicfilters.Count ,
                                                 A595Lote_AreaTrabalhoCod ,
                                                 A857Lote_PrevPagamento ,
                                                 A562Lote_Numero ,
                                                 A563Lote_Nome ,
                                                 A673Lote_NFe ,
                                                 A674Lote_DataNfe ,
                                                 Gx_date ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV6WWPContext.gxTpr_Userehcontratante ,
                                                 A1231Lote_ContratadaCod ,
                                                 AV6WWPContext.gxTpr_Contratada_codigo ,
                                                 A575Lote_Status },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING
                                                 }
            });
            lV139ExtraWWLoteAPagarDS_7_Lote_numero1 = StringUtil.PadR( StringUtil.RTrim( AV139ExtraWWLoteAPagarDS_7_Lote_numero1), 10, "%");
            lV140ExtraWWLoteAPagarDS_8_Lote_nome1 = StringUtil.PadR( StringUtil.RTrim( AV140ExtraWWLoteAPagarDS_8_Lote_nome1), 50, "%");
            lV148ExtraWWLoteAPagarDS_16_Lote_numero2 = StringUtil.PadR( StringUtil.RTrim( AV148ExtraWWLoteAPagarDS_16_Lote_numero2), 10, "%");
            lV149ExtraWWLoteAPagarDS_17_Lote_nome2 = StringUtil.PadR( StringUtil.RTrim( AV149ExtraWWLoteAPagarDS_17_Lote_nome2), 50, "%");
            lV157ExtraWWLoteAPagarDS_25_Lote_numero3 = StringUtil.PadR( StringUtil.RTrim( AV157ExtraWWLoteAPagarDS_25_Lote_numero3), 10, "%");
            lV158ExtraWWLoteAPagarDS_26_Lote_nome3 = StringUtil.PadR( StringUtil.RTrim( AV158ExtraWWLoteAPagarDS_26_Lote_nome3), 50, "%");
            /* Using cursor H00F58 */
            pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Userehcontratante, AV6WWPContext.gxTpr_Contratada_codigo, AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod, AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1, AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1, lV139ExtraWWLoteAPagarDS_7_Lote_numero1, lV140ExtraWWLoteAPagarDS_8_Lote_nome1, AV141ExtraWWLoteAPagarDS_9_Lote_nfe1, AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1, AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1, AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2, AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2, lV148ExtraWWLoteAPagarDS_16_Lote_numero2, lV149ExtraWWLoteAPagarDS_17_Lote_nome2, AV150ExtraWWLoteAPagarDS_18_Lote_nfe2, AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2, AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2, AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3, AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3, lV157ExtraWWLoteAPagarDS_25_Lote_numero3, lV158ExtraWWLoteAPagarDS_26_Lote_nome3, AV159ExtraWWLoteAPagarDS_27_Lote_nfe3, AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3, AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3, Gx_date, Gx_date, Gx_date, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_140_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A595Lote_AreaTrabalhoCod = H00F58_A595Lote_AreaTrabalhoCod[0];
               A565Lote_ValorPF = H00F58_A565Lote_ValorPF[0];
               A857Lote_PrevPagamento = H00F58_A857Lote_PrevPagamento[0];
               n857Lote_PrevPagamento = H00F58_n857Lote_PrevPagamento[0];
               A674Lote_DataNfe = H00F58_A674Lote_DataNfe[0];
               n674Lote_DataNfe = H00F58_n674Lote_DataNfe[0];
               A673Lote_NFe = H00F58_A673Lote_NFe[0];
               n673Lote_NFe = H00F58_n673Lote_NFe[0];
               A564Lote_Data = H00F58_A564Lote_Data[0];
               A563Lote_Nome = H00F58_A563Lote_Nome[0];
               A562Lote_Numero = H00F58_A562Lote_Numero[0];
               A596Lote_Codigo = H00F58_A596Lote_Codigo[0];
               A1231Lote_ContratadaCod = H00F58_A1231Lote_ContratadaCod[0];
               A575Lote_Status = H00F58_A575Lote_Status[0];
               A569Lote_QtdeDmn = H00F58_A569Lote_QtdeDmn[0];
               A1057Lote_ValorGlosas = H00F58_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00F58_n1057Lote_ValorGlosas[0];
               A1231Lote_ContratadaCod = H00F58_A1231Lote_ContratadaCod[0];
               A575Lote_Status = H00F58_A575Lote_Status[0];
               A569Lote_QtdeDmn = H00F58_A569Lote_QtdeDmn[0];
               A1057Lote_ValorGlosas = H00F58_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00F58_n1057Lote_ValorGlosas[0];
               GetLote_QtdePF( A596Lote_Codigo) ;
               A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
               /* Execute user event: E24F52 */
               E24F52 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 140;
            WBF50( ) ;
         }
         nGXsfl_140_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV133ExtraWWLoteAPagarDS_1_Lote_status = AV16Lote_Status;
         AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod = AV15Lote_AreaTrabalhoCod;
         AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod = AV73Lote_ContratadaCod;
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = AV50Lote_PrevPagamento1;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = AV51Lote_PrevPagamento_To1;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = AV18Lote_Numero1;
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = AV19Lote_Nome1;
         AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 = AV62Lote_NFe1;
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = AV63Lote_DataNfe1;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = AV64Lote_DataNfe_To1;
         AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = AV52Lote_PrevPagamento2;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = AV53Lote_PrevPagamento_To2;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = AV22Lote_Numero2;
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = AV23Lote_Nome2;
         AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 = AV65Lote_NFe2;
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = AV66Lote_DataNfe2;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = AV67Lote_DataNfe_To2;
         AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = AV54Lote_PrevPagamento3;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = AV55Lote_PrevPagamento_To3;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = AV26Lote_Numero3;
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = AV27Lote_Nome3;
         AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 = AV68Lote_NFe3;
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = AV69Lote_DataNfe3;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = AV70Lote_DataNfe_To3;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod ,
                                              AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 ,
                                              AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 ,
                                              AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 ,
                                              AV139ExtraWWLoteAPagarDS_7_Lote_numero1 ,
                                              AV140ExtraWWLoteAPagarDS_8_Lote_nome1 ,
                                              AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 ,
                                              AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 ,
                                              AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 ,
                                              AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 ,
                                              AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 ,
                                              AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 ,
                                              AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 ,
                                              AV148ExtraWWLoteAPagarDS_16_Lote_numero2 ,
                                              AV149ExtraWWLoteAPagarDS_17_Lote_nome2 ,
                                              AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 ,
                                              AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 ,
                                              AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 ,
                                              AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 ,
                                              AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 ,
                                              AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 ,
                                              AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 ,
                                              AV157ExtraWWLoteAPagarDS_25_Lote_numero3 ,
                                              AV158ExtraWWLoteAPagarDS_26_Lote_nome3 ,
                                              AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 ,
                                              AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 ,
                                              AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 ,
                                              AV10GridState.gxTpr_Dynamicfilters.Count ,
                                              A595Lote_AreaTrabalhoCod ,
                                              A857Lote_PrevPagamento ,
                                              A562Lote_Numero ,
                                              A563Lote_Nome ,
                                              A673Lote_NFe ,
                                              A674Lote_DataNfe ,
                                              Gx_date ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              AV6WWPContext.gxTpr_Userehcontratante ,
                                              A1231Lote_ContratadaCod ,
                                              AV6WWPContext.gxTpr_Contratada_codigo ,
                                              A575Lote_Status },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV139ExtraWWLoteAPagarDS_7_Lote_numero1 = StringUtil.PadR( StringUtil.RTrim( AV139ExtraWWLoteAPagarDS_7_Lote_numero1), 10, "%");
         lV140ExtraWWLoteAPagarDS_8_Lote_nome1 = StringUtil.PadR( StringUtil.RTrim( AV140ExtraWWLoteAPagarDS_8_Lote_nome1), 50, "%");
         lV148ExtraWWLoteAPagarDS_16_Lote_numero2 = StringUtil.PadR( StringUtil.RTrim( AV148ExtraWWLoteAPagarDS_16_Lote_numero2), 10, "%");
         lV149ExtraWWLoteAPagarDS_17_Lote_nome2 = StringUtil.PadR( StringUtil.RTrim( AV149ExtraWWLoteAPagarDS_17_Lote_nome2), 50, "%");
         lV157ExtraWWLoteAPagarDS_25_Lote_numero3 = StringUtil.PadR( StringUtil.RTrim( AV157ExtraWWLoteAPagarDS_25_Lote_numero3), 10, "%");
         lV158ExtraWWLoteAPagarDS_26_Lote_nome3 = StringUtil.PadR( StringUtil.RTrim( AV158ExtraWWLoteAPagarDS_26_Lote_nome3), 50, "%");
         /* Using cursor H00F513 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Userehcontratante, AV6WWPContext.gxTpr_Contratada_codigo, AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod, AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1, AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1, lV139ExtraWWLoteAPagarDS_7_Lote_numero1, lV140ExtraWWLoteAPagarDS_8_Lote_nome1, AV141ExtraWWLoteAPagarDS_9_Lote_nfe1, AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1, AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1, AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2, AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2, lV148ExtraWWLoteAPagarDS_16_Lote_numero2, lV149ExtraWWLoteAPagarDS_17_Lote_nome2, AV150ExtraWWLoteAPagarDS_18_Lote_nfe2, AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2, AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2, AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3, AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3, lV157ExtraWWLoteAPagarDS_25_Lote_numero3, lV158ExtraWWLoteAPagarDS_26_Lote_nome3, AV159ExtraWWLoteAPagarDS_27_Lote_nfe3, AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3, AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3, Gx_date, Gx_date, Gx_date});
         GRID_nRecordCount = H00F513_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV133ExtraWWLoteAPagarDS_1_Lote_status = AV16Lote_Status;
         AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod = AV15Lote_AreaTrabalhoCod;
         AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod = AV73Lote_ContratadaCod;
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = AV50Lote_PrevPagamento1;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = AV51Lote_PrevPagamento_To1;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = AV18Lote_Numero1;
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = AV19Lote_Nome1;
         AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 = AV62Lote_NFe1;
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = AV63Lote_DataNfe1;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = AV64Lote_DataNfe_To1;
         AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = AV52Lote_PrevPagamento2;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = AV53Lote_PrevPagamento_To2;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = AV22Lote_Numero2;
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = AV23Lote_Nome2;
         AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 = AV65Lote_NFe2;
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = AV66Lote_DataNfe2;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = AV67Lote_DataNfe_To2;
         AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = AV54Lote_PrevPagamento3;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = AV55Lote_PrevPagamento_To3;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = AV26Lote_Numero3;
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = AV27Lote_Nome3;
         AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 = AV68Lote_NFe3;
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = AV69Lote_DataNfe3;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = AV70Lote_DataNfe_To3;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV133ExtraWWLoteAPagarDS_1_Lote_status = AV16Lote_Status;
         AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod = AV15Lote_AreaTrabalhoCod;
         AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod = AV73Lote_ContratadaCod;
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = AV50Lote_PrevPagamento1;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = AV51Lote_PrevPagamento_To1;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = AV18Lote_Numero1;
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = AV19Lote_Nome1;
         AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 = AV62Lote_NFe1;
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = AV63Lote_DataNfe1;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = AV64Lote_DataNfe_To1;
         AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = AV52Lote_PrevPagamento2;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = AV53Lote_PrevPagamento_To2;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = AV22Lote_Numero2;
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = AV23Lote_Nome2;
         AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 = AV65Lote_NFe2;
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = AV66Lote_DataNfe2;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = AV67Lote_DataNfe_To2;
         AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = AV54Lote_PrevPagamento3;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = AV55Lote_PrevPagamento_To3;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = AV26Lote_Numero3;
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = AV27Lote_Nome3;
         AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 = AV68Lote_NFe3;
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = AV69Lote_DataNfe3;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = AV70Lote_DataNfe_To3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV133ExtraWWLoteAPagarDS_1_Lote_status = AV16Lote_Status;
         AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod = AV15Lote_AreaTrabalhoCod;
         AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod = AV73Lote_ContratadaCod;
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = AV50Lote_PrevPagamento1;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = AV51Lote_PrevPagamento_To1;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = AV18Lote_Numero1;
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = AV19Lote_Nome1;
         AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 = AV62Lote_NFe1;
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = AV63Lote_DataNfe1;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = AV64Lote_DataNfe_To1;
         AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = AV52Lote_PrevPagamento2;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = AV53Lote_PrevPagamento_To2;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = AV22Lote_Numero2;
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = AV23Lote_Nome2;
         AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 = AV65Lote_NFe2;
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = AV66Lote_DataNfe2;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = AV67Lote_DataNfe_To2;
         AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = AV54Lote_PrevPagamento3;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = AV55Lote_PrevPagamento_To3;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = AV26Lote_Numero3;
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = AV27Lote_Nome3;
         AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 = AV68Lote_NFe3;
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = AV69Lote_DataNfe3;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = AV70Lote_DataNfe_To3;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV133ExtraWWLoteAPagarDS_1_Lote_status = AV16Lote_Status;
         AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod = AV15Lote_AreaTrabalhoCod;
         AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod = AV73Lote_ContratadaCod;
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = AV50Lote_PrevPagamento1;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = AV51Lote_PrevPagamento_To1;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = AV18Lote_Numero1;
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = AV19Lote_Nome1;
         AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 = AV62Lote_NFe1;
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = AV63Lote_DataNfe1;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = AV64Lote_DataNfe_To1;
         AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = AV52Lote_PrevPagamento2;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = AV53Lote_PrevPagamento_To2;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = AV22Lote_Numero2;
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = AV23Lote_Nome2;
         AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 = AV65Lote_NFe2;
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = AV66Lote_DataNfe2;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = AV67Lote_DataNfe_To2;
         AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = AV54Lote_PrevPagamento3;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = AV55Lote_PrevPagamento_To3;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = AV26Lote_Numero3;
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = AV27Lote_Nome3;
         AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 = AV68Lote_NFe3;
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = AV69Lote_DataNfe3;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = AV70Lote_DataNfe_To3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV133ExtraWWLoteAPagarDS_1_Lote_status = AV16Lote_Status;
         AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod = AV15Lote_AreaTrabalhoCod;
         AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod = AV73Lote_ContratadaCod;
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = AV50Lote_PrevPagamento1;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = AV51Lote_PrevPagamento_To1;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = AV18Lote_Numero1;
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = AV19Lote_Nome1;
         AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 = AV62Lote_NFe1;
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = AV63Lote_DataNfe1;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = AV64Lote_DataNfe_To1;
         AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = AV52Lote_PrevPagamento2;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = AV53Lote_PrevPagamento_To2;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = AV22Lote_Numero2;
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = AV23Lote_Nome2;
         AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 = AV65Lote_NFe2;
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = AV66Lote_DataNfe2;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = AV67Lote_DataNfe_To2;
         AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = AV54Lote_PrevPagamento3;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = AV55Lote_PrevPagamento_To3;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = AV26Lote_Numero3;
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = AV27Lote_Nome3;
         AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 = AV68Lote_NFe3;
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = AV69Lote_DataNfe3;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = AV70Lote_DataNfe_To3;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         }
         return (int)(0) ;
      }

      protected void STRUPF50( )
      {
         /* Before Start, stand alone formulas. */
         AV163Pgmname = "ExtraWWLoteAPagar";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavQtddmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtddmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtddmn_Enabled), 5, 0)));
         edtavQtdpf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdpf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdpf_Enabled), 5, 0)));
         edtavValortotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValortotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValortotal_Enabled), 5, 0)));
         GXVvLOTE_AREATRABALHOCOD_htmlF52( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23F52 */
         E23F52 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavLote_status.Name = cmbavLote_status_Internalname;
            cmbavLote_status.CurrentValue = cgiGet( cmbavLote_status_Internalname);
            AV16Lote_Status = cgiGet( cmbavLote_status_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_Status", AV16Lote_Status);
            dynavLote_areatrabalhocod.Name = dynavLote_areatrabalhocod_Internalname;
            dynavLote_areatrabalhocod.CurrentValue = cgiGet( dynavLote_areatrabalhocod_Internalname);
            AV15Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavLote_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_CONTRATADACOD");
               GX_FocusControl = edtavLote_contratadacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73Lote_ContratadaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73Lote_ContratadaCod), 6, 0)));
            }
            else
            {
               AV73Lote_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73Lote_ContratadaCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV17DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento1"}), 1, "vLOTE_PREVPAGAMENTO1");
               GX_FocusControl = edtavLote_prevpagamento1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50Lote_PrevPagamento1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Lote_PrevPagamento1", context.localUtil.Format(AV50Lote_PrevPagamento1, "99/99/99"));
            }
            else
            {
               AV50Lote_PrevPagamento1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Lote_PrevPagamento1", context.localUtil.Format(AV50Lote_PrevPagamento1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento_To1"}), 1, "vLOTE_PREVPAGAMENTO_TO1");
               GX_FocusControl = edtavLote_prevpagamento_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51Lote_PrevPagamento_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Lote_PrevPagamento_To1", context.localUtil.Format(AV51Lote_PrevPagamento_To1, "99/99/99"));
            }
            else
            {
               AV51Lote_PrevPagamento_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Lote_PrevPagamento_To1", context.localUtil.Format(AV51Lote_PrevPagamento_To1, "99/99/99"));
            }
            AV18Lote_Numero1 = cgiGet( edtavLote_numero1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_Numero1", AV18Lote_Numero1);
            AV19Lote_Nome1 = StringUtil.Upper( cgiGet( edtavLote_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Lote_Nome1", AV19Lote_Nome1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE1");
               GX_FocusControl = edtavLote_nfe1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62Lote_NFe1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Lote_NFe1), 6, 0)));
            }
            else
            {
               AV62Lote_NFe1 = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Lote_NFe1), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe1"}), 1, "vLOTE_DATANFE1");
               GX_FocusControl = edtavLote_datanfe1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63Lote_DataNfe1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Lote_DataNfe1", context.localUtil.Format(AV63Lote_DataNfe1, "99/99/99"));
            }
            else
            {
               AV63Lote_DataNfe1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Lote_DataNfe1", context.localUtil.Format(AV63Lote_DataNfe1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe_To1"}), 1, "vLOTE_DATANFE_TO1");
               GX_FocusControl = edtavLote_datanfe_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64Lote_DataNfe_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Lote_DataNfe_To1", context.localUtil.Format(AV64Lote_DataNfe_To1, "99/99/99"));
            }
            else
            {
               AV64Lote_DataNfe_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Lote_DataNfe_To1", context.localUtil.Format(AV64Lote_DataNfe_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento2"}), 1, "vLOTE_PREVPAGAMENTO2");
               GX_FocusControl = edtavLote_prevpagamento2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52Lote_PrevPagamento2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Lote_PrevPagamento2", context.localUtil.Format(AV52Lote_PrevPagamento2, "99/99/99"));
            }
            else
            {
               AV52Lote_PrevPagamento2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Lote_PrevPagamento2", context.localUtil.Format(AV52Lote_PrevPagamento2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento_To2"}), 1, "vLOTE_PREVPAGAMENTO_TO2");
               GX_FocusControl = edtavLote_prevpagamento_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53Lote_PrevPagamento_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_PrevPagamento_To2", context.localUtil.Format(AV53Lote_PrevPagamento_To2, "99/99/99"));
            }
            else
            {
               AV53Lote_PrevPagamento_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_PrevPagamento_To2", context.localUtil.Format(AV53Lote_PrevPagamento_To2, "99/99/99"));
            }
            AV22Lote_Numero2 = cgiGet( edtavLote_numero2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Numero2", AV22Lote_Numero2);
            AV23Lote_Nome2 = StringUtil.Upper( cgiGet( edtavLote_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_Nome2", AV23Lote_Nome2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE2");
               GX_FocusControl = edtavLote_nfe2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65Lote_NFe2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Lote_NFe2), 6, 0)));
            }
            else
            {
               AV65Lote_NFe2 = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Lote_NFe2), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe2"}), 1, "vLOTE_DATANFE2");
               GX_FocusControl = edtavLote_datanfe2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66Lote_DataNfe2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Lote_DataNfe2", context.localUtil.Format(AV66Lote_DataNfe2, "99/99/99"));
            }
            else
            {
               AV66Lote_DataNfe2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Lote_DataNfe2", context.localUtil.Format(AV66Lote_DataNfe2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe_To2"}), 1, "vLOTE_DATANFE_TO2");
               GX_FocusControl = edtavLote_datanfe_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67Lote_DataNfe_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Lote_DataNfe_To2", context.localUtil.Format(AV67Lote_DataNfe_To2, "99/99/99"));
            }
            else
            {
               AV67Lote_DataNfe_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Lote_DataNfe_To2", context.localUtil.Format(AV67Lote_DataNfe_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento3"}), 1, "vLOTE_PREVPAGAMENTO3");
               GX_FocusControl = edtavLote_prevpagamento3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54Lote_PrevPagamento3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Lote_PrevPagamento3", context.localUtil.Format(AV54Lote_PrevPagamento3, "99/99/99"));
            }
            else
            {
               AV54Lote_PrevPagamento3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Lote_PrevPagamento3", context.localUtil.Format(AV54Lote_PrevPagamento3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_prevpagamento_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Prev Pagamento_To3"}), 1, "vLOTE_PREVPAGAMENTO_TO3");
               GX_FocusControl = edtavLote_prevpagamento_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55Lote_PrevPagamento_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Lote_PrevPagamento_To3", context.localUtil.Format(AV55Lote_PrevPagamento_To3, "99/99/99"));
            }
            else
            {
               AV55Lote_PrevPagamento_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_prevpagamento_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Lote_PrevPagamento_To3", context.localUtil.Format(AV55Lote_PrevPagamento_To3, "99/99/99"));
            }
            AV26Lote_Numero3 = cgiGet( edtavLote_numero3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Lote_Numero3", AV26Lote_Numero3);
            AV27Lote_Nome3 = StringUtil.Upper( cgiGet( edtavLote_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_nfe3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_NFE3");
               GX_FocusControl = edtavLote_nfe3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68Lote_NFe3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Lote_NFe3), 6, 0)));
            }
            else
            {
               AV68Lote_NFe3 = (int)(context.localUtil.CToN( cgiGet( edtavLote_nfe3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Lote_NFe3), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe3"}), 1, "vLOTE_DATANFE3");
               GX_FocusControl = edtavLote_datanfe3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69Lote_DataNfe3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Lote_DataNfe3", context.localUtil.Format(AV69Lote_DataNfe3, "99/99/99"));
            }
            else
            {
               AV69Lote_DataNfe3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Lote_DataNfe3", context.localUtil.Format(AV69Lote_DataNfe3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavLote_datanfe_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Lote_Data Nfe_To3"}), 1, "vLOTE_DATANFE_TO3");
               GX_FocusControl = edtavLote_datanfe_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70Lote_DataNfe_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Lote_DataNfe_To3", context.localUtil.Format(AV70Lote_DataNfe_To3, "99/99/99"));
            }
            else
            {
               AV70Lote_DataNfe_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavLote_datanfe_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Lote_DataNfe_To3", context.localUtil.Format(AV70Lote_DataNfe_To3, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQUANTIDADE");
               GX_FocusControl = edtavQuantidade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33Quantidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Quantidade), 3, 0)));
            }
            else
            {
               AV33Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Quantidade), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtddmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtddmn_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDDMN");
               GX_FocusControl = edtavQtddmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34QtdDmn = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdDmn), 3, 0)));
            }
            else
            {
               AV34QtdDmn = (short)(context.localUtil.CToN( cgiGet( edtavQtddmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdDmn), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdpf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdpf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDPF");
               GX_FocusControl = edtavQtdpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35QtdPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdPF", StringUtil.LTrim( StringUtil.Str( AV35QtdPF, 14, 5)));
            }
            else
            {
               AV35QtdPF = context.localUtil.CToN( cgiGet( edtavQtdpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdPF", StringUtil.LTrim( StringUtil.Str( AV35QtdPF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavValortotal_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavValortotal_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVALORTOTAL");
               GX_FocusControl = edtavValortotal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36ValorTotal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ValorTotal", StringUtil.LTrim( StringUtil.Str( AV36ValorTotal, 18, 5)));
            }
            else
            {
               AV36ValorTotal = context.localUtil.CToN( cgiGet( edtavValortotal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ValorTotal", StringUtil.LTrim( StringUtil.Str( AV36ValorTotal, 18, 5)));
            }
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_140 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_140"), ",", "."));
            AV128GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV129GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Innewwindow1_Target = cgiGet( "INNEWWINDOW1_Target");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV15Lote_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO1"), 0) != AV50Lote_PrevPagamento1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO1"), 0) != AV51Lote_PrevPagamento_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO1"), AV18Lote_Numero1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME1"), AV19Lote_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE1"), ",", ".") != Convert.ToDecimal( AV62Lote_NFe1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE1"), 0) != AV63Lote_DataNfe1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO1"), 0) != AV64Lote_DataNfe_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO2"), 0) != AV52Lote_PrevPagamento2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO2"), 0) != AV53Lote_PrevPagamento_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO2"), AV22Lote_Numero2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME2"), AV23Lote_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE2"), ",", ".") != Convert.ToDecimal( AV65Lote_NFe2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE2"), 0) != AV66Lote_DataNfe2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO2"), 0) != AV67Lote_DataNfe_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO3"), 0) != AV54Lote_PrevPagamento3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_PREVPAGAMENTO_TO3"), 0) != AV55Lote_PrevPagamento_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NUMERO3"), AV26Lote_Numero3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME3"), AV27Lote_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vLOTE_NFE3"), ",", ".") != Convert.ToDecimal( AV68Lote_NFe3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE3"), 0) != AV69Lote_DataNfe3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vLOTE_DATANFE_TO3"), 0) != AV70Lote_DataNfe_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23F52 */
         E23F52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23F52( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 18/03/2020 22:07", 0) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV17DynamicFiltersSelector1 = "LOTE_PREVPAGAMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "LOTE_PREVPAGAMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "LOTE_PREVPAGAMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         AV16Lote_Status = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_Status", AV16Lote_Status);
         Form.Caption = " Lotes a Pagar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Recentes", 0);
         cmbavOrderedby.addItem("2", "Lote", 0);
         cmbavOrderedby.addItem("3", "Nome", 0);
         cmbavOrderedby.addItem("4", "Data do Lote", 0);
         cmbavOrderedby.addItem("5", "NFe", 0);
         cmbavOrderedby.addItem("6", "Emiss�o", 0);
         cmbavOrderedby.addItem("7", "Previs�o", 0);
         cmbavOrderedby.addItem("8", "PF R$", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E12F52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLote_Numero_Titleformat = 2;
         edtLote_Numero_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Lote", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Numero_Internalname, "Title", edtLote_Numero_Title);
         edtLote_Nome_Titleformat = 2;
         edtLote_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Nome_Internalname, "Title", edtLote_Nome_Title);
         edtLote_Data_Titleformat = 2;
         edtLote_Data_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Data do Lote", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Data_Internalname, "Title", edtLote_Data_Title);
         edtLote_NFe_Titleformat = 2;
         edtLote_NFe_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "NFe", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_NFe_Internalname, "Title", edtLote_NFe_Title);
         edtLote_DataNfe_Titleformat = 2;
         edtLote_DataNfe_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Emiss�o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataNfe_Internalname, "Title", edtLote_DataNfe_Title);
         edtLote_PrevPagamento_Titleformat = 2;
         edtLote_PrevPagamento_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV13OrderedBy==7) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Previs�o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_PrevPagamento_Internalname, "Title", edtLote_PrevPagamento_Title);
         edtLote_ValorPF_Titleformat = 2;
         edtLote_ValorPF_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV13OrderedBy==8) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "PF R$", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_ValorPF_Internalname, "Title", edtLote_ValorPF_Title);
         AV128GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV128GridCurrentPage), 10, 0)));
         AV129GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV129GridPageCount), 10, 0)));
         edtavAction_Title = "A��o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAction_Internalname, "Title", edtavAction_Title);
         edtavAction_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAction_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAction_Visible), 5, 0)));
         AV33Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Quantidade), 3, 0)));
         AV34QtdDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdDmn), 3, 0)));
         AV35QtdPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdPF", StringUtil.LTrim( StringUtil.Str( AV35QtdPF, 14, 5)));
         AV36ValorTotal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ValorTotal", StringUtil.LTrim( StringUtil.Str( AV36ValorTotal, 18, 5)));
         AV133ExtraWWLoteAPagarDS_1_Lote_status = AV16Lote_Status;
         AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod = AV15Lote_AreaTrabalhoCod;
         AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod = AV73Lote_ContratadaCod;
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = AV17DynamicFiltersSelector1;
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = AV50Lote_PrevPagamento1;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = AV51Lote_PrevPagamento_To1;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = AV18Lote_Numero1;
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = AV19Lote_Nome1;
         AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 = AV62Lote_NFe1;
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = AV63Lote_DataNfe1;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = AV64Lote_DataNfe_To1;
         AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = AV52Lote_PrevPagamento2;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = AV53Lote_PrevPagamento_To2;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = AV22Lote_Numero2;
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = AV23Lote_Nome2;
         AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 = AV65Lote_NFe2;
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = AV66Lote_DataNfe2;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = AV67Lote_DataNfe_To2;
         AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = AV54Lote_PrevPagamento3;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = AV55Lote_PrevPagamento_To3;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = AV26Lote_Numero3;
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = AV27Lote_Nome3;
         AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 = AV68Lote_NFe3;
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = AV69Lote_DataNfe3;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = AV70Lote_DataNfe_To3;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         dynavLote_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavLote_areatrabalhocod_Internalname, "Values", dynavLote_areatrabalhocod.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11F52( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV127PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV127PageToGo) ;
         }
      }

      private void E24F52( )
      {
         /* Grid_Load Routine */
         AV61Action = context.GetImagePath( "6235c4eb-f913-43b6-a04a-91a52d975b13", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAction_Internalname, AV61Action);
         AV162Action_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6235c4eb-f913-43b6-a04a-91a52d975b13", "", context.GetTheme( )));
         edtavAction_Tooltiptext = "Incluir pagamento e marcar como Liquidado";
         edtLote_Nome_Link = formatLink("viewlote.aspx") + "?" + UrlEncode("" +A596Lote_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtavAction_Visible = (((StringUtil.StrCmp(A575Lote_Status, "P")==0)) ? 1 : 0);
         AV33Quantidade = (short)(AV33Quantidade+1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33Quantidade), 3, 0)));
         AV34QtdDmn = (short)(AV34QtdDmn+A569Lote_QtdeDmn);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdDmn), 3, 0)));
         AV35QtdPF = (decimal)(AV35QtdPF+A573Lote_QtdePF);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35QtdPF", StringUtil.LTrim( StringUtil.Str( AV35QtdPF, 14, 5)));
         AV36ValorTotal = (decimal)(AV36ValorTotal+A572Lote_Valor);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ValorTotal", StringUtil.LTrim( StringUtil.Str( AV36ValorTotal, 18, 5)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 140;
         }
         sendrow_1402( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_140_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(140, GridRow);
         }
      }

      protected void E13F52( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E18F52( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E14F52( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV29DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersIgnoreFirst", AV29DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E19F52( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20F52( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E15F52( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21F52( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16F52( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV28DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersRemoving", AV28DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Lote_AreaTrabalhoCod, AV17DynamicFiltersSelector1, AV50Lote_PrevPagamento1, AV51Lote_PrevPagamento_To1, AV18Lote_Numero1, AV19Lote_Nome1, AV62Lote_NFe1, AV63Lote_DataNfe1, AV64Lote_DataNfe_To1, AV21DynamicFiltersSelector2, AV52Lote_PrevPagamento2, AV53Lote_PrevPagamento_To2, AV22Lote_Numero2, AV23Lote_Nome2, AV65Lote_NFe2, AV66Lote_DataNfe2, AV67Lote_DataNfe_To2, AV25DynamicFiltersSelector3, AV54Lote_PrevPagamento3, AV55Lote_PrevPagamento_To3, AV26Lote_Numero3, AV27Lote_Nome3, AV68Lote_NFe3, AV69Lote_DataNfe3, AV70Lote_DataNfe_To3, AV20DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV16Lote_Status, AV73Lote_ContratadaCod, AV6WWPContext, AV163Pgmname, AV10GridState, AV29DynamicFiltersIgnoreFirst, AV28DynamicFiltersRemoving, A596Lote_Codigo, A575Lote_Status, AV33Quantidade, AV34QtdDmn, A569Lote_QtdeDmn, AV35QtdPF, A573Lote_QtdePF, AV36ValorTotal, A572Lote_Valor) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22F52( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17F52( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavLote_status.CurrentValue = StringUtil.RTrim( AV16Lote_Status);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLote_status_Internalname, "Values", cmbavLote_status.ToJavascriptSource());
         dynavLote_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavLote_areatrabalhocod_Internalname, "Values", dynavLote_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E25F52( )
      {
         /* 'DoAction' Routine */
         context.PopUp(formatLink("wp_lote_liq.aspx") + "?" + UrlEncode("" +A596Lote_Codigo) + "," + UrlEncode(StringUtil.RTrim("")), new Object[] {"A596Lote_Codigo","AV60Confirmado"});
         context.DoAjaxRefresh();
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterslote_prevpagamento1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento1_Visible), 5, 0)));
         edtavLote_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero1_Visible), 5, 0)));
         edtavLote_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome1_Visible), 5, 0)));
         edtavLote_nfe1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe1_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_datanfe1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_PREVPAGAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterslote_prevpagamento1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NUMERO") == 0 )
         {
            edtavLote_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NOME") == 0 )
         {
            edtavLote_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NFE") == 0 )
         {
            edtavLote_nfe1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_DATANFE") == 0 )
         {
            tblTablemergeddynamicfilterslote_datanfe1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterslote_prevpagamento2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento2_Visible), 5, 0)));
         edtavLote_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero2_Visible), 5, 0)));
         edtavLote_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome2_Visible), 5, 0)));
         edtavLote_nfe2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe2_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_datanfe2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_PREVPAGAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterslote_prevpagamento2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NUMERO") == 0 )
         {
            edtavLote_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NOME") == 0 )
         {
            edtavLote_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NFE") == 0 )
         {
            edtavLote_nfe2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_DATANFE") == 0 )
         {
            tblTablemergeddynamicfilterslote_datanfe2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterslote_prevpagamento3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento3_Visible), 5, 0)));
         edtavLote_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero3_Visible), 5, 0)));
         edtavLote_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome3_Visible), 5, 0)));
         edtavLote_nfe3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe3_Visible), 5, 0)));
         tblTablemergeddynamicfilterslote_datanfe3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_PREVPAGAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterslote_prevpagamento3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_prevpagamento3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NUMERO") == 0 )
         {
            edtavLote_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_numero3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 )
         {
            edtavLote_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NFE") == 0 )
         {
            edtavLote_nfe3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nfe3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_DATANFE") == 0 )
         {
            tblTablemergeddynamicfilterslote_datanfe3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterslote_datanfe3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterslote_datanfe3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "LOTE_PREVPAGAMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV52Lote_PrevPagamento2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Lote_PrevPagamento2", context.localUtil.Format(AV52Lote_PrevPagamento2, "99/99/99"));
         AV53Lote_PrevPagamento_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_PrevPagamento_To2", context.localUtil.Format(AV53Lote_PrevPagamento_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "LOTE_PREVPAGAMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV54Lote_PrevPagamento3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Lote_PrevPagamento3", context.localUtil.Format(AV54Lote_PrevPagamento3, "99/99/99"));
         AV55Lote_PrevPagamento_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Lote_PrevPagamento_To3", context.localUtil.Format(AV55Lote_PrevPagamento_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16Lote_Status = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_Status", AV16Lote_Status);
         AV15Lote_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)));
         AV73Lote_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73Lote_ContratadaCod), 6, 0)));
         AV17DynamicFiltersSelector1 = "LOTE_PREVPAGAMENTO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         AV50Lote_PrevPagamento1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Lote_PrevPagamento1", context.localUtil.Format(AV50Lote_PrevPagamento1, "99/99/99"));
         AV51Lote_PrevPagamento_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Lote_PrevPagamento_To1", context.localUtil.Format(AV51Lote_PrevPagamento_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get(AV163Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV163Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV32Session.Get(AV163Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S212( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV164GXV1 = 1;
         while ( AV164GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV164GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "LOTE_STATUS") == 0 )
            {
               AV16Lote_Status = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Lote_Status", AV16Lote_Status);
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "LOTE_AREATRABALHOCOD") == 0 )
            {
               AV15Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "LOTE_CONTRATADACOD") == 0 )
            {
               AV73Lote_ContratadaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73Lote_ContratadaCod), 6, 0)));
            }
            AV164GXV1 = (int)(AV164GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV17DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_PREVPAGAMENTO") == 0 )
            {
               AV50Lote_PrevPagamento1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Lote_PrevPagamento1", context.localUtil.Format(AV50Lote_PrevPagamento1, "99/99/99"));
               AV51Lote_PrevPagamento_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Lote_PrevPagamento_To1", context.localUtil.Format(AV51Lote_PrevPagamento_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NUMERO") == 0 )
            {
               AV18Lote_Numero1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_Numero1", AV18Lote_Numero1);
            }
            else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NOME") == 0 )
            {
               AV19Lote_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Lote_Nome1", AV19Lote_Nome1);
            }
            else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NFE") == 0 )
            {
               AV62Lote_NFe1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Lote_NFe1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62Lote_NFe1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_DATANFE") == 0 )
            {
               AV63Lote_DataNfe1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Lote_DataNfe1", context.localUtil.Format(AV63Lote_DataNfe1, "99/99/99"));
               AV64Lote_DataNfe_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64Lote_DataNfe_To1", context.localUtil.Format(AV64Lote_DataNfe_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_PREVPAGAMENTO") == 0 )
               {
                  AV52Lote_PrevPagamento2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Lote_PrevPagamento2", context.localUtil.Format(AV52Lote_PrevPagamento2, "99/99/99"));
                  AV53Lote_PrevPagamento_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Lote_PrevPagamento_To2", context.localUtil.Format(AV53Lote_PrevPagamento_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NUMERO") == 0 )
               {
                  AV22Lote_Numero2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Numero2", AV22Lote_Numero2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NOME") == 0 )
               {
                  AV23Lote_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_Nome2", AV23Lote_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NFE") == 0 )
               {
                  AV65Lote_NFe2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65Lote_NFe2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65Lote_NFe2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_DATANFE") == 0 )
               {
                  AV66Lote_DataNfe2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66Lote_DataNfe2", context.localUtil.Format(AV66Lote_DataNfe2, "99/99/99"));
                  AV67Lote_DataNfe_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Lote_DataNfe_To2", context.localUtil.Format(AV67Lote_DataNfe_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_PREVPAGAMENTO") == 0 )
                  {
                     AV54Lote_PrevPagamento3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54Lote_PrevPagamento3", context.localUtil.Format(AV54Lote_PrevPagamento3, "99/99/99"));
                     AV55Lote_PrevPagamento_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Lote_PrevPagamento_To3", context.localUtil.Format(AV55Lote_PrevPagamento_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NUMERO") == 0 )
                  {
                     AV26Lote_Numero3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Lote_Numero3", AV26Lote_Numero3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 )
                  {
                     AV27Lote_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NFE") == 0 )
                  {
                     AV68Lote_NFe3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Lote_NFe3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68Lote_NFe3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_DATANFE") == 0 )
                  {
                     AV69Lote_DataNfe3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69Lote_DataNfe3", context.localUtil.Format(AV69Lote_DataNfe3, "99/99/99"));
                     AV70Lote_DataNfe_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Lote_DataNfe_To3", context.localUtil.Format(AV70Lote_DataNfe_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV28DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV15Lote_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)));
         AV10GridState.FromXml(AV32Session.Get(AV163Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Lote_Status)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "LOTE_STATUS";
            AV11GridStateFilterValue.gxTpr_Value = AV16Lote_Status;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV15Lote_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "LOTE_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV73Lote_ContratadaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "LOTE_CONTRATADACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV73Lote_ContratadaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV163Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV29DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV17DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_PREVPAGAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV50Lote_PrevPagamento1) && (DateTime.MinValue==AV51Lote_PrevPagamento_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV50Lote_PrevPagamento1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV51Lote_PrevPagamento_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Lote_Numero1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Lote_Numero1;
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Lote_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Lote_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_NFE") == 0 ) && ! (0==AV62Lote_NFe1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV62Lote_NFe1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "LOTE_DATANFE") == 0 ) && ! ( (DateTime.MinValue==AV63Lote_DataNfe1) && (DateTime.MinValue==AV64Lote_DataNfe_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV63Lote_DataNfe1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV64Lote_DataNfe_To1, 2, "/");
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_PREVPAGAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV52Lote_PrevPagamento2) && (DateTime.MinValue==AV53Lote_PrevPagamento_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV52Lote_PrevPagamento2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV53Lote_PrevPagamento_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Lote_Numero2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Lote_Numero2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Lote_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Lote_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_NFE") == 0 ) && ! (0==AV65Lote_NFe2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV65Lote_NFe2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTE_DATANFE") == 0 ) && ! ( (DateTime.MinValue==AV66Lote_DataNfe2) && (DateTime.MinValue==AV67Lote_DataNfe_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV66Lote_DataNfe2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV67Lote_DataNfe_To2, 2, "/");
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_PREVPAGAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV54Lote_PrevPagamento3) && (DateTime.MinValue==AV55Lote_PrevPagamento_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV54Lote_PrevPagamento3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV55Lote_PrevPagamento_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Lote_Numero3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV26Lote_Numero3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Lote_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27Lote_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NFE") == 0 ) && ! (0==AV68Lote_NFe3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV68Lote_NFe3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_DATANFE") == 0 ) && ! ( (DateTime.MinValue==AV69Lote_DataNfe3) && (DateTime.MinValue==AV70Lote_DataNfe_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV69Lote_DataNfe3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV70Lote_DataNfe_To3, 2, "/");
            }
            if ( AV28DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV163Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Lote";
         AV32Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E26F52( )
      {
         /* Lote_QtdeDmn_Click Routine */
         AV71SDT_FiltroConsContadorFM.gxTpr_Lote = A596Lote_Codigo;
         AV71SDT_FiltroConsContadorFM.gxTpr_Nome = A563Lote_Nome;
         AV72WebSession.Set("FiltroConsultaContador", AV71SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_Meetrika"));
         Innewwindow1_Target = formatLink("wwcontagemresultado.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow1_Internalname, "Target", Innewwindow1_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOW1Container", "OpenWindow", "", new Object[] {});
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71SDT_FiltroConsContadorFM", AV71SDT_FiltroConsContadorFM);
      }

      protected void wb_table1_2_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_F52( true) ;
         }
         else
         {
            wb_table2_8_F52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_134_F52( true) ;
         }
         else
         {
            wb_table3_134_F52( false) ;
         }
         return  ;
      }

      protected void wb_table3_134_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_179_F52( true) ;
         }
         else
         {
            wb_table4_179_F52( false) ;
         }
         return  ;
      }

      protected void wb_table4_179_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_F52e( true) ;
         }
         else
         {
            wb_table1_2_F52e( false) ;
         }
      }

      protected void wb_table4_179_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOW1Container"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_179_F52e( true) ;
         }
         else
         {
            wb_table4_179_F52e( false) ;
         }
      }

      protected void wb_table3_134_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_137_F52( true) ;
         }
         else
         {
            wb_table5_137_F52( false) ;
         }
         return  ;
      }

      protected void wb_table5_137_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table6_159_F52( true) ;
         }
         else
         {
            wb_table6_159_F52( false) ;
         }
         return  ;
      }

      protected void wb_table6_159_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_134_F52e( true) ;
         }
         else
         {
            wb_table3_134_F52e( false) ;
         }
      }

      protected void wb_table6_159_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbltotais_Internalname, tblTbltotais_Internalname, "", "Table", 0, "", "", 10, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockquantidade_Internalname, "Lotes:", "", "", lblTextblockquantidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33Quantidade), 3, 0, ",", "")), ((edtavQuantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33Quantidade), "ZZ9")) : context.localUtil.Format( (decimal)(AV33Quantidade), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,164);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQuantidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavQuantidade_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockqtddmn_Internalname, "Dmn:", "", "", lblTextblockqtddmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 168,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtddmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34QtdDmn), 3, 0, ",", "")), ((edtavQtddmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34QtdDmn), "ZZ9")) : context.localUtil.Format( (decimal)(AV34QtdDmn), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,168);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtddmn_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavQtddmn_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockqtdpf_Internalname, "PF:", "", "", lblTextblockqtdpf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV35QtdPF, 14, 5, ",", "")), ((edtavQtdpf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV35QtdPF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV35QtdPF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,172);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdpf_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavQtdpf_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockvalortotal_Internalname, "R$:", "", "", lblTextblockvalortotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavValortotal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV36ValorTotal, 18, 5, ",", "")), ((edtavValortotal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV36ValorTotal, "ZZ,ZZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV36ValorTotal, "ZZ,ZZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,176);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavValortotal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavValortotal_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor3Casas", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_159_F52e( true) ;
         }
         else
         {
            wb_table6_159_F52e( false) ;
         }
      }

      protected void wb_table5_137_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"140\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Lote_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_NFe_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_NFe_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_NFe_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_DataNfe_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_DataNfe_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_DataNfe_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_PrevPagamento_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_PrevPagamento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_PrevPagamento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAction_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavAction_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_ValorPF_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_ValorPF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_ValorPF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "R$") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A562Lote_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A563Lote_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtLote_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_NFe_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_NFe_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_DataNfe_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_DataNfe_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_PrevPagamento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_PrevPagamento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV61Action));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAction_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAction_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAction_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_ValorPF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_ValorPF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A569Lote_QtdeDmn), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A573Lote_QtdePF, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A575Lote_Status));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 140 )
         {
            wbEnd = 0;
            nRC_GXsfl_140 = (short)(nGXsfl_140_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_137_F52e( true) ;
         }
         else
         {
            wb_table5_137_F52e( false) ;
         }
      }

      protected void wb_table2_8_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLotetitle_Internalname, "Lotes a Pagar", "", "", lblLotetitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_13_F52( true) ;
         }
         else
         {
            wb_table7_13_F52( false) ;
         }
         return  ;
      }

      protected void wb_table7_13_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_140_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_ExtraWWLoteAPagar.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table8_22_F52( true) ;
         }
         else
         {
            wb_table8_22_F52( false) ;
         }
         return  ;
      }

      protected void wb_table8_22_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_F52e( true) ;
         }
         else
         {
            wb_table2_8_F52e( false) ;
         }
      }

      protected void wb_table8_22_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextlote_status_Internalname, "Status", "", "", lblFiltertextlote_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_140_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavLote_status, cmbavLote_status_Internalname, StringUtil.RTrim( AV16Lote_Status), 1, cmbavLote_status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_ExtraWWLoteAPagar.htm");
            cmbavLote_status.CurrentValue = StringUtil.RTrim( AV16Lote_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLote_status_Internalname, "Values", (String)(cmbavLote_status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextlote_areatrabalhocod_Internalname, "Area de Trabalho", "", "", lblFiltertextlote_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_140_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavLote_areatrabalhocod, dynavLote_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0)), 1, dynavLote_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_ExtraWWLoteAPagar.htm");
            dynavLote_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Lote_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavLote_areatrabalhocod_Internalname, "Values", (String)(dynavLote_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextlote_contratadacod_Internalname, "", "", "", lblFiltertextlote_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_contratadacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73Lote_ContratadaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV73Lote_ContratadaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_contratadacod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_39_F52( true) ;
         }
         else
         {
            wb_table9_39_F52( false) ;
         }
         return  ;
      }

      protected void wb_table9_39_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_search_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(140), 3, 0)+","+"null"+");", "Procurar", bttBtn_search_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_22_F52e( true) ;
         }
         else
         {
            wb_table8_22_F52e( false) ;
         }
      }

      protected void wb_table9_39_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_140_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV17DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_ExtraWWLoteAPagar.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_48_F52( true) ;
         }
         else
         {
            wb_table10_48_F52( false) ;
         }
         return  ;
      }

      protected void wb_table10_48_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_numero1_Internalname, StringUtil.RTrim( AV18Lote_Numero1), StringUtil.RTrim( context.localUtil.Format( AV18Lote_Numero1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_numero1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteAPagar.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nome1_Internalname, StringUtil.RTrim( AV19Lote_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19Lote_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteAPagar.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62Lote_NFe1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62Lote_NFe1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nfe1_Visible, 1, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            wb_table11_59_F52( true) ;
         }
         else
         {
            wb_table11_59_F52( false) ;
         }
         return  ;
      }

      protected void wb_table11_59_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_140_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_ExtraWWLoteAPagar.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_78_F52( true) ;
         }
         else
         {
            wb_table12_78_F52( false) ;
         }
         return  ;
      }

      protected void wb_table12_78_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_numero2_Internalname, StringUtil.RTrim( AV22Lote_Numero2), StringUtil.RTrim( context.localUtil.Format( AV22Lote_Numero2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_numero2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteAPagar.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nome2_Internalname, StringUtil.RTrim( AV23Lote_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Lote_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteAPagar.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65Lote_NFe2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65Lote_NFe2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nfe2_Visible, 1, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            wb_table13_89_F52( true) ;
         }
         else
         {
            wb_table13_89_F52( false) ;
         }
         return  ;
      }

      protected void wb_table13_89_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_140_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "", true, "HLP_ExtraWWLoteAPagar.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table14_108_F52( true) ;
         }
         else
         {
            wb_table14_108_F52( false) ;
         }
         return  ;
      }

      protected void wb_table14_108_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_numero3_Internalname, StringUtil.RTrim( AV26Lote_Numero3), StringUtil.RTrim( context.localUtil.Format( AV26Lote_Numero3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_numero3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteAPagar.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nome3_Internalname, StringUtil.RTrim( AV27Lote_Nome3), StringUtil.RTrim( context.localUtil.Format( AV27Lote_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWLoteAPagar.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_140_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nfe3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68Lote_NFe3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV68Lote_NFe3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nfe3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nfe3_Visible, 1, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            wb_table15_119_F52( true) ;
         }
         else
         {
            wb_table15_119_F52( false) ;
         }
         return  ;
      }

      protected void wb_table15_119_F52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_39_F52e( true) ;
         }
         else
         {
            wb_table9_39_F52e( false) ;
         }
      }

      protected void wb_table15_119_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_datanfe3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_datanfe3_Internalname, tblTablemergeddynamicfilterslote_datanfe3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe3_Internalname, context.localUtil.Format(AV69Lote_DataNfe3, "99/99/99"), context.localUtil.Format( AV69Lote_DataNfe3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_datanfe_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterslote_datanfe_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe_to3_Internalname, context.localUtil.Format(AV70Lote_DataNfe_To3, "99/99/99"), context.localUtil.Format( AV70Lote_DataNfe_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_119_F52e( true) ;
         }
         else
         {
            wb_table15_119_F52e( false) ;
         }
      }

      protected void wb_table14_108_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_prevpagamento3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, tblTablemergeddynamicfilterslote_prevpagamento3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento3_Internalname, context.localUtil.Format(AV54Lote_PrevPagamento3, "99/99/99"), context.localUtil.Format( AV54Lote_PrevPagamento3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_prevpagamento_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterslote_prevpagamento_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento_to3_Internalname, context.localUtil.Format(AV55Lote_PrevPagamento_To3, "99/99/99"), context.localUtil.Format( AV55Lote_PrevPagamento_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_108_F52e( true) ;
         }
         else
         {
            wb_table14_108_F52e( false) ;
         }
      }

      protected void wb_table13_89_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_datanfe2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_datanfe2_Internalname, tblTablemergeddynamicfilterslote_datanfe2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe2_Internalname, context.localUtil.Format(AV66Lote_DataNfe2, "99/99/99"), context.localUtil.Format( AV66Lote_DataNfe2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_datanfe_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterslote_datanfe_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe_to2_Internalname, context.localUtil.Format(AV67Lote_DataNfe_To2, "99/99/99"), context.localUtil.Format( AV67Lote_DataNfe_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_89_F52e( true) ;
         }
         else
         {
            wb_table13_89_F52e( false) ;
         }
      }

      protected void wb_table12_78_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_prevpagamento2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, tblTablemergeddynamicfilterslote_prevpagamento2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento2_Internalname, context.localUtil.Format(AV52Lote_PrevPagamento2, "99/99/99"), context.localUtil.Format( AV52Lote_PrevPagamento2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_prevpagamento_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterslote_prevpagamento_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento_to2_Internalname, context.localUtil.Format(AV53Lote_PrevPagamento_To2, "99/99/99"), context.localUtil.Format( AV53Lote_PrevPagamento_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_78_F52e( true) ;
         }
         else
         {
            wb_table12_78_F52e( false) ;
         }
      }

      protected void wb_table11_59_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_datanfe1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_datanfe1_Internalname, tblTablemergeddynamicfilterslote_datanfe1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe1_Internalname, context.localUtil.Format(AV63Lote_DataNfe1, "99/99/99"), context.localUtil.Format( AV63Lote_DataNfe1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_datanfe_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterslote_datanfe_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_datanfe_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_datanfe_to1_Internalname, context.localUtil.Format(AV64Lote_DataNfe_To1, "99/99/99"), context.localUtil.Format( AV64Lote_DataNfe_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_datanfe_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_datanfe_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_59_F52e( true) ;
         }
         else
         {
            wb_table11_59_F52e( false) ;
         }
      }

      protected void wb_table10_48_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterslote_prevpagamento1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, tblTablemergeddynamicfilterslote_prevpagamento1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento1_Internalname, context.localUtil.Format(AV50Lote_PrevPagamento1, "99/99/99"), context.localUtil.Format( AV50Lote_PrevPagamento1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterslote_prevpagamento_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterslote_prevpagamento_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_140_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavLote_prevpagamento_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavLote_prevpagamento_to1_Internalname, context.localUtil.Format(AV51Lote_PrevPagamento_To1, "99/99/99"), context.localUtil.Format( AV51Lote_PrevPagamento_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_prevpagamento_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWLoteAPagar.htm");
            GxWebStd.gx_bitmap( context, edtavLote_prevpagamento_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWLoteAPagar.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_48_F52e( true) ;
         }
         else
         {
            wb_table10_48_F52e( false) ;
         }
      }

      protected void wb_table7_13_F52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_13_F52e( true) ;
         }
         else
         {
            wb_table7_13_F52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAF52( ) ;
         WSF52( ) ;
         WEF52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423454990");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("extrawwloteapagar.js", "?202032423454991");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1402( )
      {
         edtLote_Codigo_Internalname = "LOTE_CODIGO_"+sGXsfl_140_idx;
         edtLote_Numero_Internalname = "LOTE_NUMERO_"+sGXsfl_140_idx;
         edtLote_Nome_Internalname = "LOTE_NOME_"+sGXsfl_140_idx;
         edtLote_Data_Internalname = "LOTE_DATA_"+sGXsfl_140_idx;
         edtLote_NFe_Internalname = "LOTE_NFE_"+sGXsfl_140_idx;
         edtLote_DataNfe_Internalname = "LOTE_DATANFE_"+sGXsfl_140_idx;
         edtLote_PrevPagamento_Internalname = "LOTE_PREVPAGAMENTO_"+sGXsfl_140_idx;
         edtavAction_Internalname = "vACTION_"+sGXsfl_140_idx;
         edtLote_ValorPF_Internalname = "LOTE_VALORPF_"+sGXsfl_140_idx;
         edtLote_QtdeDmn_Internalname = "LOTE_QTDEDMN_"+sGXsfl_140_idx;
         edtLote_QtdePF_Internalname = "LOTE_QTDEPF_"+sGXsfl_140_idx;
         cmbLote_Status_Internalname = "LOTE_STATUS_"+sGXsfl_140_idx;
         edtLote_Valor_Internalname = "LOTE_VALOR_"+sGXsfl_140_idx;
      }

      protected void SubsflControlProps_fel_1402( )
      {
         edtLote_Codigo_Internalname = "LOTE_CODIGO_"+sGXsfl_140_fel_idx;
         edtLote_Numero_Internalname = "LOTE_NUMERO_"+sGXsfl_140_fel_idx;
         edtLote_Nome_Internalname = "LOTE_NOME_"+sGXsfl_140_fel_idx;
         edtLote_Data_Internalname = "LOTE_DATA_"+sGXsfl_140_fel_idx;
         edtLote_NFe_Internalname = "LOTE_NFE_"+sGXsfl_140_fel_idx;
         edtLote_DataNfe_Internalname = "LOTE_DATANFE_"+sGXsfl_140_fel_idx;
         edtLote_PrevPagamento_Internalname = "LOTE_PREVPAGAMENTO_"+sGXsfl_140_fel_idx;
         edtavAction_Internalname = "vACTION_"+sGXsfl_140_fel_idx;
         edtLote_ValorPF_Internalname = "LOTE_VALORPF_"+sGXsfl_140_fel_idx;
         edtLote_QtdeDmn_Internalname = "LOTE_QTDEDMN_"+sGXsfl_140_fel_idx;
         edtLote_QtdePF_Internalname = "LOTE_QTDEPF_"+sGXsfl_140_fel_idx;
         cmbLote_Status_Internalname = "LOTE_STATUS_"+sGXsfl_140_fel_idx;
         edtLote_Valor_Internalname = "LOTE_VALOR_"+sGXsfl_140_fel_idx;
      }

      protected void sendrow_1402( )
      {
         SubsflControlProps_1402( ) ;
         WBF50( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_140_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_140_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_140_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Numero_Internalname,StringUtil.RTrim( A562Lote_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Nome_Internalname,StringUtil.RTrim( A563Lote_Nome),StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtLote_Nome_Link,(String)"",(String)"",(String)"",(String)edtLote_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Data_Internalname,context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_NFe_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_NFe_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_DataNfe_Internalname,context.localUtil.Format(A674Lote_DataNfe, "99/99/99"),context.localUtil.Format( A674Lote_DataNfe, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_DataNfe_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_PrevPagamento_Internalname,context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"),context.localUtil.Format( A857Lote_PrevPagamento, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_PrevPagamento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAction_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAction_Enabled!=0)&&(edtavAction_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 148,'',false,'',140)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV61Action_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV61Action))&&String.IsNullOrEmpty(StringUtil.RTrim( AV162Action_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV61Action)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAction_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV61Action)) ? AV162Action_GXI : context.PathToRelativeUrl( AV61Action)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAction_Visible,(short)1,(String)"",(String)edtavAction_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavAction_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOACTION\\'."+sGXsfl_140_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV61Action_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_ValorPF_Internalname,StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ",", "")),context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_ValorPF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_QtdeDmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A569Lote_QtdeDmn), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A569Lote_QtdeDmn), "ZZZ9"),(String)"","'"+""+"'"+",false,"+"'"+"ELOTE_QTDEDMN.CLICK."+sGXsfl_140_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_QtdeDmn_Jsonclick,(short)5,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_QtdePF_Internalname,StringUtil.LTrim( StringUtil.NToC( A573Lote_QtdePF, 14, 5, ",", "")),context.localUtil.Format( A573Lote_QtdePF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_QtdePF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "LOTE_STATUS_" + sGXsfl_140_idx;
            cmbLote_Status.Name = GXCCtl;
            cmbLote_Status.WebTags = "";
            cmbLote_Status.addItem("B", "Stand by", 0);
            cmbLote_Status.addItem("S", "Solicitada", 0);
            cmbLote_Status.addItem("E", "Em An�lise", 0);
            cmbLote_Status.addItem("A", "Em execu��o", 0);
            cmbLote_Status.addItem("R", "Resolvida", 0);
            cmbLote_Status.addItem("C", "Conferida", 0);
            cmbLote_Status.addItem("D", "Rejeitada", 0);
            cmbLote_Status.addItem("H", "Homologada", 0);
            cmbLote_Status.addItem("O", "Aceite", 0);
            cmbLote_Status.addItem("P", "A Pagar", 0);
            cmbLote_Status.addItem("L", "Liquidada", 0);
            cmbLote_Status.addItem("X", "Cancelada", 0);
            cmbLote_Status.addItem("N", "N�o Faturada", 0);
            cmbLote_Status.addItem("J", "Planejamento", 0);
            cmbLote_Status.addItem("I", "An�lise Planejamento", 0);
            cmbLote_Status.addItem("T", "Validacao T�cnica", 0);
            cmbLote_Status.addItem("Q", "Validacao Qualidade", 0);
            cmbLote_Status.addItem("G", "Em Homologa��o", 0);
            cmbLote_Status.addItem("M", "Valida��o Mensura��o", 0);
            cmbLote_Status.addItem("U", "Rascunho", 0);
            if ( cmbLote_Status.ItemCount > 0 )
            {
               A575Lote_Status = cmbLote_Status.getValidValue(A575Lote_Status);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbLote_Status,(String)cmbLote_Status_Internalname,StringUtil.RTrim( A575Lote_Status),(short)1,(String)cmbLote_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbLote_Status.CurrentValue = StringUtil.RTrim( A575Lote_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLote_Status_Internalname, "Values", (String)(cmbLote_Status.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ",", "")),context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)140,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor3Casas",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NUMERO"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NOME"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATA"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NFE"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATANFE"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, A674Lote_DataNfe));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_PREVPAGAMENTO"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, A857Lote_PrevPagamento));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALORPF"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_QTDEPF"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, context.localUtil.Format( A573Lote_QtdePF, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALOR"+"_"+sGXsfl_140_idx, GetSecureSignedToken( sGXsfl_140_idx, context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_140_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_140_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_140_idx+1));
            sGXsfl_140_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_140_idx), 4, 0)), 4, "0");
            SubsflControlProps_1402( ) ;
         }
         /* End function sendrow_1402 */
      }

      protected void init_default_properties( )
      {
         lblLotetitle_Internalname = "LOTETITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextlote_status_Internalname = "FILTERTEXTLOTE_STATUS";
         cmbavLote_status_Internalname = "vLOTE_STATUS";
         lblFiltertextlote_areatrabalhocod_Internalname = "FILTERTEXTLOTE_AREATRABALHOCOD";
         dynavLote_areatrabalhocod_Internalname = "vLOTE_AREATRABALHOCOD";
         lblFiltertextlote_contratadacod_Internalname = "FILTERTEXTLOTE_CONTRATADACOD";
         edtavLote_contratadacod_Internalname = "vLOTE_CONTRATADACOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavLote_prevpagamento1_Internalname = "vLOTE_PREVPAGAMENTO1";
         lblDynamicfilterslote_prevpagamento_rangemiddletext1_Internalname = "DYNAMICFILTERSLOTE_PREVPAGAMENTO_RANGEMIDDLETEXT1";
         edtavLote_prevpagamento_to1_Internalname = "vLOTE_PREVPAGAMENTO_TO1";
         tblTablemergeddynamicfilterslote_prevpagamento1_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1";
         edtavLote_numero1_Internalname = "vLOTE_NUMERO1";
         edtavLote_nome1_Internalname = "vLOTE_NOME1";
         edtavLote_nfe1_Internalname = "vLOTE_NFE1";
         edtavLote_datanfe1_Internalname = "vLOTE_DATANFE1";
         lblDynamicfilterslote_datanfe_rangemiddletext1_Internalname = "DYNAMICFILTERSLOTE_DATANFE_RANGEMIDDLETEXT1";
         edtavLote_datanfe_to1_Internalname = "vLOTE_DATANFE_TO1";
         tblTablemergeddynamicfilterslote_datanfe1_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavLote_prevpagamento2_Internalname = "vLOTE_PREVPAGAMENTO2";
         lblDynamicfilterslote_prevpagamento_rangemiddletext2_Internalname = "DYNAMICFILTERSLOTE_PREVPAGAMENTO_RANGEMIDDLETEXT2";
         edtavLote_prevpagamento_to2_Internalname = "vLOTE_PREVPAGAMENTO_TO2";
         tblTablemergeddynamicfilterslote_prevpagamento2_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2";
         edtavLote_numero2_Internalname = "vLOTE_NUMERO2";
         edtavLote_nome2_Internalname = "vLOTE_NOME2";
         edtavLote_nfe2_Internalname = "vLOTE_NFE2";
         edtavLote_datanfe2_Internalname = "vLOTE_DATANFE2";
         lblDynamicfilterslote_datanfe_rangemiddletext2_Internalname = "DYNAMICFILTERSLOTE_DATANFE_RANGEMIDDLETEXT2";
         edtavLote_datanfe_to2_Internalname = "vLOTE_DATANFE_TO2";
         tblTablemergeddynamicfilterslote_datanfe2_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavLote_prevpagamento3_Internalname = "vLOTE_PREVPAGAMENTO3";
         lblDynamicfilterslote_prevpagamento_rangemiddletext3_Internalname = "DYNAMICFILTERSLOTE_PREVPAGAMENTO_RANGEMIDDLETEXT3";
         edtavLote_prevpagamento_to3_Internalname = "vLOTE_PREVPAGAMENTO_TO3";
         tblTablemergeddynamicfilterslote_prevpagamento3_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3";
         edtavLote_numero3_Internalname = "vLOTE_NUMERO3";
         edtavLote_nome3_Internalname = "vLOTE_NOME3";
         edtavLote_nfe3_Internalname = "vLOTE_NFE3";
         edtavLote_datanfe3_Internalname = "vLOTE_DATANFE3";
         lblDynamicfilterslote_datanfe_rangemiddletext3_Internalname = "DYNAMICFILTERSLOTE_DATANFE_RANGEMIDDLETEXT3";
         edtavLote_datanfe_to3_Internalname = "vLOTE_DATANFE_TO3";
         tblTablemergeddynamicfilterslote_datanfe3_Internalname = "TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtn_search_Internalname = "BTN_SEARCH";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtLote_Codigo_Internalname = "LOTE_CODIGO";
         edtLote_Numero_Internalname = "LOTE_NUMERO";
         edtLote_Nome_Internalname = "LOTE_NOME";
         edtLote_Data_Internalname = "LOTE_DATA";
         edtLote_NFe_Internalname = "LOTE_NFE";
         edtLote_DataNfe_Internalname = "LOTE_DATANFE";
         edtLote_PrevPagamento_Internalname = "LOTE_PREVPAGAMENTO";
         edtavAction_Internalname = "vACTION";
         edtLote_ValorPF_Internalname = "LOTE_VALORPF";
         edtLote_QtdeDmn_Internalname = "LOTE_QTDEDMN";
         edtLote_QtdePF_Internalname = "LOTE_QTDEPF";
         cmbLote_Status_Internalname = "LOTE_STATUS";
         edtLote_Valor_Internalname = "LOTE_VALOR";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         lblTextblockquantidade_Internalname = "TEXTBLOCKQUANTIDADE";
         edtavQuantidade_Internalname = "vQUANTIDADE";
         lblTextblockqtddmn_Internalname = "TEXTBLOCKQTDDMN";
         edtavQtddmn_Internalname = "vQTDDMN";
         lblTextblockqtdpf_Internalname = "TEXTBLOCKQTDPF";
         edtavQtdpf_Internalname = "vQTDPF";
         lblTextblockvalortotal_Internalname = "TEXTBLOCKVALORTOTAL";
         edtavValortotal_Internalname = "vVALORTOTAL";
         tblTbltotais_Internalname = "TBLTOTAIS";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         Innewwindow1_Internalname = "INNEWWINDOW1";
         tblUsertable_Internalname = "USERTABLE";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtLote_Valor_Jsonclick = "";
         cmbLote_Status_Jsonclick = "";
         edtLote_QtdePF_Jsonclick = "";
         edtLote_QtdeDmn_Jsonclick = "";
         edtLote_ValorPF_Jsonclick = "";
         edtavAction_Jsonclick = "";
         edtavAction_Enabled = 1;
         edtLote_PrevPagamento_Jsonclick = "";
         edtLote_DataNfe_Jsonclick = "";
         edtLote_NFe_Jsonclick = "";
         edtLote_Data_Jsonclick = "";
         edtLote_Nome_Jsonclick = "";
         edtLote_Numero_Jsonclick = "";
         edtLote_Codigo_Jsonclick = "";
         edtavLote_prevpagamento_to1_Jsonclick = "";
         edtavLote_prevpagamento1_Jsonclick = "";
         edtavLote_datanfe_to1_Jsonclick = "";
         edtavLote_datanfe1_Jsonclick = "";
         edtavLote_prevpagamento_to2_Jsonclick = "";
         edtavLote_prevpagamento2_Jsonclick = "";
         edtavLote_datanfe_to2_Jsonclick = "";
         edtavLote_datanfe2_Jsonclick = "";
         edtavLote_prevpagamento_to3_Jsonclick = "";
         edtavLote_prevpagamento3_Jsonclick = "";
         edtavLote_datanfe_to3_Jsonclick = "";
         edtavLote_datanfe3_Jsonclick = "";
         edtavLote_nfe3_Jsonclick = "";
         edtavLote_nome3_Jsonclick = "";
         edtavLote_numero3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavLote_nfe2_Jsonclick = "";
         edtavLote_nome2_Jsonclick = "";
         edtavLote_numero2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavLote_nfe1_Jsonclick = "";
         edtavLote_nome1_Jsonclick = "";
         edtavLote_numero1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavLote_contratadacod_Jsonclick = "";
         dynavLote_areatrabalhocod_Jsonclick = "";
         cmbavLote_status_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAction_Tooltiptext = "Incluir pagamento e marcar como Liquidado";
         edtLote_Nome_Link = "";
         edtLote_ValorPF_Titleformat = 0;
         edtLote_PrevPagamento_Titleformat = 0;
         edtLote_DataNfe_Titleformat = 0;
         edtLote_NFe_Titleformat = 0;
         edtLote_Data_Titleformat = 0;
         edtLote_Nome_Titleformat = 0;
         edtLote_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavValortotal_Jsonclick = "";
         edtavValortotal_Enabled = 1;
         edtavQtdpf_Jsonclick = "";
         edtavQtdpf_Enabled = 1;
         edtavQtddmn_Jsonclick = "";
         edtavQtddmn_Enabled = 1;
         edtavQuantidade_Jsonclick = "";
         edtavQuantidade_Enabled = 1;
         tblTablemergeddynamicfilterslote_datanfe3_Visible = 1;
         edtavLote_nfe3_Visible = 1;
         edtavLote_nome3_Visible = 1;
         edtavLote_numero3_Visible = 1;
         tblTablemergeddynamicfilterslote_prevpagamento3_Visible = 1;
         tblTablemergeddynamicfilterslote_datanfe2_Visible = 1;
         edtavLote_nfe2_Visible = 1;
         edtavLote_nome2_Visible = 1;
         edtavLote_numero2_Visible = 1;
         tblTablemergeddynamicfilterslote_prevpagamento2_Visible = 1;
         tblTablemergeddynamicfilterslote_datanfe1_Visible = 1;
         edtavLote_nfe1_Visible = 1;
         edtavLote_nome1_Visible = 1;
         edtavLote_numero1_Visible = 1;
         tblTablemergeddynamicfilterslote_prevpagamento1_Visible = 1;
         edtavAction_Visible = -1;
         edtavAction_Title = "";
         edtLote_ValorPF_Title = "PF R$";
         edtLote_PrevPagamento_Title = "Previs�o";
         edtLote_DataNfe_Title = "Emiss�o";
         edtLote_NFe_Title = "NFe";
         edtLote_Data_Title = "Data do Lote";
         edtLote_Nome_Title = "Nome";
         edtLote_Numero_Title = "Lote";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Innewwindow1_Target = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Lote";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLote_Numero_Titleformat',ctrl:'LOTE_NUMERO',prop:'Titleformat'},{av:'edtLote_Numero_Title',ctrl:'LOTE_NUMERO',prop:'Title'},{av:'edtLote_Nome_Titleformat',ctrl:'LOTE_NOME',prop:'Titleformat'},{av:'edtLote_Nome_Title',ctrl:'LOTE_NOME',prop:'Title'},{av:'edtLote_Data_Titleformat',ctrl:'LOTE_DATA',prop:'Titleformat'},{av:'edtLote_Data_Title',ctrl:'LOTE_DATA',prop:'Title'},{av:'edtLote_NFe_Titleformat',ctrl:'LOTE_NFE',prop:'Titleformat'},{av:'edtLote_NFe_Title',ctrl:'LOTE_NFE',prop:'Title'},{av:'edtLote_DataNfe_Titleformat',ctrl:'LOTE_DATANFE',prop:'Titleformat'},{av:'edtLote_DataNfe_Title',ctrl:'LOTE_DATANFE',prop:'Title'},{av:'edtLote_PrevPagamento_Titleformat',ctrl:'LOTE_PREVPAGAMENTO',prop:'Titleformat'},{av:'edtLote_PrevPagamento_Title',ctrl:'LOTE_PREVPAGAMENTO',prop:'Title'},{av:'edtLote_ValorPF_Titleformat',ctrl:'LOTE_VALORPF',prop:'Titleformat'},{av:'edtLote_ValorPF_Title',ctrl:'LOTE_VALORPF',prop:'Title'},{av:'AV128GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV129GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavAction_Title',ctrl:'vACTION',prop:'Title'},{av:'edtavAction_Visible',ctrl:'vACTION',prop:'Visible'},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11F52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24F52',iparms:[{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'AV61Action',fld:'vACTION',pic:'',nv:''},{av:'edtavAction_Tooltiptext',ctrl:'vACTION',prop:'Tooltiptext'},{av:'edtLote_Nome_Link',ctrl:'LOTE_NOME',prop:'Link'},{av:'edtavAction_Visible',ctrl:'vACTION',prop:'Visible'},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E13F52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18F52',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E14F52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19F52',iparms:[{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20F52',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E15F52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21F52',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E16F52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E22F52',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E17F52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterslote_prevpagamento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO1',prop:'Visible'},{av:'edtavLote_numero1_Visible',ctrl:'vLOTE_NUMERO1',prop:'Visible'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_nfe1_Visible',ctrl:'vLOTE_NFE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE1',prop:'Visible'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterslote_prevpagamento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO2',prop:'Visible'},{av:'edtavLote_numero2_Visible',ctrl:'vLOTE_NUMERO2',prop:'Visible'},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_nfe2_Visible',ctrl:'vLOTE_NFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_prevpagamento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_PREVPAGAMENTO3',prop:'Visible'},{av:'edtavLote_numero3_Visible',ctrl:'vLOTE_NUMERO3',prop:'Visible'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_nfe3_Visible',ctrl:'vLOTE_NFE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterslote_datanfe3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSLOTE_DATANFE3',prop:'Visible'}]}");
         setEventMetadata("'DOACTION'","{handler:'E25F52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV50Lote_PrevPagamento1',fld:'vLOTE_PREVPAGAMENTO1',pic:'',nv:''},{av:'AV51Lote_PrevPagamento_To1',fld:'vLOTE_PREVPAGAMENTO_TO1',pic:'',nv:''},{av:'AV18Lote_Numero1',fld:'vLOTE_NUMERO1',pic:'',nv:''},{av:'AV19Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV62Lote_NFe1',fld:'vLOTE_NFE1',pic:'ZZZZZ9',nv:0},{av:'AV63Lote_DataNfe1',fld:'vLOTE_DATANFE1',pic:'',nv:''},{av:'AV64Lote_DataNfe_To1',fld:'vLOTE_DATANFE_TO1',pic:'',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV52Lote_PrevPagamento2',fld:'vLOTE_PREVPAGAMENTO2',pic:'',nv:''},{av:'AV53Lote_PrevPagamento_To2',fld:'vLOTE_PREVPAGAMENTO_TO2',pic:'',nv:''},{av:'AV22Lote_Numero2',fld:'vLOTE_NUMERO2',pic:'',nv:''},{av:'AV23Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV65Lote_NFe2',fld:'vLOTE_NFE2',pic:'ZZZZZ9',nv:0},{av:'AV66Lote_DataNfe2',fld:'vLOTE_DATANFE2',pic:'',nv:''},{av:'AV67Lote_DataNfe_To2',fld:'vLOTE_DATANFE_TO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV54Lote_PrevPagamento3',fld:'vLOTE_PREVPAGAMENTO3',pic:'',nv:''},{av:'AV55Lote_PrevPagamento_To3',fld:'vLOTE_PREVPAGAMENTO_TO3',pic:'',nv:''},{av:'AV26Lote_Numero3',fld:'vLOTE_NUMERO3',pic:'',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV68Lote_NFe3',fld:'vLOTE_NFE3',pic:'ZZZZZ9',nv:0},{av:'AV69Lote_DataNfe3',fld:'vLOTE_DATANFE3',pic:'',nv:''},{av:'AV70Lote_DataNfe_To3',fld:'vLOTE_DATANFE_TO3',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV16Lote_Status',fld:'vLOTE_STATUS',pic:'',nv:''},{av:'AV73Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV163Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV28DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A575Lote_Status',fld:'LOTE_STATUS',pic:'',nv:''},{av:'AV33Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV34QtdDmn',fld:'vQTDDMN',pic:'ZZ9',nv:0},{av:'A569Lote_QtdeDmn',fld:'LOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV35QtdPF',fld:'vQTDPF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A573Lote_QtdePF',fld:'LOTE_QTDEPF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV36ValorTotal',fld:'vVALORTOTAL',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A572Lote_Valor',fld:'LOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("LOTE_QTDEDMN.CLICK","{handler:'E26F52',iparms:[{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV71SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'A563Lote_Nome',fld:'LOTE_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV71SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'Innewwindow1_Target',ctrl:'INNEWWINDOW1',prop:'Target'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV17DynamicFiltersSelector1 = "";
         AV50Lote_PrevPagamento1 = DateTime.MinValue;
         AV51Lote_PrevPagamento_To1 = DateTime.MinValue;
         AV18Lote_Numero1 = "";
         AV19Lote_Nome1 = "";
         AV63Lote_DataNfe1 = DateTime.MinValue;
         AV64Lote_DataNfe_To1 = DateTime.MinValue;
         AV21DynamicFiltersSelector2 = "";
         AV52Lote_PrevPagamento2 = DateTime.MinValue;
         AV53Lote_PrevPagamento_To2 = DateTime.MinValue;
         AV22Lote_Numero2 = "";
         AV23Lote_Nome2 = "";
         AV66Lote_DataNfe2 = DateTime.MinValue;
         AV67Lote_DataNfe_To2 = DateTime.MinValue;
         AV25DynamicFiltersSelector3 = "";
         AV54Lote_PrevPagamento3 = DateTime.MinValue;
         AV55Lote_PrevPagamento_To3 = DateTime.MinValue;
         AV26Lote_Numero3 = "";
         AV27Lote_Nome3 = "";
         AV69Lote_DataNfe3 = DateTime.MinValue;
         AV70Lote_DataNfe_To3 = DateTime.MinValue;
         AV16Lote_Status = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV163Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A575Lote_Status = "";
         GXKey = "";
         scmdbuf = "";
         H00F52_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00F52_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00F52_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00F52_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00F52_A456ContagemResultado_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV71SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A674Lote_DataNfe = DateTime.MinValue;
         A857Lote_PrevPagamento = DateTime.MinValue;
         AV61Action = "";
         AV162Action_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00F53_A5AreaTrabalho_Codigo = new int[1] ;
         H00F53_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00F53_A72AreaTrabalho_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         lV139ExtraWWLoteAPagarDS_7_Lote_numero1 = "";
         lV140ExtraWWLoteAPagarDS_8_Lote_nome1 = "";
         lV148ExtraWWLoteAPagarDS_16_Lote_numero2 = "";
         lV149ExtraWWLoteAPagarDS_17_Lote_nome2 = "";
         lV157ExtraWWLoteAPagarDS_25_Lote_numero3 = "";
         lV158ExtraWWLoteAPagarDS_26_Lote_nome3 = "";
         AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 = "";
         AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 = DateTime.MinValue;
         AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 = DateTime.MinValue;
         AV139ExtraWWLoteAPagarDS_7_Lote_numero1 = "";
         AV140ExtraWWLoteAPagarDS_8_Lote_nome1 = "";
         AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 = DateTime.MinValue;
         AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 = DateTime.MinValue;
         AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 = "";
         AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 = DateTime.MinValue;
         AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 = DateTime.MinValue;
         AV148ExtraWWLoteAPagarDS_16_Lote_numero2 = "";
         AV149ExtraWWLoteAPagarDS_17_Lote_nome2 = "";
         AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 = DateTime.MinValue;
         AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 = DateTime.MinValue;
         AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 = "";
         AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 = DateTime.MinValue;
         AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 = DateTime.MinValue;
         AV157ExtraWWLoteAPagarDS_25_Lote_numero3 = "";
         AV158ExtraWWLoteAPagarDS_26_Lote_nome3 = "";
         AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 = DateTime.MinValue;
         AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 = DateTime.MinValue;
         H00F58_A595Lote_AreaTrabalhoCod = new int[1] ;
         H00F58_A565Lote_ValorPF = new decimal[1] ;
         H00F58_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         H00F58_n857Lote_PrevPagamento = new bool[] {false} ;
         H00F58_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         H00F58_n674Lote_DataNfe = new bool[] {false} ;
         H00F58_A673Lote_NFe = new int[1] ;
         H00F58_n673Lote_NFe = new bool[] {false} ;
         H00F58_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         H00F58_A563Lote_Nome = new String[] {""} ;
         H00F58_A562Lote_Numero = new String[] {""} ;
         H00F58_A596Lote_Codigo = new int[1] ;
         H00F58_A1231Lote_ContratadaCod = new int[1] ;
         H00F58_A575Lote_Status = new String[] {""} ;
         H00F58_A569Lote_QtdeDmn = new short[1] ;
         H00F58_A1057Lote_ValorGlosas = new decimal[1] ;
         H00F58_n1057Lote_ValorGlosas = new bool[] {false} ;
         AV133ExtraWWLoteAPagarDS_1_Lote_status = "";
         H00F513_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GridRow = new GXWebRow();
         AV32Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         AV72WebSession = context.GetSession();
         sStyleString = "";
         lblTextblockquantidade_Jsonclick = "";
         lblTextblockqtddmn_Jsonclick = "";
         lblTextblockqtdpf_Jsonclick = "";
         lblTextblockvalortotal_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblLotetitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextlote_status_Jsonclick = "";
         lblFiltertextlote_areatrabalhocod_Jsonclick = "";
         lblFiltertextlote_contratadacod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtn_search_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterslote_datanfe_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterslote_prevpagamento_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterslote_datanfe_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterslote_prevpagamento_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterslote_datanfe_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterslote_prevpagamento_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.extrawwloteapagar__default(),
            new Object[][] {
                new Object[] {
               H00F52_A597ContagemResultado_LoteAceiteCod, H00F52_n597ContagemResultado_LoteAceiteCod, H00F52_A512ContagemResultado_ValorPF, H00F52_n512ContagemResultado_ValorPF, H00F52_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00F53_A5AreaTrabalho_Codigo, H00F53_A6AreaTrabalho_Descricao, H00F53_A72AreaTrabalho_Ativo
               }
               , new Object[] {
               H00F58_A595Lote_AreaTrabalhoCod, H00F58_A565Lote_ValorPF, H00F58_A857Lote_PrevPagamento, H00F58_n857Lote_PrevPagamento, H00F58_A674Lote_DataNfe, H00F58_n674Lote_DataNfe, H00F58_A673Lote_NFe, H00F58_n673Lote_NFe, H00F58_A564Lote_Data, H00F58_A563Lote_Nome,
               H00F58_A562Lote_Numero, H00F58_A596Lote_Codigo, H00F58_A1231Lote_ContratadaCod, H00F58_A575Lote_Status, H00F58_A569Lote_QtdeDmn, H00F58_A1057Lote_ValorGlosas, H00F58_n1057Lote_ValorGlosas
               }
               , new Object[] {
               H00F513_AGRID_nRecordCount
               }
            }
         );
         AV163Pgmname = "ExtraWWLoteAPagar";
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         AV163Pgmname = "ExtraWWLoteAPagar";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         edtavQtddmn_Enabled = 0;
         edtavQtdpf_Enabled = 0;
         edtavValortotal_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_140 ;
      private short nGXsfl_140_idx=1 ;
      private short AV13OrderedBy ;
      private short AV33Quantidade ;
      private short AV34QtdDmn ;
      private short A569Lote_QtdeDmn ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_140_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtLote_Numero_Titleformat ;
      private short edtLote_Nome_Titleformat ;
      private short edtLote_Data_Titleformat ;
      private short edtLote_NFe_Titleformat ;
      private short edtLote_DataNfe_Titleformat ;
      private short edtLote_PrevPagamento_Titleformat ;
      private short edtLote_ValorPF_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV15Lote_AreaTrabalhoCod ;
      private int AV62Lote_NFe1 ;
      private int AV65Lote_NFe2 ;
      private int AV68Lote_NFe3 ;
      private int AV73Lote_ContratadaCod ;
      private int A596Lote_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A673Lote_NFe ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavQuantidade_Enabled ;
      private int edtavQtddmn_Enabled ;
      private int edtavQtdpf_Enabled ;
      private int edtavValortotal_Enabled ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV10GridState_gxTpr_Dynamicfilters_Count ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod ;
      private int AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 ;
      private int AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 ;
      private int AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A1231Lote_ContratadaCod ;
      private int AV135ExtraWWLoteAPagarDS_3_Lote_contratadacod ;
      private int edtavOrdereddsc_Visible ;
      private int edtavAction_Visible ;
      private int AV127PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterslote_prevpagamento1_Visible ;
      private int edtavLote_numero1_Visible ;
      private int edtavLote_nome1_Visible ;
      private int edtavLote_nfe1_Visible ;
      private int tblTablemergeddynamicfilterslote_datanfe1_Visible ;
      private int tblTablemergeddynamicfilterslote_prevpagamento2_Visible ;
      private int edtavLote_numero2_Visible ;
      private int edtavLote_nome2_Visible ;
      private int edtavLote_nfe2_Visible ;
      private int tblTablemergeddynamicfilterslote_datanfe2_Visible ;
      private int tblTablemergeddynamicfilterslote_prevpagamento3_Visible ;
      private int edtavLote_numero3_Visible ;
      private int edtavLote_nome3_Visible ;
      private int edtavLote_nfe3_Visible ;
      private int tblTablemergeddynamicfilterslote_datanfe3_Visible ;
      private int AV164GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavAction_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long AV128GridCurrentPage ;
      private long AV129GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV35QtdPF ;
      private decimal A573Lote_QtdePF ;
      private decimal AV36ValorTotal ;
      private decimal A572Lote_Valor ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A565Lote_ValorPF ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_140_idx="0001" ;
      private String AV18Lote_Numero1 ;
      private String AV19Lote_Nome1 ;
      private String AV22Lote_Numero2 ;
      private String AV23Lote_Nome2 ;
      private String AV26Lote_Numero3 ;
      private String AV27Lote_Nome3 ;
      private String AV16Lote_Status ;
      private String AV163Pgmname ;
      private String A575Lote_Status ;
      private String GXKey ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Innewwindow1_Target ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtLote_Codigo_Internalname ;
      private String A562Lote_Numero ;
      private String edtLote_Numero_Internalname ;
      private String A563Lote_Nome ;
      private String edtLote_Nome_Internalname ;
      private String edtLote_Data_Internalname ;
      private String edtLote_NFe_Internalname ;
      private String edtLote_DataNfe_Internalname ;
      private String edtLote_PrevPagamento_Internalname ;
      private String edtavAction_Internalname ;
      private String edtLote_ValorPF_Internalname ;
      private String edtLote_QtdeDmn_Internalname ;
      private String edtLote_QtdePF_Internalname ;
      private String cmbLote_Status_Internalname ;
      private String edtLote_Valor_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String edtavQuantidade_Internalname ;
      private String edtavQtddmn_Internalname ;
      private String edtavQtdpf_Internalname ;
      private String edtavValortotal_Internalname ;
      private String lV139ExtraWWLoteAPagarDS_7_Lote_numero1 ;
      private String lV140ExtraWWLoteAPagarDS_8_Lote_nome1 ;
      private String lV148ExtraWWLoteAPagarDS_16_Lote_numero2 ;
      private String lV149ExtraWWLoteAPagarDS_17_Lote_nome2 ;
      private String lV157ExtraWWLoteAPagarDS_25_Lote_numero3 ;
      private String lV158ExtraWWLoteAPagarDS_26_Lote_nome3 ;
      private String AV139ExtraWWLoteAPagarDS_7_Lote_numero1 ;
      private String AV140ExtraWWLoteAPagarDS_8_Lote_nome1 ;
      private String AV148ExtraWWLoteAPagarDS_16_Lote_numero2 ;
      private String AV149ExtraWWLoteAPagarDS_17_Lote_nome2 ;
      private String AV157ExtraWWLoteAPagarDS_25_Lote_numero3 ;
      private String AV158ExtraWWLoteAPagarDS_26_Lote_nome3 ;
      private String AV133ExtraWWLoteAPagarDS_1_Lote_status ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavLote_status_Internalname ;
      private String dynavLote_areatrabalhocod_Internalname ;
      private String edtavLote_contratadacod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavLote_prevpagamento1_Internalname ;
      private String edtavLote_prevpagamento_to1_Internalname ;
      private String edtavLote_numero1_Internalname ;
      private String edtavLote_nome1_Internalname ;
      private String edtavLote_nfe1_Internalname ;
      private String edtavLote_datanfe1_Internalname ;
      private String edtavLote_datanfe_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavLote_prevpagamento2_Internalname ;
      private String edtavLote_prevpagamento_to2_Internalname ;
      private String edtavLote_numero2_Internalname ;
      private String edtavLote_nome2_Internalname ;
      private String edtavLote_nfe2_Internalname ;
      private String edtavLote_datanfe2_Internalname ;
      private String edtavLote_datanfe_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavLote_prevpagamento3_Internalname ;
      private String edtavLote_prevpagamento_to3_Internalname ;
      private String edtavLote_numero3_Internalname ;
      private String edtavLote_nome3_Internalname ;
      private String edtavLote_nfe3_Internalname ;
      private String edtavLote_datanfe3_Internalname ;
      private String edtavLote_datanfe_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtLote_Numero_Title ;
      private String edtLote_Nome_Title ;
      private String edtLote_Data_Title ;
      private String edtLote_NFe_Title ;
      private String edtLote_DataNfe_Title ;
      private String edtLote_PrevPagamento_Title ;
      private String edtLote_ValorPF_Title ;
      private String edtavAction_Title ;
      private String edtavAction_Tooltiptext ;
      private String edtLote_Nome_Link ;
      private String tblTablemergeddynamicfilterslote_prevpagamento1_Internalname ;
      private String tblTablemergeddynamicfilterslote_datanfe1_Internalname ;
      private String tblTablemergeddynamicfilterslote_prevpagamento2_Internalname ;
      private String tblTablemergeddynamicfilterslote_datanfe2_Internalname ;
      private String tblTablemergeddynamicfilterslote_prevpagamento3_Internalname ;
      private String tblTablemergeddynamicfilterslote_datanfe3_Internalname ;
      private String Innewwindow1_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblUsertable_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblTbltotais_Internalname ;
      private String lblTextblockquantidade_Internalname ;
      private String lblTextblockquantidade_Jsonclick ;
      private String edtavQuantidade_Jsonclick ;
      private String lblTextblockqtddmn_Internalname ;
      private String lblTextblockqtddmn_Jsonclick ;
      private String edtavQtddmn_Jsonclick ;
      private String lblTextblockqtdpf_Internalname ;
      private String lblTextblockqtdpf_Jsonclick ;
      private String edtavQtdpf_Jsonclick ;
      private String lblTextblockvalortotal_Internalname ;
      private String lblTextblockvalortotal_Jsonclick ;
      private String edtavValortotal_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblLotetitle_Internalname ;
      private String lblLotetitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextlote_status_Internalname ;
      private String lblFiltertextlote_status_Jsonclick ;
      private String cmbavLote_status_Jsonclick ;
      private String lblFiltertextlote_areatrabalhocod_Internalname ;
      private String lblFiltertextlote_areatrabalhocod_Jsonclick ;
      private String dynavLote_areatrabalhocod_Jsonclick ;
      private String lblFiltertextlote_contratadacod_Internalname ;
      private String lblFiltertextlote_contratadacod_Jsonclick ;
      private String edtavLote_contratadacod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtn_search_Internalname ;
      private String bttBtn_search_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavLote_numero1_Jsonclick ;
      private String edtavLote_nome1_Jsonclick ;
      private String edtavLote_nfe1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavLote_numero2_Jsonclick ;
      private String edtavLote_nome2_Jsonclick ;
      private String edtavLote_nfe2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavLote_numero3_Jsonclick ;
      private String edtavLote_nome3_Jsonclick ;
      private String edtavLote_nfe3_Jsonclick ;
      private String edtavLote_datanfe3_Jsonclick ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext3_Internalname ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext3_Jsonclick ;
      private String edtavLote_datanfe_to3_Jsonclick ;
      private String edtavLote_prevpagamento3_Jsonclick ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext3_Internalname ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext3_Jsonclick ;
      private String edtavLote_prevpagamento_to3_Jsonclick ;
      private String edtavLote_datanfe2_Jsonclick ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext2_Internalname ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext2_Jsonclick ;
      private String edtavLote_datanfe_to2_Jsonclick ;
      private String edtavLote_prevpagamento2_Jsonclick ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext2_Internalname ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext2_Jsonclick ;
      private String edtavLote_prevpagamento_to2_Jsonclick ;
      private String edtavLote_datanfe1_Jsonclick ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext1_Internalname ;
      private String lblDynamicfilterslote_datanfe_rangemiddletext1_Jsonclick ;
      private String edtavLote_datanfe_to1_Jsonclick ;
      private String edtavLote_prevpagamento1_Jsonclick ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext1_Internalname ;
      private String lblDynamicfilterslote_prevpagamento_rangemiddletext1_Jsonclick ;
      private String edtavLote_prevpagamento_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_140_fel_idx="0001" ;
      private String ROClassString ;
      private String edtLote_Codigo_Jsonclick ;
      private String edtLote_Numero_Jsonclick ;
      private String edtLote_Nome_Jsonclick ;
      private String edtLote_Data_Jsonclick ;
      private String edtLote_NFe_Jsonclick ;
      private String edtLote_DataNfe_Jsonclick ;
      private String edtLote_PrevPagamento_Jsonclick ;
      private String edtavAction_Jsonclick ;
      private String edtLote_ValorPF_Jsonclick ;
      private String edtLote_QtdeDmn_Jsonclick ;
      private String edtLote_QtdePF_Jsonclick ;
      private String cmbLote_Status_Jsonclick ;
      private String edtLote_Valor_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A564Lote_Data ;
      private DateTime AV50Lote_PrevPagamento1 ;
      private DateTime AV51Lote_PrevPagamento_To1 ;
      private DateTime AV63Lote_DataNfe1 ;
      private DateTime AV64Lote_DataNfe_To1 ;
      private DateTime AV52Lote_PrevPagamento2 ;
      private DateTime AV53Lote_PrevPagamento_To2 ;
      private DateTime AV66Lote_DataNfe2 ;
      private DateTime AV67Lote_DataNfe_To2 ;
      private DateTime AV54Lote_PrevPagamento3 ;
      private DateTime AV55Lote_PrevPagamento_To3 ;
      private DateTime AV69Lote_DataNfe3 ;
      private DateTime AV70Lote_DataNfe_To3 ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime Gx_date ;
      private DateTime AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 ;
      private DateTime AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 ;
      private DateTime AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 ;
      private DateTime AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 ;
      private DateTime AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 ;
      private DateTime AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 ;
      private DateTime AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 ;
      private DateTime AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 ;
      private DateTime AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 ;
      private DateTime AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 ;
      private DateTime AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 ;
      private DateTime AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV29DynamicFiltersIgnoreFirst ;
      private bool AV28DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n673Lote_NFe ;
      private bool n674Lote_DataNfe ;
      private bool n857Lote_PrevPagamento ;
      private bool AV6WWPContext_gxTpr_Userehcontratante ;
      private bool AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 ;
      private bool AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 ;
      private bool n1057Lote_ValorGlosas ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV61Action_IsBlob ;
      private String AV17DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV162Action_GXI ;
      private String AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 ;
      private String AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 ;
      private String AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 ;
      private String AV61Action ;
      private IGxSession AV32Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavLote_status ;
      private GXCombobox dynavLote_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbLote_Status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00F52_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00F52_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] H00F52_A512ContagemResultado_ValorPF ;
      private bool[] H00F52_n512ContagemResultado_ValorPF ;
      private int[] H00F52_A456ContagemResultado_Codigo ;
      private int[] H00F53_A5AreaTrabalho_Codigo ;
      private String[] H00F53_A6AreaTrabalho_Descricao ;
      private bool[] H00F53_A72AreaTrabalho_Ativo ;
      private int[] H00F58_A595Lote_AreaTrabalhoCod ;
      private decimal[] H00F58_A565Lote_ValorPF ;
      private DateTime[] H00F58_A857Lote_PrevPagamento ;
      private bool[] H00F58_n857Lote_PrevPagamento ;
      private DateTime[] H00F58_A674Lote_DataNfe ;
      private bool[] H00F58_n674Lote_DataNfe ;
      private int[] H00F58_A673Lote_NFe ;
      private bool[] H00F58_n673Lote_NFe ;
      private DateTime[] H00F58_A564Lote_Data ;
      private String[] H00F58_A563Lote_Nome ;
      private String[] H00F58_A562Lote_Numero ;
      private int[] H00F58_A596Lote_Codigo ;
      private int[] H00F58_A1231Lote_ContratadaCod ;
      private String[] H00F58_A575Lote_Status ;
      private short[] H00F58_A569Lote_QtdeDmn ;
      private decimal[] H00F58_A1057Lote_ValorGlosas ;
      private bool[] H00F58_n1057Lote_ValorGlosas ;
      private long[] H00F513_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private IGxSession AV72WebSession ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private SdtSDT_FiltroConsContadorFM AV71SDT_FiltroConsContadorFM ;
   }

   public class extrawwloteapagar__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00F58( IGxContext context ,
                                             int AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod ,
                                             String AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 ,
                                             DateTime AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 ,
                                             DateTime AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 ,
                                             String AV139ExtraWWLoteAPagarDS_7_Lote_numero1 ,
                                             String AV140ExtraWWLoteAPagarDS_8_Lote_nome1 ,
                                             int AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 ,
                                             DateTime AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 ,
                                             DateTime AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 ,
                                             bool AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 ,
                                             String AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 ,
                                             DateTime AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 ,
                                             DateTime AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 ,
                                             String AV148ExtraWWLoteAPagarDS_16_Lote_numero2 ,
                                             String AV149ExtraWWLoteAPagarDS_17_Lote_nome2 ,
                                             int AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 ,
                                             DateTime AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 ,
                                             DateTime AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 ,
                                             bool AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 ,
                                             String AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 ,
                                             DateTime AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 ,
                                             DateTime AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 ,
                                             String AV157ExtraWWLoteAPagarDS_25_Lote_numero3 ,
                                             String AV158ExtraWWLoteAPagarDS_26_Lote_nome3 ,
                                             int AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 ,
                                             DateTime AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 ,
                                             DateTime AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 ,
                                             int AV10GridState_gxTpr_Dynamicfilters_Count ,
                                             int A595Lote_AreaTrabalhoCod ,
                                             DateTime A857Lote_PrevPagamento ,
                                             String A562Lote_Numero ,
                                             String A563Lote_Nome ,
                                             int A673Lote_NFe ,
                                             DateTime A674Lote_DataNfe ,
                                             DateTime Gx_date ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             bool AV6WWPContext_gxTpr_Userehcontratante ,
                                             int A1231Lote_ContratadaCod ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             String A575Lote_Status )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [32] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Lote_AreaTrabalhoCod], T1.[Lote_ValorPF], T1.[Lote_PrevPagamento], T1.[Lote_DataNfe], T1.[Lote_NFe], T1.[Lote_Data], T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_Codigo], COALESCE( T2.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T2.[Lote_Status], '') AS Lote_Status, COALESCE( T2.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T3.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas";
         sFromString = " FROM (([Lote] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_StatusDmn]) AS Lote_Status, COUNT(*) AS Lote_QtdeDmn FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T2 ON T2.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T6.[GXC2], 0) + COALESCE( T5.[GXC3], 0) AS Lote_ValorGlosas, T4.[Lote_Codigo] FROM (([Lote] T4 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T7.[ContagemResultadoIndicadores_Valor]) AS GXC3, T8.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T7 WITH (NOLOCK),  [Lote] T8 WITH (NOLOCK) WHERE T7.[ContagemResultadoIndicadores_LoteCod] = T8.[Lote_Codigo] GROUP BY T8.[Lote_Codigo] ) T5 ON T5.[Lote_Codigo] = T4.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T4.[Lote_Codigo]) ) T3 ON T3.[Lote_Codigo] = T1.[Lote_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (@AV6WWPCo_2Userehcontratante = 1 or ( COALESCE( T2.[Lote_ContratadaCod], 0) = @AV6WWPCo_1Contratada_codigo))";
         sWhereString = sWhereString + " and (COALESCE( T2.[Lote_Status], '') = 'P')";
         if ( ! (0==AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_AreaTrabalhoCod] = @AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139ExtraWWLoteAPagarDS_7_Lote_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV139ExtraWWLoteAPagarDS_7_Lote_numero1 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140ExtraWWLoteAPagarDS_8_Lote_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV140ExtraWWLoteAPagarDS_8_Lote_nome1 + '%')";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_NFE") == 0 ) && ( ! (0==AV141ExtraWWLoteAPagarDS_9_Lote_nfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV141ExtraWWLoteAPagarDS_9_Lote_nfe1)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148ExtraWWLoteAPagarDS_16_Lote_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV148ExtraWWLoteAPagarDS_16_Lote_numero2 + '%')";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149ExtraWWLoteAPagarDS_17_Lote_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV149ExtraWWLoteAPagarDS_17_Lote_nome2 + '%')";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_NFE") == 0 ) && ( ! (0==AV150ExtraWWLoteAPagarDS_18_Lote_nfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV150ExtraWWLoteAPagarDS_18_Lote_nfe2)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157ExtraWWLoteAPagarDS_25_Lote_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV157ExtraWWLoteAPagarDS_25_Lote_numero3 + '%')";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158ExtraWWLoteAPagarDS_26_Lote_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV158ExtraWWLoteAPagarDS_26_Lote_nome3 + '%')";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_NFE") == 0 ) && ( ! (0==AV159ExtraWWLoteAPagarDS_27_Lote_nfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV159ExtraWWLoteAPagarDS_27_Lote_nfe3)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV10GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= DATEADD( ss,86400 * ( -DAY(@Gx_date) + 1), @Gx_date) and T1.[Lote_PrevPagamento] <= DATEADD(day, -1, DATEADD(month, 1, DATEADD(day, -DAY(@Gx_date) + 1, @Gx_date))))";
         }
         else
         {
            GXv_int2[24] = 1;
            GXv_int2[25] = 1;
            GXv_int2[26] = 1;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Data]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_NFe]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_NFe] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_DataNfe]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_DataNfe] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_PrevPagamento]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_PrevPagamento] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_ValorPF]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_ValorPF] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Lote_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00F513( IGxContext context ,
                                              int AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod ,
                                              String AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1 ,
                                              DateTime AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1 ,
                                              DateTime AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1 ,
                                              String AV139ExtraWWLoteAPagarDS_7_Lote_numero1 ,
                                              String AV140ExtraWWLoteAPagarDS_8_Lote_nome1 ,
                                              int AV141ExtraWWLoteAPagarDS_9_Lote_nfe1 ,
                                              DateTime AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1 ,
                                              DateTime AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1 ,
                                              bool AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 ,
                                              String AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2 ,
                                              DateTime AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2 ,
                                              DateTime AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2 ,
                                              String AV148ExtraWWLoteAPagarDS_16_Lote_numero2 ,
                                              String AV149ExtraWWLoteAPagarDS_17_Lote_nome2 ,
                                              int AV150ExtraWWLoteAPagarDS_18_Lote_nfe2 ,
                                              DateTime AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2 ,
                                              DateTime AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2 ,
                                              bool AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 ,
                                              String AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3 ,
                                              DateTime AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3 ,
                                              DateTime AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3 ,
                                              String AV157ExtraWWLoteAPagarDS_25_Lote_numero3 ,
                                              String AV158ExtraWWLoteAPagarDS_26_Lote_nome3 ,
                                              int AV159ExtraWWLoteAPagarDS_27_Lote_nfe3 ,
                                              DateTime AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3 ,
                                              DateTime AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3 ,
                                              int AV10GridState_gxTpr_Dynamicfilters_Count ,
                                              int A595Lote_AreaTrabalhoCod ,
                                              DateTime A857Lote_PrevPagamento ,
                                              String A562Lote_Numero ,
                                              String A563Lote_Nome ,
                                              int A673Lote_NFe ,
                                              DateTime A674Lote_DataNfe ,
                                              DateTime Gx_date ,
                                              short AV13OrderedBy ,
                                              bool AV14OrderedDsc ,
                                              bool AV6WWPContext_gxTpr_Userehcontratante ,
                                              int A1231Lote_ContratadaCod ,
                                              int AV6WWPContext_gxTpr_Contratada_codigo ,
                                              String A575Lote_Status )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [27] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Lote] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_StatusDmn]) AS Lote_Status, COUNT(*) AS Lote_QtdeDmn FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T2 ON T2.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T6.[GXC2], 0) + COALESCE( T5.[GXC3], 0) AS Lote_ValorGlosas, T4.[Lote_Codigo] FROM (([Lote] T4 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T7.[ContagemResultadoIndicadores_Valor]) AS GXC3, T8.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T7 WITH (NOLOCK),  [Lote] T8 WITH (NOLOCK) WHERE T7.[ContagemResultadoIndicadores_LoteCod] = T8.[Lote_Codigo] GROUP BY T8.[Lote_Codigo] ) T5 ON T5.[Lote_Codigo] = T4.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T4.[Lote_Codigo]) ) T3 ON T3.[Lote_Codigo] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (@AV6WWPCo_2Userehcontratante = 1 or ( COALESCE( T2.[Lote_ContratadaCod], 0) = @AV6WWPCo_1Contratada_codigo))";
         scmdbuf = scmdbuf + " and (COALESCE( T2.[Lote_Status], '') = 'P')";
         if ( ! (0==AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_AreaTrabalhoCod] = @AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139ExtraWWLoteAPagarDS_7_Lote_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV139ExtraWWLoteAPagarDS_7_Lote_numero1 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140ExtraWWLoteAPagarDS_8_Lote_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV140ExtraWWLoteAPagarDS_8_Lote_nome1 + '%')";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_NFE") == 0 ) && ( ! (0==AV141ExtraWWLoteAPagarDS_9_Lote_nfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV141ExtraWWLoteAPagarDS_9_Lote_nfe1)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV136ExtraWWLoteAPagarDS_4_Dynamicfiltersselector1, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148ExtraWWLoteAPagarDS_16_Lote_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV148ExtraWWLoteAPagarDS_16_Lote_numero2 + '%')";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149ExtraWWLoteAPagarDS_17_Lote_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV149ExtraWWLoteAPagarDS_17_Lote_nome2 + '%')";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_NFE") == 0 ) && ( ! (0==AV150ExtraWWLoteAPagarDS_18_Lote_nfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV150ExtraWWLoteAPagarDS_18_Lote_nfe2)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV144ExtraWWLoteAPagarDS_12_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV145ExtraWWLoteAPagarDS_13_Dynamicfiltersselector2, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= @AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_PREVPAGAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] <= @AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157ExtraWWLoteAPagarDS_25_Lote_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like '%' + @lV157ExtraWWLoteAPagarDS_25_Lote_numero3 + '%')";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158ExtraWWLoteAPagarDS_26_Lote_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV158ExtraWWLoteAPagarDS_26_Lote_nome3 + '%')";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_NFE") == 0 ) && ( ! (0==AV159ExtraWWLoteAPagarDS_27_Lote_nfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_NFe] = @AV159ExtraWWLoteAPagarDS_27_Lote_nfe3)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] >= @AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV153ExtraWWLoteAPagarDS_21_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV154ExtraWWLoteAPagarDS_22_Dynamicfiltersselector3, "LOTE_DATANFE") == 0 ) && ( ! (DateTime.MinValue==AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_DataNfe] <= @AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV10GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[Lote_PrevPagamento] >= DATEADD( ss,86400 * ( -DAY(@Gx_date) + 1), @Gx_date) and T1.[Lote_PrevPagamento] <= DATEADD(day, -1, DATEADD(month, 1, DATEADD(day, -DAY(@Gx_date) + 1, @Gx_date))))";
         }
         else
         {
            GXv_int4[24] = 1;
            GXv_int4[25] = 1;
            GXv_int4[26] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00F58(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (bool)dynConstraints[18] , (String)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] , (bool)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] );
               case 3 :
                     return conditional_H00F513(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (bool)dynConstraints[18] , (String)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] , (bool)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00F52 ;
          prmH00F52 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00F53 ;
          prmH00F53 = new Object[] {
          } ;
          Object[] prmH00F58 ;
          prmH00F58 = new Object[] {
          new Object[] {"@AV6WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139ExtraWWLoteAPagarDS_7_Lote_numero1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV140ExtraWWLoteAPagarDS_8_Lote_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV141ExtraWWLoteAPagarDS_9_Lote_nfe1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV148ExtraWWLoteAPagarDS_16_Lote_numero2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV149ExtraWWLoteAPagarDS_17_Lote_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV150ExtraWWLoteAPagarDS_18_Lote_nfe2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV157ExtraWWLoteAPagarDS_25_Lote_numero3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV158ExtraWWLoteAPagarDS_26_Lote_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV159ExtraWWLoteAPagarDS_27_Lote_nfe3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00F513 ;
          prmH00F513 = new Object[] {
          new Object[] {"@AV6WWPCo_2Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134ExtraWWLoteAPagarDS_2_Lote_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137ExtraWWLoteAPagarDS_5_Lote_prevpagamento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138ExtraWWLoteAPagarDS_6_Lote_prevpagamento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139ExtraWWLoteAPagarDS_7_Lote_numero1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV140ExtraWWLoteAPagarDS_8_Lote_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV141ExtraWWLoteAPagarDS_9_Lote_nfe1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV142ExtraWWLoteAPagarDS_10_Lote_datanfe1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143ExtraWWLoteAPagarDS_11_Lote_datanfe_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV146ExtraWWLoteAPagarDS_14_Lote_prevpagamento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147ExtraWWLoteAPagarDS_15_Lote_prevpagamento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV148ExtraWWLoteAPagarDS_16_Lote_numero2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV149ExtraWWLoteAPagarDS_17_Lote_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV150ExtraWWLoteAPagarDS_18_Lote_nfe2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV151ExtraWWLoteAPagarDS_19_Lote_datanfe2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV152ExtraWWLoteAPagarDS_20_Lote_datanfe_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV155ExtraWWLoteAPagarDS_23_Lote_prevpagamento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV156ExtraWWLoteAPagarDS_24_Lote_prevpagamento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV157ExtraWWLoteAPagarDS_25_Lote_numero3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV158ExtraWWLoteAPagarDS_26_Lote_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV159ExtraWWLoteAPagarDS_27_Lote_nfe3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV160ExtraWWLoteAPagarDS_28_Lote_datanfe3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV161ExtraWWLoteAPagarDS_29_Lote_datanfe_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00F52", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F52,100,0,true,false )
             ,new CursorDef("H00F53", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao], [AreaTrabalho_Ativo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Ativo] = 1 ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F53,0,0,true,false )
             ,new CursorDef("H00F58", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F58,11,0,true,false )
             ,new CursorDef("H00F513", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F513,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 10) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((String[]) buf[13])[0] = rslt.getString(11, 1) ;
                ((short[]) buf[14])[0] = rslt.getShort(12) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[27]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                return;
       }
    }

 }

}
