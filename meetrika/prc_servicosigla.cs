/*
               File: PRC_ServicoSigla
        Description: Retorna sigla do Servi�o de uma Demanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:45.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_servicosigla : GXProcedure
   {
      public prc_servicosigla( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_servicosigla( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           out String aP1_Servico_Sigla )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9Servico_Sigla = "" ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Servico_Sigla=this.AV9Servico_Sigla;
      }

      public String executeUdp( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9Servico_Sigla = "" ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Servico_Sigla=this.AV9Servico_Sigla;
         return AV9Servico_Sigla ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 out String aP1_Servico_Sigla )
      {
         prc_servicosigla objprc_servicosigla;
         objprc_servicosigla = new prc_servicosigla();
         objprc_servicosigla.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_servicosigla.AV9Servico_Sigla = "" ;
         objprc_servicosigla.context.SetSubmitInitialConfig(context);
         objprc_servicosigla.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_servicosigla);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_Servico_Sigla=this.AV9Servico_Sigla;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_servicosigla)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007A2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P007A2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P007A2_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P007A2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P007A2_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P007A2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P007A2_n801ContagemResultado_ServicoSigla[0];
            A601ContagemResultado_Servico = P007A2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P007A2_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P007A2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P007A2_n801ContagemResultado_ServicoSigla[0];
            AV9Servico_Sigla = A801ContagemResultado_ServicoSigla;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007A2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P007A2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P007A2_A601ContagemResultado_Servico = new int[1] ;
         P007A2_n601ContagemResultado_Servico = new bool[] {false} ;
         P007A2_A456ContagemResultado_Codigo = new int[1] ;
         P007A2_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P007A2_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_servicosigla__default(),
            new Object[][] {
                new Object[] {
               P007A2_A1553ContagemResultado_CntSrvCod, P007A2_n1553ContagemResultado_CntSrvCod, P007A2_A601ContagemResultado_Servico, P007A2_n601ContagemResultado_Servico, P007A2_A456ContagemResultado_Codigo, P007A2_A801ContagemResultado_ServicoSigla, P007A2_n801ContagemResultado_ServicoSigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private String AV9Servico_Sigla ;
      private String scmdbuf ;
      private String A801ContagemResultado_ServicoSigla ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P007A2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P007A2_n1553ContagemResultado_CntSrvCod ;
      private int[] P007A2_A601ContagemResultado_Servico ;
      private bool[] P007A2_n601ContagemResultado_Servico ;
      private int[] P007A2_A456ContagemResultado_Codigo ;
      private String[] P007A2_A801ContagemResultado_ServicoSigla ;
      private bool[] P007A2_n801ContagemResultado_ServicoSigla ;
      private String aP1_Servico_Sigla ;
   }

   public class prc_servicosigla__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007A2 ;
          prmP007A2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007A2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007A2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
