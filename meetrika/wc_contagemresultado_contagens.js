/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:13:49.95
*/
gx.evt.autoSkip = false;
gx.define('wc_contagemresultado_contagens', true, function (CmpContext) {
   this.ServerClass =  "wc_contagemresultado_contagens" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.Valid_Contagemresultado_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(8) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_CODIGO", gx.fn.currentGridRowImpl(8));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e11b02_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e15b02_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e16b02_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,9,10,11,12,13,14,15,16,17,18,19,23,26];
   this.GXLastCtrlId =26;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",8,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wc_contagemresultado_contagens",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addSingleLineEdit(456,9,"CONTAGEMRESULTADO_CODIGO","Contagem Resultado_Codigo","","ContagemResultado_Codigo","int",0,"px",6,6,"right",null,[],456,"ContagemResultado_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(473,10,"CONTAGEMRESULTADO_DATACNT","Data","","ContagemResultado_DataCnt","date",0,"px",8,8,"right",null,[],473,"ContagemResultado_DataCnt",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(511,11,"CONTAGEMRESULTADO_HORACNT","Hora","","ContagemResultado_HoraCnt","char",0,"px",5,5,"left",null,[],511,"ContagemResultado_HoraCnt",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(474,12,"CONTAGEMRESULTADO_CONTADORFMNOM","Contador","","ContagemResultado_ContadorFMNom","char",250,"px",100,80,"left",null,[],474,"ContagemResultado_ContadorFMNom",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(482,13,"CONTAGEMRESULTADOCONTAGENS_ESFORCO","Esforço","","ContagemResultadoContagens_Esforco","int",45,"px",4,4,"right",null,[],482,"ContagemResultadoContagens_Esforco",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(458,14,"CONTAGEMRESULTADO_PFBFS","PFB FS","","ContagemResultado_PFBFS","decimal",94,"px",14,14,"right",null,[],458,"ContagemResultado_PFBFS",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(459,15,"CONTAGEMRESULTADO_PFLFS","PFL FS","","ContagemResultado_PFLFS","decimal",94,"px",14,14,"right",null,[],459,"ContagemResultado_PFLFS",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(460,16,"CONTAGEMRESULTADO_PFBFM","PFB FM","","ContagemResultado_PFBFM","decimal",94,"px",14,14,"right",null,[],460,"ContagemResultado_PFBFM",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(461,17,"CONTAGEMRESULTADO_PFLFM","PFL FM","","ContagemResultado_PFLFM","decimal",94,"px",14,14,"right",null,[],461,"ContagemResultado_PFLFM",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(462,18,"CONTAGEMRESULTADO_DIVERGENCIA","Divergência","","ContagemResultado_Divergencia","decimal",0,"px",6,6,"right",null,[],462,"ContagemResultado_Divergencia",true,2,false,false,"BootstrapAttribute",1,"");
   GridContainer.addComboBox(483,19,"CONTAGEMRESULTADO_STATUSCNT","Status","ContagemResultado_StatusCnt","int",null,0,true,false,0,"px","");
   this.setGrid(GridContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 22, 9, "DVelop_DVPaginationBar", this.CmpContext + "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV7GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV7GridCurrentPage=UC.GetCurrentPage();GXValidFnc[23].c2v(); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV8GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV8GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV8GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11b02_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[9]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:this.Valid_Contagemresultado_codigo,isvalid:null,rgrid:[this.GridContainer],fld:"CONTAGEMRESULTADO_CODIGO",gxz:"Z456ContagemResultado_Codigo",gxold:"O456ContagemResultado_Codigo",gxvar:"A456ContagemResultado_Codigo",ucs:[],op:[12],ip:[12,9],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z456ContagemResultado_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_CODIGO",row || gx.fn.currentGridRowImpl(8),gx.O.A456ContagemResultado_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADO_CODIGO",row || gx.fn.currentGridRowImpl(8),'.')},nac:gx.falseFn};
   GXValidFnc[10]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DATACNT",gxz:"Z473ContagemResultado_DataCnt",gxold:"O473ContagemResultado_DataCnt",gxvar:"A473ContagemResultado_DataCnt",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A473ContagemResultado_DataCnt=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z473ContagemResultado_DataCnt=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DATACNT",row || gx.fn.currentGridRowImpl(8),gx.O.A473ContagemResultado_DataCnt,0)},c2v:function(){if(this.val()!==undefined)gx.O.A473ContagemResultado_DataCnt=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADO_DATACNT",row || gx.fn.currentGridRowImpl(8))},nac:gx.falseFn};
   GXValidFnc[11]={lvl:2,type:"char",len:5,dec:0,sign:false,ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_HORACNT",gxz:"Z511ContagemResultado_HoraCnt",gxold:"O511ContagemResultado_HoraCnt",gxvar:"A511ContagemResultado_HoraCnt",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A511ContagemResultado_HoraCnt=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z511ContagemResultado_HoraCnt=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_HORACNT",row || gx.fn.currentGridRowImpl(8),gx.O.A511ContagemResultado_HoraCnt,0)},c2v:function(){if(this.val()!==undefined)gx.O.A511ContagemResultado_HoraCnt=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_HORACNT",row || gx.fn.currentGridRowImpl(8))},nac:gx.falseFn};
   GXValidFnc[12]={lvl:2,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_CONTADORFMNOM",gxz:"Z474ContagemResultado_ContadorFMNom",gxold:"O474ContagemResultado_ContadorFMNom",gxvar:"A474ContagemResultado_ContadorFMNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A474ContagemResultado_ContadorFMNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z474ContagemResultado_ContadorFMNom=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_CONTADORFMNOM",row || gx.fn.currentGridRowImpl(8),gx.O.A474ContagemResultado_ContadorFMNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A474ContagemResultado_ContadorFMNom=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_CONTADORFMNOM",row || gx.fn.currentGridRowImpl(8))},nac:gx.falseFn};
   GXValidFnc[13]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOCONTAGENS_ESFORCO",gxz:"Z482ContagemResultadoContagens_Esforco",gxold:"O482ContagemResultadoContagens_Esforco",gxvar:"A482ContagemResultadoContagens_Esforco",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A482ContagemResultadoContagens_Esforco=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z482ContagemResultadoContagens_Esforco=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOCONTAGENS_ESFORCO",row || gx.fn.currentGridRowImpl(8),gx.O.A482ContagemResultadoContagens_Esforco,0)},c2v:function(){if(this.val()!==undefined)gx.O.A482ContagemResultadoContagens_Esforco=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADOCONTAGENS_ESFORCO",row || gx.fn.currentGridRowImpl(8),'.')},nac:gx.falseFn};
   GXValidFnc[14]={lvl:2,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PFBFS",gxz:"Z458ContagemResultado_PFBFS",gxold:"O458ContagemResultado_PFBFS",gxvar:"A458ContagemResultado_PFBFS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A458ContagemResultado_PFBFS=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z458ContagemResultado_PFBFS=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("CONTAGEMRESULTADO_PFBFS",row || gx.fn.currentGridRowImpl(8),gx.O.A458ContagemResultado_PFBFS,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A458ContagemResultado_PFBFS=this.val()},val:function(row){return gx.fn.getGridDecimalValue("CONTAGEMRESULTADO_PFBFS",row || gx.fn.currentGridRowImpl(8),'.',',')},nac:gx.falseFn};
   GXValidFnc[15]={lvl:2,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PFLFS",gxz:"Z459ContagemResultado_PFLFS",gxold:"O459ContagemResultado_PFLFS",gxvar:"A459ContagemResultado_PFLFS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A459ContagemResultado_PFLFS=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z459ContagemResultado_PFLFS=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("CONTAGEMRESULTADO_PFLFS",row || gx.fn.currentGridRowImpl(8),gx.O.A459ContagemResultado_PFLFS,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A459ContagemResultado_PFLFS=this.val()},val:function(row){return gx.fn.getGridDecimalValue("CONTAGEMRESULTADO_PFLFS",row || gx.fn.currentGridRowImpl(8),'.',',')},nac:gx.falseFn};
   GXValidFnc[16]={lvl:2,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PFBFM",gxz:"Z460ContagemResultado_PFBFM",gxold:"O460ContagemResultado_PFBFM",gxvar:"A460ContagemResultado_PFBFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A460ContagemResultado_PFBFM=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z460ContagemResultado_PFBFM=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("CONTAGEMRESULTADO_PFBFM",row || gx.fn.currentGridRowImpl(8),gx.O.A460ContagemResultado_PFBFM,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A460ContagemResultado_PFBFM=this.val()},val:function(row){return gx.fn.getGridDecimalValue("CONTAGEMRESULTADO_PFBFM",row || gx.fn.currentGridRowImpl(8),'.',',')},nac:gx.falseFn};
   GXValidFnc[17]={lvl:2,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PFLFM",gxz:"Z461ContagemResultado_PFLFM",gxold:"O461ContagemResultado_PFLFM",gxvar:"A461ContagemResultado_PFLFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A461ContagemResultado_PFLFM=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z461ContagemResultado_PFLFM=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("CONTAGEMRESULTADO_PFLFM",row || gx.fn.currentGridRowImpl(8),gx.O.A461ContagemResultado_PFLFM,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A461ContagemResultado_PFLFM=this.val()},val:function(row){return gx.fn.getGridDecimalValue("CONTAGEMRESULTADO_PFLFM",row || gx.fn.currentGridRowImpl(8),'.',',')},nac:gx.falseFn};
   GXValidFnc[18]={lvl:2,type:"decimal",len:6,dec:2,sign:false,pic:"ZZ9.99",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DIVERGENCIA",gxz:"Z462ContagemResultado_Divergencia",gxold:"O462ContagemResultado_Divergencia",gxvar:"A462ContagemResultado_Divergencia",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A462ContagemResultado_Divergencia=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z462ContagemResultado_Divergencia=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("CONTAGEMRESULTADO_DIVERGENCIA",row || gx.fn.currentGridRowImpl(8),gx.O.A462ContagemResultado_Divergencia,2,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A462ContagemResultado_Divergencia=this.val()},val:function(row){return gx.fn.getGridDecimalValue("CONTAGEMRESULTADO_DIVERGENCIA",row || gx.fn.currentGridRowImpl(8),'.',',')},nac:gx.falseFn};
   GXValidFnc[19]={lvl:2,type:"int",len:2,dec:0,sign:false,pic:"Z9",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_STATUSCNT",gxz:"Z483ContagemResultado_StatusCnt",gxold:"O483ContagemResultado_StatusCnt",gxvar:"A483ContagemResultado_StatusCnt",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A483ContagemResultado_StatusCnt=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z483ContagemResultado_StatusCnt=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("CONTAGEMRESULTADO_STATUSCNT",row || gx.fn.currentGridRowImpl(8),gx.O.A483ContagemResultado_StatusCnt);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A483ContagemResultado_StatusCnt=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADO_STATUSCNT",row || gx.fn.currentGridRowImpl(8),'.')},nac:gx.falseFn};
   GXValidFnc[23]={lvl:0,type:"int",len:10,dec:0,sign:false,pic:"ZZZZZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vGRIDCURRENTPAGE",gxz:"ZV7GridCurrentPage",gxold:"OV7GridCurrentPage",gxvar:"AV7GridCurrentPage",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7GridCurrentPage=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV7GridCurrentPage=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vGRIDCURRENTPAGE",gx.O.AV7GridCurrentPage,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7GridCurrentPage=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vGRIDCURRENTPAGE",'.')},nac:gx.falseFn};
   GXValidFnc[26]={fld:"TBJAVA", format:1,grid:0};
   this.Z456ContagemResultado_Codigo = 0 ;
   this.O456ContagemResultado_Codigo = 0 ;
   this.Z473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.O473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.Z511ContagemResultado_HoraCnt = "" ;
   this.O511ContagemResultado_HoraCnt = "" ;
   this.Z474ContagemResultado_ContadorFMNom = "" ;
   this.O474ContagemResultado_ContadorFMNom = "" ;
   this.Z482ContagemResultadoContagens_Esforco = 0 ;
   this.O482ContagemResultadoContagens_Esforco = 0 ;
   this.Z458ContagemResultado_PFBFS = 0 ;
   this.O458ContagemResultado_PFBFS = 0 ;
   this.Z459ContagemResultado_PFLFS = 0 ;
   this.O459ContagemResultado_PFLFS = 0 ;
   this.Z460ContagemResultado_PFBFM = 0 ;
   this.O460ContagemResultado_PFBFM = 0 ;
   this.Z461ContagemResultado_PFLFM = 0 ;
   this.O461ContagemResultado_PFLFM = 0 ;
   this.Z462ContagemResultado_Divergencia = 0 ;
   this.O462ContagemResultado_Divergencia = 0 ;
   this.Z483ContagemResultado_StatusCnt = 0 ;
   this.O483ContagemResultado_StatusCnt = 0 ;
   this.AV7GridCurrentPage = 0 ;
   this.ZV7GridCurrentPage = 0 ;
   this.OV7GridCurrentPage = 0 ;
   this.AV7GridCurrentPage = 0 ;
   this.A457ContagemResultado_Demanda = "" ;
   this.A470ContagemResultado_ContadorFMCod = 0 ;
   this.A479ContagemResultado_CrFMPessoaCod = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.A511ContagemResultado_HoraCnt = "" ;
   this.A474ContagemResultado_ContadorFMNom = "" ;
   this.A482ContagemResultadoContagens_Esforco = 0 ;
   this.A458ContagemResultado_PFBFS = 0 ;
   this.A459ContagemResultado_PFLFS = 0 ;
   this.A460ContagemResultado_PFBFM = 0 ;
   this.A461ContagemResultado_PFLFM = 0 ;
   this.A462ContagemResultado_Divergencia = 0 ;
   this.A483ContagemResultado_StatusCnt = 0 ;
   this.Events = {"e11b02_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e15b02_client": ["ENTER", true] ,"e16b02_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_CONTADORFMNOM","Visible")',ctrl:'CONTAGEMRESULTADO_CONTADORFMNOM',prop:'Visible'}]];
   this.EvtParms["GRID.LOAD"] = [[],[]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'},{av:'AV7GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],[{av:'AV7GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]];
   GridContainer.addRefreshingVar({rfrVar:"A456ContagemResultado_Codigo", rfrProp:"Value", gxAttId:"456"});
   this.InitStandaloneVars( );
});
