/*
               File: PromptHistoricoConsumo
        Description: Selecione Historico Consumo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:3:8.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prompthistoricoconsumo : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public prompthistoricoconsumo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prompthistoricoconsumo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutHistoricoConsumo_Codigo )
      {
         this.AV7InOutHistoricoConsumo_Codigo = aP0_InOutHistoricoConsumo_Codigo;
         executePrivate();
         aP0_InOutHistoricoConsumo_Codigo=this.AV7InOutHistoricoConsumo_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_27 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_27_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_27_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV31TFHistoricoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFHistoricoConsumo_Codigo), 6, 0)));
               AV32TFHistoricoConsumo_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFHistoricoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFHistoricoConsumo_Codigo_To), 6, 0)));
               AV35TFHistoricoConsumo_SaldoContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFHistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), 6, 0)));
               AV36TFHistoricoConsumo_SaldoContratoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFHistoricoConsumo_SaldoContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), 6, 0)));
               AV39TFHistoricoConsumo_NotaEmpenhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFHistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), 6, 0)));
               AV40TFHistoricoConsumo_NotaEmpenhoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFHistoricoConsumo_NotaEmpenhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), 6, 0)));
               AV43TFHistoricoConsumo_ContagemResultadoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFHistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), 6, 0)));
               AV44TFHistoricoConsumo_ContagemResultadoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFHistoricoConsumo_ContagemResultadoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), 6, 0)));
               AV47TFHistoricoConsumo_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFHistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFHistoricoConsumo_ContratoCod), 6, 0)));
               AV48TFHistoricoConsumo_ContratoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFHistoricoConsumo_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), 6, 0)));
               AV51TFHistoricoConsumo_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFHistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), 6, 0)));
               AV52TFHistoricoConsumo_UsuarioCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFHistoricoConsumo_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), 6, 0)));
               AV55TFHistoricoConsumo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFHistoricoConsumo_Data", context.localUtil.TToC( AV55TFHistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
               AV56TFHistoricoConsumo_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFHistoricoConsumo_Data_To", context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV61TFHistoricoConsumo_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFHistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV61TFHistoricoConsumo_Valor, 18, 5)));
               AV62TFHistoricoConsumo_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFHistoricoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV62TFHistoricoConsumo_Valor_To, 18, 5)));
               AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace", AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace);
               AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace", AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace);
               AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace", AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace);
               AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace", AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace);
               AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace", AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace);
               AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace", AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace);
               AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace", AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace);
               AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace", AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To, AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutHistoricoConsumo_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutHistoricoConsumo_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAMG2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSMG2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEMG2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031193888");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("prompthistoricoconsumo.aspx") + "?" + UrlEncode("" +AV7InOutHistoricoConsumo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFHistoricoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFHistoricoConsumo_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_SALDOCONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_NOTAEMPENHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFHistoricoConsumo_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_CONTRATOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_USUARIOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_DATA", context.localUtil.TToC( AV55TFHistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_DATA_TO", context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_VALOR", StringUtil.LTrim( StringUtil.NToC( AV61TFHistoricoConsumo_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFHISTORICOCONSUMO_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV62TFHistoricoConsumo_Valor_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_27", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_27), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV64DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV64DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_CODIGOTITLEFILTERDATA", AV30HistoricoConsumo_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_CODIGOTITLEFILTERDATA", AV30HistoricoConsumo_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_SALDOCONTRATOCODTITLEFILTERDATA", AV34HistoricoConsumo_SaldoContratoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_SALDOCONTRATOCODTITLEFILTERDATA", AV34HistoricoConsumo_SaldoContratoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_NOTAEMPENHOCODTITLEFILTERDATA", AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_NOTAEMPENHOCODTITLEFILTERDATA", AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLEFILTERDATA", AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLEFILTERDATA", AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_CONTRATOCODTITLEFILTERDATA", AV46HistoricoConsumo_ContratoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_CONTRATOCODTITLEFILTERDATA", AV46HistoricoConsumo_ContratoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_USUARIOCODTITLEFILTERDATA", AV50HistoricoConsumo_UsuarioCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_USUARIOCODTITLEFILTERDATA", AV50HistoricoConsumo_UsuarioCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_DATATITLEFILTERDATA", AV54HistoricoConsumo_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_DATATITLEFILTERDATA", AV54HistoricoConsumo_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHISTORICOCONSUMO_VALORTITLEFILTERDATA", AV60HistoricoConsumo_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHISTORICOCONSUMO_VALORTITLEFILTERDATA", AV60HistoricoConsumo_ValorTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vINOUTHISTORICOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutHistoricoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Caption", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Cls", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Caption", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Cls", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_saldocontratocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_saldocontratocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_saldocontratocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_saldocontratocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_saldocontratocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Caption", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Cls", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_notaempenhocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_notaempenhocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_notaempenhocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_notaempenhocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_notaempenhocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Caption", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Cls", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_contagemresultadocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_contagemresultadocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_contagemresultadocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_contagemresultadocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_contagemresultadocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Caption", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Cls", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_contratocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_contratocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_contratocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_contratocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_contratocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Caption", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Cls", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_usuariocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_usuariocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_usuariocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_usuariocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_usuariocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Caption", StringUtil.RTrim( Ddo_historicoconsumo_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Cls", StringUtil.RTrim( Ddo_historicoconsumo_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Caption", StringUtil.RTrim( Ddo_historicoconsumo_valor_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Tooltip", StringUtil.RTrim( Ddo_historicoconsumo_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Cls", StringUtil.RTrim( Ddo_historicoconsumo_valor_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_historicoconsumo_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_historicoconsumo_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_historicoconsumo_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_historicoconsumo_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_historicoconsumo_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_historicoconsumo_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_historicoconsumo_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_historicoconsumo_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Filtertype", StringUtil.RTrim( Ddo_historicoconsumo_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_historicoconsumo_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_historicoconsumo_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Sortasc", StringUtil.RTrim( Ddo_historicoconsumo_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Sortdsc", StringUtil.RTrim( Ddo_historicoconsumo_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_historicoconsumo_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_historicoconsumo_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_historicoconsumo_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_historicoconsumo_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_saldocontratocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_notaempenhocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_contratocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_usuariocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_historicoconsumo_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_historicoconsumo_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_HISTORICOCONSUMO_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_historicoconsumo_valor_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMG2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptHistoricoConsumo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Historico Consumo" ;
      }

      protected void WBMG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_MG2( true) ;
         }
         else
         {
            wb_table1_2_MG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFHistoricoConsumo_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFHistoricoConsumo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFHistoricoConsumo_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFHistoricoConsumo_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_saldocontratocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_saldocontratocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_saldocontratocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_saldocontratocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_saldocontratocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_saldocontratocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_notaempenhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_notaempenhocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_notaempenhocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_notaempenhocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_notaempenhocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_notaempenhocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_contagemresultadocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_contagemresultadocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_contagemresultadocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_contagemresultadocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_contagemresultadocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_contratocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFHistoricoConsumo_ContratoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFHistoricoConsumo_ContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_contratocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_contratocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_contratocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_contratocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_contratocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_usuariocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_usuariocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_usuariocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_usuariocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_usuariocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_usuariocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_27_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfhistoricoconsumo_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_data_Internalname, context.localUtil.TToC( AV55TFHistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV55TFHistoricoConsumo_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavTfhistoricoconsumo_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfhistoricoconsumo_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptHistoricoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_27_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfhistoricoconsumo_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_data_to_Internalname, context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV56TFHistoricoConsumo_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavTfhistoricoconsumo_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfhistoricoconsumo_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptHistoricoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_historicoconsumo_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_27_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_historicoconsumo_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_historicoconsumo_dataauxdate_Internalname, context.localUtil.Format(AV57DDO_HistoricoConsumo_DataAuxDate, "99/99/99"), context.localUtil.Format( AV57DDO_HistoricoConsumo_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_historicoconsumo_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_historicoconsumo_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptHistoricoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_27_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_historicoconsumo_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_historicoconsumo_dataauxdateto_Internalname, context.localUtil.Format(AV58DDO_HistoricoConsumo_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV58DDO_HistoricoConsumo_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_historicoconsumo_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_historicoconsumo_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptHistoricoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV61TFHistoricoConsumo_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV61TFHistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfhistoricoconsumo_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV62TFHistoricoConsumo_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV62TFHistoricoConsumo_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfhistoricoconsumo_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfhistoricoconsumo_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Internalname, AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", 0, edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_SALDOCONTRATOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Internalname, AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", 0, edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_NOTAEMPENHOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Internalname, AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", 0, edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Internalname, AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", 0, edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_CONTRATOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Internalname, AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", 0, edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_USUARIOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Internalname, AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", 0, edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Internalname, AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", 0, edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_HISTORICOCONSUMO_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_27_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Internalname, AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptHistoricoConsumo.htm");
         }
         wbLoad = true;
      }

      protected void STARTMG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Historico Consumo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMG0( ) ;
      }

      protected void WSMG2( )
      {
         STARTMG2( ) ;
         EVTMG2( ) ;
      }

      protected void EVTMG2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11MG2 */
                           E11MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12MG2 */
                           E12MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13MG2 */
                           E13MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14MG2 */
                           E14MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15MG2 */
                           E15MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_CONTRATOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16MG2 */
                           E16MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_USUARIOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17MG2 */
                           E17MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18MG2 */
                           E18MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_HISTORICOCONSUMO_VALOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19MG2 */
                           E19MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20MG2 */
                           E20MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21MG2 */
                           E21MG2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_27_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
                           SubsflControlProps_272( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV70Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Codigo_Internalname), ",", "."));
                           A1580HistoricoConsumo_SaldoContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_SaldoContratoCod_Internalname), ",", "."));
                           A1581HistoricoConsumo_NotaEmpenhoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_NotaEmpenhoCod_Internalname), ",", "."));
                           n1581HistoricoConsumo_NotaEmpenhoCod = false;
                           A1582HistoricoConsumo_ContagemResultadoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContagemResultadoCod_Internalname), ",", "."));
                           n1582HistoricoConsumo_ContagemResultadoCod = false;
                           A1579HistoricoConsumo_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContratoCod_Internalname), ",", "."));
                           n1579HistoricoConsumo_ContratoCod = false;
                           A1563HistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_UsuarioCod_Internalname), ",", "."));
                           n1563HistoricoConsumo_UsuarioCod = false;
                           A1577HistoricoConsumo_Data = context.localUtil.CToT( cgiGet( edtHistoricoConsumo_Data_Internalname), 0);
                           A1578HistoricoConsumo_Valor = context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Valor_Internalname), ",", ".");
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22MG2 */
                                 E22MG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23MG2 */
                                 E23MG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24MG2 */
                                 E24MG2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFHistoricoConsumo_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFHistoricoConsumo_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_saldocontratocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_SALDOCONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV35TFHistoricoConsumo_SaldoContratoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_saldocontratocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFHistoricoConsumo_SaldoContratoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_notaempenhocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_NOTAEMPENHOCOD"), ",", ".") != Convert.ToDecimal( AV39TFHistoricoConsumo_NotaEmpenhoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_notaempenhocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV40TFHistoricoConsumo_NotaEmpenhoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_contagemresultadocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD"), ",", ".") != Convert.ToDecimal( AV43TFHistoricoConsumo_ContagemResultadoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_contagemresultadocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO"), ",", ".") != Convert.ToDecimal( AV44TFHistoricoConsumo_ContagemResultadoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_contratocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV47TFHistoricoConsumo_ContratoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_contratocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV48TFHistoricoConsumo_ContratoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_usuariocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV51TFHistoricoConsumo_UsuarioCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_usuariocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV52TFHistoricoConsumo_UsuarioCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFHISTORICOCONSUMO_DATA"), 0) != AV55TFHistoricoConsumo_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFHISTORICOCONSUMO_DATA_TO"), 0) != AV56TFHistoricoConsumo_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_valor Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_VALOR"), ",", ".") != AV61TFHistoricoConsumo_Valor )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfhistoricoconsumo_valor_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_VALOR_TO"), ",", ".") != AV62TFHistoricoConsumo_Valor_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E25MG2 */
                                       E25MG2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEMG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMG2( ) ;
            }
         }
      }

      protected void PAMG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_272( ) ;
         while ( nGXsfl_27_idx <= nRC_GXsfl_27 )
         {
            sendrow_272( ) ;
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV31TFHistoricoConsumo_Codigo ,
                                       int AV32TFHistoricoConsumo_Codigo_To ,
                                       int AV35TFHistoricoConsumo_SaldoContratoCod ,
                                       int AV36TFHistoricoConsumo_SaldoContratoCod_To ,
                                       int AV39TFHistoricoConsumo_NotaEmpenhoCod ,
                                       int AV40TFHistoricoConsumo_NotaEmpenhoCod_To ,
                                       int AV43TFHistoricoConsumo_ContagemResultadoCod ,
                                       int AV44TFHistoricoConsumo_ContagemResultadoCod_To ,
                                       int AV47TFHistoricoConsumo_ContratoCod ,
                                       int AV48TFHistoricoConsumo_ContratoCod_To ,
                                       int AV51TFHistoricoConsumo_UsuarioCod ,
                                       int AV52TFHistoricoConsumo_UsuarioCod_To ,
                                       DateTime AV55TFHistoricoConsumo_Data ,
                                       DateTime AV56TFHistoricoConsumo_Data_To ,
                                       decimal AV61TFHistoricoConsumo_Valor ,
                                       decimal AV62TFHistoricoConsumo_Valor_To ,
                                       String AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace ,
                                       String AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace ,
                                       String AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace ,
                                       String AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace ,
                                       String AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace ,
                                       String AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace ,
                                       String AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFMG2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_SALDOCONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_SALDOCONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_NOTAEMPENHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_NOTAEMPENHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_CONTAGEMRESULTADOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_DATA", GetSecureSignedToken( "", context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_DATA", context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "HISTORICOCONSUMO_VALOR", StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFMG2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 27;
         /* Execute user event: E23MG2 */
         E23MG2 ();
         nGXsfl_27_idx = 1;
         sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
         SubsflControlProps_272( ) ;
         nGXsfl_27_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_272( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV31TFHistoricoConsumo_Codigo ,
                                                 AV32TFHistoricoConsumo_Codigo_To ,
                                                 AV35TFHistoricoConsumo_SaldoContratoCod ,
                                                 AV36TFHistoricoConsumo_SaldoContratoCod_To ,
                                                 AV39TFHistoricoConsumo_NotaEmpenhoCod ,
                                                 AV40TFHistoricoConsumo_NotaEmpenhoCod_To ,
                                                 AV43TFHistoricoConsumo_ContagemResultadoCod ,
                                                 AV44TFHistoricoConsumo_ContagemResultadoCod_To ,
                                                 AV47TFHistoricoConsumo_ContratoCod ,
                                                 AV48TFHistoricoConsumo_ContratoCod_To ,
                                                 AV51TFHistoricoConsumo_UsuarioCod ,
                                                 AV52TFHistoricoConsumo_UsuarioCod_To ,
                                                 AV55TFHistoricoConsumo_Data ,
                                                 AV56TFHistoricoConsumo_Data_To ,
                                                 AV61TFHistoricoConsumo_Valor ,
                                                 AV62TFHistoricoConsumo_Valor_To ,
                                                 A1562HistoricoConsumo_Codigo ,
                                                 A1580HistoricoConsumo_SaldoContratoCod ,
                                                 A1581HistoricoConsumo_NotaEmpenhoCod ,
                                                 A1582HistoricoConsumo_ContagemResultadoCod ,
                                                 A1579HistoricoConsumo_ContratoCod ,
                                                 A1563HistoricoConsumo_UsuarioCod ,
                                                 A1577HistoricoConsumo_Data ,
                                                 A1578HistoricoConsumo_Valor ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00MG2 */
            pr_default.execute(0, new Object[] {AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_27_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1578HistoricoConsumo_Valor = H00MG2_A1578HistoricoConsumo_Valor[0];
               A1577HistoricoConsumo_Data = H00MG2_A1577HistoricoConsumo_Data[0];
               A1563HistoricoConsumo_UsuarioCod = H00MG2_A1563HistoricoConsumo_UsuarioCod[0];
               n1563HistoricoConsumo_UsuarioCod = H00MG2_n1563HistoricoConsumo_UsuarioCod[0];
               A1579HistoricoConsumo_ContratoCod = H00MG2_A1579HistoricoConsumo_ContratoCod[0];
               n1579HistoricoConsumo_ContratoCod = H00MG2_n1579HistoricoConsumo_ContratoCod[0];
               A1582HistoricoConsumo_ContagemResultadoCod = H00MG2_A1582HistoricoConsumo_ContagemResultadoCod[0];
               n1582HistoricoConsumo_ContagemResultadoCod = H00MG2_n1582HistoricoConsumo_ContagemResultadoCod[0];
               A1581HistoricoConsumo_NotaEmpenhoCod = H00MG2_A1581HistoricoConsumo_NotaEmpenhoCod[0];
               n1581HistoricoConsumo_NotaEmpenhoCod = H00MG2_n1581HistoricoConsumo_NotaEmpenhoCod[0];
               A1580HistoricoConsumo_SaldoContratoCod = H00MG2_A1580HistoricoConsumo_SaldoContratoCod[0];
               A1562HistoricoConsumo_Codigo = H00MG2_A1562HistoricoConsumo_Codigo[0];
               /* Execute user event: E24MG2 */
               E24MG2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 27;
            WBMG0( ) ;
         }
         nGXsfl_27_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV31TFHistoricoConsumo_Codigo ,
                                              AV32TFHistoricoConsumo_Codigo_To ,
                                              AV35TFHistoricoConsumo_SaldoContratoCod ,
                                              AV36TFHistoricoConsumo_SaldoContratoCod_To ,
                                              AV39TFHistoricoConsumo_NotaEmpenhoCod ,
                                              AV40TFHistoricoConsumo_NotaEmpenhoCod_To ,
                                              AV43TFHistoricoConsumo_ContagemResultadoCod ,
                                              AV44TFHistoricoConsumo_ContagemResultadoCod_To ,
                                              AV47TFHistoricoConsumo_ContratoCod ,
                                              AV48TFHistoricoConsumo_ContratoCod_To ,
                                              AV51TFHistoricoConsumo_UsuarioCod ,
                                              AV52TFHistoricoConsumo_UsuarioCod_To ,
                                              AV55TFHistoricoConsumo_Data ,
                                              AV56TFHistoricoConsumo_Data_To ,
                                              AV61TFHistoricoConsumo_Valor ,
                                              AV62TFHistoricoConsumo_Valor_To ,
                                              A1562HistoricoConsumo_Codigo ,
                                              A1580HistoricoConsumo_SaldoContratoCod ,
                                              A1581HistoricoConsumo_NotaEmpenhoCod ,
                                              A1582HistoricoConsumo_ContagemResultadoCod ,
                                              A1579HistoricoConsumo_ContratoCod ,
                                              A1563HistoricoConsumo_UsuarioCod ,
                                              A1577HistoricoConsumo_Data ,
                                              A1578HistoricoConsumo_Valor ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00MG3 */
         pr_default.execute(1, new Object[] {AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To});
         GRID_nRecordCount = H00MG3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To, AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To, AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To, AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To, AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV31TFHistoricoConsumo_Codigo, AV32TFHistoricoConsumo_Codigo_To, AV35TFHistoricoConsumo_SaldoContratoCod, AV36TFHistoricoConsumo_SaldoContratoCod_To, AV39TFHistoricoConsumo_NotaEmpenhoCod, AV40TFHistoricoConsumo_NotaEmpenhoCod_To, AV43TFHistoricoConsumo_ContagemResultadoCod, AV44TFHistoricoConsumo_ContagemResultadoCod_To, AV47TFHistoricoConsumo_ContratoCod, AV48TFHistoricoConsumo_ContratoCod_To, AV51TFHistoricoConsumo_UsuarioCod, AV52TFHistoricoConsumo_UsuarioCod_To, AV55TFHistoricoConsumo_Data, AV56TFHistoricoConsumo_Data_To, AV61TFHistoricoConsumo_Valor, AV62TFHistoricoConsumo_Valor_To, AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUPMG0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22MG2 */
         E22MG2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV64DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_CODIGOTITLEFILTERDATA"), AV30HistoricoConsumo_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_SALDOCONTRATOCODTITLEFILTERDATA"), AV34HistoricoConsumo_SaldoContratoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_NOTAEMPENHOCODTITLEFILTERDATA"), AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLEFILTERDATA"), AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_CONTRATOCODTITLEFILTERDATA"), AV46HistoricoConsumo_ContratoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_USUARIOCODTITLEFILTERDATA"), AV50HistoricoConsumo_UsuarioCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_DATATITLEFILTERDATA"), AV54HistoricoConsumo_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vHISTORICOCONSUMO_VALORTITLEFILTERDATA"), AV60HistoricoConsumo_ValorTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_CODIGO");
               GX_FocusControl = edtavTfhistoricoconsumo_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFHistoricoConsumo_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFHistoricoConsumo_Codigo), 6, 0)));
            }
            else
            {
               AV31TFHistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFHistoricoConsumo_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_CODIGO_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFHistoricoConsumo_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFHistoricoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFHistoricoConsumo_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFHistoricoConsumo_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFHistoricoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFHistoricoConsumo_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_saldocontratocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_saldocontratocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_SALDOCONTRATOCOD");
               GX_FocusControl = edtavTfhistoricoconsumo_saldocontratocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFHistoricoConsumo_SaldoContratoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFHistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), 6, 0)));
            }
            else
            {
               AV35TFHistoricoConsumo_SaldoContratoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_saldocontratocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFHistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_saldocontratocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_saldocontratocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_saldocontratocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFHistoricoConsumo_SaldoContratoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFHistoricoConsumo_SaldoContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), 6, 0)));
            }
            else
            {
               AV36TFHistoricoConsumo_SaldoContratoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_saldocontratocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFHistoricoConsumo_SaldoContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_notaempenhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_notaempenhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_NOTAEMPENHOCOD");
               GX_FocusControl = edtavTfhistoricoconsumo_notaempenhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFHistoricoConsumo_NotaEmpenhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFHistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), 6, 0)));
            }
            else
            {
               AV39TFHistoricoConsumo_NotaEmpenhoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_notaempenhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFHistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_notaempenhocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_notaempenhocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_notaempenhocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFHistoricoConsumo_NotaEmpenhoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFHistoricoConsumo_NotaEmpenhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), 6, 0)));
            }
            else
            {
               AV40TFHistoricoConsumo_NotaEmpenhoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_notaempenhocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFHistoricoConsumo_NotaEmpenhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contagemresultadocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contagemresultadocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD");
               GX_FocusControl = edtavTfhistoricoconsumo_contagemresultadocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFHistoricoConsumo_ContagemResultadoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFHistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), 6, 0)));
            }
            else
            {
               AV43TFHistoricoConsumo_ContagemResultadoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contagemresultadocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFHistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFHistoricoConsumo_ContagemResultadoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFHistoricoConsumo_ContagemResultadoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), 6, 0)));
            }
            else
            {
               AV44TFHistoricoConsumo_ContagemResultadoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFHistoricoConsumo_ContagemResultadoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contratocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contratocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_CONTRATOCOD");
               GX_FocusControl = edtavTfhistoricoconsumo_contratocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFHistoricoConsumo_ContratoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFHistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFHistoricoConsumo_ContratoCod), 6, 0)));
            }
            else
            {
               AV47TFHistoricoConsumo_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contratocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFHistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFHistoricoConsumo_ContratoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contratocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contratocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_CONTRATOCOD_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_contratocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFHistoricoConsumo_ContratoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFHistoricoConsumo_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), 6, 0)));
            }
            else
            {
               AV48TFHistoricoConsumo_ContratoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_contratocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFHistoricoConsumo_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_usuariocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_usuariocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_USUARIOCOD");
               GX_FocusControl = edtavTfhistoricoconsumo_usuariocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFHistoricoConsumo_UsuarioCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFHistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), 6, 0)));
            }
            else
            {
               AV51TFHistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_usuariocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFHistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_usuariocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_usuariocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_USUARIOCOD_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_usuariocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFHistoricoConsumo_UsuarioCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFHistoricoConsumo_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), 6, 0)));
            }
            else
            {
               AV52TFHistoricoConsumo_UsuarioCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_usuariocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFHistoricoConsumo_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfhistoricoconsumo_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFHistorico Consumo_Data"}), 1, "vTFHISTORICOCONSUMO_DATA");
               GX_FocusControl = edtavTfhistoricoconsumo_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFHistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFHistoricoConsumo_Data", context.localUtil.TToC( AV55TFHistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV55TFHistoricoConsumo_Data = context.localUtil.CToT( cgiGet( edtavTfhistoricoconsumo_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFHistoricoConsumo_Data", context.localUtil.TToC( AV55TFHistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfhistoricoconsumo_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFHistorico Consumo_Data_To"}), 1, "vTFHISTORICOCONSUMO_DATA_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFHistoricoConsumo_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFHistoricoConsumo_Data_To", context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV56TFHistoricoConsumo_Data_To = context.localUtil.CToT( cgiGet( edtavTfhistoricoconsumo_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFHistoricoConsumo_Data_To", context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_historicoconsumo_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Historico Consumo_Data Aux Date"}), 1, "vDDO_HISTORICOCONSUMO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_historicoconsumo_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57DDO_HistoricoConsumo_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_HistoricoConsumo_DataAuxDate", context.localUtil.Format(AV57DDO_HistoricoConsumo_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV57DDO_HistoricoConsumo_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_historicoconsumo_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_HistoricoConsumo_DataAuxDate", context.localUtil.Format(AV57DDO_HistoricoConsumo_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_historicoconsumo_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Historico Consumo_Data Aux Date To"}), 1, "vDDO_HISTORICOCONSUMO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_historicoconsumo_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58DDO_HistoricoConsumo_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58DDO_HistoricoConsumo_DataAuxDateTo", context.localUtil.Format(AV58DDO_HistoricoConsumo_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV58DDO_HistoricoConsumo_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_historicoconsumo_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58DDO_HistoricoConsumo_DataAuxDateTo", context.localUtil.Format(AV58DDO_HistoricoConsumo_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_VALOR");
               GX_FocusControl = edtavTfhistoricoconsumo_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFHistoricoConsumo_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFHistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV61TFHistoricoConsumo_Valor, 18, 5)));
            }
            else
            {
               AV61TFHistoricoConsumo_Valor = context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFHistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV61TFHistoricoConsumo_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFHISTORICOCONSUMO_VALOR_TO");
               GX_FocusControl = edtavTfhistoricoconsumo_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFHistoricoConsumo_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFHistoricoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV62TFHistoricoConsumo_Valor_To, 18, 5)));
            }
            else
            {
               AV62TFHistoricoConsumo_Valor_To = context.localUtil.CToN( cgiGet( edtavTfhistoricoconsumo_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFHistoricoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV62TFHistoricoConsumo_Valor_To, 18, 5)));
            }
            AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace", AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace);
            AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace", AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace);
            AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace", AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace);
            AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace", AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace);
            AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace", AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace);
            AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace", AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace);
            AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace", AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace);
            AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace = cgiGet( edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace", AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_27 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_27"), ",", "."));
            AV66GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV67GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_historicoconsumo_codigo_Caption = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Caption");
            Ddo_historicoconsumo_codigo_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Tooltip");
            Ddo_historicoconsumo_codigo_Cls = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Cls");
            Ddo_historicoconsumo_codigo_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Filteredtext_set");
            Ddo_historicoconsumo_codigo_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Filteredtextto_set");
            Ddo_historicoconsumo_codigo_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Dropdownoptionstype");
            Ddo_historicoconsumo_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Includesortasc"));
            Ddo_historicoconsumo_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Includesortdsc"));
            Ddo_historicoconsumo_codigo_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Sortedstatus");
            Ddo_historicoconsumo_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Includefilter"));
            Ddo_historicoconsumo_codigo_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Filtertype");
            Ddo_historicoconsumo_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Filterisrange"));
            Ddo_historicoconsumo_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Includedatalist"));
            Ddo_historicoconsumo_codigo_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Sortasc");
            Ddo_historicoconsumo_codigo_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Sortdsc");
            Ddo_historicoconsumo_codigo_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Cleanfilter");
            Ddo_historicoconsumo_codigo_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Rangefilterfrom");
            Ddo_historicoconsumo_codigo_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Rangefilterto");
            Ddo_historicoconsumo_codigo_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Searchbuttontext");
            Ddo_historicoconsumo_saldocontratocod_Caption = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Caption");
            Ddo_historicoconsumo_saldocontratocod_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Tooltip");
            Ddo_historicoconsumo_saldocontratocod_Cls = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Cls");
            Ddo_historicoconsumo_saldocontratocod_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtext_set");
            Ddo_historicoconsumo_saldocontratocod_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtextto_set");
            Ddo_historicoconsumo_saldocontratocod_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Dropdownoptionstype");
            Ddo_historicoconsumo_saldocontratocod_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_saldocontratocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includesortasc"));
            Ddo_historicoconsumo_saldocontratocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includesortdsc"));
            Ddo_historicoconsumo_saldocontratocod_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Sortedstatus");
            Ddo_historicoconsumo_saldocontratocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includefilter"));
            Ddo_historicoconsumo_saldocontratocod_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filtertype");
            Ddo_historicoconsumo_saldocontratocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filterisrange"));
            Ddo_historicoconsumo_saldocontratocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Includedatalist"));
            Ddo_historicoconsumo_saldocontratocod_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Sortasc");
            Ddo_historicoconsumo_saldocontratocod_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Sortdsc");
            Ddo_historicoconsumo_saldocontratocod_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Cleanfilter");
            Ddo_historicoconsumo_saldocontratocod_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Rangefilterfrom");
            Ddo_historicoconsumo_saldocontratocod_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Rangefilterto");
            Ddo_historicoconsumo_saldocontratocod_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Searchbuttontext");
            Ddo_historicoconsumo_notaempenhocod_Caption = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Caption");
            Ddo_historicoconsumo_notaempenhocod_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Tooltip");
            Ddo_historicoconsumo_notaempenhocod_Cls = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Cls");
            Ddo_historicoconsumo_notaempenhocod_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtext_set");
            Ddo_historicoconsumo_notaempenhocod_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtextto_set");
            Ddo_historicoconsumo_notaempenhocod_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Dropdownoptionstype");
            Ddo_historicoconsumo_notaempenhocod_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_notaempenhocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includesortasc"));
            Ddo_historicoconsumo_notaempenhocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includesortdsc"));
            Ddo_historicoconsumo_notaempenhocod_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Sortedstatus");
            Ddo_historicoconsumo_notaempenhocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includefilter"));
            Ddo_historicoconsumo_notaempenhocod_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filtertype");
            Ddo_historicoconsumo_notaempenhocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filterisrange"));
            Ddo_historicoconsumo_notaempenhocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Includedatalist"));
            Ddo_historicoconsumo_notaempenhocod_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Sortasc");
            Ddo_historicoconsumo_notaempenhocod_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Sortdsc");
            Ddo_historicoconsumo_notaempenhocod_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Cleanfilter");
            Ddo_historicoconsumo_notaempenhocod_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Rangefilterfrom");
            Ddo_historicoconsumo_notaempenhocod_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Rangefilterto");
            Ddo_historicoconsumo_notaempenhocod_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Searchbuttontext");
            Ddo_historicoconsumo_contagemresultadocod_Caption = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Caption");
            Ddo_historicoconsumo_contagemresultadocod_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Tooltip");
            Ddo_historicoconsumo_contagemresultadocod_Cls = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Cls");
            Ddo_historicoconsumo_contagemresultadocod_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtext_set");
            Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtextto_set");
            Ddo_historicoconsumo_contagemresultadocod_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Dropdownoptionstype");
            Ddo_historicoconsumo_contagemresultadocod_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_contagemresultadocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includesortasc"));
            Ddo_historicoconsumo_contagemresultadocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includesortdsc"));
            Ddo_historicoconsumo_contagemresultadocod_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Sortedstatus");
            Ddo_historicoconsumo_contagemresultadocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includefilter"));
            Ddo_historicoconsumo_contagemresultadocod_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filtertype");
            Ddo_historicoconsumo_contagemresultadocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filterisrange"));
            Ddo_historicoconsumo_contagemresultadocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Includedatalist"));
            Ddo_historicoconsumo_contagemresultadocod_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Sortasc");
            Ddo_historicoconsumo_contagemresultadocod_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Sortdsc");
            Ddo_historicoconsumo_contagemresultadocod_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Cleanfilter");
            Ddo_historicoconsumo_contagemresultadocod_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Rangefilterfrom");
            Ddo_historicoconsumo_contagemresultadocod_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Rangefilterto");
            Ddo_historicoconsumo_contagemresultadocod_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Searchbuttontext");
            Ddo_historicoconsumo_contratocod_Caption = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Caption");
            Ddo_historicoconsumo_contratocod_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Tooltip");
            Ddo_historicoconsumo_contratocod_Cls = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Cls");
            Ddo_historicoconsumo_contratocod_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtext_set");
            Ddo_historicoconsumo_contratocod_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtextto_set");
            Ddo_historicoconsumo_contratocod_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Dropdownoptionstype");
            Ddo_historicoconsumo_contratocod_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_contratocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includesortasc"));
            Ddo_historicoconsumo_contratocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includesortdsc"));
            Ddo_historicoconsumo_contratocod_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Sortedstatus");
            Ddo_historicoconsumo_contratocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includefilter"));
            Ddo_historicoconsumo_contratocod_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filtertype");
            Ddo_historicoconsumo_contratocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filterisrange"));
            Ddo_historicoconsumo_contratocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Includedatalist"));
            Ddo_historicoconsumo_contratocod_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Sortasc");
            Ddo_historicoconsumo_contratocod_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Sortdsc");
            Ddo_historicoconsumo_contratocod_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Cleanfilter");
            Ddo_historicoconsumo_contratocod_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Rangefilterfrom");
            Ddo_historicoconsumo_contratocod_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Rangefilterto");
            Ddo_historicoconsumo_contratocod_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Searchbuttontext");
            Ddo_historicoconsumo_usuariocod_Caption = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Caption");
            Ddo_historicoconsumo_usuariocod_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Tooltip");
            Ddo_historicoconsumo_usuariocod_Cls = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Cls");
            Ddo_historicoconsumo_usuariocod_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtext_set");
            Ddo_historicoconsumo_usuariocod_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtextto_set");
            Ddo_historicoconsumo_usuariocod_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Dropdownoptionstype");
            Ddo_historicoconsumo_usuariocod_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_usuariocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Includesortasc"));
            Ddo_historicoconsumo_usuariocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Includesortdsc"));
            Ddo_historicoconsumo_usuariocod_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Sortedstatus");
            Ddo_historicoconsumo_usuariocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Includefilter"));
            Ddo_historicoconsumo_usuariocod_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Filtertype");
            Ddo_historicoconsumo_usuariocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Filterisrange"));
            Ddo_historicoconsumo_usuariocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Includedatalist"));
            Ddo_historicoconsumo_usuariocod_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Sortasc");
            Ddo_historicoconsumo_usuariocod_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Sortdsc");
            Ddo_historicoconsumo_usuariocod_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Cleanfilter");
            Ddo_historicoconsumo_usuariocod_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Rangefilterfrom");
            Ddo_historicoconsumo_usuariocod_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Rangefilterto");
            Ddo_historicoconsumo_usuariocod_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Searchbuttontext");
            Ddo_historicoconsumo_data_Caption = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Caption");
            Ddo_historicoconsumo_data_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Tooltip");
            Ddo_historicoconsumo_data_Cls = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Cls");
            Ddo_historicoconsumo_data_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Filteredtext_set");
            Ddo_historicoconsumo_data_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Filteredtextto_set");
            Ddo_historicoconsumo_data_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Dropdownoptionstype");
            Ddo_historicoconsumo_data_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_DATA_Includesortasc"));
            Ddo_historicoconsumo_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_DATA_Includesortdsc"));
            Ddo_historicoconsumo_data_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Sortedstatus");
            Ddo_historicoconsumo_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_DATA_Includefilter"));
            Ddo_historicoconsumo_data_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Filtertype");
            Ddo_historicoconsumo_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_DATA_Filterisrange"));
            Ddo_historicoconsumo_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_DATA_Includedatalist"));
            Ddo_historicoconsumo_data_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Sortasc");
            Ddo_historicoconsumo_data_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Sortdsc");
            Ddo_historicoconsumo_data_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Cleanfilter");
            Ddo_historicoconsumo_data_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Rangefilterfrom");
            Ddo_historicoconsumo_data_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Rangefilterto");
            Ddo_historicoconsumo_data_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Searchbuttontext");
            Ddo_historicoconsumo_valor_Caption = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Caption");
            Ddo_historicoconsumo_valor_Tooltip = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Tooltip");
            Ddo_historicoconsumo_valor_Cls = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Cls");
            Ddo_historicoconsumo_valor_Filteredtext_set = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Filteredtext_set");
            Ddo_historicoconsumo_valor_Filteredtextto_set = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Filteredtextto_set");
            Ddo_historicoconsumo_valor_Dropdownoptionstype = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Dropdownoptionstype");
            Ddo_historicoconsumo_valor_Titlecontrolidtoreplace = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Titlecontrolidtoreplace");
            Ddo_historicoconsumo_valor_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Includesortasc"));
            Ddo_historicoconsumo_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Includesortdsc"));
            Ddo_historicoconsumo_valor_Sortedstatus = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Sortedstatus");
            Ddo_historicoconsumo_valor_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Includefilter"));
            Ddo_historicoconsumo_valor_Filtertype = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Filtertype");
            Ddo_historicoconsumo_valor_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Filterisrange"));
            Ddo_historicoconsumo_valor_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Includedatalist"));
            Ddo_historicoconsumo_valor_Sortasc = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Sortasc");
            Ddo_historicoconsumo_valor_Sortdsc = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Sortdsc");
            Ddo_historicoconsumo_valor_Cleanfilter = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Cleanfilter");
            Ddo_historicoconsumo_valor_Rangefilterfrom = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Rangefilterfrom");
            Ddo_historicoconsumo_valor_Rangefilterto = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Rangefilterto");
            Ddo_historicoconsumo_valor_Searchbuttontext = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_historicoconsumo_codigo_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Activeeventkey");
            Ddo_historicoconsumo_codigo_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Filteredtext_get");
            Ddo_historicoconsumo_codigo_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_CODIGO_Filteredtextto_get");
            Ddo_historicoconsumo_saldocontratocod_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Activeeventkey");
            Ddo_historicoconsumo_saldocontratocod_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtext_get");
            Ddo_historicoconsumo_saldocontratocod_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD_Filteredtextto_get");
            Ddo_historicoconsumo_notaempenhocod_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Activeeventkey");
            Ddo_historicoconsumo_notaempenhocod_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtext_get");
            Ddo_historicoconsumo_notaempenhocod_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD_Filteredtextto_get");
            Ddo_historicoconsumo_contagemresultadocod_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Activeeventkey");
            Ddo_historicoconsumo_contagemresultadocod_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtext_get");
            Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_Filteredtextto_get");
            Ddo_historicoconsumo_contratocod_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Activeeventkey");
            Ddo_historicoconsumo_contratocod_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtext_get");
            Ddo_historicoconsumo_contratocod_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_CONTRATOCOD_Filteredtextto_get");
            Ddo_historicoconsumo_usuariocod_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Activeeventkey");
            Ddo_historicoconsumo_usuariocod_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtext_get");
            Ddo_historicoconsumo_usuariocod_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_USUARIOCOD_Filteredtextto_get");
            Ddo_historicoconsumo_data_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Activeeventkey");
            Ddo_historicoconsumo_data_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Filteredtext_get");
            Ddo_historicoconsumo_data_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_DATA_Filteredtextto_get");
            Ddo_historicoconsumo_valor_Activeeventkey = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Activeeventkey");
            Ddo_historicoconsumo_valor_Filteredtext_get = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Filteredtext_get");
            Ddo_historicoconsumo_valor_Filteredtextto_get = cgiGet( "DDO_HISTORICOCONSUMO_VALOR_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFHistoricoConsumo_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFHistoricoConsumo_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_SALDOCONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV35TFHistoricoConsumo_SaldoContratoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFHistoricoConsumo_SaldoContratoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_NOTAEMPENHOCOD"), ",", ".") != Convert.ToDecimal( AV39TFHistoricoConsumo_NotaEmpenhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV40TFHistoricoConsumo_NotaEmpenhoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD"), ",", ".") != Convert.ToDecimal( AV43TFHistoricoConsumo_ContagemResultadoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO"), ",", ".") != Convert.ToDecimal( AV44TFHistoricoConsumo_ContagemResultadoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTRATOCOD"), ",", ".") != Convert.ToDecimal( AV47TFHistoricoConsumo_ContratoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_CONTRATOCOD_TO"), ",", ".") != Convert.ToDecimal( AV48TFHistoricoConsumo_ContratoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_USUARIOCOD"), ",", ".") != Convert.ToDecimal( AV51TFHistoricoConsumo_UsuarioCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_USUARIOCOD_TO"), ",", ".") != Convert.ToDecimal( AV52TFHistoricoConsumo_UsuarioCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFHISTORICOCONSUMO_DATA"), 0) != AV55TFHistoricoConsumo_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFHISTORICOCONSUMO_DATA_TO"), 0) != AV56TFHistoricoConsumo_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_VALOR"), ",", ".") != AV61TFHistoricoConsumo_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFHISTORICOCONSUMO_VALOR_TO"), ",", ".") != AV62TFHistoricoConsumo_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22MG2 */
         E22MG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22MG2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfhistoricoconsumo_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_codigo_Visible), 5, 0)));
         edtavTfhistoricoconsumo_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_codigo_to_Visible), 5, 0)));
         edtavTfhistoricoconsumo_saldocontratocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_saldocontratocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_saldocontratocod_Visible), 5, 0)));
         edtavTfhistoricoconsumo_saldocontratocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_saldocontratocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_saldocontratocod_to_Visible), 5, 0)));
         edtavTfhistoricoconsumo_notaempenhocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_notaempenhocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_notaempenhocod_Visible), 5, 0)));
         edtavTfhistoricoconsumo_notaempenhocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_notaempenhocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_notaempenhocod_to_Visible), 5, 0)));
         edtavTfhistoricoconsumo_contagemresultadocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_contagemresultadocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_contagemresultadocod_Visible), 5, 0)));
         edtavTfhistoricoconsumo_contagemresultadocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_contagemresultadocod_to_Visible), 5, 0)));
         edtavTfhistoricoconsumo_contratocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_contratocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_contratocod_Visible), 5, 0)));
         edtavTfhistoricoconsumo_contratocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_contratocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_contratocod_to_Visible), 5, 0)));
         edtavTfhistoricoconsumo_usuariocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_usuariocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_usuariocod_Visible), 5, 0)));
         edtavTfhistoricoconsumo_usuariocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_usuariocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_usuariocod_to_Visible), 5, 0)));
         edtavTfhistoricoconsumo_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_data_Visible), 5, 0)));
         edtavTfhistoricoconsumo_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_data_to_Visible), 5, 0)));
         edtavTfhistoricoconsumo_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_valor_Visible), 5, 0)));
         edtavTfhistoricoconsumo_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfhistoricoconsumo_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfhistoricoconsumo_valor_to_Visible), 5, 0)));
         Ddo_historicoconsumo_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_codigo_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_codigo_Titlecontrolidtoreplace);
         AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace = Ddo_historicoconsumo_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace", AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace);
         edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_historicoconsumo_saldocontratocod_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_SaldoContratoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_saldocontratocod_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_saldocontratocod_Titlecontrolidtoreplace);
         AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace = Ddo_historicoconsumo_saldocontratocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace", AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace);
         edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_historicoconsumo_notaempenhocod_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_NotaEmpenhoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_notaempenhocod_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_notaempenhocod_Titlecontrolidtoreplace);
         AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace = Ddo_historicoconsumo_notaempenhocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace", AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace);
         edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_historicoconsumo_contagemresultadocod_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_ContagemResultadoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contagemresultadocod_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_contagemresultadocod_Titlecontrolidtoreplace);
         AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace = Ddo_historicoconsumo_contagemresultadocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace", AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace);
         edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_historicoconsumo_contratocod_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_ContratoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contratocod_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_contratocod_Titlecontrolidtoreplace);
         AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace = Ddo_historicoconsumo_contratocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace", AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace);
         edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_historicoconsumo_usuariocod_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_UsuarioCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_usuariocod_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_usuariocod_Titlecontrolidtoreplace);
         AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace = Ddo_historicoconsumo_usuariocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace", AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace);
         edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_historicoconsumo_data_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_data_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_data_Titlecontrolidtoreplace);
         AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace = Ddo_historicoconsumo_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace", AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace);
         edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_historicoconsumo_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_HistoricoConsumo_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_valor_Internalname, "TitleControlIdToReplace", Ddo_historicoconsumo_valor_Titlecontrolidtoreplace);
         AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace = Ddo_historicoconsumo_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace", AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace);
         edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Historico Consumo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Consumo_Codigo", 0);
         cmbavOrderedby.addItem("2", "Contrato Cod", 0);
         cmbavOrderedby.addItem("3", "Empenho Cod", 0);
         cmbavOrderedby.addItem("4", "Resultado Cod", 0);
         cmbavOrderedby.addItem("5", "C�digo", 0);
         cmbavOrderedby.addItem("6", "C�digo", 0);
         cmbavOrderedby.addItem("7", "Data", 0);
         cmbavOrderedby.addItem("8", "Valor", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV64DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV64DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E23MG2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30HistoricoConsumo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34HistoricoConsumo_SaldoContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46HistoricoConsumo_ContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50HistoricoConsumo_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54HistoricoConsumo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60HistoricoConsumo_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtHistoricoConsumo_Codigo_Titleformat = 2;
         edtHistoricoConsumo_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Consumo_Codigo", AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Codigo_Internalname, "Title", edtHistoricoConsumo_Codigo_Title);
         edtHistoricoConsumo_SaldoContratoCod_Titleformat = 2;
         edtHistoricoConsumo_SaldoContratoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato Cod", AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_SaldoContratoCod_Internalname, "Title", edtHistoricoConsumo_SaldoContratoCod_Title);
         edtHistoricoConsumo_NotaEmpenhoCod_Titleformat = 2;
         edtHistoricoConsumo_NotaEmpenhoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Empenho Cod", AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_NotaEmpenhoCod_Internalname, "Title", edtHistoricoConsumo_NotaEmpenhoCod_Title);
         edtHistoricoConsumo_ContagemResultadoCod_Titleformat = 2;
         edtHistoricoConsumo_ContagemResultadoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Resultado Cod", AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_ContagemResultadoCod_Internalname, "Title", edtHistoricoConsumo_ContagemResultadoCod_Title);
         edtHistoricoConsumo_ContratoCod_Titleformat = 2;
         edtHistoricoConsumo_ContratoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_ContratoCod_Internalname, "Title", edtHistoricoConsumo_ContratoCod_Title);
         edtHistoricoConsumo_UsuarioCod_Titleformat = 2;
         edtHistoricoConsumo_UsuarioCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_UsuarioCod_Internalname, "Title", edtHistoricoConsumo_UsuarioCod_Title);
         edtHistoricoConsumo_Data_Titleformat = 2;
         edtHistoricoConsumo_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Data_Internalname, "Title", edtHistoricoConsumo_Data_Title);
         edtHistoricoConsumo_Valor_Titleformat = 2;
         edtHistoricoConsumo_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtHistoricoConsumo_Valor_Internalname, "Title", edtHistoricoConsumo_Valor_Title);
         AV66GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66GridCurrentPage), 10, 0)));
         AV67GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30HistoricoConsumo_CodigoTitleFilterData", AV30HistoricoConsumo_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34HistoricoConsumo_SaldoContratoCodTitleFilterData", AV34HistoricoConsumo_SaldoContratoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData", AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData", AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46HistoricoConsumo_ContratoCodTitleFilterData", AV46HistoricoConsumo_ContratoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50HistoricoConsumo_UsuarioCodTitleFilterData", AV50HistoricoConsumo_UsuarioCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54HistoricoConsumo_DataTitleFilterData", AV54HistoricoConsumo_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60HistoricoConsumo_ValorTitleFilterData", AV60HistoricoConsumo_ValorTitleFilterData);
      }

      protected void E11MG2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV65PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV65PageToGo) ;
         }
      }

      protected void E12MG2( )
      {
         /* Ddo_historicoconsumo_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_codigo_Internalname, "SortedStatus", Ddo_historicoconsumo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_codigo_Internalname, "SortedStatus", Ddo_historicoconsumo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFHistoricoConsumo_Codigo = (int)(NumberUtil.Val( Ddo_historicoconsumo_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFHistoricoConsumo_Codigo), 6, 0)));
            AV32TFHistoricoConsumo_Codigo_To = (int)(NumberUtil.Val( Ddo_historicoconsumo_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFHistoricoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFHistoricoConsumo_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13MG2( )
      {
         /* Ddo_historicoconsumo_saldocontratocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_saldocontratocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_saldocontratocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_saldocontratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_saldocontratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_saldocontratocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_saldocontratocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_saldocontratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_saldocontratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_saldocontratocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFHistoricoConsumo_SaldoContratoCod = (int)(NumberUtil.Val( Ddo_historicoconsumo_saldocontratocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFHistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), 6, 0)));
            AV36TFHistoricoConsumo_SaldoContratoCod_To = (int)(NumberUtil.Val( Ddo_historicoconsumo_saldocontratocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFHistoricoConsumo_SaldoContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14MG2( )
      {
         /* Ddo_historicoconsumo_notaempenhocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_notaempenhocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_notaempenhocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_notaempenhocod_Internalname, "SortedStatus", Ddo_historicoconsumo_notaempenhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_notaempenhocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_notaempenhocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_notaempenhocod_Internalname, "SortedStatus", Ddo_historicoconsumo_notaempenhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_notaempenhocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFHistoricoConsumo_NotaEmpenhoCod = (int)(NumberUtil.Val( Ddo_historicoconsumo_notaempenhocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFHistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), 6, 0)));
            AV40TFHistoricoConsumo_NotaEmpenhoCod_To = (int)(NumberUtil.Val( Ddo_historicoconsumo_notaempenhocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFHistoricoConsumo_NotaEmpenhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15MG2( )
      {
         /* Ddo_historicoconsumo_contagemresultadocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_contagemresultadocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_contagemresultadocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contagemresultadocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contagemresultadocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_contagemresultadocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_contagemresultadocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contagemresultadocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contagemresultadocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_contagemresultadocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFHistoricoConsumo_ContagemResultadoCod = (int)(NumberUtil.Val( Ddo_historicoconsumo_contagemresultadocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFHistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), 6, 0)));
            AV44TFHistoricoConsumo_ContagemResultadoCod_To = (int)(NumberUtil.Val( Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFHistoricoConsumo_ContagemResultadoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16MG2( )
      {
         /* Ddo_historicoconsumo_contratocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_contratocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_contratocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_contratocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_contratocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contratocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_contratocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFHistoricoConsumo_ContratoCod = (int)(NumberUtil.Val( Ddo_historicoconsumo_contratocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFHistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFHistoricoConsumo_ContratoCod), 6, 0)));
            AV48TFHistoricoConsumo_ContratoCod_To = (int)(NumberUtil.Val( Ddo_historicoconsumo_contratocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFHistoricoConsumo_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17MG2( )
      {
         /* Ddo_historicoconsumo_usuariocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_usuariocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_usuariocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_usuariocod_Internalname, "SortedStatus", Ddo_historicoconsumo_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_usuariocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_usuariocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_usuariocod_Internalname, "SortedStatus", Ddo_historicoconsumo_usuariocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_usuariocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFHistoricoConsumo_UsuarioCod = (int)(NumberUtil.Val( Ddo_historicoconsumo_usuariocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFHistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), 6, 0)));
            AV52TFHistoricoConsumo_UsuarioCod_To = (int)(NumberUtil.Val( Ddo_historicoconsumo_usuariocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFHistoricoConsumo_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18MG2( )
      {
         /* Ddo_historicoconsumo_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_data_Internalname, "SortedStatus", Ddo_historicoconsumo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_data_Internalname, "SortedStatus", Ddo_historicoconsumo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFHistoricoConsumo_Data = context.localUtil.CToT( Ddo_historicoconsumo_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFHistoricoConsumo_Data", context.localUtil.TToC( AV55TFHistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
            AV56TFHistoricoConsumo_Data_To = context.localUtil.CToT( Ddo_historicoconsumo_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFHistoricoConsumo_Data_To", context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV56TFHistoricoConsumo_Data_To) )
            {
               AV56TFHistoricoConsumo_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV56TFHistoricoConsumo_Data_To)), (short)(DateTimeUtil.Month( AV56TFHistoricoConsumo_Data_To)), (short)(DateTimeUtil.Day( AV56TFHistoricoConsumo_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFHistoricoConsumo_Data_To", context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19MG2( )
      {
         /* Ddo_historicoconsumo_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_historicoconsumo_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_valor_Internalname, "SortedStatus", Ddo_historicoconsumo_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_historicoconsumo_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_valor_Internalname, "SortedStatus", Ddo_historicoconsumo_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_historicoconsumo_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFHistoricoConsumo_Valor = NumberUtil.Val( Ddo_historicoconsumo_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFHistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV61TFHistoricoConsumo_Valor, 18, 5)));
            AV62TFHistoricoConsumo_Valor_To = NumberUtil.Val( Ddo_historicoconsumo_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFHistoricoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV62TFHistoricoConsumo_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E24MG2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV70Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 27;
         }
         sendrow_272( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_27_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(27, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E25MG2 */
         E25MG2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25MG2( )
      {
         /* Enter Routine */
         AV7InOutHistoricoConsumo_Codigo = A1562HistoricoConsumo_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutHistoricoConsumo_Codigo), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutHistoricoConsumo_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E20MG2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21MG2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
      }

      protected void S122( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_historicoconsumo_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_codigo_Internalname, "SortedStatus", Ddo_historicoconsumo_codigo_Sortedstatus);
         Ddo_historicoconsumo_saldocontratocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_saldocontratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_saldocontratocod_Sortedstatus);
         Ddo_historicoconsumo_notaempenhocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_notaempenhocod_Internalname, "SortedStatus", Ddo_historicoconsumo_notaempenhocod_Sortedstatus);
         Ddo_historicoconsumo_contagemresultadocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contagemresultadocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contagemresultadocod_Sortedstatus);
         Ddo_historicoconsumo_contratocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contratocod_Sortedstatus);
         Ddo_historicoconsumo_usuariocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_usuariocod_Internalname, "SortedStatus", Ddo_historicoconsumo_usuariocod_Sortedstatus);
         Ddo_historicoconsumo_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_data_Internalname, "SortedStatus", Ddo_historicoconsumo_data_Sortedstatus);
         Ddo_historicoconsumo_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_valor_Internalname, "SortedStatus", Ddo_historicoconsumo_valor_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_historicoconsumo_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_codigo_Internalname, "SortedStatus", Ddo_historicoconsumo_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_historicoconsumo_saldocontratocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_saldocontratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_saldocontratocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_historicoconsumo_notaempenhocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_notaempenhocod_Internalname, "SortedStatus", Ddo_historicoconsumo_notaempenhocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_historicoconsumo_contagemresultadocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contagemresultadocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contagemresultadocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_historicoconsumo_contratocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contratocod_Internalname, "SortedStatus", Ddo_historicoconsumo_contratocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_historicoconsumo_usuariocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_usuariocod_Internalname, "SortedStatus", Ddo_historicoconsumo_usuariocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_historicoconsumo_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_data_Internalname, "SortedStatus", Ddo_historicoconsumo_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_historicoconsumo_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_valor_Internalname, "SortedStatus", Ddo_historicoconsumo_valor_Sortedstatus);
         }
      }

      protected void S132( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFHistoricoConsumo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFHistoricoConsumo_Codigo), 6, 0)));
         Ddo_historicoconsumo_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_codigo_Internalname, "FilteredText_set", Ddo_historicoconsumo_codigo_Filteredtext_set);
         AV32TFHistoricoConsumo_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFHistoricoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFHistoricoConsumo_Codigo_To), 6, 0)));
         Ddo_historicoconsumo_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_codigo_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_codigo_Filteredtextto_set);
         AV35TFHistoricoConsumo_SaldoContratoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFHistoricoConsumo_SaldoContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFHistoricoConsumo_SaldoContratoCod), 6, 0)));
         Ddo_historicoconsumo_saldocontratocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_saldocontratocod_Internalname, "FilteredText_set", Ddo_historicoconsumo_saldocontratocod_Filteredtext_set);
         AV36TFHistoricoConsumo_SaldoContratoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFHistoricoConsumo_SaldoContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFHistoricoConsumo_SaldoContratoCod_To), 6, 0)));
         Ddo_historicoconsumo_saldocontratocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_saldocontratocod_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_saldocontratocod_Filteredtextto_set);
         AV39TFHistoricoConsumo_NotaEmpenhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFHistoricoConsumo_NotaEmpenhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFHistoricoConsumo_NotaEmpenhoCod), 6, 0)));
         Ddo_historicoconsumo_notaempenhocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_notaempenhocod_Internalname, "FilteredText_set", Ddo_historicoconsumo_notaempenhocod_Filteredtext_set);
         AV40TFHistoricoConsumo_NotaEmpenhoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFHistoricoConsumo_NotaEmpenhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFHistoricoConsumo_NotaEmpenhoCod_To), 6, 0)));
         Ddo_historicoconsumo_notaempenhocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_notaempenhocod_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_notaempenhocod_Filteredtextto_set);
         AV43TFHistoricoConsumo_ContagemResultadoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFHistoricoConsumo_ContagemResultadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFHistoricoConsumo_ContagemResultadoCod), 6, 0)));
         Ddo_historicoconsumo_contagemresultadocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contagemresultadocod_Internalname, "FilteredText_set", Ddo_historicoconsumo_contagemresultadocod_Filteredtext_set);
         AV44TFHistoricoConsumo_ContagemResultadoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFHistoricoConsumo_ContagemResultadoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFHistoricoConsumo_ContagemResultadoCod_To), 6, 0)));
         Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contagemresultadocod_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_set);
         AV47TFHistoricoConsumo_ContratoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFHistoricoConsumo_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFHistoricoConsumo_ContratoCod), 6, 0)));
         Ddo_historicoconsumo_contratocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contratocod_Internalname, "FilteredText_set", Ddo_historicoconsumo_contratocod_Filteredtext_set);
         AV48TFHistoricoConsumo_ContratoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFHistoricoConsumo_ContratoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFHistoricoConsumo_ContratoCod_To), 6, 0)));
         Ddo_historicoconsumo_contratocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_contratocod_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_contratocod_Filteredtextto_set);
         AV51TFHistoricoConsumo_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFHistoricoConsumo_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFHistoricoConsumo_UsuarioCod), 6, 0)));
         Ddo_historicoconsumo_usuariocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_usuariocod_Internalname, "FilteredText_set", Ddo_historicoconsumo_usuariocod_Filteredtext_set);
         AV52TFHistoricoConsumo_UsuarioCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFHistoricoConsumo_UsuarioCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFHistoricoConsumo_UsuarioCod_To), 6, 0)));
         Ddo_historicoconsumo_usuariocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_usuariocod_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_usuariocod_Filteredtextto_set);
         AV55TFHistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFHistoricoConsumo_Data", context.localUtil.TToC( AV55TFHistoricoConsumo_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_historicoconsumo_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_data_Internalname, "FilteredText_set", Ddo_historicoconsumo_data_Filteredtext_set);
         AV56TFHistoricoConsumo_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFHistoricoConsumo_Data_To", context.localUtil.TToC( AV56TFHistoricoConsumo_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_historicoconsumo_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_data_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_data_Filteredtextto_set);
         AV61TFHistoricoConsumo_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFHistoricoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV61TFHistoricoConsumo_Valor, 18, 5)));
         Ddo_historicoconsumo_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_valor_Internalname, "FilteredText_set", Ddo_historicoconsumo_valor_Filteredtext_set);
         AV62TFHistoricoConsumo_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFHistoricoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV62TFHistoricoConsumo_Valor_To, 18, 5)));
         Ddo_historicoconsumo_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_historicoconsumo_valor_Internalname, "FilteredTextTo_set", Ddo_historicoconsumo_valor_Filteredtextto_set);
      }

      protected void wb_table1_2_MG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_MG2( true) ;
         }
         else
         {
            wb_table2_5_MG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_MG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_21_MG2( true) ;
         }
         else
         {
            wb_table3_21_MG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_21_MG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MG2e( true) ;
         }
         else
         {
            wb_table1_2_MG2e( false) ;
         }
      }

      protected void wb_table3_21_MG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_24_MG2( true) ;
         }
         else
         {
            wb_table4_24_MG2( false) ;
         }
         return  ;
      }

      protected void wb_table4_24_MG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_21_MG2e( true) ;
         }
         else
         {
            wb_table3_21_MG2e( false) ;
         }
      }

      protected void wb_table4_24_MG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"27\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_SaldoContratoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_SaldoContratoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_SaldoContratoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_NotaEmpenhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_NotaEmpenhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_NotaEmpenhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_ContagemResultadoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_ContagemResultadoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_ContagemResultadoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_ContratoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_ContratoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_ContratoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_UsuarioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_UsuarioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_UsuarioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_SaldoContratoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_SaldoContratoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_NotaEmpenhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_NotaEmpenhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_ContagemResultadoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_ContagemResultadoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_ContratoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_ContratoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_UsuarioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_UsuarioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 27 )
         {
            wbEnd = 0;
            nRC_GXsfl_27 = (short)(nGXsfl_27_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_24_MG2e( true) ;
         }
         else
         {
            wb_table4_24_MG2e( false) ;
         }
      }

      protected void wb_table2_5_MG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptHistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_27_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptHistoricoConsumo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptHistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_MG2( true) ;
         }
         else
         {
            wb_table5_14_MG2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_MG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_MG2e( true) ;
         }
         else
         {
            wb_table2_5_MG2e( false) ;
         }
      }

      protected void wb_table5_14_MG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptHistoricoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_MG2e( true) ;
         }
         else
         {
            wb_table5_14_MG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutHistoricoConsumo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutHistoricoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutHistoricoConsumo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMG2( ) ;
         WSMG2( ) ;
         WEMG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031193164");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("prompthistoricoconsumo.js", "?202031193164");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_272( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_27_idx;
         edtHistoricoConsumo_Codigo_Internalname = "HISTORICOCONSUMO_CODIGO_"+sGXsfl_27_idx;
         edtHistoricoConsumo_SaldoContratoCod_Internalname = "HISTORICOCONSUMO_SALDOCONTRATOCOD_"+sGXsfl_27_idx;
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = "HISTORICOCONSUMO_NOTAEMPENHOCOD_"+sGXsfl_27_idx;
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = "HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_"+sGXsfl_27_idx;
         edtHistoricoConsumo_ContratoCod_Internalname = "HISTORICOCONSUMO_CONTRATOCOD_"+sGXsfl_27_idx;
         edtHistoricoConsumo_UsuarioCod_Internalname = "HISTORICOCONSUMO_USUARIOCOD_"+sGXsfl_27_idx;
         edtHistoricoConsumo_Data_Internalname = "HISTORICOCONSUMO_DATA_"+sGXsfl_27_idx;
         edtHistoricoConsumo_Valor_Internalname = "HISTORICOCONSUMO_VALOR_"+sGXsfl_27_idx;
      }

      protected void SubsflControlProps_fel_272( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_Codigo_Internalname = "HISTORICOCONSUMO_CODIGO_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_SaldoContratoCod_Internalname = "HISTORICOCONSUMO_SALDOCONTRATOCOD_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = "HISTORICOCONSUMO_NOTAEMPENHOCOD_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = "HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_ContratoCod_Internalname = "HISTORICOCONSUMO_CONTRATOCOD_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_UsuarioCod_Internalname = "HISTORICOCONSUMO_USUARIOCOD_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_Data_Internalname = "HISTORICOCONSUMO_DATA_"+sGXsfl_27_fel_idx;
         edtHistoricoConsumo_Valor_Internalname = "HISTORICOCONSUMO_VALOR_"+sGXsfl_27_fel_idx;
      }

      protected void sendrow_272( )
      {
         SubsflControlProps_272( ) ;
         WBMG0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_27_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_27_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_27_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 28,'',false,'',27)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV70Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_27_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_SaldoContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_SaldoContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_NotaEmpenhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_ContagemResultadoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_ContagemResultadoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_Data_Internalname,context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ",", "")),context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_CODIGO"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_SALDOCONTRATOCOD"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_NOTAEMPENHOCOD"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_CONTRATOCOD"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_USUARIOCOD"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_DATA"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_HISTORICOCONSUMO_VALOR"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sGXsfl_27_idx, context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         /* End function sendrow_272 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtHistoricoConsumo_Codigo_Internalname = "HISTORICOCONSUMO_CODIGO";
         edtHistoricoConsumo_SaldoContratoCod_Internalname = "HISTORICOCONSUMO_SALDOCONTRATOCOD";
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = "HISTORICOCONSUMO_NOTAEMPENHOCOD";
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = "HISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         edtHistoricoConsumo_ContratoCod_Internalname = "HISTORICOCONSUMO_CONTRATOCOD";
         edtHistoricoConsumo_UsuarioCod_Internalname = "HISTORICOCONSUMO_USUARIOCOD";
         edtHistoricoConsumo_Data_Internalname = "HISTORICOCONSUMO_DATA";
         edtHistoricoConsumo_Valor_Internalname = "HISTORICOCONSUMO_VALOR";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfhistoricoconsumo_codigo_Internalname = "vTFHISTORICOCONSUMO_CODIGO";
         edtavTfhistoricoconsumo_codigo_to_Internalname = "vTFHISTORICOCONSUMO_CODIGO_TO";
         edtavTfhistoricoconsumo_saldocontratocod_Internalname = "vTFHISTORICOCONSUMO_SALDOCONTRATOCOD";
         edtavTfhistoricoconsumo_saldocontratocod_to_Internalname = "vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO";
         edtavTfhistoricoconsumo_notaempenhocod_Internalname = "vTFHISTORICOCONSUMO_NOTAEMPENHOCOD";
         edtavTfhistoricoconsumo_notaempenhocod_to_Internalname = "vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO";
         edtavTfhistoricoconsumo_contagemresultadocod_Internalname = "vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname = "vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO";
         edtavTfhistoricoconsumo_contratocod_Internalname = "vTFHISTORICOCONSUMO_CONTRATOCOD";
         edtavTfhistoricoconsumo_contratocod_to_Internalname = "vTFHISTORICOCONSUMO_CONTRATOCOD_TO";
         edtavTfhistoricoconsumo_usuariocod_Internalname = "vTFHISTORICOCONSUMO_USUARIOCOD";
         edtavTfhistoricoconsumo_usuariocod_to_Internalname = "vTFHISTORICOCONSUMO_USUARIOCOD_TO";
         edtavTfhistoricoconsumo_data_Internalname = "vTFHISTORICOCONSUMO_DATA";
         edtavTfhistoricoconsumo_data_to_Internalname = "vTFHISTORICOCONSUMO_DATA_TO";
         edtavDdo_historicoconsumo_dataauxdate_Internalname = "vDDO_HISTORICOCONSUMO_DATAAUXDATE";
         edtavDdo_historicoconsumo_dataauxdateto_Internalname = "vDDO_HISTORICOCONSUMO_DATAAUXDATETO";
         divDdo_historicoconsumo_dataauxdates_Internalname = "DDO_HISTORICOCONSUMO_DATAAUXDATES";
         edtavTfhistoricoconsumo_valor_Internalname = "vTFHISTORICOCONSUMO_VALOR";
         edtavTfhistoricoconsumo_valor_to_Internalname = "vTFHISTORICOCONSUMO_VALOR_TO";
         Ddo_historicoconsumo_codigo_Internalname = "DDO_HISTORICOCONSUMO_CODIGO";
         edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_historicoconsumo_saldocontratocod_Internalname = "DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD";
         edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE";
         Ddo_historicoconsumo_notaempenhocod_Internalname = "DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD";
         edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE";
         Ddo_historicoconsumo_contagemresultadocod_Internalname = "DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE";
         Ddo_historicoconsumo_contratocod_Internalname = "DDO_HISTORICOCONSUMO_CONTRATOCOD";
         edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE";
         Ddo_historicoconsumo_usuariocod_Internalname = "DDO_HISTORICOCONSUMO_USUARIOCOD";
         edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE";
         Ddo_historicoconsumo_data_Internalname = "DDO_HISTORICOCONSUMO_DATA";
         edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE";
         Ddo_historicoconsumo_valor_Internalname = "DDO_HISTORICOCONSUMO_VALOR";
         edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Internalname = "vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtHistoricoConsumo_Valor_Jsonclick = "";
         edtHistoricoConsumo_Data_Jsonclick = "";
         edtHistoricoConsumo_UsuarioCod_Jsonclick = "";
         edtHistoricoConsumo_ContratoCod_Jsonclick = "";
         edtHistoricoConsumo_ContagemResultadoCod_Jsonclick = "";
         edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick = "";
         edtHistoricoConsumo_SaldoContratoCod_Jsonclick = "";
         edtHistoricoConsumo_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtHistoricoConsumo_Valor_Titleformat = 0;
         edtHistoricoConsumo_Data_Titleformat = 0;
         edtHistoricoConsumo_UsuarioCod_Titleformat = 0;
         edtHistoricoConsumo_ContratoCod_Titleformat = 0;
         edtHistoricoConsumo_ContagemResultadoCod_Titleformat = 0;
         edtHistoricoConsumo_NotaEmpenhoCod_Titleformat = 0;
         edtHistoricoConsumo_SaldoContratoCod_Titleformat = 0;
         edtHistoricoConsumo_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtHistoricoConsumo_Valor_Title = "Valor";
         edtHistoricoConsumo_Data_Title = "Data";
         edtHistoricoConsumo_UsuarioCod_Title = "C�digo";
         edtHistoricoConsumo_ContratoCod_Title = "C�digo";
         edtHistoricoConsumo_ContagemResultadoCod_Title = "Resultado Cod";
         edtHistoricoConsumo_NotaEmpenhoCod_Title = "Empenho Cod";
         edtHistoricoConsumo_SaldoContratoCod_Title = "Contrato Cod";
         edtHistoricoConsumo_Codigo_Title = "Consumo_Codigo";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfhistoricoconsumo_valor_to_Jsonclick = "";
         edtavTfhistoricoconsumo_valor_to_Visible = 1;
         edtavTfhistoricoconsumo_valor_Jsonclick = "";
         edtavTfhistoricoconsumo_valor_Visible = 1;
         edtavDdo_historicoconsumo_dataauxdateto_Jsonclick = "";
         edtavDdo_historicoconsumo_dataauxdate_Jsonclick = "";
         edtavTfhistoricoconsumo_data_to_Jsonclick = "";
         edtavTfhistoricoconsumo_data_to_Visible = 1;
         edtavTfhistoricoconsumo_data_Jsonclick = "";
         edtavTfhistoricoconsumo_data_Visible = 1;
         edtavTfhistoricoconsumo_usuariocod_to_Jsonclick = "";
         edtavTfhistoricoconsumo_usuariocod_to_Visible = 1;
         edtavTfhistoricoconsumo_usuariocod_Jsonclick = "";
         edtavTfhistoricoconsumo_usuariocod_Visible = 1;
         edtavTfhistoricoconsumo_contratocod_to_Jsonclick = "";
         edtavTfhistoricoconsumo_contratocod_to_Visible = 1;
         edtavTfhistoricoconsumo_contratocod_Jsonclick = "";
         edtavTfhistoricoconsumo_contratocod_Visible = 1;
         edtavTfhistoricoconsumo_contagemresultadocod_to_Jsonclick = "";
         edtavTfhistoricoconsumo_contagemresultadocod_to_Visible = 1;
         edtavTfhistoricoconsumo_contagemresultadocod_Jsonclick = "";
         edtavTfhistoricoconsumo_contagemresultadocod_Visible = 1;
         edtavTfhistoricoconsumo_notaempenhocod_to_Jsonclick = "";
         edtavTfhistoricoconsumo_notaempenhocod_to_Visible = 1;
         edtavTfhistoricoconsumo_notaempenhocod_Jsonclick = "";
         edtavTfhistoricoconsumo_notaempenhocod_Visible = 1;
         edtavTfhistoricoconsumo_saldocontratocod_to_Jsonclick = "";
         edtavTfhistoricoconsumo_saldocontratocod_to_Visible = 1;
         edtavTfhistoricoconsumo_saldocontratocod_Jsonclick = "";
         edtavTfhistoricoconsumo_saldocontratocod_Visible = 1;
         edtavTfhistoricoconsumo_codigo_to_Jsonclick = "";
         edtavTfhistoricoconsumo_codigo_to_Visible = 1;
         edtavTfhistoricoconsumo_codigo_Jsonclick = "";
         edtavTfhistoricoconsumo_codigo_Visible = 1;
         Ddo_historicoconsumo_valor_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_valor_Rangefilterto = "At�";
         Ddo_historicoconsumo_valor_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_valor_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_valor_Filtertype = "Numeric";
         Ddo_historicoconsumo_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_valor_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_valor_Cls = "ColumnSettings";
         Ddo_historicoconsumo_valor_Tooltip = "Op��es";
         Ddo_historicoconsumo_valor_Caption = "";
         Ddo_historicoconsumo_data_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_data_Rangefilterto = "At�";
         Ddo_historicoconsumo_data_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_data_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_data_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_data_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_data_Filtertype = "Date";
         Ddo_historicoconsumo_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_data_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_data_Cls = "ColumnSettings";
         Ddo_historicoconsumo_data_Tooltip = "Op��es";
         Ddo_historicoconsumo_data_Caption = "";
         Ddo_historicoconsumo_usuariocod_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_usuariocod_Rangefilterto = "At�";
         Ddo_historicoconsumo_usuariocod_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_usuariocod_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_usuariocod_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_usuariocod_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_usuariocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_usuariocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_usuariocod_Filtertype = "Numeric";
         Ddo_historicoconsumo_usuariocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_usuariocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_usuariocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_usuariocod_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_usuariocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_usuariocod_Cls = "ColumnSettings";
         Ddo_historicoconsumo_usuariocod_Tooltip = "Op��es";
         Ddo_historicoconsumo_usuariocod_Caption = "";
         Ddo_historicoconsumo_contratocod_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_contratocod_Rangefilterto = "At�";
         Ddo_historicoconsumo_contratocod_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_contratocod_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_contratocod_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_contratocod_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_contratocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_contratocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contratocod_Filtertype = "Numeric";
         Ddo_historicoconsumo_contratocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contratocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contratocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contratocod_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_contratocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_contratocod_Cls = "ColumnSettings";
         Ddo_historicoconsumo_contratocod_Tooltip = "Op��es";
         Ddo_historicoconsumo_contratocod_Caption = "";
         Ddo_historicoconsumo_contagemresultadocod_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_contagemresultadocod_Rangefilterto = "At�";
         Ddo_historicoconsumo_contagemresultadocod_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_contagemresultadocod_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_contagemresultadocod_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_contagemresultadocod_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_contagemresultadocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_contagemresultadocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contagemresultadocod_Filtertype = "Numeric";
         Ddo_historicoconsumo_contagemresultadocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contagemresultadocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contagemresultadocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_contagemresultadocod_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_contagemresultadocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_contagemresultadocod_Cls = "ColumnSettings";
         Ddo_historicoconsumo_contagemresultadocod_Tooltip = "Op��es";
         Ddo_historicoconsumo_contagemresultadocod_Caption = "";
         Ddo_historicoconsumo_notaempenhocod_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_notaempenhocod_Rangefilterto = "At�";
         Ddo_historicoconsumo_notaempenhocod_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_notaempenhocod_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_notaempenhocod_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_notaempenhocod_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_notaempenhocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_notaempenhocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_notaempenhocod_Filtertype = "Numeric";
         Ddo_historicoconsumo_notaempenhocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_notaempenhocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_notaempenhocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_notaempenhocod_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_notaempenhocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_notaempenhocod_Cls = "ColumnSettings";
         Ddo_historicoconsumo_notaempenhocod_Tooltip = "Op��es";
         Ddo_historicoconsumo_notaempenhocod_Caption = "";
         Ddo_historicoconsumo_saldocontratocod_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_saldocontratocod_Rangefilterto = "At�";
         Ddo_historicoconsumo_saldocontratocod_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_saldocontratocod_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_saldocontratocod_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_saldocontratocod_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_saldocontratocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_saldocontratocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_saldocontratocod_Filtertype = "Numeric";
         Ddo_historicoconsumo_saldocontratocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_saldocontratocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_saldocontratocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_saldocontratocod_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_saldocontratocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_saldocontratocod_Cls = "ColumnSettings";
         Ddo_historicoconsumo_saldocontratocod_Tooltip = "Op��es";
         Ddo_historicoconsumo_saldocontratocod_Caption = "";
         Ddo_historicoconsumo_codigo_Searchbuttontext = "Pesquisar";
         Ddo_historicoconsumo_codigo_Rangefilterto = "At�";
         Ddo_historicoconsumo_codigo_Rangefilterfrom = "Desde";
         Ddo_historicoconsumo_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_historicoconsumo_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_historicoconsumo_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_historicoconsumo_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_historicoconsumo_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_codigo_Filtertype = "Numeric";
         Ddo_historicoconsumo_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_historicoconsumo_codigo_Titlecontrolidtoreplace = "";
         Ddo_historicoconsumo_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_historicoconsumo_codigo_Cls = "ColumnSettings";
         Ddo_historicoconsumo_codigo_Tooltip = "Op��es";
         Ddo_historicoconsumo_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Historico Consumo";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV30HistoricoConsumo_CodigoTitleFilterData',fld:'vHISTORICOCONSUMO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34HistoricoConsumo_SaldoContratoCodTitleFilterData',fld:'vHISTORICOCONSUMO_SALDOCONTRATOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData',fld:'vHISTORICOCONSUMO_NOTAEMPENHOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData',fld:'vHISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV46HistoricoConsumo_ContratoCodTitleFilterData',fld:'vHISTORICOCONSUMO_CONTRATOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV50HistoricoConsumo_UsuarioCodTitleFilterData',fld:'vHISTORICOCONSUMO_USUARIOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV54HistoricoConsumo_DataTitleFilterData',fld:'vHISTORICOCONSUMO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV60HistoricoConsumo_ValorTitleFilterData',fld:'vHISTORICOCONSUMO_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'edtHistoricoConsumo_Codigo_Titleformat',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtHistoricoConsumo_Codigo_Title',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Title'},{av:'edtHistoricoConsumo_SaldoContratoCod_Titleformat',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_SaldoContratoCod_Title',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Title'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Titleformat',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Title',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Title'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Titleformat',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Title',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Title'},{av:'edtHistoricoConsumo_ContratoCod_Titleformat',ctrl:'HISTORICOCONSUMO_CONTRATOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_ContratoCod_Title',ctrl:'HISTORICOCONSUMO_CONTRATOCOD',prop:'Title'},{av:'edtHistoricoConsumo_UsuarioCod_Titleformat',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_UsuarioCod_Title',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Title'},{av:'edtHistoricoConsumo_Data_Titleformat',ctrl:'HISTORICOCONSUMO_DATA',prop:'Titleformat'},{av:'edtHistoricoConsumo_Data_Title',ctrl:'HISTORICOCONSUMO_DATA',prop:'Title'},{av:'edtHistoricoConsumo_Valor_Titleformat',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Titleformat'},{av:'edtHistoricoConsumo_Valor_Title',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Title'},{av:'AV66GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_CODIGO.ONOPTIONCLICKED","{handler:'E12MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_codigo_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_codigo_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_codigo_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD.ONOPTIONCLICKED","{handler:'E13MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_saldocontratocod_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_saldocontratocod_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_saldocontratocod_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD.ONOPTIONCLICKED","{handler:'E14MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_notaempenhocod_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_notaempenhocod_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_notaempenhocod_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD.ONOPTIONCLICKED","{handler:'E15MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_contagemresultadocod_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_contagemresultadocod_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_CONTRATOCOD.ONOPTIONCLICKED","{handler:'E16MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_contratocod_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_contratocod_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_contratocod_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_USUARIOCOD.ONOPTIONCLICKED","{handler:'E17MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_usuariocod_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_usuariocod_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_usuariocod_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_DATA.ONOPTIONCLICKED","{handler:'E18MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_data_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_data_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_data_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_HISTORICOCONSUMO_VALOR.ONOPTIONCLICKED","{handler:'E19MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_historicoconsumo_valor_Activeeventkey',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'ActiveEventKey'},{av:'Ddo_historicoconsumo_valor_Filteredtext_get',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'FilteredText_get'},{av:'Ddo_historicoconsumo_valor_Filteredtextto_get',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_historicoconsumo_valor_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'SortedStatus'},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_historicoconsumo_codigo_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_saldocontratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_notaempenhocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contagemresultadocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_contratocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_usuariocod_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'SortedStatus'},{av:'Ddo_historicoconsumo_data_Sortedstatus',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E24MG2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E25MG2',iparms:[{av:'A1562HistoricoConsumo_Codigo',fld:'HISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutHistoricoConsumo_Codigo',fld:'vINOUTHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E20MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21MG2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_SALDOCONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_NOTAEMPENHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_CONTRATOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_USUARIOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_HISTORICOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV31TFHistoricoConsumo_Codigo',fld:'vTFHISTORICOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_codigo_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'FilteredText_set'},{av:'AV32TFHistoricoConsumo_Codigo_To',fld:'vTFHISTORICOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_codigo_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFHistoricoConsumo_SaldoContratoCod',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_saldocontratocod_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'FilteredText_set'},{av:'AV36TFHistoricoConsumo_SaldoContratoCod_To',fld:'vTFHISTORICOCONSUMO_SALDOCONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_saldocontratocod_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'FilteredTextTo_set'},{av:'AV39TFHistoricoConsumo_NotaEmpenhoCod',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_notaempenhocod_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'FilteredText_set'},{av:'AV40TFHistoricoConsumo_NotaEmpenhoCod_To',fld:'vTFHISTORICOCONSUMO_NOTAEMPENHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_notaempenhocod_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'FilteredTextTo_set'},{av:'AV43TFHistoricoConsumo_ContagemResultadoCod',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_contagemresultadocod_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'FilteredText_set'},{av:'AV44TFHistoricoConsumo_ContagemResultadoCod_To',fld:'vTFHISTORICOCONSUMO_CONTAGEMRESULTADOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'FilteredTextTo_set'},{av:'AV47TFHistoricoConsumo_ContratoCod',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_contratocod_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'FilteredText_set'},{av:'AV48TFHistoricoConsumo_ContratoCod_To',fld:'vTFHISTORICOCONSUMO_CONTRATOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_contratocod_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_CONTRATOCOD',prop:'FilteredTextTo_set'},{av:'AV51TFHistoricoConsumo_UsuarioCod',fld:'vTFHISTORICOCONSUMO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_usuariocod_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'FilteredText_set'},{av:'AV52TFHistoricoConsumo_UsuarioCod_To',fld:'vTFHISTORICOCONSUMO_USUARIOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_historicoconsumo_usuariocod_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_USUARIOCOD',prop:'FilteredTextTo_set'},{av:'AV55TFHistoricoConsumo_Data',fld:'vTFHISTORICOCONSUMO_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_historicoconsumo_data_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'FilteredText_set'},{av:'AV56TFHistoricoConsumo_Data_To',fld:'vTFHISTORICOCONSUMO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_historicoconsumo_data_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_DATA',prop:'FilteredTextTo_set'},{av:'AV61TFHistoricoConsumo_Valor',fld:'vTFHISTORICOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_historicoconsumo_valor_Filteredtext_set',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'FilteredText_set'},{av:'AV62TFHistoricoConsumo_Valor_To',fld:'vTFHISTORICOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_historicoconsumo_valor_Filteredtextto_set',ctrl:'DDO_HISTORICOCONSUMO_VALOR',prop:'FilteredTextTo_set'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_historicoconsumo_codigo_Activeeventkey = "";
         Ddo_historicoconsumo_codigo_Filteredtext_get = "";
         Ddo_historicoconsumo_codigo_Filteredtextto_get = "";
         Ddo_historicoconsumo_saldocontratocod_Activeeventkey = "";
         Ddo_historicoconsumo_saldocontratocod_Filteredtext_get = "";
         Ddo_historicoconsumo_saldocontratocod_Filteredtextto_get = "";
         Ddo_historicoconsumo_notaempenhocod_Activeeventkey = "";
         Ddo_historicoconsumo_notaempenhocod_Filteredtext_get = "";
         Ddo_historicoconsumo_notaempenhocod_Filteredtextto_get = "";
         Ddo_historicoconsumo_contagemresultadocod_Activeeventkey = "";
         Ddo_historicoconsumo_contagemresultadocod_Filteredtext_get = "";
         Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_get = "";
         Ddo_historicoconsumo_contratocod_Activeeventkey = "";
         Ddo_historicoconsumo_contratocod_Filteredtext_get = "";
         Ddo_historicoconsumo_contratocod_Filteredtextto_get = "";
         Ddo_historicoconsumo_usuariocod_Activeeventkey = "";
         Ddo_historicoconsumo_usuariocod_Filteredtext_get = "";
         Ddo_historicoconsumo_usuariocod_Filteredtextto_get = "";
         Ddo_historicoconsumo_data_Activeeventkey = "";
         Ddo_historicoconsumo_data_Filteredtext_get = "";
         Ddo_historicoconsumo_data_Filteredtextto_get = "";
         Ddo_historicoconsumo_valor_Activeeventkey = "";
         Ddo_historicoconsumo_valor_Filteredtext_get = "";
         Ddo_historicoconsumo_valor_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV55TFHistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         AV56TFHistoricoConsumo_Data_To = (DateTime)(DateTime.MinValue);
         AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace = "";
         AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace = "";
         AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace = "";
         AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace = "";
         AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace = "";
         AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace = "";
         AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace = "";
         AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV64DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30HistoricoConsumo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34HistoricoConsumo_SaldoContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46HistoricoConsumo_ContratoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50HistoricoConsumo_UsuarioCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54HistoricoConsumo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60HistoricoConsumo_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_historicoconsumo_codigo_Filteredtext_set = "";
         Ddo_historicoconsumo_codigo_Filteredtextto_set = "";
         Ddo_historicoconsumo_codigo_Sortedstatus = "";
         Ddo_historicoconsumo_saldocontratocod_Filteredtext_set = "";
         Ddo_historicoconsumo_saldocontratocod_Filteredtextto_set = "";
         Ddo_historicoconsumo_saldocontratocod_Sortedstatus = "";
         Ddo_historicoconsumo_notaempenhocod_Filteredtext_set = "";
         Ddo_historicoconsumo_notaempenhocod_Filteredtextto_set = "";
         Ddo_historicoconsumo_notaempenhocod_Sortedstatus = "";
         Ddo_historicoconsumo_contagemresultadocod_Filteredtext_set = "";
         Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_set = "";
         Ddo_historicoconsumo_contagemresultadocod_Sortedstatus = "";
         Ddo_historicoconsumo_contratocod_Filteredtext_set = "";
         Ddo_historicoconsumo_contratocod_Filteredtextto_set = "";
         Ddo_historicoconsumo_contratocod_Sortedstatus = "";
         Ddo_historicoconsumo_usuariocod_Filteredtext_set = "";
         Ddo_historicoconsumo_usuariocod_Filteredtextto_set = "";
         Ddo_historicoconsumo_usuariocod_Sortedstatus = "";
         Ddo_historicoconsumo_data_Filteredtext_set = "";
         Ddo_historicoconsumo_data_Filteredtextto_set = "";
         Ddo_historicoconsumo_data_Sortedstatus = "";
         Ddo_historicoconsumo_valor_Filteredtext_set = "";
         Ddo_historicoconsumo_valor_Filteredtextto_set = "";
         Ddo_historicoconsumo_valor_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         AV57DDO_HistoricoConsumo_DataAuxDate = DateTime.MinValue;
         AV58DDO_HistoricoConsumo_DataAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV70Select_GXI = "";
         A1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00MG2_A1578HistoricoConsumo_Valor = new decimal[1] ;
         H00MG2_A1577HistoricoConsumo_Data = new DateTime[] {DateTime.MinValue} ;
         H00MG2_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         H00MG2_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         H00MG2_A1579HistoricoConsumo_ContratoCod = new int[1] ;
         H00MG2_n1579HistoricoConsumo_ContratoCod = new bool[] {false} ;
         H00MG2_A1582HistoricoConsumo_ContagemResultadoCod = new int[1] ;
         H00MG2_n1582HistoricoConsumo_ContagemResultadoCod = new bool[] {false} ;
         H00MG2_A1581HistoricoConsumo_NotaEmpenhoCod = new int[1] ;
         H00MG2_n1581HistoricoConsumo_NotaEmpenhoCod = new bool[] {false} ;
         H00MG2_A1580HistoricoConsumo_SaldoContratoCod = new int[1] ;
         H00MG2_A1562HistoricoConsumo_Codigo = new int[1] ;
         H00MG3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prompthistoricoconsumo__default(),
            new Object[][] {
                new Object[] {
               H00MG2_A1578HistoricoConsumo_Valor, H00MG2_A1577HistoricoConsumo_Data, H00MG2_A1563HistoricoConsumo_UsuarioCod, H00MG2_n1563HistoricoConsumo_UsuarioCod, H00MG2_A1579HistoricoConsumo_ContratoCod, H00MG2_n1579HistoricoConsumo_ContratoCod, H00MG2_A1582HistoricoConsumo_ContagemResultadoCod, H00MG2_n1582HistoricoConsumo_ContagemResultadoCod, H00MG2_A1581HistoricoConsumo_NotaEmpenhoCod, H00MG2_n1581HistoricoConsumo_NotaEmpenhoCod,
               H00MG2_A1580HistoricoConsumo_SaldoContratoCod, H00MG2_A1562HistoricoConsumo_Codigo
               }
               , new Object[] {
               H00MG3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_27 ;
      private short nGXsfl_27_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_27_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtHistoricoConsumo_Codigo_Titleformat ;
      private short edtHistoricoConsumo_SaldoContratoCod_Titleformat ;
      private short edtHistoricoConsumo_NotaEmpenhoCod_Titleformat ;
      private short edtHistoricoConsumo_ContagemResultadoCod_Titleformat ;
      private short edtHistoricoConsumo_ContratoCod_Titleformat ;
      private short edtHistoricoConsumo_UsuarioCod_Titleformat ;
      private short edtHistoricoConsumo_Data_Titleformat ;
      private short edtHistoricoConsumo_Valor_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutHistoricoConsumo_Codigo ;
      private int wcpOAV7InOutHistoricoConsumo_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFHistoricoConsumo_Codigo ;
      private int AV32TFHistoricoConsumo_Codigo_To ;
      private int AV35TFHistoricoConsumo_SaldoContratoCod ;
      private int AV36TFHistoricoConsumo_SaldoContratoCod_To ;
      private int AV39TFHistoricoConsumo_NotaEmpenhoCod ;
      private int AV40TFHistoricoConsumo_NotaEmpenhoCod_To ;
      private int AV43TFHistoricoConsumo_ContagemResultadoCod ;
      private int AV44TFHistoricoConsumo_ContagemResultadoCod_To ;
      private int AV47TFHistoricoConsumo_ContratoCod ;
      private int AV48TFHistoricoConsumo_ContratoCod_To ;
      private int AV51TFHistoricoConsumo_UsuarioCod ;
      private int AV52TFHistoricoConsumo_UsuarioCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfhistoricoconsumo_codigo_Visible ;
      private int edtavTfhistoricoconsumo_codigo_to_Visible ;
      private int edtavTfhistoricoconsumo_saldocontratocod_Visible ;
      private int edtavTfhistoricoconsumo_saldocontratocod_to_Visible ;
      private int edtavTfhistoricoconsumo_notaempenhocod_Visible ;
      private int edtavTfhistoricoconsumo_notaempenhocod_to_Visible ;
      private int edtavTfhistoricoconsumo_contagemresultadocod_Visible ;
      private int edtavTfhistoricoconsumo_contagemresultadocod_to_Visible ;
      private int edtavTfhistoricoconsumo_contratocod_Visible ;
      private int edtavTfhistoricoconsumo_contratocod_to_Visible ;
      private int edtavTfhistoricoconsumo_usuariocod_Visible ;
      private int edtavTfhistoricoconsumo_usuariocod_to_Visible ;
      private int edtavTfhistoricoconsumo_data_Visible ;
      private int edtavTfhistoricoconsumo_data_to_Visible ;
      private int edtavTfhistoricoconsumo_valor_Visible ;
      private int edtavTfhistoricoconsumo_valor_to_Visible ;
      private int edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Visible ;
      private int A1562HistoricoConsumo_Codigo ;
      private int A1580HistoricoConsumo_SaldoContratoCod ;
      private int A1581HistoricoConsumo_NotaEmpenhoCod ;
      private int A1582HistoricoConsumo_ContagemResultadoCod ;
      private int A1579HistoricoConsumo_ContratoCod ;
      private int A1563HistoricoConsumo_UsuarioCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV65PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV66GridCurrentPage ;
      private long AV67GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV61TFHistoricoConsumo_Valor ;
      private decimal AV62TFHistoricoConsumo_Valor_To ;
      private decimal A1578HistoricoConsumo_Valor ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_historicoconsumo_codigo_Activeeventkey ;
      private String Ddo_historicoconsumo_codigo_Filteredtext_get ;
      private String Ddo_historicoconsumo_codigo_Filteredtextto_get ;
      private String Ddo_historicoconsumo_saldocontratocod_Activeeventkey ;
      private String Ddo_historicoconsumo_saldocontratocod_Filteredtext_get ;
      private String Ddo_historicoconsumo_saldocontratocod_Filteredtextto_get ;
      private String Ddo_historicoconsumo_notaempenhocod_Activeeventkey ;
      private String Ddo_historicoconsumo_notaempenhocod_Filteredtext_get ;
      private String Ddo_historicoconsumo_notaempenhocod_Filteredtextto_get ;
      private String Ddo_historicoconsumo_contagemresultadocod_Activeeventkey ;
      private String Ddo_historicoconsumo_contagemresultadocod_Filteredtext_get ;
      private String Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_get ;
      private String Ddo_historicoconsumo_contratocod_Activeeventkey ;
      private String Ddo_historicoconsumo_contratocod_Filteredtext_get ;
      private String Ddo_historicoconsumo_contratocod_Filteredtextto_get ;
      private String Ddo_historicoconsumo_usuariocod_Activeeventkey ;
      private String Ddo_historicoconsumo_usuariocod_Filteredtext_get ;
      private String Ddo_historicoconsumo_usuariocod_Filteredtextto_get ;
      private String Ddo_historicoconsumo_data_Activeeventkey ;
      private String Ddo_historicoconsumo_data_Filteredtext_get ;
      private String Ddo_historicoconsumo_data_Filteredtextto_get ;
      private String Ddo_historicoconsumo_valor_Activeeventkey ;
      private String Ddo_historicoconsumo_valor_Filteredtext_get ;
      private String Ddo_historicoconsumo_valor_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_27_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_historicoconsumo_codigo_Caption ;
      private String Ddo_historicoconsumo_codigo_Tooltip ;
      private String Ddo_historicoconsumo_codigo_Cls ;
      private String Ddo_historicoconsumo_codigo_Filteredtext_set ;
      private String Ddo_historicoconsumo_codigo_Filteredtextto_set ;
      private String Ddo_historicoconsumo_codigo_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_codigo_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_codigo_Sortedstatus ;
      private String Ddo_historicoconsumo_codigo_Filtertype ;
      private String Ddo_historicoconsumo_codigo_Sortasc ;
      private String Ddo_historicoconsumo_codigo_Sortdsc ;
      private String Ddo_historicoconsumo_codigo_Cleanfilter ;
      private String Ddo_historicoconsumo_codigo_Rangefilterfrom ;
      private String Ddo_historicoconsumo_codigo_Rangefilterto ;
      private String Ddo_historicoconsumo_codigo_Searchbuttontext ;
      private String Ddo_historicoconsumo_saldocontratocod_Caption ;
      private String Ddo_historicoconsumo_saldocontratocod_Tooltip ;
      private String Ddo_historicoconsumo_saldocontratocod_Cls ;
      private String Ddo_historicoconsumo_saldocontratocod_Filteredtext_set ;
      private String Ddo_historicoconsumo_saldocontratocod_Filteredtextto_set ;
      private String Ddo_historicoconsumo_saldocontratocod_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_saldocontratocod_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_saldocontratocod_Sortedstatus ;
      private String Ddo_historicoconsumo_saldocontratocod_Filtertype ;
      private String Ddo_historicoconsumo_saldocontratocod_Sortasc ;
      private String Ddo_historicoconsumo_saldocontratocod_Sortdsc ;
      private String Ddo_historicoconsumo_saldocontratocod_Cleanfilter ;
      private String Ddo_historicoconsumo_saldocontratocod_Rangefilterfrom ;
      private String Ddo_historicoconsumo_saldocontratocod_Rangefilterto ;
      private String Ddo_historicoconsumo_saldocontratocod_Searchbuttontext ;
      private String Ddo_historicoconsumo_notaempenhocod_Caption ;
      private String Ddo_historicoconsumo_notaempenhocod_Tooltip ;
      private String Ddo_historicoconsumo_notaempenhocod_Cls ;
      private String Ddo_historicoconsumo_notaempenhocod_Filteredtext_set ;
      private String Ddo_historicoconsumo_notaempenhocod_Filteredtextto_set ;
      private String Ddo_historicoconsumo_notaempenhocod_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_notaempenhocod_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_notaempenhocod_Sortedstatus ;
      private String Ddo_historicoconsumo_notaempenhocod_Filtertype ;
      private String Ddo_historicoconsumo_notaempenhocod_Sortasc ;
      private String Ddo_historicoconsumo_notaempenhocod_Sortdsc ;
      private String Ddo_historicoconsumo_notaempenhocod_Cleanfilter ;
      private String Ddo_historicoconsumo_notaempenhocod_Rangefilterfrom ;
      private String Ddo_historicoconsumo_notaempenhocod_Rangefilterto ;
      private String Ddo_historicoconsumo_notaempenhocod_Searchbuttontext ;
      private String Ddo_historicoconsumo_contagemresultadocod_Caption ;
      private String Ddo_historicoconsumo_contagemresultadocod_Tooltip ;
      private String Ddo_historicoconsumo_contagemresultadocod_Cls ;
      private String Ddo_historicoconsumo_contagemresultadocod_Filteredtext_set ;
      private String Ddo_historicoconsumo_contagemresultadocod_Filteredtextto_set ;
      private String Ddo_historicoconsumo_contagemresultadocod_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_contagemresultadocod_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_contagemresultadocod_Sortedstatus ;
      private String Ddo_historicoconsumo_contagemresultadocod_Filtertype ;
      private String Ddo_historicoconsumo_contagemresultadocod_Sortasc ;
      private String Ddo_historicoconsumo_contagemresultadocod_Sortdsc ;
      private String Ddo_historicoconsumo_contagemresultadocod_Cleanfilter ;
      private String Ddo_historicoconsumo_contagemresultadocod_Rangefilterfrom ;
      private String Ddo_historicoconsumo_contagemresultadocod_Rangefilterto ;
      private String Ddo_historicoconsumo_contagemresultadocod_Searchbuttontext ;
      private String Ddo_historicoconsumo_contratocod_Caption ;
      private String Ddo_historicoconsumo_contratocod_Tooltip ;
      private String Ddo_historicoconsumo_contratocod_Cls ;
      private String Ddo_historicoconsumo_contratocod_Filteredtext_set ;
      private String Ddo_historicoconsumo_contratocod_Filteredtextto_set ;
      private String Ddo_historicoconsumo_contratocod_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_contratocod_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_contratocod_Sortedstatus ;
      private String Ddo_historicoconsumo_contratocod_Filtertype ;
      private String Ddo_historicoconsumo_contratocod_Sortasc ;
      private String Ddo_historicoconsumo_contratocod_Sortdsc ;
      private String Ddo_historicoconsumo_contratocod_Cleanfilter ;
      private String Ddo_historicoconsumo_contratocod_Rangefilterfrom ;
      private String Ddo_historicoconsumo_contratocod_Rangefilterto ;
      private String Ddo_historicoconsumo_contratocod_Searchbuttontext ;
      private String Ddo_historicoconsumo_usuariocod_Caption ;
      private String Ddo_historicoconsumo_usuariocod_Tooltip ;
      private String Ddo_historicoconsumo_usuariocod_Cls ;
      private String Ddo_historicoconsumo_usuariocod_Filteredtext_set ;
      private String Ddo_historicoconsumo_usuariocod_Filteredtextto_set ;
      private String Ddo_historicoconsumo_usuariocod_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_usuariocod_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_usuariocod_Sortedstatus ;
      private String Ddo_historicoconsumo_usuariocod_Filtertype ;
      private String Ddo_historicoconsumo_usuariocod_Sortasc ;
      private String Ddo_historicoconsumo_usuariocod_Sortdsc ;
      private String Ddo_historicoconsumo_usuariocod_Cleanfilter ;
      private String Ddo_historicoconsumo_usuariocod_Rangefilterfrom ;
      private String Ddo_historicoconsumo_usuariocod_Rangefilterto ;
      private String Ddo_historicoconsumo_usuariocod_Searchbuttontext ;
      private String Ddo_historicoconsumo_data_Caption ;
      private String Ddo_historicoconsumo_data_Tooltip ;
      private String Ddo_historicoconsumo_data_Cls ;
      private String Ddo_historicoconsumo_data_Filteredtext_set ;
      private String Ddo_historicoconsumo_data_Filteredtextto_set ;
      private String Ddo_historicoconsumo_data_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_data_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_data_Sortedstatus ;
      private String Ddo_historicoconsumo_data_Filtertype ;
      private String Ddo_historicoconsumo_data_Sortasc ;
      private String Ddo_historicoconsumo_data_Sortdsc ;
      private String Ddo_historicoconsumo_data_Cleanfilter ;
      private String Ddo_historicoconsumo_data_Rangefilterfrom ;
      private String Ddo_historicoconsumo_data_Rangefilterto ;
      private String Ddo_historicoconsumo_data_Searchbuttontext ;
      private String Ddo_historicoconsumo_valor_Caption ;
      private String Ddo_historicoconsumo_valor_Tooltip ;
      private String Ddo_historicoconsumo_valor_Cls ;
      private String Ddo_historicoconsumo_valor_Filteredtext_set ;
      private String Ddo_historicoconsumo_valor_Filteredtextto_set ;
      private String Ddo_historicoconsumo_valor_Dropdownoptionstype ;
      private String Ddo_historicoconsumo_valor_Titlecontrolidtoreplace ;
      private String Ddo_historicoconsumo_valor_Sortedstatus ;
      private String Ddo_historicoconsumo_valor_Filtertype ;
      private String Ddo_historicoconsumo_valor_Sortasc ;
      private String Ddo_historicoconsumo_valor_Sortdsc ;
      private String Ddo_historicoconsumo_valor_Cleanfilter ;
      private String Ddo_historicoconsumo_valor_Rangefilterfrom ;
      private String Ddo_historicoconsumo_valor_Rangefilterto ;
      private String Ddo_historicoconsumo_valor_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfhistoricoconsumo_codigo_Internalname ;
      private String edtavTfhistoricoconsumo_codigo_Jsonclick ;
      private String edtavTfhistoricoconsumo_codigo_to_Internalname ;
      private String edtavTfhistoricoconsumo_codigo_to_Jsonclick ;
      private String edtavTfhistoricoconsumo_saldocontratocod_Internalname ;
      private String edtavTfhistoricoconsumo_saldocontratocod_Jsonclick ;
      private String edtavTfhistoricoconsumo_saldocontratocod_to_Internalname ;
      private String edtavTfhistoricoconsumo_saldocontratocod_to_Jsonclick ;
      private String edtavTfhistoricoconsumo_notaempenhocod_Internalname ;
      private String edtavTfhistoricoconsumo_notaempenhocod_Jsonclick ;
      private String edtavTfhistoricoconsumo_notaempenhocod_to_Internalname ;
      private String edtavTfhistoricoconsumo_notaempenhocod_to_Jsonclick ;
      private String edtavTfhistoricoconsumo_contagemresultadocod_Internalname ;
      private String edtavTfhistoricoconsumo_contagemresultadocod_Jsonclick ;
      private String edtavTfhistoricoconsumo_contagemresultadocod_to_Internalname ;
      private String edtavTfhistoricoconsumo_contagemresultadocod_to_Jsonclick ;
      private String edtavTfhistoricoconsumo_contratocod_Internalname ;
      private String edtavTfhistoricoconsumo_contratocod_Jsonclick ;
      private String edtavTfhistoricoconsumo_contratocod_to_Internalname ;
      private String edtavTfhistoricoconsumo_contratocod_to_Jsonclick ;
      private String edtavTfhistoricoconsumo_usuariocod_Internalname ;
      private String edtavTfhistoricoconsumo_usuariocod_Jsonclick ;
      private String edtavTfhistoricoconsumo_usuariocod_to_Internalname ;
      private String edtavTfhistoricoconsumo_usuariocod_to_Jsonclick ;
      private String edtavTfhistoricoconsumo_data_Internalname ;
      private String edtavTfhistoricoconsumo_data_Jsonclick ;
      private String edtavTfhistoricoconsumo_data_to_Internalname ;
      private String edtavTfhistoricoconsumo_data_to_Jsonclick ;
      private String divDdo_historicoconsumo_dataauxdates_Internalname ;
      private String edtavDdo_historicoconsumo_dataauxdate_Internalname ;
      private String edtavDdo_historicoconsumo_dataauxdate_Jsonclick ;
      private String edtavDdo_historicoconsumo_dataauxdateto_Internalname ;
      private String edtavDdo_historicoconsumo_dataauxdateto_Jsonclick ;
      private String edtavTfhistoricoconsumo_valor_Internalname ;
      private String edtavTfhistoricoconsumo_valor_Jsonclick ;
      private String edtavTfhistoricoconsumo_valor_to_Internalname ;
      private String edtavTfhistoricoconsumo_valor_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_historicoconsumo_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_historicoconsumo_saldocontratocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_historicoconsumo_notaempenhocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_historicoconsumo_contagemresultadocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_historicoconsumo_contratocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_historicoconsumo_usuariocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_historicoconsumo_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_historicoconsumo_valortitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtHistoricoConsumo_Codigo_Internalname ;
      private String edtHistoricoConsumo_SaldoContratoCod_Internalname ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Internalname ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Internalname ;
      private String edtHistoricoConsumo_ContratoCod_Internalname ;
      private String edtHistoricoConsumo_UsuarioCod_Internalname ;
      private String edtHistoricoConsumo_Data_Internalname ;
      private String edtHistoricoConsumo_Valor_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_historicoconsumo_codigo_Internalname ;
      private String Ddo_historicoconsumo_saldocontratocod_Internalname ;
      private String Ddo_historicoconsumo_notaempenhocod_Internalname ;
      private String Ddo_historicoconsumo_contagemresultadocod_Internalname ;
      private String Ddo_historicoconsumo_contratocod_Internalname ;
      private String Ddo_historicoconsumo_usuariocod_Internalname ;
      private String Ddo_historicoconsumo_data_Internalname ;
      private String Ddo_historicoconsumo_valor_Internalname ;
      private String edtHistoricoConsumo_Codigo_Title ;
      private String edtHistoricoConsumo_SaldoContratoCod_Title ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Title ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Title ;
      private String edtHistoricoConsumo_ContratoCod_Title ;
      private String edtHistoricoConsumo_UsuarioCod_Title ;
      private String edtHistoricoConsumo_Data_Title ;
      private String edtHistoricoConsumo_Valor_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String sGXsfl_27_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtHistoricoConsumo_Codigo_Jsonclick ;
      private String edtHistoricoConsumo_SaldoContratoCod_Jsonclick ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Jsonclick ;
      private String edtHistoricoConsumo_ContratoCod_Jsonclick ;
      private String edtHistoricoConsumo_UsuarioCod_Jsonclick ;
      private String edtHistoricoConsumo_Data_Jsonclick ;
      private String edtHistoricoConsumo_Valor_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV55TFHistoricoConsumo_Data ;
      private DateTime AV56TFHistoricoConsumo_Data_To ;
      private DateTime A1577HistoricoConsumo_Data ;
      private DateTime AV57DDO_HistoricoConsumo_DataAuxDate ;
      private DateTime AV58DDO_HistoricoConsumo_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_historicoconsumo_codigo_Includesortasc ;
      private bool Ddo_historicoconsumo_codigo_Includesortdsc ;
      private bool Ddo_historicoconsumo_codigo_Includefilter ;
      private bool Ddo_historicoconsumo_codigo_Filterisrange ;
      private bool Ddo_historicoconsumo_codigo_Includedatalist ;
      private bool Ddo_historicoconsumo_saldocontratocod_Includesortasc ;
      private bool Ddo_historicoconsumo_saldocontratocod_Includesortdsc ;
      private bool Ddo_historicoconsumo_saldocontratocod_Includefilter ;
      private bool Ddo_historicoconsumo_saldocontratocod_Filterisrange ;
      private bool Ddo_historicoconsumo_saldocontratocod_Includedatalist ;
      private bool Ddo_historicoconsumo_notaempenhocod_Includesortasc ;
      private bool Ddo_historicoconsumo_notaempenhocod_Includesortdsc ;
      private bool Ddo_historicoconsumo_notaempenhocod_Includefilter ;
      private bool Ddo_historicoconsumo_notaempenhocod_Filterisrange ;
      private bool Ddo_historicoconsumo_notaempenhocod_Includedatalist ;
      private bool Ddo_historicoconsumo_contagemresultadocod_Includesortasc ;
      private bool Ddo_historicoconsumo_contagemresultadocod_Includesortdsc ;
      private bool Ddo_historicoconsumo_contagemresultadocod_Includefilter ;
      private bool Ddo_historicoconsumo_contagemresultadocod_Filterisrange ;
      private bool Ddo_historicoconsumo_contagemresultadocod_Includedatalist ;
      private bool Ddo_historicoconsumo_contratocod_Includesortasc ;
      private bool Ddo_historicoconsumo_contratocod_Includesortdsc ;
      private bool Ddo_historicoconsumo_contratocod_Includefilter ;
      private bool Ddo_historicoconsumo_contratocod_Filterisrange ;
      private bool Ddo_historicoconsumo_contratocod_Includedatalist ;
      private bool Ddo_historicoconsumo_usuariocod_Includesortasc ;
      private bool Ddo_historicoconsumo_usuariocod_Includesortdsc ;
      private bool Ddo_historicoconsumo_usuariocod_Includefilter ;
      private bool Ddo_historicoconsumo_usuariocod_Filterisrange ;
      private bool Ddo_historicoconsumo_usuariocod_Includedatalist ;
      private bool Ddo_historicoconsumo_data_Includesortasc ;
      private bool Ddo_historicoconsumo_data_Includesortdsc ;
      private bool Ddo_historicoconsumo_data_Includefilter ;
      private bool Ddo_historicoconsumo_data_Filterisrange ;
      private bool Ddo_historicoconsumo_data_Includedatalist ;
      private bool Ddo_historicoconsumo_valor_Includesortasc ;
      private bool Ddo_historicoconsumo_valor_Includesortdsc ;
      private bool Ddo_historicoconsumo_valor_Includefilter ;
      private bool Ddo_historicoconsumo_valor_Filterisrange ;
      private bool Ddo_historicoconsumo_valor_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool n1582HistoricoConsumo_ContagemResultadoCod ;
      private bool n1579HistoricoConsumo_ContratoCod ;
      private bool n1563HistoricoConsumo_UsuarioCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV33ddo_HistoricoConsumo_CodigoTitleControlIdToReplace ;
      private String AV37ddo_HistoricoConsumo_SaldoContratoCodTitleControlIdToReplace ;
      private String AV41ddo_HistoricoConsumo_NotaEmpenhoCodTitleControlIdToReplace ;
      private String AV45ddo_HistoricoConsumo_ContagemResultadoCodTitleControlIdToReplace ;
      private String AV49ddo_HistoricoConsumo_ContratoCodTitleControlIdToReplace ;
      private String AV53ddo_HistoricoConsumo_UsuarioCodTitleControlIdToReplace ;
      private String AV59ddo_HistoricoConsumo_DataTitleControlIdToReplace ;
      private String AV63ddo_HistoricoConsumo_ValorTitleControlIdToReplace ;
      private String AV70Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutHistoricoConsumo_Codigo ;
      private GXCombobox cmbavOrderedby ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00MG2_A1578HistoricoConsumo_Valor ;
      private DateTime[] H00MG2_A1577HistoricoConsumo_Data ;
      private int[] H00MG2_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] H00MG2_n1563HistoricoConsumo_UsuarioCod ;
      private int[] H00MG2_A1579HistoricoConsumo_ContratoCod ;
      private bool[] H00MG2_n1579HistoricoConsumo_ContratoCod ;
      private int[] H00MG2_A1582HistoricoConsumo_ContagemResultadoCod ;
      private bool[] H00MG2_n1582HistoricoConsumo_ContagemResultadoCod ;
      private int[] H00MG2_A1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool[] H00MG2_n1581HistoricoConsumo_NotaEmpenhoCod ;
      private int[] H00MG2_A1580HistoricoConsumo_SaldoContratoCod ;
      private int[] H00MG2_A1562HistoricoConsumo_Codigo ;
      private long[] H00MG3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30HistoricoConsumo_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34HistoricoConsumo_SaldoContratoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38HistoricoConsumo_NotaEmpenhoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42HistoricoConsumo_ContagemResultadoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46HistoricoConsumo_ContratoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50HistoricoConsumo_UsuarioCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54HistoricoConsumo_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60HistoricoConsumo_ValorTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV64DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class prompthistoricoconsumo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MG2( IGxContext context ,
                                             int AV31TFHistoricoConsumo_Codigo ,
                                             int AV32TFHistoricoConsumo_Codigo_To ,
                                             int AV35TFHistoricoConsumo_SaldoContratoCod ,
                                             int AV36TFHistoricoConsumo_SaldoContratoCod_To ,
                                             int AV39TFHistoricoConsumo_NotaEmpenhoCod ,
                                             int AV40TFHistoricoConsumo_NotaEmpenhoCod_To ,
                                             int AV43TFHistoricoConsumo_ContagemResultadoCod ,
                                             int AV44TFHistoricoConsumo_ContagemResultadoCod_To ,
                                             int AV47TFHistoricoConsumo_ContratoCod ,
                                             int AV48TFHistoricoConsumo_ContratoCod_To ,
                                             int AV51TFHistoricoConsumo_UsuarioCod ,
                                             int AV52TFHistoricoConsumo_UsuarioCod_To ,
                                             DateTime AV55TFHistoricoConsumo_Data ,
                                             DateTime AV56TFHistoricoConsumo_Data_To ,
                                             decimal AV61TFHistoricoConsumo_Valor ,
                                             decimal AV62TFHistoricoConsumo_Valor_To ,
                                             int A1562HistoricoConsumo_Codigo ,
                                             int A1580HistoricoConsumo_SaldoContratoCod ,
                                             int A1581HistoricoConsumo_NotaEmpenhoCod ,
                                             int A1582HistoricoConsumo_ContagemResultadoCod ,
                                             int A1579HistoricoConsumo_ContratoCod ,
                                             int A1563HistoricoConsumo_UsuarioCod ,
                                             DateTime A1577HistoricoConsumo_Data ,
                                             decimal A1578HistoricoConsumo_Valor ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [HistoricoConsumo_Valor], [HistoricoConsumo_Data], [HistoricoConsumo_UsuarioCod], [HistoricoConsumo_ContratoCod], [HistoricoConsumo_ContagemResultadoCod], [HistoricoConsumo_NotaEmpenhoCod], [HistoricoConsumo_SaldoContratoCod], [HistoricoConsumo_Codigo]";
         sFromString = " FROM [HistoricoConsumo] WITH (NOLOCK)";
         sOrderString = "";
         if ( ! (0==AV31TFHistoricoConsumo_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Codigo] >= @AV31TFHistoricoConsumo_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Codigo] >= @AV31TFHistoricoConsumo_Codigo)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (0==AV32TFHistoricoConsumo_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Codigo] <= @AV32TFHistoricoConsumo_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Codigo] <= @AV32TFHistoricoConsumo_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV35TFHistoricoConsumo_SaldoContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_SaldoContratoCod] >= @AV35TFHistoricoConsumo_SaldoContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_SaldoContratoCod] >= @AV35TFHistoricoConsumo_SaldoContratoCod)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV36TFHistoricoConsumo_SaldoContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_SaldoContratoCod] <= @AV36TFHistoricoConsumo_SaldoContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_SaldoContratoCod] <= @AV36TFHistoricoConsumo_SaldoContratoCod_To)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV39TFHistoricoConsumo_NotaEmpenhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_NotaEmpenhoCod] >= @AV39TFHistoricoConsumo_NotaEmpenhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_NotaEmpenhoCod] >= @AV39TFHistoricoConsumo_NotaEmpenhoCod)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV40TFHistoricoConsumo_NotaEmpenhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_NotaEmpenhoCod] <= @AV40TFHistoricoConsumo_NotaEmpenhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_NotaEmpenhoCod] <= @AV40TFHistoricoConsumo_NotaEmpenhoCod_To)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV43TFHistoricoConsumo_ContagemResultadoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContagemResultadoCod] >= @AV43TFHistoricoConsumo_ContagemResultadoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContagemResultadoCod] >= @AV43TFHistoricoConsumo_ContagemResultadoCod)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV44TFHistoricoConsumo_ContagemResultadoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContagemResultadoCod] <= @AV44TFHistoricoConsumo_ContagemResultadoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContagemResultadoCod] <= @AV44TFHistoricoConsumo_ContagemResultadoCod_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV47TFHistoricoConsumo_ContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContratoCod] >= @AV47TFHistoricoConsumo_ContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContratoCod] >= @AV47TFHistoricoConsumo_ContratoCod)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV48TFHistoricoConsumo_ContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContratoCod] <= @AV48TFHistoricoConsumo_ContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContratoCod] <= @AV48TFHistoricoConsumo_ContratoCod_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV51TFHistoricoConsumo_UsuarioCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_UsuarioCod] >= @AV51TFHistoricoConsumo_UsuarioCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_UsuarioCod] >= @AV51TFHistoricoConsumo_UsuarioCod)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV52TFHistoricoConsumo_UsuarioCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_UsuarioCod] <= @AV52TFHistoricoConsumo_UsuarioCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_UsuarioCod] <= @AV52TFHistoricoConsumo_UsuarioCod_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV55TFHistoricoConsumo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Data] >= @AV55TFHistoricoConsumo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Data] >= @AV55TFHistoricoConsumo_Data)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV56TFHistoricoConsumo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Data] <= @AV56TFHistoricoConsumo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Data] <= @AV56TFHistoricoConsumo_Data_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFHistoricoConsumo_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Valor] >= @AV61TFHistoricoConsumo_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Valor] >= @AV61TFHistoricoConsumo_Valor)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFHistoricoConsumo_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Valor] <= @AV62TFHistoricoConsumo_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Valor] <= @AV62TFHistoricoConsumo_Valor_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_SaldoContratoCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_SaldoContratoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_NotaEmpenhoCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_NotaEmpenhoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_ContagemResultadoCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_ContagemResultadoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_ContratoCod]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_ContratoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_UsuarioCod]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_UsuarioCod] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_Data]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_Valor]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_Valor] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [HistoricoConsumo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00MG3( IGxContext context ,
                                             int AV31TFHistoricoConsumo_Codigo ,
                                             int AV32TFHistoricoConsumo_Codigo_To ,
                                             int AV35TFHistoricoConsumo_SaldoContratoCod ,
                                             int AV36TFHistoricoConsumo_SaldoContratoCod_To ,
                                             int AV39TFHistoricoConsumo_NotaEmpenhoCod ,
                                             int AV40TFHistoricoConsumo_NotaEmpenhoCod_To ,
                                             int AV43TFHistoricoConsumo_ContagemResultadoCod ,
                                             int AV44TFHistoricoConsumo_ContagemResultadoCod_To ,
                                             int AV47TFHistoricoConsumo_ContratoCod ,
                                             int AV48TFHistoricoConsumo_ContratoCod_To ,
                                             int AV51TFHistoricoConsumo_UsuarioCod ,
                                             int AV52TFHistoricoConsumo_UsuarioCod_To ,
                                             DateTime AV55TFHistoricoConsumo_Data ,
                                             DateTime AV56TFHistoricoConsumo_Data_To ,
                                             decimal AV61TFHistoricoConsumo_Valor ,
                                             decimal AV62TFHistoricoConsumo_Valor_To ,
                                             int A1562HistoricoConsumo_Codigo ,
                                             int A1580HistoricoConsumo_SaldoContratoCod ,
                                             int A1581HistoricoConsumo_NotaEmpenhoCod ,
                                             int A1582HistoricoConsumo_ContagemResultadoCod ,
                                             int A1579HistoricoConsumo_ContratoCod ,
                                             int A1563HistoricoConsumo_UsuarioCod ,
                                             DateTime A1577HistoricoConsumo_Data ,
                                             decimal A1578HistoricoConsumo_Valor ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [HistoricoConsumo] WITH (NOLOCK)";
         if ( ! (0==AV31TFHistoricoConsumo_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Codigo] >= @AV31TFHistoricoConsumo_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Codigo] >= @AV31TFHistoricoConsumo_Codigo)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! (0==AV32TFHistoricoConsumo_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Codigo] <= @AV32TFHistoricoConsumo_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Codigo] <= @AV32TFHistoricoConsumo_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV35TFHistoricoConsumo_SaldoContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_SaldoContratoCod] >= @AV35TFHistoricoConsumo_SaldoContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_SaldoContratoCod] >= @AV35TFHistoricoConsumo_SaldoContratoCod)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV36TFHistoricoConsumo_SaldoContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_SaldoContratoCod] <= @AV36TFHistoricoConsumo_SaldoContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_SaldoContratoCod] <= @AV36TFHistoricoConsumo_SaldoContratoCod_To)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV39TFHistoricoConsumo_NotaEmpenhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_NotaEmpenhoCod] >= @AV39TFHistoricoConsumo_NotaEmpenhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_NotaEmpenhoCod] >= @AV39TFHistoricoConsumo_NotaEmpenhoCod)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (0==AV40TFHistoricoConsumo_NotaEmpenhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_NotaEmpenhoCod] <= @AV40TFHistoricoConsumo_NotaEmpenhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_NotaEmpenhoCod] <= @AV40TFHistoricoConsumo_NotaEmpenhoCod_To)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV43TFHistoricoConsumo_ContagemResultadoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContagemResultadoCod] >= @AV43TFHistoricoConsumo_ContagemResultadoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContagemResultadoCod] >= @AV43TFHistoricoConsumo_ContagemResultadoCod)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV44TFHistoricoConsumo_ContagemResultadoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContagemResultadoCod] <= @AV44TFHistoricoConsumo_ContagemResultadoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContagemResultadoCod] <= @AV44TFHistoricoConsumo_ContagemResultadoCod_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV47TFHistoricoConsumo_ContratoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContratoCod] >= @AV47TFHistoricoConsumo_ContratoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContratoCod] >= @AV47TFHistoricoConsumo_ContratoCod)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV48TFHistoricoConsumo_ContratoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_ContratoCod] <= @AV48TFHistoricoConsumo_ContratoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_ContratoCod] <= @AV48TFHistoricoConsumo_ContratoCod_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV51TFHistoricoConsumo_UsuarioCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_UsuarioCod] >= @AV51TFHistoricoConsumo_UsuarioCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_UsuarioCod] >= @AV51TFHistoricoConsumo_UsuarioCod)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV52TFHistoricoConsumo_UsuarioCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_UsuarioCod] <= @AV52TFHistoricoConsumo_UsuarioCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_UsuarioCod] <= @AV52TFHistoricoConsumo_UsuarioCod_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV55TFHistoricoConsumo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Data] >= @AV55TFHistoricoConsumo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Data] >= @AV55TFHistoricoConsumo_Data)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV56TFHistoricoConsumo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Data] <= @AV56TFHistoricoConsumo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Data] <= @AV56TFHistoricoConsumo_Data_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFHistoricoConsumo_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Valor] >= @AV61TFHistoricoConsumo_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Valor] >= @AV61TFHistoricoConsumo_Valor)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFHistoricoConsumo_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([HistoricoConsumo_Valor] <= @AV62TFHistoricoConsumo_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([HistoricoConsumo_Valor] <= @AV62TFHistoricoConsumo_Valor_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00MG2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (DateTime)dynConstraints[22] , (decimal)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
               case 1 :
                     return conditional_H00MG3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (DateTime)dynConstraints[22] , (decimal)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MG2 ;
          prmH00MG2 = new Object[] {
          new Object[] {"@AV31TFHistoricoConsumo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFHistoricoConsumo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFHistoricoConsumo_SaldoContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFHistoricoConsumo_SaldoContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFHistoricoConsumo_NotaEmpenhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFHistoricoConsumo_NotaEmpenhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43TFHistoricoConsumo_ContagemResultadoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFHistoricoConsumo_ContagemResultadoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFHistoricoConsumo_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFHistoricoConsumo_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51TFHistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFHistoricoConsumo_UsuarioCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFHistoricoConsumo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV56TFHistoricoConsumo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV61TFHistoricoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV62TFHistoricoConsumo_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00MG3 ;
          prmH00MG3 = new Object[] {
          new Object[] {"@AV31TFHistoricoConsumo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFHistoricoConsumo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFHistoricoConsumo_SaldoContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFHistoricoConsumo_SaldoContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFHistoricoConsumo_NotaEmpenhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFHistoricoConsumo_NotaEmpenhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43TFHistoricoConsumo_ContagemResultadoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFHistoricoConsumo_ContagemResultadoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFHistoricoConsumo_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFHistoricoConsumo_ContratoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51TFHistoricoConsumo_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFHistoricoConsumo_UsuarioCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFHistoricoConsumo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV56TFHistoricoConsumo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV61TFHistoricoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV62TFHistoricoConsumo_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MG2,11,0,true,false )
             ,new CursorDef("H00MG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MG3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                return;
       }
    }

 }

}
