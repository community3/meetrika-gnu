/*
               File: GetSistemaModuloWCFilterData
        Description: Get Sistema Modulo WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:39.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getsistemamodulowcfilterdata : GXProcedure
   {
      public getsistemamodulowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getsistemamodulowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getsistemamodulowcfilterdata objgetsistemamodulowcfilterdata;
         objgetsistemamodulowcfilterdata = new getsistemamodulowcfilterdata();
         objgetsistemamodulowcfilterdata.AV16DDOName = aP0_DDOName;
         objgetsistemamodulowcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetsistemamodulowcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetsistemamodulowcfilterdata.AV20OptionsJson = "" ;
         objgetsistemamodulowcfilterdata.AV23OptionsDescJson = "" ;
         objgetsistemamodulowcfilterdata.AV25OptionIndexesJson = "" ;
         objgetsistemamodulowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetsistemamodulowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetsistemamodulowcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getsistemamodulowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_MODULO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMODULO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_MODULO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADMODULO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("SistemaModuloWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "SistemaModuloWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("SistemaModuloWCGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME") == 0 )
            {
               AV10TFModulo_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME_SEL") == 0 )
            {
               AV11TFModulo_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA") == 0 )
            {
               AV12TFModulo_Sigla = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA_SEL") == 0 )
            {
               AV13TFModulo_Sigla_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&SISTEMA_CODIGO") == 0 )
            {
               AV34Sistema_Codigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "MODULO_NOME") == 0 )
            {
               AV33Modulo_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADMODULO_NOMEOPTIONS' Routine */
         AV10TFModulo_Nome = AV14SearchTxt;
         AV11TFModulo_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33Modulo_Nome1 ,
                                              AV11TFModulo_Nome_Sel ,
                                              AV10TFModulo_Nome ,
                                              AV13TFModulo_Sigla_Sel ,
                                              AV12TFModulo_Sigla ,
                                              A143Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              AV34Sistema_Codigo ,
                                              A127Sistema_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV33Modulo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV33Modulo_Nome1), 50, "%");
         lV10TFModulo_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFModulo_Nome), 50, "%");
         lV12TFModulo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFModulo_Sigla), 15, "%");
         /* Using cursor P00FT2 */
         pr_default.execute(0, new Object[] {AV34Sistema_Codigo, lV33Modulo_Nome1, lV10TFModulo_Nome, AV11TFModulo_Nome_Sel, lV12TFModulo_Sigla, AV13TFModulo_Sigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFT2 = false;
            A127Sistema_Codigo = P00FT2_A127Sistema_Codigo[0];
            A143Modulo_Nome = P00FT2_A143Modulo_Nome[0];
            A145Modulo_Sigla = P00FT2_A145Modulo_Sigla[0];
            A146Modulo_Codigo = P00FT2_A146Modulo_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00FT2_A127Sistema_Codigo[0] == A127Sistema_Codigo ) && ( StringUtil.StrCmp(P00FT2_A143Modulo_Nome[0], A143Modulo_Nome) == 0 ) )
            {
               BRKFT2 = false;
               A146Modulo_Codigo = P00FT2_A146Modulo_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKFT2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A143Modulo_Nome)) )
            {
               AV18Option = A143Modulo_Nome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFT2 )
            {
               BRKFT2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADMODULO_SIGLAOPTIONS' Routine */
         AV12TFModulo_Sigla = AV14SearchTxt;
         AV13TFModulo_Sigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33Modulo_Nome1 ,
                                              AV11TFModulo_Nome_Sel ,
                                              AV10TFModulo_Nome ,
                                              AV13TFModulo_Sigla_Sel ,
                                              AV12TFModulo_Sigla ,
                                              A143Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              AV34Sistema_Codigo ,
                                              A127Sistema_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV33Modulo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV33Modulo_Nome1), 50, "%");
         lV10TFModulo_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFModulo_Nome), 50, "%");
         lV12TFModulo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFModulo_Sigla), 15, "%");
         /* Using cursor P00FT3 */
         pr_default.execute(1, new Object[] {AV34Sistema_Codigo, lV33Modulo_Nome1, lV10TFModulo_Nome, AV11TFModulo_Nome_Sel, lV12TFModulo_Sigla, AV13TFModulo_Sigla_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFT4 = false;
            A127Sistema_Codigo = P00FT3_A127Sistema_Codigo[0];
            A145Modulo_Sigla = P00FT3_A145Modulo_Sigla[0];
            A143Modulo_Nome = P00FT3_A143Modulo_Nome[0];
            A146Modulo_Codigo = P00FT3_A146Modulo_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00FT3_A127Sistema_Codigo[0] == A127Sistema_Codigo ) && ( StringUtil.StrCmp(P00FT3_A145Modulo_Sigla[0], A145Modulo_Sigla) == 0 ) )
            {
               BRKFT4 = false;
               A146Modulo_Codigo = P00FT3_A146Modulo_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKFT4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A145Modulo_Sigla)) )
            {
               AV18Option = A145Modulo_Sigla;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFT4 )
            {
               BRKFT4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFModulo_Nome = "";
         AV11TFModulo_Nome_Sel = "";
         AV12TFModulo_Sigla = "";
         AV13TFModulo_Sigla_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV33Modulo_Nome1 = "";
         scmdbuf = "";
         lV33Modulo_Nome1 = "";
         lV10TFModulo_Nome = "";
         lV12TFModulo_Sigla = "";
         A143Modulo_Nome = "";
         A145Modulo_Sigla = "";
         P00FT2_A127Sistema_Codigo = new int[1] ;
         P00FT2_A143Modulo_Nome = new String[] {""} ;
         P00FT2_A145Modulo_Sigla = new String[] {""} ;
         P00FT2_A146Modulo_Codigo = new int[1] ;
         AV18Option = "";
         P00FT3_A127Sistema_Codigo = new int[1] ;
         P00FT3_A145Modulo_Sigla = new String[] {""} ;
         P00FT3_A143Modulo_Nome = new String[] {""} ;
         P00FT3_A146Modulo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getsistemamodulowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FT2_A127Sistema_Codigo, P00FT2_A143Modulo_Nome, P00FT2_A145Modulo_Sigla, P00FT2_A146Modulo_Codigo
               }
               , new Object[] {
               P00FT3_A127Sistema_Codigo, P00FT3_A145Modulo_Sigla, P00FT3_A143Modulo_Nome, P00FT3_A146Modulo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV37GXV1 ;
      private int AV34Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int A146Modulo_Codigo ;
      private long AV26count ;
      private String AV10TFModulo_Nome ;
      private String AV11TFModulo_Nome_Sel ;
      private String AV12TFModulo_Sigla ;
      private String AV13TFModulo_Sigla_Sel ;
      private String AV33Modulo_Nome1 ;
      private String scmdbuf ;
      private String lV33Modulo_Nome1 ;
      private String lV10TFModulo_Nome ;
      private String lV12TFModulo_Sigla ;
      private String A143Modulo_Nome ;
      private String A145Modulo_Sigla ;
      private bool returnInSub ;
      private bool BRKFT2 ;
      private bool BRKFT4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FT2_A127Sistema_Codigo ;
      private String[] P00FT2_A143Modulo_Nome ;
      private String[] P00FT2_A145Modulo_Sigla ;
      private int[] P00FT2_A146Modulo_Codigo ;
      private int[] P00FT3_A127Sistema_Codigo ;
      private String[] P00FT3_A145Modulo_Sigla ;
      private String[] P00FT3_A143Modulo_Nome ;
      private int[] P00FT3_A146Modulo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getsistemamodulowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FT2( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             String AV33Modulo_Nome1 ,
                                             String AV11TFModulo_Nome_Sel ,
                                             String AV10TFModulo_Nome ,
                                             String AV13TFModulo_Sigla_Sel ,
                                             String AV12TFModulo_Sigla ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             int AV34Sistema_Codigo ,
                                             int A127Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Sistema_Codigo], [Modulo_Nome], [Modulo_Sigla], [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Sistema_Codigo] = @AV34Sistema_Codigo)";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Modulo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like '%' + @lV33Modulo_Nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFModulo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFModulo_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like @lV10TFModulo_Nome)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFModulo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] = @AV11TFModulo_Nome_Sel)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFModulo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFModulo_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] like @lV12TFModulo_Sigla)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFModulo_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] = @AV13TFModulo_Sigla_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Sistema_Codigo], [Modulo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00FT3( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             String AV33Modulo_Nome1 ,
                                             String AV11TFModulo_Nome_Sel ,
                                             String AV10TFModulo_Nome ,
                                             String AV13TFModulo_Sigla_Sel ,
                                             String AV12TFModulo_Sigla ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             int AV34Sistema_Codigo ,
                                             int A127Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Sistema_Codigo], [Modulo_Sigla], [Modulo_Nome], [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Sistema_Codigo] = @AV34Sistema_Codigo)";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Modulo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like '%' + @lV33Modulo_Nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFModulo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFModulo_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] like @lV10TFModulo_Nome)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFModulo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Nome] = @AV11TFModulo_Nome_Sel)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFModulo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFModulo_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] like @lV12TFModulo_Sigla)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFModulo_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Modulo_Sigla] = @AV13TFModulo_Sigla_Sel)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Sistema_Codigo], [Modulo_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FT2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
               case 1 :
                     return conditional_P00FT3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FT2 ;
          prmP00FT2 = new Object[] {
          new Object[] {"@AV34Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33Modulo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFModulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFModulo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFModulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFModulo_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00FT3 ;
          prmP00FT3 = new Object[] {
          new Object[] {"@AV34Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV33Modulo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFModulo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFModulo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFModulo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFModulo_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FT2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FT2,100,0,true,false )
             ,new CursorDef("P00FT3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FT3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getsistemamodulowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getsistemamodulowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getsistemamodulowcfilterdata") )
          {
             return  ;
          }
          getsistemamodulowcfilterdata worker = new getsistemamodulowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
