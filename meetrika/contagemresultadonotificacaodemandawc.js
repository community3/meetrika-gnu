/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:13:48.40
*/
gx.evt.autoSkip = false;
gx.define('contagemresultadonotificacaodemandawc', true, function (CmpContext) {
   this.ServerClass =  "contagemresultadonotificacaodemandawc" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7ContagemResultadoNotificacao_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADONOTIFICACAO_CODIGO",'.') ;
      this.AV21Pgmname=gx.fn.getControlValue("vPGMNAME") ;
   };
   this.Valid_Contagemresultado_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(11) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_CODIGO", gx.fn.currentGridRowImpl(11));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e11qw2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e15qw2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e16qw2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,12,13,14,15,16,20,22,23];
   this.GXLastCtrlId =23;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",11,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"contagemresultadonotificacaodemandawc",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addSingleLineEdit(456,12,"CONTAGEMRESULTADO_CODIGO","Contagem Resultado_Codigo","","ContagemResultado_Codigo","int",0,"px",6,6,"right",null,[],456,"ContagemResultado_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(493,13,"CONTAGEMRESULTADO_DEMANDAFM","","","ContagemResultado_DemandaFM","svchar",80,"px",50,50,"left",null,[],493,"ContagemResultado_DemandaFM",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(457,14,"CONTAGEMRESULTADO_DEMANDA","","","ContagemResultado_Demanda","svchar",0,"px",30,30,"left",null,[],457,"ContagemResultado_Demanda",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(801,15,"CONTAGEMRESULTADO_SERVICOSIGLA","","","ContagemResultado_ServicoSigla","char",0,"px",15,15,"left",null,[],801,"ContagemResultado_ServicoSigla",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1227,16,"CONTAGEMRESULTADO_PRAZOINICIALDIAS","","","ContagemResultado_PrazoInicialDias","int",0,"px",4,4,"right",null,[],1227,"ContagemResultado_PrazoInicialDias",true,0,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 21, 20, "DVelop_WorkWithPlusUtilities", this.CmpContext + "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 19, 12, "DVelop_DVPaginationBar", this.CmpContext + "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV17GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV17GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV17GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV18GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV18GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV18GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11qw2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[8]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[12]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:this.Valid_Contagemresultado_codigo,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_CODIGO",gxz:"Z456ContagemResultado_Codigo",gxold:"O456ContagemResultado_Codigo",gxvar:"A456ContagemResultado_Codigo",ucs:[],op:[15,13,14,16],ip:[15,13,14,16,12],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z456ContagemResultado_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_CODIGO",row || gx.fn.currentGridRowImpl(11),gx.O.A456ContagemResultado_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADO_CODIGO",row || gx.fn.currentGridRowImpl(11),'.')},nac:gx.falseFn};
   GXValidFnc[13]={lvl:2,type:"svchar",len:50,dec:0,sign:false,ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DEMANDAFM",gxz:"Z493ContagemResultado_DemandaFM",gxold:"O493ContagemResultado_DemandaFM",gxvar:"A493ContagemResultado_DemandaFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A493ContagemResultado_DemandaFM=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z493ContagemResultado_DemandaFM=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DEMANDAFM",row || gx.fn.currentGridRowImpl(11),gx.O.A493ContagemResultado_DemandaFM,0)},c2v:function(){if(this.val()!==undefined)gx.O.A493ContagemResultado_DemandaFM=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_DEMANDAFM",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[14]={lvl:2,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DEMANDA",gxz:"Z457ContagemResultado_Demanda",gxold:"O457ContagemResultado_Demanda",gxvar:"A457ContagemResultado_Demanda",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A457ContagemResultado_Demanda=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z457ContagemResultado_Demanda=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DEMANDA",row || gx.fn.currentGridRowImpl(11),gx.O.A457ContagemResultado_Demanda,0)},c2v:function(){if(this.val()!==undefined)gx.O.A457ContagemResultado_Demanda=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_DEMANDA",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[15]={lvl:2,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_SERVICOSIGLA",gxz:"Z801ContagemResultado_ServicoSigla",gxold:"O801ContagemResultado_ServicoSigla",gxvar:"A801ContagemResultado_ServicoSigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A801ContagemResultado_ServicoSigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z801ContagemResultado_ServicoSigla=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_SERVICOSIGLA",row || gx.fn.currentGridRowImpl(11),gx.O.A801ContagemResultado_ServicoSigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A801ContagemResultado_ServicoSigla=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_SERVICOSIGLA",row || gx.fn.currentGridRowImpl(11))},nac:gx.falseFn};
   GXValidFnc[16]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:11,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PRAZOINICIALDIAS",gxz:"Z1227ContagemResultado_PrazoInicialDias",gxold:"O1227ContagemResultado_PrazoInicialDias",gxvar:"A1227ContagemResultado_PrazoInicialDias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1227ContagemResultado_PrazoInicialDias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1227ContagemResultado_PrazoInicialDias=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_PRAZOINICIALDIAS",row || gx.fn.currentGridRowImpl(11),gx.O.A1227ContagemResultado_PrazoInicialDias,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1227ContagemResultado_PrazoInicialDias=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADO_PRAZOINICIALDIAS",row || gx.fn.currentGridRowImpl(11),'.')},nac:gx.falseFn};
   GXValidFnc[20]={lvl:0,type:"int",len:10,dec:0,sign:false,pic:"ZZZZZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADONOTIFICACAO_CODIGO",gxz:"Z1412ContagemResultadoNotificacao_Codigo",gxold:"O1412ContagemResultadoNotificacao_Codigo",gxvar:"A1412ContagemResultadoNotificacao_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1412ContagemResultadoNotificacao_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1412ContagemResultadoNotificacao_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADONOTIFICACAO_CODIGO",gx.O.A1412ContagemResultadoNotificacao_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1412ContagemResultadoNotificacao_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADONOTIFICACAO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 20 , function() {
   });
   GXValidFnc[22]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDBY",gx.O.AV13OrderedBy,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[23]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   this.Z456ContagemResultado_Codigo = 0 ;
   this.O456ContagemResultado_Codigo = 0 ;
   this.Z493ContagemResultado_DemandaFM = "" ;
   this.O493ContagemResultado_DemandaFM = "" ;
   this.Z457ContagemResultado_Demanda = "" ;
   this.O457ContagemResultado_Demanda = "" ;
   this.Z801ContagemResultado_ServicoSigla = "" ;
   this.O801ContagemResultado_ServicoSigla = "" ;
   this.Z1227ContagemResultado_PrazoInicialDias = 0 ;
   this.O1227ContagemResultado_PrazoInicialDias = 0 ;
   this.A1412ContagemResultadoNotificacao_Codigo = 0 ;
   this.Z1412ContagemResultadoNotificacao_Codigo = 0 ;
   this.O1412ContagemResultadoNotificacao_Codigo = 0 ;
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV17GridCurrentPage = 0 ;
   this.A1412ContagemResultadoNotificacao_Codigo = 0 ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV7ContagemResultadoNotificacao_Codigo = 0 ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.A601ContagemResultado_Servico = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A493ContagemResultado_DemandaFM = "" ;
   this.A457ContagemResultado_Demanda = "" ;
   this.A801ContagemResultado_ServicoSigla = "" ;
   this.A1227ContagemResultado_PrazoInicialDias = 0 ;
   this.AV21Pgmname = "" ;
   this.Events = {"e11qw2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e15qw2_client": ["ENTER", true] ,"e16qw2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21Pgmname',fld:'vPGMNAME',pic:'',nv:''}],[{ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DEMANDAFM","Title")',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DEMANDA","Title")',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_SERVICOSIGLA',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_SERVICOSIGLA","Title")',ctrl:'CONTAGEMRESULTADO_SERVICOSIGLA',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_PRAZOINICIALDIAS","Title")',ctrl:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',prop:'Title'},{av:'AV17GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DEMANDAFM","Linktarget")',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Linktarget'}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV21Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DEMANDAFM","Link")',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Link'}]];
   this.setVCMap("AV7ContagemResultadoNotificacao_Codigo", "vCONTAGEMRESULTADONOTIFICACAO_CODIGO", 0, "int");
   this.setVCMap("AV21Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV7ContagemResultadoNotificacao_Codigo", "vCONTAGEMRESULTADONOTIFICACAO_CODIGO", 0, "int");
   this.setVCMap("AV21Pgmname", "vPGMNAME", 0, "char");
   GridContainer.addRefreshingVar(this.GXValidFnc[22]);
   GridContainer.addRefreshingVar(this.GXValidFnc[23]);
   GridContainer.addRefreshingVar({rfrVar:"AV7ContagemResultadoNotificacao_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"AV21Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"A456ContagemResultado_Codigo", rfrProp:"Value", gxAttId:"456"});
   this.InitStandaloneVars( );
});
