/*
               File: type_SdtGAMError
        Description: GAMError
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:11.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMError : GxUserType, IGxExternalObject
   {
      public SdtGAMError( )
      {
         initialize();
      }

      public SdtGAMError( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMError_externalReference == null )
         {
            GAMError_externalReference = new Artech.Security.GAMError(context);
         }
         returntostring = "";
         returntostring = (String)(GAMError_externalReference.ToString());
         return returntostring ;
      }

      public long gxTpr_Code
      {
         get {
            if ( GAMError_externalReference == null )
            {
               GAMError_externalReference = new Artech.Security.GAMError(context);
            }
            return GAMError_externalReference.Code ;
         }

         set {
            if ( GAMError_externalReference == null )
            {
               GAMError_externalReference = new Artech.Security.GAMError(context);
            }
            GAMError_externalReference.Code = value;
         }

      }

      public String gxTpr_Message
      {
         get {
            if ( GAMError_externalReference == null )
            {
               GAMError_externalReference = new Artech.Security.GAMError(context);
            }
            return GAMError_externalReference.Message ;
         }

         set {
            if ( GAMError_externalReference == null )
            {
               GAMError_externalReference = new Artech.Security.GAMError(context);
            }
            GAMError_externalReference.Message = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMError_externalReference == null )
            {
               GAMError_externalReference = new Artech.Security.GAMError(context);
            }
            return GAMError_externalReference ;
         }

         set {
            GAMError_externalReference = (Artech.Security.GAMError)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMError GAMError_externalReference=null ;
   }

}
