/*
               File: PRC_PutRedmineData
        Description: Put Redmine Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:11.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_putredminedata : GXProcedure
   {
      public prc_putredminedata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_putredminedata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho ,
                           int aP1_RdmnIssueId ,
                           int aP2_Status_Id )
      {
         this.AV10AreaTrabalho = aP0_AreaTrabalho;
         this.AV8RdmnIssueId = aP1_RdmnIssueId;
         this.AV15Status_Id = aP2_Status_Id;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_AreaTrabalho ,
                                 int aP1_RdmnIssueId ,
                                 int aP2_Status_Id )
      {
         prc_putredminedata objprc_putredminedata;
         objprc_putredminedata = new prc_putredminedata();
         objprc_putredminedata.AV10AreaTrabalho = aP0_AreaTrabalho;
         objprc_putredminedata.AV8RdmnIssueId = aP1_RdmnIssueId;
         objprc_putredminedata.AV15Status_Id = aP2_Status_Id;
         objprc_putredminedata.context.SetSubmitInitialConfig(context);
         objprc_putredminedata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_putredminedata);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_putredminedata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P009I2 */
         pr_default.execute(0, new Object[] {AV10AreaTrabalho});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P009I2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P009I2_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = P009I2_A5AreaTrabalho_Codigo[0];
            /* Using cursor P009I3 */
            pr_default.execute(1, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1384Redmine_ContratanteCod = P009I3_A1384Redmine_ContratanteCod[0];
               A1381Redmine_Host = P009I3_A1381Redmine_Host[0];
               A1382Redmine_Url = P009I3_A1382Redmine_Url[0];
               A1383Redmine_Key = P009I3_A1383Redmine_Key[0];
               A1380Redmine_Codigo = P009I3_A1380Redmine_Codigo[0];
               AV11Host = A1381Redmine_Host;
               AV12Url = A1382Redmine_Url;
               AV13Key = A1383Redmine_Key;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11Host)) )
         {
            this.cleanup();
            if (true) return;
         }
         else
         {
            AV9httpclient.Host = StringUtil.Trim( AV11Host);
            AV9httpclient.BaseURL = StringUtil.Trim( AV12Url);
            AV16Execute = "issues/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV8RdmnIssueId), 6, 0)) + ".xml?";
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13Key)) )
            {
               AV16Execute = AV16Execute + "&key=" + StringUtil.Trim( AV13Key);
            }
            AV14SDT_IssuePut.gxTpr_Issue.gxTpr_Status_id = (short)(AV15Status_Id);
            AV16Execute = AV16Execute + AV14SDT_IssuePut.ToXml(false, true, "SDT_RedmineIssuePut", "GxEv3Up14_Meetrika");
            AV9httpclient.Execute("PUT", AV16Execute);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009I2_A29Contratante_Codigo = new int[1] ;
         P009I2_n29Contratante_Codigo = new bool[] {false} ;
         P009I2_A5AreaTrabalho_Codigo = new int[1] ;
         P009I3_A1384Redmine_ContratanteCod = new int[1] ;
         P009I3_A1381Redmine_Host = new String[] {""} ;
         P009I3_A1382Redmine_Url = new String[] {""} ;
         P009I3_A1383Redmine_Key = new String[] {""} ;
         P009I3_A1380Redmine_Codigo = new int[1] ;
         A1381Redmine_Host = "";
         A1382Redmine_Url = "";
         A1383Redmine_Key = "";
         AV11Host = "";
         AV12Url = "";
         AV13Key = "";
         AV9httpclient = new GxHttpClient( context);
         AV16Execute = "";
         AV14SDT_IssuePut = new SdtSDT_RedmineIssuePut(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_putredminedata__default(),
            new Object[][] {
                new Object[] {
               P009I2_A29Contratante_Codigo, P009I2_n29Contratante_Codigo, P009I2_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               P009I3_A1384Redmine_ContratanteCod, P009I3_A1381Redmine_Host, P009I3_A1382Redmine_Url, P009I3_A1383Redmine_Key, P009I3_A1380Redmine_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV10AreaTrabalho ;
      private int AV8RdmnIssueId ;
      private int AV15Status_Id ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A1384Redmine_ContratanteCod ;
      private int A1380Redmine_Codigo ;
      private String scmdbuf ;
      private String A1381Redmine_Host ;
      private String A1382Redmine_Url ;
      private String A1383Redmine_Key ;
      private String AV11Host ;
      private String AV12Url ;
      private String AV13Key ;
      private String AV16Execute ;
      private bool n29Contratante_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P009I2_A29Contratante_Codigo ;
      private bool[] P009I2_n29Contratante_Codigo ;
      private int[] P009I2_A5AreaTrabalho_Codigo ;
      private int[] P009I3_A1384Redmine_ContratanteCod ;
      private String[] P009I3_A1381Redmine_Host ;
      private String[] P009I3_A1382Redmine_Url ;
      private String[] P009I3_A1383Redmine_Key ;
      private int[] P009I3_A1380Redmine_Codigo ;
      private GxHttpClient AV9httpclient ;
      private SdtSDT_RedmineIssuePut AV14SDT_IssuePut ;
   }

   public class prc_putredminedata__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009I2 ;
          prmP009I2 = new Object[] {
          new Object[] {"@AV10AreaTrabalho",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009I3 ;
          prmP009I3 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009I2", "SELECT TOP 1 [Contratante_Codigo], [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV10AreaTrabalho ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009I2,1,0,true,true )
             ,new CursorDef("P009I3", "SELECT TOP 1 [Redmine_ContratanteCod], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Contratante_Codigo ORDER BY [Redmine_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009I3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
