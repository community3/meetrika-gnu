/*
               File: ContratoClausulasGeneral
        Description: Contrato Clausulas General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:41.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoclausulasgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoclausulasgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoclausulasgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoClausulas_Codigo )
      {
         this.A152ContratoClausulas_Codigo = aP0_ContratoClausulas_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A152ContratoClausulas_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A152ContratoClausulas_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A152ContratoClausulas_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A152ContratoClausulas_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA6R2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContratoClausulasGeneral";
               context.Gx_err = 0;
               WS6R2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Clausulas General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117174147");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoclausulasgeneral.aspx") + "?" + UrlEncode("" +A152ContratoClausulas_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA152ContratoClausulas_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA152ContratoClausulas_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOCLAUSULAS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A152ContratoClausulas_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOCLAUSULAS_ITEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A153ContratoClausulas_Item, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOCLAUSULAS_DESCRICAO", GetSecureSignedToken( sPrefix, A154ContratoClausulas_Descricao));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm6R2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoclausulasgeneral.js", "?20203117174149");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoClausulasGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Clausulas General" ;
      }

      protected void WB6R0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoclausulasgeneral.aspx");
            }
            wb_table1_2_6R2( true) ;
         }
         else
         {
            wb_table1_2_6R2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6R2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START6R2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Clausulas General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP6R0( ) ;
            }
         }
      }

      protected void WS6R2( )
      {
         START6R2( ) ;
         EVT6R2( ) ;
      }

      protected void EVT6R2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E116R2 */
                                    E116R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E126R2 */
                                    E126R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E136R2 */
                                    E136R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E146R2 */
                                    E146R2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP6R0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE6R2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm6R2( ) ;
            }
         }
      }

      protected void PA6R2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6R2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoClausulasGeneral";
         context.Gx_err = 0;
      }

      protected void RF6R2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H006R2 */
            pr_default.execute(0, new Object[] {A152ContratoClausulas_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A40Contratada_PessoaCod = H006R2_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = H006R2_A74Contrato_Codigo[0];
               A39Contratada_Codigo = H006R2_A39Contratada_Codigo[0];
               A154ContratoClausulas_Descricao = H006R2_A154ContratoClausulas_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A154ContratoClausulas_Descricao", A154ContratoClausulas_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOCLAUSULAS_DESCRICAO", GetSecureSignedToken( sPrefix, A154ContratoClausulas_Descricao));
               A153ContratoClausulas_Item = H006R2_A153ContratoClausulas_Item[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A153ContratoClausulas_Item", A153ContratoClausulas_Item);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOCLAUSULAS_ITEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A153ContratoClausulas_Item, ""))));
               A42Contratada_PessoaCNPJ = H006R2_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H006R2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006R2_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H006R2_n41Contratada_PessoaNom[0];
               A79Contrato_Ano = H006R2_A79Contrato_Ano[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A78Contrato_NumeroAta = H006R2_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = H006R2_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H006R2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               A39Contratada_Codigo = H006R2_A39Contratada_Codigo[0];
               A79Contrato_Ano = H006R2_A79Contrato_Ano[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A78Contrato_NumeroAta = H006R2_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = H006R2_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H006R2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               A40Contratada_PessoaCod = H006R2_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H006R2_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H006R2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006R2_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H006R2_n41Contratada_PessoaNom[0];
               /* Execute user event: E126R2 */
               E126R2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB6R0( ) ;
         }
      }

      protected void STRUP6R0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContratoClausulasGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E116R2 */
         E116R2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
            n78Contrato_NumeroAta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
            n41Contratada_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
            n42Contratada_PessoaCNPJ = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            A153ContratoClausulas_Item = cgiGet( edtContratoClausulas_Item_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A153ContratoClausulas_Item", A153ContratoClausulas_Item);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOCLAUSULAS_ITEM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A153ContratoClausulas_Item, ""))));
            A154ContratoClausulas_Descricao = cgiGet( edtContratoClausulas_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A154ContratoClausulas_Descricao", A154ContratoClausulas_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOCLAUSULAS_DESCRICAO", GetSecureSignedToken( sPrefix, A154ContratoClausulas_Descricao));
            /* Read saved values. */
            wcpOA152ContratoClausulas_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA152ContratoClausulas_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E116R2 */
         E116R2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E116R2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E126R2( )
      {
         /* Load Routine */
         edtContrato_NumeroAta_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_NumeroAta_Internalname, "Link", edtContrato_NumeroAta_Link);
         edtContratada_PessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaNom_Internalname, "Link", edtContratada_PessoaNom_Link);
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E136R2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoclausulas.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A152ContratoClausulas_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E146R2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoclausulas.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A152ContratoClausulas_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoClausulas";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoClausulas_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoClausulas_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_6R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_6R2( true) ;
         }
         else
         {
            wb_table2_8_6R2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_6R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_55_6R2( true) ;
         }
         else
         {
            wb_table3_55_6R2( false) ;
         }
         return  ;
      }

      protected void wb_table3_55_6R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6R2e( true) ;
         }
         else
         {
            wb_table1_2_6R2e( false) ;
         }
      }

      protected void wb_table3_55_6R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_55_6R2e( true) ;
         }
         else
         {
            wb_table3_55_6R2e( false) ;
         }
      }

      protected void wb_table2_8_6R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "do Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoClausulasGeneral.htm");
            wb_table4_12_6R2( true) ;
         }
         else
         {
            wb_table4_12_6R2( false) ;
         }
         return  ;
      }

      protected void wb_table4_12_6R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoclausulas_item_Internalname, "Item", "", "", lblTextblockcontratoclausulas_item_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoClausulas_Item_Internalname, StringUtil.RTrim( A153ContratoClausulas_Item), StringUtil.RTrim( context.localUtil.Format( A153ContratoClausulas_Item, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoClausulas_Item_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "ItemClausula", "left", true, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoclausulas_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoclausulas_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoClausulas_Descricao_Internalname, A154ContratoClausulas_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_6R2e( true) ;
         }
         else
         {
            wb_table2_8_6R2e( false) ;
         }
      }

      protected void wb_table4_12_6R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_17_6R2( true) ;
         }
         else
         {
            wb_table5_17_6R2( false) ;
         }
         return  ;
      }

      protected void wb_table5_17_6R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_33_6R2( true) ;
         }
         else
         {
            wb_table6_33_6R2( false) ;
         }
         return  ;
      }

      protected void wb_table6_33_6R2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_12_6R2e( true) ;
         }
         else
         {
            wb_table4_12_6R2e( false) ;
         }
      }

      protected void wb_table6_33_6R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratada_PessoaNom_Link, "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_33_6R2e( true) ;
         }
         else
         {
            wb_table6_33_6R2e( false) ;
         }
      }

      protected void wb_table5_17_6R2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContrato_NumeroAta_Link, "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoClausulasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_6R2e( true) ;
         }
         else
         {
            wb_table5_17_6R2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A152ContratoClausulas_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A152ContratoClausulas_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A152ContratoClausulas_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6R2( ) ;
         WS6R2( ) ;
         WE6R2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA152ContratoClausulas_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA6R2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoclausulasgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA6R2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A152ContratoClausulas_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A152ContratoClausulas_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A152ContratoClausulas_Codigo), 6, 0)));
         }
         wcpOA152ContratoClausulas_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA152ContratoClausulas_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A152ContratoClausulas_Codigo != wcpOA152ContratoClausulas_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA152ContratoClausulas_Codigo = A152ContratoClausulas_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA152ContratoClausulas_Codigo = cgiGet( sPrefix+"A152ContratoClausulas_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA152ContratoClausulas_Codigo) > 0 )
         {
            A152ContratoClausulas_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA152ContratoClausulas_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A152ContratoClausulas_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A152ContratoClausulas_Codigo), 6, 0)));
         }
         else
         {
            A152ContratoClausulas_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A152ContratoClausulas_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA6R2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS6R2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS6R2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A152ContratoClausulas_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A152ContratoClausulas_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA152ContratoClausulas_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A152ContratoClausulas_Codigo_CTRL", StringUtil.RTrim( sCtrlA152ContratoClausulas_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE6R2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117174214");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoclausulasgeneral.js", "?20203117174215");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = sPrefix+"CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = sPrefix+"TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = sPrefix+"CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = sPrefix+"TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = sPrefix+"TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = sPrefix+"CONTRATO";
         grpUnnamedgroup1_Internalname = sPrefix+"UNNAMEDGROUP1";
         lblTextblockcontratoclausulas_item_Internalname = sPrefix+"TEXTBLOCKCONTRATOCLAUSULAS_ITEM";
         edtContratoClausulas_Item_Internalname = sPrefix+"CONTRATOCLAUSULAS_ITEM";
         lblTextblockcontratoclausulas_descricao_Internalname = sPrefix+"TEXTBLOCKCONTRATOCLAUSULAS_DESCRICAO";
         edtContratoClausulas_Descricao_Internalname = sPrefix+"CONTRATOCLAUSULAS_DESCRICAO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContrato_Ano_Jsonclick = "";
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratoClausulas_Item_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtndelete_Visible = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnupdate_Visible = 1;
         edtContratada_PessoaNom_Link = "";
         edtContrato_NumeroAta_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E136R2',iparms:[{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E146R2',iparms:[{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A153ContratoClausulas_Item = "";
         A154ContratoClausulas_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H006R2_A40Contratada_PessoaCod = new int[1] ;
         H006R2_A152ContratoClausulas_Codigo = new int[1] ;
         H006R2_A74Contrato_Codigo = new int[1] ;
         H006R2_A39Contratada_Codigo = new int[1] ;
         H006R2_A154ContratoClausulas_Descricao = new String[] {""} ;
         H006R2_A153ContratoClausulas_Item = new String[] {""} ;
         H006R2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006R2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006R2_A41Contratada_PessoaNom = new String[] {""} ;
         H006R2_n41Contratada_PessoaNom = new bool[] {false} ;
         H006R2_A79Contrato_Ano = new short[1] ;
         H006R2_A78Contrato_NumeroAta = new String[] {""} ;
         H006R2_n78Contrato_NumeroAta = new bool[] {false} ;
         H006R2_A77Contrato_Numero = new String[] {""} ;
         A42Contratada_PessoaCNPJ = "";
         A41Contratada_PessoaNom = "";
         A78Contrato_NumeroAta = "";
         A77Contrato_Numero = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratoclausulas_item_Jsonclick = "";
         lblTextblockcontratoclausulas_descricao_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA152ContratoClausulas_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoclausulasgeneral__default(),
            new Object[][] {
                new Object[] {
               H006R2_A40Contratada_PessoaCod, H006R2_A152ContratoClausulas_Codigo, H006R2_A74Contrato_Codigo, H006R2_A39Contratada_Codigo, H006R2_A154ContratoClausulas_Descricao, H006R2_A153ContratoClausulas_Item, H006R2_A42Contratada_PessoaCNPJ, H006R2_n42Contratada_PessoaCNPJ, H006R2_A41Contratada_PessoaNom, H006R2_n41Contratada_PessoaNom,
               H006R2_A79Contrato_Ano, H006R2_A78Contrato_NumeroAta, H006R2_n78Contrato_NumeroAta, H006R2_A77Contrato_Numero
               }
            }
         );
         AV14Pgmname = "ContratoClausulasGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoClausulasGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A79Contrato_Ano ;
      private short nGXWrapped ;
      private int A152ContratoClausulas_Codigo ;
      private int wcpOA152ContratoClausulas_Codigo ;
      private int A74Contrato_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7ContratoClausulas_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A153ContratoClausulas_Item ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String A78Contrato_NumeroAta ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContrato_NumeroAta_Internalname ;
      private String edtContrato_Ano_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratoClausulas_Item_Internalname ;
      private String edtContratoClausulas_Descricao_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContrato_NumeroAta_Link ;
      private String edtContratada_PessoaNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String lblTextblockcontratoclausulas_item_Internalname ;
      private String lblTextblockcontratoclausulas_item_Jsonclick ;
      private String edtContratoClausulas_Item_Jsonclick ;
      private String lblTextblockcontratoclausulas_descricao_Internalname ;
      private String lblTextblockcontratoclausulas_descricao_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Jsonclick ;
      private String sCtrlA152ContratoClausulas_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool n78Contrato_NumeroAta ;
      private bool returnInSub ;
      private String A154ContratoClausulas_Descricao ;
      private String A42Contratada_PessoaCNPJ ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H006R2_A40Contratada_PessoaCod ;
      private int[] H006R2_A152ContratoClausulas_Codigo ;
      private int[] H006R2_A74Contrato_Codigo ;
      private int[] H006R2_A39Contratada_Codigo ;
      private String[] H006R2_A154ContratoClausulas_Descricao ;
      private String[] H006R2_A153ContratoClausulas_Item ;
      private String[] H006R2_A42Contratada_PessoaCNPJ ;
      private bool[] H006R2_n42Contratada_PessoaCNPJ ;
      private String[] H006R2_A41Contratada_PessoaNom ;
      private bool[] H006R2_n41Contratada_PessoaNom ;
      private short[] H006R2_A79Contrato_Ano ;
      private String[] H006R2_A78Contrato_NumeroAta ;
      private bool[] H006R2_n78Contrato_NumeroAta ;
      private String[] H006R2_A77Contrato_Numero ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratoclausulasgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006R2 ;
          prmH006R2 = new Object[] {
          new Object[] {"@ContratoClausulas_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006R2", "SELECT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoClausulas_Codigo], T1.[Contrato_Codigo], T2.[Contratada_Codigo], T1.[ContratoClausulas_Descricao], T1.[ContratoClausulas_Item], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Ano], T2.[Contrato_NumeroAta], T2.[Contrato_Numero] FROM ((([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE T1.[ContratoClausulas_Codigo] = @ContratoClausulas_Codigo ORDER BY T1.[ContratoClausulas_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006R2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 10) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((short[]) buf[10])[0] = rslt.getShort(9) ;
                ((String[]) buf[11])[0] = rslt.getString(10, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                ((String[]) buf[13])[0] = rslt.getString(11, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
