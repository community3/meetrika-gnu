/*
               File: ContagemResultadoLiqLog
        Description: Contagem Resultado Liquida Log
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:10:24.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoliqlog : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A1033ContagemResultadoLiqLog_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A1033ContagemResultadoLiqLog_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A1371ContagemResultadoLiqLogOS_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A1371ContagemResultadoLiqLogOS_OSCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A1372ContagemResultadoLiqLogOS_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A1372ContagemResultadoLiqLogOS_UserCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A1374ContagemResultadoLiqLogOS_UserPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1374ContagemResultadoLiqLogOS_UserPesCod = false;
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A1374ContagemResultadoLiqLogOS_UserPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridcontagemresultadoliqlog_os") == 0 )
         {
            nRC_GXsfl_66 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_66_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_66_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridcontagemresultadoliqlog_os_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Liquida Log", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoliqlog( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoliqlog( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_32125( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_32125e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_32125( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_32125( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_32125e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Liquida Log", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoLiqLog.htm");
            wb_table3_28_32125( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_32125e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_32125e( true) ;
         }
         else
         {
            wb_table1_2_32125e( false) ;
         }
      }

      protected void wb_table3_28_32125( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_32125( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_32125e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoLiqLog.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoLiqLog.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_32125e( true) ;
         }
         else
         {
            wb_table3_28_32125e( false) ;
         }
      }

      protected void wb_table4_34_32125( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoliqlog_codigo_Internalname, "Codigo", "", "", lblTextblockcontagemresultadoliqlog_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoLiqLog_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0, ",", "")), ((edtContagemResultadoLiqLog_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1033ContagemResultadoLiqLog_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1033ContagemResultadoLiqLog_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoLiqLog_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoLiqLog_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoliqlog_data_Internalname, "Data", "", "", lblTextblockcontagemresultadoliqlog_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoLiqLog_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoLiqLog_Data_Internalname, context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1034ContagemResultadoLiqLog_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoLiqLog_Data_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoLiqLog_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoLiqLog.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoLiqLog_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoLiqLog_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoliqlog_observacao_Internalname, "Observa��o", "", "", lblTextblockcontagemresultadoliqlog_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEMRESULTADOLIQLOG_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoliqlog_owner_Internalname, "Owner", "", "", lblTextblockcontagemresultadoliqlog_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoLiqLog_Owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1368ContagemResultadoLiqLog_Owner), 6, 0, ",", "")), ((edtContagemResultadoLiqLog_Owner_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1368ContagemResultadoLiqLog_Owner), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1368ContagemResultadoLiqLog_Owner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoLiqLog_Owner_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoLiqLog_Owner_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoliqlog_linhas_Internalname, "Liq Log_Linhas", "", "", lblTextblockcontagemresultadoliqlog_linhas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoLiqLog_Linhas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0, ",", "")), ((edtContagemResultadoLiqLog_Linhas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1369ContagemResultadoLiqLog_Linhas), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1369ContagemResultadoLiqLog_Linhas), "ZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoLiqLog_Linhas_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoLiqLog_Linhas_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "<br/>") ;
            wb_table5_62_32125( true) ;
         }
         return  ;
      }

      protected void wb_table5_62_32125e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<hr class=\"HRDefault\"/>") ;
            /*  Grid Control  */
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("GridName", "Gridcontagemresultadoliqlog_os");
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Class", "Grid");
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcontagemresultadoliqlog_os_Backcolorstyle), 1, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("CmpContext", "");
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("InMasterPage", "false");
            Gridcontagemresultadoliqlog_osColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1370ContagemResultadoLiqLogOS_Codigo), 6, 0, ".", "")));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddColumnProperties(Gridcontagemresultadoliqlog_osColumn);
            Gridcontagemresultadoliqlog_osColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ".", "")));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_OSCod_Enabled), 5, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddColumnProperties(Gridcontagemresultadoliqlog_osColumn);
            Gridcontagemresultadoliqlog_osColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ".", "")));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserCod_Enabled), 5, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddColumnProperties(Gridcontagemresultadoliqlog_osColumn);
            Gridcontagemresultadoliqlog_osColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), 6, 0, ".", "")));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserPesCod_Enabled), 5, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddColumnProperties(Gridcontagemresultadoliqlog_osColumn);
            Gridcontagemresultadoliqlog_osColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Value", StringUtil.RTrim( A1373ContagemResultadoLiqLogOS_UserNom));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserNom_Enabled), 5, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddColumnProperties(Gridcontagemresultadoliqlog_osColumn);
            Gridcontagemresultadoliqlog_osColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1375ContagemResultadoLiqLogOS_Valor, 18, 5, ".", "")));
            Gridcontagemresultadoliqlog_osColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Valor_Enabled), 5, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddColumnProperties(Gridcontagemresultadoliqlog_osColumn);
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcontagemresultadoliqlog_os_Allowselection), 1, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcontagemresultadoliqlog_os_Selectioncolor), 9, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcontagemresultadoliqlog_os_Allowhovering), 1, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcontagemresultadoliqlog_os_Hoveringcolor), 9, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcontagemresultadoliqlog_os_Allowcollapsing), 1, 0, ".", "")));
            Gridcontagemresultadoliqlog_osContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcontagemresultadoliqlog_os_Collapsed), 1, 0, ".", "")));
            nGXsfl_66_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount164 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_164 = 1;
                  ScanStart32164( ) ;
                  while ( RcdFound164 != 0 )
                  {
                     init_level_properties164( ) ;
                     getByPrimaryKey32164( ) ;
                     AddRow32164( ) ;
                     ScanNext32164( ) ;
                  }
                  ScanEnd32164( ) ;
                  nBlankRcdCount164 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
               standaloneNotModal32164( ) ;
               standaloneModal32164( ) ;
               sMode164 = Gx_mode;
               while ( nGXsfl_66_idx < nRC_GXsfl_66 )
               {
                  ReadRow32164( ) ;
                  edtContagemResultadoLiqLogOS_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_CODIGO_"+sGXsfl_66_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0)));
                  edtContagemResultadoLiqLogOS_OSCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_OSCOD_"+sGXsfl_66_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_OSCod_Enabled), 5, 0)));
                  edtContagemResultadoLiqLogOS_UserCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_USERCOD_"+sGXsfl_66_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_UserCod_Enabled), 5, 0)));
                  edtContagemResultadoLiqLogOS_UserPesCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD_"+sGXsfl_66_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_UserPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_UserPesCod_Enabled), 5, 0)));
                  edtContagemResultadoLiqLogOS_UserNom_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_USERNOM_"+sGXsfl_66_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_UserNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_UserNom_Enabled), 5, 0)));
                  edtContagemResultadoLiqLogOS_Valor_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_VALOR_"+sGXsfl_66_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_Valor_Enabled), 5, 0)));
                  if ( ( nRcdExists_164 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     standaloneModal32164( ) ;
                  }
                  SendRow32164( ) ;
               }
               Gx_mode = sMode164;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A1369ContagemResultadoLiqLog_Linhas = B1369ContagemResultadoLiqLog_Linhas;
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount164 = 5;
               nRcdExists_164 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart32164( ) ;
                  while ( RcdFound164 != 0 )
                  {
                     sGXsfl_66_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_66_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_66164( ) ;
                     init_level_properties164( ) ;
                     standaloneNotModal32164( ) ;
                     getByPrimaryKey32164( ) ;
                     standaloneModal32164( ) ;
                     AddRow32164( ) ;
                     ScanNext32164( ) ;
                  }
                  ScanEnd32164( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            sMode164 = Gx_mode;
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            sGXsfl_66_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_66_idx+1), 4, 0)), 4, "0");
            SubsflControlProps_66164( ) ;
            InitAll32164( ) ;
            init_level_properties164( ) ;
            B1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            standaloneNotModal32164( ) ;
            standaloneModal32164( ) ;
            nRcdExists_164 = 0;
            nIsMod_164 = 0;
            nRcdDeleted_164 = 0;
            nBlankRcdCount164 = (short)(nBlankRcdUsr164+nBlankRcdCount164);
            fRowAdded = 0;
            while ( nBlankRcdCount164 > 0 )
            {
               AddRow32164( ) ;
               if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
               {
                  fRowAdded = 1;
                  GX_FocusControl = edtContagemResultadoLiqLogOS_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               nBlankRcdCount164 = (short)(nBlankRcdCount164-1);
            }
            Gx_mode = sMode164;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            A1369ContagemResultadoLiqLog_Linhas = B1369ContagemResultadoLiqLog_Linhas;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Gridcontagemresultadoliqlog_osContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridcontagemresultadoliqlog_os", Gridcontagemresultadoliqlog_osContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridcontagemresultadoliqlog_osContainerData", Gridcontagemresultadoliqlog_osContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridcontagemresultadoliqlog_osContainerData"+"V", Gridcontagemresultadoliqlog_osContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridcontagemresultadoliqlog_osContainerData"+"V"+"\" value='"+Gridcontagemresultadoliqlog_osContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_32125e( true) ;
         }
         else
         {
            wb_table4_34_32125e( false) ;
         }
      }

      protected void wb_table5_62_32125( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table95", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='SubTitle'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitleos_Internalname, "OS", "", "", lblTitleos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 0, "HLP_ContagemResultadoLiqLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_62_32125e( true) ;
         }
         else
         {
            wb_table5_62_32125e( false) ;
         }
      }

      protected void wb_table2_5_32125( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoLiqLog.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_32125e( true) ;
         }
         else
         {
            wb_table2_5_32125e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLog_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLog_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOLIQLOG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1033ContagemResultadoLiqLog_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
               }
               else
               {
                  A1033ContagemResultadoLiqLog_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLog_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoLiqLog_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "CONTAGEMRESULTADOLIQLOG_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1034ContagemResultadoLiqLog_Data", context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1034ContagemResultadoLiqLog_Data = context.localUtil.CToT( cgiGet( edtContagemResultadoLiqLog_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1034ContagemResultadoLiqLog_Data", context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLog_Owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLog_Owner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOLIQLOG_OWNER");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoLiqLog_Owner_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1368ContagemResultadoLiqLog_Owner = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1368ContagemResultadoLiqLog_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1368ContagemResultadoLiqLog_Owner), 6, 0)));
               }
               else
               {
                  A1368ContagemResultadoLiqLog_Owner = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLog_Owner_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1368ContagemResultadoLiqLog_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1368ContagemResultadoLiqLog_Owner), 6, 0)));
               }
               A1369ContagemResultadoLiqLog_Linhas = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLog_Linhas_Internalname), ",", "."));
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
               /* Read saved values. */
               Z1033ContagemResultadoLiqLog_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1033ContagemResultadoLiqLog_Codigo"), ",", "."));
               Z1034ContagemResultadoLiqLog_Data = context.localUtil.CToT( cgiGet( "Z1034ContagemResultadoLiqLog_Data"), 0);
               Z1368ContagemResultadoLiqLog_Owner = (int)(context.localUtil.CToN( cgiGet( "Z1368ContagemResultadoLiqLog_Owner"), ",", "."));
               O1369ContagemResultadoLiqLog_Linhas = (int)(context.localUtil.CToN( cgiGet( "O1369ContagemResultadoLiqLog_Linhas"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_66 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_66"), ",", "."));
               A1366ContagemResultadoLiqLog_Observacao = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO");
               n1366ContagemResultadoLiqLog_Observacao = false;
               n1366ContagemResultadoLiqLog_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1366ContagemResultadoLiqLog_Observacao)) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Contagemresultadoliqlog_observacao_Width = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Width");
               Contagemresultadoliqlog_observacao_Height = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Height");
               Contagemresultadoliqlog_observacao_Skin = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Skin");
               Contagemresultadoliqlog_observacao_Toolbar = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Toolbar");
               Contagemresultadoliqlog_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Color"), ",", "."));
               Contagemresultadoliqlog_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Enabled"));
               Contagemresultadoliqlog_observacao_Class = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Class");
               Contagemresultadoliqlog_observacao_Customtoolbar = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Customtoolbar");
               Contagemresultadoliqlog_observacao_Customconfiguration = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Customconfiguration");
               Contagemresultadoliqlog_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Toolbarcancollapse"));
               Contagemresultadoliqlog_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Toolbarexpanded"));
               Contagemresultadoliqlog_observacao_Buttonpressedid = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Buttonpressedid");
               Contagemresultadoliqlog_observacao_Captionvalue = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Captionvalue");
               Contagemresultadoliqlog_observacao_Captionclass = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Captionclass");
               Contagemresultadoliqlog_observacao_Captionposition = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Captionposition");
               Contagemresultadoliqlog_observacao_Coltitle = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Coltitle");
               Contagemresultadoliqlog_observacao_Coltitlefont = cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Coltitlefont");
               Contagemresultadoliqlog_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Coltitlecolor"), ",", "."));
               Contagemresultadoliqlog_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Usercontroliscolumn"));
               Contagemresultadoliqlog_observacao_Visible = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1033ContagemResultadoLiqLog_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll32125( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes32125( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_32164( )
      {
         s1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         nGXsfl_66_idx = 0;
         while ( nGXsfl_66_idx < nRC_GXsfl_66 )
         {
            ReadRow32164( ) ;
            if ( ( nRcdExists_164 != 0 ) || ( nIsMod_164 != 0 ) )
            {
               GetKey32164( ) ;
               if ( ( nRcdExists_164 == 0 ) && ( nRcdDeleted_164 == 0 ) )
               {
                  if ( RcdFound164 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate32164( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable32164( ) ;
                        CloseExtendedTableCursors32164( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
                        n1369ContagemResultadoLiqLog_Linhas = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                     }
                  }
                  else
                  {
                     GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_CODIGO_" + sGXsfl_66_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoLiqLogOS_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound164 != 0 )
                  {
                     if ( nRcdDeleted_164 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey32164( ) ;
                        Load32164( ) ;
                        BeforeValidate32164( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls32164( ) ;
                           O1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
                           n1369ContagemResultadoLiqLog_Linhas = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_164 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate32164( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable32164( ) ;
                              CloseExtendedTableCursors32164( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
                              n1369ContagemResultadoLiqLog_Linhas = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_164 == 0 )
                     {
                        GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_CODIGO_" + sGXsfl_66_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultadoLiqLogOS_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtContagemResultadoLiqLogOS_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1370ContagemResultadoLiqLogOS_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_UserPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_UserNom_Internalname, StringUtil.RTrim( A1373ContagemResultadoLiqLogOS_UserNom)) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1375ContagemResultadoLiqLogOS_Valor, 18, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1370ContagemResultadoLiqLogOS_Codigo_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1370ContagemResultadoLiqLogOS_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1375ContagemResultadoLiqLogOS_Valor_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( Z1375ContagemResultadoLiqLogOS_Valor, 18, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1372ContagemResultadoLiqLogOS_UserCod_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1371ContagemResultadoLiqLogOS_OSCod_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_164_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_164), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_164_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_164), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_164_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_164), 4, 0, ",", ""))) ;
            if ( nIsMod_164 != 0 )
            {
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_CODIGO_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_OSCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_OSCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_USERCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserPesCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_USERNOM_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserNom_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_VALOR_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Valor_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O1369ContagemResultadoLiqLog_Linhas = s1369ContagemResultadoLiqLog_Linhas;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption320( )
      {
      }

      protected void ZM32125( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1034ContagemResultadoLiqLog_Data = T00328_A1034ContagemResultadoLiqLog_Data[0];
               Z1368ContagemResultadoLiqLog_Owner = T00328_A1368ContagemResultadoLiqLog_Owner[0];
            }
            else
            {
               Z1034ContagemResultadoLiqLog_Data = A1034ContagemResultadoLiqLog_Data;
               Z1368ContagemResultadoLiqLog_Owner = A1368ContagemResultadoLiqLog_Owner;
            }
         }
         if ( GX_JID == -7 )
         {
            Z1033ContagemResultadoLiqLog_Codigo = A1033ContagemResultadoLiqLog_Codigo;
            Z1034ContagemResultadoLiqLog_Data = A1034ContagemResultadoLiqLog_Data;
            Z1366ContagemResultadoLiqLog_Observacao = A1366ContagemResultadoLiqLog_Observacao;
            Z1368ContagemResultadoLiqLog_Owner = A1368ContagemResultadoLiqLog_Owner;
            Z1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContagemResultadoLiqLog_Linhas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Linhas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Linhas_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtContagemResultadoLiqLog_Linhas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Linhas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Linhas_Enabled), 5, 0)));
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load32125( )
      {
         /* Using cursor T003212 */
         pr_default.execute(8, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound125 = 1;
            A1034ContagemResultadoLiqLog_Data = T003212_A1034ContagemResultadoLiqLog_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1034ContagemResultadoLiqLog_Data", context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " "));
            A1366ContagemResultadoLiqLog_Observacao = T003212_A1366ContagemResultadoLiqLog_Observacao[0];
            n1366ContagemResultadoLiqLog_Observacao = T003212_n1366ContagemResultadoLiqLog_Observacao[0];
            A1368ContagemResultadoLiqLog_Owner = T003212_A1368ContagemResultadoLiqLog_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1368ContagemResultadoLiqLog_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1368ContagemResultadoLiqLog_Owner), 6, 0)));
            A1369ContagemResultadoLiqLog_Linhas = T003212_A1369ContagemResultadoLiqLog_Linhas[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            n1369ContagemResultadoLiqLog_Linhas = T003212_n1369ContagemResultadoLiqLog_Linhas[0];
            ZM32125( -7) ;
         }
         pr_default.close(8);
         OnLoadActions32125( ) ;
      }

      protected void OnLoadActions32125( )
      {
         O1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
      }

      protected void CheckExtendedTable32125( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T003210 */
         pr_default.execute(7, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A1369ContagemResultadoLiqLog_Linhas = T003210_A1369ContagemResultadoLiqLog_Linhas[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            n1369ContagemResultadoLiqLog_Linhas = T003210_n1369ContagemResultadoLiqLog_Linhas[0];
         }
         else
         {
            A1369ContagemResultadoLiqLog_Linhas = 0;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         }
         pr_default.close(7);
         if ( ! ( (DateTime.MinValue==A1034ContagemResultadoLiqLog_Data) || ( A1034ContagemResultadoLiqLog_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOLIQLOG_DATA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors32125( )
      {
         pr_default.close(7);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( int A1033ContagemResultadoLiqLog_Codigo )
      {
         /* Using cursor T003214 */
         pr_default.execute(9, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            A1369ContagemResultadoLiqLog_Linhas = T003214_A1369ContagemResultadoLiqLog_Linhas[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            n1369ContagemResultadoLiqLog_Linhas = T003214_n1369ContagemResultadoLiqLog_Linhas[0];
         }
         else
         {
            A1369ContagemResultadoLiqLog_Linhas = 0;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void GetKey32125( )
      {
         /* Using cursor T003215 */
         pr_default.execute(10, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound125 = 1;
         }
         else
         {
            RcdFound125 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00328 */
         pr_default.execute(6, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            ZM32125( 7) ;
            RcdFound125 = 1;
            A1033ContagemResultadoLiqLog_Codigo = T00328_A1033ContagemResultadoLiqLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
            A1034ContagemResultadoLiqLog_Data = T00328_A1034ContagemResultadoLiqLog_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1034ContagemResultadoLiqLog_Data", context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " "));
            A1366ContagemResultadoLiqLog_Observacao = T00328_A1366ContagemResultadoLiqLog_Observacao[0];
            n1366ContagemResultadoLiqLog_Observacao = T00328_n1366ContagemResultadoLiqLog_Observacao[0];
            A1368ContagemResultadoLiqLog_Owner = T00328_A1368ContagemResultadoLiqLog_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1368ContagemResultadoLiqLog_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1368ContagemResultadoLiqLog_Owner), 6, 0)));
            Z1033ContagemResultadoLiqLog_Codigo = A1033ContagemResultadoLiqLog_Codigo;
            sMode125 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load32125( ) ;
            if ( AnyError == 1 )
            {
               RcdFound125 = 0;
               InitializeNonKey32125( ) ;
            }
            Gx_mode = sMode125;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound125 = 0;
            InitializeNonKey32125( ) ;
            sMode125 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode125;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(6);
      }

      protected void getEqualNoModal( )
      {
         GetKey32125( ) ;
         if ( RcdFound125 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound125 = 0;
         /* Using cursor T003216 */
         pr_default.execute(11, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T003216_A1033ContagemResultadoLiqLog_Codigo[0] < A1033ContagemResultadoLiqLog_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T003216_A1033ContagemResultadoLiqLog_Codigo[0] > A1033ContagemResultadoLiqLog_Codigo ) ) )
            {
               A1033ContagemResultadoLiqLog_Codigo = T003216_A1033ContagemResultadoLiqLog_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
               RcdFound125 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void move_previous( )
      {
         RcdFound125 = 0;
         /* Using cursor T003217 */
         pr_default.execute(12, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T003217_A1033ContagemResultadoLiqLog_Codigo[0] > A1033ContagemResultadoLiqLog_Codigo ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T003217_A1033ContagemResultadoLiqLog_Codigo[0] < A1033ContagemResultadoLiqLog_Codigo ) ) )
            {
               A1033ContagemResultadoLiqLog_Codigo = T003217_A1033ContagemResultadoLiqLog_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
               RcdFound125 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey32125( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert32125( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound125 == 1 )
            {
               if ( A1033ContagemResultadoLiqLog_Codigo != Z1033ContagemResultadoLiqLog_Codigo )
               {
                  A1033ContagemResultadoLiqLog_Codigo = Z1033ContagemResultadoLiqLog_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOLIQLOG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
                  n1369ContagemResultadoLiqLog_Linhas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
                  n1369ContagemResultadoLiqLog_Linhas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                  Update32125( ) ;
                  GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1033ContagemResultadoLiqLog_Codigo != Z1033ContagemResultadoLiqLog_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
                  n1369ContagemResultadoLiqLog_Linhas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                  GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert32125( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOLIQLOG_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
                     n1369ContagemResultadoLiqLog_Linhas = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                     GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert32125( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1033ContagemResultadoLiqLog_Codigo != Z1033ContagemResultadoLiqLog_Codigo )
         {
            A1033ContagemResultadoLiqLog_Codigo = Z1033ContagemResultadoLiqLog_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOLIQLOG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound125 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOLIQLOG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart32125( ) ;
         if ( RcdFound125 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd32125( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound125 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound125 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart32125( ) ;
         if ( RcdFound125 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound125 != 0 )
            {
               ScanNext32125( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd32125( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency32125( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00327 */
            pr_default.execute(5, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
            if ( (pr_default.getStatus(5) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoLiqLog"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(5) == 101) || ( Z1034ContagemResultadoLiqLog_Data != T00327_A1034ContagemResultadoLiqLog_Data[0] ) || ( Z1368ContagemResultadoLiqLog_Owner != T00327_A1368ContagemResultadoLiqLog_Owner[0] ) )
            {
               if ( Z1034ContagemResultadoLiqLog_Data != T00327_A1034ContagemResultadoLiqLog_Data[0] )
               {
                  GXUtil.WriteLog("contagemresultadoliqlog:[seudo value changed for attri]"+"ContagemResultadoLiqLog_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1034ContagemResultadoLiqLog_Data);
                  GXUtil.WriteLogRaw("Current: ",T00327_A1034ContagemResultadoLiqLog_Data[0]);
               }
               if ( Z1368ContagemResultadoLiqLog_Owner != T00327_A1368ContagemResultadoLiqLog_Owner[0] )
               {
                  GXUtil.WriteLog("contagemresultadoliqlog:[seudo value changed for attri]"+"ContagemResultadoLiqLog_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z1368ContagemResultadoLiqLog_Owner);
                  GXUtil.WriteLogRaw("Current: ",T00327_A1368ContagemResultadoLiqLog_Owner[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoLiqLog"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert32125( )
      {
         BeforeValidate32125( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable32125( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM32125( 0) ;
            CheckOptimisticConcurrency32125( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm32125( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert32125( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003218 */
                     pr_default.execute(13, new Object[] {A1034ContagemResultadoLiqLog_Data, n1366ContagemResultadoLiqLog_Observacao, A1366ContagemResultadoLiqLog_Observacao, A1368ContagemResultadoLiqLog_Owner});
                     A1033ContagemResultadoLiqLog_Codigo = T003218_A1033ContagemResultadoLiqLog_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLog") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel32125( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption320( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load32125( ) ;
            }
            EndLevel32125( ) ;
         }
         CloseExtendedTableCursors32125( ) ;
      }

      protected void Update32125( )
      {
         BeforeValidate32125( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable32125( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency32125( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm32125( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate32125( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003219 */
                     pr_default.execute(14, new Object[] {A1034ContagemResultadoLiqLog_Data, n1366ContagemResultadoLiqLog_Observacao, A1366ContagemResultadoLiqLog_Observacao, A1368ContagemResultadoLiqLog_Owner, A1033ContagemResultadoLiqLog_Codigo});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLog") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoLiqLog"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate32125( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel32125( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                              ResetCaption320( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel32125( ) ;
         }
         CloseExtendedTableCursors32125( ) ;
      }

      protected void DeferredUpdate32125( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate32125( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency32125( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls32125( ) ;
            AfterConfirm32125( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete32125( ) ;
               if ( AnyError == 0 )
               {
                  A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
                  n1369ContagemResultadoLiqLog_Linhas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                  ScanStart32164( ) ;
                  while ( RcdFound164 != 0 )
                  {
                     getByPrimaryKey32164( ) ;
                     Delete32164( ) ;
                     ScanNext32164( ) ;
                     O1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
                     n1369ContagemResultadoLiqLog_Linhas = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                  }
                  ScanEnd32164( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003220 */
                     pr_default.execute(15, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLog") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound125 == 0 )
                           {
                              InitAll32125( ) ;
                              Gx_mode = "INS";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                           ResetCaption320( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode125 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel32125( ) ;
         Gx_mode = sMode125;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls32125( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003222 */
            pr_default.execute(16, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               A1369ContagemResultadoLiqLog_Linhas = T003222_A1369ContagemResultadoLiqLog_Linhas[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
               n1369ContagemResultadoLiqLog_Linhas = T003222_n1369ContagemResultadoLiqLog_Linhas[0];
            }
            else
            {
               A1369ContagemResultadoLiqLog_Linhas = 0;
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            }
            pr_default.close(16);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T003223 */
            pr_default.execute(17, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
         }
      }

      protected void ProcessNestedLevel32164( )
      {
         s1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         nGXsfl_66_idx = 0;
         while ( nGXsfl_66_idx < nRC_GXsfl_66 )
         {
            ReadRow32164( ) ;
            if ( ( nRcdExists_164 != 0 ) || ( nIsMod_164 != 0 ) )
            {
               standaloneNotModal32164( ) ;
               GetKey32164( ) ;
               if ( ( nRcdExists_164 == 0 ) && ( nRcdDeleted_164 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Insert32164( ) ;
               }
               else
               {
                  if ( RcdFound164 != 0 )
                  {
                     if ( ( nRcdDeleted_164 != 0 ) && ( nRcdExists_164 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        Delete32164( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_164 != 0 ) && ( nRcdExists_164 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           Update32164( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_164 == 0 )
                     {
                        GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_CODIGO_" + sGXsfl_66_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultadoLiqLogOS_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
               O1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            }
            ChangePostValue( edtContagemResultadoLiqLogOS_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1370ContagemResultadoLiqLogOS_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_OSCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_UserPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), 6, 0, ",", ""))) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_UserNom_Internalname, StringUtil.RTrim( A1373ContagemResultadoLiqLogOS_UserNom)) ;
            ChangePostValue( edtContagemResultadoLiqLogOS_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1375ContagemResultadoLiqLogOS_Valor, 18, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1370ContagemResultadoLiqLogOS_Codigo_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1370ContagemResultadoLiqLogOS_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1375ContagemResultadoLiqLogOS_Valor_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( Z1375ContagemResultadoLiqLogOS_Valor, 18, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1372ContagemResultadoLiqLogOS_UserCod_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1371ContagemResultadoLiqLogOS_OSCod_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_164_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_164), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_164_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_164), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_164_"+sGXsfl_66_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_164), 4, 0, ",", ""))) ;
            if ( nIsMod_164 != 0 )
            {
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_CODIGO_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_OSCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_OSCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_USERCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserPesCod_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_USERNOM_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserNom_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTAGEMRESULTADOLIQLOGOS_VALOR_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Valor_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll32164( ) ;
         if ( AnyError != 0 )
         {
            O1369ContagemResultadoLiqLog_Linhas = s1369ContagemResultadoLiqLog_Linhas;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         }
         nRcdExists_164 = 0;
         nIsMod_164 = 0;
         nRcdDeleted_164 = 0;
      }

      protected void ProcessLevel32125( )
      {
         /* Save parent mode. */
         sMode125 = Gx_mode;
         ProcessNestedLevel32164( ) ;
         if ( AnyError != 0 )
         {
            O1369ContagemResultadoLiqLog_Linhas = s1369ContagemResultadoLiqLog_Linhas;
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode125;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel32125( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(5);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete32125( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(6);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(16);
            pr_default.close(2);
            pr_default.close(3);
            pr_default.close(4);
            context.CommitDataStores( "ContagemResultadoLiqLog");
            if ( AnyError == 0 )
            {
               ConfirmValues320( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(6);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(16);
            pr_default.close(2);
            pr_default.close(3);
            pr_default.close(4);
            context.RollbackDataStores( "ContagemResultadoLiqLog");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart32125( )
      {
         /* Using cursor T003224 */
         pr_default.execute(18);
         RcdFound125 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound125 = 1;
            A1033ContagemResultadoLiqLog_Codigo = T003224_A1033ContagemResultadoLiqLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext32125( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound125 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound125 = 1;
            A1033ContagemResultadoLiqLog_Codigo = T003224_A1033ContagemResultadoLiqLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd32125( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm32125( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert32125( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate32125( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete32125( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete32125( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate32125( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes32125( )
      {
         edtContagemResultadoLiqLog_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Codigo_Enabled), 5, 0)));
         edtContagemResultadoLiqLog_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Data_Enabled), 5, 0)));
         edtContagemResultadoLiqLog_Owner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Owner_Enabled), 5, 0)));
         edtContagemResultadoLiqLog_Linhas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Linhas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Linhas_Enabled), 5, 0)));
         Contagemresultadoliqlog_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagemresultadoliqlog_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Contagemresultadoliqlog_observacao_Enabled));
      }

      protected void ZM32164( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1375ContagemResultadoLiqLogOS_Valor = T00323_A1375ContagemResultadoLiqLogOS_Valor[0];
               Z1372ContagemResultadoLiqLogOS_UserCod = T00323_A1372ContagemResultadoLiqLogOS_UserCod[0];
               Z1371ContagemResultadoLiqLogOS_OSCod = T00323_A1371ContagemResultadoLiqLogOS_OSCod[0];
            }
            else
            {
               Z1375ContagemResultadoLiqLogOS_Valor = A1375ContagemResultadoLiqLogOS_Valor;
               Z1372ContagemResultadoLiqLogOS_UserCod = A1372ContagemResultadoLiqLogOS_UserCod;
               Z1371ContagemResultadoLiqLogOS_OSCod = A1371ContagemResultadoLiqLogOS_OSCod;
            }
         }
         if ( GX_JID == -9 )
         {
            Z1033ContagemResultadoLiqLog_Codigo = A1033ContagemResultadoLiqLog_Codigo;
            Z1370ContagemResultadoLiqLogOS_Codigo = A1370ContagemResultadoLiqLogOS_Codigo;
            Z1375ContagemResultadoLiqLogOS_Valor = A1375ContagemResultadoLiqLogOS_Valor;
            Z1372ContagemResultadoLiqLogOS_UserCod = A1372ContagemResultadoLiqLogOS_UserCod;
            Z1371ContagemResultadoLiqLogOS_OSCod = A1371ContagemResultadoLiqLogOS_OSCod;
            Z1374ContagemResultadoLiqLogOS_UserPesCod = A1374ContagemResultadoLiqLogOS_UserPesCod;
            Z1373ContagemResultadoLiqLogOS_UserNom = A1373ContagemResultadoLiqLogOS_UserNom;
         }
      }

      protected void standaloneNotModal32164( )
      {
         edtContagemResultadoLiqLog_Linhas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Linhas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Linhas_Enabled), 5, 0)));
         edtContagemResultadoLiqLog_Linhas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Linhas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Linhas_Enabled), 5, 0)));
      }

      protected void standaloneModal32164( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1369ContagemResultadoLiqLog_Linhas = (int)(O1369ContagemResultadoLiqLog_Linhas+1);
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( Gx_BScreen == 1 ) )
         {
            A1370ContagemResultadoLiqLogOS_Codigo = A1369ContagemResultadoLiqLog_Linhas;
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtContagemResultadoLiqLogOS_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultadoLiqLogOS_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0)));
         }
      }

      protected void Load32164( )
      {
         /* Using cursor T003225 */
         pr_default.execute(19, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound164 = 1;
            A1373ContagemResultadoLiqLogOS_UserNom = T003225_A1373ContagemResultadoLiqLogOS_UserNom[0];
            n1373ContagemResultadoLiqLogOS_UserNom = T003225_n1373ContagemResultadoLiqLogOS_UserNom[0];
            A1375ContagemResultadoLiqLogOS_Valor = T003225_A1375ContagemResultadoLiqLogOS_Valor[0];
            A1372ContagemResultadoLiqLogOS_UserCod = T003225_A1372ContagemResultadoLiqLogOS_UserCod[0];
            A1371ContagemResultadoLiqLogOS_OSCod = T003225_A1371ContagemResultadoLiqLogOS_OSCod[0];
            A1374ContagemResultadoLiqLogOS_UserPesCod = T003225_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
            n1374ContagemResultadoLiqLogOS_UserPesCod = T003225_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
            ZM32164( -9) ;
         }
         pr_default.close(19);
         OnLoadActions32164( ) ;
      }

      protected void OnLoadActions32164( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1369ContagemResultadoLiqLog_Linhas = (int)(O1369ContagemResultadoLiqLog_Linhas+1);
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1369ContagemResultadoLiqLog_Linhas = (int)(O1369ContagemResultadoLiqLog_Linhas-1);
                  n1369ContagemResultadoLiqLog_Linhas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
               }
            }
         }
      }

      protected void CheckExtendedTable32164( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal32164( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1369ContagemResultadoLiqLog_Linhas = (int)(O1369ContagemResultadoLiqLog_Linhas+1);
            n1369ContagemResultadoLiqLog_Linhas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1369ContagemResultadoLiqLog_Linhas = (int)(O1369ContagemResultadoLiqLog_Linhas-1);
                  n1369ContagemResultadoLiqLog_Linhas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
               }
            }
         }
         /* Using cursor T00325 */
         pr_default.execute(3, new Object[] {A1371ContagemResultadoLiqLogOS_OSCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_OSCOD_" + sGXsfl_66_idx;
            GX_msglist.addItem("N�o existe 'Contagem Resultado Liq Log Os_Contagem Resultado'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T00324 */
         pr_default.execute(2, new Object[] {A1372ContagemResultadoLiqLogOS_UserCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_USERCOD_" + sGXsfl_66_idx;
            GX_msglist.addItem("N�o existe 'Contagem Resultado Liq Log_Usuario'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1374ContagemResultadoLiqLogOS_UserPesCod = T00324_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
         n1374ContagemResultadoLiqLogOS_UserPesCod = T00324_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
         pr_default.close(2);
         /* Using cursor T00326 */
         pr_default.execute(4, new Object[] {n1374ContagemResultadoLiqLogOS_UserPesCod, A1374ContagemResultadoLiqLogOS_UserPesCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1373ContagemResultadoLiqLogOS_UserNom = T00326_A1373ContagemResultadoLiqLogOS_UserNom[0];
         n1373ContagemResultadoLiqLogOS_UserNom = T00326_n1373ContagemResultadoLiqLogOS_UserNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors32164( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(4);
      }

      protected void enableDisable32164( )
      {
      }

      protected void gxLoad_11( int A1371ContagemResultadoLiqLogOS_OSCod )
      {
         /* Using cursor T003226 */
         pr_default.execute(20, new Object[] {A1371ContagemResultadoLiqLogOS_OSCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_OSCOD_" + sGXsfl_66_idx;
            GX_msglist.addItem("N�o existe 'Contagem Resultado Liq Log Os_Contagem Resultado'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_OSCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(20);
      }

      protected void gxLoad_10( int A1372ContagemResultadoLiqLogOS_UserCod )
      {
         /* Using cursor T003227 */
         pr_default.execute(21, new Object[] {A1372ContagemResultadoLiqLogOS_UserCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_USERCOD_" + sGXsfl_66_idx;
            GX_msglist.addItem("N�o existe 'Contagem Resultado Liq Log_Usuario'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1374ContagemResultadoLiqLogOS_UserPesCod = T003227_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
         n1374ContagemResultadoLiqLogOS_UserPesCod = T003227_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(21) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(21);
      }

      protected void gxLoad_12( int A1374ContagemResultadoLiqLogOS_UserPesCod )
      {
         /* Using cursor T003228 */
         pr_default.execute(22, new Object[] {n1374ContagemResultadoLiqLogOS_UserPesCod, A1374ContagemResultadoLiqLogOS_UserPesCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1373ContagemResultadoLiqLogOS_UserNom = T003228_A1373ContagemResultadoLiqLogOS_UserNom[0];
         n1373ContagemResultadoLiqLogOS_UserNom = T003228_n1373ContagemResultadoLiqLogOS_UserNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1373ContagemResultadoLiqLogOS_UserNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(22) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(22);
      }

      protected void GetKey32164( )
      {
         /* Using cursor T003229 */
         pr_default.execute(23, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo});
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound164 = 1;
         }
         else
         {
            RcdFound164 = 0;
         }
         pr_default.close(23);
      }

      protected void getByPrimaryKey32164( )
      {
         /* Using cursor T00323 */
         pr_default.execute(1, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM32164( 9) ;
            RcdFound164 = 1;
            InitializeNonKey32164( ) ;
            A1370ContagemResultadoLiqLogOS_Codigo = T00323_A1370ContagemResultadoLiqLogOS_Codigo[0];
            A1375ContagemResultadoLiqLogOS_Valor = T00323_A1375ContagemResultadoLiqLogOS_Valor[0];
            A1372ContagemResultadoLiqLogOS_UserCod = T00323_A1372ContagemResultadoLiqLogOS_UserCod[0];
            A1371ContagemResultadoLiqLogOS_OSCod = T00323_A1371ContagemResultadoLiqLogOS_OSCod[0];
            Z1033ContagemResultadoLiqLog_Codigo = A1033ContagemResultadoLiqLog_Codigo;
            Z1370ContagemResultadoLiqLogOS_Codigo = A1370ContagemResultadoLiqLogOS_Codigo;
            sMode164 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal32164( ) ;
            Load32164( ) ;
            Gx_mode = sMode164;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound164 = 0;
            InitializeNonKey32164( ) ;
            sMode164 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal32164( ) ;
            Gx_mode = sMode164;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes32164( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency32164( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00322 */
            pr_default.execute(0, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoLiqLogOS"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1375ContagemResultadoLiqLogOS_Valor != T00322_A1375ContagemResultadoLiqLogOS_Valor[0] ) || ( Z1372ContagemResultadoLiqLogOS_UserCod != T00322_A1372ContagemResultadoLiqLogOS_UserCod[0] ) || ( Z1371ContagemResultadoLiqLogOS_OSCod != T00322_A1371ContagemResultadoLiqLogOS_OSCod[0] ) )
            {
               if ( Z1375ContagemResultadoLiqLogOS_Valor != T00322_A1375ContagemResultadoLiqLogOS_Valor[0] )
               {
                  GXUtil.WriteLog("contagemresultadoliqlog:[seudo value changed for attri]"+"ContagemResultadoLiqLogOS_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z1375ContagemResultadoLiqLogOS_Valor);
                  GXUtil.WriteLogRaw("Current: ",T00322_A1375ContagemResultadoLiqLogOS_Valor[0]);
               }
               if ( Z1372ContagemResultadoLiqLogOS_UserCod != T00322_A1372ContagemResultadoLiqLogOS_UserCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoliqlog:[seudo value changed for attri]"+"ContagemResultadoLiqLogOS_UserCod");
                  GXUtil.WriteLogRaw("Old: ",Z1372ContagemResultadoLiqLogOS_UserCod);
                  GXUtil.WriteLogRaw("Current: ",T00322_A1372ContagemResultadoLiqLogOS_UserCod[0]);
               }
               if ( Z1371ContagemResultadoLiqLogOS_OSCod != T00322_A1371ContagemResultadoLiqLogOS_OSCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoliqlog:[seudo value changed for attri]"+"ContagemResultadoLiqLogOS_OSCod");
                  GXUtil.WriteLogRaw("Old: ",Z1371ContagemResultadoLiqLogOS_OSCod);
                  GXUtil.WriteLogRaw("Current: ",T00322_A1371ContagemResultadoLiqLogOS_OSCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoLiqLogOS"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert32164( )
      {
         BeforeValidate32164( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable32164( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM32164( 0) ;
            CheckOptimisticConcurrency32164( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm32164( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert32164( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003230 */
                     pr_default.execute(24, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo, A1375ContagemResultadoLiqLogOS_Valor, A1372ContagemResultadoLiqLogOS_UserCod, A1371ContagemResultadoLiqLogOS_OSCod});
                     pr_default.close(24);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLogOS") ;
                     if ( (pr_default.getStatus(24) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load32164( ) ;
            }
            EndLevel32164( ) ;
         }
         CloseExtendedTableCursors32164( ) ;
      }

      protected void Update32164( )
      {
         BeforeValidate32164( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable32164( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency32164( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm32164( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate32164( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003231 */
                     pr_default.execute(25, new Object[] {A1375ContagemResultadoLiqLogOS_Valor, A1372ContagemResultadoLiqLogOS_UserCod, A1371ContagemResultadoLiqLogOS_OSCod, A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo});
                     pr_default.close(25);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLogOS") ;
                     if ( (pr_default.getStatus(25) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoLiqLogOS"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate32164( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey32164( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel32164( ) ;
         }
         CloseExtendedTableCursors32164( ) ;
      }

      protected void DeferredUpdate32164( )
      {
      }

      protected void Delete32164( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate32164( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency32164( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls32164( ) ;
            AfterConfirm32164( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete32164( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003232 */
                  pr_default.execute(26, new Object[] {A1033ContagemResultadoLiqLog_Codigo, A1370ContagemResultadoLiqLogOS_Codigo});
                  pr_default.close(26);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoLiqLogOS") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode164 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel32164( ) ;
         Gx_mode = sMode164;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls32164( )
      {
         standaloneModal32164( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A1369ContagemResultadoLiqLog_Linhas = (int)(O1369ContagemResultadoLiqLog_Linhas+1);
               n1369ContagemResultadoLiqLog_Linhas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A1369ContagemResultadoLiqLog_Linhas = O1369ContagemResultadoLiqLog_Linhas;
                  n1369ContagemResultadoLiqLog_Linhas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A1369ContagemResultadoLiqLog_Linhas = (int)(O1369ContagemResultadoLiqLog_Linhas-1);
                     n1369ContagemResultadoLiqLog_Linhas = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
                  }
               }
            }
            /* Using cursor T003233 */
            pr_default.execute(27, new Object[] {A1372ContagemResultadoLiqLogOS_UserCod});
            A1374ContagemResultadoLiqLogOS_UserPesCod = T003233_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
            n1374ContagemResultadoLiqLogOS_UserPesCod = T003233_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
            pr_default.close(27);
            /* Using cursor T003234 */
            pr_default.execute(28, new Object[] {n1374ContagemResultadoLiqLogOS_UserPesCod, A1374ContagemResultadoLiqLogOS_UserPesCod});
            A1373ContagemResultadoLiqLogOS_UserNom = T003234_A1373ContagemResultadoLiqLogOS_UserNom[0];
            n1373ContagemResultadoLiqLogOS_UserNom = T003234_n1373ContagemResultadoLiqLogOS_UserNom[0];
            pr_default.close(28);
         }
      }

      protected void EndLevel32164( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart32164( )
      {
         /* Scan By routine */
         /* Using cursor T003235 */
         pr_default.execute(29, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         RcdFound164 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound164 = 1;
            A1370ContagemResultadoLiqLogOS_Codigo = T003235_A1370ContagemResultadoLiqLogOS_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext32164( )
      {
         /* Scan next routine */
         pr_default.readNext(29);
         RcdFound164 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound164 = 1;
            A1370ContagemResultadoLiqLogOS_Codigo = T003235_A1370ContagemResultadoLiqLogOS_Codigo[0];
         }
      }

      protected void ScanEnd32164( )
      {
         pr_default.close(29);
      }

      protected void AfterConfirm32164( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert32164( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate32164( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete32164( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete32164( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate32164( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes32164( )
      {
         edtContagemResultadoLiqLogOS_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0)));
         edtContagemResultadoLiqLogOS_OSCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_OSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_OSCod_Enabled), 5, 0)));
         edtContagemResultadoLiqLogOS_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_UserCod_Enabled), 5, 0)));
         edtContagemResultadoLiqLogOS_UserPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_UserPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_UserPesCod_Enabled), 5, 0)));
         edtContagemResultadoLiqLogOS_UserNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_UserNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_UserNom_Enabled), 5, 0)));
         edtContagemResultadoLiqLogOS_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_Valor_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_66164( )
      {
         edtContagemResultadoLiqLogOS_Codigo_Internalname = "CONTAGEMRESULTADOLIQLOGOS_CODIGO_"+sGXsfl_66_idx;
         edtContagemResultadoLiqLogOS_OSCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_OSCOD_"+sGXsfl_66_idx;
         edtContagemResultadoLiqLogOS_UserCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERCOD_"+sGXsfl_66_idx;
         edtContagemResultadoLiqLogOS_UserPesCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD_"+sGXsfl_66_idx;
         edtContagemResultadoLiqLogOS_UserNom_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERNOM_"+sGXsfl_66_idx;
         edtContagemResultadoLiqLogOS_Valor_Internalname = "CONTAGEMRESULTADOLIQLOGOS_VALOR_"+sGXsfl_66_idx;
      }

      protected void SubsflControlProps_fel_66164( )
      {
         edtContagemResultadoLiqLogOS_Codigo_Internalname = "CONTAGEMRESULTADOLIQLOGOS_CODIGO_"+sGXsfl_66_fel_idx;
         edtContagemResultadoLiqLogOS_OSCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_OSCOD_"+sGXsfl_66_fel_idx;
         edtContagemResultadoLiqLogOS_UserCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERCOD_"+sGXsfl_66_fel_idx;
         edtContagemResultadoLiqLogOS_UserPesCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD_"+sGXsfl_66_fel_idx;
         edtContagemResultadoLiqLogOS_UserNom_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERNOM_"+sGXsfl_66_fel_idx;
         edtContagemResultadoLiqLogOS_Valor_Internalname = "CONTAGEMRESULTADOLIQLOGOS_VALOR_"+sGXsfl_66_fel_idx;
      }

      protected void AddRow32164( )
      {
         nGXsfl_66_idx = (short)(nGXsfl_66_idx+1);
         sGXsfl_66_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_66_idx), 4, 0)), 4, "0");
         SubsflControlProps_66164( ) ;
         SendRow32164( ) ;
      }

      protected void SendRow32164( )
      {
         Gridcontagemresultadoliqlog_osRow = GXWebRow.GetNew(context);
         if ( subGridcontagemresultadoliqlog_os_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridcontagemresultadoliqlog_os_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridcontagemresultadoliqlog_os_Class, "") != 0 )
            {
               subGridcontagemresultadoliqlog_os_Linesclass = subGridcontagemresultadoliqlog_os_Class+"Odd";
            }
         }
         else if ( subGridcontagemresultadoliqlog_os_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridcontagemresultadoliqlog_os_Backstyle = 0;
            subGridcontagemresultadoliqlog_os_Backcolor = subGridcontagemresultadoliqlog_os_Allbackcolor;
            if ( StringUtil.StrCmp(subGridcontagemresultadoliqlog_os_Class, "") != 0 )
            {
               subGridcontagemresultadoliqlog_os_Linesclass = subGridcontagemresultadoliqlog_os_Class+"Uniform";
            }
         }
         else if ( subGridcontagemresultadoliqlog_os_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridcontagemresultadoliqlog_os_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridcontagemresultadoliqlog_os_Class, "") != 0 )
            {
               subGridcontagemresultadoliqlog_os_Linesclass = subGridcontagemresultadoliqlog_os_Class+"Odd";
            }
            subGridcontagemresultadoliqlog_os_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGridcontagemresultadoliqlog_os_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridcontagemresultadoliqlog_os_Backstyle = 1;
            if ( ((int)((nGXsfl_66_idx) % (2))) == 0 )
            {
               subGridcontagemresultadoliqlog_os_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridcontagemresultadoliqlog_os_Class, "") != 0 )
               {
                  subGridcontagemresultadoliqlog_os_Linesclass = subGridcontagemresultadoliqlog_os_Class+"Even";
               }
            }
            else
            {
               subGridcontagemresultadoliqlog_os_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGridcontagemresultadoliqlog_os_Class, "") != 0 )
               {
                  subGridcontagemresultadoliqlog_os_Linesclass = subGridcontagemresultadoliqlog_os_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_164_" + sGXsfl_66_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_66_idx + "',66)\"";
         ROClassString = "Attribute";
         Gridcontagemresultadoliqlog_osRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoLiqLogOS_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1370ContagemResultadoLiqLogOS_Codigo), 6, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A1370ContagemResultadoLiqLogOS_Codigo), "ZZZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,67);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoLiqLogOS_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoLiqLogOS_Codigo_Enabled,(short)1,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)66,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_164_" + sGXsfl_66_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_66_idx + "',66)\"";
         ROClassString = "Attribute";
         Gridcontagemresultadoliqlog_osRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoLiqLogOS_OSCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ",", "")),((edtContagemResultadoLiqLogOS_OSCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), "ZZZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoLiqLogOS_OSCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoLiqLogOS_OSCod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)66,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_164_" + sGXsfl_66_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_66_idx + "',66)\"";
         ROClassString = "Attribute";
         Gridcontagemresultadoliqlog_osRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoLiqLogOS_UserCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ",", "")),((edtContagemResultadoLiqLogOS_UserCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), "ZZZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoLiqLogOS_UserCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoLiqLogOS_UserCod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)66,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridcontagemresultadoliqlog_osRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoLiqLogOS_UserPesCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), 6, 0, ",", "")),((edtContagemResultadoLiqLogOS_UserPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoLiqLogOS_UserPesCod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoLiqLogOS_UserPesCod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)66,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute100";
         Gridcontagemresultadoliqlog_osRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoLiqLogOS_UserNom_Internalname,StringUtil.RTrim( A1373ContagemResultadoLiqLogOS_UserNom),StringUtil.RTrim( context.localUtil.Format( A1373ContagemResultadoLiqLogOS_UserNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoLiqLogOS_UserNom_Jsonclick,(short)0,(String)"BootstrapAttribute100",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoLiqLogOS_UserNom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)66,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_164_" + sGXsfl_66_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_66_idx + "',66)\"";
         ROClassString = "Attribute";
         Gridcontagemresultadoliqlog_osRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoLiqLogOS_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A1375ContagemResultadoLiqLogOS_Valor, 18, 5, ",", "")),((edtContagemResultadoLiqLogOS_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1375ContagemResultadoLiqLogOS_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1375ContagemResultadoLiqLogOS_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,72);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoLiqLogOS_Valor_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContagemResultadoLiqLogOS_Valor_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)66,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(Gridcontagemresultadoliqlog_osRow);
         GXCCtl = "Z1370ContagemResultadoLiqLogOS_Codigo_" + sGXsfl_66_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1370ContagemResultadoLiqLogOS_Codigo), 6, 0, ",", "")));
         GXCCtl = "Z1375ContagemResultadoLiqLogOS_Valor_" + sGXsfl_66_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z1375ContagemResultadoLiqLogOS_Valor, 18, 5, ",", "")));
         GXCCtl = "Z1372ContagemResultadoLiqLogOS_UserCod_" + sGXsfl_66_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ",", "")));
         GXCCtl = "Z1371ContagemResultadoLiqLogOS_OSCod_" + sGXsfl_66_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ",", "")));
         GXCCtl = "nRcdDeleted_164_" + sGXsfl_66_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_164), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_164_" + sGXsfl_66_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_164), 4, 0, ",", "")));
         GXCCtl = "nIsMod_164_" + sGXsfl_66_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_164), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_CODIGO_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_OSCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_OSCod_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_USERCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserCod_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserPesCod_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_USERNOM_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_UserNom_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_VALOR_"+sGXsfl_66_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLogOS_Valor_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridcontagemresultadoliqlog_osContainer.AddRow(Gridcontagemresultadoliqlog_osRow);
      }

      protected void ReadRow32164( )
      {
         nGXsfl_66_idx = (short)(nGXsfl_66_idx+1);
         sGXsfl_66_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_66_idx), 4, 0)), 4, "0");
         SubsflControlProps_66164( ) ;
         edtContagemResultadoLiqLogOS_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_CODIGO_"+sGXsfl_66_idx+"Enabled"), ",", "."));
         edtContagemResultadoLiqLogOS_OSCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_OSCOD_"+sGXsfl_66_idx+"Enabled"), ",", "."));
         edtContagemResultadoLiqLogOS_UserCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_USERCOD_"+sGXsfl_66_idx+"Enabled"), ",", "."));
         edtContagemResultadoLiqLogOS_UserPesCod_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD_"+sGXsfl_66_idx+"Enabled"), ",", "."));
         edtContagemResultadoLiqLogOS_UserNom_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_USERNOM_"+sGXsfl_66_idx+"Enabled"), ",", "."));
         edtContagemResultadoLiqLogOS_Valor_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOLIQLOGOS_VALOR_"+sGXsfl_66_idx+"Enabled"), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_CODIGO_" + sGXsfl_66_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_Codigo_Internalname;
            wbErr = true;
            A1370ContagemResultadoLiqLogOS_Codigo = 0;
         }
         else
         {
            A1370ContagemResultadoLiqLogOS_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_Codigo_Internalname), ",", "."));
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_OSCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_OSCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_OSCOD_" + sGXsfl_66_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_OSCod_Internalname;
            wbErr = true;
            A1371ContagemResultadoLiqLogOS_OSCod = 0;
         }
         else
         {
            A1371ContagemResultadoLiqLogOS_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_OSCod_Internalname), ",", "."));
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_UserCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_UserCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_USERCOD_" + sGXsfl_66_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_UserCod_Internalname;
            wbErr = true;
            A1372ContagemResultadoLiqLogOS_UserCod = 0;
         }
         else
         {
            A1372ContagemResultadoLiqLogOS_UserCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_UserCod_Internalname), ",", "."));
         }
         A1374ContagemResultadoLiqLogOS_UserPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_UserPesCod_Internalname), ",", "."));
         n1374ContagemResultadoLiqLogOS_UserPesCod = false;
         A1373ContagemResultadoLiqLogOS_UserNom = StringUtil.Upper( cgiGet( edtContagemResultadoLiqLogOS_UserNom_Internalname));
         n1373ContagemResultadoLiqLogOS_UserNom = false;
         if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
         {
            GXCCtl = "CONTAGEMRESULTADOLIQLOGOS_VALOR_" + sGXsfl_66_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_Valor_Internalname;
            wbErr = true;
            A1375ContagemResultadoLiqLogOS_Valor = 0;
         }
         else
         {
            A1375ContagemResultadoLiqLogOS_Valor = context.localUtil.CToN( cgiGet( edtContagemResultadoLiqLogOS_Valor_Internalname), ",", ".");
         }
         GXCCtl = "Z1370ContagemResultadoLiqLogOS_Codigo_" + sGXsfl_66_idx;
         Z1370ContagemResultadoLiqLogOS_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1375ContagemResultadoLiqLogOS_Valor_" + sGXsfl_66_idx;
         Z1375ContagemResultadoLiqLogOS_Valor = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z1372ContagemResultadoLiqLogOS_UserCod_" + sGXsfl_66_idx;
         Z1372ContagemResultadoLiqLogOS_UserCod = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1371ContagemResultadoLiqLogOS_OSCod_" + sGXsfl_66_idx;
         Z1371ContagemResultadoLiqLogOS_OSCod = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_164_" + sGXsfl_66_idx;
         nRcdDeleted_164 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_164_" + sGXsfl_66_idx;
         nRcdExists_164 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_164_" + sGXsfl_66_idx;
         nIsMod_164 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtContagemResultadoLiqLogOS_Codigo_Enabled = edtContagemResultadoLiqLogOS_Codigo_Enabled;
      }

      protected void ConfirmValues320( )
      {
         nGXsfl_66_idx = 0;
         sGXsfl_66_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_66_idx), 4, 0)), 4, "0");
         SubsflControlProps_66164( ) ;
         while ( nGXsfl_66_idx < nRC_GXsfl_66 )
         {
            nGXsfl_66_idx = (short)(nGXsfl_66_idx+1);
            sGXsfl_66_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_66_idx), 4, 0)), 4, "0");
            SubsflControlProps_66164( ) ;
            ChangePostValue( "Z1370ContagemResultadoLiqLogOS_Codigo_"+sGXsfl_66_idx, cgiGet( "ZT_"+"Z1370ContagemResultadoLiqLogOS_Codigo_"+sGXsfl_66_idx)) ;
            DeletePostValue( "ZT_"+"Z1370ContagemResultadoLiqLogOS_Codigo_"+sGXsfl_66_idx) ;
            ChangePostValue( "Z1375ContagemResultadoLiqLogOS_Valor_"+sGXsfl_66_idx, cgiGet( "ZT_"+"Z1375ContagemResultadoLiqLogOS_Valor_"+sGXsfl_66_idx)) ;
            DeletePostValue( "ZT_"+"Z1375ContagemResultadoLiqLogOS_Valor_"+sGXsfl_66_idx) ;
            ChangePostValue( "Z1372ContagemResultadoLiqLogOS_UserCod_"+sGXsfl_66_idx, cgiGet( "ZT_"+"Z1372ContagemResultadoLiqLogOS_UserCod_"+sGXsfl_66_idx)) ;
            DeletePostValue( "ZT_"+"Z1372ContagemResultadoLiqLogOS_UserCod_"+sGXsfl_66_idx) ;
            ChangePostValue( "Z1371ContagemResultadoLiqLogOS_OSCod_"+sGXsfl_66_idx, cgiGet( "ZT_"+"Z1371ContagemResultadoLiqLogOS_OSCod_"+sGXsfl_66_idx)) ;
            DeletePostValue( "ZT_"+"Z1371ContagemResultadoLiqLogOS_OSCod_"+sGXsfl_66_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221102727");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoliqlog.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1033ContagemResultadoLiqLog_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1034ContagemResultadoLiqLog_Data", context.localUtil.TToC( Z1034ContagemResultadoLiqLog_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1368ContagemResultadoLiqLog_Owner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1368ContagemResultadoLiqLog_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.NToC( (decimal)(O1369ContagemResultadoLiqLog_Linhas), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_66", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_66_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOG_OBSERVACAO", A1366ContagemResultadoLiqLog_Observacao);
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOG_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultadoliqlog_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoliqlog.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoLiqLog" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Liquida Log" ;
      }

      protected void InitializeNonKey32125( )
      {
         A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1034ContagemResultadoLiqLog_Data", context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 8, 5, 0, 3, "/", ":", " "));
         A1366ContagemResultadoLiqLog_Observacao = "";
         n1366ContagemResultadoLiqLog_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1366ContagemResultadoLiqLog_Observacao", A1366ContagemResultadoLiqLog_Observacao);
         A1368ContagemResultadoLiqLog_Owner = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1368ContagemResultadoLiqLog_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1368ContagemResultadoLiqLog_Owner), 6, 0)));
         A1369ContagemResultadoLiqLog_Linhas = 0;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         O1369ContagemResultadoLiqLog_Linhas = A1369ContagemResultadoLiqLog_Linhas;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
         Z1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         Z1368ContagemResultadoLiqLog_Owner = 0;
      }

      protected void InitAll32125( )
      {
         A1033ContagemResultadoLiqLog_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
         InitializeNonKey32125( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey32164( )
      {
         A1371ContagemResultadoLiqLogOS_OSCod = 0;
         A1372ContagemResultadoLiqLogOS_UserCod = 0;
         A1374ContagemResultadoLiqLogOS_UserPesCod = 0;
         n1374ContagemResultadoLiqLogOS_UserPesCod = false;
         A1373ContagemResultadoLiqLogOS_UserNom = "";
         n1373ContagemResultadoLiqLogOS_UserNom = false;
         A1375ContagemResultadoLiqLogOS_Valor = 0;
         Z1375ContagemResultadoLiqLogOS_Valor = 0;
         Z1372ContagemResultadoLiqLogOS_UserCod = 0;
         Z1371ContagemResultadoLiqLogOS_OSCod = 0;
      }

      protected void InitAll32164( )
      {
         A1370ContagemResultadoLiqLogOS_Codigo = 0;
         InitializeNonKey32164( ) ;
      }

      protected void StandaloneModalInsert32164( )
      {
         A1369ContagemResultadoLiqLog_Linhas = i1369ContagemResultadoLiqLog_Linhas;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1369ContagemResultadoLiqLog_Linhas", StringUtil.LTrim( StringUtil.Str( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221102738");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoliqlog.js", "?202031221102738");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties164( )
      {
         edtContagemResultadoLiqLogOS_Codigo_Enabled = defedtContagemResultadoLiqLogOS_Codigo_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLogOS_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLogOS_Codigo_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadoliqlog_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOLIQLOG_CODIGO";
         edtContagemResultadoLiqLog_Codigo_Internalname = "CONTAGEMRESULTADOLIQLOG_CODIGO";
         lblTextblockcontagemresultadoliqlog_data_Internalname = "TEXTBLOCKCONTAGEMRESULTADOLIQLOG_DATA";
         edtContagemResultadoLiqLog_Data_Internalname = "CONTAGEMRESULTADOLIQLOG_DATA";
         lblTextblockcontagemresultadoliqlog_observacao_Internalname = "TEXTBLOCKCONTAGEMRESULTADOLIQLOG_OBSERVACAO";
         Contagemresultadoliqlog_observacao_Internalname = "CONTAGEMRESULTADOLIQLOG_OBSERVACAO";
         lblTextblockcontagemresultadoliqlog_owner_Internalname = "TEXTBLOCKCONTAGEMRESULTADOLIQLOG_OWNER";
         edtContagemResultadoLiqLog_Owner_Internalname = "CONTAGEMRESULTADOLIQLOG_OWNER";
         lblTextblockcontagemresultadoliqlog_linhas_Internalname = "TEXTBLOCKCONTAGEMRESULTADOLIQLOG_LINHAS";
         edtContagemResultadoLiqLog_Linhas_Internalname = "CONTAGEMRESULTADOLIQLOG_LINHAS";
         lblTitleos_Internalname = "TITLEOS";
         tblTable3_Internalname = "TABLE3";
         edtContagemResultadoLiqLogOS_Codigo_Internalname = "CONTAGEMRESULTADOLIQLOGOS_CODIGO";
         edtContagemResultadoLiqLogOS_OSCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_OSCOD";
         edtContagemResultadoLiqLogOS_UserCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERCOD";
         edtContagemResultadoLiqLogOS_UserPesCod_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERPESCOD";
         edtContagemResultadoLiqLogOS_UserNom_Internalname = "CONTAGEMRESULTADOLIQLOGOS_USERNOM";
         edtContagemResultadoLiqLogOS_Valor_Internalname = "CONTAGEMRESULTADOLIQLOGOS_VALOR";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGridcontagemresultadoliqlog_os_Internalname = "GRIDCONTAGEMRESULTADOLIQLOG_OS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Liquida Log";
         edtContagemResultadoLiqLogOS_Valor_Jsonclick = "";
         edtContagemResultadoLiqLogOS_UserNom_Jsonclick = "";
         edtContagemResultadoLiqLogOS_UserPesCod_Jsonclick = "";
         edtContagemResultadoLiqLogOS_UserCod_Jsonclick = "";
         edtContagemResultadoLiqLogOS_OSCod_Jsonclick = "";
         edtContagemResultadoLiqLogOS_Codigo_Jsonclick = "";
         subGridcontagemresultadoliqlog_os_Class = "Grid";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         subGridcontagemresultadoliqlog_os_Allowcollapsing = 0;
         subGridcontagemresultadoliqlog_os_Allowselection = 0;
         edtContagemResultadoLiqLogOS_Valor_Enabled = 1;
         edtContagemResultadoLiqLogOS_UserNom_Enabled = 0;
         edtContagemResultadoLiqLogOS_UserPesCod_Enabled = 0;
         edtContagemResultadoLiqLogOS_UserCod_Enabled = 1;
         edtContagemResultadoLiqLogOS_OSCod_Enabled = 1;
         edtContagemResultadoLiqLogOS_Codigo_Enabled = 1;
         subGridcontagemresultadoliqlog_os_Backcolorstyle = 2;
         edtContagemResultadoLiqLog_Linhas_Jsonclick = "";
         edtContagemResultadoLiqLog_Linhas_Enabled = 0;
         edtContagemResultadoLiqLog_Owner_Jsonclick = "";
         edtContagemResultadoLiqLog_Owner_Enabled = 1;
         Contagemresultadoliqlog_observacao_Enabled = Convert.ToBoolean( 1);
         edtContagemResultadoLiqLog_Data_Jsonclick = "";
         edtContagemResultadoLiqLog_Data_Enabled = 1;
         edtContagemResultadoLiqLog_Codigo_Jsonclick = "";
         edtContagemResultadoLiqLog_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridcontagemresultadoliqlog_os_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_66164( ) ;
         while ( nGXsfl_66_idx <= nRC_GXsfl_66 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal32164( ) ;
            standaloneModal32164( ) ;
            dynload_actions( ) ;
            SendRow32164( ) ;
            nGXsfl_66_idx = (short)(nGXsfl_66_idx+1);
            sGXsfl_66_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_66_idx), 4, 0)), 4, "0");
            SubsflControlProps_66164( ) ;
         }
         context.GX_webresponse.AddString(Gridcontagemresultadoliqlog_osContainer.ToJavascriptSource());
         /* End function gxnrGridcontagemresultadoliqlog_os_newrow */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoLiqLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadoliqlog_codigo( int GX_Parm1 ,
                                                        DateTime GX_Parm2 ,
                                                        String GX_Parm3 ,
                                                        int GX_Parm4 ,
                                                        int GX_Parm5 )
      {
         A1033ContagemResultadoLiqLog_Codigo = GX_Parm1;
         A1034ContagemResultadoLiqLog_Data = GX_Parm2;
         A1366ContagemResultadoLiqLog_Observacao = GX_Parm3;
         n1366ContagemResultadoLiqLog_Observacao = false;
         A1368ContagemResultadoLiqLog_Owner = GX_Parm4;
         A1369ContagemResultadoLiqLog_Linhas = GX_Parm5;
         n1369ContagemResultadoLiqLog_Linhas = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T003222 */
         pr_default.execute(16, new Object[] {A1033ContagemResultadoLiqLog_Codigo});
         if ( (pr_default.getStatus(16) != 101) )
         {
            A1369ContagemResultadoLiqLog_Linhas = T003222_A1369ContagemResultadoLiqLog_Linhas[0];
            n1369ContagemResultadoLiqLog_Linhas = T003222_n1369ContagemResultadoLiqLog_Linhas[0];
         }
         else
         {
            A1369ContagemResultadoLiqLog_Linhas = 0;
            n1369ContagemResultadoLiqLog_Linhas = false;
         }
         pr_default.close(16);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1369ContagemResultadoLiqLog_Linhas = 0;
            n1369ContagemResultadoLiqLog_Linhas = false;
         }
         isValidOutput.Add(context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(A1366ContagemResultadoLiqLog_Observacao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1368ContagemResultadoLiqLog_Owner), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1369ContagemResultadoLiqLog_Linhas), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1033ContagemResultadoLiqLog_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1034ContagemResultadoLiqLog_Data, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(Z1366ContagemResultadoLiqLog_Observacao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1368ContagemResultadoLiqLog_Owner), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1369ContagemResultadoLiqLog_Linhas), 8, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(O1369ContagemResultadoLiqLog_Linhas), 8, 0, ",", "")));
         isValidOutput.Add(Gridcontagemresultadoliqlog_osContainer);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoliqlogos_oscod( int GX_Parm1 )
      {
         A1371ContagemResultadoLiqLogOS_OSCod = GX_Parm1;
         /* Using cursor T003236 */
         pr_default.execute(30, new Object[] {A1371ContagemResultadoLiqLogOS_OSCod});
         if ( (pr_default.getStatus(30) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Liq Log Os_Contagem Resultado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOLIQLOGOS_OSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_OSCod_Internalname;
         }
         pr_default.close(30);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoliqlogos_usercod( int GX_Parm1 ,
                                                           int GX_Parm2 ,
                                                           String GX_Parm3 )
      {
         A1372ContagemResultadoLiqLogOS_UserCod = GX_Parm1;
         A1374ContagemResultadoLiqLogOS_UserPesCod = GX_Parm2;
         n1374ContagemResultadoLiqLogOS_UserPesCod = false;
         A1373ContagemResultadoLiqLogOS_UserNom = GX_Parm3;
         n1373ContagemResultadoLiqLogOS_UserNom = false;
         /* Using cursor T003233 */
         pr_default.execute(27, new Object[] {A1372ContagemResultadoLiqLogOS_UserCod});
         if ( (pr_default.getStatus(27) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Liq Log_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOLIQLOGOS_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoLiqLogOS_UserCod_Internalname;
         }
         A1374ContagemResultadoLiqLogOS_UserPesCod = T003233_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
         n1374ContagemResultadoLiqLogOS_UserPesCod = T003233_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
         pr_default.close(27);
         /* Using cursor T003234 */
         pr_default.execute(28, new Object[] {n1374ContagemResultadoLiqLogOS_UserPesCod, A1374ContagemResultadoLiqLogOS_UserPesCod});
         if ( (pr_default.getStatus(28) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1373ContagemResultadoLiqLogOS_UserNom = T003234_A1373ContagemResultadoLiqLogOS_UserNom[0];
         n1373ContagemResultadoLiqLogOS_UserNom = T003234_n1373ContagemResultadoLiqLogOS_UserNom[0];
         pr_default.close(28);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1374ContagemResultadoLiqLogOS_UserPesCod = 0;
            n1374ContagemResultadoLiqLogOS_UserPesCod = false;
            A1373ContagemResultadoLiqLogOS_UserNom = "";
            n1373ContagemResultadoLiqLogOS_UserNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1374ContagemResultadoLiqLogOS_UserPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1373ContagemResultadoLiqLogOS_UserNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(27);
         pr_default.close(30);
         pr_default.close(28);
         pr_default.close(6);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoliqlog_codigo_Jsonclick = "";
         lblTextblockcontagemresultadoliqlog_data_Jsonclick = "";
         A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultadoliqlog_observacao_Jsonclick = "";
         lblTextblockcontagemresultadoliqlog_owner_Jsonclick = "";
         lblTextblockcontagemresultadoliqlog_linhas_Jsonclick = "";
         Gridcontagemresultadoliqlog_osContainer = new GXWebGrid( context);
         Gridcontagemresultadoliqlog_osColumn = new GXWebColumn();
         A1373ContagemResultadoLiqLogOS_UserNom = "";
         Gx_mode = "";
         sMode164 = "";
         lblTitleos_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         A1366ContagemResultadoLiqLog_Observacao = "";
         Contagemresultadoliqlog_observacao_Width = "";
         Contagemresultadoliqlog_observacao_Height = "";
         Contagemresultadoliqlog_observacao_Skin = "";
         Contagemresultadoliqlog_observacao_Toolbar = "";
         Contagemresultadoliqlog_observacao_Class = "";
         Contagemresultadoliqlog_observacao_Customtoolbar = "";
         Contagemresultadoliqlog_observacao_Customconfiguration = "";
         Contagemresultadoliqlog_observacao_Buttonpressedid = "";
         Contagemresultadoliqlog_observacao_Captionvalue = "";
         Contagemresultadoliqlog_observacao_Captionclass = "";
         Contagemresultadoliqlog_observacao_Captionposition = "";
         Contagemresultadoliqlog_observacao_Coltitle = "";
         Contagemresultadoliqlog_observacao_Coltitlefont = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Z1366ContagemResultadoLiqLog_Observacao = "";
         T003212_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T003212_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         T003212_A1366ContagemResultadoLiqLog_Observacao = new String[] {""} ;
         T003212_n1366ContagemResultadoLiqLog_Observacao = new bool[] {false} ;
         T003212_A1368ContagemResultadoLiqLog_Owner = new int[1] ;
         T003212_A1369ContagemResultadoLiqLog_Linhas = new int[1] ;
         T003212_n1369ContagemResultadoLiqLog_Linhas = new bool[] {false} ;
         T003210_A1369ContagemResultadoLiqLog_Linhas = new int[1] ;
         T003210_n1369ContagemResultadoLiqLog_Linhas = new bool[] {false} ;
         T003214_A1369ContagemResultadoLiqLog_Linhas = new int[1] ;
         T003214_n1369ContagemResultadoLiqLog_Linhas = new bool[] {false} ;
         T003215_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T00328_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T00328_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         T00328_A1366ContagemResultadoLiqLog_Observacao = new String[] {""} ;
         T00328_n1366ContagemResultadoLiqLog_Observacao = new bool[] {false} ;
         T00328_A1368ContagemResultadoLiqLog_Owner = new int[1] ;
         sMode125 = "";
         T003216_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T003217_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T00327_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T00327_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         T00327_A1366ContagemResultadoLiqLog_Observacao = new String[] {""} ;
         T00327_n1366ContagemResultadoLiqLog_Observacao = new bool[] {false} ;
         T00327_A1368ContagemResultadoLiqLog_Owner = new int[1] ;
         T003218_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T003222_A1369ContagemResultadoLiqLog_Linhas = new int[1] ;
         T003222_n1369ContagemResultadoLiqLog_Linhas = new bool[] {false} ;
         T003223_A456ContagemResultado_Codigo = new int[1] ;
         T003224_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         Z1373ContagemResultadoLiqLogOS_UserNom = "";
         T003225_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T003225_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         T003225_A1373ContagemResultadoLiqLogOS_UserNom = new String[] {""} ;
         T003225_n1373ContagemResultadoLiqLogOS_UserNom = new bool[] {false} ;
         T003225_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         T003225_A1372ContagemResultadoLiqLogOS_UserCod = new int[1] ;
         T003225_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         T003225_A1374ContagemResultadoLiqLogOS_UserPesCod = new int[1] ;
         T003225_n1374ContagemResultadoLiqLogOS_UserPesCod = new bool[] {false} ;
         T00325_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         T00324_A1374ContagemResultadoLiqLogOS_UserPesCod = new int[1] ;
         T00324_n1374ContagemResultadoLiqLogOS_UserPesCod = new bool[] {false} ;
         T00326_A1373ContagemResultadoLiqLogOS_UserNom = new String[] {""} ;
         T00326_n1373ContagemResultadoLiqLogOS_UserNom = new bool[] {false} ;
         T003226_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         T003227_A1374ContagemResultadoLiqLogOS_UserPesCod = new int[1] ;
         T003227_n1374ContagemResultadoLiqLogOS_UserPesCod = new bool[] {false} ;
         T003228_A1373ContagemResultadoLiqLogOS_UserNom = new String[] {""} ;
         T003228_n1373ContagemResultadoLiqLogOS_UserNom = new bool[] {false} ;
         T003229_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T003229_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         T00323_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T00323_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         T00323_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         T00323_A1372ContagemResultadoLiqLogOS_UserCod = new int[1] ;
         T00323_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         T00322_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T00322_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         T00322_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         T00322_A1372ContagemResultadoLiqLogOS_UserCod = new int[1] ;
         T00322_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         T003233_A1374ContagemResultadoLiqLogOS_UserPesCod = new int[1] ;
         T003233_n1374ContagemResultadoLiqLogOS_UserPesCod = new bool[] {false} ;
         T003234_A1373ContagemResultadoLiqLogOS_UserNom = new String[] {""} ;
         T003234_n1373ContagemResultadoLiqLogOS_UserNom = new bool[] {false} ;
         T003235_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T003235_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         Gridcontagemresultadoliqlog_osRow = new GXWebRow();
         subGridcontagemresultadoliqlog_os_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T003236_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoliqlog__default(),
            new Object[][] {
                new Object[] {
               T00322_A1033ContagemResultadoLiqLog_Codigo, T00322_A1370ContagemResultadoLiqLogOS_Codigo, T00322_A1375ContagemResultadoLiqLogOS_Valor, T00322_A1372ContagemResultadoLiqLogOS_UserCod, T00322_A1371ContagemResultadoLiqLogOS_OSCod
               }
               , new Object[] {
               T00323_A1033ContagemResultadoLiqLog_Codigo, T00323_A1370ContagemResultadoLiqLogOS_Codigo, T00323_A1375ContagemResultadoLiqLogOS_Valor, T00323_A1372ContagemResultadoLiqLogOS_UserCod, T00323_A1371ContagemResultadoLiqLogOS_OSCod
               }
               , new Object[] {
               T00324_A1374ContagemResultadoLiqLogOS_UserPesCod, T00324_n1374ContagemResultadoLiqLogOS_UserPesCod
               }
               , new Object[] {
               T00325_A1371ContagemResultadoLiqLogOS_OSCod
               }
               , new Object[] {
               T00326_A1373ContagemResultadoLiqLogOS_UserNom, T00326_n1373ContagemResultadoLiqLogOS_UserNom
               }
               , new Object[] {
               T00327_A1033ContagemResultadoLiqLog_Codigo, T00327_A1034ContagemResultadoLiqLog_Data, T00327_A1366ContagemResultadoLiqLog_Observacao, T00327_n1366ContagemResultadoLiqLog_Observacao, T00327_A1368ContagemResultadoLiqLog_Owner
               }
               , new Object[] {
               T00328_A1033ContagemResultadoLiqLog_Codigo, T00328_A1034ContagemResultadoLiqLog_Data, T00328_A1366ContagemResultadoLiqLog_Observacao, T00328_n1366ContagemResultadoLiqLog_Observacao, T00328_A1368ContagemResultadoLiqLog_Owner
               }
               , new Object[] {
               T003210_A1369ContagemResultadoLiqLog_Linhas, T003210_n1369ContagemResultadoLiqLog_Linhas
               }
               , new Object[] {
               T003212_A1033ContagemResultadoLiqLog_Codigo, T003212_A1034ContagemResultadoLiqLog_Data, T003212_A1366ContagemResultadoLiqLog_Observacao, T003212_n1366ContagemResultadoLiqLog_Observacao, T003212_A1368ContagemResultadoLiqLog_Owner, T003212_A1369ContagemResultadoLiqLog_Linhas, T003212_n1369ContagemResultadoLiqLog_Linhas
               }
               , new Object[] {
               T003214_A1369ContagemResultadoLiqLog_Linhas, T003214_n1369ContagemResultadoLiqLog_Linhas
               }
               , new Object[] {
               T003215_A1033ContagemResultadoLiqLog_Codigo
               }
               , new Object[] {
               T003216_A1033ContagemResultadoLiqLog_Codigo
               }
               , new Object[] {
               T003217_A1033ContagemResultadoLiqLog_Codigo
               }
               , new Object[] {
               T003218_A1033ContagemResultadoLiqLog_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003222_A1369ContagemResultadoLiqLog_Linhas, T003222_n1369ContagemResultadoLiqLog_Linhas
               }
               , new Object[] {
               T003223_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T003224_A1033ContagemResultadoLiqLog_Codigo
               }
               , new Object[] {
               T003225_A1033ContagemResultadoLiqLog_Codigo, T003225_A1370ContagemResultadoLiqLogOS_Codigo, T003225_A1373ContagemResultadoLiqLogOS_UserNom, T003225_n1373ContagemResultadoLiqLogOS_UserNom, T003225_A1375ContagemResultadoLiqLogOS_Valor, T003225_A1372ContagemResultadoLiqLogOS_UserCod, T003225_A1371ContagemResultadoLiqLogOS_OSCod, T003225_A1374ContagemResultadoLiqLogOS_UserPesCod, T003225_n1374ContagemResultadoLiqLogOS_UserPesCod
               }
               , new Object[] {
               T003226_A1371ContagemResultadoLiqLogOS_OSCod
               }
               , new Object[] {
               T003227_A1374ContagemResultadoLiqLogOS_UserPesCod, T003227_n1374ContagemResultadoLiqLogOS_UserPesCod
               }
               , new Object[] {
               T003228_A1373ContagemResultadoLiqLogOS_UserNom, T003228_n1373ContagemResultadoLiqLogOS_UserNom
               }
               , new Object[] {
               T003229_A1033ContagemResultadoLiqLog_Codigo, T003229_A1370ContagemResultadoLiqLogOS_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003233_A1374ContagemResultadoLiqLogOS_UserPesCod, T003233_n1374ContagemResultadoLiqLogOS_UserPesCod
               }
               , new Object[] {
               T003234_A1373ContagemResultadoLiqLogOS_UserNom, T003234_n1373ContagemResultadoLiqLogOS_UserNom
               }
               , new Object[] {
               T003235_A1033ContagemResultadoLiqLog_Codigo, T003235_A1370ContagemResultadoLiqLogOS_Codigo
               }
               , new Object[] {
               T003236_A1371ContagemResultadoLiqLogOS_OSCod
               }
            }
         );
      }

      private short nRC_GXsfl_66 ;
      private short nGXsfl_66_idx=1 ;
      private short nRcdDeleted_164 ;
      private short nRcdExists_164 ;
      private short nIsMod_164 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridcontagemresultadoliqlog_os_Backcolorstyle ;
      private short subGridcontagemresultadoliqlog_os_Allowselection ;
      private short subGridcontagemresultadoliqlog_os_Allowhovering ;
      private short subGridcontagemresultadoliqlog_os_Allowcollapsing ;
      private short subGridcontagemresultadoliqlog_os_Collapsed ;
      private short nBlankRcdCount164 ;
      private short RcdFound164 ;
      private short nBlankRcdUsr164 ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound125 ;
      private short subGridcontagemresultadoliqlog_os_Backstyle ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1033ContagemResultadoLiqLog_Codigo ;
      private int Z1368ContagemResultadoLiqLog_Owner ;
      private int O1369ContagemResultadoLiqLog_Linhas ;
      private int Z1370ContagemResultadoLiqLogOS_Codigo ;
      private int Z1372ContagemResultadoLiqLogOS_UserCod ;
      private int Z1371ContagemResultadoLiqLogOS_OSCod ;
      private int A1033ContagemResultadoLiqLog_Codigo ;
      private int A1371ContagemResultadoLiqLogOS_OSCod ;
      private int A1372ContagemResultadoLiqLogOS_UserCod ;
      private int A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContagemResultadoLiqLog_Codigo_Enabled ;
      private int edtContagemResultadoLiqLog_Data_Enabled ;
      private int A1368ContagemResultadoLiqLog_Owner ;
      private int edtContagemResultadoLiqLog_Owner_Enabled ;
      private int A1369ContagemResultadoLiqLog_Linhas ;
      private int edtContagemResultadoLiqLog_Linhas_Enabled ;
      private int A1370ContagemResultadoLiqLogOS_Codigo ;
      private int edtContagemResultadoLiqLogOS_Codigo_Enabled ;
      private int edtContagemResultadoLiqLogOS_OSCod_Enabled ;
      private int edtContagemResultadoLiqLogOS_UserCod_Enabled ;
      private int edtContagemResultadoLiqLogOS_UserPesCod_Enabled ;
      private int edtContagemResultadoLiqLogOS_UserNom_Enabled ;
      private int edtContagemResultadoLiqLogOS_Valor_Enabled ;
      private int subGridcontagemresultadoliqlog_os_Selectioncolor ;
      private int subGridcontagemresultadoliqlog_os_Hoveringcolor ;
      private int B1369ContagemResultadoLiqLog_Linhas ;
      private int fRowAdded ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Contagemresultadoliqlog_observacao_Color ;
      private int Contagemresultadoliqlog_observacao_Coltitlecolor ;
      private int s1369ContagemResultadoLiqLog_Linhas ;
      private int Z1369ContagemResultadoLiqLog_Linhas ;
      private int Z1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int subGridcontagemresultadoliqlog_os_Backcolor ;
      private int subGridcontagemresultadoliqlog_os_Allbackcolor ;
      private int defedtContagemResultadoLiqLogOS_Codigo_Enabled ;
      private int i1369ContagemResultadoLiqLog_Linhas ;
      private int idxLst ;
      private long GRIDCONTAGEMRESULTADOLIQLOG_OS_nFirstRecordOnPage ;
      private decimal Z1375ContagemResultadoLiqLogOS_Valor ;
      private decimal A1375ContagemResultadoLiqLogOS_Valor ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_66_idx="0001" ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoLiqLog_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadoliqlog_codigo_Internalname ;
      private String lblTextblockcontagemresultadoliqlog_codigo_Jsonclick ;
      private String edtContagemResultadoLiqLog_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadoliqlog_data_Internalname ;
      private String lblTextblockcontagemresultadoliqlog_data_Jsonclick ;
      private String edtContagemResultadoLiqLog_Data_Internalname ;
      private String edtContagemResultadoLiqLog_Data_Jsonclick ;
      private String lblTextblockcontagemresultadoliqlog_observacao_Internalname ;
      private String lblTextblockcontagemresultadoliqlog_observacao_Jsonclick ;
      private String lblTextblockcontagemresultadoliqlog_owner_Internalname ;
      private String lblTextblockcontagemresultadoliqlog_owner_Jsonclick ;
      private String edtContagemResultadoLiqLog_Owner_Internalname ;
      private String edtContagemResultadoLiqLog_Owner_Jsonclick ;
      private String lblTextblockcontagemresultadoliqlog_linhas_Internalname ;
      private String lblTextblockcontagemresultadoliqlog_linhas_Jsonclick ;
      private String edtContagemResultadoLiqLog_Linhas_Internalname ;
      private String edtContagemResultadoLiqLog_Linhas_Jsonclick ;
      private String A1373ContagemResultadoLiqLogOS_UserNom ;
      private String Gx_mode ;
      private String sMode164 ;
      private String edtContagemResultadoLiqLogOS_Codigo_Internalname ;
      private String edtContagemResultadoLiqLogOS_OSCod_Internalname ;
      private String edtContagemResultadoLiqLogOS_UserCod_Internalname ;
      private String edtContagemResultadoLiqLogOS_UserPesCod_Internalname ;
      private String edtContagemResultadoLiqLogOS_UserNom_Internalname ;
      private String edtContagemResultadoLiqLogOS_Valor_Internalname ;
      private String tblTable3_Internalname ;
      private String lblTitleos_Internalname ;
      private String lblTitleos_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Contagemresultadoliqlog_observacao_Width ;
      private String Contagemresultadoliqlog_observacao_Height ;
      private String Contagemresultadoliqlog_observacao_Skin ;
      private String Contagemresultadoliqlog_observacao_Toolbar ;
      private String Contagemresultadoliqlog_observacao_Class ;
      private String Contagemresultadoliqlog_observacao_Customtoolbar ;
      private String Contagemresultadoliqlog_observacao_Customconfiguration ;
      private String Contagemresultadoliqlog_observacao_Buttonpressedid ;
      private String Contagemresultadoliqlog_observacao_Captionvalue ;
      private String Contagemresultadoliqlog_observacao_Captionclass ;
      private String Contagemresultadoliqlog_observacao_Captionposition ;
      private String Contagemresultadoliqlog_observacao_Coltitle ;
      private String Contagemresultadoliqlog_observacao_Coltitlefont ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String sMode125 ;
      private String Contagemresultadoliqlog_observacao_Internalname ;
      private String Z1373ContagemResultadoLiqLogOS_UserNom ;
      private String sGXsfl_66_fel_idx="0001" ;
      private String subGridcontagemresultadoliqlog_os_Class ;
      private String subGridcontagemresultadoliqlog_os_Linesclass ;
      private String ROClassString ;
      private String edtContagemResultadoLiqLogOS_Codigo_Jsonclick ;
      private String edtContagemResultadoLiqLogOS_OSCod_Jsonclick ;
      private String edtContagemResultadoLiqLogOS_UserCod_Jsonclick ;
      private String edtContagemResultadoLiqLogOS_UserPesCod_Jsonclick ;
      private String edtContagemResultadoLiqLogOS_UserNom_Jsonclick ;
      private String edtContagemResultadoLiqLogOS_Valor_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridcontagemresultadoliqlog_os_Internalname ;
      private DateTime Z1034ContagemResultadoLiqLog_Data ;
      private DateTime A1034ContagemResultadoLiqLog_Data ;
      private bool entryPointCalled ;
      private bool n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1369ContagemResultadoLiqLog_Linhas ;
      private bool n1366ContagemResultadoLiqLog_Observacao ;
      private bool Contagemresultadoliqlog_observacao_Enabled ;
      private bool Contagemresultadoliqlog_observacao_Toolbarcancollapse ;
      private bool Contagemresultadoliqlog_observacao_Toolbarexpanded ;
      private bool Contagemresultadoliqlog_observacao_Usercontroliscolumn ;
      private bool Contagemresultadoliqlog_observacao_Visible ;
      private bool n1373ContagemResultadoLiqLogOS_UserNom ;
      private String A1366ContagemResultadoLiqLog_Observacao ;
      private String Z1366ContagemResultadoLiqLog_Observacao ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Gridcontagemresultadoliqlog_osContainer ;
      private GXWebRow Gridcontagemresultadoliqlog_osRow ;
      private GXWebColumn Gridcontagemresultadoliqlog_osColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T003212_A1033ContagemResultadoLiqLog_Codigo ;
      private DateTime[] T003212_A1034ContagemResultadoLiqLog_Data ;
      private String[] T003212_A1366ContagemResultadoLiqLog_Observacao ;
      private bool[] T003212_n1366ContagemResultadoLiqLog_Observacao ;
      private int[] T003212_A1368ContagemResultadoLiqLog_Owner ;
      private int[] T003212_A1369ContagemResultadoLiqLog_Linhas ;
      private bool[] T003212_n1369ContagemResultadoLiqLog_Linhas ;
      private int[] T003210_A1369ContagemResultadoLiqLog_Linhas ;
      private bool[] T003210_n1369ContagemResultadoLiqLog_Linhas ;
      private int[] T003214_A1369ContagemResultadoLiqLog_Linhas ;
      private bool[] T003214_n1369ContagemResultadoLiqLog_Linhas ;
      private int[] T003215_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T00328_A1033ContagemResultadoLiqLog_Codigo ;
      private DateTime[] T00328_A1034ContagemResultadoLiqLog_Data ;
      private String[] T00328_A1366ContagemResultadoLiqLog_Observacao ;
      private bool[] T00328_n1366ContagemResultadoLiqLog_Observacao ;
      private int[] T00328_A1368ContagemResultadoLiqLog_Owner ;
      private int[] T003216_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T003217_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T00327_A1033ContagemResultadoLiqLog_Codigo ;
      private DateTime[] T00327_A1034ContagemResultadoLiqLog_Data ;
      private String[] T00327_A1366ContagemResultadoLiqLog_Observacao ;
      private bool[] T00327_n1366ContagemResultadoLiqLog_Observacao ;
      private int[] T00327_A1368ContagemResultadoLiqLog_Owner ;
      private int[] T003218_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T003222_A1369ContagemResultadoLiqLog_Linhas ;
      private bool[] T003222_n1369ContagemResultadoLiqLog_Linhas ;
      private int[] T003223_A456ContagemResultado_Codigo ;
      private int[] T003224_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T003225_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T003225_A1370ContagemResultadoLiqLogOS_Codigo ;
      private String[] T003225_A1373ContagemResultadoLiqLogOS_UserNom ;
      private bool[] T003225_n1373ContagemResultadoLiqLogOS_UserNom ;
      private decimal[] T003225_A1375ContagemResultadoLiqLogOS_Valor ;
      private int[] T003225_A1372ContagemResultadoLiqLogOS_UserCod ;
      private int[] T003225_A1371ContagemResultadoLiqLogOS_OSCod ;
      private int[] T003225_A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool[] T003225_n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int[] T00325_A1371ContagemResultadoLiqLogOS_OSCod ;
      private int[] T00324_A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool[] T00324_n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private String[] T00326_A1373ContagemResultadoLiqLogOS_UserNom ;
      private bool[] T00326_n1373ContagemResultadoLiqLogOS_UserNom ;
      private int[] T003226_A1371ContagemResultadoLiqLogOS_OSCod ;
      private int[] T003227_A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool[] T003227_n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private String[] T003228_A1373ContagemResultadoLiqLogOS_UserNom ;
      private bool[] T003228_n1373ContagemResultadoLiqLogOS_UserNom ;
      private int[] T003229_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T003229_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] T00323_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T00323_A1370ContagemResultadoLiqLogOS_Codigo ;
      private decimal[] T00323_A1375ContagemResultadoLiqLogOS_Valor ;
      private int[] T00323_A1372ContagemResultadoLiqLogOS_UserCod ;
      private int[] T00323_A1371ContagemResultadoLiqLogOS_OSCod ;
      private int[] T00322_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T00322_A1370ContagemResultadoLiqLogOS_Codigo ;
      private decimal[] T00322_A1375ContagemResultadoLiqLogOS_Valor ;
      private int[] T00322_A1372ContagemResultadoLiqLogOS_UserCod ;
      private int[] T00322_A1371ContagemResultadoLiqLogOS_OSCod ;
      private int[] T003233_A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool[] T003233_n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private String[] T003234_A1373ContagemResultadoLiqLogOS_UserNom ;
      private bool[] T003234_n1373ContagemResultadoLiqLogOS_UserNom ;
      private int[] T003235_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T003235_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] T003236_A1371ContagemResultadoLiqLogOS_OSCod ;
      private GXWebForm Form ;
   }

   public class contagemresultadoliqlog__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003212 ;
          prmT003212 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003210 ;
          prmT003210 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003214 ;
          prmT003214 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003215 ;
          prmT003215 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00328 ;
          prmT00328 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003216 ;
          prmT003216 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003217 ;
          prmT003217 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00327 ;
          prmT00327 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003218 ;
          prmT003218 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoLiqLog_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoLiqLog_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003219 ;
          prmT003219 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoLiqLog_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContagemResultadoLiqLog_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003220 ;
          prmT003220 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003223 ;
          prmT003223 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003224 ;
          prmT003224 = new Object[] {
          } ;
          Object[] prmT003225 ;
          prmT003225 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00325 ;
          prmT00325 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00324 ;
          prmT00324 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00326 ;
          prmT00326 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003226 ;
          prmT003226 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003227 ;
          prmT003227 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003228 ;
          prmT003228 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003229 ;
          prmT003229 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00323 ;
          prmT00323 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00322 ;
          prmT00322 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003230 ;
          prmT003230 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoLiqLogOS_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003231 ;
          prmT003231 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoLiqLogOS_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003232 ;
          prmT003232 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoLiqLogOS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003235 ;
          prmT003235 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003222 ;
          prmT003222 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003236 ;
          prmT003236 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003233 ;
          prmT003233 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003234 ;
          prmT003234 = new Object[] {
          new Object[] {"@ContagemResultadoLiqLogOS_UserPesCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00322", "SELECT [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo], [ContagemResultadoLiqLogOS_Valor], [ContagemResultadoLiqLogOS_UserCod] AS ContagemResultadoLiqLogOS_UserCod, [ContagemResultadoLiqLogOS_OSCod] AS ContagemResultadoLiqLogOS_OSCod FROM [ContagemResultadoLiqLogOS] WITH (UPDLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo AND [ContagemResultadoLiqLogOS_Codigo] = @ContagemResultadoLiqLogOS_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00322,1,0,true,false )
             ,new CursorDef("T00323", "SELECT [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo], [ContagemResultadoLiqLogOS_Valor], [ContagemResultadoLiqLogOS_UserCod] AS ContagemResultadoLiqLogOS_UserCod, [ContagemResultadoLiqLogOS_OSCod] AS ContagemResultadoLiqLogOS_OSCod FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo AND [ContagemResultadoLiqLogOS_Codigo] = @ContagemResultadoLiqLogOS_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00323,1,0,true,false )
             ,new CursorDef("T00324", "SELECT [Usuario_PessoaCod] AS ContagemResultadoLiqLogOS_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoLiqLogOS_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00324,1,0,true,false )
             ,new CursorDef("T00325", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoLiqLogOS_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoLiqLogOS_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00325,1,0,true,false )
             ,new CursorDef("T00326", "SELECT [Pessoa_Nome] AS ContagemResultadoLiqLogOS_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoLiqLogOS_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00326,1,0,true,false )
             ,new CursorDef("T00327", "SELECT [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLog_Data], [ContagemResultadoLiqLog_Observacao], [ContagemResultadoLiqLog_Owner] FROM [ContagemResultadoLiqLog] WITH (UPDLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00327,1,0,true,false )
             ,new CursorDef("T00328", "SELECT [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLog_Data], [ContagemResultadoLiqLog_Observacao], [ContagemResultadoLiqLog_Owner] FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00328,1,0,true,false )
             ,new CursorDef("T003210", "SELECT COALESCE( T1.[ContagemResultadoLiqLog_Linhas], 0) AS ContagemResultadoLiqLog_Linhas FROM (SELECT COUNT(*) AS ContagemResultadoLiqLog_Linhas, [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (UPDLOCK) GROUP BY [ContagemResultadoLiqLog_Codigo] ) T1 WHERE T1.[ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003210,1,0,true,false )
             ,new CursorDef("T003212", "SELECT TM1.[ContagemResultadoLiqLog_Codigo], TM1.[ContagemResultadoLiqLog_Data], TM1.[ContagemResultadoLiqLog_Observacao], TM1.[ContagemResultadoLiqLog_Owner], COALESCE( T2.[ContagemResultadoLiqLog_Linhas], 0) AS ContagemResultadoLiqLog_Linhas FROM ([ContagemResultadoLiqLog] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS ContagemResultadoLiqLog_Linhas, [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) GROUP BY [ContagemResultadoLiqLog_Codigo] ) T2 ON T2.[ContagemResultadoLiqLog_Codigo] = TM1.[ContagemResultadoLiqLog_Codigo]) WHERE TM1.[ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo ORDER BY TM1.[ContagemResultadoLiqLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003212,100,0,true,false )
             ,new CursorDef("T003214", "SELECT COALESCE( T1.[ContagemResultadoLiqLog_Linhas], 0) AS ContagemResultadoLiqLog_Linhas FROM (SELECT COUNT(*) AS ContagemResultadoLiqLog_Linhas, [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (UPDLOCK) GROUP BY [ContagemResultadoLiqLog_Codigo] ) T1 WHERE T1.[ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003214,1,0,true,false )
             ,new CursorDef("T003215", "SELECT [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003215,1,0,true,false )
             ,new CursorDef("T003216", "SELECT TOP 1 [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE ( [ContagemResultadoLiqLog_Codigo] > @ContagemResultadoLiqLog_Codigo) ORDER BY [ContagemResultadoLiqLog_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003216,1,0,true,true )
             ,new CursorDef("T003217", "SELECT TOP 1 [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE ( [ContagemResultadoLiqLog_Codigo] < @ContagemResultadoLiqLog_Codigo) ORDER BY [ContagemResultadoLiqLog_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003217,1,0,true,true )
             ,new CursorDef("T003218", "INSERT INTO [ContagemResultadoLiqLog]([ContagemResultadoLiqLog_Data], [ContagemResultadoLiqLog_Observacao], [ContagemResultadoLiqLog_Owner]) VALUES(@ContagemResultadoLiqLog_Data, @ContagemResultadoLiqLog_Observacao, @ContagemResultadoLiqLog_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003218)
             ,new CursorDef("T003219", "UPDATE [ContagemResultadoLiqLog] SET [ContagemResultadoLiqLog_Data]=@ContagemResultadoLiqLog_Data, [ContagemResultadoLiqLog_Observacao]=@ContagemResultadoLiqLog_Observacao, [ContagemResultadoLiqLog_Owner]=@ContagemResultadoLiqLog_Owner  WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo", GxErrorMask.GX_NOMASK,prmT003219)
             ,new CursorDef("T003220", "DELETE FROM [ContagemResultadoLiqLog]  WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo", GxErrorMask.GX_NOMASK,prmT003220)
             ,new CursorDef("T003222", "SELECT COALESCE( T1.[ContagemResultadoLiqLog_Linhas], 0) AS ContagemResultadoLiqLog_Linhas FROM (SELECT COUNT(*) AS ContagemResultadoLiqLog_Linhas, [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (UPDLOCK) GROUP BY [ContagemResultadoLiqLog_Codigo] ) T1 WHERE T1.[ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003222,1,0,true,false )
             ,new CursorDef("T003223", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LiqLogCod] = @ContagemResultadoLiqLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003223,1,0,true,true )
             ,new CursorDef("T003224", "SELECT [ContagemResultadoLiqLog_Codigo] FROM [ContagemResultadoLiqLog] WITH (NOLOCK) ORDER BY [ContagemResultadoLiqLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003224,100,0,true,false )
             ,new CursorDef("T003225", "SELECT T1.[ContagemResultadoLiqLog_Codigo], T1.[ContagemResultadoLiqLogOS_Codigo], T3.[Pessoa_Nome] AS ContagemResultadoLiqLogOS_UserNom, T1.[ContagemResultadoLiqLogOS_Valor], T1.[ContagemResultadoLiqLogOS_UserCod] AS ContagemResultadoLiqLogOS_UserCod, T1.[ContagemResultadoLiqLogOS_OSCod] AS ContagemResultadoLiqLogOS_OSCod, T2.[Usuario_PessoaCod] AS ContagemResultadoLiqLogOS_UserPesCod FROM (([ContagemResultadoLiqLogOS] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoLiqLogOS_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo and T1.[ContagemResultadoLiqLogOS_Codigo] = @ContagemResultadoLiqLogOS_Codigo ORDER BY T1.[ContagemResultadoLiqLog_Codigo], T1.[ContagemResultadoLiqLogOS_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003225,11,0,true,false )
             ,new CursorDef("T003226", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoLiqLogOS_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoLiqLogOS_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003226,1,0,true,false )
             ,new CursorDef("T003227", "SELECT [Usuario_PessoaCod] AS ContagemResultadoLiqLogOS_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoLiqLogOS_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003227,1,0,true,false )
             ,new CursorDef("T003228", "SELECT [Pessoa_Nome] AS ContagemResultadoLiqLogOS_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoLiqLogOS_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003228,1,0,true,false )
             ,new CursorDef("T003229", "SELECT [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo AND [ContagemResultadoLiqLogOS_Codigo] = @ContagemResultadoLiqLogOS_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003229,1,0,true,false )
             ,new CursorDef("T003230", "INSERT INTO [ContagemResultadoLiqLogOS]([ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo], [ContagemResultadoLiqLogOS_Valor], [ContagemResultadoLiqLogOS_UserCod], [ContagemResultadoLiqLogOS_OSCod]) VALUES(@ContagemResultadoLiqLog_Codigo, @ContagemResultadoLiqLogOS_Codigo, @ContagemResultadoLiqLogOS_Valor, @ContagemResultadoLiqLogOS_UserCod, @ContagemResultadoLiqLogOS_OSCod)", GxErrorMask.GX_NOMASK,prmT003230)
             ,new CursorDef("T003231", "UPDATE [ContagemResultadoLiqLogOS] SET [ContagemResultadoLiqLogOS_Valor]=@ContagemResultadoLiqLogOS_Valor, [ContagemResultadoLiqLogOS_UserCod]=@ContagemResultadoLiqLogOS_UserCod, [ContagemResultadoLiqLogOS_OSCod]=@ContagemResultadoLiqLogOS_OSCod  WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo AND [ContagemResultadoLiqLogOS_Codigo] = @ContagemResultadoLiqLogOS_Codigo", GxErrorMask.GX_NOMASK,prmT003231)
             ,new CursorDef("T003232", "DELETE FROM [ContagemResultadoLiqLogOS]  WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo AND [ContagemResultadoLiqLogOS_Codigo] = @ContagemResultadoLiqLogOS_Codigo", GxErrorMask.GX_NOMASK,prmT003232)
             ,new CursorDef("T003233", "SELECT [Usuario_PessoaCod] AS ContagemResultadoLiqLogOS_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoLiqLogOS_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003233,1,0,true,false )
             ,new CursorDef("T003234", "SELECT [Pessoa_Nome] AS ContagemResultadoLiqLogOS_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoLiqLogOS_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003234,1,0,true,false )
             ,new CursorDef("T003235", "SELECT [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultadoLiqLog_Codigo ORDER BY [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003235,11,0,true,false )
             ,new CursorDef("T003236", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoLiqLogOS_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoLiqLogOS_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003236,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 14 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 25 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
