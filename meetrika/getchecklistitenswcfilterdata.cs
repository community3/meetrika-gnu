/*
               File: GetCheckListItensWCFilterData
        Description: Get Check List Itens WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:13.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getchecklistitenswcfilterdata : GXProcedure
   {
      public getchecklistitenswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getchecklistitenswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getchecklistitenswcfilterdata objgetchecklistitenswcfilterdata;
         objgetchecklistitenswcfilterdata = new getchecklistitenswcfilterdata();
         objgetchecklistitenswcfilterdata.AV15DDOName = aP0_DDOName;
         objgetchecklistitenswcfilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetchecklistitenswcfilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetchecklistitenswcfilterdata.AV19OptionsJson = "" ;
         objgetchecklistitenswcfilterdata.AV22OptionsDescJson = "" ;
         objgetchecklistitenswcfilterdata.AV24OptionIndexesJson = "" ;
         objgetchecklistitenswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetchecklistitenswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetchecklistitenswcfilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getchecklistitenswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_CHECKLIST_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCHECKLIST_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("CheckListItensWCGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "CheckListItensWCGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("CheckListItensWCGridState"), "");
         }
         AV44GXV1 = 1;
         while ( AV44GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV44GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO") == 0 )
            {
               AV10TFCheckList_Descricao = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO_SEL") == 0 )
            {
               AV11TFCheckList_Descricao_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_IMPEDITIVO_SEL") == 0 )
            {
               AV37TFCheckList_Impeditivo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_SENAOCUMPRE_SEL") == 0 )
            {
               AV35TFCheckList_SeNaoCumpre_SelsJson = AV29GridStateFilterValue.gxTpr_Value;
               AV36TFCheckList_SeNaoCumpre_Sels.FromJSonString(AV35TFCheckList_SeNaoCumpre_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_NAOCNFQDO_SEL") == 0 )
            {
               AV38TFCheckList_NaoCnfQdo_SelsJson = AV29GridStateFilterValue.gxTpr_Value;
               AV39TFCheckList_NaoCnfQdo_Sels.FromJSonString(AV38TFCheckList_NaoCnfQdo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "PARM_&CHECK_CODIGO") == 0 )
            {
               AV31Check_Codigo = (int)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV44GXV1 = (int)(AV44GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCHECKLIST_DESCRICAOOPTIONS' Routine */
         AV10TFCheckList_Descricao = AV13SearchTxt;
         AV11TFCheckList_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1848CheckList_SeNaoCumpre ,
                                              AV36TFCheckList_SeNaoCumpre_Sels ,
                                              A1851CheckList_NaoCnfQdo ,
                                              AV39TFCheckList_NaoCnfQdo_Sels ,
                                              AV11TFCheckList_Descricao_Sel ,
                                              AV10TFCheckList_Descricao ,
                                              AV37TFCheckList_Impeditivo_Sel ,
                                              AV36TFCheckList_SeNaoCumpre_Sels.Count ,
                                              AV39TFCheckList_NaoCnfQdo_Sels.Count ,
                                              A763CheckList_Descricao ,
                                              A1850CheckList_Impeditivo ,
                                              AV31Check_Codigo ,
                                              A1839Check_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV10TFCheckList_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFCheckList_Descricao), "%", "");
         /* Using cursor P00T42 */
         pr_default.execute(0, new Object[] {AV31Check_Codigo, lV10TFCheckList_Descricao, AV11TFCheckList_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKT42 = false;
            A1839Check_Codigo = P00T42_A1839Check_Codigo[0];
            n1839Check_Codigo = P00T42_n1839Check_Codigo[0];
            A763CheckList_Descricao = P00T42_A763CheckList_Descricao[0];
            A1851CheckList_NaoCnfQdo = P00T42_A1851CheckList_NaoCnfQdo[0];
            n1851CheckList_NaoCnfQdo = P00T42_n1851CheckList_NaoCnfQdo[0];
            A1848CheckList_SeNaoCumpre = P00T42_A1848CheckList_SeNaoCumpre[0];
            n1848CheckList_SeNaoCumpre = P00T42_n1848CheckList_SeNaoCumpre[0];
            A1850CheckList_Impeditivo = P00T42_A1850CheckList_Impeditivo[0];
            n1850CheckList_Impeditivo = P00T42_n1850CheckList_Impeditivo[0];
            A758CheckList_Codigo = P00T42_A758CheckList_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00T42_A1839Check_Codigo[0] == A1839Check_Codigo ) && ( StringUtil.StrCmp(P00T42_A763CheckList_Descricao[0], A763CheckList_Descricao) == 0 ) )
            {
               BRKT42 = false;
               A758CheckList_Codigo = P00T42_A758CheckList_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKT42 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A763CheckList_Descricao)) )
            {
               AV17Option = A763CheckList_Descricao;
               AV18Options.Add(AV17Option, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKT42 )
            {
               BRKT42 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFCheckList_Descricao = "";
         AV11TFCheckList_Descricao_Sel = "";
         AV35TFCheckList_SeNaoCumpre_SelsJson = "";
         AV36TFCheckList_SeNaoCumpre_Sels = new GxSimpleCollection();
         AV38TFCheckList_NaoCnfQdo_SelsJson = "";
         AV39TFCheckList_NaoCnfQdo_Sels = new GxSimpleCollection();
         scmdbuf = "";
         lV10TFCheckList_Descricao = "";
         A1851CheckList_NaoCnfQdo = "";
         A763CheckList_Descricao = "";
         P00T42_A1839Check_Codigo = new int[1] ;
         P00T42_n1839Check_Codigo = new bool[] {false} ;
         P00T42_A763CheckList_Descricao = new String[] {""} ;
         P00T42_A1851CheckList_NaoCnfQdo = new String[] {""} ;
         P00T42_n1851CheckList_NaoCnfQdo = new bool[] {false} ;
         P00T42_A1848CheckList_SeNaoCumpre = new short[1] ;
         P00T42_n1848CheckList_SeNaoCumpre = new bool[] {false} ;
         P00T42_A1850CheckList_Impeditivo = new bool[] {false} ;
         P00T42_n1850CheckList_Impeditivo = new bool[] {false} ;
         P00T42_A758CheckList_Codigo = new int[1] ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getchecklistitenswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00T42_A1839Check_Codigo, P00T42_n1839Check_Codigo, P00T42_A763CheckList_Descricao, P00T42_A1851CheckList_NaoCnfQdo, P00T42_n1851CheckList_NaoCnfQdo, P00T42_A1848CheckList_SeNaoCumpre, P00T42_n1848CheckList_SeNaoCumpre, P00T42_A1850CheckList_Impeditivo, P00T42_n1850CheckList_Impeditivo, P00T42_A758CheckList_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV37TFCheckList_Impeditivo_Sel ;
      private short A1848CheckList_SeNaoCumpre ;
      private int AV44GXV1 ;
      private int AV31Check_Codigo ;
      private int AV36TFCheckList_SeNaoCumpre_Sels_Count ;
      private int AV39TFCheckList_NaoCnfQdo_Sels_Count ;
      private int A1839Check_Codigo ;
      private int A758CheckList_Codigo ;
      private long AV25count ;
      private String scmdbuf ;
      private String A1851CheckList_NaoCnfQdo ;
      private bool returnInSub ;
      private bool A1850CheckList_Impeditivo ;
      private bool BRKT42 ;
      private bool n1839Check_Codigo ;
      private bool n1851CheckList_NaoCnfQdo ;
      private bool n1848CheckList_SeNaoCumpre ;
      private bool n1850CheckList_Impeditivo ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV35TFCheckList_SeNaoCumpre_SelsJson ;
      private String AV38TFCheckList_NaoCnfQdo_SelsJson ;
      private String A763CheckList_Descricao ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV10TFCheckList_Descricao ;
      private String AV11TFCheckList_Descricao_Sel ;
      private String lV10TFCheckList_Descricao ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00T42_A1839Check_Codigo ;
      private bool[] P00T42_n1839Check_Codigo ;
      private String[] P00T42_A763CheckList_Descricao ;
      private String[] P00T42_A1851CheckList_NaoCnfQdo ;
      private bool[] P00T42_n1851CheckList_NaoCnfQdo ;
      private short[] P00T42_A1848CheckList_SeNaoCumpre ;
      private bool[] P00T42_n1848CheckList_SeNaoCumpre ;
      private bool[] P00T42_A1850CheckList_Impeditivo ;
      private bool[] P00T42_n1850CheckList_Impeditivo ;
      private int[] P00T42_A758CheckList_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV36TFCheckList_SeNaoCumpre_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39TFCheckList_NaoCnfQdo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class getchecklistitenswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00T42( IGxContext context ,
                                             short A1848CheckList_SeNaoCumpre ,
                                             IGxCollection AV36TFCheckList_SeNaoCumpre_Sels ,
                                             String A1851CheckList_NaoCnfQdo ,
                                             IGxCollection AV39TFCheckList_NaoCnfQdo_Sels ,
                                             String AV11TFCheckList_Descricao_Sel ,
                                             String AV10TFCheckList_Descricao ,
                                             short AV37TFCheckList_Impeditivo_Sel ,
                                             int AV36TFCheckList_SeNaoCumpre_Sels_Count ,
                                             int AV39TFCheckList_NaoCnfQdo_Sels_Count ,
                                             String A763CheckList_Descricao ,
                                             bool A1850CheckList_Impeditivo ,
                                             int AV31Check_Codigo ,
                                             int A1839Check_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [3] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Check_Codigo], [CheckList_Descricao], [CheckList_NaoCnfQdo], [CheckList_SeNaoCumpre], [CheckList_Impeditivo], [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Check_Codigo] = @AV31Check_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCheckList_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFCheckList_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([CheckList_Descricao] like @lV10TFCheckList_Descricao)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCheckList_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([CheckList_Descricao] = @AV11TFCheckList_Descricao_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV37TFCheckList_Impeditivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([CheckList_Impeditivo] = 1)";
         }
         if ( AV37TFCheckList_Impeditivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([CheckList_Impeditivo] = 0)";
         }
         if ( AV36TFCheckList_SeNaoCumpre_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV36TFCheckList_SeNaoCumpre_Sels, "[CheckList_SeNaoCumpre] IN (", ")") + ")";
         }
         if ( AV39TFCheckList_NaoCnfQdo_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV39TFCheckList_NaoCnfQdo_Sels, "[CheckList_NaoCnfQdo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Check_Codigo], [CheckList_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00T42(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00T42 ;
          prmP00T42 = new Object[] {
          new Object[] {"@AV31Check_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFCheckList_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFCheckList_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00T42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00T42,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getchecklistitenswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getchecklistitenswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getchecklistitenswcfilterdata") )
          {
             return  ;
          }
          getchecklistitenswcfilterdata worker = new getchecklistitenswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
