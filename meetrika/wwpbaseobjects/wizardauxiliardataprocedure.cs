/*
               File: WWPBaseObjects.WizardAuxiliarDataProcedure
        Description: Wizard Auxiliar Data Procedure
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:49.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   public class wizardauxiliardataprocedure : GXProcedure
   {
      public wizardauxiliardataprocedure( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wizardauxiliardataprocedure( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref IGxCollection aP0_WizardAuxiliarData ,
                           ref String aP1_Key ,
                           ref String aP2_Value ,
                           ref String aP3_WizardMode )
      {
         this.AV12WizardAuxiliarData = aP0_WizardAuxiliarData;
         this.AV9Key = aP1_Key;
         this.AV10Value = aP2_Value;
         this.AV11WizardMode = aP3_WizardMode;
         initialize();
         executePrivate();
         aP0_WizardAuxiliarData=this.AV12WizardAuxiliarData;
         aP1_Key=this.AV9Key;
         aP2_Value=this.AV10Value;
         aP3_WizardMode=this.AV11WizardMode;
      }

      public String executeUdp( ref IGxCollection aP0_WizardAuxiliarData ,
                                ref String aP1_Key ,
                                ref String aP2_Value )
      {
         this.AV12WizardAuxiliarData = aP0_WizardAuxiliarData;
         this.AV9Key = aP1_Key;
         this.AV10Value = aP2_Value;
         this.AV11WizardMode = aP3_WizardMode;
         initialize();
         executePrivate();
         aP0_WizardAuxiliarData=this.AV12WizardAuxiliarData;
         aP1_Key=this.AV9Key;
         aP2_Value=this.AV10Value;
         aP3_WizardMode=this.AV11WizardMode;
         return AV11WizardMode ;
      }

      public void executeSubmit( ref IGxCollection aP0_WizardAuxiliarData ,
                                 ref String aP1_Key ,
                                 ref String aP2_Value ,
                                 ref String aP3_WizardMode )
      {
         wizardauxiliardataprocedure objwizardauxiliardataprocedure;
         objwizardauxiliardataprocedure = new wizardauxiliardataprocedure();
         objwizardauxiliardataprocedure.AV12WizardAuxiliarData = aP0_WizardAuxiliarData;
         objwizardauxiliardataprocedure.AV9Key = aP1_Key;
         objwizardauxiliardataprocedure.AV10Value = aP2_Value;
         objwizardauxiliardataprocedure.AV11WizardMode = aP3_WizardMode;
         objwizardauxiliardataprocedure.context.SetSubmitInitialConfig(context);
         objwizardauxiliardataprocedure.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objwizardauxiliardataprocedure);
         aP0_WizardAuxiliarData=this.AV12WizardAuxiliarData;
         aP1_Key=this.AV9Key;
         aP2_Value=this.AV10Value;
         aP3_WizardMode=this.AV11WizardMode;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((wizardauxiliardataprocedure)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(StringUtil.Upper( AV11WizardMode), "GET") == 0 )
         {
            AV18GXV1 = 1;
            while ( AV18GXV1 <= AV12WizardAuxiliarData.Count )
            {
               AV13WizardAuxiliarDataItem = ((wwpbaseobjects.SdtWizardAuxiliarData_WizardAuxiliarDataItem)AV12WizardAuxiliarData.Item(AV18GXV1));
               if ( StringUtil.StrCmp(AV13WizardAuxiliarDataItem.gxTpr_Key, AV9Key) == 0 )
               {
                  AV10Value = AV13WizardAuxiliarDataItem.gxTpr_Value;
               }
               AV18GXV1 = (int)(AV18GXV1+1);
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV11WizardMode), "SET") == 0 )
         {
            AV14ExistKey = false;
            AV19GXV2 = 1;
            while ( AV19GXV2 <= AV12WizardAuxiliarData.Count )
            {
               AV13WizardAuxiliarDataItem = ((wwpbaseobjects.SdtWizardAuxiliarData_WizardAuxiliarDataItem)AV12WizardAuxiliarData.Item(AV19GXV2));
               if ( StringUtil.StrCmp(AV13WizardAuxiliarDataItem.gxTpr_Key, AV9Key) == 0 )
               {
                  AV14ExistKey = true;
                  AV13WizardAuxiliarDataItem.gxTpr_Value = AV10Value;
               }
               AV19GXV2 = (int)(AV19GXV2+1);
            }
            if ( ! AV14ExistKey )
            {
               AV13WizardAuxiliarDataItem = new wwpbaseobjects.SdtWizardAuxiliarData_WizardAuxiliarDataItem(context);
               AV13WizardAuxiliarDataItem.gxTpr_Key = AV9Key;
               AV13WizardAuxiliarDataItem.gxTpr_Value = AV10Value;
               AV12WizardAuxiliarData.Add(AV13WizardAuxiliarDataItem, 0);
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV11WizardMode), "DLT") == 0 )
         {
            AV15Index = 1;
            AV20GXV3 = 1;
            while ( AV20GXV3 <= AV12WizardAuxiliarData.Count )
            {
               AV13WizardAuxiliarDataItem = ((wwpbaseobjects.SdtWizardAuxiliarData_WizardAuxiliarDataItem)AV12WizardAuxiliarData.Item(AV20GXV3));
               if ( StringUtil.StrCmp(AV13WizardAuxiliarDataItem.gxTpr_Key, AV9Key) == 0 )
               {
                  if (true) break;
               }
               else
               {
                  AV15Index = (short)(AV15Index+1);
               }
               AV20GXV3 = (int)(AV20GXV3+1);
            }
            if ( AV15Index <= AV12WizardAuxiliarData.Count )
            {
               AV12WizardAuxiliarData.RemoveItem(AV15Index);
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13WizardAuxiliarDataItem = new wwpbaseobjects.SdtWizardAuxiliarData_WizardAuxiliarDataItem(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15Index ;
      private int AV18GXV1 ;
      private int AV19GXV2 ;
      private int AV20GXV3 ;
      private String AV11WizardMode ;
      private bool AV14ExistKey ;
      private String AV10Value ;
      private String AV9Key ;
      private IGxCollection aP0_WizardAuxiliarData ;
      private String aP1_Key ;
      private String aP2_Value ;
      private String aP3_WizardMode ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWizardAuxiliarData_WizardAuxiliarDataItem ))]
      private IGxCollection AV12WizardAuxiliarData ;
      private wwpbaseobjects.SdtWizardAuxiliarData_WizardAuxiliarDataItem AV13WizardAuxiliarDataItem ;
   }

}
