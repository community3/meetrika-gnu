/*
               File: wwpbaseobjects.type_SdtGridStateCollection_Item
        Description: WWPBaseObjects\GridStateCollection
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 21:37:17.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GridStateCollection.Item" )]
   [XmlType(TypeName =  "GridStateCollection.Item" , Namespace = "" )]
   [Serializable]
   public class SdtGridStateCollection_Item : GxUserType
   {
      public SdtGridStateCollection_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtGridStateCollection_Item_Title = "";
         gxTv_SdtGridStateCollection_Item_Gridstatexml = "";
      }

      public SdtGridStateCollection_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtGridStateCollection_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtGridStateCollection_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtGridStateCollection_Item obj ;
         obj = this;
         obj.gxTpr_Title = deserialized.gxTpr_Title;
         obj.gxTpr_Gridstatexml = deserialized.gxTpr_Gridstatexml;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Title") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtGridStateCollection_Item_Title = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "GridStateXML") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtGridStateCollection_Item_Gridstatexml = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\GridStateCollection.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Title", StringUtil.RTrim( gxTv_SdtGridStateCollection_Item_Title));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("GridStateXML", StringUtil.RTrim( gxTv_SdtGridStateCollection_Item_Gridstatexml));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Title", gxTv_SdtGridStateCollection_Item_Title, false);
         AddObjectProperty("GridStateXML", gxTv_SdtGridStateCollection_Item_Gridstatexml, false);
         return  ;
      }

      [  SoapElement( ElementName = "Title" )]
      [  XmlElement( ElementName = "Title"   )]
      public String gxTpr_Title
      {
         get {
            return gxTv_SdtGridStateCollection_Item_Title ;
         }

         set {
            gxTv_SdtGridStateCollection_Item_Title = (String)(value);
         }

      }

      [  SoapElement( ElementName = "GridStateXML" )]
      [  XmlElement( ElementName = "GridStateXML"   )]
      public String gxTpr_Gridstatexml
      {
         get {
            return gxTv_SdtGridStateCollection_Item_Gridstatexml ;
         }

         set {
            gxTv_SdtGridStateCollection_Item_Gridstatexml = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGridStateCollection_Item_Title = "";
         gxTv_SdtGridStateCollection_Item_Gridstatexml = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtGridStateCollection_Item_Gridstatexml ;
      protected String gxTv_SdtGridStateCollection_Item_Title ;
   }

   [DataContract(Name = @"WWPBaseObjects\GridStateCollection.Item", Namespace = "")]
   public class SdtGridStateCollection_Item_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtGridStateCollection_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGridStateCollection_Item_RESTInterface( ) : base()
      {
      }

      public SdtGridStateCollection_Item_RESTInterface( wwpbaseobjects.SdtGridStateCollection_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Title" , Order = 0 )]
      public String gxTpr_Title
      {
         get {
            return sdt.gxTpr_Title ;
         }

         set {
            sdt.gxTpr_Title = (String)(value);
         }

      }

      [DataMember( Name = "GridStateXML" , Order = 1 )]
      public String gxTpr_Gridstatexml
      {
         get {
            return sdt.gxTpr_Gridstatexml ;
         }

         set {
            sdt.gxTpr_Gridstatexml = (String)(value);
         }

      }

      public wwpbaseobjects.SdtGridStateCollection_Item sdt
      {
         get {
            return (wwpbaseobjects.SdtGridStateCollection_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtGridStateCollection_Item() ;
         }
      }

   }

}
