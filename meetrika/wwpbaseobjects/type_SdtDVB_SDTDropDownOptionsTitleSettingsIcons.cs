/*
               File: wwpbaseobjects.type_SdtDVB_SDTDropDownOptionsTitleSettingsIcons
        Description: WWPBaseObjects\DVB_SDTDropDownOptionsTitleSettingsIcons
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:56.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "TitleSettingsIcons" )]
   [XmlType(TypeName =  "TitleSettingsIcons" , Namespace = "" )]
   [Serializable]
   public class SdtDVB_SDTDropDownOptionsTitleSettingsIcons : GxUserType
   {
      public SdtDVB_SDTDropDownOptionsTitleSettingsIcons( )
      {
         /* Constructor for serialization */
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi = "";
      }

      public SdtDVB_SDTDropDownOptionsTitleSettingsIcons( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons obj ;
         obj = this;
         obj.gxTpr_Default = deserialized.gxTpr_Default;
         obj.gxTpr_Default_gxi = deserialized.gxTpr_Default_gxi;
         obj.gxTpr_Filtered = deserialized.gxTpr_Filtered;
         obj.gxTpr_Filtered_gxi = deserialized.gxTpr_Filtered_gxi;
         obj.gxTpr_Sortedasc = deserialized.gxTpr_Sortedasc;
         obj.gxTpr_Sortedasc_gxi = deserialized.gxTpr_Sortedasc_gxi;
         obj.gxTpr_Sorteddsc = deserialized.gxTpr_Sorteddsc;
         obj.gxTpr_Sorteddsc_gxi = deserialized.gxTpr_Sorteddsc_gxi;
         obj.gxTpr_Filteredsortedasc = deserialized.gxTpr_Filteredsortedasc;
         obj.gxTpr_Filteredsortedasc_gxi = deserialized.gxTpr_Filteredsortedasc_gxi;
         obj.gxTpr_Filteredsorteddsc = deserialized.gxTpr_Filteredsorteddsc;
         obj.gxTpr_Filteredsorteddsc_gxi = deserialized.gxTpr_Filteredsorteddsc_gxi;
         obj.gxTpr_Optionsortasc = deserialized.gxTpr_Optionsortasc;
         obj.gxTpr_Optionsortasc_gxi = deserialized.gxTpr_Optionsortasc_gxi;
         obj.gxTpr_Optionsortdsc = deserialized.gxTpr_Optionsortdsc;
         obj.gxTpr_Optionsortdsc_gxi = deserialized.gxTpr_Optionsortdsc_gxi;
         obj.gxTpr_Optionapplyfilter = deserialized.gxTpr_Optionapplyfilter;
         obj.gxTpr_Optionapplyfilter_gxi = deserialized.gxTpr_Optionapplyfilter_gxi;
         obj.gxTpr_Optionfilteringdata = deserialized.gxTpr_Optionfilteringdata;
         obj.gxTpr_Optionfilteringdata_gxi = deserialized.gxTpr_Optionfilteringdata_gxi;
         obj.gxTpr_Optioncleanfilters = deserialized.gxTpr_Optioncleanfilters;
         obj.gxTpr_Optioncleanfilters_gxi = deserialized.gxTpr_Optioncleanfilters_gxi;
         obj.gxTpr_Selectedoption = deserialized.gxTpr_Selectedoption;
         obj.gxTpr_Selectedoption_gxi = deserialized.gxTpr_Selectedoption_gxi;
         obj.gxTpr_Multiseloption = deserialized.gxTpr_Multiseloption;
         obj.gxTpr_Multiseloption_gxi = deserialized.gxTpr_Multiseloption_gxi;
         obj.gxTpr_Multiselseloption = deserialized.gxTpr_Multiselseloption;
         obj.gxTpr_Multiselseloption_gxi = deserialized.gxTpr_Multiselseloption_gxi;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Default") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Default_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Filtered") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Filtered_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SortedASC") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SortedASC_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SortedDSC") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SortedDSC_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FilteredSortedASC") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FilteredSortedASC_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FilteredSortedDSC") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FilteredSortedDSC_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionSortASC") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionSortASC_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionSortDSC") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionSortDSC_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionApplyFilter") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionApplyFilter_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionFilteringData") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionFilteringData_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionCleanFilters") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OptionCleanFilters_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SelectedOption") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SelectedOption_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "MultiselOption") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "MultiselOption_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "MultiselSelOption") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "MultiselSelOption_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "TitleSettingsIcons";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Default", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("Default_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("Filtered", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("Filtered_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("SortedASC", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("SortedASC_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("SortedDSC", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("SortedDSC_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("FilteredSortedASC", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("FilteredSortedASC_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("FilteredSortedDSC", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("FilteredSortedDSC_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionSortASC", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionSortASC_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionSortDSC", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionSortDSC_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionApplyFilter", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionApplyFilter_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionFilteringData", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionFilteringData_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionCleanFilters", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("OptionCleanFilters_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("SelectedOption", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("SelectedOption_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("MultiselOption", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("MultiselOption_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("MultiselSelOption", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("MultiselSelOption_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Default", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default, false);
         AddObjectProperty("Default_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi, false);
         AddObjectProperty("Filtered", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered, false);
         AddObjectProperty("Filtered_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi, false);
         AddObjectProperty("SortedASC", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc, false);
         AddObjectProperty("SortedASC_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi, false);
         AddObjectProperty("SortedDSC", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc, false);
         AddObjectProperty("SortedDSC_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi, false);
         AddObjectProperty("FilteredSortedASC", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc, false);
         AddObjectProperty("FilteredSortedASC_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi, false);
         AddObjectProperty("FilteredSortedDSC", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc, false);
         AddObjectProperty("FilteredSortedDSC_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi, false);
         AddObjectProperty("OptionSortASC", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc, false);
         AddObjectProperty("OptionSortASC_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi, false);
         AddObjectProperty("OptionSortDSC", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc, false);
         AddObjectProperty("OptionSortDSC_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi, false);
         AddObjectProperty("OptionApplyFilter", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter, false);
         AddObjectProperty("OptionApplyFilter_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi, false);
         AddObjectProperty("OptionFilteringData", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata, false);
         AddObjectProperty("OptionFilteringData_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi, false);
         AddObjectProperty("OptionCleanFilters", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters, false);
         AddObjectProperty("OptionCleanFilters_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi, false);
         AddObjectProperty("SelectedOption", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption, false);
         AddObjectProperty("SelectedOption_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi, false);
         AddObjectProperty("MultiselOption", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption, false);
         AddObjectProperty("MultiselOption_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi, false);
         AddObjectProperty("MultiselSelOption", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption, false);
         AddObjectProperty("MultiselSelOption_GXI", gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi, false);
         return  ;
      }

      [  SoapElement( ElementName = "Default" )]
      [  XmlElement( ElementName = "Default"   )]
      [GxUpload()]
      public String gxTpr_Default
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Default_GXI" )]
      [  XmlElement( ElementName = "Default_GXI"   )]
      public String gxTpr_Default_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Filtered" )]
      [  XmlElement( ElementName = "Filtered"   )]
      [GxUpload()]
      public String gxTpr_Filtered
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Filtered_GXI" )]
      [  XmlElement( ElementName = "Filtered_GXI"   )]
      public String gxTpr_Filtered_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SortedASC" )]
      [  XmlElement( ElementName = "SortedASC"   )]
      [GxUpload()]
      public String gxTpr_Sortedasc
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SortedASC_GXI" )]
      [  XmlElement( ElementName = "SortedASC_GXI"   )]
      public String gxTpr_Sortedasc_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SortedDSC" )]
      [  XmlElement( ElementName = "SortedDSC"   )]
      [GxUpload()]
      public String gxTpr_Sorteddsc
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SortedDSC_GXI" )]
      [  XmlElement( ElementName = "SortedDSC_GXI"   )]
      public String gxTpr_Sorteddsc_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "FilteredSortedASC" )]
      [  XmlElement( ElementName = "FilteredSortedASC"   )]
      [GxUpload()]
      public String gxTpr_Filteredsortedasc
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "FilteredSortedASC_GXI" )]
      [  XmlElement( ElementName = "FilteredSortedASC_GXI"   )]
      public String gxTpr_Filteredsortedasc_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "FilteredSortedDSC" )]
      [  XmlElement( ElementName = "FilteredSortedDSC"   )]
      [GxUpload()]
      public String gxTpr_Filteredsorteddsc
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "FilteredSortedDSC_GXI" )]
      [  XmlElement( ElementName = "FilteredSortedDSC_GXI"   )]
      public String gxTpr_Filteredsorteddsc_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionSortASC" )]
      [  XmlElement( ElementName = "OptionSortASC"   )]
      [GxUpload()]
      public String gxTpr_Optionsortasc
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionSortASC_GXI" )]
      [  XmlElement( ElementName = "OptionSortASC_GXI"   )]
      public String gxTpr_Optionsortasc_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionSortDSC" )]
      [  XmlElement( ElementName = "OptionSortDSC"   )]
      [GxUpload()]
      public String gxTpr_Optionsortdsc
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionSortDSC_GXI" )]
      [  XmlElement( ElementName = "OptionSortDSC_GXI"   )]
      public String gxTpr_Optionsortdsc_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionApplyFilter" )]
      [  XmlElement( ElementName = "OptionApplyFilter"   )]
      [GxUpload()]
      public String gxTpr_Optionapplyfilter
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionApplyFilter_GXI" )]
      [  XmlElement( ElementName = "OptionApplyFilter_GXI"   )]
      public String gxTpr_Optionapplyfilter_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionFilteringData" )]
      [  XmlElement( ElementName = "OptionFilteringData"   )]
      [GxUpload()]
      public String gxTpr_Optionfilteringdata
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionFilteringData_GXI" )]
      [  XmlElement( ElementName = "OptionFilteringData_GXI"   )]
      public String gxTpr_Optionfilteringdata_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionCleanFilters" )]
      [  XmlElement( ElementName = "OptionCleanFilters"   )]
      [GxUpload()]
      public String gxTpr_Optioncleanfilters
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OptionCleanFilters_GXI" )]
      [  XmlElement( ElementName = "OptionCleanFilters_GXI"   )]
      public String gxTpr_Optioncleanfilters_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SelectedOption" )]
      [  XmlElement( ElementName = "SelectedOption"   )]
      [GxUpload()]
      public String gxTpr_Selectedoption
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SelectedOption_GXI" )]
      [  XmlElement( ElementName = "SelectedOption_GXI"   )]
      public String gxTpr_Selectedoption_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "MultiselOption" )]
      [  XmlElement( ElementName = "MultiselOption"   )]
      [GxUpload()]
      public String gxTpr_Multiseloption
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption = (String)(value);
         }

      }

      [  SoapElement( ElementName = "MultiselOption_GXI" )]
      [  XmlElement( ElementName = "MultiselOption_GXI"   )]
      public String gxTpr_Multiseloption_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "MultiselSelOption" )]
      [  XmlElement( ElementName = "MultiselSelOption"   )]
      [GxUpload()]
      public String gxTpr_Multiselseloption
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption = (String)(value);
         }

      }

      [  SoapElement( ElementName = "MultiselSelOption_GXI" )]
      [  XmlElement( ElementName = "MultiselSelOption_GXI"   )]
      public String gxTpr_Multiselseloption_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption = "";
         gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Default ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filtered ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sortedasc ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Sorteddsc ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsortedasc ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Filteredsorteddsc ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortasc ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionsortdsc ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionapplyfilter ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optionfilteringdata ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Optioncleanfilters ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Selectedoption ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiseloption ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsTitleSettingsIcons_Multiselseloption ;
   }

   [DataContract(Name = @"WWPBaseObjects\DVB_SDTDropDownOptionsTitleSettingsIcons", Namespace = "")]
   public class SdtDVB_SDTDropDownOptionsTitleSettingsIcons_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtDVB_SDTDropDownOptionsTitleSettingsIcons_RESTInterface( ) : base()
      {
      }

      public SdtDVB_SDTDropDownOptionsTitleSettingsIcons_RESTInterface( wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Default" , Order = 0 )]
      [GxUpload()]
      public String gxTpr_Default
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Default)) ? PathUtil.RelativePath( sdt.gxTpr_Default) : StringUtil.RTrim( sdt.gxTpr_Default_gxi)) ;
         }

         set {
            sdt.gxTpr_Default = (String)(value);
         }

      }

      [DataMember( Name = "Filtered" , Order = 2 )]
      [GxUpload()]
      public String gxTpr_Filtered
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Filtered)) ? PathUtil.RelativePath( sdt.gxTpr_Filtered) : StringUtil.RTrim( sdt.gxTpr_Filtered_gxi)) ;
         }

         set {
            sdt.gxTpr_Filtered = (String)(value);
         }

      }

      [DataMember( Name = "SortedASC" , Order = 4 )]
      [GxUpload()]
      public String gxTpr_Sortedasc
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Sortedasc)) ? PathUtil.RelativePath( sdt.gxTpr_Sortedasc) : StringUtil.RTrim( sdt.gxTpr_Sortedasc_gxi)) ;
         }

         set {
            sdt.gxTpr_Sortedasc = (String)(value);
         }

      }

      [DataMember( Name = "SortedDSC" , Order = 6 )]
      [GxUpload()]
      public String gxTpr_Sorteddsc
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Sorteddsc)) ? PathUtil.RelativePath( sdt.gxTpr_Sorteddsc) : StringUtil.RTrim( sdt.gxTpr_Sorteddsc_gxi)) ;
         }

         set {
            sdt.gxTpr_Sorteddsc = (String)(value);
         }

      }

      [DataMember( Name = "FilteredSortedASC" , Order = 8 )]
      [GxUpload()]
      public String gxTpr_Filteredsortedasc
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Filteredsortedasc)) ? PathUtil.RelativePath( sdt.gxTpr_Filteredsortedasc) : StringUtil.RTrim( sdt.gxTpr_Filteredsortedasc_gxi)) ;
         }

         set {
            sdt.gxTpr_Filteredsortedasc = (String)(value);
         }

      }

      [DataMember( Name = "FilteredSortedDSC" , Order = 10 )]
      [GxUpload()]
      public String gxTpr_Filteredsorteddsc
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Filteredsorteddsc)) ? PathUtil.RelativePath( sdt.gxTpr_Filteredsorteddsc) : StringUtil.RTrim( sdt.gxTpr_Filteredsorteddsc_gxi)) ;
         }

         set {
            sdt.gxTpr_Filteredsorteddsc = (String)(value);
         }

      }

      [DataMember( Name = "OptionSortASC" , Order = 12 )]
      [GxUpload()]
      public String gxTpr_Optionsortasc
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Optionsortasc)) ? PathUtil.RelativePath( sdt.gxTpr_Optionsortasc) : StringUtil.RTrim( sdt.gxTpr_Optionsortasc_gxi)) ;
         }

         set {
            sdt.gxTpr_Optionsortasc = (String)(value);
         }

      }

      [DataMember( Name = "OptionSortDSC" , Order = 14 )]
      [GxUpload()]
      public String gxTpr_Optionsortdsc
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Optionsortdsc)) ? PathUtil.RelativePath( sdt.gxTpr_Optionsortdsc) : StringUtil.RTrim( sdt.gxTpr_Optionsortdsc_gxi)) ;
         }

         set {
            sdt.gxTpr_Optionsortdsc = (String)(value);
         }

      }

      [DataMember( Name = "OptionApplyFilter" , Order = 16 )]
      [GxUpload()]
      public String gxTpr_Optionapplyfilter
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Optionapplyfilter)) ? PathUtil.RelativePath( sdt.gxTpr_Optionapplyfilter) : StringUtil.RTrim( sdt.gxTpr_Optionapplyfilter_gxi)) ;
         }

         set {
            sdt.gxTpr_Optionapplyfilter = (String)(value);
         }

      }

      [DataMember( Name = "OptionFilteringData" , Order = 18 )]
      [GxUpload()]
      public String gxTpr_Optionfilteringdata
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Optionfilteringdata)) ? PathUtil.RelativePath( sdt.gxTpr_Optionfilteringdata) : StringUtil.RTrim( sdt.gxTpr_Optionfilteringdata_gxi)) ;
         }

         set {
            sdt.gxTpr_Optionfilteringdata = (String)(value);
         }

      }

      [DataMember( Name = "OptionCleanFilters" , Order = 20 )]
      [GxUpload()]
      public String gxTpr_Optioncleanfilters
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Optioncleanfilters)) ? PathUtil.RelativePath( sdt.gxTpr_Optioncleanfilters) : StringUtil.RTrim( sdt.gxTpr_Optioncleanfilters_gxi)) ;
         }

         set {
            sdt.gxTpr_Optioncleanfilters = (String)(value);
         }

      }

      [DataMember( Name = "SelectedOption" , Order = 22 )]
      [GxUpload()]
      public String gxTpr_Selectedoption
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Selectedoption)) ? PathUtil.RelativePath( sdt.gxTpr_Selectedoption) : StringUtil.RTrim( sdt.gxTpr_Selectedoption_gxi)) ;
         }

         set {
            sdt.gxTpr_Selectedoption = (String)(value);
         }

      }

      [DataMember( Name = "MultiselOption" , Order = 24 )]
      [GxUpload()]
      public String gxTpr_Multiseloption
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Multiseloption)) ? PathUtil.RelativePath( sdt.gxTpr_Multiseloption) : StringUtil.RTrim( sdt.gxTpr_Multiseloption_gxi)) ;
         }

         set {
            sdt.gxTpr_Multiseloption = (String)(value);
         }

      }

      [DataMember( Name = "MultiselSelOption" , Order = 26 )]
      [GxUpload()]
      public String gxTpr_Multiselseloption
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Multiselseloption)) ? PathUtil.RelativePath( sdt.gxTpr_Multiselseloption) : StringUtil.RTrim( sdt.gxTpr_Multiselseloption_gxi)) ;
         }

         set {
            sdt.gxTpr_Multiselseloption = (String)(value);
         }

      }

      public wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons sdt
      {
         get {
            return (wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons() ;
         }
      }

   }

}
