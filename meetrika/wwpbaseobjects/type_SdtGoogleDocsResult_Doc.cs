/*
               File: wwpbaseobjects.type_SdtGoogleDocsResult_Doc
        Description: WWPBaseObjects\GoogleDocsResult
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:8.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GoogleDocsResult.Doc" )]
   [XmlType(TypeName =  "GoogleDocsResult.Doc" , Namespace = "DVelop.Extensions.GoogleDocs" )]
   [Serializable]
   public class SdtGoogleDocsResult_Doc : GxUserType
   {
      public SdtGoogleDocsResult_Doc( )
      {
         /* Constructor for serialization */
         gxTv_SdtGoogleDocsResult_Doc_Type = "";
         gxTv_SdtGoogleDocsResult_Doc_Title = "";
         gxTv_SdtGoogleDocsResult_Doc_Url = "";
      }

      public SdtGoogleDocsResult_Doc( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtGoogleDocsResult_Doc deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "DVelop.Extensions.GoogleDocs" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtGoogleDocsResult_Doc)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtGoogleDocsResult_Doc obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Title = deserialized.gxTpr_Title;
         obj.gxTpr_Url = deserialized.gxTpr_Url;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Type") )
               {
                  gxTv_SdtGoogleDocsResult_Doc_Type = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Title") )
               {
                  gxTv_SdtGoogleDocsResult_Doc_Title = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "URL") )
               {
                  gxTv_SdtGoogleDocsResult_Doc_Url = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\GoogleDocsResult.Doc";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Type", StringUtil.RTrim( gxTv_SdtGoogleDocsResult_Doc_Type));
         if ( StringUtil.StrCmp(sNameSpace, "DVelop.Extensions.GoogleDocs") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "DVelop.Extensions.GoogleDocs");
         }
         oWriter.WriteElement("Title", StringUtil.RTrim( gxTv_SdtGoogleDocsResult_Doc_Title));
         if ( StringUtil.StrCmp(sNameSpace, "DVelop.Extensions.GoogleDocs") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "DVelop.Extensions.GoogleDocs");
         }
         oWriter.WriteElement("URL", StringUtil.RTrim( gxTv_SdtGoogleDocsResult_Doc_Url));
         if ( StringUtil.StrCmp(sNameSpace, "DVelop.Extensions.GoogleDocs") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "DVelop.Extensions.GoogleDocs");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Type", gxTv_SdtGoogleDocsResult_Doc_Type, false);
         AddObjectProperty("Title", gxTv_SdtGoogleDocsResult_Doc_Title, false);
         AddObjectProperty("URL", gxTv_SdtGoogleDocsResult_Doc_Url, false);
         return  ;
      }

      [  SoapElement( ElementName = "Type" )]
      [  XmlElement( ElementName = "Type"   )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtGoogleDocsResult_Doc_Type ;
         }

         set {
            gxTv_SdtGoogleDocsResult_Doc_Type = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Title" )]
      [  XmlElement( ElementName = "Title"   )]
      public String gxTpr_Title
      {
         get {
            return gxTv_SdtGoogleDocsResult_Doc_Title ;
         }

         set {
            gxTv_SdtGoogleDocsResult_Doc_Title = (String)(value);
         }

      }

      [  SoapElement( ElementName = "URL" )]
      [  XmlElement( ElementName = "URL"   )]
      public String gxTpr_Url
      {
         get {
            return gxTv_SdtGoogleDocsResult_Doc_Url ;
         }

         set {
            gxTv_SdtGoogleDocsResult_Doc_Url = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtGoogleDocsResult_Doc_Type = "";
         gxTv_SdtGoogleDocsResult_Doc_Title = "";
         gxTv_SdtGoogleDocsResult_Doc_Url = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtGoogleDocsResult_Doc_Type ;
      protected String gxTv_SdtGoogleDocsResult_Doc_Title ;
      protected String gxTv_SdtGoogleDocsResult_Doc_Url ;
   }

   [DataContract(Name = @"WWPBaseObjects\GoogleDocsResult.Doc", Namespace = "DVelop.Extensions.GoogleDocs")]
   public class SdtGoogleDocsResult_Doc_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtGoogleDocsResult_Doc>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGoogleDocsResult_Doc_RESTInterface( ) : base()
      {
      }

      public SdtGoogleDocsResult_Doc_RESTInterface( wwpbaseobjects.SdtGoogleDocsResult_Doc psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return sdt.gxTpr_Type ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "Title" , Order = 1 )]
      public String gxTpr_Title
      {
         get {
            return sdt.gxTpr_Title ;
         }

         set {
            sdt.gxTpr_Title = (String)(value);
         }

      }

      [DataMember( Name = "URL" , Order = 2 )]
      public String gxTpr_Url
      {
         get {
            return sdt.gxTpr_Url ;
         }

         set {
            sdt.gxTpr_Url = (String)(value);
         }

      }

      public wwpbaseobjects.SdtGoogleDocsResult_Doc sdt
      {
         get {
            return (wwpbaseobjects.SdtGoogleDocsResult_Doc)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtGoogleDocsResult_Doc() ;
         }
      }

   }

}
