/**@preserve  GeneXus C# 10_3_14-114418 on 3/1/2020 17:6:37.25
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.wizardstepsarrowwc', true, function (CmpContext) {
   this.ServerClass =  "wwpbaseobjects.wizardstepsarrowwc" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7StepNumber=gx.fn.getIntegerValue("vSTEPNUMBER",'.') ;
      this.AV5WizardSteps=gx.fn.getControlValue("vWIZARDSTEPS") ;
      this.AV6SelectedStep=gx.fn.getControlValue("vSELECTEDSTEP") ;
      this.AV8PreviousSelected=gx.fn.getControlValue("vPREVIOUSSELECTED") ;
   };
   this.e13hx2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e14hx2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,13,14,16];
   this.GXLastCtrlId =16;
   this.GridwizardstepsContainer = new gx.grid.grid(this, 2,"WbpLvl2",5,"Gridwizardsteps","Gridwizardsteps","GridwizardstepsContainer",this.CmpContext,this.IsMasterPage,"wwpbaseobjects.wizardstepsarrowwc",[],true,0,true,true,0,false,false,false,"CollWWPBaseObjects\WizardSteps.WizardStepsItem",0,"px","Novo registro",false,false,false,null,null,false,"AV5WizardSteps",false,[1,1,1,1]);
   var GridwizardstepsContainer = this.GridwizardstepsContainer;
   GridwizardstepsContainer.startRow("","","","","","");
   GridwizardstepsContainer.startCell("","","","","","","","","","");
   GridwizardstepsContainer.startTable("Tblcontainerstep",8,"0px");
   GridwizardstepsContainer.addHtmlCode("<tbody>");
   GridwizardstepsContainer.startRow("","","","","","");
   GridwizardstepsContainer.startCell("","","","","","","","","","");
   GridwizardstepsContainer.addBitmap("Stepimg","STEPIMG",11,0,"px",0,"px",null,"","","StepImageUnSelected","");
   GridwizardstepsContainer.endCell();
   GridwizardstepsContainer.startCell("","","","","","","","","","TableStepNumberCell");
   GridwizardstepsContainer.startDiv("Tblstep","0px","0px");
   GridwizardstepsContainer.addTextBlock('STEPNUMBER',null);
   GridwizardstepsContainer.endDiv();
   GridwizardstepsContainer.endCell();
   GridwizardstepsContainer.startCell("","","","","","","","","","AttributeStepCell");
   GridwizardstepsContainer.addSingleLineEdit("GXV2",16,"WIZARDSTEPS__TITLE","","","Title","svchar",40,"chr",40,40,"left",null,[],"GXV2","GXV2",true,0,false,false,"",1,"");
   GridwizardstepsContainer.endCell();
   GridwizardstepsContainer.endRow();
   GridwizardstepsContainer.addHtmlCode("</tbody>");
   GridwizardstepsContainer.endTable();
   GridwizardstepsContainer.endCell();
   GridwizardstepsContainer.endRow();
   this.setGrid(GridwizardstepsContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TBLCONTAINERSTEP",grid:5};
   GXValidFnc[13]={fld:"TBLSTEP",grid:5};
   GXValidFnc[14]={fld:"STEPNUMBER", format:0,grid:5};
   GXValidFnc[16]={lvl:2,type:"svchar",len:40,dec:0,sign:false,ro:1,isacc:0,grid:5,gxgrid:this.GridwizardstepsContainer,fnc:null,isvalid:null,rgrid:[],fld:"WIZARDSTEPS__TITLE",gxz:"ZV16GXV2",gxold:"OV16GXV2",gxvar:"GXV2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.GXV2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV16GXV2=Value},v2c:function(row){gx.fn.setGridControlValue("WIZARDSTEPS__TITLE",row || gx.fn.currentGridRowImpl(5),gx.O.GXV2,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV2=this.val()},val:function(row){return gx.fn.getGridControlValue("WIZARDSTEPS__TITLE",row || gx.fn.currentGridRowImpl(5))},nac:gx.falseFn};
   this.AV5WizardSteps = [ ] ;
   this.AV6SelectedStep = "" ;
   this.GXV2 = "" ;
   this.AV7StepNumber = 0 ;
   this.AV8PreviousSelected = false ;
   this.Events = {"e13hx2_client": ["ENTER", true] ,"e14hx2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRIDWIZARDSTEPS_nFirstRecordOnPage',nv:0},{av:'GRIDWIZARDSTEPS_nEOF',nv:0},{av:'AV7StepNumber',fld:'vSTEPNUMBER',pic:'ZZZ9',nv:0},{av:'AV5WizardSteps',fld:'vWIZARDSTEPS',grid:5,pic:'',nv:null},{av:'AV6SelectedStep',fld:'vSELECTEDSTEP',pic:'',nv:''},{av:'AV8PreviousSelected',fld:'vPREVIOUSSELECTED',pic:'',nv:false},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["GRIDWIZARDSTEPS.LOAD"] = [[{av:'AV7StepNumber',fld:'vSTEPNUMBER',pic:'ZZZ9',nv:0},{av:'AV5WizardSteps',fld:'vWIZARDSTEPS',grid:5,pic:'',nv:null},{av:'AV6SelectedStep',fld:'vSELECTEDSTEP',pic:'',nv:''},{av:'AV8PreviousSelected',fld:'vPREVIOUSSELECTED',pic:'',nv:false}],[{av:'gx.fn.getCtrlProperty("STEPNUMBER","Caption")',ctrl:'STEPNUMBER',prop:'Caption'},{ctrl:'WIZARDSTEPS__TITLE',prop:'Class'},{av:'gx.fn.getCtrlProperty("STEPIMG","Class")',ctrl:'STEPIMG',prop:'Class'},{av:'gx.fn.getCtrlProperty("STEPNUMBER","Class")',ctrl:'STEPNUMBER',prop:'Class'},{av:'gx.fn.getCtrlProperty("TBLSTEP","Class")',ctrl:'TBLSTEP',prop:'Class'},{av:'AV8PreviousSelected',fld:'vPREVIOUSSELECTED',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("STEPIMG","Visible")',ctrl:'STEPIMG',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TBLCONTAINERSTEP","Class")',ctrl:'TBLCONTAINERSTEP',prop:'Class'},{av:'AV7StepNumber',fld:'vSTEPNUMBER',pic:'ZZZ9',nv:0}]];
   this.setVCMap("AV7StepNumber", "STEPNUMBER", 0, "int");
   this.setVCMap("AV5WizardSteps", "vWIZARDSTEPS", 0, "CollWWPBaseObjects\WizardSteps.WizardStepsItem");
   this.setVCMap("AV6SelectedStep", "vSELECTEDSTEP", 0, "svchar");
   this.setVCMap("AV8PreviousSelected", "vPREVIOUSSELECTED", 0, "boolean");
   this.setVCMap("AV7StepNumber", "STEPNUMBER", 0, "int");
   this.setVCMap("AV5WizardSteps", "vWIZARDSTEPS", 0, "CollWWPBaseObjects\WizardSteps.WizardStepsItem");
   this.setVCMap("AV6SelectedStep", "vSELECTEDSTEP", 0, "svchar");
   this.setVCMap("AV8PreviousSelected", "vPREVIOUSSELECTED", 0, "boolean");
   GridwizardstepsContainer.addRefreshingVar({rfrVar:"AV7StepNumber"});
   GridwizardstepsContainer.addRefreshingVar({rfrVar:"AV5WizardSteps"});
   GridwizardstepsContainer.addRefreshingVar({rfrVar:"AV6SelectedStep"});
   GridwizardstepsContainer.addRefreshingVar({rfrVar:"AV8PreviousSelected"});
   this.addGridBCProperty("Wizardsteps", ["Title"], this.GXValidFnc[16], "AV5WizardSteps");
   this.InitStandaloneVars( );
});
