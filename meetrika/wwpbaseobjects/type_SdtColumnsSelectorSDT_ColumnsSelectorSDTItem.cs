/*
               File: wwpbaseobjects.type_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem
        Description: WWPBaseObjects\ColumnsSelectorSDT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:7.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ColumnsSelectorSDT.ColumnsSelectorSDTItem" )]
   [XmlType(TypeName =  "ColumnsSelectorSDT.ColumnsSelectorSDTItem" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtColumnsSelectorSDT_ColumnsSelectorSDTItem : GxUserType
   {
      public SdtColumnsSelectorSDT_ColumnsSelectorSDTItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname = "";
      }

      public SdtColumnsSelectorSDT_ColumnsSelectorSDTItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem obj ;
         obj = this;
         obj.gxTpr_Visible = deserialized.gxTpr_Visible;
         obj.gxTpr_Columnname = deserialized.gxTpr_Columnname;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Visible") )
               {
                  gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Visible = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ColumnName") )
               {
                  gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\ColumnsSelectorSDT.ColumnsSelectorSDTItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Visible", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Visible)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ColumnName", StringUtil.RTrim( gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Visible", gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Visible, false);
         AddObjectProperty("ColumnName", gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname, false);
         return  ;
      }

      [  SoapElement( ElementName = "Visible" )]
      [  XmlElement( ElementName = "Visible"   )]
      public bool gxTpr_Visible
      {
         get {
            return gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Visible ;
         }

         set {
            gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Visible = value;
         }

      }

      [  SoapElement( ElementName = "ColumnName" )]
      [  XmlElement( ElementName = "ColumnName"   )]
      public String gxTpr_Columnname
      {
         get {
            return gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname ;
         }

         set {
            gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected bool gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Visible ;
      protected String gxTv_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_Columnname ;
   }

   [DataContract(Name = @"WWPBaseObjects\ColumnsSelectorSDT.ColumnsSelectorSDTItem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_RESTInterface( ) : base()
      {
      }

      public SdtColumnsSelectorSDT_ColumnsSelectorSDTItem_RESTInterface( wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Visible" , Order = 0 )]
      public bool gxTpr_Visible
      {
         get {
            return sdt.gxTpr_Visible ;
         }

         set {
            sdt.gxTpr_Visible = value;
         }

      }

      [DataMember( Name = "ColumnName" , Order = 1 )]
      public String gxTpr_Columnname
      {
         get {
            return sdt.gxTpr_Columnname ;
         }

         set {
            sdt.gxTpr_Columnname = (String)(value);
         }

      }

      public wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem sdt
      {
         get {
            return (wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtColumnsSelectorSDT_ColumnsSelectorSDTItem() ;
         }
      }

   }

}
