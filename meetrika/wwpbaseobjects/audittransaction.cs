/*
               File: WWPBaseObjects.AuditTransaction
        Description: Audit Transaction
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:15.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   public class audittransaction : GXProcedure
   {
      public audittransaction( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public audittransaction( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( wwpbaseobjects.SdtAuditingObject aP0_AuditingObject ,
                           String aP1_CallerName )
      {
         this.AV8AuditingObject = aP0_AuditingObject;
         this.AV11CallerName = aP1_CallerName;
         initialize();
         executePrivate();
      }

      public void executeSubmit( wwpbaseobjects.SdtAuditingObject aP0_AuditingObject ,
                                 String aP1_CallerName )
      {
         audittransaction objaudittransaction;
         objaudittransaction = new audittransaction();
         objaudittransaction.AV8AuditingObject = aP0_AuditingObject;
         objaudittransaction.AV11CallerName = aP1_CallerName;
         objaudittransaction.context.SetSubmitInitialConfig(context);
         objaudittransaction.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaudittransaction);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((audittransaction)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV16WWPContext) ;
         AV17AuditPrimaryKey = "";
         AV18FirstRecord = true;
         AV22GXV1 = 1;
         while ( AV22GXV1 <= AV8AuditingObject.gxTpr_Record.Count )
         {
            AV9AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV8AuditingObject.gxTpr_Record.Item(AV22GXV1));
            AV12Audit = new SdtAudit(context);
            AV12Audit.gxTpr_Auditdate = DateTimeUtil.Now( context);
            if ( AV16WWPContext.gxTpr_Userid > 0 )
            {
               AV12Audit.gxTpr_Usuario_codigo = AV16WWPContext.gxTpr_Userid;
            }
            else
            {
               AV12Audit.gxTv_SdtAudit_Usuario_codigo_SetNull();
            }
            AV12Audit.gxTpr_Audittablename = AV9AuditingObjectRecordItem.gxTpr_Tablename;
            if ( AV18FirstRecord )
            {
               AV14AuditShortDescription = "Registro com chave '";
               AV15AuditDescription = "Registro com chave '";
               AV19ActualMode = AV8AuditingObject.gxTpr_Mode;
            }
            else
            {
               AV14AuditShortDescription = AV17AuditPrimaryKey;
               AV15AuditDescription = AV17AuditPrimaryKey;
               AV19ActualMode = AV9AuditingObjectRecordItem.gxTpr_Mode;
            }
            AV12Audit.gxTpr_Auditaction = AV19ActualMode;
            AV23GXV2 = 1;
            while ( AV23GXV2 <= AV9AuditingObjectRecordItem.gxTpr_Attribute.Count )
            {
               AV10AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV9AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
               if ( AV10AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey )
               {
                  if ( StringUtil.StrCmp(AV19ActualMode, "INS") == 0 )
                  {
                     AV14AuditShortDescription = AV14AuditShortDescription + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Name + " = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue + " ";
                     AV15AuditDescription = AV15AuditDescription + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Name + " = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue + " ";
                  }
                  else
                  {
                     AV14AuditShortDescription = AV14AuditShortDescription + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Name + " = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue + " ";
                     AV15AuditDescription = AV15AuditDescription + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Name + " = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue + " ";
                  }
               }
               AV23GXV2 = (int)(AV23GXV2+1);
            }
            if ( AV18FirstRecord )
            {
               AV18FirstRecord = false;
               AV17AuditPrimaryKey = AV14AuditShortDescription;
            }
            AV14AuditShortDescription = AV14AuditShortDescription + "' foi ";
            AV15AuditDescription = AV15AuditDescription + "' foi ";
            if ( StringUtil.StrCmp(AV19ActualMode, "INS") == 0 )
            {
               AV14AuditShortDescription = AV14AuditShortDescription + "inserido";
               AV15AuditDescription = AV15AuditDescription + "inserido. Campos: ";
            }
            else if ( StringUtil.StrCmp(AV19ActualMode, "UPD") == 0 )
            {
               AV14AuditShortDescription = AV14AuditShortDescription + "atualizado";
               AV15AuditDescription = AV15AuditDescription + "atualizado. Campos Modificados: ";
            }
            else if ( StringUtil.StrCmp(AV19ActualMode, "DLT") == 0 )
            {
               AV14AuditShortDescription = AV14AuditShortDescription + "exclu�dos";
               AV15AuditDescription = AV15AuditDescription + "exclu�dos. Campos: ";
            }
            AV14AuditShortDescription = AV14AuditShortDescription + ".";
            AV24GXV3 = 1;
            while ( AV24GXV3 <= AV9AuditingObjectRecordItem.gxTpr_Attribute.Count )
            {
               AV10AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV9AuditingObjectRecordItem.gxTpr_Attribute.Item(AV24GXV3));
               if ( ! ( AV10AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey ) )
               {
                  if ( StringUtil.StrCmp(AV19ActualMode, "INS") == 0 )
                  {
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue)) )
                     {
                        AV15AuditDescription = AV15AuditDescription + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Name + " = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue + " ";
                     }
                  }
                  else if ( StringUtil.StrCmp(AV19ActualMode, "UPD") == 0 )
                  {
                     if ( StringUtil.StrCmp(AV10AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue, AV10AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue) != 0 )
                     {
                        AV15AuditDescription = AV15AuditDescription + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Name + " = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue + " (Old value = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue + ") ";
                     }
                  }
                  else if ( StringUtil.StrCmp(AV19ActualMode, "DLT") == 0 )
                  {
                     if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV10AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue)) ) )
                     {
                        AV15AuditDescription = AV15AuditDescription + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Name + " = " + AV10AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue + " ";
                     }
                  }
               }
               AV24GXV3 = (int)(AV24GXV3+1);
            }
            AV12Audit.gxTpr_Auditdescription = AV15AuditDescription;
            AV12Audit.gxTpr_Auditshortdescription = AV14AuditShortDescription;
            AV12Audit.Save();
            if ( AV12Audit.Success() )
            {
               context.CommitDataStores( "WWPBaseObjects\\AuditTransaction");
            }
            AV22GXV1 = (int)(AV22GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV16WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV17AuditPrimaryKey = "";
         AV9AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12Audit = new SdtAudit(context);
         AV14AuditShortDescription = "";
         AV15AuditDescription = "";
         AV19ActualMode = "";
         AV10AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwpbaseobjects.audittransaction__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV22GXV1 ;
      private int AV23GXV2 ;
      private int AV24GXV3 ;
      private String AV19ActualMode ;
      private bool AV18FirstRecord ;
      private String AV15AuditDescription ;
      private String AV11CallerName ;
      private String AV17AuditPrimaryKey ;
      private String AV14AuditShortDescription ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private wwpbaseobjects.SdtAuditingObject AV8AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV9AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV10AuditingObjectRecordItemAttributeItem ;
      private SdtAudit AV12Audit ;
      private wwpbaseobjects.SdtWWPContext AV16WWPContext ;
   }

   public class audittransaction__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
