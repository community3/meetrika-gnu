/*
               File: wwpbaseobjects.type_SdtWWPTransactionContext
        Description: WWPBaseObjects\WWPTransactionContext
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 9:44:37.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPTransactionContext" )]
   [XmlType(TypeName =  "WWPTransactionContext" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtWWPTransactionContext_Attribute ))]
   [Serializable]
   public class SdtWWPTransactionContext : GxUserType
   {
      public SdtWWPTransactionContext( )
      {
         /* Constructor for serialization */
         gxTv_SdtWWPTransactionContext_Callerobject = "";
         gxTv_SdtWWPTransactionContext_Callerurl = "";
         gxTv_SdtWWPTransactionContext_Transactionname = "";
      }

      public SdtWWPTransactionContext( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPTransactionContext deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPTransactionContext)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPTransactionContext obj ;
         obj = this;
         obj.gxTpr_Callerobject = deserialized.gxTpr_Callerobject;
         obj.gxTpr_Callerondelete = deserialized.gxTpr_Callerondelete;
         obj.gxTpr_Callerurl = deserialized.gxTpr_Callerurl;
         obj.gxTpr_Transactionname = deserialized.gxTpr_Transactionname;
         obj.gxTpr_Attributes = deserialized.gxTpr_Attributes;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "CallerObject") )
               {
                  gxTv_SdtWWPTransactionContext_Callerobject = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CallerOnDelete") )
               {
                  gxTv_SdtWWPTransactionContext_Callerondelete = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CallerURL") )
               {
                  gxTv_SdtWWPTransactionContext_Callerurl = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TransactionName") )
               {
                  gxTv_SdtWWPTransactionContext_Transactionname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Attributes") )
               {
                  if ( gxTv_SdtWWPTransactionContext_Attributes == null )
                  {
                     gxTv_SdtWWPTransactionContext_Attributes = new GxObjectCollection( context, "WWPTransactionContext.Attribute", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTransactionContext_Attribute", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtWWPTransactionContext_Attributes.readxmlcollection(oReader, "Attributes", "Attribute");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPTransactionContext";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("CallerObject", StringUtil.RTrim( gxTv_SdtWWPTransactionContext_Callerobject));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("CallerOnDelete", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPTransactionContext_Callerondelete)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("CallerURL", StringUtil.RTrim( gxTv_SdtWWPTransactionContext_Callerurl));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TransactionName", StringUtil.RTrim( gxTv_SdtWWPTransactionContext_Transactionname));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtWWPTransactionContext_Attributes != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtWWPTransactionContext_Attributes.writexmlcollection(oWriter, "Attributes", sNameSpace1, "Attribute", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("CallerObject", gxTv_SdtWWPTransactionContext_Callerobject, false);
         AddObjectProperty("CallerOnDelete", gxTv_SdtWWPTransactionContext_Callerondelete, false);
         AddObjectProperty("CallerURL", gxTv_SdtWWPTransactionContext_Callerurl, false);
         AddObjectProperty("TransactionName", gxTv_SdtWWPTransactionContext_Transactionname, false);
         if ( gxTv_SdtWWPTransactionContext_Attributes != null )
         {
            AddObjectProperty("Attributes", gxTv_SdtWWPTransactionContext_Attributes, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "CallerObject" )]
      [  XmlElement( ElementName = "CallerObject"   )]
      public String gxTpr_Callerobject
      {
         get {
            return gxTv_SdtWWPTransactionContext_Callerobject ;
         }

         set {
            gxTv_SdtWWPTransactionContext_Callerobject = (String)(value);
         }

      }

      [  SoapElement( ElementName = "CallerOnDelete" )]
      [  XmlElement( ElementName = "CallerOnDelete"   )]
      public bool gxTpr_Callerondelete
      {
         get {
            return gxTv_SdtWWPTransactionContext_Callerondelete ;
         }

         set {
            gxTv_SdtWWPTransactionContext_Callerondelete = value;
         }

      }

      [  SoapElement( ElementName = "CallerURL" )]
      [  XmlElement( ElementName = "CallerURL"   )]
      public String gxTpr_Callerurl
      {
         get {
            return gxTv_SdtWWPTransactionContext_Callerurl ;
         }

         set {
            gxTv_SdtWWPTransactionContext_Callerurl = (String)(value);
         }

      }

      [  SoapElement( ElementName = "TransactionName" )]
      [  XmlElement( ElementName = "TransactionName"   )]
      public String gxTpr_Transactionname
      {
         get {
            return gxTv_SdtWWPTransactionContext_Transactionname ;
         }

         set {
            gxTv_SdtWWPTransactionContext_Transactionname = (String)(value);
         }

      }

      public class gxTv_SdtWWPTransactionContext_Attributes_wwpbaseobjects_SdtWWPTransactionContext_Attribute_80compatibility:wwpbaseobjects.SdtWWPTransactionContext_Attribute {}
      [  SoapElement( ElementName = "Attributes" )]
      [  XmlArray( ElementName = "Attributes"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtWWPTransactionContext_Attribute ), ElementName= "Attribute"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtWWPTransactionContext_Attributes_wwpbaseobjects_SdtWWPTransactionContext_Attribute_80compatibility ), ElementName= "WWPTransactionContext.Attribute"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Attributes_GxObjectCollection
      {
         get {
            if ( gxTv_SdtWWPTransactionContext_Attributes == null )
            {
               gxTv_SdtWWPTransactionContext_Attributes = new GxObjectCollection( context, "WWPTransactionContext.Attribute", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTransactionContext_Attribute", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtWWPTransactionContext_Attributes ;
         }

         set {
            if ( gxTv_SdtWWPTransactionContext_Attributes == null )
            {
               gxTv_SdtWWPTransactionContext_Attributes = new GxObjectCollection( context, "WWPTransactionContext.Attribute", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTransactionContext_Attribute", "GeneXus.Programs");
            }
            gxTv_SdtWWPTransactionContext_Attributes = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Attributes
      {
         get {
            if ( gxTv_SdtWWPTransactionContext_Attributes == null )
            {
               gxTv_SdtWWPTransactionContext_Attributes = new GxObjectCollection( context, "WWPTransactionContext.Attribute", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTransactionContext_Attribute", "GeneXus.Programs");
            }
            return gxTv_SdtWWPTransactionContext_Attributes ;
         }

         set {
            gxTv_SdtWWPTransactionContext_Attributes = value;
         }

      }

      public void gxTv_SdtWWPTransactionContext_Attributes_SetNull( )
      {
         gxTv_SdtWWPTransactionContext_Attributes = null;
         return  ;
      }

      public bool gxTv_SdtWWPTransactionContext_Attributes_IsNull( )
      {
         if ( gxTv_SdtWWPTransactionContext_Attributes == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtWWPTransactionContext_Callerobject = "";
         gxTv_SdtWWPTransactionContext_Callerurl = "";
         gxTv_SdtWWPTransactionContext_Transactionname = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected bool gxTv_SdtWWPTransactionContext_Callerondelete ;
      protected String gxTv_SdtWWPTransactionContext_Callerobject ;
      protected String gxTv_SdtWWPTransactionContext_Callerurl ;
      protected String gxTv_SdtWWPTransactionContext_Transactionname ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTransactionContext_Attribute ))]
      protected IGxCollection gxTv_SdtWWPTransactionContext_Attributes=null ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPTransactionContext", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtWWPTransactionContext_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPTransactionContext>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPTransactionContext_RESTInterface( ) : base()
      {
      }

      public SdtWWPTransactionContext_RESTInterface( wwpbaseobjects.SdtWWPTransactionContext psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "CallerObject" , Order = 0 )]
      public String gxTpr_Callerobject
      {
         get {
            return sdt.gxTpr_Callerobject ;
         }

         set {
            sdt.gxTpr_Callerobject = (String)(value);
         }

      }

      [DataMember( Name = "CallerOnDelete" , Order = 1 )]
      public bool gxTpr_Callerondelete
      {
         get {
            return sdt.gxTpr_Callerondelete ;
         }

         set {
            sdt.gxTpr_Callerondelete = value;
         }

      }

      [DataMember( Name = "CallerURL" , Order = 2 )]
      public String gxTpr_Callerurl
      {
         get {
            return sdt.gxTpr_Callerurl ;
         }

         set {
            sdt.gxTpr_Callerurl = (String)(value);
         }

      }

      [DataMember( Name = "TransactionName" , Order = 3 )]
      public String gxTpr_Transactionname
      {
         get {
            return sdt.gxTpr_Transactionname ;
         }

         set {
            sdt.gxTpr_Transactionname = (String)(value);
         }

      }

      [DataMember( Name = "Attributes" , Order = 4 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPTransactionContext_Attribute_RESTInterface> gxTpr_Attributes
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtWWPTransactionContext_Attribute_RESTInterface>(sdt.gxTpr_Attributes) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Attributes);
         }

      }

      public wwpbaseobjects.SdtWWPTransactionContext sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPTransactionContext)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPTransactionContext() ;
         }
      }

   }

}
