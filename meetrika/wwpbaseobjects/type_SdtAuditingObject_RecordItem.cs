/*
               File: wwpbaseobjects.type_SdtAuditingObject_RecordItem
        Description: WWPBaseObjects\AuditingObject
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 9:44:36.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "AuditingObject.RecordItem" )]
   [XmlType(TypeName =  "AuditingObject.RecordItem" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem ))]
   [Serializable]
   public class SdtAuditingObject_RecordItem : GxUserType
   {
      public SdtAuditingObject_RecordItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtAuditingObject_RecordItem_Tablename = "";
         gxTv_SdtAuditingObject_RecordItem_Mode = "";
      }

      public SdtAuditingObject_RecordItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtAuditingObject_RecordItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtAuditingObject_RecordItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtAuditingObject_RecordItem obj ;
         obj = this;
         obj.gxTpr_Tablename = deserialized.gxTpr_Tablename;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Attribute = deserialized.gxTpr_Attribute;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "TableName") )
               {
                  gxTv_SdtAuditingObject_RecordItem_Tablename = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAuditingObject_RecordItem_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Attribute") )
               {
                  if ( gxTv_SdtAuditingObject_RecordItem_Attribute == null )
                  {
                     gxTv_SdtAuditingObject_RecordItem_Attribute = new GxObjectCollection( context, "AuditingObject.RecordItem.AttributeItem", "", "wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtAuditingObject_RecordItem_Attribute.readxmlcollection(oReader, "Attribute", "AttributeItem");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\AuditingObject.RecordItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("TableName", StringUtil.RTrim( gxTv_SdtAuditingObject_RecordItem_Tablename));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAuditingObject_RecordItem_Mode));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtAuditingObject_RecordItem_Attribute != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtAuditingObject_RecordItem_Attribute.writexmlcollection(oWriter, "Attribute", sNameSpace1, "AttributeItem", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("TableName", gxTv_SdtAuditingObject_RecordItem_Tablename, false);
         AddObjectProperty("Mode", gxTv_SdtAuditingObject_RecordItem_Mode, false);
         if ( gxTv_SdtAuditingObject_RecordItem_Attribute != null )
         {
            AddObjectProperty("Attribute", gxTv_SdtAuditingObject_RecordItem_Attribute, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "TableName" )]
      [  XmlElement( ElementName = "TableName"   )]
      public String gxTpr_Tablename
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_Tablename ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_Tablename = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_Mode ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_Mode = (String)(value);
         }

      }

      public class gxTv_SdtAuditingObject_RecordItem_Attribute_wwpbaseobjects_SdtAuditingObject_RecordItem_AttributeItem_80compatibility:wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem {}
      [  SoapElement( ElementName = "Attribute" )]
      [  XmlArray( ElementName = "Attribute"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem ), ElementName= "AttributeItem"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtAuditingObject_RecordItem_Attribute_wwpbaseobjects_SdtAuditingObject_RecordItem_AttributeItem_80compatibility ), ElementName= "AuditingObject.RecordItem.AttributeItem"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Attribute_GxObjectCollection
      {
         get {
            if ( gxTv_SdtAuditingObject_RecordItem_Attribute == null )
            {
               gxTv_SdtAuditingObject_RecordItem_Attribute = new GxObjectCollection( context, "AuditingObject.RecordItem.AttributeItem", "", "wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtAuditingObject_RecordItem_Attribute ;
         }

         set {
            if ( gxTv_SdtAuditingObject_RecordItem_Attribute == null )
            {
               gxTv_SdtAuditingObject_RecordItem_Attribute = new GxObjectCollection( context, "AuditingObject.RecordItem.AttributeItem", "", "wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem", "GeneXus.Programs");
            }
            gxTv_SdtAuditingObject_RecordItem_Attribute = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Attribute
      {
         get {
            if ( gxTv_SdtAuditingObject_RecordItem_Attribute == null )
            {
               gxTv_SdtAuditingObject_RecordItem_Attribute = new GxObjectCollection( context, "AuditingObject.RecordItem.AttributeItem", "", "wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem", "GeneXus.Programs");
            }
            return gxTv_SdtAuditingObject_RecordItem_Attribute ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_Attribute = value;
         }

      }

      public void gxTv_SdtAuditingObject_RecordItem_Attribute_SetNull( )
      {
         gxTv_SdtAuditingObject_RecordItem_Attribute = null;
         return  ;
      }

      public bool gxTv_SdtAuditingObject_RecordItem_Attribute_IsNull( )
      {
         if ( gxTv_SdtAuditingObject_RecordItem_Attribute == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAuditingObject_RecordItem_Tablename = "";
         gxTv_SdtAuditingObject_RecordItem_Mode = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtAuditingObject_RecordItem_Mode ;
      protected String sTagName ;
      protected String gxTv_SdtAuditingObject_RecordItem_Tablename ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem ))]
      protected IGxCollection gxTv_SdtAuditingObject_RecordItem_Attribute=null ;
   }

   [DataContract(Name = @"WWPBaseObjects\AuditingObject.RecordItem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtAuditingObject_RecordItem_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAuditingObject_RecordItem_RESTInterface( ) : base()
      {
      }

      public SdtAuditingObject_RecordItem_RESTInterface( wwpbaseobjects.SdtAuditingObject_RecordItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "TableName" , Order = 0 )]
      public String gxTpr_Tablename
      {
         get {
            return sdt.gxTpr_Tablename ;
         }

         set {
            sdt.gxTpr_Tablename = (String)(value);
         }

      }

      [DataMember( Name = "Mode" , Order = 1 )]
      public String gxTpr_Mode
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Mode) ;
         }

         set {
            sdt.gxTpr_Mode = (String)(value);
         }

      }

      [DataMember( Name = "Attribute" , Order = 2 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem_RESTInterface> gxTpr_Attribute
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem_RESTInterface>(sdt.gxTpr_Attribute) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Attribute);
         }

      }

      public wwpbaseobjects.SdtAuditingObject_RecordItem sdt
      {
         get {
            return (wwpbaseobjects.SdtAuditingObject_RecordItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem() ;
         }
      }

   }

}
