/*
               File: wwpbaseobjects.type_SdtAuditingObject_RecordItem_AttributeItem
        Description: WWPBaseObjects\AuditingObject
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 9:44:37.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "AuditingObject.RecordItem.AttributeItem" )]
   [XmlType(TypeName =  "AuditingObject.RecordItem.AttributeItem" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtAuditingObject_RecordItem_AttributeItem : GxUserType
   {
      public SdtAuditingObject_RecordItem_AttributeItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name = "";
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue = "";
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue = "";
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description = "";
      }

      public SdtAuditingObject_RecordItem_AttributeItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Ispartofkey = deserialized.gxTpr_Ispartofkey;
         obj.gxTpr_Isdescriptionattribute = deserialized.gxTpr_Isdescriptionattribute;
         obj.gxTpr_Oldvalue = deserialized.gxTpr_Oldvalue;
         obj.gxTpr_Newvalue = deserialized.gxTpr_Newvalue;
         obj.gxTpr_Description = deserialized.gxTpr_Description;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "IsPartOfKey") )
               {
                  gxTv_SdtAuditingObject_RecordItem_AttributeItem_Ispartofkey = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "IsDescriptionAttribute") )
               {
                  gxTv_SdtAuditingObject_RecordItem_AttributeItem_Isdescriptionattribute = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OldValue") )
               {
                  gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NewValue") )
               {
                  gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Description") )
               {
                  gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\AuditingObject.RecordItem.AttributeItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("IsPartOfKey", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAuditingObject_RecordItem_AttributeItem_Ispartofkey)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("IsDescriptionAttribute", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAuditingObject_RecordItem_AttributeItem_Isdescriptionattribute)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("OldValue", StringUtil.RTrim( gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("NewValue", StringUtil.RTrim( gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Description", StringUtil.RTrim( gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name, false);
         AddObjectProperty("IsPartOfKey", gxTv_SdtAuditingObject_RecordItem_AttributeItem_Ispartofkey, false);
         AddObjectProperty("IsDescriptionAttribute", gxTv_SdtAuditingObject_RecordItem_AttributeItem_Isdescriptionattribute, false);
         AddObjectProperty("OldValue", gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue, false);
         AddObjectProperty("NewValue", gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue, false);
         AddObjectProperty("Description", gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description, false);
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "IsPartOfKey" )]
      [  XmlElement( ElementName = "IsPartOfKey"   )]
      public bool gxTpr_Ispartofkey
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_AttributeItem_Ispartofkey ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_AttributeItem_Ispartofkey = value;
         }

      }

      [  SoapElement( ElementName = "IsDescriptionAttribute" )]
      [  XmlElement( ElementName = "IsDescriptionAttribute"   )]
      public bool gxTpr_Isdescriptionattribute
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_AttributeItem_Isdescriptionattribute ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_AttributeItem_Isdescriptionattribute = value;
         }

      }

      [  SoapElement( ElementName = "OldValue" )]
      [  XmlElement( ElementName = "OldValue"   )]
      public String gxTpr_Oldvalue
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue = (String)(value);
         }

      }

      [  SoapElement( ElementName = "NewValue" )]
      [  XmlElement( ElementName = "NewValue"   )]
      public String gxTpr_Newvalue
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Description" )]
      [  XmlElement( ElementName = "Description"   )]
      public String gxTpr_Description
      {
         get {
            return gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description ;
         }

         set {
            gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name = "";
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue = "";
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue = "";
         gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected bool gxTv_SdtAuditingObject_RecordItem_AttributeItem_Ispartofkey ;
      protected bool gxTv_SdtAuditingObject_RecordItem_AttributeItem_Isdescriptionattribute ;
      protected String gxTv_SdtAuditingObject_RecordItem_AttributeItem_Name ;
      protected String gxTv_SdtAuditingObject_RecordItem_AttributeItem_Oldvalue ;
      protected String gxTv_SdtAuditingObject_RecordItem_AttributeItem_Newvalue ;
      protected String gxTv_SdtAuditingObject_RecordItem_AttributeItem_Description ;
   }

   [DataContract(Name = @"WWPBaseObjects\AuditingObject.RecordItem.AttributeItem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtAuditingObject_RecordItem_AttributeItem_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAuditingObject_RecordItem_AttributeItem_RESTInterface( ) : base()
      {
      }

      public SdtAuditingObject_RecordItem_AttributeItem_RESTInterface( wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return sdt.gxTpr_Name ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "IsPartOfKey" , Order = 1 )]
      public bool gxTpr_Ispartofkey
      {
         get {
            return sdt.gxTpr_Ispartofkey ;
         }

         set {
            sdt.gxTpr_Ispartofkey = value;
         }

      }

      [DataMember( Name = "IsDescriptionAttribute" , Order = 2 )]
      public bool gxTpr_Isdescriptionattribute
      {
         get {
            return sdt.gxTpr_Isdescriptionattribute ;
         }

         set {
            sdt.gxTpr_Isdescriptionattribute = value;
         }

      }

      [DataMember( Name = "OldValue" , Order = 3 )]
      public String gxTpr_Oldvalue
      {
         get {
            return sdt.gxTpr_Oldvalue ;
         }

         set {
            sdt.gxTpr_Oldvalue = (String)(value);
         }

      }

      [DataMember( Name = "NewValue" , Order = 4 )]
      public String gxTpr_Newvalue
      {
         get {
            return sdt.gxTpr_Newvalue ;
         }

         set {
            sdt.gxTpr_Newvalue = (String)(value);
         }

      }

      [DataMember( Name = "Description" , Order = 5 )]
      public String gxTpr_Description
      {
         get {
            return sdt.gxTpr_Description ;
         }

         set {
            sdt.gxTpr_Description = (String)(value);
         }

      }

      public wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem sdt
      {
         get {
            return (wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem() ;
         }
      }

   }

}
