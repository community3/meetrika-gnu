/**@preserve  GeneXus C# 10_3_14-114418 on 3/1/2020 17:30:31.91
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.savefilteras', false, function () {
   this.ServerClass =  "wwpbaseobjects.savefilteras" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV13UserKey=gx.fn.getControlValue("vUSERKEY") ;
      this.AV16GridStateKey=gx.fn.getControlValue("vGRIDSTATEKEY") ;
   };
   this.e12082_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e14081_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,16,24,29];
   this.GXLastCtrlId =29;
   this.DVPANEL_UNNAMEDTABLE1Container = gx.uc.getNew(this, 27, 13, "BootstrapPanel", "DVPANEL_UNNAMEDTABLE1Container", "Dvpanel_unnamedtable1");
   var DVPANEL_UNNAMEDTABLE1Container = this.DVPANEL_UNNAMEDTABLE1Container;
   DVPANEL_UNNAMEDTABLE1Container.setProp("Width", "Width", "100%", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Height", "Height", "100", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Title", "Title", "", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Collapsible", "Collapsible", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_UNNAMEDTABLE1Container.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Visible", "Visible", true, "bool");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_UNNAMEDTABLE1Container.setProp("Class", "Class", "", "char");
   DVPANEL_UNNAMEDTABLE1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_UNNAMEDTABLE1Container);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[11]={fld:"TEXTBLOCKFILTERNAME", format:0,grid:0};
   GXValidFnc[13]={lvl:0,type:"char",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vFILTERNAME",gxz:"ZV6FilterName",gxold:"OV6FilterName",gxvar:"AV6FilterName",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV6FilterName=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV6FilterName=Value},v2c:function(){gx.fn.setControlValue("vFILTERNAME",gx.O.AV6FilterName,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV6FilterName=this.val()},val:function(){return gx.fn.getControlValue("vFILTERNAME")},nac:gx.falseFn};
   this.declareDomainHdlr( 13 , function() {
   });
   GXValidFnc[16]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[24]={fld:"DUMMYTABLETOIMPORTBOOTSTRAPCSS",grid:0};
   GXValidFnc[29]={fld:"UNNAMEDTABLE1",grid:0};
   this.AV6FilterName = "" ;
   this.ZV6FilterName = "" ;
   this.OV6FilterName = "" ;
   this.AV6FilterName = "" ;
   this.AV13UserKey = "" ;
   this.AV16GridStateKey = "" ;
   this.Events = {"e12082_client": ["ENTER", true] ,"e14081_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["ENTER"] = [[{av:'AV6FilterName',fld:'vFILTERNAME',pic:'',nv:''},{av:'AV13UserKey',fld:'vUSERKEY',pic:'',hsh:true,nv:''},{av:'AV16GridStateKey',fld:'vGRIDSTATEKEY',pic:'',hsh:true,nv:''}],[{av:'AV6FilterName',fld:'vFILTERNAME',pic:'',nv:''}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV13UserKey", "vUSERKEY", 0, "svchar");
   this.setVCMap("AV16GridStateKey", "vGRIDSTATEKEY", 0, "svchar");
   this.InitStandaloneVars( );
});
gx.createParentObj(wwpbaseobjects.savefilteras);
