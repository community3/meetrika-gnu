/*
               File: wwpbaseobjects.type_SdtWWPGridState_FilterValue
        Description: WWPBaseObjects\WWPGridState
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:57.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPGridState.FilterValue" )]
   [XmlType(TypeName =  "WWPGridState.FilterValue" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtWWPGridState_FilterValue : GxUserType
   {
      public SdtWWPGridState_FilterValue( )
      {
         /* Constructor for serialization */
         gxTv_SdtWWPGridState_FilterValue_Name = "";
         gxTv_SdtWWPGridState_FilterValue_Name_N = 1;
         gxTv_SdtWWPGridState_FilterValue_Value = "";
         gxTv_SdtWWPGridState_FilterValue_Valueto = "";
      }

      public SdtWWPGridState_FilterValue( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPGridState_FilterValue deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPGridState_FilterValue)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPGridState_FilterValue obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Value = deserialized.gxTpr_Value;
         obj.gxTpr_Operator = deserialized.gxTpr_Operator;
         obj.gxTpr_Valueto = deserialized.gxTpr_Valueto;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtWWPGridState_FilterValue_Name = oReader.Value;
                  gxTv_SdtWWPGridState_FilterValue_Name_N = 0;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value") )
               {
                  gxTv_SdtWWPGridState_FilterValue_Value = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Operator") )
               {
                  gxTv_SdtWWPGridState_FilterValue_Operator = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ValueTo") )
               {
                  gxTv_SdtWWPGridState_FilterValue_Valueto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\WWPGridState.FilterValue";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( gxTv_SdtWWPGridState_FilterValue_Name)) || ( gxTv_SdtWWPGridState_FilterValue_Name_N != 1 ) )
         {
            oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtWWPGridState_FilterValue_Name));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Value", StringUtil.RTrim( gxTv_SdtWWPGridState_FilterValue_Value));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Operator", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPGridState_FilterValue_Operator), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ValueTo", StringUtil.RTrim( gxTv_SdtWWPGridState_FilterValue_Valueto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtWWPGridState_FilterValue_Name, false);
         AddObjectProperty("Value", gxTv_SdtWWPGridState_FilterValue_Value, false);
         AddObjectProperty("Operator", gxTv_SdtWWPGridState_FilterValue_Operator, false);
         AddObjectProperty("ValueTo", gxTv_SdtWWPGridState_FilterValue_Valueto, false);
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtWWPGridState_FilterValue_Name ;
         }

         set {
            gxTv_SdtWWPGridState_FilterValue_Name_N = 0;
            gxTv_SdtWWPGridState_FilterValue_Name = (String)(value);
         }

      }

      public bool ShouldSerializegxTpr_Name( )
      {
         return (bool)(gxTv_SdtWWPGridState_FilterValue_Name_N == 0) ;
      }

      [  SoapElement( ElementName = "Value" )]
      [  XmlElement( ElementName = "Value"   )]
      public String gxTpr_Value
      {
         get {
            return gxTv_SdtWWPGridState_FilterValue_Value ;
         }

         set {
            gxTv_SdtWWPGridState_FilterValue_Value = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Operator" )]
      [  XmlElement( ElementName = "Operator"   )]
      public short gxTpr_Operator
      {
         get {
            return gxTv_SdtWWPGridState_FilterValue_Operator ;
         }

         set {
            gxTv_SdtWWPGridState_FilterValue_Operator = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ValueTo" )]
      [  XmlElement( ElementName = "ValueTo"   )]
      public String gxTpr_Valueto
      {
         get {
            return gxTv_SdtWWPGridState_FilterValue_Valueto ;
         }

         set {
            gxTv_SdtWWPGridState_FilterValue_Valueto = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtWWPGridState_FilterValue_Name = "";
         gxTv_SdtWWPGridState_FilterValue_Name_N = 1;
         gxTv_SdtWWPGridState_FilterValue_Value = "";
         gxTv_SdtWWPGridState_FilterValue_Valueto = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtWWPGridState_FilterValue_Name_N ;
      protected short gxTv_SdtWWPGridState_FilterValue_Operator ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtWWPGridState_FilterValue_Name ;
      protected String gxTv_SdtWWPGridState_FilterValue_Value ;
      protected String gxTv_SdtWWPGridState_FilterValue_Valueto ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPGridState.FilterValue", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtWWPGridState_FilterValue_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_FilterValue>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPGridState_FilterValue_RESTInterface( ) : base()
      {
      }

      public SdtWWPGridState_FilterValue_RESTInterface( wwpbaseobjects.SdtWWPGridState_FilterValue psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return sdt.gxTpr_Name ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Value" , Order = 1 )]
      public String gxTpr_Value
      {
         get {
            return sdt.gxTpr_Value ;
         }

         set {
            sdt.gxTpr_Value = (String)(value);
         }

      }

      [DataMember( Name = "Operator" , Order = 2 )]
      public Nullable<short> gxTpr_Operator
      {
         get {
            return sdt.gxTpr_Operator ;
         }

         set {
            sdt.gxTpr_Operator = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ValueTo" , Order = 3 )]
      public String gxTpr_Valueto
      {
         get {
            return sdt.gxTpr_Valueto ;
         }

         set {
            sdt.gxTpr_Valueto = (String)(value);
         }

      }

      public wwpbaseobjects.SdtWWPGridState_FilterValue sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPGridState_FilterValue)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPGridState_FilterValue() ;
         }
      }

   }

}
