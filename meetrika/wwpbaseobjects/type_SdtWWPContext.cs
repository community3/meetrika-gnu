/*
               File: wwpbaseobjects.type_SdtWWPContext
        Description: WWPBaseObjects\WWPContext
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 9:44:37.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "WWPContext" )]
   [XmlType(TypeName =  "WWPContext" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtWWPContext : GxUserType
   {
      public SdtWWPContext( )
      {
         /* Constructor for serialization */
         gxTv_SdtWWPContext_Username = "";
         gxTv_SdtWWPContext_Servicosgeridos = "";
         gxTv_SdtWWPContext_Areatrabalho_descricao = "";
         gxTv_SdtWWPContext_Contratante_razaosocial = "";
         gxTv_SdtWWPContext_Contratante_nomefantasia = "";
         gxTv_SdtWWPContext_Contratante_cnpj = "";
         gxTv_SdtWWPContext_Contratante_telefone = "";
         gxTv_SdtWWPContext_Contratada_pessoanom = "";
         gxTv_SdtWWPContext_Parametrossistema_nomesistema = "";
         gxTv_SdtWWPContext_Calculopfinal = "";
         gxTv_SdtWWPContext_Pathcertificacao = "";
         gxTv_SdtWWPContext_Usuario_email = "";
         gxTv_SdtWWPContext_Validationkey = "";
      }

      public SdtWWPContext( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtWWPContext deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtWWPContext)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtWWPContext obj ;
         obj = this;
         obj.gxTpr_Userid = deserialized.gxTpr_Userid;
         obj.gxTpr_Username = deserialized.gxTpr_Username;
         obj.gxTpr_Userehcontador = deserialized.gxTpr_Userehcontador;
         obj.gxTpr_Userehauditorfm = deserialized.gxTpr_Userehauditorfm;
         obj.gxTpr_Userehcontratada = deserialized.gxTpr_Userehcontratada;
         obj.gxTpr_Userehcontratante = deserialized.gxTpr_Userehcontratante;
         obj.gxTpr_Userehfinanceiro = deserialized.gxTpr_Userehfinanceiro;
         obj.gxTpr_Userehlicenciado = deserialized.gxTpr_Userehlicenciado;
         obj.gxTpr_Userehgestor = deserialized.gxTpr_Userehgestor;
         obj.gxTpr_Servicosgeridos = deserialized.gxTpr_Servicosgeridos;
         obj.gxTpr_Userehadministradorgam = deserialized.gxTpr_Userehadministradorgam;
         obj.gxTpr_Areatrabalho_codigo = deserialized.gxTpr_Areatrabalho_codigo;
         obj.gxTpr_Areatrabalho_descricao = deserialized.gxTpr_Areatrabalho_descricao;
         obj.gxTpr_Contratante_codigo = deserialized.gxTpr_Contratante_codigo;
         obj.gxTpr_Contratante_razaosocial = deserialized.gxTpr_Contratante_razaosocial;
         obj.gxTpr_Contratante_nomefantasia = deserialized.gxTpr_Contratante_nomefantasia;
         obj.gxTpr_Contratante_cnpj = deserialized.gxTpr_Contratante_cnpj;
         obj.gxTpr_Contratante_telefone = deserialized.gxTpr_Contratante_telefone;
         obj.gxTpr_Contratada_codigo = deserialized.gxTpr_Contratada_codigo;
         obj.gxTpr_Contratada_pessoacod = deserialized.gxTpr_Contratada_pessoacod;
         obj.gxTpr_Contratada_pessoanom = deserialized.gxTpr_Contratada_pessoanom;
         obj.gxTpr_Parametrossistema_nomesistema = deserialized.gxTpr_Parametrossistema_nomesistema;
         obj.gxTpr_Parametrossistema_appid = deserialized.gxTpr_Parametrossistema_appid;
         obj.gxTpr_Updcomboareatrabalho = deserialized.gxTpr_Updcomboareatrabalho;
         obj.gxTpr_Servicopadrao = deserialized.gxTpr_Servicopadrao;
         obj.gxTpr_Calculopfinal = deserialized.gxTpr_Calculopfinal;
         obj.gxTpr_Insert = deserialized.gxTpr_Insert;
         obj.gxTpr_Update = deserialized.gxTpr_Update;
         obj.gxTpr_Delete = deserialized.gxTpr_Delete;
         obj.gxTpr_Display = deserialized.gxTpr_Display;
         obj.gxTpr_Pathcertificacao = deserialized.gxTpr_Pathcertificacao;
         obj.gxTpr_Organizacao_codigo = deserialized.gxTpr_Organizacao_codigo;
         obj.gxTpr_Usuario_email = deserialized.gxTpr_Usuario_email;
         obj.gxTpr_Screen_height = deserialized.gxTpr_Screen_height;
         obj.gxTpr_Screen_width = deserialized.gxTpr_Screen_width;
         obj.gxTpr_Validationkey = deserialized.gxTpr_Validationkey;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserId") )
               {
                  gxTv_SdtWWPContext_Userid = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserName") )
               {
                  gxTv_SdtWWPContext_Username = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhContador") )
               {
                  gxTv_SdtWWPContext_Userehcontador = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhAuditorFM") )
               {
                  gxTv_SdtWWPContext_Userehauditorfm = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhContratada") )
               {
                  gxTv_SdtWWPContext_Userehcontratada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhContratante") )
               {
                  gxTv_SdtWWPContext_Userehcontratante = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhFinanceiro") )
               {
                  gxTv_SdtWWPContext_Userehfinanceiro = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhLicenciado") )
               {
                  gxTv_SdtWWPContext_Userehlicenciado = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhGestor") )
               {
                  gxTv_SdtWWPContext_Userehgestor = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicosGeridos") )
               {
                  gxTv_SdtWWPContext_Servicosgeridos = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserEhAdministradorGAM") )
               {
                  gxTv_SdtWWPContext_Userehadministradorgam = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo") )
               {
                  gxTv_SdtWWPContext_Areatrabalho_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Descricao") )
               {
                  gxTv_SdtWWPContext_Areatrabalho_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo") )
               {
                  gxTv_SdtWWPContext_Contratante_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial") )
               {
                  gxTv_SdtWWPContext_Contratante_razaosocial = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_NomeFantasia") )
               {
                  gxTv_SdtWWPContext_Contratante_nomefantasia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_CNPJ") )
               {
                  gxTv_SdtWWPContext_Contratante_cnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Telefone") )
               {
                  gxTv_SdtWWPContext_Contratante_telefone = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo") )
               {
                  gxTv_SdtWWPContext_Contratada_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaCod") )
               {
                  gxTv_SdtWWPContext_Contratada_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom") )
               {
                  gxTv_SdtWWPContext_Contratada_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_NomeSistema") )
               {
                  gxTv_SdtWWPContext_Parametrossistema_nomesistema = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_AppID") )
               {
                  gxTv_SdtWWPContext_Parametrossistema_appid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UpdComboAreaTrabalho") )
               {
                  gxTv_SdtWWPContext_Updcomboareatrabalho = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoPadrao") )
               {
                  gxTv_SdtWWPContext_Servicopadrao = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CalculoPFinal") )
               {
                  gxTv_SdtWWPContext_Calculopfinal = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Insert") )
               {
                  gxTv_SdtWWPContext_Insert = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Update") )
               {
                  gxTv_SdtWWPContext_Update = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Delete") )
               {
                  gxTv_SdtWWPContext_Delete = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Display") )
               {
                  gxTv_SdtWWPContext_Display = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PathCertificacao") )
               {
                  gxTv_SdtWWPContext_Pathcertificacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Organizacao_Codigo") )
               {
                  gxTv_SdtWWPContext_Organizacao_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Email") )
               {
                  gxTv_SdtWWPContext_Usuario_email = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "screen_height") )
               {
                  gxTv_SdtWWPContext_Screen_height = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "screen_width") )
               {
                  gxTv_SdtWWPContext_Screen_width = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ValidationKey") )
               {
                  gxTv_SdtWWPContext_Validationkey = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPContext";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("UserId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Userid), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserName", StringUtil.RTrim( gxTv_SdtWWPContext_Username));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhContador", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehcontador)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhAuditorFM", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehauditorfm)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhContratada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehcontratada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhContratante", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehcontratante)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhFinanceiro", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehfinanceiro)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhLicenciado", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehlicenciado)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhGestor", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehgestor)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicosGeridos", StringUtil.RTrim( gxTv_SdtWWPContext_Servicosgeridos));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UserEhAdministradorGAM", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Userehadministradorgam)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Areatrabalho_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_Descricao", StringUtil.RTrim( gxTv_SdtWWPContext_Areatrabalho_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Contratante_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_RazaoSocial", StringUtil.RTrim( gxTv_SdtWWPContext_Contratante_razaosocial));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_NomeFantasia", StringUtil.RTrim( gxTv_SdtWWPContext_Contratante_nomefantasia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_CNPJ", StringUtil.RTrim( gxTv_SdtWWPContext_Contratante_cnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_Telefone", StringUtil.RTrim( gxTv_SdtWWPContext_Contratante_telefone));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Contratada_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Contratada_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_PessoaNom", StringUtil.RTrim( gxTv_SdtWWPContext_Contratada_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ParametrosSistema_NomeSistema", StringUtil.RTrim( gxTv_SdtWWPContext_Parametrossistema_nomesistema));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ParametrosSistema_AppID", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Parametrossistema_appid), 12, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("UpdComboAreaTrabalho", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Updcomboareatrabalho)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoPadrao", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Servicopadrao), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("CalculoPFinal", StringUtil.RTrim( gxTv_SdtWWPContext_Calculopfinal));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Insert", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Insert)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Update", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Update)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Delete", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Delete)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Display", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtWWPContext_Display)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PathCertificacao", StringUtil.RTrim( gxTv_SdtWWPContext_Pathcertificacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Organizacao_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Organizacao_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Usuario_Email", StringUtil.RTrim( gxTv_SdtWWPContext_Usuario_email));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("screen_height", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Screen_height), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("screen_width", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtWWPContext_Screen_width), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ValidationKey", StringUtil.RTrim( gxTv_SdtWWPContext_Validationkey));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("UserId", gxTv_SdtWWPContext_Userid, false);
         AddObjectProperty("UserName", gxTv_SdtWWPContext_Username, false);
         AddObjectProperty("UserEhContador", gxTv_SdtWWPContext_Userehcontador, false);
         AddObjectProperty("UserEhAuditorFM", gxTv_SdtWWPContext_Userehauditorfm, false);
         AddObjectProperty("UserEhContratada", gxTv_SdtWWPContext_Userehcontratada, false);
         AddObjectProperty("UserEhContratante", gxTv_SdtWWPContext_Userehcontratante, false);
         AddObjectProperty("UserEhFinanceiro", gxTv_SdtWWPContext_Userehfinanceiro, false);
         AddObjectProperty("UserEhLicenciado", gxTv_SdtWWPContext_Userehlicenciado, false);
         AddObjectProperty("UserEhGestor", gxTv_SdtWWPContext_Userehgestor, false);
         AddObjectProperty("ServicosGeridos", gxTv_SdtWWPContext_Servicosgeridos, false);
         AddObjectProperty("UserEhAdministradorGAM", gxTv_SdtWWPContext_Userehadministradorgam, false);
         AddObjectProperty("AreaTrabalho_Codigo", gxTv_SdtWWPContext_Areatrabalho_codigo, false);
         AddObjectProperty("AreaTrabalho_Descricao", gxTv_SdtWWPContext_Areatrabalho_descricao, false);
         AddObjectProperty("Contratante_Codigo", gxTv_SdtWWPContext_Contratante_codigo, false);
         AddObjectProperty("Contratante_RazaoSocial", gxTv_SdtWWPContext_Contratante_razaosocial, false);
         AddObjectProperty("Contratante_NomeFantasia", gxTv_SdtWWPContext_Contratante_nomefantasia, false);
         AddObjectProperty("Contratante_CNPJ", gxTv_SdtWWPContext_Contratante_cnpj, false);
         AddObjectProperty("Contratante_Telefone", gxTv_SdtWWPContext_Contratante_telefone, false);
         AddObjectProperty("Contratada_Codigo", gxTv_SdtWWPContext_Contratada_codigo, false);
         AddObjectProperty("Contratada_PessoaCod", gxTv_SdtWWPContext_Contratada_pessoacod, false);
         AddObjectProperty("Contratada_PessoaNom", gxTv_SdtWWPContext_Contratada_pessoanom, false);
         AddObjectProperty("ParametrosSistema_NomeSistema", gxTv_SdtWWPContext_Parametrossistema_nomesistema, false);
         AddObjectProperty("ParametrosSistema_AppID", gxTv_SdtWWPContext_Parametrossistema_appid, false);
         AddObjectProperty("UpdComboAreaTrabalho", gxTv_SdtWWPContext_Updcomboareatrabalho, false);
         AddObjectProperty("ServicoPadrao", gxTv_SdtWWPContext_Servicopadrao, false);
         AddObjectProperty("CalculoPFinal", gxTv_SdtWWPContext_Calculopfinal, false);
         AddObjectProperty("Insert", gxTv_SdtWWPContext_Insert, false);
         AddObjectProperty("Update", gxTv_SdtWWPContext_Update, false);
         AddObjectProperty("Delete", gxTv_SdtWWPContext_Delete, false);
         AddObjectProperty("Display", gxTv_SdtWWPContext_Display, false);
         AddObjectProperty("PathCertificacao", gxTv_SdtWWPContext_Pathcertificacao, false);
         AddObjectProperty("Organizacao_Codigo", gxTv_SdtWWPContext_Organizacao_codigo, false);
         AddObjectProperty("Usuario_Email", gxTv_SdtWWPContext_Usuario_email, false);
         AddObjectProperty("screen_height", gxTv_SdtWWPContext_Screen_height, false);
         AddObjectProperty("screen_width", gxTv_SdtWWPContext_Screen_width, false);
         AddObjectProperty("ValidationKey", gxTv_SdtWWPContext_Validationkey, false);
         return  ;
      }

      [  SoapElement( ElementName = "UserId" )]
      [  XmlElement( ElementName = "UserId"   )]
      public short gxTpr_Userid
      {
         get {
            return gxTv_SdtWWPContext_Userid ;
         }

         set {
            gxTv_SdtWWPContext_Userid = (short)(value);
         }

      }

      [  SoapElement( ElementName = "UserName" )]
      [  XmlElement( ElementName = "UserName"   )]
      public String gxTpr_Username
      {
         get {
            return gxTv_SdtWWPContext_Username ;
         }

         set {
            gxTv_SdtWWPContext_Username = (String)(value);
         }

      }

      [  SoapElement( ElementName = "UserEhContador" )]
      [  XmlElement( ElementName = "UserEhContador"   )]
      public bool gxTpr_Userehcontador
      {
         get {
            return gxTv_SdtWWPContext_Userehcontador ;
         }

         set {
            gxTv_SdtWWPContext_Userehcontador = value;
         }

      }

      [  SoapElement( ElementName = "UserEhAuditorFM" )]
      [  XmlElement( ElementName = "UserEhAuditorFM"   )]
      public bool gxTpr_Userehauditorfm
      {
         get {
            return gxTv_SdtWWPContext_Userehauditorfm ;
         }

         set {
            gxTv_SdtWWPContext_Userehauditorfm = value;
         }

      }

      [  SoapElement( ElementName = "UserEhContratada" )]
      [  XmlElement( ElementName = "UserEhContratada"   )]
      public bool gxTpr_Userehcontratada
      {
         get {
            return gxTv_SdtWWPContext_Userehcontratada ;
         }

         set {
            gxTv_SdtWWPContext_Userehcontratada = value;
         }

      }

      [  SoapElement( ElementName = "UserEhContratante" )]
      [  XmlElement( ElementName = "UserEhContratante"   )]
      public bool gxTpr_Userehcontratante
      {
         get {
            return gxTv_SdtWWPContext_Userehcontratante ;
         }

         set {
            gxTv_SdtWWPContext_Userehcontratante = value;
         }

      }

      [  SoapElement( ElementName = "UserEhFinanceiro" )]
      [  XmlElement( ElementName = "UserEhFinanceiro"   )]
      public bool gxTpr_Userehfinanceiro
      {
         get {
            return gxTv_SdtWWPContext_Userehfinanceiro ;
         }

         set {
            gxTv_SdtWWPContext_Userehfinanceiro = value;
         }

      }

      [  SoapElement( ElementName = "UserEhLicenciado" )]
      [  XmlElement( ElementName = "UserEhLicenciado"   )]
      public bool gxTpr_Userehlicenciado
      {
         get {
            return gxTv_SdtWWPContext_Userehlicenciado ;
         }

         set {
            gxTv_SdtWWPContext_Userehlicenciado = value;
         }

      }

      [  SoapElement( ElementName = "UserEhGestor" )]
      [  XmlElement( ElementName = "UserEhGestor"   )]
      public bool gxTpr_Userehgestor
      {
         get {
            return gxTv_SdtWWPContext_Userehgestor ;
         }

         set {
            gxTv_SdtWWPContext_Userehgestor = value;
         }

      }

      [  SoapElement( ElementName = "ServicosGeridos" )]
      [  XmlElement( ElementName = "ServicosGeridos"   )]
      public String gxTpr_Servicosgeridos
      {
         get {
            return gxTv_SdtWWPContext_Servicosgeridos ;
         }

         set {
            gxTv_SdtWWPContext_Servicosgeridos = (String)(value);
         }

      }

      [  SoapElement( ElementName = "UserEhAdministradorGAM" )]
      [  XmlElement( ElementName = "UserEhAdministradorGAM"   )]
      public bool gxTpr_Userehadministradorgam
      {
         get {
            return gxTv_SdtWWPContext_Userehadministradorgam ;
         }

         set {
            gxTv_SdtWWPContext_Userehadministradorgam = value;
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo"   )]
      public int gxTpr_Areatrabalho_codigo
      {
         get {
            return gxTv_SdtWWPContext_Areatrabalho_codigo ;
         }

         set {
            gxTv_SdtWWPContext_Areatrabalho_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_Descricao" )]
      [  XmlElement( ElementName = "AreaTrabalho_Descricao"   )]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return gxTv_SdtWWPContext_Areatrabalho_descricao ;
         }

         set {
            gxTv_SdtWWPContext_Areatrabalho_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_Codigo" )]
      [  XmlElement( ElementName = "Contratante_Codigo"   )]
      public int gxTpr_Contratante_codigo
      {
         get {
            return gxTv_SdtWWPContext_Contratante_codigo ;
         }

         set {
            gxTv_SdtWWPContext_Contratante_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial"   )]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return gxTv_SdtWWPContext_Contratante_razaosocial ;
         }

         set {
            gxTv_SdtWWPContext_Contratante_razaosocial = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_NomeFantasia" )]
      [  XmlElement( ElementName = "Contratante_NomeFantasia"   )]
      public String gxTpr_Contratante_nomefantasia
      {
         get {
            return gxTv_SdtWWPContext_Contratante_nomefantasia ;
         }

         set {
            gxTv_SdtWWPContext_Contratante_nomefantasia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_CNPJ" )]
      [  XmlElement( ElementName = "Contratante_CNPJ"   )]
      public String gxTpr_Contratante_cnpj
      {
         get {
            return gxTv_SdtWWPContext_Contratante_cnpj ;
         }

         set {
            gxTv_SdtWWPContext_Contratante_cnpj = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_Telefone" )]
      [  XmlElement( ElementName = "Contratante_Telefone"   )]
      public String gxTpr_Contratante_telefone
      {
         get {
            return gxTv_SdtWWPContext_Contratante_telefone ;
         }

         set {
            gxTv_SdtWWPContext_Contratante_telefone = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_Codigo" )]
      [  XmlElement( ElementName = "Contratada_Codigo"   )]
      public int gxTpr_Contratada_codigo
      {
         get {
            return gxTv_SdtWWPContext_Contratada_codigo ;
         }

         set {
            gxTv_SdtWWPContext_Contratada_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaCod" )]
      [  XmlElement( ElementName = "Contratada_PessoaCod"   )]
      public int gxTpr_Contratada_pessoacod
      {
         get {
            return gxTv_SdtWWPContext_Contratada_pessoacod ;
         }

         set {
            gxTv_SdtWWPContext_Contratada_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaNom" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom"   )]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return gxTv_SdtWWPContext_Contratada_pessoanom ;
         }

         set {
            gxTv_SdtWWPContext_Contratada_pessoanom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ParametrosSistema_NomeSistema" )]
      [  XmlElement( ElementName = "ParametrosSistema_NomeSistema"   )]
      public String gxTpr_Parametrossistema_nomesistema
      {
         get {
            return gxTv_SdtWWPContext_Parametrossistema_nomesistema ;
         }

         set {
            gxTv_SdtWWPContext_Parametrossistema_nomesistema = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ParametrosSistema_AppID" )]
      [  XmlElement( ElementName = "ParametrosSistema_AppID"   )]
      public long gxTpr_Parametrossistema_appid
      {
         get {
            return gxTv_SdtWWPContext_Parametrossistema_appid ;
         }

         set {
            gxTv_SdtWWPContext_Parametrossistema_appid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "UpdComboAreaTrabalho" )]
      [  XmlElement( ElementName = "UpdComboAreaTrabalho"   )]
      public bool gxTpr_Updcomboareatrabalho
      {
         get {
            return gxTv_SdtWWPContext_Updcomboareatrabalho ;
         }

         set {
            gxTv_SdtWWPContext_Updcomboareatrabalho = value;
         }

      }

      [  SoapElement( ElementName = "ServicoPadrao" )]
      [  XmlElement( ElementName = "ServicoPadrao"   )]
      public short gxTpr_Servicopadrao
      {
         get {
            return gxTv_SdtWWPContext_Servicopadrao ;
         }

         set {
            gxTv_SdtWWPContext_Servicopadrao = (short)(value);
         }

      }

      [  SoapElement( ElementName = "CalculoPFinal" )]
      [  XmlElement( ElementName = "CalculoPFinal"   )]
      public String gxTpr_Calculopfinal
      {
         get {
            return gxTv_SdtWWPContext_Calculopfinal ;
         }

         set {
            gxTv_SdtWWPContext_Calculopfinal = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Insert" )]
      [  XmlElement( ElementName = "Insert"   )]
      public bool gxTpr_Insert
      {
         get {
            return gxTv_SdtWWPContext_Insert ;
         }

         set {
            gxTv_SdtWWPContext_Insert = value;
         }

      }

      [  SoapElement( ElementName = "Update" )]
      [  XmlElement( ElementName = "Update"   )]
      public bool gxTpr_Update
      {
         get {
            return gxTv_SdtWWPContext_Update ;
         }

         set {
            gxTv_SdtWWPContext_Update = value;
         }

      }

      [  SoapElement( ElementName = "Delete" )]
      [  XmlElement( ElementName = "Delete"   )]
      public bool gxTpr_Delete
      {
         get {
            return gxTv_SdtWWPContext_Delete ;
         }

         set {
            gxTv_SdtWWPContext_Delete = value;
         }

      }

      [  SoapElement( ElementName = "Display" )]
      [  XmlElement( ElementName = "Display"   )]
      public bool gxTpr_Display
      {
         get {
            return gxTv_SdtWWPContext_Display ;
         }

         set {
            gxTv_SdtWWPContext_Display = value;
         }

      }

      [  SoapElement( ElementName = "PathCertificacao" )]
      [  XmlElement( ElementName = "PathCertificacao"   )]
      public String gxTpr_Pathcertificacao
      {
         get {
            return gxTv_SdtWWPContext_Pathcertificacao ;
         }

         set {
            gxTv_SdtWWPContext_Pathcertificacao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Organizacao_Codigo" )]
      [  XmlElement( ElementName = "Organizacao_Codigo"   )]
      public int gxTpr_Organizacao_codigo
      {
         get {
            return gxTv_SdtWWPContext_Organizacao_codigo ;
         }

         set {
            gxTv_SdtWWPContext_Organizacao_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Email" )]
      [  XmlElement( ElementName = "Usuario_Email"   )]
      public String gxTpr_Usuario_email
      {
         get {
            return gxTv_SdtWWPContext_Usuario_email ;
         }

         set {
            gxTv_SdtWWPContext_Usuario_email = (String)(value);
         }

      }

      [  SoapElement( ElementName = "screen_height" )]
      [  XmlElement( ElementName = "screen_height"   )]
      public short gxTpr_Screen_height
      {
         get {
            return gxTv_SdtWWPContext_Screen_height ;
         }

         set {
            gxTv_SdtWWPContext_Screen_height = (short)(value);
         }

      }

      [  SoapElement( ElementName = "screen_width" )]
      [  XmlElement( ElementName = "screen_width"   )]
      public short gxTpr_Screen_width
      {
         get {
            return gxTv_SdtWWPContext_Screen_width ;
         }

         set {
            gxTv_SdtWWPContext_Screen_width = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ValidationKey" )]
      [  XmlElement( ElementName = "ValidationKey"   )]
      public String gxTpr_Validationkey
      {
         get {
            return gxTv_SdtWWPContext_Validationkey ;
         }

         set {
            gxTv_SdtWWPContext_Validationkey = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtWWPContext_Username = "";
         gxTv_SdtWWPContext_Servicosgeridos = "";
         gxTv_SdtWWPContext_Areatrabalho_descricao = "";
         gxTv_SdtWWPContext_Contratante_razaosocial = "";
         gxTv_SdtWWPContext_Contratante_nomefantasia = "";
         gxTv_SdtWWPContext_Contratante_cnpj = "";
         gxTv_SdtWWPContext_Contratante_telefone = "";
         gxTv_SdtWWPContext_Contratada_pessoanom = "";
         gxTv_SdtWWPContext_Parametrossistema_nomesistema = "";
         gxTv_SdtWWPContext_Calculopfinal = "";
         gxTv_SdtWWPContext_Pathcertificacao = "";
         gxTv_SdtWWPContext_Usuario_email = "";
         gxTv_SdtWWPContext_Validationkey = "";
         gxTv_SdtWWPContext_Updcomboareatrabalho = false;
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtWWPContext_Userid ;
      protected short gxTv_SdtWWPContext_Servicopadrao ;
      protected short gxTv_SdtWWPContext_Screen_height ;
      protected short gxTv_SdtWWPContext_Screen_width ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtWWPContext_Areatrabalho_codigo ;
      protected int gxTv_SdtWWPContext_Contratante_codigo ;
      protected int gxTv_SdtWWPContext_Contratada_codigo ;
      protected int gxTv_SdtWWPContext_Contratada_pessoacod ;
      protected int gxTv_SdtWWPContext_Organizacao_codigo ;
      protected long gxTv_SdtWWPContext_Parametrossistema_appid ;
      protected String gxTv_SdtWWPContext_Contratante_razaosocial ;
      protected String gxTv_SdtWWPContext_Contratante_nomefantasia ;
      protected String gxTv_SdtWWPContext_Contratante_telefone ;
      protected String gxTv_SdtWWPContext_Contratada_pessoanom ;
      protected String gxTv_SdtWWPContext_Parametrossistema_nomesistema ;
      protected String gxTv_SdtWWPContext_Calculopfinal ;
      protected String sTagName ;
      protected bool gxTv_SdtWWPContext_Userehcontador ;
      protected bool gxTv_SdtWWPContext_Userehauditorfm ;
      protected bool gxTv_SdtWWPContext_Userehcontratada ;
      protected bool gxTv_SdtWWPContext_Userehcontratante ;
      protected bool gxTv_SdtWWPContext_Userehfinanceiro ;
      protected bool gxTv_SdtWWPContext_Userehlicenciado ;
      protected bool gxTv_SdtWWPContext_Userehgestor ;
      protected bool gxTv_SdtWWPContext_Userehadministradorgam ;
      protected bool gxTv_SdtWWPContext_Updcomboareatrabalho ;
      protected bool gxTv_SdtWWPContext_Insert ;
      protected bool gxTv_SdtWWPContext_Update ;
      protected bool gxTv_SdtWWPContext_Delete ;
      protected bool gxTv_SdtWWPContext_Display ;
      protected String gxTv_SdtWWPContext_Username ;
      protected String gxTv_SdtWWPContext_Servicosgeridos ;
      protected String gxTv_SdtWWPContext_Areatrabalho_descricao ;
      protected String gxTv_SdtWWPContext_Contratante_cnpj ;
      protected String gxTv_SdtWWPContext_Pathcertificacao ;
      protected String gxTv_SdtWWPContext_Usuario_email ;
      protected String gxTv_SdtWWPContext_Validationkey ;
   }

   [DataContract(Name = @"WWPBaseObjects\WWPContext", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtWWPContext_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtWWPContext>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtWWPContext_RESTInterface( ) : base()
      {
      }

      public SdtWWPContext_RESTInterface( wwpbaseobjects.SdtWWPContext psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "UserId" , Order = 0 )]
      public Nullable<short> gxTpr_Userid
      {
         get {
            return sdt.gxTpr_Userid ;
         }

         set {
            sdt.gxTpr_Userid = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "UserName" , Order = 1 )]
      public String gxTpr_Username
      {
         get {
            return sdt.gxTpr_Username ;
         }

         set {
            sdt.gxTpr_Username = (String)(value);
         }

      }

      [DataMember( Name = "UserEhContador" , Order = 2 )]
      public bool gxTpr_Userehcontador
      {
         get {
            return sdt.gxTpr_Userehcontador ;
         }

         set {
            sdt.gxTpr_Userehcontador = value;
         }

      }

      [DataMember( Name = "UserEhAuditorFM" , Order = 3 )]
      public bool gxTpr_Userehauditorfm
      {
         get {
            return sdt.gxTpr_Userehauditorfm ;
         }

         set {
            sdt.gxTpr_Userehauditorfm = value;
         }

      }

      [DataMember( Name = "UserEhContratada" , Order = 4 )]
      public bool gxTpr_Userehcontratada
      {
         get {
            return sdt.gxTpr_Userehcontratada ;
         }

         set {
            sdt.gxTpr_Userehcontratada = value;
         }

      }

      [DataMember( Name = "UserEhContratante" , Order = 5 )]
      public bool gxTpr_Userehcontratante
      {
         get {
            return sdt.gxTpr_Userehcontratante ;
         }

         set {
            sdt.gxTpr_Userehcontratante = value;
         }

      }

      [DataMember( Name = "UserEhFinanceiro" , Order = 6 )]
      public bool gxTpr_Userehfinanceiro
      {
         get {
            return sdt.gxTpr_Userehfinanceiro ;
         }

         set {
            sdt.gxTpr_Userehfinanceiro = value;
         }

      }

      [DataMember( Name = "UserEhLicenciado" , Order = 7 )]
      public bool gxTpr_Userehlicenciado
      {
         get {
            return sdt.gxTpr_Userehlicenciado ;
         }

         set {
            sdt.gxTpr_Userehlicenciado = value;
         }

      }

      [DataMember( Name = "UserEhGestor" , Order = 8 )]
      public bool gxTpr_Userehgestor
      {
         get {
            return sdt.gxTpr_Userehgestor ;
         }

         set {
            sdt.gxTpr_Userehgestor = value;
         }

      }

      [DataMember( Name = "ServicosGeridos" , Order = 9 )]
      public String gxTpr_Servicosgeridos
      {
         get {
            return sdt.gxTpr_Servicosgeridos ;
         }

         set {
            sdt.gxTpr_Servicosgeridos = (String)(value);
         }

      }

      [DataMember( Name = "UserEhAdministradorGAM" , Order = 10 )]
      public bool gxTpr_Userehadministradorgam
      {
         get {
            return sdt.gxTpr_Userehadministradorgam ;
         }

         set {
            sdt.gxTpr_Userehadministradorgam = value;
         }

      }

      [DataMember( Name = "AreaTrabalho_Codigo" , Order = 11 )]
      public Nullable<int> gxTpr_Areatrabalho_codigo
      {
         get {
            return sdt.gxTpr_Areatrabalho_codigo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_Descricao" , Order = 12 )]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return sdt.gxTpr_Areatrabalho_descricao ;
         }

         set {
            sdt.gxTpr_Areatrabalho_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Codigo" , Order = 13 )]
      public Nullable<int> gxTpr_Contratante_codigo
      {
         get {
            return sdt.gxTpr_Contratante_codigo ;
         }

         set {
            sdt.gxTpr_Contratante_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_RazaoSocial" , Order = 14 )]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_razaosocial) ;
         }

         set {
            sdt.gxTpr_Contratante_razaosocial = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_NomeFantasia" , Order = 15 )]
      public String gxTpr_Contratante_nomefantasia
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_nomefantasia) ;
         }

         set {
            sdt.gxTpr_Contratante_nomefantasia = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_CNPJ" , Order = 16 )]
      public String gxTpr_Contratante_cnpj
      {
         get {
            return sdt.gxTpr_Contratante_cnpj ;
         }

         set {
            sdt.gxTpr_Contratante_cnpj = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Telefone" , Order = 17 )]
      public String gxTpr_Contratante_telefone
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_telefone) ;
         }

         set {
            sdt.gxTpr_Contratante_telefone = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_Codigo" , Order = 18 )]
      public Nullable<int> gxTpr_Contratada_codigo
      {
         get {
            return sdt.gxTpr_Contratada_codigo ;
         }

         set {
            sdt.gxTpr_Contratada_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaCod" , Order = 19 )]
      public Nullable<int> gxTpr_Contratada_pessoacod
      {
         get {
            return sdt.gxTpr_Contratada_pessoacod ;
         }

         set {
            sdt.gxTpr_Contratada_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaNom" , Order = 20 )]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_pessoanom) ;
         }

         set {
            sdt.gxTpr_Contratada_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_NomeSistema" , Order = 21 )]
      public String gxTpr_Parametrossistema_nomesistema
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Parametrossistema_nomesistema) ;
         }

         set {
            sdt.gxTpr_Parametrossistema_nomesistema = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_AppID" , Order = 22 )]
      public String gxTpr_Parametrossistema_appid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Parametrossistema_appid), 12, 0)) ;
         }

         set {
            sdt.gxTpr_Parametrossistema_appid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "UpdComboAreaTrabalho" , Order = 23 )]
      public bool gxTpr_Updcomboareatrabalho
      {
         get {
            return sdt.gxTpr_Updcomboareatrabalho ;
         }

         set {
            sdt.gxTpr_Updcomboareatrabalho = value;
         }

      }

      [DataMember( Name = "ServicoPadrao" , Order = 24 )]
      public Nullable<short> gxTpr_Servicopadrao
      {
         get {
            return sdt.gxTpr_Servicopadrao ;
         }

         set {
            sdt.gxTpr_Servicopadrao = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "CalculoPFinal" , Order = 25 )]
      public String gxTpr_Calculopfinal
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Calculopfinal) ;
         }

         set {
            sdt.gxTpr_Calculopfinal = (String)(value);
         }

      }

      [DataMember( Name = "Insert" , Order = 26 )]
      public bool gxTpr_Insert
      {
         get {
            return sdt.gxTpr_Insert ;
         }

         set {
            sdt.gxTpr_Insert = value;
         }

      }

      [DataMember( Name = "Update" , Order = 27 )]
      public bool gxTpr_Update
      {
         get {
            return sdt.gxTpr_Update ;
         }

         set {
            sdt.gxTpr_Update = value;
         }

      }

      [DataMember( Name = "Delete" , Order = 28 )]
      public bool gxTpr_Delete
      {
         get {
            return sdt.gxTpr_Delete ;
         }

         set {
            sdt.gxTpr_Delete = value;
         }

      }

      [DataMember( Name = "Display" , Order = 29 )]
      public bool gxTpr_Display
      {
         get {
            return sdt.gxTpr_Display ;
         }

         set {
            sdt.gxTpr_Display = value;
         }

      }

      [DataMember( Name = "PathCertificacao" , Order = 30 )]
      public String gxTpr_Pathcertificacao
      {
         get {
            return sdt.gxTpr_Pathcertificacao ;
         }

         set {
            sdt.gxTpr_Pathcertificacao = (String)(value);
         }

      }

      [DataMember( Name = "Organizacao_Codigo" , Order = 31 )]
      public Nullable<int> gxTpr_Organizacao_codigo
      {
         get {
            return sdt.gxTpr_Organizacao_codigo ;
         }

         set {
            sdt.gxTpr_Organizacao_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_Email" , Order = 32 )]
      public String gxTpr_Usuario_email
      {
         get {
            return sdt.gxTpr_Usuario_email ;
         }

         set {
            sdt.gxTpr_Usuario_email = (String)(value);
         }

      }

      [DataMember( Name = "screen_height" , Order = 33 )]
      public Nullable<short> gxTpr_Screen_height
      {
         get {
            return sdt.gxTpr_Screen_height ;
         }

         set {
            sdt.gxTpr_Screen_height = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "screen_width" , Order = 34 )]
      public Nullable<short> gxTpr_Screen_width
      {
         get {
            return sdt.gxTpr_Screen_width ;
         }

         set {
            sdt.gxTpr_Screen_width = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ValidationKey" , Order = 35 )]
      public String gxTpr_Validationkey
      {
         get {
            return sdt.gxTpr_Validationkey ;
         }

         set {
            sdt.gxTpr_Validationkey = (String)(value);
         }

      }

      public wwpbaseobjects.SdtWWPContext sdt
      {
         get {
            return (wwpbaseobjects.SdtWWPContext)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtWWPContext() ;
         }
      }

   }

}
