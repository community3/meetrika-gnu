/*
               File: wwpbaseobjects.type_SdtDVB_SDTDropDownOptionsData_Item
        Description: WWPBaseObjects\DVB_SDTDropDownOptionsData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:56.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "DVB_SDTDropDownOptionsData.Item" )]
   [XmlType(TypeName =  "DVB_SDTDropDownOptionsData.Item" , Namespace = "" )]
   [Serializable]
   public class SdtDVB_SDTDropDownOptionsData_Item : GxUserType
   {
      public SdtDVB_SDTDropDownOptionsData_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent = "";
      }

      public SdtDVB_SDTDropDownOptionsData_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item obj ;
         obj = this;
         obj.gxTpr_Icon = deserialized.gxTpr_Icon;
         obj.gxTpr_Icon_gxi = deserialized.gxTpr_Icon_gxi;
         obj.gxTpr_Title = deserialized.gxTpr_Title;
         obj.gxTpr_Tooltip = deserialized.gxTpr_Tooltip;
         obj.gxTpr_Link = deserialized.gxTpr_Link;
         obj.gxTpr_Eventkey = deserialized.gxTpr_Eventkey;
         obj.gxTpr_Isdivider = deserialized.gxTpr_Isdivider;
         obj.gxTpr_Jsonclickevent = deserialized.gxTpr_Jsonclickevent;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Icon") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Icon_GXI") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Title") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tooltip") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Link") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventKey") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "IsDivider") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Isdivider = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "JSonclickEvent") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\DVB_SDTDropDownOptionsData.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Icon", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("Icon_GXI", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("Title", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("Tooltip", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("Link", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("EventKey", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("IsDivider", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Isdivider)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("JSonclickEvent", StringUtil.RTrim( gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Icon", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon, false);
         AddObjectProperty("Icon_GXI", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi, false);
         AddObjectProperty("Title", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title, false);
         AddObjectProperty("Tooltip", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip, false);
         AddObjectProperty("Link", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link, false);
         AddObjectProperty("EventKey", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey, false);
         AddObjectProperty("IsDivider", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Isdivider, false);
         AddObjectProperty("JSonclickEvent", gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent, false);
         return  ;
      }

      [  SoapElement( ElementName = "Icon" )]
      [  XmlElement( ElementName = "Icon"   )]
      [GxUpload()]
      public String gxTpr_Icon
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Icon_GXI" )]
      [  XmlElement( ElementName = "Icon_GXI"   )]
      public String gxTpr_Icon_gxi
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Title" )]
      [  XmlElement( ElementName = "Title"   )]
      public String gxTpr_Title
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Tooltip" )]
      [  XmlElement( ElementName = "Tooltip"   )]
      public String gxTpr_Tooltip
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Link" )]
      [  XmlElement( ElementName = "Link"   )]
      public String gxTpr_Link
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "EventKey" )]
      [  XmlElement( ElementName = "EventKey"   )]
      public String gxTpr_Eventkey
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey = (String)(value);
         }

      }

      [  SoapElement( ElementName = "IsDivider" )]
      [  XmlElement( ElementName = "IsDivider"   )]
      public bool gxTpr_Isdivider
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Isdivider ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Isdivider = value;
         }

      }

      [  SoapElement( ElementName = "JSonclickEvent" )]
      [  XmlElement( ElementName = "JSonclickEvent"   )]
      public String gxTpr_Jsonclickevent
      {
         get {
            return gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent ;
         }

         set {
            gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey = "";
         gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsData_Item_Title ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsData_Item_Tooltip ;
      protected String sTagName ;
      protected bool gxTv_SdtDVB_SDTDropDownOptionsData_Item_Isdivider ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon_gxi ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsData_Item_Link ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsData_Item_Eventkey ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsData_Item_Jsonclickevent ;
      protected String gxTv_SdtDVB_SDTDropDownOptionsData_Item_Icon ;
   }

   [DataContract(Name = @"WWPBaseObjects\DVB_SDTDropDownOptionsData.Item", Namespace = "")]
   public class SdtDVB_SDTDropDownOptionsData_Item_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtDVB_SDTDropDownOptionsData_Item_RESTInterface( ) : base()
      {
      }

      public SdtDVB_SDTDropDownOptionsData_Item_RESTInterface( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Icon" , Order = 0 )]
      [GxUpload()]
      public String gxTpr_Icon
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Icon)) ? PathUtil.RelativePath( sdt.gxTpr_Icon) : StringUtil.RTrim( sdt.gxTpr_Icon_gxi)) ;
         }

         set {
            sdt.gxTpr_Icon = (String)(value);
         }

      }

      [DataMember( Name = "Title" , Order = 2 )]
      public String gxTpr_Title
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Title) ;
         }

         set {
            sdt.gxTpr_Title = (String)(value);
         }

      }

      [DataMember( Name = "Tooltip" , Order = 3 )]
      public String gxTpr_Tooltip
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tooltip) ;
         }

         set {
            sdt.gxTpr_Tooltip = (String)(value);
         }

      }

      [DataMember( Name = "Link" , Order = 4 )]
      public String gxTpr_Link
      {
         get {
            return sdt.gxTpr_Link ;
         }

         set {
            sdt.gxTpr_Link = (String)(value);
         }

      }

      [DataMember( Name = "EventKey" , Order = 5 )]
      public String gxTpr_Eventkey
      {
         get {
            return sdt.gxTpr_Eventkey ;
         }

         set {
            sdt.gxTpr_Eventkey = (String)(value);
         }

      }

      [DataMember( Name = "IsDivider" , Order = 6 )]
      public bool gxTpr_Isdivider
      {
         get {
            return sdt.gxTpr_Isdivider ;
         }

         set {
            sdt.gxTpr_Isdivider = value;
         }

      }

      [DataMember( Name = "JSonclickEvent" , Order = 7 )]
      public String gxTpr_Jsonclickevent
      {
         get {
            return sdt.gxTpr_Jsonclickevent ;
         }

         set {
            sdt.gxTpr_Jsonclickevent = (String)(value);
         }

      }

      public wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item sdt
      {
         get {
            return (wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item() ;
         }
      }

   }

}
