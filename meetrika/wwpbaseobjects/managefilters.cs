/*
               File: WWPBaseObjects.ManageFilters
        Description: Gerenciador de filtros
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:33.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   public class managefilters : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public managefilters( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public managefilters( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_UserKey )
      {
         this.AV17UserKey = aP0_UserKey;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavCollectionisempty = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridgridstatecollections") == 0 )
            {
               nRC_GXsfl_14 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_14_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_14_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridgridstatecollections_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridgridstatecollections") == 0 )
            {
               subGridgridstatecollections_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV17UserKey = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17UserKey", AV17UserKey);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERKEY", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17UserKey, ""))));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA072( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START072( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117303350");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV17UserKey))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Gridstatecollection", AV7GridStateCollection);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Gridstatecollection", AV7GridStateCollection);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_14", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_14), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDGRIDSTATECOLLECTIONSCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20GridGridStateCollectionsCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDGRIDSTATECOLLECTIONSPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21GridGridStateCollectionsPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATECOLLECTION", AV7GridStateCollection);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATECOLLECTION", AV7GridStateCollection);
         }
         GxWebStd.gx_hidden_field( context, "vUSERKEY", AV17UserKey);
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERKEY", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17UserKey, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERKEY", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17UserKey, ""))));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Class", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_First", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Previous", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Next", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Last", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Caption", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridgridstatecollectionspaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridgridstatecollectionspaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridgridstatecollectionspaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridgridstatecollectionspaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridgridstatecollectionspaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Width", StringUtil.RTrim( Dvpanel_unnamedtable1_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Cls", StringUtil.RTrim( Dvpanel_unnamedtable1_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Title", StringUtil.RTrim( Dvpanel_unnamedtable1_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Collapsed", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autowidth", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoheight", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Iconposition", StringUtil.RTrim( Dvpanel_unnamedtable1_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_UNNAMEDTABLE1_Autoscroll", StringUtil.BoolToStr( Dvpanel_unnamedtable1_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridgridstatecollectionspaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE072( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT072( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV17UserKey)) ;
      }

      public override String GetPgmname( )
      {
         return "WWPBaseObjects.ManageFilters" ;
      }

      public override String GetPgmdesc( )
      {
         return "Gerenciador de filtros" ;
      }

      protected void WB070( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_072( true) ;
         }
         else
         {
            wb_table1_2_072( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_072e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_14_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCollectionisempty_Internalname, StringUtil.BoolToStr( AV18CollectionIsEmpty), "", "", chkavCollectionisempty.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(41, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"");
         }
         wbLoad = true;
      }

      protected void START072( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Gerenciador de filtros", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP070( ) ;
      }

      protected void WS072( )
      {
         START072( ) ;
         EVT072( ) ;
      }

      protected void EVT072( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11072 */
                              E11072 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12072 */
                                    E12072 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 29), "GRIDGRIDSTATECOLLECTIONS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VMOVEUP.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VMOVEDOWN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VUDELETE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VMOVEUP.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VMOVEDOWN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VUDELETE.CLICK") == 0 ) )
                           {
                              nGXsfl_14_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
                              SubsflControlProps_142( ) ;
                              AV24GXV1 = (short)(nGXsfl_14_idx+GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage);
                              if ( ( AV7GridStateCollection.Count >= AV24GXV1 ) && ( AV24GXV1 > 0 ) )
                              {
                                 AV7GridStateCollection.CurrentItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1));
                                 AV16MoveUp = cgiGet( edtavMoveup_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMoveup_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16MoveUp)) ? AV27Moveup_GXI : context.convertURL( context.PathToRelativeUrl( AV16MoveUp))));
                                 AV15MoveDown = cgiGet( edtavMovedown_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMovedown_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15MoveDown)) ? AV28Movedown_GXI : context.convertURL( context.PathToRelativeUrl( AV15MoveDown))));
                                 AV6UDelete = cgiGet( edtavUdelete_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUdelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV6UDelete)) ? AV29Udelete_GXI : context.convertURL( context.PathToRelativeUrl( AV6UDelete))));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13072 */
                                    E13072 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14072 */
                                    E14072 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDGRIDSTATECOLLECTIONS.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15072 */
                                    E15072 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VMOVEUP.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16072 */
                                    E16072 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VMOVEDOWN.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17072 */
                                    E17072 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VUDELETE.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18072 */
                                    E18072 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE072( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA072( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavCollectionisempty.Name = "vCOLLECTIONISEMPTY";
            chkavCollectionisempty.WebTags = "";
            chkavCollectionisempty.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCollectionisempty_Internalname, "TitleCaption", chkavCollectionisempty.Caption);
            chkavCollectionisempty.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavCollectionisempty_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridgridstatecollections_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_142( ) ;
         while ( nGXsfl_14_idx <= nRC_GXsfl_14 )
         {
            sendrow_142( ) ;
            nGXsfl_14_idx = (short)(((subGridgridstatecollections_Islastpage==1)&&(nGXsfl_14_idx+1>subGridgridstatecollections_Recordsperpage( )) ? 1 : nGXsfl_14_idx+1));
            sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
            SubsflControlProps_142( ) ;
         }
         context.GX_webresponse.AddString(GridgridstatecollectionsContainer.ToJavascriptSource());
         /* End function gxnrGridgridstatecollections_newrow */
      }

      protected void gxgrGridgridstatecollections_refresh( int subGridgridstatecollections_Rows )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Rows), 6, 0, ".", "")));
         GRIDGRIDSTATECOLLECTIONS_nCurrentRecord = 0;
         RF072( ) ;
         /* End function gxgrGridgridstatecollections_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF072( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavGridstatecollection__gridstatexml_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGridstatecollection__gridstatexml_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridstatecollection__gridstatexml_Enabled), 5, 0)));
      }

      protected void RF072( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridgridstatecollectionsContainer.ClearRows();
         }
         wbStart = 14;
         /* Execute user event: E14072 */
         E14072 ();
         nGXsfl_14_idx = 1;
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
         nGXsfl_14_Refreshing = 1;
         GridgridstatecollectionsContainer.AddObjectProperty("GridName", "Gridgridstatecollections");
         GridgridstatecollectionsContainer.AddObjectProperty("CmpContext", "");
         GridgridstatecollectionsContainer.AddObjectProperty("InMasterPage", "false");
         GridgridstatecollectionsContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridgridstatecollectionsContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridgridstatecollectionsContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridgridstatecollectionsContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Backcolorstyle), 1, 0, ".", "")));
         GridgridstatecollectionsContainer.PageSize = subGridgridstatecollections_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_142( ) ;
            /* Execute user event: E15072 */
            E15072 ();
            if ( ( GRIDGRIDSTATECOLLECTIONS_nCurrentRecord > 0 ) && ( GRIDGRIDSTATECOLLECTIONS_nGridOutOfScope == 0 ) && ( nGXsfl_14_idx == 1 ) )
            {
               GRIDGRIDSTATECOLLECTIONS_nCurrentRecord = 0;
               GRIDGRIDSTATECOLLECTIONS_nGridOutOfScope = 1;
               subgridgridstatecollections_firstpage( ) ;
               /* Execute user event: E15072 */
               E15072 ();
            }
            wbEnd = 14;
            WB070( ) ;
         }
         nGXsfl_14_Refreshing = 0;
      }

      protected int subGridgridstatecollections_Pagecount( )
      {
         GRIDGRIDSTATECOLLECTIONS_nRecordCount = subGridgridstatecollections_Recordcount( );
         if ( ((int)((GRIDGRIDSTATECOLLECTIONS_nRecordCount) % (subGridgridstatecollections_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDGRIDSTATECOLLECTIONS_nRecordCount/ (decimal)(subGridgridstatecollections_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDGRIDSTATECOLLECTIONS_nRecordCount/ (decimal)(subGridgridstatecollections_Recordsperpage( ))))+1) ;
      }

      protected int subGridgridstatecollections_Recordcount( )
      {
         return AV7GridStateCollection.Count ;
      }

      protected int subGridgridstatecollections_Recordsperpage( )
      {
         if ( subGridgridstatecollections_Rows > 0 )
         {
            return subGridgridstatecollections_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridgridstatecollections_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage/ (decimal)(subGridgridstatecollections_Recordsperpage( ))))+1) ;
      }

      protected short subgridgridstatecollections_firstpage( )
      {
         GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         }
         return 0 ;
      }

      protected short subgridgridstatecollections_nextpage( )
      {
         GRIDGRIDSTATECOLLECTIONS_nRecordCount = subGridgridstatecollections_Recordcount( );
         if ( ( GRIDGRIDSTATECOLLECTIONS_nRecordCount >= subGridgridstatecollections_Recordsperpage( ) ) && ( GRIDGRIDSTATECOLLECTIONS_nEOF == 0 ) )
         {
            GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = (long)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage+subGridgridstatecollections_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         }
         return (short)(((GRIDGRIDSTATECOLLECTIONS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridgridstatecollections_previouspage( )
      {
         if ( GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage >= subGridgridstatecollections_Recordsperpage( ) )
         {
            GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = (long)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage-subGridgridstatecollections_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         }
         return 0 ;
      }

      protected short subgridgridstatecollections_lastpage( )
      {
         GRIDGRIDSTATECOLLECTIONS_nRecordCount = subGridgridstatecollections_Recordcount( );
         if ( GRIDGRIDSTATECOLLECTIONS_nRecordCount > subGridgridstatecollections_Recordsperpage( ) )
         {
            if ( ((int)((GRIDGRIDSTATECOLLECTIONS_nRecordCount) % (subGridgridstatecollections_Recordsperpage( )))) == 0 )
            {
               GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = (long)(GRIDGRIDSTATECOLLECTIONS_nRecordCount-subGridgridstatecollections_Recordsperpage( ));
            }
            else
            {
               GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = (long)(GRIDGRIDSTATECOLLECTIONS_nRecordCount-((int)((GRIDGRIDSTATECOLLECTIONS_nRecordCount) % (subGridgridstatecollections_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         }
         return 0 ;
      }

      protected int subgridgridstatecollections_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = (long)(subGridgridstatecollections_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         }
         return (int)(0) ;
      }

      protected void STRUP070( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavGridstatecollection__gridstatexml_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGridstatecollection__gridstatexml_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridstatecollection__gridstatexml_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13072 */
         E13072 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Gridstatecollection"), AV7GridStateCollection);
            ajax_req_read_hidden_sdt(cgiGet( "vGRIDSTATECOLLECTION"), AV7GridStateCollection);
            /* Read variables values. */
            AV18CollectionIsEmpty = StringUtil.StrToBool( cgiGet( chkavCollectionisempty_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18CollectionIsEmpty", AV18CollectionIsEmpty);
            /* Read saved values. */
            nRC_GXsfl_14 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_14"), ",", "."));
            AV20GridGridStateCollectionsCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDGRIDSTATECOLLECTIONSCURRENTPAGE"), ",", "."));
            AV21GridGridStateCollectionsPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDGRIDSTATECOLLECTIONSPAGECOUNT"), ",", "."));
            GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage"), ",", "."));
            GRIDGRIDSTATECOLLECTIONS_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDGRIDSTATECOLLECTIONS_nEOF"), ",", "."));
            subGridgridstatecollections_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDGRIDSTATECOLLECTIONS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Rows), 6, 0, ".", "")));
            Gridgridstatecollectionspaginationbar_Class = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Class");
            Gridgridstatecollectionspaginationbar_First = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_First");
            Gridgridstatecollectionspaginationbar_Previous = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Previous");
            Gridgridstatecollectionspaginationbar_Next = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Next");
            Gridgridstatecollectionspaginationbar_Last = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Last");
            Gridgridstatecollectionspaginationbar_Caption = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Caption");
            Gridgridstatecollectionspaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Showfirst"));
            Gridgridstatecollectionspaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Showprevious"));
            Gridgridstatecollectionspaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Shownext"));
            Gridgridstatecollectionspaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Showlast"));
            Gridgridstatecollectionspaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridgridstatecollectionspaginationbar_Pagingbuttonsposition = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Pagingbuttonsposition");
            Gridgridstatecollectionspaginationbar_Pagingcaptionposition = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Pagingcaptionposition");
            Gridgridstatecollectionspaginationbar_Emptygridclass = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Emptygridclass");
            Gridgridstatecollectionspaginationbar_Emptygridcaption = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Emptygridcaption");
            Dvpanel_unnamedtable1_Width = cgiGet( "DVPANEL_UNNAMEDTABLE1_Width");
            Dvpanel_unnamedtable1_Cls = cgiGet( "DVPANEL_UNNAMEDTABLE1_Cls");
            Dvpanel_unnamedtable1_Title = cgiGet( "DVPANEL_UNNAMEDTABLE1_Title");
            Dvpanel_unnamedtable1_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsible"));
            Dvpanel_unnamedtable1_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Collapsed"));
            Dvpanel_unnamedtable1_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autowidth"));
            Dvpanel_unnamedtable1_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoheight"));
            Dvpanel_unnamedtable1_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Showcollapseicon"));
            Dvpanel_unnamedtable1_Iconposition = cgiGet( "DVPANEL_UNNAMEDTABLE1_Iconposition");
            Dvpanel_unnamedtable1_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_UNNAMEDTABLE1_Autoscroll"));
            Gridgridstatecollectionspaginationbar_Selectedpage = cgiGet( "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR_Selectedpage");
            nRC_GXsfl_14 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_14"), ",", "."));
            nGXsfl_14_fel_idx = 0;
            while ( nGXsfl_14_fel_idx < nRC_GXsfl_14 )
            {
               nGXsfl_14_fel_idx = (short)(((subGridgridstatecollections_Islastpage==1)&&(nGXsfl_14_fel_idx+1>subGridgridstatecollections_Recordsperpage( )) ? 1 : nGXsfl_14_fel_idx+1));
               sGXsfl_14_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_142( ) ;
               AV24GXV1 = (short)(nGXsfl_14_fel_idx+GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage);
               if ( ( AV7GridStateCollection.Count >= AV24GXV1 ) && ( AV24GXV1 > 0 ) )
               {
                  AV7GridStateCollection.CurrentItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1));
                  AV16MoveUp = cgiGet( edtavMoveup_Internalname);
                  AV15MoveDown = cgiGet( edtavMovedown_Internalname);
                  AV6UDelete = cgiGet( edtavUdelete_Internalname);
               }
            }
            if ( nGXsfl_14_fel_idx == 0 )
            {
               nGXsfl_14_idx = 1;
               sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
               SubsflControlProps_142( ) ;
            }
            nGXsfl_14_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13072 */
         E13072 ();
         if (returnInSub) return;
      }

      protected void E13072( )
      {
         /* Start Routine */
         chkavCollectionisempty.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCollectionisempty_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavCollectionisempty.Visible), 5, 0)));
         subGridgridstatecollections_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV7GridStateCollection.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  AV17UserKey), "");
            gx_BV14 = true;
         }
      }

      protected void E14072( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV16MoveUp = context.GetImagePath( "18fea524-2fca-4d65-a716-0747be033f02", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMoveup_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16MoveUp)) ? AV27Moveup_GXI : context.convertURL( context.PathToRelativeUrl( AV16MoveUp))));
         AV27Moveup_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "18fea524-2fca-4d65-a716-0747be033f02", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMoveup_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16MoveUp)) ? AV27Moveup_GXI : context.convertURL( context.PathToRelativeUrl( AV16MoveUp))));
         edtavMoveup_Tooltiptext = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMoveup_Internalname, "Tooltiptext", edtavMoveup_Tooltiptext);
         AV15MoveDown = context.GetImagePath( "2aa88aca-af12-4417-abf9-461bf944ba5d", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMovedown_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15MoveDown)) ? AV28Movedown_GXI : context.convertURL( context.PathToRelativeUrl( AV15MoveDown))));
         AV28Movedown_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "2aa88aca-af12-4417-abf9-461bf944ba5d", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMovedown_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15MoveDown)) ? AV28Movedown_GXI : context.convertURL( context.PathToRelativeUrl( AV15MoveDown))));
         edtavMovedown_Tooltiptext = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMovedown_Internalname, "Tooltiptext", edtavMovedown_Tooltiptext);
         AV6UDelete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUdelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV6UDelete)) ? AV29Udelete_GXI : context.convertURL( context.PathToRelativeUrl( AV6UDelete))));
         AV29Udelete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUdelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV6UDelete)) ? AV29Udelete_GXI : context.convertURL( context.PathToRelativeUrl( AV6UDelete))));
         edtavUdelete_Tooltiptext = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUdelete_Internalname, "Tooltiptext", edtavUdelete_Tooltiptext);
         AV20GridGridStateCollectionsCurrentPage = subGridgridstatecollections_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GridGridStateCollectionsCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GridGridStateCollectionsCurrentPage), 10, 0)));
         AV21GridGridStateCollectionsPageCount = subGridgridstatecollections_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GridGridStateCollectionsPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21GridGridStateCollectionsPageCount), 10, 0)));
      }

      private void E15072( )
      {
         /* Gridgridstatecollections_Load Routine */
         AV24GXV1 = 1;
         while ( AV24GXV1 <= AV7GridStateCollection.Count )
         {
            AV7GridStateCollection.CurrentItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1));
            AV16MoveUp = context.GetImagePath( "18fea524-2fca-4d65-a716-0747be033f02", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavMoveup_Internalname, AV16MoveUp);
            AV27Moveup_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "18fea524-2fca-4d65-a716-0747be033f02", "", context.GetTheme( )));
            edtavMoveup_Tooltiptext = "";
            AV15MoveDown = context.GetImagePath( "2aa88aca-af12-4417-abf9-461bf944ba5d", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavMovedown_Internalname, AV15MoveDown);
            AV28Movedown_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "2aa88aca-af12-4417-abf9-461bf944ba5d", "", context.GetTheme( )));
            edtavMovedown_Tooltiptext = "";
            AV6UDelete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUdelete_Internalname, AV6UDelete);
            AV29Udelete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavUdelete_Tooltiptext = "";
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 14;
            }
            if ( ( subGridgridstatecollections_Islastpage == 1 ) || ( subGridgridstatecollections_Rows == 0 ) || ( ( GRIDGRIDSTATECOLLECTIONS_nCurrentRecord >= GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage ) && ( GRIDGRIDSTATECOLLECTIONS_nCurrentRecord < GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage + subGridgridstatecollections_Recordsperpage( ) ) ) )
            {
               sendrow_142( ) ;
               GRIDGRIDSTATECOLLECTIONS_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nEOF), 1, 0, ".", "")));
               if ( GRIDGRIDSTATECOLLECTIONS_nCurrentRecord + 1 >= subGridgridstatecollections_Recordcount( ) )
               {
                  GRIDGRIDSTATECOLLECTIONS_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDGRIDSTATECOLLECTIONS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDGRIDSTATECOLLECTIONS_nEOF), 1, 0, ".", "")));
               }
            }
            GRIDGRIDSTATECOLLECTIONS_nCurrentRecord = (long)(GRIDGRIDSTATECOLLECTIONS_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_14_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(14, GridgridstatecollectionsRow);
            }
            AV24GXV1 = (short)(AV24GXV1+1);
         }
      }

      protected void E11072( )
      {
         /* Gridgridstatecollectionspaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridgridstatecollectionspaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgridgridstatecollections_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridgridstatecollectionspaginationbar_Selectedpage, "Next") == 0 )
         {
            AV19PageToGo = subGridgridstatecollections_Currentpage( );
            AV19PageToGo = (int)(AV19PageToGo+1);
            subgridgridstatecollections_gotopage( AV19PageToGo) ;
         }
         else
         {
            AV19PageToGo = (int)(NumberUtil.Val( Gridgridstatecollectionspaginationbar_Selectedpage, "."));
            subgridgridstatecollections_gotopage( AV19PageToGo) ;
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12072 */
         E12072 ();
         if (returnInSub) return;
      }

      protected void E12072( )
      {
         AV24GXV1 = (short)(nGXsfl_14_idx+GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage);
         if ( AV7GridStateCollection.Count >= AV24GXV1 )
         {
            AV7GridStateCollection.CurrentItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1));
         }
         /* Enter Routine */
         AV12IsOK = true;
         AV30GXV2 = 1;
         while ( AV30GXV2 <= AV7GridStateCollection.Count )
         {
            AV8GridStateCollectionItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV30GXV2));
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV8GridStateCollectionItem.gxTpr_Title)) )
            {
               GX_msglist.addItem("Empty");
               AV12IsOK = false;
               if (true) break;
            }
            AV30GXV2 = (int)(AV30GXV2+1);
         }
         if ( AV12IsOK )
         {
            if ( AV18CollectionIsEmpty )
            {
               AV7GridStateCollection = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
               gx_BV14 = true;
            }
            new wwpbaseobjects.savemanagefiltersstate(context ).execute(  AV17UserKey,  AV7GridStateCollection.ToXml(false, true, "Items", "")) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17UserKey", AV17UserKey);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERKEY", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17UserKey, ""))));
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7GridStateCollection", AV7GridStateCollection);
         nGXsfl_14_bak_idx = nGXsfl_14_idx;
         gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         nGXsfl_14_idx = nGXsfl_14_bak_idx;
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
      }

      protected void E16072( )
      {
         AV24GXV1 = (short)(nGXsfl_14_idx+GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage);
         if ( AV7GridStateCollection.Count >= AV24GXV1 )
         {
            AV7GridStateCollection.CurrentItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1));
         }
         /* Moveup_Click Routine */
         AV10i = (short)(AV7GridStateCollection.IndexOf(((wwpbaseobjects.SdtGridStateCollection_Item)(AV7GridStateCollection.CurrentItem))));
         if ( AV10i > 1 )
         {
            AV8GridStateCollectionItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV10i));
            AV7GridStateCollection.RemoveItem(AV10i);
            gx_BV14 = true;
            AV7GridStateCollection.Add(AV8GridStateCollectionItem, AV10i-1);
            gx_BV14 = true;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7GridStateCollection", AV7GridStateCollection);
         nGXsfl_14_bak_idx = nGXsfl_14_idx;
         gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         nGXsfl_14_idx = nGXsfl_14_bak_idx;
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
      }

      protected void E17072( )
      {
         AV24GXV1 = (short)(nGXsfl_14_idx+GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage);
         if ( AV7GridStateCollection.Count >= AV24GXV1 )
         {
            AV7GridStateCollection.CurrentItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1));
         }
         /* Movedown_Click Routine */
         AV10i = (short)(AV7GridStateCollection.IndexOf(((wwpbaseobjects.SdtGridStateCollection_Item)(AV7GridStateCollection.CurrentItem))));
         if ( AV10i < AV7GridStateCollection.Count )
         {
            AV8GridStateCollectionItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV10i));
            AV7GridStateCollection.RemoveItem(AV10i);
            gx_BV14 = true;
            AV7GridStateCollection.Add(AV8GridStateCollectionItem, AV10i+1);
            gx_BV14 = true;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7GridStateCollection", AV7GridStateCollection);
         nGXsfl_14_bak_idx = nGXsfl_14_idx;
         gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         nGXsfl_14_idx = nGXsfl_14_bak_idx;
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
      }

      protected void E18072( )
      {
         AV24GXV1 = (short)(nGXsfl_14_idx+GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage);
         if ( AV7GridStateCollection.Count >= AV24GXV1 )
         {
            AV7GridStateCollection.CurrentItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1));
         }
         /* Udelete_Click Routine */
         AV10i = (short)(AV7GridStateCollection.IndexOf(((wwpbaseobjects.SdtGridStateCollection_Item)(AV7GridStateCollection.CurrentItem))));
         AV7GridStateCollection.RemoveItem(AV10i);
         gx_BV14 = true;
         if ( AV7GridStateCollection.Count == 0 )
         {
            AV18CollectionIsEmpty = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18CollectionIsEmpty", AV18CollectionIsEmpty);
         }
         gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7GridStateCollection", AV7GridStateCollection);
         nGXsfl_14_bak_idx = nGXsfl_14_idx;
         gxgrGridgridstatecollections_refresh( subGridgridstatecollections_Rows) ;
         nGXsfl_14_idx = nGXsfl_14_bak_idx;
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
      }

      protected void wb_table1_2_072( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='PopupContentCell'>") ;
            wb_table2_8_072( true) ;
         }
         else
         {
            wb_table2_8_072( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_072e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table3_25_072( true) ;
         }
         else
         {
            wb_table3_25_072( false) ;
         }
         return  ;
      }

      protected void wb_table3_25_072e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_33_072( true) ;
         }
         else
         {
            wb_table4_33_072( false) ;
         }
         return  ;
      }

      protected void wb_table4_33_072e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_072e( true) ;
         }
         else
         {
            wb_table1_2_072e( false) ;
         }
      }

      protected void wb_table4_33_072( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblDummytabletoimportbootstrapcss_Internalname, tblDummytabletoimportbootstrapcss_Internalname, "", "Invisible", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_UNNAMEDTABLE1Container"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_38_072( true) ;
         }
         else
         {
            wb_table5_38_072( false) ;
         }
         return  ;
      }

      protected void wb_table5_38_072e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_33_072e( true) ;
         }
         else
         {
            wb_table4_33_072e( false) ;
         }
      }

      protected void wb_table5_38_072( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_38_072e( true) ;
         }
         else
         {
            wb_table5_38_072e( false) ;
         }
      }

      protected void wb_table3_25_072( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(14), 2, 0)+","+"null"+");", "Salvar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWPBaseObjects\\ManageFilters.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(14), 2, 0)+","+"null"+");", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWPBaseObjects\\ManageFilters.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_25_072e( true) ;
         }
         else
         {
            wb_table3_25_072e( false) ;
         }
      }

      protected void wb_table2_8_072( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_11_072( true) ;
         }
         else
         {
            wb_table6_11_072( false) ;
         }
         return  ;
      }

      protected void wb_table6_11_072e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_072e( true) ;
         }
         else
         {
            wb_table2_8_072e( false) ;
         }
      }

      protected void wb_table6_11_072( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridgridstatecollectionstablewithpaginationbar_Internalname, tblGridgridstatecollectionstablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridgridstatecollectionsContainer.SetWrapped(nGXWrapped);
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridgridstatecollectionsContainer"+"DivS\" data-gxgridid=\"14\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridgridstatecollections_Internalname, subGridgridstatecollections_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridgridstatecollections_Backcolorstyle == 0 )
               {
                  subGridgridstatecollections_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridgridstatecollections_Class) > 0 )
                  {
                     subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"Title";
                  }
               }
               else
               {
                  subGridgridstatecollections_Titlebackstyle = 1;
                  if ( subGridgridstatecollections_Backcolorstyle == 1 )
                  {
                     subGridgridstatecollections_Titlebackcolor = subGridgridstatecollections_Allbackcolor;
                     if ( StringUtil.Len( subGridgridstatecollections_Class) > 0 )
                     {
                        subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridgridstatecollections_Class) > 0 )
                     {
                        subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(15), 4, 0))+"px"+" class=\""+subGridgridstatecollections_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(15), 4, 0))+"px"+" class=\""+subGridgridstatecollections_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridgridstatecollections_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Filtros") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridgridstatecollections_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Grid State XML") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridgridstatecollections_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridgridstatecollectionsContainer.AddObjectProperty("GridName", "Gridgridstatecollections");
            }
            else
            {
               GridgridstatecollectionsContainer.AddObjectProperty("GridName", "Gridgridstatecollections");
               GridgridstatecollectionsContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridgridstatecollectionsContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Backcolorstyle), 1, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("CmpContext", "");
               GridgridstatecollectionsContainer.AddObjectProperty("InMasterPage", "false");
               GridgridstatecollectionsColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridgridstatecollectionsColumn.AddObjectProperty("Value", context.convertURL( AV16MoveUp));
               GridgridstatecollectionsColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavMoveup_Tooltiptext));
               GridgridstatecollectionsContainer.AddColumnProperties(GridgridstatecollectionsColumn);
               GridgridstatecollectionsColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridgridstatecollectionsColumn.AddObjectProperty("Value", context.convertURL( AV15MoveDown));
               GridgridstatecollectionsColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavMovedown_Tooltiptext));
               GridgridstatecollectionsContainer.AddColumnProperties(GridgridstatecollectionsColumn);
               GridgridstatecollectionsColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridgridstatecollectionsContainer.AddColumnProperties(GridgridstatecollectionsColumn);
               GridgridstatecollectionsColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridgridstatecollectionsColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGridstatecollection__gridstatexml_Enabled), 5, 0, ".", "")));
               GridgridstatecollectionsContainer.AddColumnProperties(GridgridstatecollectionsColumn);
               GridgridstatecollectionsColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridgridstatecollectionsColumn.AddObjectProperty("Value", context.convertURL( AV6UDelete));
               GridgridstatecollectionsColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUdelete_Tooltiptext));
               GridgridstatecollectionsContainer.AddColumnProperties(GridgridstatecollectionsColumn);
               GridgridstatecollectionsContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Allowselection), 1, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Selectioncolor), 9, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Allowhovering), 1, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Hoveringcolor), 9, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Allowcollapsing), 1, 0, ".", "")));
               GridgridstatecollectionsContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridgridstatecollections_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 14 )
         {
            wbEnd = 0;
            nRC_GXsfl_14 = (short)(nGXsfl_14_idx-1);
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV24GXV1 = nGXsfl_14_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridgridstatecollectionsContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridgridstatecollections", GridgridstatecollectionsContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridgridstatecollectionsContainerData", GridgridstatecollectionsContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridgridstatecollectionsContainerData"+"V", GridgridstatecollectionsContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridgridstatecollectionsContainerData"+"V"+"\" value='"+GridgridstatecollectionsContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDGRIDSTATECOLLECTIONSPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_11_072e( true) ;
         }
         else
         {
            wb_table6_11_072e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV17UserKey = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17UserKey", AV17UserKey);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERKEY", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17UserKey, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA072( ) ;
         WS072( ) ;
         WE072( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117303455");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwpbaseobjects/managefilters.js", "?20203117303455");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_142( )
      {
         edtavMoveup_Internalname = "vMOVEUP_"+sGXsfl_14_idx;
         edtavMovedown_Internalname = "vMOVEDOWN_"+sGXsfl_14_idx;
         edtavGridstatecollection__title_Internalname = "GRIDSTATECOLLECTION__TITLE_"+sGXsfl_14_idx;
         edtavGridstatecollection__gridstatexml_Internalname = "GRIDSTATECOLLECTION__GRIDSTATEXML_"+sGXsfl_14_idx;
         edtavUdelete_Internalname = "vUDELETE_"+sGXsfl_14_idx;
      }

      protected void SubsflControlProps_fel_142( )
      {
         edtavMoveup_Internalname = "vMOVEUP_"+sGXsfl_14_fel_idx;
         edtavMovedown_Internalname = "vMOVEDOWN_"+sGXsfl_14_fel_idx;
         edtavGridstatecollection__title_Internalname = "GRIDSTATECOLLECTION__TITLE_"+sGXsfl_14_fel_idx;
         edtavGridstatecollection__gridstatexml_Internalname = "GRIDSTATECOLLECTION__GRIDSTATEXML_"+sGXsfl_14_fel_idx;
         edtavUdelete_Internalname = "vUDELETE_"+sGXsfl_14_fel_idx;
      }

      protected void sendrow_142( )
      {
         SubsflControlProps_142( ) ;
         WB070( ) ;
         if ( ( subGridgridstatecollections_Rows * 1 == 0 ) || ( nGXsfl_14_idx <= subGridgridstatecollections_Recordsperpage( ) * 1 ) )
         {
            GridgridstatecollectionsRow = GXWebRow.GetNew(context,GridgridstatecollectionsContainer);
            if ( subGridgridstatecollections_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridgridstatecollections_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridgridstatecollections_Class, "") != 0 )
               {
                  subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"Odd";
               }
            }
            else if ( subGridgridstatecollections_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridgridstatecollections_Backstyle = 0;
               subGridgridstatecollections_Backcolor = subGridgridstatecollections_Allbackcolor;
               if ( StringUtil.StrCmp(subGridgridstatecollections_Class, "") != 0 )
               {
                  subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"Uniform";
               }
            }
            else if ( subGridgridstatecollections_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridgridstatecollections_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridgridstatecollections_Class, "") != 0 )
               {
                  subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"Odd";
               }
               subGridgridstatecollections_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridgridstatecollections_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridgridstatecollections_Backstyle = 1;
               if ( ((int)((nGXsfl_14_idx) % (2))) == 0 )
               {
                  subGridgridstatecollections_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridgridstatecollections_Class, "") != 0 )
                  {
                     subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"Even";
                  }
               }
               else
               {
                  subGridgridstatecollections_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridgridstatecollections_Class, "") != 0 )
                  {
                     subGridgridstatecollections_Linesclass = subGridgridstatecollections_Class+"Odd";
                  }
               }
            }
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridgridstatecollections_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_14_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavMoveup_Enabled!=0)&&(edtavMoveup_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'',false,'',14)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV16MoveUp_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16MoveUp))&&String.IsNullOrEmpty(StringUtil.RTrim( AV27Moveup_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16MoveUp)));
            GridgridstatecollectionsRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavMoveup_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16MoveUp)) ? AV27Moveup_GXI : context.PathToRelativeUrl( AV16MoveUp)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavMoveup_Tooltiptext,(short)0,(short)1,(short)15,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavMoveup_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVMOVEUP.CLICK."+sGXsfl_14_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV16MoveUp_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavMovedown_Enabled!=0)&&(edtavMovedown_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'',false,'',14)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV15MoveDown_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15MoveDown))&&String.IsNullOrEmpty(StringUtil.RTrim( AV28Movedown_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15MoveDown)));
            GridgridstatecollectionsRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavMovedown_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15MoveDown)) ? AV28Movedown_GXI : context.PathToRelativeUrl( AV15MoveDown)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavMovedown_Tooltiptext,(short)0,(short)1,(short)15,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavMovedown_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVMOVEDOWN.CLICK."+sGXsfl_14_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV15MoveDown_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavGridstatecollection__title_Enabled!=0)&&(edtavGridstatecollection__title_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'',false,'"+sGXsfl_14_idx+"',14)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridgridstatecollectionsRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavGridstatecollection__title_Internalname,((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1)).gxTpr_Title,(String)"",TempTags+((edtavGridstatecollection__title_Enabled!=0)&&(edtavGridstatecollection__title_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavGridstatecollection__title_Enabled!=0)&&(edtavGridstatecollection__title_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,17);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavGridstatecollection__title_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridgridstatecollectionsRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavGridstatecollection__gridstatexml_Internalname,((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1)).gxTpr_Gridstatexml,((wwpbaseobjects.SdtGridStateCollection_Item)AV7GridStateCollection.Item(AV24GXV1)).gxTpr_Gridstatexml,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavGridstatecollection__gridstatexml_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavGridstatecollection__gridstatexml_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)14,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridgridstatecollectionsContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavUdelete_Enabled!=0)&&(edtavUdelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 19,'',false,'',14)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV6UDelete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV6UDelete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV29Udelete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV6UDelete)));
            GridgridstatecollectionsRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUdelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV6UDelete)) ? AV29Udelete_GXI : context.PathToRelativeUrl( AV6UDelete)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUdelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavUdelete_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVUDELETE.CLICK."+sGXsfl_14_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV6UDelete_IsBlob,(bool)false});
            GridgridstatecollectionsContainer.AddRow(GridgridstatecollectionsRow);
            nGXsfl_14_idx = (short)(((subGridgridstatecollections_Islastpage==1)&&(nGXsfl_14_idx+1>subGridgridstatecollections_Recordsperpage( )) ? 1 : nGXsfl_14_idx+1));
            sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
            SubsflControlProps_142( ) ;
         }
         /* End function sendrow_142 */
      }

      protected void init_default_properties( )
      {
         edtavMoveup_Internalname = "vMOVEUP";
         edtavMovedown_Internalname = "vMOVEDOWN";
         edtavGridstatecollection__title_Internalname = "GRIDSTATECOLLECTION__TITLE";
         edtavGridstatecollection__gridstatexml_Internalname = "GRIDSTATECOLLECTION__GRIDSTATEXML";
         edtavUdelete_Internalname = "vUDELETE";
         Gridgridstatecollectionspaginationbar_Internalname = "GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR";
         tblGridgridstatecollectionstablewithpaginationbar_Internalname = "GRIDGRIDSTATECOLLECTIONSTABLEWITHPAGINATIONBAR";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Dvpanel_unnamedtable1_Internalname = "DVPANEL_UNNAMEDTABLE1";
         tblDummytabletoimportbootstrapcss_Internalname = "DUMMYTABLETOIMPORTBOOTSTRAPCSS";
         tblTablemain_Internalname = "TABLEMAIN";
         chkavCollectionisempty_Internalname = "vCOLLECTIONISEMPTY";
         Form.Internalname = "FORM";
         subGridgridstatecollections_Internalname = "GRIDGRIDSTATECOLLECTIONS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavUdelete_Jsonclick = "";
         edtavUdelete_Visible = -1;
         edtavUdelete_Enabled = 1;
         edtavGridstatecollection__gridstatexml_Jsonclick = "";
         edtavGridstatecollection__title_Jsonclick = "";
         edtavGridstatecollection__title_Visible = -1;
         edtavGridstatecollection__title_Enabled = 1;
         edtavMovedown_Jsonclick = "";
         edtavMovedown_Visible = -1;
         edtavMovedown_Enabled = 1;
         edtavMoveup_Jsonclick = "";
         edtavMoveup_Visible = -1;
         edtavMoveup_Enabled = 1;
         subGridgridstatecollections_Allowcollapsing = 0;
         subGridgridstatecollections_Allowselection = 0;
         edtavGridstatecollection__gridstatexml_Enabled = 0;
         subGridgridstatecollections_Class = "WorkWithBorder WorkWith";
         edtavUdelete_Tooltiptext = "";
         edtavMovedown_Tooltiptext = "";
         edtavMoveup_Tooltiptext = "";
         subGridgridstatecollections_Backcolorstyle = 3;
         edtavGridstatecollection__gridstatexml_Enabled = -1;
         chkavCollectionisempty.Caption = "";
         chkavCollectionisempty.Visible = 1;
         Dvpanel_unnamedtable1_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Iconposition = "left";
         Dvpanel_unnamedtable1_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable1_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable1_Title = "";
         Dvpanel_unnamedtable1_Cls = "GXUI-DVelop-Panel";
         Dvpanel_unnamedtable1_Width = "100%";
         Gridgridstatecollectionspaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridgridstatecollectionspaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridgridstatecollectionspaginationbar_Pagingcaptionposition = "Left";
         Gridgridstatecollectionspaginationbar_Pagingbuttonsposition = "Right";
         Gridgridstatecollectionspaginationbar_Pagestoshow = 5;
         Gridgridstatecollectionspaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridgridstatecollectionspaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridgridstatecollectionspaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridgridstatecollectionspaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridgridstatecollectionspaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridgridstatecollectionspaginationbar_Last = "�|";
         Gridgridstatecollectionspaginationbar_Next = "�";
         Gridgridstatecollectionspaginationbar_Previous = "�";
         Gridgridstatecollectionspaginationbar_First = "|�";
         Gridgridstatecollectionspaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Gerenciador de filtros";
         subGridgridstatecollections_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'subGridgridstatecollections_Rows',nv:0}],oparms:[{av:'AV16MoveUp',fld:'vMOVEUP',pic:'',nv:''},{av:'edtavMoveup_Tooltiptext',ctrl:'vMOVEUP',prop:'Tooltiptext'},{av:'AV15MoveDown',fld:'vMOVEDOWN',pic:'',nv:''},{av:'edtavMovedown_Tooltiptext',ctrl:'vMOVEDOWN',prop:'Tooltiptext'},{av:'AV6UDelete',fld:'vUDELETE',pic:'',nv:''},{av:'edtavUdelete_Tooltiptext',ctrl:'vUDELETE',prop:'Tooltiptext'},{av:'AV20GridGridStateCollectionsCurrentPage',fld:'vGRIDGRIDSTATECOLLECTIONSCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV21GridGridStateCollectionsPageCount',fld:'vGRIDGRIDSTATECOLLECTIONSPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDGRIDSTATECOLLECTIONS.LOAD","{handler:'E15072',iparms:[],oparms:[{av:'AV16MoveUp',fld:'vMOVEUP',pic:'',nv:''},{av:'edtavMoveup_Tooltiptext',ctrl:'vMOVEUP',prop:'Tooltiptext'},{av:'AV15MoveDown',fld:'vMOVEDOWN',pic:'',nv:''},{av:'edtavMovedown_Tooltiptext',ctrl:'vMOVEDOWN',prop:'Tooltiptext'},{av:'AV6UDelete',fld:'vUDELETE',pic:'',nv:''},{av:'edtavUdelete_Tooltiptext',ctrl:'vUDELETE',prop:'Tooltiptext'}]}");
         setEventMetadata("GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR.CHANGEPAGE","{handler:'E11072',iparms:[{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'subGridgridstatecollections_Rows',nv:0},{av:'Gridgridstatecollectionspaginationbar_Selectedpage',ctrl:'GRIDGRIDSTATECOLLECTIONSPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12072',iparms:[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'AV18CollectionIsEmpty',fld:'vCOLLECTIONISEMPTY',pic:'',nv:false},{av:'AV17UserKey',fld:'vUSERKEY',pic:'',hsh:true,nv:''},{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'subGridgridstatecollections_Rows',nv:0}],oparms:[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null}]}");
         setEventMetadata("VMOVEUP.CLICK","{handler:'E16072',iparms:[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'subGridgridstatecollections_Rows',nv:0}],oparms:[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null}]}");
         setEventMetadata("VMOVEDOWN.CLICK","{handler:'E17072',iparms:[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'subGridgridstatecollections_Rows',nv:0}],oparms:[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null}]}");
         setEventMetadata("VUDELETE.CLICK","{handler:'E18072',iparms:[{av:'GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage',nv:0},{av:'GRIDGRIDSTATECOLLECTIONS_nEOF',nv:0},{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'subGridgridstatecollections_Rows',nv:0}],oparms:[{av:'AV7GridStateCollection',fld:'vGRIDSTATECOLLECTION',grid:14,pic:'',nv:null},{av:'AV18CollectionIsEmpty',fld:'vCOLLECTIONISEMPTY',pic:'',nv:false}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV17UserKey = "";
         Gridgridstatecollectionspaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7GridStateCollection = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16MoveUp = "";
         AV27Moveup_GXI = "";
         AV15MoveDown = "";
         AV28Movedown_GXI = "";
         AV6UDelete = "";
         AV29Udelete_GXI = "";
         GridgridstatecollectionsContainer = new GXWebGrid( context);
         AV9HTTPRequest = new GxHttpRequest( context);
         GridgridstatecollectionsRow = new GXWebRow();
         AV8GridStateCollectionItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         sStyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         subGridgridstatecollections_Linesclass = "";
         GridgridstatecollectionsColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavGridstatecollection__gridstatexml_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_14 ;
      private short nGXsfl_14_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRIDGRIDSTATECOLLECTIONS_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV24GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_14_Refreshing=0 ;
      private short subGridgridstatecollections_Backcolorstyle ;
      private short nGXsfl_14_fel_idx=1 ;
      private short nGXsfl_14_bak_idx=1 ;
      private short AV10i ;
      private short subGridgridstatecollections_Titlebackstyle ;
      private short subGridgridstatecollections_Allowselection ;
      private short subGridgridstatecollections_Allowhovering ;
      private short subGridgridstatecollections_Allowcollapsing ;
      private short subGridgridstatecollections_Collapsed ;
      private short nGXWrapped ;
      private short subGridgridstatecollections_Backstyle ;
      private int subGridgridstatecollections_Rows ;
      private int Gridgridstatecollectionspaginationbar_Pagestoshow ;
      private int subGridgridstatecollections_Islastpage ;
      private int edtavGridstatecollection__gridstatexml_Enabled ;
      private int GRIDGRIDSTATECOLLECTIONS_nGridOutOfScope ;
      private int AV19PageToGo ;
      private int AV30GXV2 ;
      private int subGridgridstatecollections_Titlebackcolor ;
      private int subGridgridstatecollections_Allbackcolor ;
      private int subGridgridstatecollections_Selectioncolor ;
      private int subGridgridstatecollections_Hoveringcolor ;
      private int idxLst ;
      private int subGridgridstatecollections_Backcolor ;
      private int edtavMoveup_Enabled ;
      private int edtavMoveup_Visible ;
      private int edtavMovedown_Enabled ;
      private int edtavMovedown_Visible ;
      private int edtavGridstatecollection__title_Enabled ;
      private int edtavGridstatecollection__title_Visible ;
      private int edtavUdelete_Enabled ;
      private int edtavUdelete_Visible ;
      private long GRIDGRIDSTATECOLLECTIONS_nFirstRecordOnPage ;
      private long AV20GridGridStateCollectionsCurrentPage ;
      private long AV21GridGridStateCollectionsPageCount ;
      private long GRIDGRIDSTATECOLLECTIONS_nCurrentRecord ;
      private long GRIDGRIDSTATECOLLECTIONS_nRecordCount ;
      private String Gridgridstatecollectionspaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_14_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridgridstatecollectionspaginationbar_Class ;
      private String Gridgridstatecollectionspaginationbar_First ;
      private String Gridgridstatecollectionspaginationbar_Previous ;
      private String Gridgridstatecollectionspaginationbar_Next ;
      private String Gridgridstatecollectionspaginationbar_Last ;
      private String Gridgridstatecollectionspaginationbar_Caption ;
      private String Gridgridstatecollectionspaginationbar_Pagingbuttonsposition ;
      private String Gridgridstatecollectionspaginationbar_Pagingcaptionposition ;
      private String Gridgridstatecollectionspaginationbar_Emptygridclass ;
      private String Gridgridstatecollectionspaginationbar_Emptygridcaption ;
      private String Dvpanel_unnamedtable1_Width ;
      private String Dvpanel_unnamedtable1_Cls ;
      private String Dvpanel_unnamedtable1_Title ;
      private String Dvpanel_unnamedtable1_Iconposition ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavCollectionisempty_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavMoveup_Internalname ;
      private String edtavMovedown_Internalname ;
      private String edtavUdelete_Internalname ;
      private String edtavGridstatecollection__gridstatexml_Internalname ;
      private String sGXsfl_14_fel_idx="0001" ;
      private String edtavMoveup_Tooltiptext ;
      private String edtavMovedown_Tooltiptext ;
      private String edtavUdelete_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblDummytabletoimportbootstrapcss_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblGridgridstatecollectionstablewithpaginationbar_Internalname ;
      private String subGridgridstatecollections_Internalname ;
      private String subGridgridstatecollections_Class ;
      private String subGridgridstatecollections_Linesclass ;
      private String edtavGridstatecollection__title_Internalname ;
      private String edtavMoveup_Jsonclick ;
      private String edtavMovedown_Jsonclick ;
      private String ROClassString ;
      private String edtavGridstatecollection__title_Jsonclick ;
      private String edtavGridstatecollection__gridstatexml_Jsonclick ;
      private String edtavUdelete_Jsonclick ;
      private String Gridgridstatecollectionspaginationbar_Internalname ;
      private String Dvpanel_unnamedtable1_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Gridgridstatecollectionspaginationbar_Showfirst ;
      private bool Gridgridstatecollectionspaginationbar_Showprevious ;
      private bool Gridgridstatecollectionspaginationbar_Shownext ;
      private bool Gridgridstatecollectionspaginationbar_Showlast ;
      private bool Dvpanel_unnamedtable1_Collapsible ;
      private bool Dvpanel_unnamedtable1_Collapsed ;
      private bool Dvpanel_unnamedtable1_Autowidth ;
      private bool Dvpanel_unnamedtable1_Autoheight ;
      private bool Dvpanel_unnamedtable1_Showcollapseicon ;
      private bool Dvpanel_unnamedtable1_Autoscroll ;
      private bool wbLoad ;
      private bool AV18CollectionIsEmpty ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_BV14 ;
      private bool gx_refresh_fired ;
      private bool AV12IsOK ;
      private bool AV16MoveUp_IsBlob ;
      private bool AV15MoveDown_IsBlob ;
      private bool AV6UDelete_IsBlob ;
      private String AV17UserKey ;
      private String wcpOAV17UserKey ;
      private String AV27Moveup_GXI ;
      private String AV28Movedown_GXI ;
      private String AV29Udelete_GXI ;
      private String AV16MoveUp ;
      private String AV15MoveDown ;
      private String AV6UDelete ;
      private GXWebGrid GridgridstatecollectionsContainer ;
      private GXWebRow GridgridstatecollectionsRow ;
      private GXWebColumn GridgridstatecollectionsColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavCollectionisempty ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV7GridStateCollection ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV8GridStateCollectionItem ;
   }

}
