/*
               File: wwpbaseobjects.type_SdtGoogleDocsResult
        Description: WWPBaseObjects\GoogleDocsResult
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:8.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Result" )]
   [XmlType(TypeName =  "Result" , Namespace = "DVelop.Extensions.GoogleDocs" )]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtGoogleDocsResult_Doc ))]
   [Serializable]
   public class SdtGoogleDocsResult : GxUserType
   {
      public SdtGoogleDocsResult( )
      {
         /* Constructor for serialization */
         gxTv_SdtGoogleDocsResult_Error = "";
      }

      public SdtGoogleDocsResult( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtGoogleDocsResult deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "DVelop.Extensions.GoogleDocs" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtGoogleDocsResult)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtGoogleDocsResult obj ;
         obj = this;
         obj.gxTpr_Success = deserialized.gxTpr_Success;
         obj.gxTpr_Error = deserialized.gxTpr_Error;
         obj.gxTpr_Docs = deserialized.gxTpr_Docs;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Success") )
               {
                  gxTv_SdtGoogleDocsResult_Success = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Error") )
               {
                  gxTv_SdtGoogleDocsResult_Error = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Docs") )
               {
                  if ( gxTv_SdtGoogleDocsResult_Docs == null )
                  {
                     gxTv_SdtGoogleDocsResult_Docs = new GxObjectCollection( context, "GoogleDocsResult.Doc", "DVelop.Extensions.GoogleDocs", "wwpbaseobjects.SdtGoogleDocsResult_Doc", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtGoogleDocsResult_Docs.readxmlcollection(oReader, "Docs", "Doc");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Result";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "DVelop.Extensions.GoogleDocs";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Success", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtGoogleDocsResult_Success)));
         if ( StringUtil.StrCmp(sNameSpace, "DVelop.Extensions.GoogleDocs") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "DVelop.Extensions.GoogleDocs");
         }
         oWriter.WriteElement("Error", StringUtil.RTrim( gxTv_SdtGoogleDocsResult_Error));
         if ( StringUtil.StrCmp(sNameSpace, "DVelop.Extensions.GoogleDocs") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "DVelop.Extensions.GoogleDocs");
         }
         if ( gxTv_SdtGoogleDocsResult_Docs != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "DVelop.Extensions.GoogleDocs") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "DVelop.Extensions.GoogleDocs";
            }
            else
            {
               sNameSpace1 = "DVelop.Extensions.GoogleDocs";
            }
            gxTv_SdtGoogleDocsResult_Docs.writexmlcollection(oWriter, "Docs", sNameSpace1, "Doc", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Success", gxTv_SdtGoogleDocsResult_Success, false);
         AddObjectProperty("Error", gxTv_SdtGoogleDocsResult_Error, false);
         if ( gxTv_SdtGoogleDocsResult_Docs != null )
         {
            AddObjectProperty("Docs", gxTv_SdtGoogleDocsResult_Docs, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Success" )]
      [  XmlElement( ElementName = "Success"   )]
      public bool gxTpr_Success
      {
         get {
            return gxTv_SdtGoogleDocsResult_Success ;
         }

         set {
            gxTv_SdtGoogleDocsResult_Success = value;
         }

      }

      [  SoapElement( ElementName = "Error" )]
      [  XmlElement( ElementName = "Error"   )]
      public String gxTpr_Error
      {
         get {
            return gxTv_SdtGoogleDocsResult_Error ;
         }

         set {
            gxTv_SdtGoogleDocsResult_Error = (String)(value);
         }

      }

      public class gxTv_SdtGoogleDocsResult_Docs_wwpbaseobjects_SdtGoogleDocsResult_Doc_80compatibility:wwpbaseobjects.SdtGoogleDocsResult_Doc {}
      [  SoapElement( ElementName = "Docs" )]
      [  XmlArray( ElementName = "Docs"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtGoogleDocsResult_Doc ), ElementName= "Doc"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtGoogleDocsResult_Docs_wwpbaseobjects_SdtGoogleDocsResult_Doc_80compatibility ), ElementName= "GoogleDocsResult.Doc"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Docs_GxObjectCollection
      {
         get {
            if ( gxTv_SdtGoogleDocsResult_Docs == null )
            {
               gxTv_SdtGoogleDocsResult_Docs = new GxObjectCollection( context, "GoogleDocsResult.Doc", "DVelop.Extensions.GoogleDocs", "wwpbaseobjects.SdtGoogleDocsResult_Doc", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtGoogleDocsResult_Docs ;
         }

         set {
            if ( gxTv_SdtGoogleDocsResult_Docs == null )
            {
               gxTv_SdtGoogleDocsResult_Docs = new GxObjectCollection( context, "GoogleDocsResult.Doc", "DVelop.Extensions.GoogleDocs", "wwpbaseobjects.SdtGoogleDocsResult_Doc", "GeneXus.Programs");
            }
            gxTv_SdtGoogleDocsResult_Docs = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Docs
      {
         get {
            if ( gxTv_SdtGoogleDocsResult_Docs == null )
            {
               gxTv_SdtGoogleDocsResult_Docs = new GxObjectCollection( context, "GoogleDocsResult.Doc", "DVelop.Extensions.GoogleDocs", "wwpbaseobjects.SdtGoogleDocsResult_Doc", "GeneXus.Programs");
            }
            return gxTv_SdtGoogleDocsResult_Docs ;
         }

         set {
            gxTv_SdtGoogleDocsResult_Docs = value;
         }

      }

      public void gxTv_SdtGoogleDocsResult_Docs_SetNull( )
      {
         gxTv_SdtGoogleDocsResult_Docs = null;
         return  ;
      }

      public bool gxTv_SdtGoogleDocsResult_Docs_IsNull( )
      {
         if ( gxTv_SdtGoogleDocsResult_Docs == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtGoogleDocsResult_Error = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected bool gxTv_SdtGoogleDocsResult_Success ;
      protected String gxTv_SdtGoogleDocsResult_Error ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGoogleDocsResult_Doc ))]
      protected IGxCollection gxTv_SdtGoogleDocsResult_Docs=null ;
   }

   [DataContract(Name = @"WWPBaseObjects\GoogleDocsResult", Namespace = "DVelop.Extensions.GoogleDocs")]
   public class SdtGoogleDocsResult_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtGoogleDocsResult>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGoogleDocsResult_RESTInterface( ) : base()
      {
      }

      public SdtGoogleDocsResult_RESTInterface( wwpbaseobjects.SdtGoogleDocsResult psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Success" , Order = 0 )]
      public bool gxTpr_Success
      {
         get {
            return sdt.gxTpr_Success ;
         }

         set {
            sdt.gxTpr_Success = value;
         }

      }

      [DataMember( Name = "Error" , Order = 1 )]
      public String gxTpr_Error
      {
         get {
            return sdt.gxTpr_Error ;
         }

         set {
            sdt.gxTpr_Error = (String)(value);
         }

      }

      [DataMember( Name = "Docs" , Order = 2 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtGoogleDocsResult_Doc_RESTInterface> gxTpr_Docs
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtGoogleDocsResult_Doc_RESTInterface>(sdt.gxTpr_Docs) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Docs);
         }

      }

      public wwpbaseobjects.SdtGoogleDocsResult sdt
      {
         get {
            return (wwpbaseobjects.SdtGoogleDocsResult)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtGoogleDocsResult() ;
         }
      }

   }

}
