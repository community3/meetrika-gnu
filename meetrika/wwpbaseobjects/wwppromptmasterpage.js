/**@preserve  GeneXus C# 10_3_14-114418 on 3/1/2020 19:5:18.22
*/
gx.evt.autoSkip = false;
gx.define('wwpbaseobjects.wwppromptmasterpage', false, function () {
   this.ServerClass =  "wwpbaseobjects.wwppromptmasterpage" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.IsMasterPage=true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.e13nj2_client=function()
   {
      this.executeServerEvent("ENTER_MPAGE", true, null, false, false);
   };
   this.e14nj2_client=function()
   {
      this.executeServerEvent("CANCEL_MPAGE", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,3,4,5,6,7,8,9];
   this.GXLastCtrlId =9;
   GXValidFnc[2]={fld:"LAYOUT_UNNAMEDTABLE1",grid:0};
   GXValidFnc[3]={fld:"",grid:0};
   GXValidFnc[4]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[5]={fld:"",grid:0};
   GXValidFnc[6]={fld:"",grid:0};
   GXValidFnc[7]={fld:"UTCONTENTHOLDER",grid:0};
   GXValidFnc[8]={fld:"",grid:0};
   GXValidFnc[9]={fld:"",grid:0};
   this.Events = {"e13nj2_client": ["ENTER_MPAGE", true] ,"e14nj2_client": ["CANCEL_MPAGE", true]};
   this.EvtParms["REFRESH_MPAGE"] = [[],[]];
   this.InitStandaloneVars( );
});
gx.createMasterPage(wwpbaseobjects.wwppromptmasterpage);
