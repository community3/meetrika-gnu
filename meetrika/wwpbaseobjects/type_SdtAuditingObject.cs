/*
               File: wwpbaseobjects.type_SdtAuditingObject
        Description: WWPBaseObjects\AuditingObject
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 9:44:36.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "AuditingObject" )]
   [XmlType(TypeName =  "AuditingObject" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtAuditingObject_RecordItem ))]
   [Serializable]
   public class SdtAuditingObject : GxUserType
   {
      public SdtAuditingObject( )
      {
         /* Constructor for serialization */
         gxTv_SdtAuditingObject_Mode = "";
      }

      public SdtAuditingObject( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtAuditingObject deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtAuditingObject)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtAuditingObject obj ;
         obj = this;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Record = deserialized.gxTpr_Record;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAuditingObject_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Record") )
               {
                  if ( gxTv_SdtAuditingObject_Record == null )
                  {
                     gxTv_SdtAuditingObject_Record = new GxObjectCollection( context, "AuditingObject.RecordItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtAuditingObject_RecordItem", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtAuditingObject_Record.readxmlcollection(oReader, "Record", "RecordItem");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "AuditingObject";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAuditingObject_Mode));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtAuditingObject_Record != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtAuditingObject_Record.writexmlcollection(oWriter, "Record", sNameSpace1, "RecordItem", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Mode", gxTv_SdtAuditingObject_Mode, false);
         if ( gxTv_SdtAuditingObject_Record != null )
         {
            AddObjectProperty("Record", gxTv_SdtAuditingObject_Record, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAuditingObject_Mode ;
         }

         set {
            gxTv_SdtAuditingObject_Mode = (String)(value);
         }

      }

      public class gxTv_SdtAuditingObject_Record_wwpbaseobjects_SdtAuditingObject_RecordItem_80compatibility:wwpbaseobjects.SdtAuditingObject_RecordItem {}
      [  SoapElement( ElementName = "Record" )]
      [  XmlArray( ElementName = "Record"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtAuditingObject_RecordItem ), ElementName= "RecordItem"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtAuditingObject_Record_wwpbaseobjects_SdtAuditingObject_RecordItem_80compatibility ), ElementName= "AuditingObject.RecordItem"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Record_GxObjectCollection
      {
         get {
            if ( gxTv_SdtAuditingObject_Record == null )
            {
               gxTv_SdtAuditingObject_Record = new GxObjectCollection( context, "AuditingObject.RecordItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtAuditingObject_RecordItem", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtAuditingObject_Record ;
         }

         set {
            if ( gxTv_SdtAuditingObject_Record == null )
            {
               gxTv_SdtAuditingObject_Record = new GxObjectCollection( context, "AuditingObject.RecordItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtAuditingObject_RecordItem", "GeneXus.Programs");
            }
            gxTv_SdtAuditingObject_Record = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Record
      {
         get {
            if ( gxTv_SdtAuditingObject_Record == null )
            {
               gxTv_SdtAuditingObject_Record = new GxObjectCollection( context, "AuditingObject.RecordItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtAuditingObject_RecordItem", "GeneXus.Programs");
            }
            return gxTv_SdtAuditingObject_Record ;
         }

         set {
            gxTv_SdtAuditingObject_Record = value;
         }

      }

      public void gxTv_SdtAuditingObject_Record_SetNull( )
      {
         gxTv_SdtAuditingObject_Record = null;
         return  ;
      }

      public bool gxTv_SdtAuditingObject_Record_IsNull( )
      {
         if ( gxTv_SdtAuditingObject_Record == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAuditingObject_Mode = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtAuditingObject_Mode ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtAuditingObject_RecordItem ))]
      protected IGxCollection gxTv_SdtAuditingObject_Record=null ;
   }

   [DataContract(Name = @"WWPBaseObjects\AuditingObject", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtAuditingObject_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtAuditingObject>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAuditingObject_RESTInterface( ) : base()
      {
      }

      public SdtAuditingObject_RESTInterface( wwpbaseobjects.SdtAuditingObject psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Mode" , Order = 0 )]
      public String gxTpr_Mode
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Mode) ;
         }

         set {
            sdt.gxTpr_Mode = (String)(value);
         }

      }

      [DataMember( Name = "Record" , Order = 1 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem_RESTInterface> gxTpr_Record
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtAuditingObject_RecordItem_RESTInterface>(sdt.gxTpr_Record) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Record);
         }

      }

      public wwpbaseobjects.SdtAuditingObject sdt
      {
         get {
            return (wwpbaseobjects.SdtAuditingObject)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtAuditingObject() ;
         }
      }

   }

}
