/*
               File: wwpbaseobjects.type_SdtDVB_SidebarMenuOptionsData_Item
        Description: WWPBaseObjects\DVB_SidebarMenuOptionsData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:8.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs.wwpbaseobjects {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "DVB_SidebarMenuOptionsData.Item" )]
   [XmlType(TypeName =  "DVB_SidebarMenuOptionsData.Item" , Namespace = "GxEv3Up14_Meetrika" )]
   [System.Xml.Serialization.XmlInclude( typeof( wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item ))]
   [Serializable]
   public class SdtDVB_SidebarMenuOptionsData_Item : GxUserType
   {
      public SdtDVB_SidebarMenuOptionsData_Item( )
      {
         /* Constructor for serialization */
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey = "";
      }

      public SdtDVB_SidebarMenuOptionsData_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Tooltip = deserialized.gxTpr_Tooltip;
         obj.gxTpr_Link = deserialized.gxTpr_Link;
         obj.gxTpr_Linktarget = deserialized.gxTpr_Linktarget;
         obj.gxTpr_Iconclass = deserialized.gxTpr_Iconclass;
         obj.gxTpr_Caption = deserialized.gxTpr_Caption;
         obj.gxTpr_Authorizationkey = deserialized.gxTpr_Authorizationkey;
         obj.gxTpr_Subitems = deserialized.gxTpr_Subitems;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") )
               {
                  gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "tooltip") )
               {
                  gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "link") )
               {
                  gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "linkTarget") )
               {
                  gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "iconClass") )
               {
                  gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "caption") )
               {
                  gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "authorizationKey") )
               {
                  gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "subItems") )
               {
                  if ( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems == null )
                  {
                     gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems = new GxObjectCollection( context, "DVB_SidebarMenuOptionsData.Item", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems.readxmlcollection(oReader, "subItems", "Item");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "WWPBaseObjects\\DVB_SidebarMenuOptionsData.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.RTrim( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("tooltip", StringUtil.RTrim( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("link", StringUtil.RTrim( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("linkTarget", StringUtil.RTrim( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("iconClass", StringUtil.RTrim( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("caption", StringUtil.RTrim( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("authorizationKey", StringUtil.RTrim( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_Meetrika";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_Meetrika";
            }
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems.writexmlcollection(oWriter, "subItems", sNameSpace1, "Item", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id, false);
         AddObjectProperty("tooltip", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip, false);
         AddObjectProperty("link", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link, false);
         AddObjectProperty("linkTarget", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget, false);
         AddObjectProperty("iconClass", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass, false);
         AddObjectProperty("caption", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption, false);
         AddObjectProperty("authorizationKey", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey, false);
         if ( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems != null )
         {
            AddObjectProperty("subItems", gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id"   )]
      public String gxTpr_Id
      {
         get {
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id = (String)(value);
         }

      }

      [  SoapElement( ElementName = "tooltip" )]
      [  XmlElement( ElementName = "tooltip"   )]
      public String gxTpr_Tooltip
      {
         get {
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip = (String)(value);
         }

      }

      [  SoapElement( ElementName = "link" )]
      [  XmlElement( ElementName = "link"   )]
      public String gxTpr_Link
      {
         get {
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "linkTarget" )]
      [  XmlElement( ElementName = "linkTarget"   )]
      public String gxTpr_Linktarget
      {
         get {
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget = (String)(value);
         }

      }

      [  SoapElement( ElementName = "iconClass" )]
      [  XmlElement( ElementName = "iconClass"   )]
      public String gxTpr_Iconclass
      {
         get {
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass = (String)(value);
         }

      }

      [  SoapElement( ElementName = "caption" )]
      [  XmlElement( ElementName = "caption"   )]
      public String gxTpr_Caption
      {
         get {
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption = (String)(value);
         }

      }

      [  SoapElement( ElementName = "authorizationKey" )]
      [  XmlElement( ElementName = "authorizationKey"   )]
      public String gxTpr_Authorizationkey
      {
         get {
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey = (String)(value);
         }

      }

      public class gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems_wwpbaseobjects_SdtDVB_SidebarMenuOptionsData_Item_80compatibility:wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item {}
      [  SoapElement( ElementName = "subItems" )]
      [  XmlArray( ElementName = "subItems"  )]
      [  XmlArrayItemAttribute( Type= typeof( wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item ), ElementName= "Item"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems_wwpbaseobjects_SdtDVB_SidebarMenuOptionsData_Item_80compatibility ), ElementName= "DVB_SidebarMenuOptionsData.Item"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Subitems_GxObjectCollection
      {
         get {
            if ( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems == null )
            {
               gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems = new GxObjectCollection( context, "DVB_SidebarMenuOptionsData.Item", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems ;
         }

         set {
            if ( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems == null )
            {
               gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems = new GxObjectCollection( context, "DVB_SidebarMenuOptionsData.Item", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item", "GeneXus.Programs");
            }
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Subitems
      {
         get {
            if ( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems == null )
            {
               gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems = new GxObjectCollection( context, "DVB_SidebarMenuOptionsData.Item", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item", "GeneXus.Programs");
            }
            return gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems ;
         }

         set {
            gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems = value;
         }

      }

      public void gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems_SetNull( )
      {
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems = null;
         return  ;
      }

      public bool gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems_IsNull( )
      {
         if ( gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption = "";
         gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String gxTv_SdtDVB_SidebarMenuOptionsData_Item_Id ;
      protected String gxTv_SdtDVB_SidebarMenuOptionsData_Item_Tooltip ;
      protected String gxTv_SdtDVB_SidebarMenuOptionsData_Item_Link ;
      protected String gxTv_SdtDVB_SidebarMenuOptionsData_Item_Linktarget ;
      protected String gxTv_SdtDVB_SidebarMenuOptionsData_Item_Iconclass ;
      protected String gxTv_SdtDVB_SidebarMenuOptionsData_Item_Caption ;
      protected String gxTv_SdtDVB_SidebarMenuOptionsData_Item_Authorizationkey ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item ))]
      protected IGxCollection gxTv_SdtDVB_SidebarMenuOptionsData_Item_Subitems=null ;
   }

   [DataContract(Name = @"WWPBaseObjects\DVB_SidebarMenuOptionsData.Item", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtDVB_SidebarMenuOptionsData_Item_RESTInterface : GxGenericCollectionItem<GeneXus.Programs.wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtDVB_SidebarMenuOptionsData_Item_RESTInterface( ) : base()
      {
      }

      public SdtDVB_SidebarMenuOptionsData_Item_RESTInterface( wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public String gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (String)(value);
         }

      }

      [DataMember( Name = "tooltip" , Order = 1 )]
      public String gxTpr_Tooltip
      {
         get {
            return sdt.gxTpr_Tooltip ;
         }

         set {
            sdt.gxTpr_Tooltip = (String)(value);
         }

      }

      [DataMember( Name = "link" , Order = 2 )]
      public String gxTpr_Link
      {
         get {
            return sdt.gxTpr_Link ;
         }

         set {
            sdt.gxTpr_Link = (String)(value);
         }

      }

      [DataMember( Name = "linkTarget" , Order = 3 )]
      public String gxTpr_Linktarget
      {
         get {
            return sdt.gxTpr_Linktarget ;
         }

         set {
            sdt.gxTpr_Linktarget = (String)(value);
         }

      }

      [DataMember( Name = "iconClass" , Order = 4 )]
      public String gxTpr_Iconclass
      {
         get {
            return sdt.gxTpr_Iconclass ;
         }

         set {
            sdt.gxTpr_Iconclass = (String)(value);
         }

      }

      [DataMember( Name = "caption" , Order = 5 )]
      public String gxTpr_Caption
      {
         get {
            return sdt.gxTpr_Caption ;
         }

         set {
            sdt.gxTpr_Caption = (String)(value);
         }

      }

      [DataMember( Name = "authorizationKey" , Order = 6 )]
      public String gxTpr_Authorizationkey
      {
         get {
            return sdt.gxTpr_Authorizationkey ;
         }

         set {
            sdt.gxTpr_Authorizationkey = (String)(value);
         }

      }

      [DataMember( Name = "subItems" , Order = 7 )]
      public GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item_RESTInterface> gxTpr_Subitems
      {
         get {
            return new GxGenericCollection<GeneXus.Programs.wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item_RESTInterface>(sdt.gxTpr_Subitems) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Subitems);
         }

      }

      public wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item sdt
      {
         get {
            return (wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new GeneXus.Programs.wwpbaseobjects.SdtDVB_SidebarMenuOptionsData_Item() ;
         }
      }

   }

}
