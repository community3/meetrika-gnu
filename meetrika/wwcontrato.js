/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:12:45.68
*/
gx.evt.autoSkip = false;
gx.define('wwcontrato', false, function () {
   this.ServerClass =  "wwcontrato" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.A83Contrato_DataVigenciaTermino=gx.fn.getDateValue("CONTRATO_DATAVIGENCIATERMINO") ;
      this.A843Contrato_DataFimTA=gx.fn.getDateValue("CONTRATO_DATAFIMTA") ;
      this.AV189Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV10GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV30DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV29DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
      this.A39Contratada_Codigo=gx.fn.getIntegerValue("CONTRATADA_CODIGO",'.') ;
      this.AV6WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
   };
   this.Valid_Contrato_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(128) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTRATO_CODIGO", gx.fn.currentGridRowImpl(128));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATO_NUMERO1","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATO_NUMEROATA1","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATO_PREPOSTONOM1","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM1","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR1","Visible", false );
      if ( this.AV15DynamicFiltersSelector1 == "CONTRATO_NUMERO" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_NUMERO1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTRATO_NUMEROATA" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_NUMEROATA1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTRATO_PREPOSTONOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_PREPOSTONOM1","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTRATADA_PESSOANOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM1","Visible", true );
      }
      else if ( this.AV15DynamicFiltersSelector1 == "CONTRATO_ANO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1","Visible", true );
      }
   };
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATO_NUMERO2","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATO_NUMEROATA2","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATO_PREPOSTONOM2","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM2","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR2","Visible", false );
      if ( this.AV20DynamicFiltersSelector2 == "CONTRATO_NUMERO" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_NUMERO2","Visible", true );
      }
      else if ( this.AV20DynamicFiltersSelector2 == "CONTRATO_NUMEROATA" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_NUMEROATA2","Visible", true );
      }
      else if ( this.AV20DynamicFiltersSelector2 == "CONTRATO_PREPOSTONOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_PREPOSTONOM2","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR2","Visible", true );
      }
      else if ( this.AV20DynamicFiltersSelector2 == "CONTRATADA_PESSOANOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM2","Visible", true );
      }
      else if ( this.AV20DynamicFiltersSelector2 == "CONTRATO_ANO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATO_NUMERO3","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATO_NUMEROATA3","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATO_PREPOSTONOM3","Visible", false );
      gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM3","Visible", false );
      gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR3","Visible", false );
      if ( this.AV25DynamicFiltersSelector3 == "CONTRATO_NUMERO" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_NUMERO3","Visible", true );
      }
      else if ( this.AV25DynamicFiltersSelector3 == "CONTRATO_NUMEROATA" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_NUMEROATA3","Visible", true );
      }
      else if ( this.AV25DynamicFiltersSelector3 == "CONTRATO_PREPOSTONOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATO_PREPOSTONOM3","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR3","Visible", true );
      }
      else if ( this.AV25DynamicFiltersSelector3 == "CONTRATADA_PESSOANOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM3","Visible", true );
      }
      else if ( this.AV25DynamicFiltersSelector3 == "CONTRATO_ANO" )
      {
         gx.fn.setCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3","Visible", true );
      }
   };
   this.s162_client=function()
   {
      this.s182_client();
      if ( this.AV13OrderedBy == 6 )
      {
         this.DDO_CONTRATO_ATIVOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s182_client=function()
   {
      this.DDO_CONTRATO_ATIVOContainer.SortedStatus =  ""  ;
   };
   this.s202_client=function()
   {
      this.AV19DynamicFiltersEnabled2 =  false  ;
      this.AV20DynamicFiltersSelector2 =  "CONTRATO_NUMERO"  ;
      this.AV41Contrato_Numero2 =  ''  ;
      this.s122_client();
      this.AV24DynamicFiltersEnabled3 =  false  ;
      this.AV25DynamicFiltersSelector3 =  "CONTRATO_NUMERO"  ;
      this.AV42Contrato_Numero3 =  ''  ;
      this.s132_client();
   };
   this.e27652_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_associarcontrato_gestor.aspx",[this.A74Contrato_Codigo]), []);
      this.refreshOutputs([]);
   };
   this.e28652_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_associarcontrato_auxiliar.aspx",[this.A74Contrato_Codigo]), []);
      this.refreshOutputs([]);
   };
   this.e29652_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_associarcontrato_sistema.aspx",[this.A74Contrato_Codigo]), []);
      this.refreshOutputs([]);
   };
   this.e11652_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12652_client=function()
   {
      this.executeServerEvent("DDO_CONTRATO_VALORUNDCNTATUAL.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13652_client=function()
   {
      this.executeServerEvent("DDO_CONTRATO_ATIVO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14652_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e15652_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e16652_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e17652_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e18652_client=function()
   {
      this.executeServerEvent("'DOCLEANFILTERS'", true, null, false, false);
   };
   this.e19652_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e20652_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e21652_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e22652_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e23652_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e30652_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e31652_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,14,16,18,19,21,24,26,29,31,34,37,39,41,43,46,48,49,50,51,52,55,57,59,61,62,65,67,69,71,74,76,77,78,79,80,83,85,87,89,90,93,95,97,99,102,104,105,106,107,108,111,113,115,117,118,122,125,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,149,150,151,152,153,155,157];
   this.GXLastCtrlId =157;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",128,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwcontrato",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",129,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",130,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Display","vDISPLAY",131,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(74,132,"CONTRATO_CODIGO","Código","","Contrato_Codigo","int",0,"px",6,6,"right",null,[],74,"Contrato_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(41,133,"CONTRATADA_PESSOANOM","","","Contratada_PessoaNom","char",0,"px",100,80,"left",null,[],41,"Contratada_PessoaNom",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(77,134,"CONTRATO_NUMERO","","","Contrato_Numero","char",110,"px",20,20,"left",null,[],77,"Contrato_Numero",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(78,135,"CONTRATO_NUMEROATA","","","Contrato_NumeroAta","char",60,"px",10,10,"left",null,[],78,"Contrato_NumeroAta",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(79,136,"CONTRATO_ANO","","","Contrato_Ano","int",34,"px",4,4,"right",null,[],79,"Contrato_Ano",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1869,137,"CONTRATO_DATATERMINO","Vigência","","Contrato_DataTermino","date",0,"px",8,8,"right",null,[],1869,"Contrato_DataTermino",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1870,138,"CONTRATO_VALORUNDCNTATUAL","","","Contrato_ValorUndCntAtual","decimal",0,"px",18,18,"right",null,[],1870,"Contrato_ValorUndCntAtual",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit("Saldocontrato_saldo",139,"vSALDOCONTRATO_SALDO","Saldo","","SaldoContrato_Saldo","decimal",0,"px",18,18,"right",null,[],"Saldocontrato_saldo","SaldoContrato_Saldo",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(42,140,"CONTRATADA_PESSOACNPJ","","","Contratada_PessoaCNPJ","svchar",0,"px",15,15,"left",null,[],42,"Contratada_PessoaCNPJ",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addCheckBox(92,141,"CONTRATO_ATIVO","","","Contrato_Ativo","boolean","true","false",null,true,false,0,"px","");
   GridContainer.addBitmap("&Associargestor","vASSOCIARGESTOR",142,36,"px",17,"px","e27652_client","","","Image","");
   GridContainer.addBitmap("&Associarauxiliar","vASSOCIARAUXILIAR",143,36,"px",17,"px","e28652_client","","","Image","");
   GridContainer.addBitmap("&Associarsistema","vASSOCIARSISTEMA",144,36,"px",17,"px","e29652_client","","","Image","");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 148, 18, "DVelop_WorkWithPlusUtilities", "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_CONTRATO_VALORUNDCNTATUALContainer = gx.uc.getNew(this, 154, 18, "BootstrapDropDownOptions", "DDO_CONTRATO_VALORUNDCNTATUALContainer", "Ddo_contrato_valorundcntatual");
   var DDO_CONTRATO_VALORUNDCNTATUALContainer = this.DDO_CONTRATO_VALORUNDCNTATUALContainer;
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.addV2CFunction('AV115DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTRATO_VALORUNDCNTATUALContainer.addC2VFunction(function(UC) { UC.ParentObject.AV115DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV115DDO_TitleSettingsIcons); });
   DDO_CONTRATO_VALORUNDCNTATUALContainer.addV2CFunction('AV129Contrato_ValorUndCntAtualTitleFilterData', "vCONTRATO_VALORUNDCNTATUALTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTRATO_VALORUNDCNTATUALContainer.addC2VFunction(function(UC) { UC.ParentObject.AV129Contrato_ValorUndCntAtualTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTRATO_VALORUNDCNTATUALTITLEFILTERDATA",UC.ParentObject.AV129Contrato_ValorUndCntAtualTitleFilterData); });
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setProp("Class", "Class", "", "char");
   DDO_CONTRATO_VALORUNDCNTATUALContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTRATO_VALORUNDCNTATUALContainer.addEventHandler("OnOptionClicked", this.e12652_client);
   this.setUserControl(DDO_CONTRATO_VALORUNDCNTATUALContainer);
   this.DDO_CONTRATO_ATIVOContainer = gx.uc.getNew(this, 156, 18, "BootstrapDropDownOptions", "DDO_CONTRATO_ATIVOContainer", "Ddo_contrato_ativo");
   var DDO_CONTRATO_ATIVOContainer = this.DDO_CONTRATO_ATIVOContainer;
   DDO_CONTRATO_ATIVOContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTRATO_ATIVOContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTRATO_ATIVOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_CONTRATO_ATIVOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_CONTRATO_ATIVOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_CONTRATO_ATIVOContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_CONTRATO_ATIVOContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_CONTRATO_ATIVOContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_CONTRATO_ATIVOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "1:Marcado,2:Desmarcado", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTRATO_ATIVOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTRATO_ATIVOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTRATO_ATIVOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTRATO_ATIVOContainer.addV2CFunction('AV115DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTRATO_ATIVOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV115DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV115DDO_TitleSettingsIcons); });
   DDO_CONTRATO_ATIVOContainer.addV2CFunction('AV120Contrato_AtivoTitleFilterData', "vCONTRATO_ATIVOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTRATO_ATIVOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV120Contrato_AtivoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTRATO_ATIVOTITLEFILTERDATA",UC.ParentObject.AV120Contrato_AtivoTitleFilterData); });
   DDO_CONTRATO_ATIVOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTRATO_ATIVOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTRATO_ATIVOContainer.setProp("Class", "Class", "", "char");
   DDO_CONTRATO_ATIVOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTRATO_ATIVOContainer.addEventHandler("OnOptionClicked", this.e13652_client);
   this.setUserControl(DDO_CONTRATO_ATIVOContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 147, 18, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV117GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV117GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV117GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV118GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV118GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV118GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11652_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[14]={fld:"CONTRATOTITLE", format:0,grid:0};
   GXValidFnc[16]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV13OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[19]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[21]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[24]={fld:"CLEANFILTERS",grid:0};
   GXValidFnc[26]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[29]={fld:"FILTERTEXTCONTRATO_AREATRABALHOCOD", format:0,grid:0};
   GXValidFnc[31]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_AREATRABALHOCOD",gxz:"ZV50Contrato_AreaTrabalhoCod",gxold:"OV50Contrato_AreaTrabalhoCod",gxvar:"AV50Contrato_AreaTrabalhoCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV50Contrato_AreaTrabalhoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV50Contrato_AreaTrabalhoCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTRATO_AREATRABALHOCOD",gx.O.AV50Contrato_AreaTrabalhoCod)},c2v:function(){if(this.val()!==undefined)gx.O.AV50Contrato_AreaTrabalhoCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_AREATRABALHOCOD",'.')},nac:gx.falseFn};
   GXValidFnc[34]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[37]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[39]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV15DynamicFiltersSelector1",gxold:"OV15DynamicFiltersSelector1",gxvar:"AV15DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV15DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV15DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[41]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[43]={fld:"TABLEMERGEDDYNAMICFILTERS1",grid:0};
   GXValidFnc[46]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR1",gxz:"ZV16DynamicFiltersOperator1",gxold:"OV16DynamicFiltersOperator1",gxvar:"AV16DynamicFiltersOperator1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV16DynamicFiltersOperator1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16DynamicFiltersOperator1=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR1",gx.O.AV16DynamicFiltersOperator1)},c2v:function(){if(this.val()!==undefined)gx.O.AV16DynamicFiltersOperator1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR1",'.')},nac:gx.falseFn};
   GXValidFnc[48]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_NUMERO1",gxz:"ZV40Contrato_Numero1",gxold:"OV40Contrato_Numero1",gxvar:"AV40Contrato_Numero1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV40Contrato_Numero1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV40Contrato_Numero1=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_NUMERO1",gx.O.AV40Contrato_Numero1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV40Contrato_Numero1=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_NUMERO1")},nac:gx.falseFn};
   GXValidFnc[49]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_NUMEROATA1",gxz:"ZV51Contrato_NumeroAta1",gxold:"OV51Contrato_NumeroAta1",gxvar:"AV51Contrato_NumeroAta1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV51Contrato_NumeroAta1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV51Contrato_NumeroAta1=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_NUMEROATA1",gx.O.AV51Contrato_NumeroAta1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV51Contrato_NumeroAta1=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_NUMEROATA1")},nac:gx.falseFn};
   GXValidFnc[50]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_PREPOSTONOM1",gxz:"ZV133Contrato_PrepostoNom1",gxold:"OV133Contrato_PrepostoNom1",gxvar:"AV133Contrato_PrepostoNom1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV133Contrato_PrepostoNom1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV133Contrato_PrepostoNom1=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_PREPOSTONOM1",gx.O.AV133Contrato_PrepostoNom1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV133Contrato_PrepostoNom1=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_PREPOSTONOM1")},nac:gx.falseFn};
   GXValidFnc[51]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATADA_PESSOANOM1",gxz:"ZV18Contratada_PessoaNom1",gxold:"OV18Contratada_PessoaNom1",gxvar:"AV18Contratada_PessoaNom1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV18Contratada_PessoaNom1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV18Contratada_PessoaNom1=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_PESSOANOM1",gx.O.AV18Contratada_PessoaNom1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV18Contratada_PessoaNom1=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_PESSOANOM1")},nac:gx.falseFn};
   GXValidFnc[52]={fld:"TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1",grid:0};
   GXValidFnc[55]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_ANO1",gxz:"ZV43Contrato_Ano1",gxold:"OV43Contrato_Ano1",gxvar:"AV43Contrato_Ano1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV43Contrato_Ano1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV43Contrato_Ano1=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATO_ANO1",gx.O.AV43Contrato_Ano1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43Contrato_Ano1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_ANO1",'.')},nac:gx.falseFn};
   GXValidFnc[57]={fld:"DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT1", format:0,grid:0};
   GXValidFnc[59]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_ANO_TO1",gxz:"ZV44Contrato_Ano_To1",gxold:"OV44Contrato_Ano_To1",gxvar:"AV44Contrato_Ano_To1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV44Contrato_Ano_To1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV44Contrato_Ano_To1=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATO_ANO_TO1",gx.O.AV44Contrato_Ano_To1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV44Contrato_Ano_To1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_ANO_TO1",'.')},nac:gx.falseFn};
   GXValidFnc[61]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[62]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[65]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[67]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV20DynamicFiltersSelector2",gxold:"OV20DynamicFiltersSelector2",gxvar:"AV20DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV20DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV20DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV20DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV20DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[69]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[71]={fld:"TABLEMERGEDDYNAMICFILTERS2",grid:0};
   GXValidFnc[74]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR2",gxz:"ZV21DynamicFiltersOperator2",gxold:"OV21DynamicFiltersOperator2",gxvar:"AV21DynamicFiltersOperator2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV21DynamicFiltersOperator2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21DynamicFiltersOperator2=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR2",gx.O.AV21DynamicFiltersOperator2)},c2v:function(){if(this.val()!==undefined)gx.O.AV21DynamicFiltersOperator2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR2",'.')},nac:gx.falseFn};
   GXValidFnc[76]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_NUMERO2",gxz:"ZV41Contrato_Numero2",gxold:"OV41Contrato_Numero2",gxvar:"AV41Contrato_Numero2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV41Contrato_Numero2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV41Contrato_Numero2=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_NUMERO2",gx.O.AV41Contrato_Numero2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV41Contrato_Numero2=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_NUMERO2")},nac:gx.falseFn};
   GXValidFnc[77]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_NUMEROATA2",gxz:"ZV52Contrato_NumeroAta2",gxold:"OV52Contrato_NumeroAta2",gxvar:"AV52Contrato_NumeroAta2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV52Contrato_NumeroAta2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV52Contrato_NumeroAta2=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_NUMEROATA2",gx.O.AV52Contrato_NumeroAta2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV52Contrato_NumeroAta2=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_NUMEROATA2")},nac:gx.falseFn};
   GXValidFnc[78]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_PREPOSTONOM2",gxz:"ZV134Contrato_PrepostoNom2",gxold:"OV134Contrato_PrepostoNom2",gxvar:"AV134Contrato_PrepostoNom2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV134Contrato_PrepostoNom2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV134Contrato_PrepostoNom2=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_PREPOSTONOM2",gx.O.AV134Contrato_PrepostoNom2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV134Contrato_PrepostoNom2=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_PREPOSTONOM2")},nac:gx.falseFn};
   GXValidFnc[79]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATADA_PESSOANOM2",gxz:"ZV23Contratada_PessoaNom2",gxold:"OV23Contratada_PessoaNom2",gxvar:"AV23Contratada_PessoaNom2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23Contratada_PessoaNom2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23Contratada_PessoaNom2=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_PESSOANOM2",gx.O.AV23Contratada_PessoaNom2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23Contratada_PessoaNom2=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_PESSOANOM2")},nac:gx.falseFn};
   GXValidFnc[80]={fld:"TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2",grid:0};
   GXValidFnc[83]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_ANO2",gxz:"ZV45Contrato_Ano2",gxold:"OV45Contrato_Ano2",gxvar:"AV45Contrato_Ano2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV45Contrato_Ano2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV45Contrato_Ano2=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATO_ANO2",gx.O.AV45Contrato_Ano2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV45Contrato_Ano2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_ANO2",'.')},nac:gx.falseFn};
   GXValidFnc[85]={fld:"DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT2", format:0,grid:0};
   GXValidFnc[87]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_ANO_TO2",gxz:"ZV46Contrato_Ano_To2",gxold:"OV46Contrato_Ano_To2",gxvar:"AV46Contrato_Ano_To2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV46Contrato_Ano_To2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV46Contrato_Ano_To2=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATO_ANO_TO2",gx.O.AV46Contrato_Ano_To2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV46Contrato_Ano_To2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_ANO_TO2",'.')},nac:gx.falseFn};
   GXValidFnc[89]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[90]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[93]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[95]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV25DynamicFiltersSelector3",gxold:"OV25DynamicFiltersSelector3",gxvar:"AV25DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV25DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV25DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV25DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV25DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[97]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[99]={fld:"TABLEMERGEDDYNAMICFILTERS3",grid:0};
   GXValidFnc[102]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR3",gxz:"ZV26DynamicFiltersOperator3",gxold:"OV26DynamicFiltersOperator3",gxvar:"AV26DynamicFiltersOperator3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV26DynamicFiltersOperator3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV26DynamicFiltersOperator3=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR3",gx.O.AV26DynamicFiltersOperator3)},c2v:function(){if(this.val()!==undefined)gx.O.AV26DynamicFiltersOperator3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR3",'.')},nac:gx.falseFn};
   GXValidFnc[104]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_NUMERO3",gxz:"ZV42Contrato_Numero3",gxold:"OV42Contrato_Numero3",gxvar:"AV42Contrato_Numero3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV42Contrato_Numero3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV42Contrato_Numero3=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_NUMERO3",gx.O.AV42Contrato_Numero3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV42Contrato_Numero3=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_NUMERO3")},nac:gx.falseFn};
   GXValidFnc[105]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_NUMEROATA3",gxz:"ZV53Contrato_NumeroAta3",gxold:"OV53Contrato_NumeroAta3",gxvar:"AV53Contrato_NumeroAta3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV53Contrato_NumeroAta3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV53Contrato_NumeroAta3=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_NUMEROATA3",gx.O.AV53Contrato_NumeroAta3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV53Contrato_NumeroAta3=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_NUMEROATA3")},nac:gx.falseFn};
   GXValidFnc[106]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_PREPOSTONOM3",gxz:"ZV135Contrato_PrepostoNom3",gxold:"OV135Contrato_PrepostoNom3",gxvar:"AV135Contrato_PrepostoNom3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV135Contrato_PrepostoNom3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV135Contrato_PrepostoNom3=Value},v2c:function(){gx.fn.setControlValue("vCONTRATO_PREPOSTONOM3",gx.O.AV135Contrato_PrepostoNom3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV135Contrato_PrepostoNom3=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATO_PREPOSTONOM3")},nac:gx.falseFn};
   GXValidFnc[107]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATADA_PESSOANOM3",gxz:"ZV28Contratada_PessoaNom3",gxold:"OV28Contratada_PessoaNom3",gxvar:"AV28Contratada_PessoaNom3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV28Contratada_PessoaNom3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV28Contratada_PessoaNom3=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_PESSOANOM3",gx.O.AV28Contratada_PessoaNom3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV28Contratada_PessoaNom3=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_PESSOANOM3")},nac:gx.falseFn};
   GXValidFnc[108]={fld:"TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3",grid:0};
   GXValidFnc[111]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_ANO3",gxz:"ZV47Contrato_Ano3",gxold:"OV47Contrato_Ano3",gxvar:"AV47Contrato_Ano3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV47Contrato_Ano3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV47Contrato_Ano3=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATO_ANO3",gx.O.AV47Contrato_Ano3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV47Contrato_Ano3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_ANO3",'.')},nac:gx.falseFn};
   GXValidFnc[113]={fld:"DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT3", format:0,grid:0};
   GXValidFnc[115]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATO_ANO_TO3",gxz:"ZV48Contrato_Ano_To3",gxold:"OV48Contrato_Ano_To3",gxvar:"AV48Contrato_Ano_To3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV48Contrato_Ano_To3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV48Contrato_Ano_To3=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATO_ANO_TO3",gx.O.AV48Contrato_Ano_To3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV48Contrato_Ano_To3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATO_ANO_TO3",'.')},nac:gx.falseFn};
   GXValidFnc[117]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[118]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[122]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[125]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[129]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV31Update",gxold:"OV31Update",gxvar:"AV31Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV31Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV31Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(128),gx.O.AV31Update,gx.O.AV183Update_GXI)},c2v:function(){gx.O.AV183Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV31Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(128))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(128))}, gxvar_GXI:'AV183Update_GXI',nac:gx.falseFn};
   GXValidFnc[130]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV32Delete",gxold:"OV32Delete",gxvar:"AV32Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV32Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV32Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(128),gx.O.AV32Delete,gx.O.AV184Delete_GXI)},c2v:function(){gx.O.AV184Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV32Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(128))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(128))}, gxvar_GXI:'AV184Delete_GXI',nac:gx.falseFn};
   GXValidFnc[131]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDISPLAY",gxz:"ZV49Display",gxold:"OV49Display",gxvar:"AV49Display",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV49Display=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV49Display=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDISPLAY",row || gx.fn.currentGridRowImpl(128),gx.O.AV49Display,gx.O.AV185Display_GXI)},c2v:function(){gx.O.AV185Display_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV49Display=this.val()},val:function(row){return gx.fn.getGridControlValue("vDISPLAY",row || gx.fn.currentGridRowImpl(128))},val_GXI:function(row){return gx.fn.getGridControlValue("vDISPLAY_GXI",row || gx.fn.currentGridRowImpl(128))}, gxvar_GXI:'AV185Display_GXI',nac:gx.falseFn};
   GXValidFnc[132]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:this.Valid_Contrato_codigo,isvalid:null,rgrid:[],fld:"CONTRATO_CODIGO",gxz:"Z74Contrato_Codigo",gxold:"O74Contrato_Codigo",gxvar:"A74Contrato_Codigo",ucs:[],op:[137,138,133,140],ip:[137,138,133,140,132],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A74Contrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z74Contrato_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(128),gx.O.A74Contrato_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A74Contrato_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(128),'.')},nac:gx.falseFn};
   GXValidFnc[133]={lvl:2,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_PESSOANOM",gxz:"Z41Contratada_PessoaNom",gxold:"O41Contratada_PessoaNom",gxvar:"A41Contratada_PessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A41Contratada_PessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z41Contratada_PessoaNom=Value},v2c:function(row){gx.fn.setGridControlValue("CONTRATADA_PESSOANOM",row || gx.fn.currentGridRowImpl(128),gx.O.A41Contratada_PessoaNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A41Contratada_PessoaNom=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTRATADA_PESSOANOM",row || gx.fn.currentGridRowImpl(128))},nac:gx.falseFn};
   GXValidFnc[134]={lvl:2,type:"char",len:20,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_NUMERO",gxz:"Z77Contrato_Numero",gxold:"O77Contrato_Numero",gxvar:"A77Contrato_Numero",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A77Contrato_Numero=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z77Contrato_Numero=Value},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_NUMERO",row || gx.fn.currentGridRowImpl(128),gx.O.A77Contrato_Numero,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A77Contrato_Numero=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTRATO_NUMERO",row || gx.fn.currentGridRowImpl(128))},nac:gx.falseFn};
   GXValidFnc[135]={lvl:2,type:"char",len:10,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_NUMEROATA",gxz:"Z78Contrato_NumeroAta",gxold:"O78Contrato_NumeroAta",gxvar:"A78Contrato_NumeroAta",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A78Contrato_NumeroAta=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z78Contrato_NumeroAta=Value},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_NUMEROATA",row || gx.fn.currentGridRowImpl(128),gx.O.A78Contrato_NumeroAta,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A78Contrato_NumeroAta=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTRATO_NUMEROATA",row || gx.fn.currentGridRowImpl(128))},nac:gx.falseFn};
   GXValidFnc[136]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_ANO",gxz:"Z79Contrato_Ano",gxold:"O79Contrato_Ano",gxvar:"A79Contrato_Ano",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A79Contrato_Ano=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z79Contrato_Ano=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_ANO",row || gx.fn.currentGridRowImpl(128),gx.O.A79Contrato_Ano,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A79Contrato_Ano=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATO_ANO",row || gx.fn.currentGridRowImpl(128),'.')},nac:gx.falseFn};
   GXValidFnc[137]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_DATATERMINO",gxz:"Z1869Contrato_DataTermino",gxold:"O1869Contrato_DataTermino",gxvar:"A1869Contrato_DataTermino",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1869Contrato_DataTermino=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1869Contrato_DataTermino=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATO_DATATERMINO",row || gx.fn.currentGridRowImpl(128),gx.O.A1869Contrato_DataTermino,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1869Contrato_DataTermino=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTRATO_DATATERMINO",row || gx.fn.currentGridRowImpl(128))},nac:gx.falseFn};
   GXValidFnc[138]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_VALORUNDCNTATUAL",gxz:"Z1870Contrato_ValorUndCntAtual",gxold:"O1870Contrato_ValorUndCntAtual",gxvar:"A1870Contrato_ValorUndCntAtual",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1870Contrato_ValorUndCntAtual=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1870Contrato_ValorUndCntAtual=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("CONTRATO_VALORUNDCNTATUAL",row || gx.fn.currentGridRowImpl(128),gx.O.A1870Contrato_ValorUndCntAtual,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1870Contrato_ValorUndCntAtual=this.val()},val:function(row){return gx.fn.getGridDecimalValue("CONTRATO_VALORUNDCNTATUAL",row || gx.fn.currentGridRowImpl(128),'.',',')},nac:gx.falseFn};
   GXValidFnc[139]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vSALDOCONTRATO_SALDO",gxz:"ZV78SaldoContrato_Saldo",gxold:"OV78SaldoContrato_Saldo",gxvar:"AV78SaldoContrato_Saldo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV78SaldoContrato_Saldo=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV78SaldoContrato_Saldo=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("vSALDOCONTRATO_SALDO",row || gx.fn.currentGridRowImpl(128),gx.O.AV78SaldoContrato_Saldo,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV78SaldoContrato_Saldo=this.val()},val:function(row){return gx.fn.getGridDecimalValue("vSALDOCONTRATO_SALDO",row || gx.fn.currentGridRowImpl(128),'.',',')},nac:gx.falseFn};
   GXValidFnc[140]={lvl:2,type:"svchar",len:15,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_PESSOACNPJ",gxz:"Z42Contratada_PessoaCNPJ",gxold:"O42Contratada_PessoaCNPJ",gxvar:"A42Contratada_PessoaCNPJ",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A42Contratada_PessoaCNPJ=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z42Contratada_PessoaCNPJ=Value},v2c:function(row){gx.fn.setGridControlValue("CONTRATADA_PESSOACNPJ",row || gx.fn.currentGridRowImpl(128),gx.O.A42Contratada_PessoaCNPJ,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A42Contratada_PessoaCNPJ=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTRATADA_PESSOACNPJ",row || gx.fn.currentGridRowImpl(128))},nac:gx.falseFn};
   GXValidFnc[141]={lvl:2,type:"boolean",len:4,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATO_ATIVO",gxz:"Z92Contrato_Ativo",gxold:"O92Contrato_Ativo",gxvar:"A92Contrato_Ativo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A92Contrato_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z92Contrato_Ativo=gx.lang.booleanValue(Value)},v2c:function(row){gx.fn.setGridCheckBoxValue("CONTRATO_ATIVO",row || gx.fn.currentGridRowImpl(128),gx.O.A92Contrato_Ativo,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A92Contrato_Ativo=gx.lang.booleanValue(this.val())},val:function(row){return gx.fn.getGridControlValue("CONTRATO_ATIVO",row || gx.fn.currentGridRowImpl(128))},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[142]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vASSOCIARGESTOR",gxz:"ZV75AssociarGestor",gxold:"OV75AssociarGestor",gxvar:"AV75AssociarGestor",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV75AssociarGestor=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV75AssociarGestor=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vASSOCIARGESTOR",row || gx.fn.currentGridRowImpl(128),gx.O.AV75AssociarGestor,gx.O.AV186Associargestor_GXI)},c2v:function(){gx.O.AV186Associargestor_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV75AssociarGestor=this.val()},val:function(row){return gx.fn.getGridControlValue("vASSOCIARGESTOR",row || gx.fn.currentGridRowImpl(128))},val_GXI:function(row){return gx.fn.getGridControlValue("vASSOCIARGESTOR_GXI",row || gx.fn.currentGridRowImpl(128))}, gxvar_GXI:'AV186Associargestor_GXI',nac:gx.falseFn};
   GXValidFnc[143]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vASSOCIARAUXILIAR",gxz:"ZV119AssociarAuxiliar",gxold:"OV119AssociarAuxiliar",gxvar:"AV119AssociarAuxiliar",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV119AssociarAuxiliar=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV119AssociarAuxiliar=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vASSOCIARAUXILIAR",row || gx.fn.currentGridRowImpl(128),gx.O.AV119AssociarAuxiliar,gx.O.AV187Associarauxiliar_GXI)},c2v:function(){gx.O.AV187Associarauxiliar_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV119AssociarAuxiliar=this.val()},val:function(row){return gx.fn.getGridControlValue("vASSOCIARAUXILIAR",row || gx.fn.currentGridRowImpl(128))},val_GXI:function(row){return gx.fn.getGridControlValue("vASSOCIARAUXILIAR_GXI",row || gx.fn.currentGridRowImpl(128))}, gxvar_GXI:'AV187Associarauxiliar_GXI',nac:gx.falseFn};
   GXValidFnc[144]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:128,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vASSOCIARSISTEMA",gxz:"ZV80AssociarSistema",gxold:"OV80AssociarSistema",gxvar:"AV80AssociarSistema",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV80AssociarSistema=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV80AssociarSistema=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vASSOCIARSISTEMA",row || gx.fn.currentGridRowImpl(128),gx.O.AV80AssociarSistema,gx.O.AV188Associarsistema_GXI)},c2v:function(){gx.O.AV188Associarsistema_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV80AssociarSistema=this.val()},val:function(row){return gx.fn.getGridControlValue("vASSOCIARSISTEMA",row || gx.fn.currentGridRowImpl(128))},val_GXI:function(row){return gx.fn.getGridControlValue("vASSOCIARSISTEMA_GXI",row || gx.fn.currentGridRowImpl(128))}, gxvar_GXI:'AV188Associarsistema_GXI',nac:gx.falseFn};
   GXValidFnc[149]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV19DynamicFiltersEnabled2",gxold:"OV19DynamicFiltersEnabled2",gxvar:"AV19DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV19DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV19DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV19DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV19DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[150]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV24DynamicFiltersEnabled3",gxold:"OV24DynamicFiltersEnabled3",gxvar:"AV24DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV24DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV24DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV24DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[151]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vTFCONTRATO_VALORUNDCNTATUAL",gxz:"ZV130TFContrato_ValorUndCntAtual",gxold:"OV130TFContrato_ValorUndCntAtual",gxvar:"AV130TFContrato_ValorUndCntAtual",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV130TFContrato_ValorUndCntAtual=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV130TFContrato_ValorUndCntAtual=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFCONTRATO_VALORUNDCNTATUAL",gx.O.AV130TFContrato_ValorUndCntAtual,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV130TFContrato_ValorUndCntAtual=this.val()},val:function(){return gx.fn.getDecimalValue("vTFCONTRATO_VALORUNDCNTATUAL",'.',',')},nac:gx.falseFn};
   GXValidFnc[152]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vTFCONTRATO_VALORUNDCNTATUAL_TO",gxz:"ZV131TFContrato_ValorUndCntAtual_To",gxold:"OV131TFContrato_ValorUndCntAtual_To",gxvar:"AV131TFContrato_ValorUndCntAtual_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV131TFContrato_ValorUndCntAtual_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV131TFContrato_ValorUndCntAtual_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFCONTRATO_VALORUNDCNTATUAL_TO",gx.O.AV131TFContrato_ValorUndCntAtual_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV131TFContrato_ValorUndCntAtual_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFCONTRATO_VALORUNDCNTATUAL_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[153]={lvl:0,type:"int",len:1,dec:0,sign:false,pic:"9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTRATO_ATIVO_SEL",gxz:"ZV121TFContrato_Ativo_Sel",gxold:"OV121TFContrato_Ativo_Sel",gxvar:"AV121TFContrato_Ativo_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV121TFContrato_Ativo_Sel=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV121TFContrato_Ativo_Sel=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTRATO_ATIVO_SEL",gx.O.AV121TFContrato_Ativo_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV121TFContrato_Ativo_Sel=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFCONTRATO_ATIVO_SEL",'.')},nac:gx.falseFn};
   GXValidFnc[155]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE",gxz:"ZV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace",gxold:"OV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace",gxvar:"AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE",gx.O.AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[157]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE",gxz:"ZV122ddo_Contrato_AtivoTitleControlIdToReplace",gxold:"OV122ddo_Contrato_AtivoTitleControlIdToReplace",gxvar:"AV122ddo_Contrato_AtivoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV122ddo_Contrato_AtivoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV122ddo_Contrato_AtivoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE",gx.O.AV122ddo_Contrato_AtivoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV122ddo_Contrato_AtivoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV50Contrato_AreaTrabalhoCod = 0 ;
   this.ZV50Contrato_AreaTrabalhoCod = 0 ;
   this.OV50Contrato_AreaTrabalhoCod = 0 ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.ZV15DynamicFiltersSelector1 = "" ;
   this.OV15DynamicFiltersSelector1 = "" ;
   this.AV16DynamicFiltersOperator1 = 0 ;
   this.ZV16DynamicFiltersOperator1 = 0 ;
   this.OV16DynamicFiltersOperator1 = 0 ;
   this.AV40Contrato_Numero1 = "" ;
   this.ZV40Contrato_Numero1 = "" ;
   this.OV40Contrato_Numero1 = "" ;
   this.AV51Contrato_NumeroAta1 = "" ;
   this.ZV51Contrato_NumeroAta1 = "" ;
   this.OV51Contrato_NumeroAta1 = "" ;
   this.AV133Contrato_PrepostoNom1 = "" ;
   this.ZV133Contrato_PrepostoNom1 = "" ;
   this.OV133Contrato_PrepostoNom1 = "" ;
   this.AV18Contratada_PessoaNom1 = "" ;
   this.ZV18Contratada_PessoaNom1 = "" ;
   this.OV18Contratada_PessoaNom1 = "" ;
   this.AV43Contrato_Ano1 = 0 ;
   this.ZV43Contrato_Ano1 = 0 ;
   this.OV43Contrato_Ano1 = 0 ;
   this.AV44Contrato_Ano_To1 = 0 ;
   this.ZV44Contrato_Ano_To1 = 0 ;
   this.OV44Contrato_Ano_To1 = 0 ;
   this.AV20DynamicFiltersSelector2 = "" ;
   this.ZV20DynamicFiltersSelector2 = "" ;
   this.OV20DynamicFiltersSelector2 = "" ;
   this.AV21DynamicFiltersOperator2 = 0 ;
   this.ZV21DynamicFiltersOperator2 = 0 ;
   this.OV21DynamicFiltersOperator2 = 0 ;
   this.AV41Contrato_Numero2 = "" ;
   this.ZV41Contrato_Numero2 = "" ;
   this.OV41Contrato_Numero2 = "" ;
   this.AV52Contrato_NumeroAta2 = "" ;
   this.ZV52Contrato_NumeroAta2 = "" ;
   this.OV52Contrato_NumeroAta2 = "" ;
   this.AV134Contrato_PrepostoNom2 = "" ;
   this.ZV134Contrato_PrepostoNom2 = "" ;
   this.OV134Contrato_PrepostoNom2 = "" ;
   this.AV23Contratada_PessoaNom2 = "" ;
   this.ZV23Contratada_PessoaNom2 = "" ;
   this.OV23Contratada_PessoaNom2 = "" ;
   this.AV45Contrato_Ano2 = 0 ;
   this.ZV45Contrato_Ano2 = 0 ;
   this.OV45Contrato_Ano2 = 0 ;
   this.AV46Contrato_Ano_To2 = 0 ;
   this.ZV46Contrato_Ano_To2 = 0 ;
   this.OV46Contrato_Ano_To2 = 0 ;
   this.AV25DynamicFiltersSelector3 = "" ;
   this.ZV25DynamicFiltersSelector3 = "" ;
   this.OV25DynamicFiltersSelector3 = "" ;
   this.AV26DynamicFiltersOperator3 = 0 ;
   this.ZV26DynamicFiltersOperator3 = 0 ;
   this.OV26DynamicFiltersOperator3 = 0 ;
   this.AV42Contrato_Numero3 = "" ;
   this.ZV42Contrato_Numero3 = "" ;
   this.OV42Contrato_Numero3 = "" ;
   this.AV53Contrato_NumeroAta3 = "" ;
   this.ZV53Contrato_NumeroAta3 = "" ;
   this.OV53Contrato_NumeroAta3 = "" ;
   this.AV135Contrato_PrepostoNom3 = "" ;
   this.ZV135Contrato_PrepostoNom3 = "" ;
   this.OV135Contrato_PrepostoNom3 = "" ;
   this.AV28Contratada_PessoaNom3 = "" ;
   this.ZV28Contratada_PessoaNom3 = "" ;
   this.OV28Contratada_PessoaNom3 = "" ;
   this.AV47Contrato_Ano3 = 0 ;
   this.ZV47Contrato_Ano3 = 0 ;
   this.OV47Contrato_Ano3 = 0 ;
   this.AV48Contrato_Ano_To3 = 0 ;
   this.ZV48Contrato_Ano_To3 = 0 ;
   this.OV48Contrato_Ano_To3 = 0 ;
   this.ZV31Update = "" ;
   this.OV31Update = "" ;
   this.ZV32Delete = "" ;
   this.OV32Delete = "" ;
   this.ZV49Display = "" ;
   this.OV49Display = "" ;
   this.Z74Contrato_Codigo = 0 ;
   this.O74Contrato_Codigo = 0 ;
   this.Z41Contratada_PessoaNom = "" ;
   this.O41Contratada_PessoaNom = "" ;
   this.Z77Contrato_Numero = "" ;
   this.O77Contrato_Numero = "" ;
   this.Z78Contrato_NumeroAta = "" ;
   this.O78Contrato_NumeroAta = "" ;
   this.Z79Contrato_Ano = 0 ;
   this.O79Contrato_Ano = 0 ;
   this.Z1869Contrato_DataTermino = gx.date.nullDate() ;
   this.O1869Contrato_DataTermino = gx.date.nullDate() ;
   this.Z1870Contrato_ValorUndCntAtual = 0 ;
   this.O1870Contrato_ValorUndCntAtual = 0 ;
   this.ZV78SaldoContrato_Saldo = 0 ;
   this.OV78SaldoContrato_Saldo = 0 ;
   this.Z42Contratada_PessoaCNPJ = "" ;
   this.O42Contratada_PessoaCNPJ = "" ;
   this.Z92Contrato_Ativo = false ;
   this.O92Contrato_Ativo = false ;
   this.ZV75AssociarGestor = "" ;
   this.OV75AssociarGestor = "" ;
   this.ZV119AssociarAuxiliar = "" ;
   this.OV119AssociarAuxiliar = "" ;
   this.ZV80AssociarSistema = "" ;
   this.OV80AssociarSistema = "" ;
   this.AV19DynamicFiltersEnabled2 = false ;
   this.ZV19DynamicFiltersEnabled2 = false ;
   this.OV19DynamicFiltersEnabled2 = false ;
   this.AV24DynamicFiltersEnabled3 = false ;
   this.ZV24DynamicFiltersEnabled3 = false ;
   this.OV24DynamicFiltersEnabled3 = false ;
   this.AV130TFContrato_ValorUndCntAtual = 0 ;
   this.ZV130TFContrato_ValorUndCntAtual = 0 ;
   this.OV130TFContrato_ValorUndCntAtual = 0 ;
   this.AV131TFContrato_ValorUndCntAtual_To = 0 ;
   this.ZV131TFContrato_ValorUndCntAtual_To = 0 ;
   this.OV131TFContrato_ValorUndCntAtual_To = 0 ;
   this.AV121TFContrato_Ativo_Sel = 0 ;
   this.ZV121TFContrato_Ativo_Sel = 0 ;
   this.OV121TFContrato_Ativo_Sel = 0 ;
   this.AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = "" ;
   this.ZV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = "" ;
   this.OV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = "" ;
   this.AV122ddo_Contrato_AtivoTitleControlIdToReplace = "" ;
   this.ZV122ddo_Contrato_AtivoTitleControlIdToReplace = "" ;
   this.OV122ddo_Contrato_AtivoTitleControlIdToReplace = "" ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV50Contrato_AreaTrabalhoCod = 0 ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.AV16DynamicFiltersOperator1 = 0 ;
   this.AV40Contrato_Numero1 = "" ;
   this.AV51Contrato_NumeroAta1 = "" ;
   this.AV133Contrato_PrepostoNom1 = "" ;
   this.AV18Contratada_PessoaNom1 = "" ;
   this.AV43Contrato_Ano1 = 0 ;
   this.AV44Contrato_Ano_To1 = 0 ;
   this.AV20DynamicFiltersSelector2 = "" ;
   this.AV21DynamicFiltersOperator2 = 0 ;
   this.AV41Contrato_Numero2 = "" ;
   this.AV52Contrato_NumeroAta2 = "" ;
   this.AV134Contrato_PrepostoNom2 = "" ;
   this.AV23Contratada_PessoaNom2 = "" ;
   this.AV45Contrato_Ano2 = 0 ;
   this.AV46Contrato_Ano_To2 = 0 ;
   this.AV25DynamicFiltersSelector3 = "" ;
   this.AV26DynamicFiltersOperator3 = 0 ;
   this.AV42Contrato_Numero3 = "" ;
   this.AV53Contrato_NumeroAta3 = "" ;
   this.AV135Contrato_PrepostoNom3 = "" ;
   this.AV28Contratada_PessoaNom3 = "" ;
   this.AV47Contrato_Ano3 = 0 ;
   this.AV48Contrato_Ano_To3 = 0 ;
   this.AV117GridCurrentPage = 0 ;
   this.AV19DynamicFiltersEnabled2 = false ;
   this.AV24DynamicFiltersEnabled3 = false ;
   this.AV130TFContrato_ValorUndCntAtual = 0 ;
   this.AV131TFContrato_ValorUndCntAtual_To = 0 ;
   this.AV121TFContrato_Ativo_Sel = 0 ;
   this.AV115DDO_TitleSettingsIcons = {} ;
   this.AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = "" ;
   this.AV122ddo_Contrato_AtivoTitleControlIdToReplace = "" ;
   this.A1015Contrato_PrepostoNom = "" ;
   this.A75Contrato_AreaTrabalhoCod = 0 ;
   this.A39Contratada_Codigo = 0 ;
   this.A40000GXC1 = 0 ;
   this.A843Contrato_DataFimTA = gx.date.nullDate() ;
   this.A83Contrato_DataVigenciaTermino = gx.date.nullDate() ;
   this.A40Contratada_PessoaCod = 0 ;
   this.A1013Contrato_PrepostoCod = 0 ;
   this.A1016Contrato_PrepostoPesCod = 0 ;
   this.AV31Update = "" ;
   this.AV32Delete = "" ;
   this.AV49Display = "" ;
   this.A74Contrato_Codigo = 0 ;
   this.A41Contratada_PessoaNom = "" ;
   this.A77Contrato_Numero = "" ;
   this.A78Contrato_NumeroAta = "" ;
   this.A79Contrato_Ano = 0 ;
   this.A1869Contrato_DataTermino = gx.date.nullDate() ;
   this.A1870Contrato_ValorUndCntAtual = 0 ;
   this.AV78SaldoContrato_Saldo = 0 ;
   this.A42Contratada_PessoaCNPJ = "" ;
   this.A92Contrato_Ativo = false ;
   this.AV75AssociarGestor = "" ;
   this.AV119AssociarAuxiliar = "" ;
   this.AV80AssociarSistema = "" ;
   this.AV189Pgmname = "" ;
   this.AV10GridState = {} ;
   this.AV30DynamicFiltersIgnoreFirst = false ;
   this.AV29DynamicFiltersRemoving = false ;
   this.AV6WWPContext = {} ;
   this.Events = {"e11652_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12652_client": ["DDO_CONTRATO_VALORUNDCNTATUAL.ONOPTIONCLICKED", true] ,"e13652_client": ["DDO_CONTRATO_ATIVO.ONOPTIONCLICKED", true] ,"e14652_client": ["VORDEREDBY.CLICK", true] ,"e15652_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e16652_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e17652_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e18652_client": ["'DOCLEANFILTERS'", true] ,"e19652_client": ["'ADDDYNAMICFILTERS1'", true] ,"e20652_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e21652_client": ["'ADDDYNAMICFILTERS2'", true] ,"e22652_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e23652_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e30652_client": ["ENTER", true] ,"e31652_client": ["CANCEL", true] ,"e27652_client": ["'DOASSOCIARGESTOR'", false] ,"e28652_client": ["'DOASSOCIARAUXILIAR'", false] ,"e29652_client": ["'DOASSOCIARSISTEMA'", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],[{av:'AV129Contrato_ValorUndCntAtualTitleFilterData',fld:'vCONTRATO_VALORUNDCNTATUALTITLEFILTERDATA',pic:'',nv:null},{av:'AV120Contrato_AtivoTitleFilterData',fld:'vCONTRATO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTRATADA_PESSOANOM","Title")',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{ctrl:'CONTRATO_NUMERO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTRATO_NUMERO","Title")',ctrl:'CONTRATO_NUMERO',prop:'Title'},{ctrl:'CONTRATO_NUMEROATA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTRATO_NUMEROATA","Title")',ctrl:'CONTRATO_NUMEROATA',prop:'Title'},{ctrl:'CONTRATO_ANO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTRATO_ANO","Title")',ctrl:'CONTRATO_ANO',prop:'Title'},{ctrl:'CONTRATO_VALORUNDCNTATUAL',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTRATO_VALORUNDCNTATUAL","Title")',ctrl:'CONTRATO_VALORUNDCNTATUAL',prop:'Title'},{ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTRATADA_PESSOACNPJ","Title")',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{ctrl:'CONTRATO_ATIVO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTRATO_ATIVO","Title")',ctrl:'CONTRATO_ATIVO',prop:'Title'},{av:'AV117GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV118GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("vASSOCIARGESTOR","Title")',ctrl:'vASSOCIARGESTOR',prop:'Title'},{av:'gx.fn.getCtrlProperty("vASSOCIARAUXILIAR","Title")',ctrl:'vASSOCIARAUXILIAR',prop:'Title'},{av:'gx.fn.getCtrlProperty("vASSOCIARSISTEMA","Title")',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'gx.fn.getCtrlProperty("vUPDATE","Visible")',ctrl:'vUPDATE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDELETE","Visible")',ctrl:'vDELETE',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Visible")',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_CONTRATO_VALORUNDCNTATUAL.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'this.DDO_CONTRATO_VALORUNDCNTATUALContainer.ActiveEventKey',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'ActiveEventKey'},{av:'this.DDO_CONTRATO_VALORUNDCNTATUALContainer.FilteredText_get',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredText_get'},{av:'this.DDO_CONTRATO_VALORUNDCNTATUALContainer.FilteredTextTo_get',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredTextTo_get'}],[{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]];
   this.EvtParms["DDO_CONTRATO_ATIVO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'this.DDO_CONTRATO_ATIVOContainer.ActiveEventKey',ctrl:'DDO_CONTRATO_ATIVO',prop:'ActiveEventKey'},{av:'this.DDO_CONTRATO_ATIVOContainer.SelectedValue_get',ctrl:'DDO_CONTRATO_ATIVO',prop:'SelectedValue_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_CONTRATO_ATIVOContainer.SortedStatus',ctrl:'DDO_CONTRATO_ATIVO',prop:'SortedStatus'},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],[{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Enabled")',ctrl:'vUPDATE',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Enabled")',ctrl:'vDELETE',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Tooltiptext")',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Link")',ctrl:'vDISPLAY',prop:'Link'},{av:'AV49Display',fld:'vDISPLAY',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDISPLAY","Enabled")',ctrl:'vDISPLAY',prop:'Enabled'},{av:'AV75AssociarGestor',fld:'vASSOCIARGESTOR',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vASSOCIARGESTOR","Tooltiptext")',ctrl:'vASSOCIARGESTOR',prop:'Tooltiptext'},{av:'AV119AssociarAuxiliar',fld:'vASSOCIARAUXILIAR',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vASSOCIARAUXILIAR","Tooltiptext")',ctrl:'vASSOCIARAUXILIAR',prop:'Tooltiptext'},{av:'AV80AssociarSistema',fld:'vASSOCIARSISTEMA',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vASSOCIARSISTEMA","Tooltiptext")',ctrl:'vASSOCIARSISTEMA',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("CONTRATADA_PESSOANOM","Link")',ctrl:'CONTRATADA_PESSOANOM',prop:'Link'},{av:'gx.fn.getCtrlProperty("CONTRATO_NUMEROATA","Link")',ctrl:'CONTRATO_NUMEROATA',prop:'Link'},{av:'gx.fn.getCtrlProperty("CONTRATADA_PESSOANOM","Forecolor")',ctrl:'CONTRATADA_PESSOANOM',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("CONTRATO_NUMERO","Forecolor")',ctrl:'CONTRATO_NUMERO',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("CONTRATO_NUMEROATA","Forecolor")',ctrl:'CONTRATO_NUMEROATA',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("CONTRATO_ANO","Forecolor")',ctrl:'CONTRATO_ANO',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("CONTRATO_DATATERMINO","Forecolor")',ctrl:'CONTRATO_DATATERMINO',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("CONTRATO_VALORUNDCNTATUAL","Forecolor")',ctrl:'CONTRATO_VALORUNDCNTATUAL',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_SALDO","Forecolor")',ctrl:'vSALDOCONTRATO_SALDO',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("CONTRATADA_PESSOACNPJ","Forecolor")',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Forecolor'},{av:'AV78SaldoContrato_Saldo',fld:'vSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],[]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO2","Visible")',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA2","Visible")',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM2","Visible")',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO3","Visible")',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA3","Visible")',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM3","Visible")',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO1","Visible")',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA1","Visible")',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM1","Visible")',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO1","Visible")',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA1","Visible")',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM1","Visible")',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO2","Visible")',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA2","Visible")',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM2","Visible")',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO3","Visible")',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA3","Visible")',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM3","Visible")',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO1","Visible")',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA1","Visible")',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM1","Visible")',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO2","Visible")',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA2","Visible")',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM2","Visible")',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO2","Visible")',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA2","Visible")',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM2","Visible")',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO3","Visible")',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA3","Visible")',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM3","Visible")',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO1","Visible")',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA1","Visible")',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM1","Visible")',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO3","Visible")',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA3","Visible")',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM3","Visible")',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'}]];
   this.EvtParms["'DOCLEANFILTERS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],[{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_CONTRATO_VALORUNDCNTATUALContainer.FilteredText_set',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredText_set'},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_CONTRATO_VALORUNDCNTATUALContainer.FilteredTextTo_set',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredTextTo_set'},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'this.DDO_CONTRATO_ATIVOContainer.SelectedValue_set',ctrl:'DDO_CONTRATO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO1","Visible")',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA1","Visible")',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM1","Visible")',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO2","Visible")',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA2","Visible")',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM2","Visible")',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMERO3","Visible")',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_NUMEROATA3","Visible")',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATO_PREPOSTONOM3","Visible")',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3","Visible")',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'}]];
   this.EvtParms["'DOASSOCIARGESTOR'"] = [[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["'DOASSOCIARAUXILIAR'"] = [[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["'DOASSOCIARSISTEMA'"] = [[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.setVCMap("A83Contrato_DataVigenciaTermino", "CONTRATO_DATAVIGENCIATERMINO", 0, "date");
   this.setVCMap("A843Contrato_DataFimTA", "CONTRATO_DATAFIMTA", 0, "date");
   this.setVCMap("AV189Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV30DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV29DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A39Contratada_Codigo", "CONTRATADA_CODIGO", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV189Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV30DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV29DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A39Contratada_Codigo", "CONTRATADA_CODIGO", 0, "int");
   this.setVCMap("AV6WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   GridContainer.addRefreshingVar(this.GXValidFnc[18]);
   GridContainer.addRefreshingVar(this.GXValidFnc[19]);
   GridContainer.addRefreshingVar(this.GXValidFnc[31]);
   GridContainer.addRefreshingVar(this.GXValidFnc[39]);
   GridContainer.addRefreshingVar(this.GXValidFnc[46]);
   GridContainer.addRefreshingVar(this.GXValidFnc[48]);
   GridContainer.addRefreshingVar(this.GXValidFnc[49]);
   GridContainer.addRefreshingVar(this.GXValidFnc[50]);
   GridContainer.addRefreshingVar(this.GXValidFnc[51]);
   GridContainer.addRefreshingVar(this.GXValidFnc[55]);
   GridContainer.addRefreshingVar(this.GXValidFnc[59]);
   GridContainer.addRefreshingVar(this.GXValidFnc[67]);
   GridContainer.addRefreshingVar(this.GXValidFnc[74]);
   GridContainer.addRefreshingVar(this.GXValidFnc[76]);
   GridContainer.addRefreshingVar(this.GXValidFnc[77]);
   GridContainer.addRefreshingVar(this.GXValidFnc[78]);
   GridContainer.addRefreshingVar(this.GXValidFnc[79]);
   GridContainer.addRefreshingVar(this.GXValidFnc[83]);
   GridContainer.addRefreshingVar(this.GXValidFnc[87]);
   GridContainer.addRefreshingVar(this.GXValidFnc[95]);
   GridContainer.addRefreshingVar(this.GXValidFnc[102]);
   GridContainer.addRefreshingVar(this.GXValidFnc[104]);
   GridContainer.addRefreshingVar(this.GXValidFnc[105]);
   GridContainer.addRefreshingVar(this.GXValidFnc[106]);
   GridContainer.addRefreshingVar(this.GXValidFnc[107]);
   GridContainer.addRefreshingVar(this.GXValidFnc[111]);
   GridContainer.addRefreshingVar(this.GXValidFnc[115]);
   GridContainer.addRefreshingVar(this.GXValidFnc[149]);
   GridContainer.addRefreshingVar(this.GXValidFnc[150]);
   GridContainer.addRefreshingVar(this.GXValidFnc[153]);
   GridContainer.addRefreshingVar(this.GXValidFnc[155]);
   GridContainer.addRefreshingVar(this.GXValidFnc[157]);
   GridContainer.addRefreshingVar(this.GXValidFnc[151]);
   GridContainer.addRefreshingVar(this.GXValidFnc[152]);
   GridContainer.addRefreshingVar({rfrVar:"AV189Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV10GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV30DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV29DynamicFiltersRemoving"});
   GridContainer.addRefreshingVar({rfrVar:"A39Contratada_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"AV6WWPContext"});
   GridContainer.addRefreshingVar({rfrVar:"A74Contrato_Codigo", rfrProp:"Value", gxAttId:"74"});
   GridContainer.addRefreshingVar({rfrVar:"A92Contrato_Ativo", rfrProp:"Value", gxAttId:"92"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwcontrato);
