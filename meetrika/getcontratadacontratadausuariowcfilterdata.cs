/*
               File: GetContratadaContratadaUsuarioWCFilterData
        Description: Get Contratada Contratada Usuario WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:21.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratadacontratadausuariowcfilterdata : GXProcedure
   {
      public getcontratadacontratadausuariowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratadacontratadausuariowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratadacontratadausuariowcfilterdata objgetcontratadacontratadausuariowcfilterdata;
         objgetcontratadacontratadausuariowcfilterdata = new getcontratadacontratadausuariowcfilterdata();
         objgetcontratadacontratadausuariowcfilterdata.AV20DDOName = aP0_DDOName;
         objgetcontratadacontratadausuariowcfilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetcontratadacontratadausuariowcfilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratadacontratadausuariowcfilterdata.AV24OptionsJson = "" ;
         objgetcontratadacontratadausuariowcfilterdata.AV27OptionsDescJson = "" ;
         objgetcontratadacontratadausuariowcfilterdata.AV29OptionIndexesJson = "" ;
         objgetcontratadacontratadausuariowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratadacontratadausuariowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratadacontratadausuariowcfilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratadacontratadausuariowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_USUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADAUSUARIO_USUARIOPESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_CONTRATADAUSUARIO_USUARIOPESSOADOC") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADAUSUARIO_USUARIOPESSOADOCOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("ContratadaContratadaUsuarioWCGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratadaContratadaUsuarioWCGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("ContratadaContratadaUsuarioWCGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV10TFUsuario_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV11TFUsuario_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV12TFContratadaUsuario_UsuarioPessoaNom = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOADOC") == 0 )
            {
               AV14TFContratadaUsuario_UsuarioPessoaDoc = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOADOC_SEL") == 0 )
            {
               AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOATIVO_SEL") == 0 )
            {
               AV40TFContratadaUsuario_UsuarioAtivo_Sel = (short)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATADAUSUARIO_CONTRATADACOD") == 0 )
            {
               AV38ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV37ContratadaUsuario_UsuarioPessoaNom1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUSUARIO_NOMEOPTIONS' Routine */
         AV10TFUsuario_Nome = AV18SearchTxt;
         AV11TFUsuario_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV36DynamicFiltersSelector1 ,
                                              AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                              AV14TFContratadaUsuario_UsuarioPessoaDoc ,
                                              AV40TFContratadaUsuario_UsuarioAtivo_Sel ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A2Usuario_Nome ,
                                              A491ContratadaUsuario_UsuarioPessoaDoc ,
                                              A1394ContratadaUsuario_UsuarioAtivo ,
                                              AV38ContratadaUsuario_ContratadaCod ,
                                              A66ContratadaUsuario_ContratadaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         lV14TFContratadaUsuario_UsuarioPessoaDoc = StringUtil.Concat( StringUtil.RTrim( AV14TFContratadaUsuario_UsuarioPessoaDoc), "%", "");
         /* Using cursor P00FD2 */
         pr_default.execute(0, new Object[] {AV38ContratadaUsuario_ContratadaCod, lV37ContratadaUsuario_UsuarioPessoaNom1, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFContratadaUsuario_UsuarioPessoaNom, AV13TFContratadaUsuario_UsuarioPessoaNom_Sel, lV14TFContratadaUsuario_UsuarioPessoaDoc, AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFD2 = false;
            A69ContratadaUsuario_UsuarioCod = P00FD2_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00FD2_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00FD2_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A66ContratadaUsuario_ContratadaCod = P00FD2_A66ContratadaUsuario_ContratadaCod[0];
            A2Usuario_Nome = P00FD2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00FD2_n2Usuario_Nome[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00FD2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00FD2_n1394ContratadaUsuario_UsuarioAtivo[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = P00FD2_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = P00FD2_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00FD2_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00FD2_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00FD2_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00FD2_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00FD2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00FD2_n2Usuario_Nome[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00FD2_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00FD2_n1394ContratadaUsuario_UsuarioAtivo[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = P00FD2_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = P00FD2_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00FD2_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00FD2_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00FD2_A66ContratadaUsuario_ContratadaCod[0] == A66ContratadaUsuario_ContratadaCod ) && ( StringUtil.StrCmp(P00FD2_A2Usuario_Nome[0], A2Usuario_Nome) == 0 ) )
            {
               BRKFD2 = false;
               A69ContratadaUsuario_UsuarioCod = P00FD2_A69ContratadaUsuario_UsuarioCod[0];
               AV30count = (long)(AV30count+1);
               BRKFD2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) )
            {
               AV22Option = A2Usuario_Nome;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFD2 )
            {
               BRKFD2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADAUSUARIO_USUARIOPESSOANOMOPTIONS' Routine */
         AV12TFContratadaUsuario_UsuarioPessoaNom = AV18SearchTxt;
         AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV36DynamicFiltersSelector1 ,
                                              AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                              AV14TFContratadaUsuario_UsuarioPessoaDoc ,
                                              AV40TFContratadaUsuario_UsuarioAtivo_Sel ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A2Usuario_Nome ,
                                              A491ContratadaUsuario_UsuarioPessoaDoc ,
                                              A1394ContratadaUsuario_UsuarioAtivo ,
                                              AV38ContratadaUsuario_ContratadaCod ,
                                              A66ContratadaUsuario_ContratadaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         lV14TFContratadaUsuario_UsuarioPessoaDoc = StringUtil.Concat( StringUtil.RTrim( AV14TFContratadaUsuario_UsuarioPessoaDoc), "%", "");
         /* Using cursor P00FD3 */
         pr_default.execute(1, new Object[] {AV38ContratadaUsuario_ContratadaCod, lV37ContratadaUsuario_UsuarioPessoaNom1, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFContratadaUsuario_UsuarioPessoaNom, AV13TFContratadaUsuario_UsuarioPessoaNom_Sel, lV14TFContratadaUsuario_UsuarioPessoaDoc, AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFD4 = false;
            A69ContratadaUsuario_UsuarioCod = P00FD3_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00FD3_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00FD3_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A66ContratadaUsuario_ContratadaCod = P00FD3_A66ContratadaUsuario_ContratadaCod[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00FD3_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00FD3_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00FD3_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00FD3_n1394ContratadaUsuario_UsuarioAtivo[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = P00FD3_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = P00FD3_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A2Usuario_Nome = P00FD3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00FD3_n2Usuario_Nome[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00FD3_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00FD3_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00FD3_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00FD3_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = P00FD3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00FD3_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00FD3_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00FD3_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = P00FD3_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = P00FD3_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00FD3_A66ContratadaUsuario_ContratadaCod[0] == A66ContratadaUsuario_ContratadaCod ) && ( StringUtil.StrCmp(P00FD3_A71ContratadaUsuario_UsuarioPessoaNom[0], A71ContratadaUsuario_UsuarioPessoaNom) == 0 ) )
            {
               BRKFD4 = false;
               A69ContratadaUsuario_UsuarioCod = P00FD3_A69ContratadaUsuario_UsuarioCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00FD3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00FD3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00FD3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00FD3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               AV30count = (long)(AV30count+1);
               BRKFD4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom)) )
            {
               AV22Option = A71ContratadaUsuario_UsuarioPessoaNom;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFD4 )
            {
               BRKFD4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADAUSUARIO_USUARIOPESSOADOCOPTIONS' Routine */
         AV14TFContratadaUsuario_UsuarioPessoaDoc = AV18SearchTxt;
         AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV36DynamicFiltersSelector1 ,
                                              AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV11TFUsuario_Nome_Sel ,
                                              AV10TFUsuario_Nome ,
                                              AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                              AV14TFContratadaUsuario_UsuarioPessoaDoc ,
                                              AV40TFContratadaUsuario_UsuarioAtivo_Sel ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A2Usuario_Nome ,
                                              A491ContratadaUsuario_UsuarioPessoaDoc ,
                                              A1394ContratadaUsuario_UsuarioAtivo ,
                                              AV38ContratadaUsuario_ContratadaCod ,
                                              A66ContratadaUsuario_ContratadaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV37ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         lV10TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUsuario_Nome), 50, "%");
         lV12TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         lV14TFContratadaUsuario_UsuarioPessoaDoc = StringUtil.Concat( StringUtil.RTrim( AV14TFContratadaUsuario_UsuarioPessoaDoc), "%", "");
         /* Using cursor P00FD4 */
         pr_default.execute(2, new Object[] {AV38ContratadaUsuario_ContratadaCod, lV37ContratadaUsuario_UsuarioPessoaNom1, lV10TFUsuario_Nome, AV11TFUsuario_Nome_Sel, lV12TFContratadaUsuario_UsuarioPessoaNom, AV13TFContratadaUsuario_UsuarioPessoaNom_Sel, lV14TFContratadaUsuario_UsuarioPessoaDoc, AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKFD6 = false;
            A69ContratadaUsuario_UsuarioCod = P00FD4_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00FD4_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00FD4_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A66ContratadaUsuario_ContratadaCod = P00FD4_A66ContratadaUsuario_ContratadaCod[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = P00FD4_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = P00FD4_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00FD4_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00FD4_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = P00FD4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00FD4_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00FD4_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00FD4_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00FD4_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00FD4_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A1394ContratadaUsuario_UsuarioAtivo = P00FD4_A1394ContratadaUsuario_UsuarioAtivo[0];
            n1394ContratadaUsuario_UsuarioAtivo = P00FD4_n1394ContratadaUsuario_UsuarioAtivo[0];
            A2Usuario_Nome = P00FD4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00FD4_n2Usuario_Nome[0];
            A491ContratadaUsuario_UsuarioPessoaDoc = P00FD4_A491ContratadaUsuario_UsuarioPessoaDoc[0];
            n491ContratadaUsuario_UsuarioPessoaDoc = P00FD4_n491ContratadaUsuario_UsuarioPessoaDoc[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00FD4_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00FD4_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00FD4_A66ContratadaUsuario_ContratadaCod[0] == A66ContratadaUsuario_ContratadaCod ) && ( StringUtil.StrCmp(P00FD4_A491ContratadaUsuario_UsuarioPessoaDoc[0], A491ContratadaUsuario_UsuarioPessoaDoc) == 0 ) )
            {
               BRKFD6 = false;
               A69ContratadaUsuario_UsuarioCod = P00FD4_A69ContratadaUsuario_UsuarioCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00FD4_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00FD4_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00FD4_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00FD4_n70ContratadaUsuario_UsuarioPessoaCod[0];
               AV30count = (long)(AV30count+1);
               BRKFD6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A491ContratadaUsuario_UsuarioPessoaDoc)) )
            {
               AV22Option = A491ContratadaUsuario_UsuarioPessoaDoc;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFD6 )
            {
               BRKFD6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUsuario_Nome = "";
         AV11TFUsuario_Nome_Sel = "";
         AV12TFContratadaUsuario_UsuarioPessoaNom = "";
         AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         AV14TFContratadaUsuario_UsuarioPessoaDoc = "";
         AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37ContratadaUsuario_UsuarioPessoaNom1 = "";
         scmdbuf = "";
         lV37ContratadaUsuario_UsuarioPessoaNom1 = "";
         lV10TFUsuario_Nome = "";
         lV12TFContratadaUsuario_UsuarioPessoaNom = "";
         lV14TFContratadaUsuario_UsuarioPessoaDoc = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         A2Usuario_Nome = "";
         A491ContratadaUsuario_UsuarioPessoaDoc = "";
         P00FD2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00FD2_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00FD2_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00FD2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00FD2_A2Usuario_Nome = new String[] {""} ;
         P00FD2_n2Usuario_Nome = new bool[] {false} ;
         P00FD2_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00FD2_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00FD2_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         P00FD2_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         P00FD2_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00FD2_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         AV22Option = "";
         P00FD3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00FD3_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00FD3_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00FD3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00FD3_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00FD3_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00FD3_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00FD3_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00FD3_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         P00FD3_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         P00FD3_A2Usuario_Nome = new String[] {""} ;
         P00FD3_n2Usuario_Nome = new bool[] {false} ;
         P00FD4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00FD4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00FD4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00FD4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00FD4_A491ContratadaUsuario_UsuarioPessoaDoc = new String[] {""} ;
         P00FD4_n491ContratadaUsuario_UsuarioPessoaDoc = new bool[] {false} ;
         P00FD4_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00FD4_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00FD4_A2Usuario_Nome = new String[] {""} ;
         P00FD4_n2Usuario_Nome = new bool[] {false} ;
         P00FD4_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00FD4_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratadacontratadausuariowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FD2_A69ContratadaUsuario_UsuarioCod, P00FD2_A70ContratadaUsuario_UsuarioPessoaCod, P00FD2_n70ContratadaUsuario_UsuarioPessoaCod, P00FD2_A66ContratadaUsuario_ContratadaCod, P00FD2_A2Usuario_Nome, P00FD2_n2Usuario_Nome, P00FD2_A1394ContratadaUsuario_UsuarioAtivo, P00FD2_n1394ContratadaUsuario_UsuarioAtivo, P00FD2_A491ContratadaUsuario_UsuarioPessoaDoc, P00FD2_n491ContratadaUsuario_UsuarioPessoaDoc,
               P00FD2_A71ContratadaUsuario_UsuarioPessoaNom, P00FD2_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               P00FD3_A69ContratadaUsuario_UsuarioCod, P00FD3_A70ContratadaUsuario_UsuarioPessoaCod, P00FD3_n70ContratadaUsuario_UsuarioPessoaCod, P00FD3_A66ContratadaUsuario_ContratadaCod, P00FD3_A71ContratadaUsuario_UsuarioPessoaNom, P00FD3_n71ContratadaUsuario_UsuarioPessoaNom, P00FD3_A1394ContratadaUsuario_UsuarioAtivo, P00FD3_n1394ContratadaUsuario_UsuarioAtivo, P00FD3_A491ContratadaUsuario_UsuarioPessoaDoc, P00FD3_n491ContratadaUsuario_UsuarioPessoaDoc,
               P00FD3_A2Usuario_Nome, P00FD3_n2Usuario_Nome
               }
               , new Object[] {
               P00FD4_A69ContratadaUsuario_UsuarioCod, P00FD4_A70ContratadaUsuario_UsuarioPessoaCod, P00FD4_n70ContratadaUsuario_UsuarioPessoaCod, P00FD4_A66ContratadaUsuario_ContratadaCod, P00FD4_A491ContratadaUsuario_UsuarioPessoaDoc, P00FD4_n491ContratadaUsuario_UsuarioPessoaDoc, P00FD4_A1394ContratadaUsuario_UsuarioAtivo, P00FD4_n1394ContratadaUsuario_UsuarioAtivo, P00FD4_A2Usuario_Nome, P00FD4_n2Usuario_Nome,
               P00FD4_A71ContratadaUsuario_UsuarioPessoaNom, P00FD4_n71ContratadaUsuario_UsuarioPessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV40TFContratadaUsuario_UsuarioAtivo_Sel ;
      private int AV43GXV1 ;
      private int AV38ContratadaUsuario_ContratadaCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private long AV30count ;
      private String AV10TFUsuario_Nome ;
      private String AV11TFUsuario_Nome_Sel ;
      private String AV12TFContratadaUsuario_UsuarioPessoaNom ;
      private String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ;
      private String AV37ContratadaUsuario_UsuarioPessoaNom1 ;
      private String scmdbuf ;
      private String lV37ContratadaUsuario_UsuarioPessoaNom1 ;
      private String lV10TFUsuario_Nome ;
      private String lV12TFContratadaUsuario_UsuarioPessoaNom ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A2Usuario_Nome ;
      private bool returnInSub ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool BRKFD2 ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n2Usuario_Nome ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool BRKFD4 ;
      private bool BRKFD6 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV14TFContratadaUsuario_UsuarioPessoaDoc ;
      private String AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel ;
      private String AV36DynamicFiltersSelector1 ;
      private String lV14TFContratadaUsuario_UsuarioPessoaDoc ;
      private String A491ContratadaUsuario_UsuarioPessoaDoc ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FD2_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00FD2_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00FD2_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] P00FD2_A66ContratadaUsuario_ContratadaCod ;
      private String[] P00FD2_A2Usuario_Nome ;
      private bool[] P00FD2_n2Usuario_Nome ;
      private bool[] P00FD2_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P00FD2_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] P00FD2_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] P00FD2_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private String[] P00FD2_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00FD2_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] P00FD3_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00FD3_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00FD3_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] P00FD3_A66ContratadaUsuario_ContratadaCod ;
      private String[] P00FD3_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00FD3_n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00FD3_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P00FD3_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] P00FD3_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] P00FD3_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private String[] P00FD3_A2Usuario_Nome ;
      private bool[] P00FD3_n2Usuario_Nome ;
      private int[] P00FD4_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00FD4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00FD4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] P00FD4_A66ContratadaUsuario_ContratadaCod ;
      private String[] P00FD4_A491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] P00FD4_n491ContratadaUsuario_UsuarioPessoaDoc ;
      private bool[] P00FD4_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P00FD4_n1394ContratadaUsuario_UsuarioAtivo ;
      private String[] P00FD4_A2Usuario_Nome ;
      private bool[] P00FD4_n2Usuario_Nome ;
      private String[] P00FD4_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00FD4_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getcontratadacontratadausuariowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FD2( IGxContext context ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                             String AV14TFContratadaUsuario_UsuarioPessoaDoc ,
                                             short AV40TFContratadaUsuario_UsuarioAtivo_Sel ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A491ContratadaUsuario_UsuarioPessoaDoc ,
                                             bool A1394ContratadaUsuario_UsuarioAtivo ,
                                             int AV38ContratadaUsuario_ContratadaCod ,
                                             int A66ContratadaUsuario_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Nome], T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T3.[Pessoa_Docto] AS ContratadaUsuario_UsuarioPessoaDoc, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV38ContratadaUsuario_ContratadaCod)";
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratadaUsuario_UsuarioPessoaDoc)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV14TFContratadaUsuario_UsuarioPessoaDoc)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV40TFContratadaUsuario_UsuarioAtivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 1)";
         }
         if ( AV40TFContratadaUsuario_UsuarioAtivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T2.[Usuario_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00FD3( IGxContext context ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                             String AV14TFContratadaUsuario_UsuarioPessoaDoc ,
                                             short AV40TFContratadaUsuario_UsuarioAtivo_Sel ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A491ContratadaUsuario_UsuarioPessoaDoc ,
                                             bool A1394ContratadaUsuario_UsuarioAtivo ,
                                             int AV38ContratadaUsuario_ContratadaCod ,
                                             int A66ContratadaUsuario_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [8] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T3.[Pessoa_Docto] AS ContratadaUsuario_UsuarioPessoaDoc, T2.[Usuario_Nome] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV38ContratadaUsuario_ContratadaCod)";
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratadaUsuario_UsuarioPessoaDoc)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV14TFContratadaUsuario_UsuarioPessoaDoc)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV40TFContratadaUsuario_UsuarioAtivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 1)";
         }
         if ( AV40TFContratadaUsuario_UsuarioAtivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00FD4( IGxContext context ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV11TFUsuario_Nome_Sel ,
                                             String AV10TFUsuario_Nome ,
                                             String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV12TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel ,
                                             String AV14TFContratadaUsuario_UsuarioPessoaDoc ,
                                             short AV40TFContratadaUsuario_UsuarioAtivo_Sel ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A491ContratadaUsuario_UsuarioPessoaDoc ,
                                             bool A1394ContratadaUsuario_UsuarioAtivo ,
                                             int AV38ContratadaUsuario_ContratadaCod ,
                                             int A66ContratadaUsuario_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [8] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Docto] AS ContratadaUsuario_UsuarioPessoaDoc, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T2.[Usuario_Nome], T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV38ContratadaUsuario_ContratadaCod)";
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV37ContratadaUsuario_UsuarioPessoaNom1 + '%')";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV10TFUsuario_Nome)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV11TFUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV12TFContratadaUsuario_UsuarioPessoaNom)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV13TFContratadaUsuario_UsuarioPessoaNom_Sel)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratadaUsuario_UsuarioPessoaDoc)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV14TFContratadaUsuario_UsuarioPessoaDoc)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV40TFContratadaUsuario_UsuarioAtivo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 1)";
         }
         if ( AV40TFContratadaUsuario_UsuarioAtivo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T3.[Pessoa_Docto]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FD2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
               case 1 :
                     return conditional_P00FD3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
               case 2 :
                     return conditional_P00FD4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FD2 ;
          prmP00FD2 = new Object[] {
          new Object[] {"@AV38ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratadaUsuario_UsuarioPessoaDoc",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00FD3 ;
          prmP00FD3 = new Object[] {
          new Object[] {"@AV38ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratadaUsuario_UsuarioPessoaDoc",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00FD4 ;
          prmP00FD4 = new Object[] {
          new Object[] {"@AV38ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV10TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratadaUsuario_UsuarioPessoaDoc",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV15TFContratadaUsuario_UsuarioPessoaDoc_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FD2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FD2,100,0,true,false )
             ,new CursorDef("P00FD3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FD3,100,0,true,false )
             ,new CursorDef("P00FD4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FD4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratadacontratadausuariowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratadacontratadausuariowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratadacontratadausuariowcfilterdata") )
          {
             return  ;
          }
          getcontratadacontratadausuariowcfilterdata worker = new getcontratadacontratadausuariowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
