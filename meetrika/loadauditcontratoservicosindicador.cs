/*
               File: LoadAuditContratoServicosIndicador
        Description: Load Audit Contrato Servicos Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:18:19.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditcontratoservicosindicador : GXProcedure
   {
      public loadauditcontratoservicosindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditcontratoservicosindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_ContratoServicosIndicador_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16ContratoServicosIndicador_Codigo = aP2_ContratoServicosIndicador_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_ContratoServicosIndicador_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditcontratoservicosindicador objloadauditcontratoservicosindicador;
         objloadauditcontratoservicosindicador = new loadauditcontratoservicosindicador();
         objloadauditcontratoservicosindicador.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditcontratoservicosindicador.AV10AuditingObject = aP1_AuditingObject;
         objloadauditcontratoservicosindicador.AV16ContratoServicosIndicador_Codigo = aP2_ContratoServicosIndicador_Codigo;
         objloadauditcontratoservicosindicador.AV14ActualMode = aP3_ActualMode;
         objloadauditcontratoservicosindicador.context.SetSubmitInitialConfig(context);
         objloadauditcontratoservicosindicador.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditcontratoservicosindicador);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditcontratoservicosindicador)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00WF3 */
         pr_default.execute(0, new Object[] {AV16ContratoServicosIndicador_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1269ContratoServicosIndicador_Codigo = P00WF3_A1269ContratoServicosIndicador_Codigo[0];
            A1270ContratoServicosIndicador_CntSrvCod = P00WF3_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1296ContratoServicosIndicador_ContratoCod = P00WF3_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00WF3_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF3_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF3_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1271ContratoServicosIndicador_Numero = P00WF3_A1271ContratoServicosIndicador_Numero[0];
            A2051ContratoServicosIndicador_Sigla = P00WF3_A2051ContratoServicosIndicador_Sigla[0];
            n2051ContratoServicosIndicador_Sigla = P00WF3_n2051ContratoServicosIndicador_Sigla[0];
            A1305ContratoServicosIndicador_Finalidade = P00WF3_A1305ContratoServicosIndicador_Finalidade[0];
            n1305ContratoServicosIndicador_Finalidade = P00WF3_n1305ContratoServicosIndicador_Finalidade[0];
            A1306ContratoServicosIndicador_Meta = P00WF3_A1306ContratoServicosIndicador_Meta[0];
            n1306ContratoServicosIndicador_Meta = P00WF3_n1306ContratoServicosIndicador_Meta[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = P00WF3_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            n1307ContratoServicosIndicador_InstrumentoMedicao = P00WF3_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1308ContratoServicosIndicador_Tipo = P00WF3_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00WF3_n1308ContratoServicosIndicador_Tipo[0];
            A1309ContratoServicosIndicador_Periodicidade = P00WF3_A1309ContratoServicosIndicador_Periodicidade[0];
            n1309ContratoServicosIndicador_Periodicidade = P00WF3_n1309ContratoServicosIndicador_Periodicidade[0];
            A1310ContratoServicosIndicador_Vigencia = P00WF3_A1310ContratoServicosIndicador_Vigencia[0];
            n1310ContratoServicosIndicador_Vigencia = P00WF3_n1310ContratoServicosIndicador_Vigencia[0];
            A1345ContratoServicosIndicador_CalculoSob = P00WF3_A1345ContratoServicosIndicador_CalculoSob[0];
            n1345ContratoServicosIndicador_CalculoSob = P00WF3_n1345ContratoServicosIndicador_CalculoSob[0];
            A2052ContratoServicosIndicador_Formato = P00WF3_A2052ContratoServicosIndicador_Formato[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00WF3_A1298ContratoServicosIndicador_QtdeFaixas[0];
            A1274ContratoServicosIndicador_Indicador = P00WF3_A1274ContratoServicosIndicador_Indicador[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00WF3_A1298ContratoServicosIndicador_QtdeFaixas[0];
            A1296ContratoServicosIndicador_ContratoCod = P00WF3_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00WF3_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF3_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF3_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1395ContratoServicosIndicador_ComQtdeFaixas = StringUtil.Substring( A1274ContratoServicosIndicador_Indicador, 1, 30) + "(" + StringUtil.Trim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)) + ")";
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosIndicador";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_CntSrvCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_ContratoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Numero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Indicador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indicador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1274ContratoServicosIndicador_Indicador;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2051ContratoServicosIndicador_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Finalidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Finalidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1305ContratoServicosIndicador_Finalidade;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Meta";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Meta a cumprir";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1306ContratoServicosIndicador_Meta;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_InstrumentoMedicao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Instrumento de Medi��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1307ContratoServicosIndicador_InstrumentoMedicao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Indicador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1308ContratoServicosIndicador_Tipo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Periodicidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Periodicidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1309ContratoServicosIndicador_Periodicidade;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Vigencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vig�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1310ContratoServicosIndicador_Vigencia;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_CalculoSob";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo Sob";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1345ContratoServicosIndicador_CalculoSob;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Formato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Formato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_QtdeFaixas";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faixas";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_ComQtdeFaixas";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indicador";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1395ContratoServicosIndicador_ComQtdeFaixas;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Using cursor P00WF4 */
            pr_default.execute(1, new Object[] {A1269ContratoServicosIndicador_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1299ContratoServicosIndicadorFaixa_Codigo = P00WF4_A1299ContratoServicosIndicadorFaixa_Codigo[0];
               A1300ContratoServicosIndicadorFaixa_Numero = P00WF4_A1300ContratoServicosIndicadorFaixa_Numero[0];
               A1301ContratoServicosIndicadorFaixa_Desde = P00WF4_A1301ContratoServicosIndicadorFaixa_Desde[0];
               A1302ContratoServicosIndicadorFaixa_Ate = P00WF4_A1302ContratoServicosIndicadorFaixa_Ate[0];
               A1303ContratoServicosIndicadorFaixa_Reduz = P00WF4_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               A1304ContratoServicosIndicadorFaixa_Und = P00WF4_A1304ContratoServicosIndicadorFaixa_Und[0];
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosIndicadorFaixas";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Numero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Desde";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Ate";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Reduz";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Reduz";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1303ContratoServicosIndicadorFaixa_Reduz, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Und";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00WF7 */
         pr_default.execute(2, new Object[] {AV16ContratoServicosIndicador_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1270ContratoServicosIndicador_CntSrvCod = P00WF7_A1270ContratoServicosIndicador_CntSrvCod[0];
            A1296ContratoServicosIndicador_ContratoCod = P00WF7_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00WF7_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1271ContratoServicosIndicador_Numero = P00WF7_A1271ContratoServicosIndicador_Numero[0];
            A2051ContratoServicosIndicador_Sigla = P00WF7_A2051ContratoServicosIndicador_Sigla[0];
            n2051ContratoServicosIndicador_Sigla = P00WF7_n2051ContratoServicosIndicador_Sigla[0];
            A1305ContratoServicosIndicador_Finalidade = P00WF7_A1305ContratoServicosIndicador_Finalidade[0];
            n1305ContratoServicosIndicador_Finalidade = P00WF7_n1305ContratoServicosIndicador_Finalidade[0];
            A1306ContratoServicosIndicador_Meta = P00WF7_A1306ContratoServicosIndicador_Meta[0];
            n1306ContratoServicosIndicador_Meta = P00WF7_n1306ContratoServicosIndicador_Meta[0];
            A1307ContratoServicosIndicador_InstrumentoMedicao = P00WF7_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
            n1307ContratoServicosIndicador_InstrumentoMedicao = P00WF7_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
            A1308ContratoServicosIndicador_Tipo = P00WF7_A1308ContratoServicosIndicador_Tipo[0];
            n1308ContratoServicosIndicador_Tipo = P00WF7_n1308ContratoServicosIndicador_Tipo[0];
            A1309ContratoServicosIndicador_Periodicidade = P00WF7_A1309ContratoServicosIndicador_Periodicidade[0];
            n1309ContratoServicosIndicador_Periodicidade = P00WF7_n1309ContratoServicosIndicador_Periodicidade[0];
            A1310ContratoServicosIndicador_Vigencia = P00WF7_A1310ContratoServicosIndicador_Vigencia[0];
            n1310ContratoServicosIndicador_Vigencia = P00WF7_n1310ContratoServicosIndicador_Vigencia[0];
            A1345ContratoServicosIndicador_CalculoSob = P00WF7_A1345ContratoServicosIndicador_CalculoSob[0];
            n1345ContratoServicosIndicador_CalculoSob = P00WF7_n1345ContratoServicosIndicador_CalculoSob[0];
            A2052ContratoServicosIndicador_Formato = P00WF7_A2052ContratoServicosIndicador_Formato[0];
            A1269ContratoServicosIndicador_Codigo = P00WF7_A1269ContratoServicosIndicador_Codigo[0];
            A40000ContratoServicosIndicador_QtdeFaixas = P00WF7_A40000ContratoServicosIndicador_QtdeFaixas[0];
            n40000ContratoServicosIndicador_QtdeFaixas = P00WF7_n40000ContratoServicosIndicador_QtdeFaixas[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00WF7_A1298ContratoServicosIndicador_QtdeFaixas[0];
            A1274ContratoServicosIndicador_Indicador = P00WF7_A1274ContratoServicosIndicador_Indicador[0];
            A1296ContratoServicosIndicador_ContratoCod = P00WF7_A1296ContratoServicosIndicador_ContratoCod[0];
            n1296ContratoServicosIndicador_ContratoCod = P00WF7_n1296ContratoServicosIndicador_ContratoCod[0];
            A1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            n1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
            A1298ContratoServicosIndicador_QtdeFaixas = P00WF7_A1298ContratoServicosIndicador_QtdeFaixas[0];
            A40000ContratoServicosIndicador_QtdeFaixas = P00WF7_A40000ContratoServicosIndicador_QtdeFaixas[0];
            n40000ContratoServicosIndicador_QtdeFaixas = P00WF7_n40000ContratoServicosIndicador_QtdeFaixas[0];
            A1395ContratoServicosIndicador_ComQtdeFaixas = StringUtil.Substring( A1274ContratoServicosIndicador_Indicador, 1, 30) + "(" + StringUtil.Trim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)) + ")";
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosIndicador";
               AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_CntSrvCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_ContratoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Numero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Indicador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indicador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1274ContratoServicosIndicador_Indicador;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2051ContratoServicosIndicador_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Finalidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Finalidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1305ContratoServicosIndicador_Finalidade;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Meta";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Meta a cumprir";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1306ContratoServicosIndicador_Meta;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_InstrumentoMedicao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Instrumento de Medi��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1307ContratoServicosIndicador_InstrumentoMedicao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Indicador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1308ContratoServicosIndicador_Tipo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Periodicidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Periodicidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1309ContratoServicosIndicador_Periodicidade;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Vigencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Vig�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1310ContratoServicosIndicador_Vigencia;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_CalculoSob";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�lculo Sob";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1345ContratoServicosIndicador_CalculoSob;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_Formato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Formato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_QtdeFaixas";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Faixas";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicador_ComQtdeFaixas";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Indicador";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1395ContratoServicosIndicador_ComQtdeFaixas;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               /* Using cursor P00WF8 */
               pr_default.execute(3, new Object[] {A1269ContratoServicosIndicador_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A1299ContratoServicosIndicadorFaixa_Codigo = P00WF8_A1299ContratoServicosIndicadorFaixa_Codigo[0];
                  A1300ContratoServicosIndicadorFaixa_Numero = P00WF8_A1300ContratoServicosIndicadorFaixa_Numero[0];
                  A1301ContratoServicosIndicadorFaixa_Desde = P00WF8_A1301ContratoServicosIndicadorFaixa_Desde[0];
                  A1302ContratoServicosIndicadorFaixa_Ate = P00WF8_A1302ContratoServicosIndicadorFaixa_Ate[0];
                  A1303ContratoServicosIndicadorFaixa_Reduz = P00WF8_A1303ContratoServicosIndicadorFaixa_Reduz[0];
                  A1304ContratoServicosIndicadorFaixa_Und = P00WF8_A1304ContratoServicosIndicadorFaixa_Und[0];
                  AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                  AV11AuditingObjectRecordItem.gxTpr_Tablename = "ContratoServicosIndicadorFaixas";
                  AV11AuditingObjectRecordItem.gxTpr_Mode = "INS";
                  AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Codigo";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Codigo";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Numero";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Desde";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Ate";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Reduz";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Reduz";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1303ContratoServicosIndicadorFaixa_Reduz, 6, 2);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ContratoServicosIndicadorFaixa_Und";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
                  AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0);
                  AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
                  pr_default.readNext(3);
               }
               pr_default.close(3);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21CountUpdatedContratoServicosIndicadorFaixa_Codigo = 0;
               AV30GXV1 = 1;
               while ( AV30GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV30GXV1));
                  if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosIndicador") == 0 )
                  {
                     while ( (pr_default.getStatus(2) != 101) && ( P00WF7_A1269ContratoServicosIndicador_Codigo[0] == A1269ContratoServicosIndicador_Codigo ) )
                     {
                        A1270ContratoServicosIndicador_CntSrvCod = P00WF7_A1270ContratoServicosIndicador_CntSrvCod[0];
                        A1296ContratoServicosIndicador_ContratoCod = P00WF7_A1296ContratoServicosIndicador_ContratoCod[0];
                        n1296ContratoServicosIndicador_ContratoCod = P00WF7_n1296ContratoServicosIndicador_ContratoCod[0];
                        A1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
                        n1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
                        A1271ContratoServicosIndicador_Numero = P00WF7_A1271ContratoServicosIndicador_Numero[0];
                        A2051ContratoServicosIndicador_Sigla = P00WF7_A2051ContratoServicosIndicador_Sigla[0];
                        n2051ContratoServicosIndicador_Sigla = P00WF7_n2051ContratoServicosIndicador_Sigla[0];
                        A1305ContratoServicosIndicador_Finalidade = P00WF7_A1305ContratoServicosIndicador_Finalidade[0];
                        n1305ContratoServicosIndicador_Finalidade = P00WF7_n1305ContratoServicosIndicador_Finalidade[0];
                        A1306ContratoServicosIndicador_Meta = P00WF7_A1306ContratoServicosIndicador_Meta[0];
                        n1306ContratoServicosIndicador_Meta = P00WF7_n1306ContratoServicosIndicador_Meta[0];
                        A1307ContratoServicosIndicador_InstrumentoMedicao = P00WF7_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
                        n1307ContratoServicosIndicador_InstrumentoMedicao = P00WF7_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
                        A1308ContratoServicosIndicador_Tipo = P00WF7_A1308ContratoServicosIndicador_Tipo[0];
                        n1308ContratoServicosIndicador_Tipo = P00WF7_n1308ContratoServicosIndicador_Tipo[0];
                        A1309ContratoServicosIndicador_Periodicidade = P00WF7_A1309ContratoServicosIndicador_Periodicidade[0];
                        n1309ContratoServicosIndicador_Periodicidade = P00WF7_n1309ContratoServicosIndicador_Periodicidade[0];
                        A1310ContratoServicosIndicador_Vigencia = P00WF7_A1310ContratoServicosIndicador_Vigencia[0];
                        n1310ContratoServicosIndicador_Vigencia = P00WF7_n1310ContratoServicosIndicador_Vigencia[0];
                        A1345ContratoServicosIndicador_CalculoSob = P00WF7_A1345ContratoServicosIndicador_CalculoSob[0];
                        n1345ContratoServicosIndicador_CalculoSob = P00WF7_n1345ContratoServicosIndicador_CalculoSob[0];
                        A2052ContratoServicosIndicador_Formato = P00WF7_A2052ContratoServicosIndicador_Formato[0];
                        A1298ContratoServicosIndicador_QtdeFaixas = P00WF7_A1298ContratoServicosIndicador_QtdeFaixas[0];
                        A1274ContratoServicosIndicador_Indicador = P00WF7_A1274ContratoServicosIndicador_Indicador[0];
                        A1296ContratoServicosIndicador_ContratoCod = P00WF7_A1296ContratoServicosIndicador_ContratoCod[0];
                        n1296ContratoServicosIndicador_ContratoCod = P00WF7_n1296ContratoServicosIndicador_ContratoCod[0];
                        A1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_A1295ContratoServicosIndicador_AreaTrabalhoCod[0];
                        n1295ContratoServicosIndicador_AreaTrabalhoCod = P00WF7_n1295ContratoServicosIndicador_AreaTrabalhoCod[0];
                        A1298ContratoServicosIndicador_QtdeFaixas = P00WF7_A1298ContratoServicosIndicador_QtdeFaixas[0];
                        A1395ContratoServicosIndicador_ComQtdeFaixas = StringUtil.Substring( A1274ContratoServicosIndicador_Indicador, 1, 30) + "(" + StringUtil.Trim( StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0)) + ")";
                        AV32GXV2 = 1;
                        while ( AV32GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                        {
                           AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV32GXV2));
                           if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Codigo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_CntSrvCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1270ContratoServicosIndicador_CntSrvCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_ContratoCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1296ContratoServicosIndicador_ContratoCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_AreaTrabalhoCod") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1295ContratoServicosIndicador_AreaTrabalhoCod), 6, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Numero") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1271ContratoServicosIndicador_Numero), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Indicador") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1274ContratoServicosIndicador_Indicador;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Sigla") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2051ContratoServicosIndicador_Sigla;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Finalidade") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1305ContratoServicosIndicador_Finalidade;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Meta") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1306ContratoServicosIndicador_Meta;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_InstrumentoMedicao") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1307ContratoServicosIndicador_InstrumentoMedicao;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Tipo") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1308ContratoServicosIndicador_Tipo;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Periodicidade") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1309ContratoServicosIndicador_Periodicidade;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Vigencia") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1310ContratoServicosIndicador_Vigencia;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_CalculoSob") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1345ContratoServicosIndicador_CalculoSob;
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_Formato") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_QtdeFaixas") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1298ContratoServicosIndicador_QtdeFaixas), 4, 0);
                           }
                           else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicador_ComQtdeFaixas") == 0 )
                           {
                              AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1395ContratoServicosIndicador_ComQtdeFaixas;
                           }
                           AV32GXV2 = (int)(AV32GXV2+1);
                        }
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosIndicadorFaixas") == 0 )
                  {
                     AV20CountKeyAttributes = 0;
                     AV33GXV3 = 1;
                     while ( AV33GXV3 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV33GXV3));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Codigo") == 0 )
                        {
                           AV22KeyContratoServicosIndicadorFaixa_Codigo = AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue;
                           AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                           if ( AV20CountKeyAttributes == 1 )
                           {
                              if (true) break;
                           }
                        }
                        AV33GXV3 = (int)(AV33GXV3+1);
                     }
                     AV34GXLvl514 = 0;
                     /* Using cursor P00WF9 */
                     pr_default.execute(4, new Object[] {A1269ContratoServicosIndicador_Codigo});
                     while ( (pr_default.getStatus(4) != 101) )
                     {
                        A1299ContratoServicosIndicadorFaixa_Codigo = P00WF9_A1299ContratoServicosIndicadorFaixa_Codigo[0];
                        A1300ContratoServicosIndicadorFaixa_Numero = P00WF9_A1300ContratoServicosIndicadorFaixa_Numero[0];
                        A1301ContratoServicosIndicadorFaixa_Desde = P00WF9_A1301ContratoServicosIndicadorFaixa_Desde[0];
                        A1302ContratoServicosIndicadorFaixa_Ate = P00WF9_A1302ContratoServicosIndicadorFaixa_Ate[0];
                        A1303ContratoServicosIndicadorFaixa_Reduz = P00WF9_A1303ContratoServicosIndicadorFaixa_Reduz[0];
                        A1304ContratoServicosIndicadorFaixa_Und = P00WF9_A1304ContratoServicosIndicadorFaixa_Und[0];
                        if ( StringUtil.StrCmp(StringUtil.Str( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0), AV22KeyContratoServicosIndicadorFaixa_Codigo) == 0 )
                        {
                           AV34GXLvl514 = 1;
                           AV11AuditingObjectRecordItem.gxTpr_Mode = "UPD";
                           AV21CountUpdatedContratoServicosIndicadorFaixa_Codigo = (short)(AV21CountUpdatedContratoServicosIndicadorFaixa_Codigo+1);
                           AV35GXV4 = 1;
                           while ( AV35GXV4 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV35GXV4));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Codigo") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Numero") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Desde") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Ate") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Reduz") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1303ContratoServicosIndicadorFaixa_Reduz, 6, 2);
                              }
                              else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Und") == 0 )
                              {
                                 AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0);
                              }
                              AV35GXV4 = (int)(AV35GXV4+1);
                           }
                        }
                        pr_default.readNext(4);
                     }
                     pr_default.close(4);
                     if ( AV34GXLvl514 == 0 )
                     {
                        AV11AuditingObjectRecordItem.gxTpr_Mode = "DLT";
                     }
                  }
                  AV30GXV1 = (int)(AV30GXV1+1);
               }
               if ( AV21CountUpdatedContratoServicosIndicadorFaixa_Codigo < A40000ContratoServicosIndicador_QtdeFaixas )
               {
                  AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
                  /* Using cursor P00WF10 */
                  pr_default.execute(5, new Object[] {AV16ContratoServicosIndicador_Codigo});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A1269ContratoServicosIndicador_Codigo = P00WF10_A1269ContratoServicosIndicador_Codigo[0];
                     A1299ContratoServicosIndicadorFaixa_Codigo = P00WF10_A1299ContratoServicosIndicadorFaixa_Codigo[0];
                     A1300ContratoServicosIndicadorFaixa_Numero = P00WF10_A1300ContratoServicosIndicadorFaixa_Numero[0];
                     A1301ContratoServicosIndicadorFaixa_Desde = P00WF10_A1301ContratoServicosIndicadorFaixa_Desde[0];
                     A1302ContratoServicosIndicadorFaixa_Ate = P00WF10_A1302ContratoServicosIndicadorFaixa_Ate[0];
                     A1303ContratoServicosIndicadorFaixa_Reduz = P00WF10_A1303ContratoServicosIndicadorFaixa_Reduz[0];
                     A1304ContratoServicosIndicadorFaixa_Und = P00WF10_A1304ContratoServicosIndicadorFaixa_Und[0];
                     AV22KeyContratoServicosIndicadorFaixa_Codigo = StringUtil.Str( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0);
                     AV23RecordExistsContratoServicosIndicadorFaixa_Codigo = false;
                     AV37GXV5 = 1;
                     while ( AV37GXV5 <= AV10AuditingObject.gxTpr_Record.Count )
                     {
                        AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV37GXV5));
                        if ( StringUtil.StrCmp(AV11AuditingObjectRecordItem.gxTpr_Tablename, "ContratoServicosIndicadorFaixas") == 0 )
                        {
                           AV20CountKeyAttributes = 0;
                           AV38GXV6 = 1;
                           while ( AV38GXV6 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                           {
                              AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV38GXV6));
                              if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ContratoServicosIndicadorFaixa_Codigo") == 0 )
                              {
                                 if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue, AV22KeyContratoServicosIndicadorFaixa_Codigo) == 0 )
                                 {
                                    AV23RecordExistsContratoServicosIndicadorFaixa_Codigo = true;
                                    AV20CountKeyAttributes = (short)(AV20CountKeyAttributes+1);
                                    if ( AV20CountKeyAttributes == 1 )
                                    {
                                       if (true) break;
                                    }
                                 }
                              }
                              AV38GXV6 = (int)(AV38GXV6+1);
                           }
                        }
                        AV37GXV5 = (int)(AV37GXV5+1);
                     }
                     if ( ! ( AV23RecordExistsContratoServicosIndicadorFaixa_Codigo ) )
                     {
                        AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
                        AV18AuditingObjectRecordItemNew.gxTpr_Tablename = "ContratoServicosIndicadorFaixas";
                        AV18AuditingObjectRecordItemNew.gxTpr_Mode = "INS";
                        AV17AuditingObjectNewRecords.gxTpr_Record.Add(AV18AuditingObjectRecordItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosIndicadorFaixa_Codigo";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosIndicadorFaixa_Numero";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = true;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosIndicadorFaixa_Desde";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosIndicadorFaixa_Ate";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosIndicadorFaixa_Reduz";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( A1303ContratoServicosIndicadorFaixa_Reduz, 6, 2);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                        AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Name = "ContratoServicosIndicadorFaixa_Und";
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Ispartofkey = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Isdescriptionattribute = false;
                        AV19AuditingObjectRecordItemAttributeItemNew.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0);
                        AV18AuditingObjectRecordItemNew.gxTpr_Attribute.Add(AV19AuditingObjectRecordItemAttributeItemNew, 0);
                     }
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
                  AV39GXV7 = 1;
                  while ( AV39GXV7 <= AV17AuditingObjectNewRecords.gxTpr_Record.Count )
                  {
                     AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV17AuditingObjectNewRecords.gxTpr_Record.Item(AV39GXV7));
                     AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
                     AV39GXV7 = (int)(AV39GXV7+1);
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WF3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00WF3_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00WF3_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         P00WF3_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         P00WF3_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         P00WF3_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         P00WF3_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00WF3_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         P00WF3_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         P00WF3_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00WF3_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00WF3_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00WF3_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00WF3_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00WF3_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00WF3_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00WF3_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00WF3_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00WF3_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00WF3_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00WF3_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00WF3_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         P00WF3_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         P00WF3_A2052ContratoServicosIndicador_Formato = new short[1] ;
         P00WF3_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00WF3_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         A2051ContratoServicosIndicador_Sigla = "";
         A1305ContratoServicosIndicador_Finalidade = "";
         A1306ContratoServicosIndicador_Meta = "";
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A1309ContratoServicosIndicador_Periodicidade = "";
         A1310ContratoServicosIndicador_Vigencia = "";
         A1345ContratoServicosIndicador_CalculoSob = "";
         A1274ContratoServicosIndicador_Indicador = "";
         A1395ContratoServicosIndicador_ComQtdeFaixas = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00WF4_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00WF4_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         P00WF4_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P00WF4_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P00WF4_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P00WF4_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P00WF4_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         P00WF7_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P00WF7_A1296ContratoServicosIndicador_ContratoCod = new int[1] ;
         P00WF7_n1296ContratoServicosIndicador_ContratoCod = new bool[] {false} ;
         P00WF7_A1295ContratoServicosIndicador_AreaTrabalhoCod = new int[1] ;
         P00WF7_n1295ContratoServicosIndicador_AreaTrabalhoCod = new bool[] {false} ;
         P00WF7_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P00WF7_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         P00WF7_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         P00WF7_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P00WF7_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P00WF7_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P00WF7_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P00WF7_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P00WF7_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P00WF7_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P00WF7_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P00WF7_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P00WF7_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P00WF7_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P00WF7_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P00WF7_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         P00WF7_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         P00WF7_A2052ContratoServicosIndicador_Formato = new short[1] ;
         P00WF7_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00WF7_A40000ContratoServicosIndicador_QtdeFaixas = new int[1] ;
         P00WF7_n40000ContratoServicosIndicador_QtdeFaixas = new bool[] {false} ;
         P00WF7_A1298ContratoServicosIndicador_QtdeFaixas = new short[1] ;
         P00WF7_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P00WF8_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00WF8_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         P00WF8_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P00WF8_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P00WF8_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P00WF8_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P00WF8_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         AV22KeyContratoServicosIndicadorFaixa_Codigo = "";
         P00WF9_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00WF9_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         P00WF9_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P00WF9_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P00WF9_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P00WF9_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P00WF9_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         AV17AuditingObjectNewRecords = new wwpbaseobjects.SdtAuditingObject(context);
         P00WF10_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P00WF10_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         P00WF10_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         P00WF10_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P00WF10_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P00WF10_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P00WF10_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         AV18AuditingObjectRecordItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV19AuditingObjectRecordItemAttributeItemNew = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditcontratoservicosindicador__default(),
            new Object[][] {
                new Object[] {
               P00WF3_A1269ContratoServicosIndicador_Codigo, P00WF3_A1270ContratoServicosIndicador_CntSrvCod, P00WF3_A1296ContratoServicosIndicador_ContratoCod, P00WF3_n1296ContratoServicosIndicador_ContratoCod, P00WF3_A1295ContratoServicosIndicador_AreaTrabalhoCod, P00WF3_n1295ContratoServicosIndicador_AreaTrabalhoCod, P00WF3_A1271ContratoServicosIndicador_Numero, P00WF3_A2051ContratoServicosIndicador_Sigla, P00WF3_n2051ContratoServicosIndicador_Sigla, P00WF3_A1305ContratoServicosIndicador_Finalidade,
               P00WF3_n1305ContratoServicosIndicador_Finalidade, P00WF3_A1306ContratoServicosIndicador_Meta, P00WF3_n1306ContratoServicosIndicador_Meta, P00WF3_A1307ContratoServicosIndicador_InstrumentoMedicao, P00WF3_n1307ContratoServicosIndicador_InstrumentoMedicao, P00WF3_A1308ContratoServicosIndicador_Tipo, P00WF3_n1308ContratoServicosIndicador_Tipo, P00WF3_A1309ContratoServicosIndicador_Periodicidade, P00WF3_n1309ContratoServicosIndicador_Periodicidade, P00WF3_A1310ContratoServicosIndicador_Vigencia,
               P00WF3_n1310ContratoServicosIndicador_Vigencia, P00WF3_A1345ContratoServicosIndicador_CalculoSob, P00WF3_n1345ContratoServicosIndicador_CalculoSob, P00WF3_A2052ContratoServicosIndicador_Formato, P00WF3_A1298ContratoServicosIndicador_QtdeFaixas, P00WF3_A1274ContratoServicosIndicador_Indicador
               }
               , new Object[] {
               P00WF4_A1269ContratoServicosIndicador_Codigo, P00WF4_A1299ContratoServicosIndicadorFaixa_Codigo, P00WF4_A1300ContratoServicosIndicadorFaixa_Numero, P00WF4_A1301ContratoServicosIndicadorFaixa_Desde, P00WF4_A1302ContratoServicosIndicadorFaixa_Ate, P00WF4_A1303ContratoServicosIndicadorFaixa_Reduz, P00WF4_A1304ContratoServicosIndicadorFaixa_Und
               }
               , new Object[] {
               P00WF7_A1270ContratoServicosIndicador_CntSrvCod, P00WF7_A1296ContratoServicosIndicador_ContratoCod, P00WF7_n1296ContratoServicosIndicador_ContratoCod, P00WF7_A1295ContratoServicosIndicador_AreaTrabalhoCod, P00WF7_n1295ContratoServicosIndicador_AreaTrabalhoCod, P00WF7_A1271ContratoServicosIndicador_Numero, P00WF7_A2051ContratoServicosIndicador_Sigla, P00WF7_n2051ContratoServicosIndicador_Sigla, P00WF7_A1305ContratoServicosIndicador_Finalidade, P00WF7_n1305ContratoServicosIndicador_Finalidade,
               P00WF7_A1306ContratoServicosIndicador_Meta, P00WF7_n1306ContratoServicosIndicador_Meta, P00WF7_A1307ContratoServicosIndicador_InstrumentoMedicao, P00WF7_n1307ContratoServicosIndicador_InstrumentoMedicao, P00WF7_A1308ContratoServicosIndicador_Tipo, P00WF7_n1308ContratoServicosIndicador_Tipo, P00WF7_A1309ContratoServicosIndicador_Periodicidade, P00WF7_n1309ContratoServicosIndicador_Periodicidade, P00WF7_A1310ContratoServicosIndicador_Vigencia, P00WF7_n1310ContratoServicosIndicador_Vigencia,
               P00WF7_A1345ContratoServicosIndicador_CalculoSob, P00WF7_n1345ContratoServicosIndicador_CalculoSob, P00WF7_A2052ContratoServicosIndicador_Formato, P00WF7_A1269ContratoServicosIndicador_Codigo, P00WF7_A40000ContratoServicosIndicador_QtdeFaixas, P00WF7_n40000ContratoServicosIndicador_QtdeFaixas, P00WF7_A1298ContratoServicosIndicador_QtdeFaixas, P00WF7_A1274ContratoServicosIndicador_Indicador
               }
               , new Object[] {
               P00WF8_A1269ContratoServicosIndicador_Codigo, P00WF8_A1299ContratoServicosIndicadorFaixa_Codigo, P00WF8_A1300ContratoServicosIndicadorFaixa_Numero, P00WF8_A1301ContratoServicosIndicadorFaixa_Desde, P00WF8_A1302ContratoServicosIndicadorFaixa_Ate, P00WF8_A1303ContratoServicosIndicadorFaixa_Reduz, P00WF8_A1304ContratoServicosIndicadorFaixa_Und
               }
               , new Object[] {
               P00WF9_A1269ContratoServicosIndicador_Codigo, P00WF9_A1299ContratoServicosIndicadorFaixa_Codigo, P00WF9_A1300ContratoServicosIndicadorFaixa_Numero, P00WF9_A1301ContratoServicosIndicadorFaixa_Desde, P00WF9_A1302ContratoServicosIndicadorFaixa_Ate, P00WF9_A1303ContratoServicosIndicadorFaixa_Reduz, P00WF9_A1304ContratoServicosIndicadorFaixa_Und
               }
               , new Object[] {
               P00WF10_A1269ContratoServicosIndicador_Codigo, P00WF10_A1299ContratoServicosIndicadorFaixa_Codigo, P00WF10_A1300ContratoServicosIndicadorFaixa_Numero, P00WF10_A1301ContratoServicosIndicadorFaixa_Desde, P00WF10_A1302ContratoServicosIndicadorFaixa_Ate, P00WF10_A1303ContratoServicosIndicadorFaixa_Reduz, P00WF10_A1304ContratoServicosIndicadorFaixa_Und
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1271ContratoServicosIndicador_Numero ;
      private short A2052ContratoServicosIndicador_Formato ;
      private short A1298ContratoServicosIndicador_QtdeFaixas ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private short A1304ContratoServicosIndicadorFaixa_Und ;
      private short AV21CountUpdatedContratoServicosIndicadorFaixa_Codigo ;
      private short AV20CountKeyAttributes ;
      private short AV34GXLvl514 ;
      private int AV16ContratoServicosIndicador_Codigo ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int A1296ContratoServicosIndicador_ContratoCod ;
      private int A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int A40000ContratoServicosIndicador_QtdeFaixas ;
      private int AV30GXV1 ;
      private int AV32GXV2 ;
      private int AV33GXV3 ;
      private int AV35GXV4 ;
      private int AV37GXV5 ;
      private int AV38GXV6 ;
      private int AV39GXV7 ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A2051ContratoServicosIndicador_Sigla ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String A1309ContratoServicosIndicador_Periodicidade ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private bool returnInSub ;
      private bool n1296ContratoServicosIndicador_ContratoCod ;
      private bool n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool n2051ContratoServicosIndicador_Sigla ;
      private bool n1305ContratoServicosIndicador_Finalidade ;
      private bool n1306ContratoServicosIndicador_Meta ;
      private bool n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n1309ContratoServicosIndicador_Periodicidade ;
      private bool n1310ContratoServicosIndicador_Vigencia ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool n40000ContratoServicosIndicador_QtdeFaixas ;
      private bool AV23RecordExistsContratoServicosIndicadorFaixa_Codigo ;
      private String A1305ContratoServicosIndicador_Finalidade ;
      private String A1306ContratoServicosIndicador_Meta ;
      private String A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A1310ContratoServicosIndicador_Vigencia ;
      private String A1395ContratoServicosIndicador_ComQtdeFaixas ;
      private String AV22KeyContratoServicosIndicadorFaixa_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00WF3_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00WF3_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P00WF3_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] P00WF3_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] P00WF3_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] P00WF3_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private short[] P00WF3_A1271ContratoServicosIndicador_Numero ;
      private String[] P00WF3_A2051ContratoServicosIndicador_Sigla ;
      private bool[] P00WF3_n2051ContratoServicosIndicador_Sigla ;
      private String[] P00WF3_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00WF3_n1305ContratoServicosIndicador_Finalidade ;
      private String[] P00WF3_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00WF3_n1306ContratoServicosIndicador_Meta ;
      private String[] P00WF3_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00WF3_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00WF3_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00WF3_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00WF3_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00WF3_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00WF3_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00WF3_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00WF3_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] P00WF3_n1345ContratoServicosIndicador_CalculoSob ;
      private short[] P00WF3_A2052ContratoServicosIndicador_Formato ;
      private short[] P00WF3_A1298ContratoServicosIndicador_QtdeFaixas ;
      private String[] P00WF3_A1274ContratoServicosIndicador_Indicador ;
      private int[] P00WF4_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00WF4_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] P00WF4_A1300ContratoServicosIndicadorFaixa_Numero ;
      private decimal[] P00WF4_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P00WF4_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P00WF4_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private short[] P00WF4_A1304ContratoServicosIndicadorFaixa_Und ;
      private int[] P00WF7_A1270ContratoServicosIndicador_CntSrvCod ;
      private int[] P00WF7_A1296ContratoServicosIndicador_ContratoCod ;
      private bool[] P00WF7_n1296ContratoServicosIndicador_ContratoCod ;
      private int[] P00WF7_A1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private bool[] P00WF7_n1295ContratoServicosIndicador_AreaTrabalhoCod ;
      private short[] P00WF7_A1271ContratoServicosIndicador_Numero ;
      private String[] P00WF7_A2051ContratoServicosIndicador_Sigla ;
      private bool[] P00WF7_n2051ContratoServicosIndicador_Sigla ;
      private String[] P00WF7_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P00WF7_n1305ContratoServicosIndicador_Finalidade ;
      private String[] P00WF7_A1306ContratoServicosIndicador_Meta ;
      private bool[] P00WF7_n1306ContratoServicosIndicador_Meta ;
      private String[] P00WF7_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P00WF7_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P00WF7_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P00WF7_n1308ContratoServicosIndicador_Tipo ;
      private String[] P00WF7_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P00WF7_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P00WF7_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P00WF7_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P00WF7_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] P00WF7_n1345ContratoServicosIndicador_CalculoSob ;
      private short[] P00WF7_A2052ContratoServicosIndicador_Formato ;
      private int[] P00WF7_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00WF7_A40000ContratoServicosIndicador_QtdeFaixas ;
      private bool[] P00WF7_n40000ContratoServicosIndicador_QtdeFaixas ;
      private short[] P00WF7_A1298ContratoServicosIndicador_QtdeFaixas ;
      private String[] P00WF7_A1274ContratoServicosIndicador_Indicador ;
      private int[] P00WF8_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00WF8_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] P00WF8_A1300ContratoServicosIndicadorFaixa_Numero ;
      private decimal[] P00WF8_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P00WF8_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P00WF8_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private short[] P00WF8_A1304ContratoServicosIndicadorFaixa_Und ;
      private int[] P00WF9_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00WF9_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] P00WF9_A1300ContratoServicosIndicadorFaixa_Numero ;
      private decimal[] P00WF9_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P00WF9_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P00WF9_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private short[] P00WF9_A1304ContratoServicosIndicadorFaixa_Und ;
      private int[] P00WF10_A1269ContratoServicosIndicador_Codigo ;
      private int[] P00WF10_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private short[] P00WF10_A1300ContratoServicosIndicadorFaixa_Numero ;
      private decimal[] P00WF10_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P00WF10_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P00WF10_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private short[] P00WF10_A1304ContratoServicosIndicadorFaixa_Und ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject AV17AuditingObjectNewRecords ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV18AuditingObjectRecordItemNew ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV19AuditingObjectRecordItemAttributeItemNew ;
   }

   public class loadauditcontratoservicosindicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WF3 ;
          prmP00WF3 = new Object[] {
          new Object[] {"@AV16ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WF4 ;
          prmP00WF4 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WF7 ;
          prmP00WF7 = new Object[] {
          new Object[] {"@AV16ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WF8 ;
          prmP00WF8 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WF9 ;
          prmP00WF9 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WF10 ;
          prmP00WF10 = new Object[] {
          new Object[] {"@AV16ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WF3", "SELECT T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T3.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T4.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T1.[ContratoServicosIndicador_Numero], T1.[ContratoServicosIndicador_Sigla], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_CalculoSob], T1.[ContratoServicosIndicador_Formato], COALESCE( T2.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas, T1.[ContratoServicosIndicador_Indicador] FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) LEFT JOIN (SELECT [ContratoServicosIndicador_Codigo], COUNT(*) AS ContratoServicosIndicador_QtdeFaixas FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T2 ON T2.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo]) INNER JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo]) WHERE T1.[ContratoServicosIndicador_Codigo] = @AV16ContratoServicosIndicador_Codigo ORDER BY T1.[ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WF3,1,0,true,true )
             ,new CursorDef("P00WF4", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WF4,100,0,false,false )
             ,new CursorDef("P00WF7", "SELECT T1.[ContratoServicosIndicador_CntSrvCod] AS ContratoServicosIndicador_CntSrvCod, T2.[Contrato_Codigo] AS ContratoServicosIndicador_ContratoCod, T3.[Contrato_AreaTrabalhoCod] AS ContratoServicosIndicador_AreaTrabalhoCod, T1.[ContratoServicosIndicador_Numero], T1.[ContratoServicosIndicador_Sigla], T1.[ContratoServicosIndicador_Finalidade], T1.[ContratoServicosIndicador_Meta], T1.[ContratoServicosIndicador_InstrumentoMedicao], T1.[ContratoServicosIndicador_Tipo], T1.[ContratoServicosIndicador_Periodicidade], T1.[ContratoServicosIndicador_Vigencia], T1.[ContratoServicosIndicador_CalculoSob], T1.[ContratoServicosIndicador_Formato], T1.[ContratoServicosIndicador_Codigo], COALESCE( T5.[ContratoServicosIndicador_QtdeFaixas], 0) AS GXC1, COALESCE( T4.[ContratoServicosIndicador_QtdeFaixas], 0) AS ContratoServicosIndicador_QtdeFaixas, T1.[ContratoServicosIndicador_Indicador] FROM ((([ContratoServicosIndicador] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosIndicador_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN (SELECT [ContratoServicosIndicador_Codigo], COUNT(*) AS ContratoServicosIndicador_QtdeFaixas FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_Codigo] ) T4 ON T4.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo]),  (SELECT COUNT(*) AS ContratoServicosIndicador_QtdeFaixas FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @AV16ContratoServicosIndicador_Codigo ) T5 WHERE T1.[ContratoServicosIndicador_Codigo] = @AV16ContratoServicosIndicador_Codigo ORDER BY T1.[ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WF7,1,0,true,true )
             ,new CursorDef("P00WF8", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WF8,100,0,false,false )
             ,new CursorDef("P00WF9", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WF9,100,0,false,false )
             ,new CursorDef("P00WF10", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @AV16ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WF10,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 2) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((String[]) buf[25])[0] = rslt.getLongVarchar(16) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 2) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((short[]) buf[26])[0] = rslt.getShort(16) ;
                ((String[]) buf[27])[0] = rslt.getLongVarchar(17) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
