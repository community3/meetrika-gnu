/*
               File: PRC_ValidaDocumento
        Description: Valida Documento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:42.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_validadocumento : GXProcedure
   {
      public prc_validadocumento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_validadocumento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_NroDocumento ,
                           String aP1_TipoPessoa ,
                           out bool aP2_RetErro )
      {
         this.AV23NroDocumento = aP0_NroDocumento;
         this.AV27TipoPessoa = aP1_TipoPessoa;
         this.AV25RetErro = false ;
         initialize();
         executePrivate();
         aP2_RetErro=this.AV25RetErro;
      }

      public bool executeUdp( String aP0_NroDocumento ,
                              String aP1_TipoPessoa )
      {
         this.AV23NroDocumento = aP0_NroDocumento;
         this.AV27TipoPessoa = aP1_TipoPessoa;
         this.AV25RetErro = false ;
         initialize();
         executePrivate();
         aP2_RetErro=this.AV25RetErro;
         return AV25RetErro ;
      }

      public void executeSubmit( String aP0_NroDocumento ,
                                 String aP1_TipoPessoa ,
                                 out bool aP2_RetErro )
      {
         prc_validadocumento objprc_validadocumento;
         objprc_validadocumento = new prc_validadocumento();
         objprc_validadocumento.AV23NroDocumento = aP0_NroDocumento;
         objprc_validadocumento.AV27TipoPessoa = aP1_TipoPessoa;
         objprc_validadocumento.AV25RetErro = false ;
         objprc_validadocumento.context.SetSubmitInitialConfig(context);
         objprc_validadocumento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_validadocumento);
         aP2_RetErro=this.AV25RetErro;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_validadocumento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25RetErro = false;
         if ( StringUtil.StrCmp(AV27TipoPessoa, "J") == 0 )
         {
            AV8An = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 1, 1), "."));
            AV9Bn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 2, 1), "."));
            AV10Cn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 3, 1), "."));
            AV11Dn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 4, 1), "."));
            AV12En = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 5, 1), "."));
            AV13Fn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 6, 1), "."));
            AV14Gn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 7, 1), "."));
            AV15Hn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 8, 1), "."));
            AV16In = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 9, 1), "."));
            AV17Jn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 10, 1), "."));
            AV18Kn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 11, 1), "."));
            AV19Ln = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 12, 1), "."));
            AV20Mn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 13, 1), "."));
            AV22Nn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 14, 1), "."));
            AV26S = (decimal)((AV8An*5)+(AV9Bn*4)+(AV10Cn*3)+(AV11Dn*2)+(AV12En*9));
            AV26S = (decimal)(AV26S+(AV13Fn*8)+(AV14Gn*7)+(AV15Hn*6)+(AV16In*5)+(AV17Jn*4));
            AV26S = (decimal)(AV26S+(AV18Kn*3)+(AV19Ln*2));
            AV24R = (int)(NumberUtil.Int( (long)(AV26S/ (decimal)(11))));
            AV26S = (decimal)(AV26S-(AV24R*11));
            if ( ( AV26S == Convert.ToDecimal( 1 )) || ( AV26S == Convert.ToDecimal( 0 )) )
            {
               AV21N = 0;
            }
            else
            {
               AV21N = (short)(11-AV26S);
            }
            if ( AV21N != AV20Mn )
            {
               AV25RetErro = true;
            }
            else
            {
               AV26S = (decimal)((AV8An*6)+(AV9Bn*5)+(AV10Cn*4)+(AV11Dn*3)+(AV12En*2));
               AV26S = (decimal)(AV26S+(AV13Fn*9)+(AV14Gn*8)+(AV15Hn*7)+(AV16In*6)+(AV17Jn*5));
               AV26S = (decimal)(AV26S+(AV18Kn*4)+(AV19Ln*3)+(AV20Mn*2));
               AV24R = (int)(NumberUtil.Int( (long)(AV26S/ (decimal)(11))));
               AV26S = (decimal)(AV26S-(AV24R*11));
               if ( ( AV26S == Convert.ToDecimal( 1 )) || ( AV26S == Convert.ToDecimal( 0 )) )
               {
                  AV21N = 0;
               }
               else
               {
                  AV21N = (short)(11-AV26S);
               }
               if ( AV21N != AV22Nn )
               {
                  AV25RetErro = true;
               }
            }
            if ( StringUtil.Len( AV23NroDocumento) != 14 )
            {
               AV25RetErro = true;
            }
         }
         else if ( StringUtil.StrCmp(AV27TipoPessoa, "F") == 0 )
         {
            AV11Dn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 1, 1), "."));
            AV12En = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 2, 1), "."));
            AV13Fn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 3, 1), "."));
            AV14Gn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 4, 1), "."));
            AV15Hn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 5, 1), "."));
            AV16In = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 6, 1), "."));
            AV17Jn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 7, 1), "."));
            AV18Kn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 8, 1), "."));
            AV19Ln = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 9, 1), "."));
            AV20Mn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 10, 1), "."));
            AV22Nn = (short)(NumberUtil.Val( StringUtil.Substring( AV23NroDocumento, 11, 1), "."));
            AV26S = (decimal)((AV11Dn*10)+(AV12En*9)+(AV13Fn*8)+(AV14Gn*7)+(AV15Hn*6));
            AV26S = (decimal)(AV26S+(AV16In*5)+(AV17Jn*4)+(AV18Kn*3)+(AV19Ln*2));
            AV24R = (int)(NumberUtil.Int( (long)(AV26S/ (decimal)(11))));
            AV26S = (decimal)(AV26S-(AV24R*11));
            if ( ( AV26S == Convert.ToDecimal( 1 )) || ( AV26S == Convert.ToDecimal( 0 )) )
            {
               AV21N = 0;
            }
            else
            {
               AV21N = (short)(11-AV26S);
            }
            if ( AV21N != AV20Mn )
            {
               AV25RetErro = true;
            }
            else
            {
               AV26S = (decimal)((AV11Dn*11)+(AV12En*10)+(AV13Fn*9)+(AV14Gn*8)+(AV15Hn*7));
               AV26S = (decimal)(AV26S+(AV16In*6)+(AV17Jn*5)+(AV18Kn*4)+(AV19Ln*3)+(AV20Mn*2));
               AV24R = (int)(NumberUtil.Int( (long)(AV26S/ (decimal)(11))));
               AV26S = (decimal)(AV26S-(AV24R*11));
               if ( ( AV26S == Convert.ToDecimal( 1 )) || ( AV26S == Convert.ToDecimal( 0 )) )
               {
                  AV21N = 0;
               }
               else
               {
                  AV21N = (short)(11-AV26S);
               }
               if ( AV21N != AV22Nn )
               {
                  AV25RetErro = true;
               }
            }
            if ( StringUtil.Len( AV23NroDocumento) != 11 )
            {
               AV25RetErro = true;
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8An ;
      private short AV9Bn ;
      private short AV10Cn ;
      private short AV11Dn ;
      private short AV12En ;
      private short AV13Fn ;
      private short AV14Gn ;
      private short AV15Hn ;
      private short AV16In ;
      private short AV17Jn ;
      private short AV18Kn ;
      private short AV19Ln ;
      private short AV20Mn ;
      private short AV22Nn ;
      private short AV21N ;
      private int AV24R ;
      private decimal AV26S ;
      private String AV27TipoPessoa ;
      private bool AV25RetErro ;
      private String AV23NroDocumento ;
      private bool aP2_RetErro ;
   }

}
