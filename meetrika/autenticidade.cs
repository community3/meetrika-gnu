/*
               File: autenticidade
        Description: Consulta/Valida��o de Documentos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:55:51.82
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class autenticidade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public autenticidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public autenticidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynLoteArquivoAnexo_LoteCod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridfile") == 0 )
            {
               nRC_GXsfl_17 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_17_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_17_idx = GetNextPar( );
               edtLoteArquivoAnexo_Arquivo_Linktarget = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Linktarget", edtLoteArquivoAnexo_Arquivo_Linktarget);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridfile_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridfile") == 0 )
            {
               AV5LoteArquivoAnexo_Verificador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5LoteArquivoAnexo_Verificador", AV5LoteArquivoAnexo_Verificador);
               edtLoteArquivoAnexo_Arquivo_Linktarget = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Linktarget", edtLoteArquivoAnexo_Arquivo_Linktarget);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridfile_refresh( AV5LoteArquivoAnexo_Verificador) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAH92( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTH92( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118555185");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("https://www.google.com/recaptcha/api.js", "");
         context.AddJavascriptSource("gpxReCAPTCHA/gpxreCAPTCHARender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("autenticidade.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vLOTEARQUIVOANEXO_VERIFICADOR", AV5LoteArquivoAnexo_Verificador);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_17", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_17), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GPXRECAPTCHA1_Sitekey", StringUtil.RTrim( Gpxrecaptcha1_Sitekey));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_ARQUIVO_Linktarget", StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Linktarget));
         GxWebStd.gx_hidden_field( context, "GPXRECAPTCHA1_Response", StringUtil.RTrim( Gpxrecaptcha1_Response));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEH92( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTH92( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("autenticidade.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "autenticidade" ;
      }

      public override String GetPgmdesc( )
      {
         return "Consulta/Valida��o de Documentos" ;
      }

      protected void WBH90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_H92( true) ;
         }
         else
         {
            wb_table1_2_H92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_H92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p></p>") ;
            /*  Grid Control  */
            GridfileContainer.SetWrapped(nGXWrapped);
            if ( GridfileContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridfileContainer"+"DivS\" data-gxgridid=\"17\">") ;
               sStyleString = "";
               if ( subGridfile_Visible == 0 )
               {
                  sStyleString = sStyleString + "display:none;";
               }
               GxWebStd.gx_table_start( context, subGridfile_Internalname, subGridfile_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridfile_Backcolorstyle == 0 )
               {
                  subGridfile_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridfile_Class) > 0 )
                  {
                     subGridfile_Linesclass = subGridfile_Class+"Title";
                  }
               }
               else
               {
                  subGridfile_Titlebackstyle = 1;
                  if ( subGridfile_Backcolorstyle == 1 )
                  {
                     subGridfile_Titlebackcolor = subGridfile_Allbackcolor;
                     if ( StringUtil.Len( subGridfile_Class) > 0 )
                     {
                        subGridfile_Linesclass = subGridfile_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridfile_Class) > 0 )
                     {
                        subGridfile_Linesclass = subGridfile_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfile_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Lote Arquivo Anexo_Lote Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfile_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data/Hora") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfile_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfile_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome do Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfile_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo de Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridfileContainer.AddObjectProperty("GridName", "Gridfile");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridfileContainer = new GXWebGrid( context);
               }
               else
               {
                  GridfileContainer.Clear();
               }
               GridfileContainer.SetWrapped(nGXWrapped);
               GridfileContainer.AddObjectProperty("GridName", "Gridfile");
               GridfileContainer.AddObjectProperty("Class", "WorkWith");
               GridfileContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Backcolorstyle), 1, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Visible), 5, 0, ".", "")));
               GridfileContainer.AddObjectProperty("CmpContext", "");
               GridfileContainer.AddObjectProperty("InMasterPage", "false");
               GridfileColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfileColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
               GridfileContainer.AddColumnProperties(GridfileColumn);
               GridfileColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfileColumn.AddObjectProperty("Value", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
               GridfileContainer.AddColumnProperties(GridfileColumn);
               GridfileColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfileColumn.AddObjectProperty("Value", A838LoteArquivoAnexo_Arquivo);
               GridfileColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Linktarget));
               GridfileContainer.AddColumnProperties(GridfileColumn);
               GridfileColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfileColumn.AddObjectProperty("Value", StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq));
               GridfileContainer.AddColumnProperties(GridfileColumn);
               GridfileColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfileColumn.AddObjectProperty("Value", StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq));
               GridfileContainer.AddColumnProperties(GridfileColumn);
               GridfileContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Allowselection), 1, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Selectioncolor), 9, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Allowhovering), 1, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Hoveringcolor), 9, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Allowcollapsing), 1, 0, ".", "")));
               GridfileContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 17 )
         {
            wbEnd = 0;
            nRC_GXsfl_17 = (short)(nGXsfl_17_idx-1);
            if ( GridfileContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               if ( subGridfile_Visible != 0 )
               {
                  sStyleString = "";
               }
               else
               {
                  sStyleString = " style=\"display:none;\"";
               }
               context.WriteHtmlText( "<div id=\""+"GridfileContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridfile", GridfileContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridfileContainerData", GridfileContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridfileContainerData"+"V", GridfileContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridfileContainerData"+"V"+"\" value='"+GridfileContainer.GridValuesHidden()+"'/>") ;
               }
            }
         }
         wbLoad = true;
      }

      protected void STARTH92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Consulta/Valida��o de Documentos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPH90( ) ;
      }

      protected void WSH92( )
      {
         STARTH92( ) ;
         EVTH92( ) ;
      }

      protected void EVTH92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 /* Set Refresh If Lotearquivoanexo_verificador Changed */
                                 if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_VERIFICADOR"), AV5LoteArquivoAnexo_Verificador) != 0 )
                                 {
                                    Rfr0gs = true;
                                 }
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11H92 */
                                    E11H92 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_17_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
                              SubsflControlProps_172( ) ;
                              dynLoteArquivoAnexo_LoteCod.Name = dynLoteArquivoAnexo_LoteCod_Internalname;
                              dynLoteArquivoAnexo_LoteCod.CurrentValue = cgiGet( dynLoteArquivoAnexo_LoteCod_Internalname);
                              A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( cgiGet( dynLoteArquivoAnexo_LoteCod_Internalname), "."));
                              A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtLoteArquivoAnexo_Data_Internalname), 0);
                              A838LoteArquivoAnexo_Arquivo = cgiGet( edtLoteArquivoAnexo_Arquivo_Internalname);
                              n838LoteArquivoAnexo_Arquivo = false;
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12H92 */
                                    E12H92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13H92 */
                                    E13H92 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEH92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAH92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "LOTEARQUIVOANEXO_LOTECOD_" + sGXsfl_17_idx;
            dynLoteArquivoAnexo_LoteCod.Name = GXCCtl;
            dynLoteArquivoAnexo_LoteCod.WebTags = "";
            dynLoteArquivoAnexo_LoteCod.removeAllItems();
            /* Using cursor H00H92 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynLoteArquivoAnexo_LoteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00H92_A596Lote_Codigo[0]), 6, 0)), H00H92_A563Lote_Nome[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynLoteArquivoAnexo_LoteCod.ItemCount > 0 )
            {
               A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( dynLoteArquivoAnexo_LoteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0))), "."));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavLotearquivoanexo_verificador_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridfile_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_172( ) ;
         while ( nGXsfl_17_idx <= nRC_GXsfl_17 )
         {
            sendrow_172( ) ;
            nGXsfl_17_idx = (short)(((subGridfile_Islastpage==1)&&(nGXsfl_17_idx+1>subGridfile_Recordsperpage( )) ? 1 : nGXsfl_17_idx+1));
            sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
            SubsflControlProps_172( ) ;
         }
         context.GX_webresponse.AddString(GridfileContainer.ToJavascriptSource());
         /* End function gxnrGridfile_newrow */
      }

      protected void gxgrGridfile_refresh( String AV5LoteArquivoAnexo_Verificador )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDFILE_nCurrentRecord = 0;
         RFH92( ) ;
         /* End function gxgrGridfile_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_DATA", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFH92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFH92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridfileContainer.ClearRows();
         }
         wbStart = 17;
         nGXsfl_17_idx = 1;
         sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
         SubsflControlProps_172( ) ;
         nGXsfl_17_Refreshing = 1;
         GridfileContainer.AddObjectProperty("GridName", "Gridfile");
         GridfileContainer.AddObjectProperty("CmpContext", "");
         GridfileContainer.AddObjectProperty("InMasterPage", "false");
         GridfileContainer.AddObjectProperty("Class", "WorkWith");
         GridfileContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridfileContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridfileContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Backcolorstyle), 1, 0, ".", "")));
         GridfileContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfile_Visible), 5, 0, ".", "")));
         GridfileContainer.PageSize = subGridfile_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_172( ) ;
            /* Using cursor H00H93 */
            pr_default.execute(1, new Object[] {AV5LoteArquivoAnexo_Verificador});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1027LoteArquivoAnexo_Verificador = H00H93_A1027LoteArquivoAnexo_Verificador[0];
               n1027LoteArquivoAnexo_Verificador = H00H93_n1027LoteArquivoAnexo_Verificador[0];
               A839LoteArquivoAnexo_NomeArq = H00H93_A839LoteArquivoAnexo_NomeArq[0];
               n839LoteArquivoAnexo_NomeArq = H00H93_n839LoteArquivoAnexo_NomeArq[0];
               edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
               A840LoteArquivoAnexo_TipoArq = H00H93_A840LoteArquivoAnexo_TipoArq[0];
               n840LoteArquivoAnexo_TipoArq = H00H93_n840LoteArquivoAnexo_TipoArq[0];
               edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               A836LoteArquivoAnexo_Data = H00H93_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = H00H93_A841LoteArquivoAnexo_LoteCod[0];
               A838LoteArquivoAnexo_Arquivo = H00H93_A838LoteArquivoAnexo_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
               n838LoteArquivoAnexo_Arquivo = H00H93_n838LoteArquivoAnexo_Arquivo[0];
               /* Execute user event: E13H92 */
               E13H92 ();
               pr_default.readNext(1);
            }
            pr_default.close(1);
            wbEnd = 17;
            WBH90( ) ;
         }
         nGXsfl_17_Refreshing = 0;
      }

      protected int subGridfile_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridfile_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridfile_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridfile_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPH90( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12H92 */
         E12H92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5LoteArquivoAnexo_Verificador = cgiGet( edtavLotearquivoanexo_verificador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5LoteArquivoAnexo_Verificador", AV5LoteArquivoAnexo_Verificador);
            /* Read saved values. */
            nRC_GXsfl_17 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_17"), ",", "."));
            Gpxrecaptcha1_Sitekey = cgiGet( "GPXRECAPTCHA1_Sitekey");
            Gpxrecaptcha1_Response = cgiGet( "GPXRECAPTCHA1_Response");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12H92 */
         E12H92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12H92( )
      {
         /* Start Routine */
         subGridfile_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridfileContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridfile_Visible), 5, 0)));
         edtLoteArquivoAnexo_Arquivo_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Linktarget", edtLoteArquivoAnexo_Arquivo_Linktarget);
      }

      public void GXEnter( )
      {
         /* Execute user event: E11H92 */
         E11H92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11H92( )
      {
         /* Enter Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5LoteArquivoAnexo_Verificador)) )
         {
            new gpxrecaptchavalid(context ).execute(  "6LcSHhcTAAAAAKOr-C4AOwI5Lb082OmFRFU5vF-t",  Gpxrecaptcha1_Response, out  AV8CaptchaMessage, out  AV7CaptchaValid) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gpxrecaptcha1_Internalname, "Response", Gpxrecaptcha1_Response);
            if ( AV7CaptchaValid )
            {
               GX_msglist.addItem(AV8CaptchaMessage);
               GXt_int1 = AV6vrQtd;
               new prc_contararquivoanexo(context ).execute(  AV5LoteArquivoAnexo_Verificador, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5LoteArquivoAnexo_Verificador", AV5LoteArquivoAnexo_Verificador);
               AV6vrQtd = GXt_int1;
               if ( AV6vrQtd > 0 )
               {
                  subGridfile_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridfileContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridfile_Visible), 5, 0)));
               }
               else
               {
                  GX_msglist.addItem("C�digo verificador N�o Encontrado!");
                  subGridfile_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridfileContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGridfile_Visible), 5, 0)));
               }
            }
            else
            {
               GX_msglist.addItem("Por favor clique na op��o \"N�o sou um rob�\"!");
               this.executeUsercontrolMethod("", false, "GPXRECAPTCHA1Container", "Reload", "", new Object[] {});
            }
         }
      }

      private void E13H92( )
      {
         /* Load Routine */
         sendrow_172( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_17_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(17, GridfileRow);
         }
      }

      protected void wb_table1_2_H92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "C�digo Verificador:") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 6,'',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_verificador_Internalname, AV5LoteArquivoAnexo_Verificador, StringUtil.RTrim( context.localUtil.Format( AV5LoteArquivoAnexo_Verificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,6);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_verificador_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_autenticidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GPXRECAPTCHA1Container"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(17), 2, 0)+","+"null"+");", "Procurar", bttButton1_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_autenticidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_H92e( true) ;
         }
         else
         {
            wb_table1_2_H92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAH92( ) ;
         WSH92( ) ;
         WEH92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311855525");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("autenticidade.js", "?2020311855525");
         context.AddJavascriptSource("https://www.google.com/recaptcha/api.js", "");
         context.AddJavascriptSource("gpxReCAPTCHA/gpxreCAPTCHARender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_172( )
      {
         dynLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_17_idx;
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA_"+sGXsfl_17_idx;
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO_"+sGXsfl_17_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_17_idx;
         edtLoteArquivoAnexo_TipoArq_Internalname = "LOTEARQUIVOANEXO_TIPOARQ_"+sGXsfl_17_idx;
      }

      protected void SubsflControlProps_fel_172( )
      {
         dynLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_17_fel_idx;
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA_"+sGXsfl_17_fel_idx;
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO_"+sGXsfl_17_fel_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_17_fel_idx;
         edtLoteArquivoAnexo_TipoArq_Internalname = "LOTEARQUIVOANEXO_TIPOARQ_"+sGXsfl_17_fel_idx;
      }

      protected void sendrow_172( )
      {
         SubsflControlProps_172( ) ;
         WBH90( ) ;
         GridfileRow = GXWebRow.GetNew(context,GridfileContainer);
         if ( subGridfile_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridfile_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridfile_Class, "") != 0 )
            {
               subGridfile_Linesclass = subGridfile_Class+"Odd";
            }
         }
         else if ( subGridfile_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridfile_Backstyle = 0;
            subGridfile_Backcolor = subGridfile_Allbackcolor;
            if ( StringUtil.StrCmp(subGridfile_Class, "") != 0 )
            {
               subGridfile_Linesclass = subGridfile_Class+"Uniform";
            }
         }
         else if ( subGridfile_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridfile_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridfile_Class, "") != 0 )
            {
               subGridfile_Linesclass = subGridfile_Class+"Odd";
            }
            subGridfile_Backcolor = (int)(0x0);
         }
         else if ( subGridfile_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridfile_Backstyle = 1;
            if ( ((int)((nGXsfl_17_idx) % (2))) == 0 )
            {
               subGridfile_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridfile_Class, "") != 0 )
               {
                  subGridfile_Linesclass = subGridfile_Class+"Even";
               }
            }
            else
            {
               subGridfile_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridfile_Class, "") != 0 )
               {
                  subGridfile_Linesclass = subGridfile_Class+"Odd";
               }
            }
         }
         if ( GridfileContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGridfile_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_17_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridfileContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         if ( ( nGXsfl_17_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "LOTEARQUIVOANEXO_LOTECOD_" + sGXsfl_17_idx;
            dynLoteArquivoAnexo_LoteCod.Name = GXCCtl;
            dynLoteArquivoAnexo_LoteCod.WebTags = "";
            dynLoteArquivoAnexo_LoteCod.removeAllItems();
            /* Using cursor H00H94 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               dynLoteArquivoAnexo_LoteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00H94_A596Lote_Codigo[0]), 6, 0)), H00H94_A563Lote_Nome[0], 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( dynLoteArquivoAnexo_LoteCod.ItemCount > 0 )
            {
               A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( dynLoteArquivoAnexo_LoteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0))), "."));
            }
         }
         /* ComboBox */
         GridfileRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynLoteArquivoAnexo_LoteCod,(String)dynLoteArquivoAnexo_LoteCod_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)),(short)1,(String)dynLoteArquivoAnexo_LoteCod_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)0,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         dynLoteArquivoAnexo_LoteCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynLoteArquivoAnexo_LoteCod_Internalname, "Values", (String)(dynLoteArquivoAnexo_LoteCod.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridfileContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridfileRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Data_Internalname,context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_Data_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridfileContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         ClassString = "Image";
         StyleString = "";
         edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
         edtLoteArquivoAnexo_Arquivo_Filetype = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
         edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) )
         {
            gxblobfileaux.Source = A838LoteArquivoAnexo_Arquivo;
            if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtLoteArquivoAnexo_Arquivo_Filetype, "tmp") != 0 ) )
            {
               gxblobfileaux.SetExtension(StringUtil.Trim( edtLoteArquivoAnexo_Arquivo_Filetype));
            }
            if ( gxblobfileaux.ErrCode == 0 )
            {
               A838LoteArquivoAnexo_Arquivo = gxblobfileaux.GetAbsoluteName();
               n838LoteArquivoAnexo_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
               edtLoteArquivoAnexo_Arquivo_Filetype = gxblobfileaux.GetExtension();
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            }
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
         }
         GridfileRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Arquivo_Internalname,StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo),context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filetype)) ? A838LoteArquivoAnexo_Arquivo : edtLoteArquivoAnexo_Arquivo_Filetype)) : edtLoteArquivoAnexo_Arquivo_Contenttype),(bool)true,(String)edtLoteArquivoAnexo_Arquivo_Linktarget,(String)edtLoteArquivoAnexo_Arquivo_Parameters,(short)1,(short)0,(short)-1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtLoteArquivoAnexo_Arquivo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",(String)""+"",(String)"",(String)""});
         /* Subfile cell */
         if ( GridfileContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridfileRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_NomeArq_Internalname,StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_NomeArq_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridfileContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridfileRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_TipoArq_Internalname,StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_TipoArq_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_LOTECOD"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sGXsfl_17_idx, context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DATA"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sGXsfl_17_idx, context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GridfileContainer.AddRow(GridfileRow);
         nGXsfl_17_idx = (short)(((subGridfile_Islastpage==1)&&(nGXsfl_17_idx+1>subGridfile_Recordsperpage( )) ? 1 : nGXsfl_17_idx+1));
         sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
         SubsflControlProps_172( ) ;
         /* End function sendrow_172 */
      }

      protected void init_default_properties( )
      {
         edtavLotearquivoanexo_verificador_Internalname = "vLOTEARQUIVOANEXO_VERIFICADOR";
         Gpxrecaptcha1_Internalname = "GPXRECAPTCHA1";
         bttButton1_Internalname = "BUTTON1";
         tblTable1_Internalname = "TABLE1";
         dynLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD";
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA";
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO";
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ";
         edtLoteArquivoAnexo_TipoArq_Internalname = "LOTEARQUIVOANEXO_TIPOARQ";
         Form.Internalname = "FORM";
         subGridfile_Internalname = "GRIDFILE";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtLoteArquivoAnexo_TipoArq_Jsonclick = "";
         edtLoteArquivoAnexo_NomeArq_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Parameters = "";
         edtLoteArquivoAnexo_Arquivo_Contenttype = "";
         edtLoteArquivoAnexo_Data_Jsonclick = "";
         dynLoteArquivoAnexo_LoteCod_Jsonclick = "";
         edtavLotearquivoanexo_verificador_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Filetype = "";
         subGridfile_Allowcollapsing = 0;
         subGridfile_Allowselection = 0;
         subGridfile_Class = "WorkWith";
         subGridfile_Backcolorstyle = 0;
         subGridfile_Visible = 1;
         Gpxrecaptcha1_Response = "";
         Gpxrecaptcha1_Sitekey = "6LcSHhcTAAAAAF21luzjQpn_7wNmYcqt0tuw5HnW";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Consulta/Valida��o de Documentos";
         edtLoteArquivoAnexo_Arquivo_Linktarget = "";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDFILE_nFirstRecordOnPage',nv:0},{av:'GRIDFILE_nEOF',nv:0},{av:'AV5LoteArquivoAnexo_Verificador',fld:'vLOTEARQUIVOANEXO_VERIFICADOR',pic:'',nv:''},{av:'edtLoteArquivoAnexo_Arquivo_Linktarget',ctrl:'LOTEARQUIVOANEXO_ARQUIVO',prop:'Linktarget'}],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E11H92',iparms:[{av:'AV5LoteArquivoAnexo_Verificador',fld:'vLOTEARQUIVOANEXO_VERIFICADOR',pic:'',nv:''},{av:'Gpxrecaptcha1_Response',ctrl:'GPXRECAPTCHA1',prop:'Response'}],oparms:[{av:'subGridfile_Visible',ctrl:'GRIDFILE',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV5LoteArquivoAnexo_Verificador = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         GridfileContainer = new GXWebGrid( context);
         sStyleString = "";
         subGridfile_Linesclass = "";
         GridfileColumn = new GXWebColumn();
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         A838LoteArquivoAnexo_Arquivo = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A840LoteArquivoAnexo_TipoArq = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         scmdbuf = "";
         H00H92_A596Lote_Codigo = new int[1] ;
         H00H92_A563Lote_Nome = new String[] {""} ;
         H00H93_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         H00H93_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         H00H93_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         H00H93_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         H00H93_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         H00H93_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         H00H93_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00H93_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         H00H93_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         H00H93_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         A1027LoteArquivoAnexo_Verificador = "";
         edtLoteArquivoAnexo_Arquivo_Filename = "";
         AV8CaptchaMessage = "";
         GridfileRow = new GXWebRow();
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         H00H94_A596Lote_Codigo = new int[1] ;
         H00H94_A563Lote_Nome = new String[] {""} ;
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.autenticidade__default(),
            new Object[][] {
                new Object[] {
               H00H92_A596Lote_Codigo, H00H92_A563Lote_Nome
               }
               , new Object[] {
               H00H93_A1027LoteArquivoAnexo_Verificador, H00H93_n1027LoteArquivoAnexo_Verificador, H00H93_A839LoteArquivoAnexo_NomeArq, H00H93_n839LoteArquivoAnexo_NomeArq, H00H93_A840LoteArquivoAnexo_TipoArq, H00H93_n840LoteArquivoAnexo_TipoArq, H00H93_A836LoteArquivoAnexo_Data, H00H93_A841LoteArquivoAnexo_LoteCod, H00H93_A838LoteArquivoAnexo_Arquivo, H00H93_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               H00H94_A596Lote_Codigo, H00H94_A563Lote_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_17 ;
      private short nGXsfl_17_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGridfile_Backcolorstyle ;
      private short subGridfile_Titlebackstyle ;
      private short subGridfile_Allowselection ;
      private short subGridfile_Allowhovering ;
      private short subGridfile_Allowcollapsing ;
      private short subGridfile_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_17_Refreshing=0 ;
      private short AV6vrQtd ;
      private short GXt_int1 ;
      private short nGXWrapped ;
      private short subGridfile_Backstyle ;
      private short GRIDFILE_nEOF ;
      private int subGridfile_Visible ;
      private int subGridfile_Titlebackcolor ;
      private int subGridfile_Allbackcolor ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int subGridfile_Selectioncolor ;
      private int subGridfile_Hoveringcolor ;
      private int subGridfile_Islastpage ;
      private int idxLst ;
      private int subGridfile_Backcolor ;
      private long GRIDFILE_nCurrentRecord ;
      private long GRIDFILE_nFirstRecordOnPage ;
      private String edtLoteArquivoAnexo_Arquivo_Linktarget ;
      private String Gpxrecaptcha1_Response ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_17_idx="0001" ;
      private String edtLoteArquivoAnexo_Arquivo_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gpxrecaptcha1_Sitekey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sStyleString ;
      private String subGridfile_Internalname ;
      private String subGridfile_Class ;
      private String subGridfile_Linesclass ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynLoteArquivoAnexo_LoteCod_Internalname ;
      private String edtLoteArquivoAnexo_Data_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String edtavLotearquivoanexo_verificador_Internalname ;
      private String edtLoteArquivoAnexo_Arquivo_Filename ;
      private String edtLoteArquivoAnexo_Arquivo_Filetype ;
      private String AV8CaptchaMessage ;
      private String Gpxrecaptcha1_Internalname ;
      private String tblTable1_Internalname ;
      private String TempTags ;
      private String edtavLotearquivoanexo_verificador_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Internalname ;
      private String edtLoteArquivoAnexo_TipoArq_Internalname ;
      private String sGXsfl_17_fel_idx="0001" ;
      private String dynLoteArquivoAnexo_LoteCod_Jsonclick ;
      private String ROClassString ;
      private String edtLoteArquivoAnexo_Data_Jsonclick ;
      private String edtLoteArquivoAnexo_Arquivo_Contenttype ;
      private String edtLoteArquivoAnexo_Arquivo_Parameters ;
      private String edtLoteArquivoAnexo_Arquivo_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Jsonclick ;
      private String edtLoteArquivoAnexo_TipoArq_Jsonclick ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private bool n1027LoteArquivoAnexo_Verificador ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool returnInSub ;
      private bool AV7CaptchaValid ;
      private String AV5LoteArquivoAnexo_Verificador ;
      private String A1027LoteArquivoAnexo_Verificador ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridfileContainer ;
      private GXWebRow GridfileRow ;
      private GXWebColumn GridfileColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynLoteArquivoAnexo_LoteCod ;
      private IDataStoreProvider pr_default ;
      private int[] H00H92_A596Lote_Codigo ;
      private String[] H00H92_A563Lote_Nome ;
      private String[] H00H93_A1027LoteArquivoAnexo_Verificador ;
      private bool[] H00H93_n1027LoteArquivoAnexo_Verificador ;
      private String[] H00H93_A839LoteArquivoAnexo_NomeArq ;
      private bool[] H00H93_n839LoteArquivoAnexo_NomeArq ;
      private String[] H00H93_A840LoteArquivoAnexo_TipoArq ;
      private bool[] H00H93_n840LoteArquivoAnexo_TipoArq ;
      private DateTime[] H00H93_A836LoteArquivoAnexo_Data ;
      private int[] H00H93_A841LoteArquivoAnexo_LoteCod ;
      private String[] H00H93_A838LoteArquivoAnexo_Arquivo ;
      private bool[] H00H93_n838LoteArquivoAnexo_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] H00H94_A596Lote_Codigo ;
      private String[] H00H94_A563Lote_Nome ;
      private GXWebForm Form ;
   }

   public class autenticidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00H92 ;
          prmH00H92 = new Object[] {
          } ;
          Object[] prmH00H93 ;
          prmH00H93 = new Object[] {
          new Object[] {"@AV5LoteArquivoAnexo_Verificador",SqlDbType.VarChar,40,0}
          } ;
          Object[] prmH00H94 ;
          prmH00H94 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00H92", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H92,0,0,true,false )
             ,new CursorDef("H00H93", "SELECT [LoteArquivoAnexo_Verificador], [LoteArquivoAnexo_NomeArq], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_Data], [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Arquivo] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_Verificador] = @AV5LoteArquivoAnexo_Verificador ORDER BY [LoteArquivoAnexo_Verificador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H93,11,0,true,false )
             ,new CursorDef("H00H94", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00H94,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getBLOBFile(6, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
