/*
               File: ContagemResultadoNotificacaoDestinatarioWC
        Description: Contagem Resultado Notificacao Destinatario WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:15.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadonotificacaodestinatariowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadonotificacaodestinatariowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadonotificacaodestinatariowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( long aP0_ContagemResultadoNotificacao_Codigo )
      {
         this.AV7ContagemResultadoNotificacao_Codigo = aP0_ContagemResultadoNotificacao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkavFlag = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(long)AV7ContagemResultadoNotificacao_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_9 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_9_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_9_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV7ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0)));
                  AV38Pgmname = GetNextPar( );
                  A1419ContagemResultadoNotificacao_DestEmail = GetNextPar( );
                  n1419ContagemResultadoNotificacao_DestEmail = false;
                  A1414ContagemResultadoNotificacao_DestCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV21Email = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Email", AV21Email);
                  A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
                  AV31ContagemResultadoNotificacao_DestCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoNotificacao_DestCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContagemResultadoNotificacao_DestCod), 6, 0)));
                  A1647Usuario_Email = GetNextPar( );
                  n1647Usuario_Email = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1647Usuario_Email", A1647Usuario_Email);
                  A341Usuario_UserGamGuid = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ContagemResultadoNotificacao_Codigo, AV38Pgmname, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod, AV6WWPContext, AV21Email, A1Usuario_Codigo, AV31ContagemResultadoNotificacao_DestCod, A1647Usuario_Email, A341Usuario_UserGamGuid, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ContagemResultadoNotificacaoDestinatarioWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("contagemresultadonotificacaodestinatariowc:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_Codigo:"+context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAQV2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV38Pgmname = "ContagemResultadoNotificacaoDestinatarioWC";
               context.Gx_err = 0;
               WSQV2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Notificacao Destinatario WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311761638");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadonotificacaodestinatariowc.aspx") + "?" + UrlEncode("" +AV7ContagemResultadoNotificacao_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_9", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_9), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV38Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vEMAIL", AV21Email);
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADONOTIFICACAO_DESTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31ContagemResultadoNotificacao_DestCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_EMAIL", A1647Usuario_Email);
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_USERGAMGUID", StringUtil.RTrim( A341Usuario_UserGamGuid));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUSUARIOS", AV24Usuarios);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUSUARIOS", AV24Usuarios);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL", A1418ContagemResultadoNotificacao_CorpoEmail);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTIFICACAO_ASSUNTO", A1417ContagemResultadoNotificacao_Assunto);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCODIGOS", AV26Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCODIGOS", AV26Codigos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vATTACHMENTS", AV30Attachments);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vATTACHMENTS", AV30Attachments);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vRESULTADO", StringUtil.RTrim( AV29Resultado));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContagemResultadoNotificacaoDestinatarioWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadonotificacaodestinatariowc:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_Codigo:"+context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"));
      }

      protected void RenderHtmlCloseFormQV2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadonotificacaodestinatariowc.js", "?2020311761651");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoNotificacaoDestinatarioWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Notificacao Destinatario WC" ;
      }

      protected void WBQV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadonotificacaodestinatariowc.aspx");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_QV2( true) ;
         }
         else
         {
            wb_table1_2_QV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")), context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoNotificacao_Codigo_Visible, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "Codigo10", "right", false, "HLP_ContagemResultadoNotificacaoDestinatarioWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoNotificacaoDestinatarioWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'" + sGXsfl_9_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContagemResultadoNotificacaoDestinatarioWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTQV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Notificacao Destinatario WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPQV0( ) ;
            }
         }
      }

      protected void WSQV2( )
      {
         STARTQV2( ) ;
         EVTQV2( ) ;
      }

      protected void EVTQV2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQV0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOREENVIAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQV0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11QV2 */
                                    E11QV2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQV0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = chkavFlag_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQV0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQV0( ) ;
                              }
                              nGXsfl_9_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
                              SubsflControlProps_92( ) ;
                              AV19Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFlag_Internalname, AV19Flag);
                              A1414ContagemResultadoNotificacao_DestCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestCod_Internalname), ",", "."));
                              A1421ContagemResultadoNotificacao_DestNome = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_DestNome_Internalname));
                              n1421ContagemResultadoNotificacao_DestNome = false;
                              A1419ContagemResultadoNotificacao_DestEmail = cgiGet( edtContagemResultadoNotificacao_DestEmail_Internalname);
                              n1419ContagemResultadoNotificacao_DestEmail = false;
                              AV20Warning = cgiGet( edtavWarning_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavWarning_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV20Warning)) ? AV34Warning_GXI : context.convertURL( context.PathToRelativeUrl( AV20Warning))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12QV2 */
                                          E12QV2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13QV2 */
                                          E13QV2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14QV2 */
                                          E14QV2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPQV0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQV2( ) ;
            }
         }
      }

      protected void PAQV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "vFLAG_" + sGXsfl_9_idx;
            chkavFlag.Name = GXCCtl;
            chkavFlag.WebTags = "";
            chkavFlag.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavFlag_Internalname, "TitleCaption", chkavFlag.Caption);
            chkavFlag.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_92( ) ;
         while ( nGXsfl_9_idx <= nRC_GXsfl_9 )
         {
            sendrow_92( ) ;
            nGXsfl_9_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_idx+1));
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       long AV7ContagemResultadoNotificacao_Codigo ,
                                       String AV38Pgmname ,
                                       String A1419ContagemResultadoNotificacao_DestEmail ,
                                       int A1414ContagemResultadoNotificacao_DestCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV21Email ,
                                       int A1Usuario_Codigo ,
                                       int AV31ContagemResultadoNotificacao_DestCod ,
                                       String A1647Usuario_Email ,
                                       String A341Usuario_UserGamGuid ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFQV2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1414ContagemResultadoNotificacao_DestCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1419ContagemResultadoNotificacao_DestEmail, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL", A1419ContagemResultadoNotificacao_DestEmail);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV38Pgmname = "ContagemResultadoNotificacaoDestinatarioWC";
         context.Gx_err = 0;
      }

      protected void RFQV2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 9;
         /* Execute user event: E13QV2 */
         E13QV2 ();
         nGXsfl_9_idx = 1;
         sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
         SubsflControlProps_92( ) ;
         nGXsfl_9_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_92( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1412ContagemResultadoNotificacao_Codigo ,
                                                 AV7ContagemResultadoNotificacao_Codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.LONG, TypeConstants.LONG
                                                 }
            });
            /* Using cursor H00QV2 */
            pr_default.execute(0, new Object[] {AV7ContagemResultadoNotificacao_Codigo, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_9_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1426ContagemResultadoNotificacao_DestPesCod = H00QV2_A1426ContagemResultadoNotificacao_DestPesCod[0];
               n1426ContagemResultadoNotificacao_DestPesCod = H00QV2_n1426ContagemResultadoNotificacao_DestPesCod[0];
               A1418ContagemResultadoNotificacao_CorpoEmail = H00QV2_A1418ContagemResultadoNotificacao_CorpoEmail[0];
               n1418ContagemResultadoNotificacao_CorpoEmail = H00QV2_n1418ContagemResultadoNotificacao_CorpoEmail[0];
               A1417ContagemResultadoNotificacao_Assunto = H00QV2_A1417ContagemResultadoNotificacao_Assunto[0];
               n1417ContagemResultadoNotificacao_Assunto = H00QV2_n1417ContagemResultadoNotificacao_Assunto[0];
               A1412ContagemResultadoNotificacao_Codigo = H00QV2_A1412ContagemResultadoNotificacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
               A1419ContagemResultadoNotificacao_DestEmail = H00QV2_A1419ContagemResultadoNotificacao_DestEmail[0];
               n1419ContagemResultadoNotificacao_DestEmail = H00QV2_n1419ContagemResultadoNotificacao_DestEmail[0];
               A1421ContagemResultadoNotificacao_DestNome = H00QV2_A1421ContagemResultadoNotificacao_DestNome[0];
               n1421ContagemResultadoNotificacao_DestNome = H00QV2_n1421ContagemResultadoNotificacao_DestNome[0];
               A1414ContagemResultadoNotificacao_DestCod = H00QV2_A1414ContagemResultadoNotificacao_DestCod[0];
               A1418ContagemResultadoNotificacao_CorpoEmail = H00QV2_A1418ContagemResultadoNotificacao_CorpoEmail[0];
               n1418ContagemResultadoNotificacao_CorpoEmail = H00QV2_n1418ContagemResultadoNotificacao_CorpoEmail[0];
               A1417ContagemResultadoNotificacao_Assunto = H00QV2_A1417ContagemResultadoNotificacao_Assunto[0];
               n1417ContagemResultadoNotificacao_Assunto = H00QV2_n1417ContagemResultadoNotificacao_Assunto[0];
               A1426ContagemResultadoNotificacao_DestPesCod = H00QV2_A1426ContagemResultadoNotificacao_DestPesCod[0];
               n1426ContagemResultadoNotificacao_DestPesCod = H00QV2_n1426ContagemResultadoNotificacao_DestPesCod[0];
               A1421ContagemResultadoNotificacao_DestNome = H00QV2_A1421ContagemResultadoNotificacao_DestNome[0];
               n1421ContagemResultadoNotificacao_DestNome = H00QV2_n1421ContagemResultadoNotificacao_DestNome[0];
               /* Execute user event: E14QV2 */
               E14QV2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 9;
            WBQV0( ) ;
         }
         nGXsfl_9_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1412ContagemResultadoNotificacao_Codigo ,
                                              AV7ContagemResultadoNotificacao_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.LONG, TypeConstants.LONG
                                              }
         });
         /* Using cursor H00QV3 */
         pr_default.execute(1, new Object[] {AV7ContagemResultadoNotificacao_Codigo});
         GRID_nRecordCount = H00QV3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ContagemResultadoNotificacao_Codigo, AV38Pgmname, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod, AV6WWPContext, AV21Email, A1Usuario_Codigo, AV31ContagemResultadoNotificacao_DestCod, A1647Usuario_Email, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ContagemResultadoNotificacao_Codigo, AV38Pgmname, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod, AV6WWPContext, AV21Email, A1Usuario_Codigo, AV31ContagemResultadoNotificacao_DestCod, A1647Usuario_Email, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ContagemResultadoNotificacao_Codigo, AV38Pgmname, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod, AV6WWPContext, AV21Email, A1Usuario_Codigo, AV31ContagemResultadoNotificacao_DestCod, A1647Usuario_Email, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ContagemResultadoNotificacao_Codigo, AV38Pgmname, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod, AV6WWPContext, AV21Email, A1Usuario_Codigo, AV31ContagemResultadoNotificacao_DestCod, A1647Usuario_Email, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ContagemResultadoNotificacao_Codigo, AV38Pgmname, A1419ContagemResultadoNotificacao_DestEmail, A1414ContagemResultadoNotificacao_DestCod, AV6WWPContext, AV21Email, A1Usuario_Codigo, AV31ContagemResultadoNotificacao_DestCod, A1647Usuario_Email, A341Usuario_UserGamGuid, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPQV0( )
      {
         /* Before Start, stand alone formulas. */
         AV38Pgmname = "ContagemResultadoNotificacaoDestinatarioWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12QV2 */
         E12QV2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            /* Read saved values. */
            nRC_GXsfl_9 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_9"), ",", "."));
            wcpOAV7ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultadoNotificacao_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContagemResultadoNotificacaoDestinatarioWC";
            A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contagemresultadonotificacaodestinatariowc:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_Codigo:"+context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12QV2 */
         E12QV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12QV2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtContagemResultadoNotificacao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E13QV2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultadoNotificacao_DestNome_Titleformat = 2;
         edtContagemResultadoNotificacao_DestNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Destinat�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_DestNome_Internalname, "Title", edtContagemResultadoNotificacao_DestNome_Title);
         edtContagemResultadoNotificacao_DestEmail_Titleformat = 2;
         edtContagemResultadoNotificacao_DestEmail_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Email", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_DestEmail_Internalname, "Title", edtContagemResultadoNotificacao_DestEmail_Title);
         AV23WebSession.Set("Caller", "NtfDst");
         AV23WebSession.Set("CodigoNtf", StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      private void E14QV2( )
      {
         /* Grid_Load Routine */
         AV20Warning = context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavWarning_Internalname, AV20Warning);
         AV34Warning_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         edtavWarning_Tooltiptext = "";
         edtContagemResultadoNotificacao_DestEmail_Link = "mailto:"+A1419ContagemResultadoNotificacao_DestEmail;
         edtContagemResultadoNotificacao_DestNome_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1414ContagemResultadoNotificacao_DestCod) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( AV6WWPContext.gxTpr_Contratante_codigo > 0 )
         {
            GXt_int1 = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            if ( ! new prc_usuarioehcontratantedaarea(context).executeUdp( ref  GXt_int1, ref  A1414ContagemResultadoNotificacao_DestCod) )
            {
               edtContagemResultadoNotificacao_DestNome_Link = "";
            }
         }
         else if ( AV6WWPContext.gxTpr_Contratada_codigo > 0 )
         {
            if ( ! new prc_usuarioehcontratada(context).executeUdp(  AV6WWPContext.gxTpr_Contratada_codigo,  A1414ContagemResultadoNotificacao_DestCod) )
            {
               edtContagemResultadoNotificacao_DestNome_Link = "";
            }
         }
         AV31ContagemResultadoNotificacao_DestCod = A1414ContagemResultadoNotificacao_DestCod;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ContagemResultadoNotificacao_DestCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContagemResultadoNotificacao_DestCod), 6, 0)));
         /* Execute user subroutine: 'EMAILATUAL' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( StringUtil.StrCmp(AV21Email, A1419ContagemResultadoNotificacao_DestEmail) != 0 )
         {
            edtavWarning_Visible = 1;
            edtavWarning_Tooltiptext = "Alterado para "+AV21Email;
         }
         else
         {
            edtavWarning_Visible = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 9;
         }
         sendrow_92( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_9_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(9, GridRow);
         }
      }

      protected void E11QV2( )
      {
         /* 'DoReenviar' Routine */
         /* Start For Each Line */
         nRC_GXsfl_9 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_9"), ",", "."));
         nGXsfl_9_fel_idx = 0;
         while ( nGXsfl_9_fel_idx < nRC_GXsfl_9 )
         {
            nGXsfl_9_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_fel_idx+1));
            sGXsfl_9_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_92( ) ;
            AV19Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
            A1414ContagemResultadoNotificacao_DestCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestCod_Internalname), ",", "."));
            A1421ContagemResultadoNotificacao_DestNome = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_DestNome_Internalname));
            n1421ContagemResultadoNotificacao_DestNome = false;
            A1419ContagemResultadoNotificacao_DestEmail = cgiGet( edtContagemResultadoNotificacao_DestEmail_Internalname);
            n1419ContagemResultadoNotificacao_DestEmail = false;
            AV20Warning = cgiGet( edtavWarning_Internalname);
            if ( AV19Flag )
            {
               AV24Usuarios.Add(A1414ContagemResultadoNotificacao_DestCod, 0);
            }
            /* End For Each Line */
         }
         if ( nGXsfl_9_fel_idx == 0 )
         {
            nGXsfl_9_idx = 1;
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         nGXsfl_9_fel_idx = 1;
         if ( (0==AV24Usuarios.Count) )
         {
            GX_msglist.addItem("Selecione os usu�rios aos que deseja reenviar a notifica��o!");
         }
         else
         {
            AV28EmailText = A1418ContagemResultadoNotificacao_CorpoEmail;
            AV27Subject = A1417ContagemResultadoNotificacao_Assunto;
            if ( (Convert.ToDecimal(0)==NumberUtil.Val( AV23WebSession.Get("DemandaCodigo"), ".")) )
            {
               /* Using cursor H00QV4 */
               pr_default.execute(2, new Object[] {AV7ContagemResultadoNotificacao_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1412ContagemResultadoNotificacao_Codigo = H00QV4_A1412ContagemResultadoNotificacao_Codigo[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9")));
                  A456ContagemResultado_Codigo = H00QV4_A456ContagemResultado_Codigo[0];
                  AV26Codigos.Add(A456ContagemResultado_Codigo, 0);
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               AV23WebSession.Set("DemandaCodigo", AV26Codigos.ToXml(false, true, "Collection", ""));
            }
            new prc_enviaremail(context ).execute(  AV6WWPContext.gxTpr_Areatrabalho_codigo,  AV24Usuarios,  AV27Subject,  AV28EmailText,  AV30Attachments, ref  AV29Resultado) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Resultado", AV29Resultado);
            if ( StringUtil.StringSearch( AV29Resultado, "com sucesso", 1) > 0 )
            {
               /* Start For Each Line */
               nRC_GXsfl_9 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_9"), ",", "."));
               nGXsfl_9_fel_idx = 0;
               while ( nGXsfl_9_fel_idx < nRC_GXsfl_9 )
               {
                  nGXsfl_9_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_fel_idx+1));
                  sGXsfl_9_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_fel_idx), 4, 0)), 4, "0");
                  SubsflControlProps_fel_92( ) ;
                  AV19Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
                  A1414ContagemResultadoNotificacao_DestCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_DestCod_Internalname), ",", "."));
                  A1421ContagemResultadoNotificacao_DestNome = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_DestNome_Internalname));
                  n1421ContagemResultadoNotificacao_DestNome = false;
                  A1419ContagemResultadoNotificacao_DestEmail = cgiGet( edtContagemResultadoNotificacao_DestEmail_Internalname);
                  n1419ContagemResultadoNotificacao_DestEmail = false;
                  AV20Warning = cgiGet( edtavWarning_Internalname);
                  AV19Flag = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFlag_Internalname, AV19Flag);
                  /* End For Each Line */
               }
               if ( nGXsfl_9_fel_idx == 0 )
               {
                  nGXsfl_9_idx = 1;
                  sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
                  SubsflControlProps_92( ) ;
               }
               nGXsfl_9_fel_idx = 1;
            }
            GX_msglist.addItem(AV29Resultado);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24Usuarios", AV24Usuarios);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV26Codigos", AV26Codigos);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV38Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV38Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV38Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV38Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         new wwpbaseobjects.savegridstate(context ).execute(  AV38Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV38Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoNotificacao";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultadoNotificacao_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S142( )
      {
         /* 'EMAILATUAL' Routine */
         AV21Email = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Email", AV21Email);
         /* Using cursor H00QV5 */
         pr_default.execute(3, new Object[] {AV31ContagemResultadoNotificacao_DestCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1Usuario_Codigo = H00QV5_A1Usuario_Codigo[0];
            A1647Usuario_Email = H00QV5_A1647Usuario_Email[0];
            n1647Usuario_Email = H00QV5_n1647Usuario_Email[0];
            A341Usuario_UserGamGuid = H00QV5_A341Usuario_UserGamGuid[0];
            if ( StringUtil.Len( A1647Usuario_Email) > 0 )
            {
               AV21Email = A1647Usuario_Email;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Email", AV21Email);
            }
            else
            {
               AV22GamUser.load( A341Usuario_UserGamGuid);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
               AV21Email = AV22GamUser.gxTpr_Email;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Email", AV21Email);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      protected void wb_table1_2_QV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 10, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"9\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DestNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DestNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DestNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoNotificacao_DestEmail_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoNotificacao_DestEmail_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoNotificacao_DestEmail_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(20), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavWarning_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV19Flag));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DestNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestNome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultadoNotificacao_DestNome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1419ContagemResultadoNotificacao_DestEmail);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoNotificacao_DestEmail_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoNotificacao_DestEmail_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultadoNotificacao_DestEmail_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV20Warning));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavWarning_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavWarning_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 9 )
         {
            wbEnd = 0;
            nRC_GXsfl_9 = (short)(nGXsfl_9_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgReenviar_Internalname, context.GetImagePath( "2b9a356f-9313-4e8e-922b-4c68202af1d2", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Reenviar notifica��o aos destinat�rios selecionados", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgReenviar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOREENVIAR\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNotificacaoDestinatarioWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QV2e( true) ;
         }
         else
         {
            wb_table1_2_QV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultadoNotificacao_Codigo = Convert.ToInt64(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQV2( ) ;
         WSQV2( ) ;
         WEQV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContagemResultadoNotificacao_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAQV2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadonotificacaodestinatariowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAQV2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContagemResultadoNotificacao_Codigo = Convert.ToInt64(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0)));
         }
         wcpOAV7ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultadoNotificacao_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContagemResultadoNotificacao_Codigo != wcpOAV7ContagemResultadoNotificacao_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContagemResultadoNotificacao_Codigo = AV7ContagemResultadoNotificacao_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContagemResultadoNotificacao_Codigo = cgiGet( sPrefix+"AV7ContagemResultadoNotificacao_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContagemResultadoNotificacao_Codigo) > 0 )
         {
            AV7ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sCtrlAV7ContagemResultadoNotificacao_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0)));
         }
         else
         {
            AV7ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContagemResultadoNotificacao_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAQV2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSQV2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSQV2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultadoNotificacao_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContagemResultadoNotificacao_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultadoNotificacao_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContagemResultadoNotificacao_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEQV2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031176188");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("contagemresultadonotificacaodestinatariowc.js", "?202031176189");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_92( )
      {
         chkavFlag_Internalname = sPrefix+"vFLAG_"+sGXsfl_9_idx;
         edtContagemResultadoNotificacao_DestCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_9_idx;
         edtContagemResultadoNotificacao_DestNome_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_9_idx;
         edtContagemResultadoNotificacao_DestEmail_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_9_idx;
         edtavWarning_Internalname = sPrefix+"vWARNING_"+sGXsfl_9_idx;
      }

      protected void SubsflControlProps_fel_92( )
      {
         chkavFlag_Internalname = sPrefix+"vFLAG_"+sGXsfl_9_fel_idx;
         edtContagemResultadoNotificacao_DestCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTCOD_"+sGXsfl_9_fel_idx;
         edtContagemResultadoNotificacao_DestNome_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTNOME_"+sGXsfl_9_fel_idx;
         edtContagemResultadoNotificacao_DestEmail_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL_"+sGXsfl_9_fel_idx;
         edtavWarning_Internalname = sPrefix+"vWARNING_"+sGXsfl_9_fel_idx;
      }

      protected void sendrow_92( )
      {
         SubsflControlProps_92( ) ;
         WBQV0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_9_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_9_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_9_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 10,'"+sPrefix+"',false,'"+sGXsfl_9_idx+"',9)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavFlag_Internalname,StringUtil.BoolToStr( AV19Flag),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(10, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,10);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1414ContagemResultadoNotificacao_DestCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1414ContagemResultadoNotificacao_DestCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestNome_Internalname,StringUtil.RTrim( A1421ContagemResultadoNotificacao_DestNome),StringUtil.RTrim( context.localUtil.Format( A1421ContagemResultadoNotificacao_DestNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultadoNotificacao_DestNome_Link,(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoNotificacao_DestEmail_Internalname,(String)A1419ContagemResultadoNotificacao_DestEmail,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultadoNotificacao_DestEmail_Link,(String)"",(String)"",(String)"",(String)edtContagemResultadoNotificacao_DestEmail_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"email",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"Email",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavWarning_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV20Warning_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV20Warning))&&String.IsNullOrEmpty(StringUtil.RTrim( AV34Warning_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV20Warning)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavWarning_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV20Warning)) ? AV34Warning_GXI : context.PathToRelativeUrl( AV20Warning)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavWarning_Visible,(short)0,(String)"",(String)edtavWarning_Tooltiptext,(short)0,(short)1,(short)20,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV20Warning_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTCOD"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, context.localUtil.Format( (decimal)(A1414ContagemResultadoNotificacao_DestCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sPrefix+sGXsfl_9_idx, StringUtil.RTrim( context.localUtil.Format( A1419ContagemResultadoNotificacao_DestEmail, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_9_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_idx+1));
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         /* End function sendrow_92 */
      }

      protected void init_default_properties( )
      {
         chkavFlag_Internalname = sPrefix+"vFLAG";
         edtContagemResultadoNotificacao_DestCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTCOD";
         edtContagemResultadoNotificacao_DestNome_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTNOME";
         edtContagemResultadoNotificacao_DestEmail_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL";
         edtavWarning_Internalname = sPrefix+"vWARNING";
         imgReenviar_Internalname = sPrefix+"REENVIAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContagemResultadoNotificacao_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultadoNotificacao_DestEmail_Jsonclick = "";
         edtContagemResultadoNotificacao_DestNome_Jsonclick = "";
         edtContagemResultadoNotificacao_DestCod_Jsonclick = "";
         chkavFlag.Visible = -1;
         chkavFlag.Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavWarning_Tooltiptext = "";
         edtContagemResultadoNotificacao_DestEmail_Link = "";
         edtContagemResultadoNotificacao_DestNome_Link = "";
         edtavWarning_Visible = -1;
         edtContagemResultadoNotificacao_DestEmail_Titleformat = 0;
         edtContagemResultadoNotificacao_DestNome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtContagemResultadoNotificacao_DestEmail_Title = "Email";
         edtContagemResultadoNotificacao_DestNome_Title = "Destinat�rio";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavFlag.Caption = "";
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContagemResultadoNotificacao_Codigo_Jsonclick = "";
         edtContagemResultadoNotificacao_Codigo_Visible = 1;
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Email',fld:'vEMAIL',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31ContagemResultadoNotificacao_DestCod',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNotificacao_DestNome_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestNome_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestEmail_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestEmail_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Title'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E14QV2',iparms:[{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Email',fld:'vEMAIL',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31ContagemResultadoNotificacao_DestCod',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''}],oparms:[{av:'AV20Warning',fld:'vWARNING',pic:'',nv:''},{av:'edtavWarning_Tooltiptext',ctrl:'vWARNING',prop:'Tooltiptext'},{av:'edtContagemResultadoNotificacao_DestEmail_Link',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Link'},{av:'edtContagemResultadoNotificacao_DestNome_Link',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Link'},{av:'AV31ContagemResultadoNotificacao_DestCod',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'edtavWarning_Visible',ctrl:'vWARNING',prop:'Visible'},{av:'AV21Email',fld:'vEMAIL',pic:'',nv:''}]}");
         setEventMetadata("'DOREENVIAR'","{handler:'E11QV2',iparms:[{av:'AV19Flag',fld:'vFLAG',grid:9,pic:'',nv:false},{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',grid:9,pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV24Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'A1418ContagemResultadoNotificacao_CorpoEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL',pic:'',nv:''},{av:'A1417ContagemResultadoNotificacao_Assunto',fld:'CONTAGEMRESULTADONOTIFICACAO_ASSUNTO',pic:'',nv:''},{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV26Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV30Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV29Resultado',fld:'vRESULTADO',pic:'',nv:''}],oparms:[{av:'AV24Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV26Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV29Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'AV19Flag',fld:'vFLAG',pic:'',nv:false}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Email',fld:'vEMAIL',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31ContagemResultadoNotificacao_DestCod',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNotificacao_DestNome_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestNome_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestEmail_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestEmail_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Title'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Email',fld:'vEMAIL',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31ContagemResultadoNotificacao_DestCod',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNotificacao_DestNome_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestNome_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestEmail_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestEmail_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Title'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Email',fld:'vEMAIL',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31ContagemResultadoNotificacao_DestCod',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNotificacao_DestNome_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestNome_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestEmail_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestEmail_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Title'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1419ContagemResultadoNotificacao_DestEmail',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',pic:'',hsh:true,nv:''},{av:'A1414ContagemResultadoNotificacao_DestCod',fld:'CONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV21Email',fld:'vEMAIL',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31ContagemResultadoNotificacao_DestCod',fld:'vCONTAGEMRESULTADONOTIFICACAO_DESTCOD',pic:'ZZZZZ9',nv:0},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ContagemResultadoNotificacao_Codigo',fld:'vCONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContagemResultadoNotificacao_DestNome_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestNome_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTNOME',prop:'Title'},{av:'edtContagemResultadoNotificacao_DestEmail_Titleformat',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Titleformat'},{av:'edtContagemResultadoNotificacao_DestEmail_Title',ctrl:'CONTAGEMRESULTADONOTIFICACAO_DESTEMAIL',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV38Pgmname = "";
         A1419ContagemResultadoNotificacao_DestEmail = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV21Email = "";
         A1647Usuario_Email = "";
         A341Usuario_UserGamGuid = "";
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV24Usuarios = new GxSimpleCollection();
         A1418ContagemResultadoNotificacao_CorpoEmail = "";
         A1417ContagemResultadoNotificacao_Assunto = "";
         AV26Codigos = new GxSimpleCollection();
         AV30Attachments = new GxSimpleCollection();
         AV29Resultado = "";
         GX_FocusControl = "";
         TempTags = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1421ContagemResultadoNotificacao_DestNome = "";
         AV20Warning = "";
         AV34Warning_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00QV2_A1426ContagemResultadoNotificacao_DestPesCod = new int[1] ;
         H00QV2_n1426ContagemResultadoNotificacao_DestPesCod = new bool[] {false} ;
         H00QV2_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         H00QV2_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         H00QV2_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         H00QV2_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         H00QV2_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         H00QV2_A1419ContagemResultadoNotificacao_DestEmail = new String[] {""} ;
         H00QV2_n1419ContagemResultadoNotificacao_DestEmail = new bool[] {false} ;
         H00QV2_A1421ContagemResultadoNotificacao_DestNome = new String[] {""} ;
         H00QV2_n1421ContagemResultadoNotificacao_DestNome = new bool[] {false} ;
         H00QV2_A1414ContagemResultadoNotificacao_DestCod = new int[1] ;
         H00QV3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         AV23WebSession = context.GetSession();
         GridRow = new GXWebRow();
         AV28EmailText = "";
         AV27Subject = "";
         H00QV4_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         H00QV4_A456ContagemResultado_Codigo = new int[1] ;
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00QV5_A1Usuario_Codigo = new int[1] ;
         H00QV5_A1647Usuario_Email = new String[] {""} ;
         H00QV5_n1647Usuario_Email = new bool[] {false} ;
         H00QV5_A341Usuario_UserGamGuid = new String[] {""} ;
         AV22GamUser = new SdtGAMUser(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgReenviar_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContagemResultadoNotificacao_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadonotificacaodestinatariowc__default(),
            new Object[][] {
                new Object[] {
               H00QV2_A1426ContagemResultadoNotificacao_DestPesCod, H00QV2_n1426ContagemResultadoNotificacao_DestPesCod, H00QV2_A1418ContagemResultadoNotificacao_CorpoEmail, H00QV2_n1418ContagemResultadoNotificacao_CorpoEmail, H00QV2_A1417ContagemResultadoNotificacao_Assunto, H00QV2_n1417ContagemResultadoNotificacao_Assunto, H00QV2_A1412ContagemResultadoNotificacao_Codigo, H00QV2_A1419ContagemResultadoNotificacao_DestEmail, H00QV2_n1419ContagemResultadoNotificacao_DestEmail, H00QV2_A1421ContagemResultadoNotificacao_DestNome,
               H00QV2_n1421ContagemResultadoNotificacao_DestNome, H00QV2_A1414ContagemResultadoNotificacao_DestCod
               }
               , new Object[] {
               H00QV3_AGRID_nRecordCount
               }
               , new Object[] {
               H00QV4_A1412ContagemResultadoNotificacao_Codigo, H00QV4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00QV5_A1Usuario_Codigo, H00QV5_A1647Usuario_Email, H00QV5_n1647Usuario_Email, H00QV5_A341Usuario_UserGamGuid
               }
            }
         );
         AV38Pgmname = "ContagemResultadoNotificacaoDestinatarioWC";
         /* GeneXus formulas. */
         AV38Pgmname = "ContagemResultadoNotificacaoDestinatarioWC";
         context.Gx_err = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_9 ;
      private short nGXsfl_9_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_9_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoNotificacao_DestNome_Titleformat ;
      private short edtContagemResultadoNotificacao_DestEmail_Titleformat ;
      private short nGXsfl_9_fel_idx=1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1414ContagemResultadoNotificacao_DestCod ;
      private int A1Usuario_Codigo ;
      private int AV31ContagemResultadoNotificacao_DestCod ;
      private int A456ContagemResultado_Codigo ;
      private int edtContagemResultadoNotificacao_Codigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A1426ContagemResultadoNotificacao_DestPesCod ;
      private int GXt_int1 ;
      private int edtavWarning_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long AV7ContagemResultadoNotificacao_Codigo ;
      private long wcpOAV7ContagemResultadoNotificacao_Codigo ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_9_idx="0001" ;
      private String AV38Pgmname ;
      private String A341Usuario_UserGamGuid ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV29Resultado ;
      private String GX_FocusControl ;
      private String edtContagemResultadoNotificacao_Codigo_Internalname ;
      private String edtContagemResultadoNotificacao_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavFlag_Internalname ;
      private String edtContagemResultadoNotificacao_DestCod_Internalname ;
      private String A1421ContagemResultadoNotificacao_DestNome ;
      private String edtContagemResultadoNotificacao_DestNome_Internalname ;
      private String edtContagemResultadoNotificacao_DestEmail_Internalname ;
      private String edtavWarning_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String hsh ;
      private String edtContagemResultadoNotificacao_DestNome_Title ;
      private String edtContagemResultadoNotificacao_DestEmail_Title ;
      private String edtavWarning_Tooltiptext ;
      private String edtContagemResultadoNotificacao_DestEmail_Link ;
      private String edtContagemResultadoNotificacao_DestNome_Link ;
      private String sGXsfl_9_fel_idx="0001" ;
      private String AV27Subject ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgReenviar_Internalname ;
      private String imgReenviar_Jsonclick ;
      private String sCtrlAV7ContagemResultadoNotificacao_Codigo ;
      private String ROClassString ;
      private String edtContagemResultadoNotificacao_DestCod_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestNome_Jsonclick ;
      private String edtContagemResultadoNotificacao_DestEmail_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n1419ContagemResultadoNotificacao_DestEmail ;
      private bool n1647Usuario_Email ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV19Flag ;
      private bool n1421ContagemResultadoNotificacao_DestNome ;
      private bool n1426ContagemResultadoNotificacao_DestPesCod ;
      private bool n1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV20Warning_IsBlob ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String AV28EmailText ;
      private String A1419ContagemResultadoNotificacao_DestEmail ;
      private String AV21Email ;
      private String A1647Usuario_Email ;
      private String A1418ContagemResultadoNotificacao_CorpoEmail ;
      private String AV34Warning_GXI ;
      private String AV20Warning ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavFlag ;
      private IDataStoreProvider pr_default ;
      private int[] H00QV2_A1426ContagemResultadoNotificacao_DestPesCod ;
      private bool[] H00QV2_n1426ContagemResultadoNotificacao_DestPesCod ;
      private String[] H00QV2_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] H00QV2_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] H00QV2_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] H00QV2_n1417ContagemResultadoNotificacao_Assunto ;
      private long[] H00QV2_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] H00QV2_A1419ContagemResultadoNotificacao_DestEmail ;
      private bool[] H00QV2_n1419ContagemResultadoNotificacao_DestEmail ;
      private String[] H00QV2_A1421ContagemResultadoNotificacao_DestNome ;
      private bool[] H00QV2_n1421ContagemResultadoNotificacao_DestNome ;
      private int[] H00QV2_A1414ContagemResultadoNotificacao_DestCod ;
      private long[] H00QV3_AGRID_nRecordCount ;
      private long[] H00QV4_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] H00QV4_A456ContagemResultado_Codigo ;
      private int[] H00QV5_A1Usuario_Codigo ;
      private String[] H00QV5_A1647Usuario_Email ;
      private bool[] H00QV5_n1647Usuario_Email ;
      private String[] H00QV5_A341Usuario_UserGamGuid ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private IGxSession AV23WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV24Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV26Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30Attachments ;
      private SdtGAMUser AV22GamUser ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemresultadonotificacaodestinatariowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00QV2( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             long A1412ContagemResultadoNotificacao_Codigo ,
                                             long AV7ContagemResultadoNotificacao_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [6] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_DestPesCod, T2.[ContagemResultadoNotificacao_CorpoEmail], T2.[ContagemResultadoNotificacao_Assunto], T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestEmail], T4.[Pessoa_Nome] AS ContagemResultadoNotificacao_DestNome, T1.[ContagemResultadoNotificacao_DestCod] AS ContagemResultadoNotificacao_DestCod";
         sFromString = " FROM ((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContagemResultadoNotificacao_Codigo] = @AV7ContagemResultadoNotificacao_Codigo)";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo] DESC, T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestEmail]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo] DESC, T1.[ContagemResultadoNotificacao_DestEmail] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_DestCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00QV3( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             long A1412ContagemResultadoNotificacao_Codigo ,
                                             long AV7ContagemResultadoNotificacao_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [1] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContagemResultadoNotificacaoDestinatario] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoNotificacao] T2 WITH (NOLOCK) ON T2.[ContagemResultadoNotificacao_Codigo] = T1.[ContagemResultadoNotificacao_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_DestCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoNotificacao_Codigo] = @AV7ContagemResultadoNotificacao_Codigo)";
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00QV2(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (long)dynConstraints[2] , (long)dynConstraints[3] );
               case 1 :
                     return conditional_H00QV3(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (long)dynConstraints[2] , (long)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QV4 ;
          prmH00QV4 = new Object[] {
          new Object[] {"@AV7ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00QV5 ;
          prmH00QV5 = new Object[] {
          new Object[] {"@AV31ContagemResultadoNotificacao_DestCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QV2 ;
          prmH00QV2 = new Object[] {
          new Object[] {"@AV7ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00QV3 ;
          prmH00QV3 = new Object[] {
          new Object[] {"@AV7ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QV2,11,0,true,false )
             ,new CursorDef("H00QV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QV3,1,0,true,false )
             ,new CursorDef("H00QV4", "SELECT [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultadoNotificacao_Codigo] = @AV7ContagemResultadoNotificacao_Codigo ORDER BY [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QV4,100,0,false,false )
             ,new CursorDef("H00QV5", "SELECT TOP 1 [Usuario_Codigo], [Usuario_Email], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV31ContagemResultadoNotificacao_DestCod ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QV5,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((long[]) buf[6])[0] = rslt.getLong(4) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
