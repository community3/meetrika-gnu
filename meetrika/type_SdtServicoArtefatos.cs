/*
               File: type_SdtServicoArtefatos
        Description: Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:55.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ServicoArtefatos" )]
   [XmlType(TypeName =  "ServicoArtefatos" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtServicoArtefatos : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoArtefatos( )
      {
         /* Constructor for serialization */
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc = "";
         gxTv_SdtServicoArtefatos_Mode = "";
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z = "";
      }

      public SdtServicoArtefatos( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV155Servico_Codigo ,
                        int AV1766ServicoArtefato_ArtefatoCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV155Servico_Codigo,(int)AV1766ServicoArtefato_ArtefatoCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Servico_Codigo", typeof(int)}, new Object[]{"ServicoArtefato_ArtefatoCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ServicoArtefatos");
         metadata.Set("BT", "ServicoArtefatos");
         metadata.Set("PK", "[ \"Servico_Codigo\",\"ServicoArtefato_ArtefatoCod\" ]");
         metadata.Set("PKAssigned", "[ \"ServicoArtefato_ArtefatoCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Artefatos_Codigo\" ],\"FKMap\":[ \"ServicoArtefato_ArtefatoCod-Artefatos_Codigo\" ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoartefato_artefatocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoartefato_artefatodsc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicoartefato_artefatodsc_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtServicoArtefatos deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtServicoArtefatos)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtServicoArtefatos obj ;
         obj = this;
         obj.gxTpr_Servico_codigo = deserialized.gxTpr_Servico_codigo;
         obj.gxTpr_Servicoartefato_artefatocod = deserialized.gxTpr_Servicoartefato_artefatocod;
         obj.gxTpr_Servicoartefato_artefatodsc = deserialized.gxTpr_Servicoartefato_artefatodsc;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Servico_codigo_Z = deserialized.gxTpr_Servico_codigo_Z;
         obj.gxTpr_Servicoartefato_artefatocod_Z = deserialized.gxTpr_Servicoartefato_artefatocod_Z;
         obj.gxTpr_Servicoartefato_artefatodsc_Z = deserialized.gxTpr_Servicoartefato_artefatodsc_Z;
         obj.gxTpr_Servicoartefato_artefatodsc_N = deserialized.gxTpr_Servicoartefato_artefatodsc_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo") )
               {
                  gxTv_SdtServicoArtefatos_Servico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoArtefato_ArtefatoCod") )
               {
                  gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoArtefato_ArtefatoDsc") )
               {
                  gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtServicoArtefatos_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtServicoArtefatos_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo_Z") )
               {
                  gxTv_SdtServicoArtefatos_Servico_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoArtefato_ArtefatoCod_Z") )
               {
                  gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoArtefato_ArtefatoDsc_Z") )
               {
                  gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoArtefato_ArtefatoDsc_N") )
               {
                  gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ServicoArtefatos";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Servico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoArtefatos_Servico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoArtefato_ArtefatoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoArtefato_ArtefatoDsc", StringUtil.RTrim( gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtServicoArtefatos_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoArtefatos_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Servico_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoArtefatos_Servico_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoArtefato_ArtefatoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoArtefato_ArtefatoDsc_Z", StringUtil.RTrim( gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ServicoArtefato_ArtefatoDsc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Servico_Codigo", gxTv_SdtServicoArtefatos_Servico_codigo, false);
         AddObjectProperty("ServicoArtefato_ArtefatoCod", gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod, false);
         AddObjectProperty("ServicoArtefato_ArtefatoDsc", gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtServicoArtefatos_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtServicoArtefatos_Initialized, false);
            AddObjectProperty("Servico_Codigo_Z", gxTv_SdtServicoArtefatos_Servico_codigo_Z, false);
            AddObjectProperty("ServicoArtefato_ArtefatoCod_Z", gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z, false);
            AddObjectProperty("ServicoArtefato_ArtefatoDsc_Z", gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z, false);
            AddObjectProperty("ServicoArtefato_ArtefatoDsc_N", gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Servico_Codigo" )]
      [  XmlElement( ElementName = "Servico_Codigo"   )]
      public int gxTpr_Servico_codigo
      {
         get {
            return gxTv_SdtServicoArtefatos_Servico_codigo ;
         }

         set {
            if ( gxTv_SdtServicoArtefatos_Servico_codigo != value )
            {
               gxTv_SdtServicoArtefatos_Mode = "INS";
               this.gxTv_SdtServicoArtefatos_Servico_codigo_Z_SetNull( );
               this.gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z_SetNull( );
               this.gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z_SetNull( );
            }
            gxTv_SdtServicoArtefatos_Servico_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoArtefato_ArtefatoCod" )]
      [  XmlElement( ElementName = "ServicoArtefato_ArtefatoCod"   )]
      public int gxTpr_Servicoartefato_artefatocod
      {
         get {
            return gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod ;
         }

         set {
            if ( gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod != value )
            {
               gxTv_SdtServicoArtefatos_Mode = "INS";
               this.gxTv_SdtServicoArtefatos_Servico_codigo_Z_SetNull( );
               this.gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z_SetNull( );
               this.gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z_SetNull( );
            }
            gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoArtefato_ArtefatoDsc" )]
      [  XmlElement( ElementName = "ServicoArtefato_ArtefatoDsc"   )]
      public String gxTpr_Servicoartefato_artefatodsc
      {
         get {
            return gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc ;
         }

         set {
            gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N = 0;
            gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc = (String)(value);
         }

      }

      public void gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_SetNull( )
      {
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N = 1;
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc = "";
         return  ;
      }

      public bool gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtServicoArtefatos_Mode ;
         }

         set {
            gxTv_SdtServicoArtefatos_Mode = (String)(value);
         }

      }

      public void gxTv_SdtServicoArtefatos_Mode_SetNull( )
      {
         gxTv_SdtServicoArtefatos_Mode = "";
         return  ;
      }

      public bool gxTv_SdtServicoArtefatos_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtServicoArtefatos_Initialized ;
         }

         set {
            gxTv_SdtServicoArtefatos_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtServicoArtefatos_Initialized_SetNull( )
      {
         gxTv_SdtServicoArtefatos_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtServicoArtefatos_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Codigo_Z" )]
      [  XmlElement( ElementName = "Servico_Codigo_Z"   )]
      public int gxTpr_Servico_codigo_Z
      {
         get {
            return gxTv_SdtServicoArtefatos_Servico_codigo_Z ;
         }

         set {
            gxTv_SdtServicoArtefatos_Servico_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoArtefatos_Servico_codigo_Z_SetNull( )
      {
         gxTv_SdtServicoArtefatos_Servico_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoArtefatos_Servico_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoArtefato_ArtefatoCod_Z" )]
      [  XmlElement( ElementName = "ServicoArtefato_ArtefatoCod_Z"   )]
      public int gxTpr_Servicoartefato_artefatocod_Z
      {
         get {
            return gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z ;
         }

         set {
            gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z_SetNull( )
      {
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoArtefato_ArtefatoDsc_Z" )]
      [  XmlElement( ElementName = "ServicoArtefato_ArtefatoDsc_Z"   )]
      public String gxTpr_Servicoartefato_artefatodsc_Z
      {
         get {
            return gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z ;
         }

         set {
            gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z = (String)(value);
         }

      }

      public void gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z_SetNull( )
      {
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z = "";
         return  ;
      }

      public bool gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoArtefato_ArtefatoDsc_N" )]
      [  XmlElement( ElementName = "ServicoArtefato_ArtefatoDsc_N"   )]
      public short gxTpr_Servicoartefato_artefatodsc_N
      {
         get {
            return gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N ;
         }

         set {
            gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N = (short)(value);
         }

      }

      public void gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N_SetNull( )
      {
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N = 0;
         return  ;
      }

      public bool gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc = "";
         gxTv_SdtServicoArtefatos_Mode = "";
         gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "servicoartefatos", "GeneXus.Programs.servicoartefatos_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtServicoArtefatos_Initialized ;
      private short gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtServicoArtefatos_Servico_codigo ;
      private int gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod ;
      private int gxTv_SdtServicoArtefatos_Servico_codigo_Z ;
      private int gxTv_SdtServicoArtefatos_Servicoartefato_artefatocod_Z ;
      private String gxTv_SdtServicoArtefatos_Mode ;
      private String sTagName ;
      private String gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc ;
      private String gxTv_SdtServicoArtefatos_Servicoartefato_artefatodsc_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ServicoArtefatos", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtServicoArtefatos_RESTInterface : GxGenericCollectionItem<SdtServicoArtefatos>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServicoArtefatos_RESTInterface( ) : base()
      {
      }

      public SdtServicoArtefatos_RESTInterface( SdtServicoArtefatos psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Servico_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_codigo
      {
         get {
            return sdt.gxTpr_Servico_codigo ;
         }

         set {
            sdt.gxTpr_Servico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoArtefato_ArtefatoCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicoartefato_artefatocod
      {
         get {
            return sdt.gxTpr_Servicoartefato_artefatocod ;
         }

         set {
            sdt.gxTpr_Servicoartefato_artefatocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoArtefato_ArtefatoDsc" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Servicoartefato_artefatodsc
      {
         get {
            return sdt.gxTpr_Servicoartefato_artefatodsc ;
         }

         set {
            sdt.gxTpr_Servicoartefato_artefatodsc = (String)(value);
         }

      }

      public SdtServicoArtefatos sdt
      {
         get {
            return (SdtServicoArtefatos)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtServicoArtefatos() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 9 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
