/*
               File: WP_EstatisticaPieGrf
        Description: Estatistica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:45:53.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_estatisticapiegrf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_estatisticapiegrf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_estatisticapiegrf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_contratada_Codigo ,
                           int aP2_ContagemResultado_SistemaCod ,
                           short aP3_Filtro_Ano ,
                           long aP4_Filtro_Mes ,
                           String aP5_Graficar )
      {
         this.AV5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV7contratada_Codigo = aP1_contratada_Codigo;
         this.AV6ContagemResultado_SistemaCod = aP2_ContagemResultado_SistemaCod;
         this.AV13Filtro_Ano = aP3_Filtro_Ano;
         this.AV14Filtro_Mes = aP4_Filtro_Mes;
         this.AV15Graficar = aP5_Graficar;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV5AreaTrabalho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5AreaTrabalho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5AreaTrabalho_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7contratada_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7contratada_Codigo), "ZZZZZ9")));
                  AV6ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_SistemaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_SistemaCod), "ZZZZZ9")));
                  AV13Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Filtro_Ano), "ZZZ9")));
                  AV14Filtro_Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Filtro_Mes), 10, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14Filtro_Mes), "ZZZZZZZZZ9")));
                  AV15Graficar = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Graficar", AV15Graficar);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15Graficar, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEL2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEL2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423455372");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("GxChart/gxChart.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_estatisticapiegrf.aspx") + "?" + UrlEncode("" +AV5AreaTrabalho_Codigo) + "," + UrlEncode("" +AV7contratada_Codigo) + "," + UrlEncode("" +AV6ContagemResultado_SistemaCod) + "," + UrlEncode("" +AV13Filtro_Ano) + "," + UrlEncode("" +AV14Filtro_Mes) + "," + UrlEncode(StringUtil.RTrim(AV15Graficar))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGXCHARTDATA", GxChartData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGXCHARTDATA", GxChartData);
         }
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFILTRO_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Filtro_Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFILTRO_MES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Filtro_Mes), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRAFICAR", StringUtil.RTrim( AV15Graficar));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Filtro_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14Filtro_Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15Graficar, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Filtro_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14Filtro_Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15Graficar, ""))));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Title", StringUtil.RTrim( Gxchartcontrol_Title));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Width", StringUtil.RTrim( Gxchartcontrol_Width));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Height", StringUtil.RTrim( Gxchartcontrol_Height));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Opacity", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol_Opacity), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Charttype", StringUtil.RTrim( Gxchartcontrol_Charttype));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_X_axistitle", StringUtil.RTrim( Gxchartcontrol_X_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Y_axistitle", StringUtil.RTrim( Gxchartcontrol_Y_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Backgroundcolor1", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol_Backgroundcolor1), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Graphcolor2", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol_Graphcolor2), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL_Visible", StringUtil.BoolToStr( Gxchartcontrol_Visible));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEL2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEL2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_estatisticapiegrf.aspx") + "?" + UrlEncode("" +AV5AreaTrabalho_Codigo) + "," + UrlEncode("" +AV7contratada_Codigo) + "," + UrlEncode("" +AV6ContagemResultado_SistemaCod) + "," + UrlEncode("" +AV13Filtro_Ano) + "," + UrlEncode("" +AV14Filtro_Mes) + "," + UrlEncode(StringUtil.RTrim(AV15Graficar)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_EstatisticaPieGrf" ;
      }

      public override String GetPgmdesc( )
      {
         return "Estatistica" ;
      }

      protected void WBEL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_EL2( true) ;
         }
         else
         {
            wb_table1_2_EL2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EL2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTEL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Estatistica", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEL0( ) ;
      }

      protected void WSEL2( )
      {
         STARTEL2( ) ;
         EVTEL2( ) ;
      }

      protected void EVTEL2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11EL2 */
                              E11EL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12EL2 */
                              E12EL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFEL2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12EL2 */
            E12EL2 ();
            WBEL0( ) ;
         }
      }

      protected void STRUPEL0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11EL2 */
         E11EL2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vGXCHARTDATA"), GxChartData);
            /* Read variables values. */
            /* Read saved values. */
            Gxchartcontrol_Title = cgiGet( "GXCHARTCONTROL_Title");
            Gxchartcontrol_Width = cgiGet( "GXCHARTCONTROL_Width");
            Gxchartcontrol_Height = cgiGet( "GXCHARTCONTROL_Height");
            Gxchartcontrol_Opacity = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL_Opacity"), ",", "."));
            Gxchartcontrol_Charttype = cgiGet( "GXCHARTCONTROL_Charttype");
            Gxchartcontrol_X_axistitle = cgiGet( "GXCHARTCONTROL_X_axistitle");
            Gxchartcontrol_Y_axistitle = cgiGet( "GXCHARTCONTROL_Y_axistitle");
            Gxchartcontrol_Backgroundcolor1 = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL_Backgroundcolor1"), ",", "."));
            Gxchartcontrol_Graphcolor2 = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL_Graphcolor2"), ",", "."));
            Gxchartcontrol_Visible = StringUtil.StrToBool( cgiGet( "GXCHARTCONTROL_Visible"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11EL2 */
         E11EL2 ();
         if (returnInSub) return;
      }

      protected void E11EL2( )
      {
         /* Start Routine */
         lblTbsemdados_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbsemdados_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbsemdados_Visible), 5, 0)));
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV7contratada_Codigo ,
                                              A39Contratada_Codigo ,
                                              A92Contrato_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00EL6 */
         pr_default.execute(0, new Object[] {AV7contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A40Contratada_PessoaCod = H00EL6_A40Contratada_PessoaCod[0];
            A74Contrato_Codigo = H00EL6_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00EL6_n74Contrato_Codigo[0];
            A92Contrato_Ativo = H00EL6_A92Contrato_Ativo[0];
            A39Contratada_Codigo = H00EL6_A39Contratada_Codigo[0];
            A81Contrato_Quantidade = H00EL6_A81Contrato_Quantidade[0];
            A41Contratada_PessoaNom = H00EL6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00EL6_n41Contratada_PessoaNom[0];
            A82Contrato_DataVigenciaInicio = H00EL6_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = H00EL6_A83Contrato_DataVigenciaTermino[0];
            A842Contrato_DataInicioTA = H00EL6_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = H00EL6_n842Contrato_DataInicioTA[0];
            A843Contrato_DataFimTA = H00EL6_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00EL6_n843Contrato_DataFimTA[0];
            A842Contrato_DataInicioTA = H00EL6_A842Contrato_DataInicioTA[0];
            n842Contrato_DataInicioTA = H00EL6_n842Contrato_DataInicioTA[0];
            A843Contrato_DataFimTA = H00EL6_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00EL6_n843Contrato_DataFimTA[0];
            A40Contratada_PessoaCod = H00EL6_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = H00EL6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00EL6_n41Contratada_PessoaNom[0];
            AV10Contrato_Quantidade = (int)(AV10Contrato_Quantidade+A81Contrato_Quantidade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contrato_Quantidade), 9, 0)));
            AV8Contratada_Nome = " da " + A41Contratada_PessoaNom;
            AV51Contrato_DataInicioTA = A842Contrato_DataInicioTA;
            if ( (DateTime.MinValue==AV51Contrato_DataInicioTA) )
            {
               AV47DataIni = A82Contrato_DataVigenciaInicio;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DataIni", context.localUtil.Format(AV47DataIni, "99/99/99"));
               AV48DataFim = A83Contrato_DataVigenciaTermino;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DataFim", context.localUtil.Format(AV48DataFim, "99/99/99"));
            }
            else
            {
               AV47DataIni = AV51Contrato_DataInicioTA;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DataIni", context.localUtil.Format(AV47DataIni, "99/99/99"));
               AV48DataFim = A843Contrato_DataFimTA;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DataFim", context.localUtil.Format(AV48DataFim, "99/99/99"));
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( (0==AV7contratada_Codigo) )
         {
            AV8Contratada_Nome = " (Todas as Contratadas)";
         }
         Gxchartcontrol_Y_axistitle = "Demandas";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
         if ( StringUtil.StrCmp(AV15Graficar, "DXS") == 0 )
         {
            Gxchartcontrol_Title = "Demandas por Status"+AV8Contratada_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
            /* Execute user subroutine: 'DXSOUPFS' */
            S112 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "PFS") == 0 )
         {
            Gxchartcontrol_Title = "Pontos de Fun��o por Status"+AV8Contratada_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
            Gxchartcontrol_Y_axistitle = "Pontos de Fun��o";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
            /* Execute user subroutine: 'DXSOUPFS' */
            S112 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "PXS") == 0 )
         {
            Gxchartcontrol_Title = "Pontos de Fun��o por Sistema"+AV8Contratada_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
            Gxchartcontrol_Y_axistitle = "Pontos de Fun��o";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
            /* Execute user subroutine: 'PXS' */
            S122 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "UDI") == 0 )
         {
            Gxchartcontrol_Title = "Percentual de INM usados"+AV8Contratada_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
            Gxchartcontrol_Y_axistitle = "Percentual";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
            /* Execute user subroutine: 'UDI' */
            S132 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "UDC") == 0 )
         {
            Gxchartcontrol_Title = "Utiliza��o do Contrato"+AV8Contratada_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
            Gxchartcontrol_X_axistitle = context.localUtil.DToC( AV47DataIni, 2, "/")+" - "+context.localUtil.DToC( AV48DataFim, 2, "/");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "X_AxisTitle", Gxchartcontrol_X_axistitle);
            Gxchartcontrol_Y_axistitle = "Percentual";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
            /* Execute user subroutine: 'UDC' */
            S142 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "PUC") == 0 )
         {
            Gxchartcontrol_Title = "Utiliza��o do Contrato"+AV8Contratada_Nome;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
            Gxchartcontrol_X_axistitle = context.localUtil.DToC( AV47DataIni, 2, "/")+" - "+context.localUtil.DToC( AV48DataFim, 2, "/");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "X_AxisTitle", Gxchartcontrol_X_axistitle);
            Gxchartcontrol_Y_axistitle = "Pontos de Fun��o";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
            /* Execute user subroutine: 'PUC' */
            S152 ();
            if (returnInSub) return;
         }
         else
         {
            if ( (0==AV14Filtro_Mes) )
            {
               GxChartData.gxTpr_Categories.Add("Janeiro", 0);
               GxChartData.gxTpr_Categories.Add("Fevereiro", 0);
               GxChartData.gxTpr_Categories.Add("Mar�o", 0);
               GxChartData.gxTpr_Categories.Add("Abril", 0);
               GxChartData.gxTpr_Categories.Add("Maio", 0);
               GxChartData.gxTpr_Categories.Add("Junho", 0);
               GxChartData.gxTpr_Categories.Add("Julho", 0);
               GxChartData.gxTpr_Categories.Add("Agosto", 0);
               GxChartData.gxTpr_Categories.Add("Setembro", 0);
               GxChartData.gxTpr_Categories.Add("Outubro", 0);
               GxChartData.gxTpr_Categories.Add("Novembro", 0);
               GxChartData.gxTpr_Categories.Add("Dezembro", 0);
            }
            else
            {
               AV37String = "01/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV14Filtro_Mes), 10, 0)), 2, "0") + "/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0));
               AV12Data = context.localUtil.CToD( AV37String, 2);
               GxChartData.gxTpr_Categories.Add(DateTimeUtil.CMonth( AV12Data, "por")+"/"+StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0), 0);
            }
         }
         if ( StringUtil.StrCmp(AV15Graficar, "DMN") == 0 )
         {
            Gxchartcontrol_Title = "Demandas por Sistema - Ano "+StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0);
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "PFB") == 0 )
         {
            Gxchartcontrol_Y_axistitle = "Pontos de Fun��o";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
            Gxchartcontrol_Title = "PF por Sistemas - Ano "+StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0);
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Title", Gxchartcontrol_Title);
         }
         if ( ( StringUtil.StrCmp(AV15Graficar, "DMN") == 0 ) || ( StringUtil.StrCmp(AV15Graficar, "PF") == 0 ) )
         {
            /* Execute user subroutine: 'GRAFICAR' */
            S162 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "FND") == 0 )
         {
            /* Execute user subroutine: 'FND' */
            S172 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "FNT") == 0 )
         {
            /* Execute user subroutine: 'FNT' */
            S182 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(AV15Graficar, "CP") == 0 )
         {
            /* Execute user subroutine: 'CP' */
            S192 ();
            if (returnInSub) return;
         }
      }

      protected void S162( )
      {
         /* 'GRAFICAR' Routine */
         AV35SDT_Grafico.Clear();
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV7contratada_Codigo ,
                                              AV6ContagemResultado_SistemaCod ,
                                              AV14Filtro_Mes ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV13Filtro_Ano },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00EL7 */
         pr_default.execute(1, new Object[] {AV13Filtro_Ano, AV5AreaTrabalho_Codigo, AV7contratada_Codigo, AV6ContagemResultado_SistemaCod, AV14Filtro_Mes});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKEL4 = false;
            A509ContagemrResultado_SistemaSigla = H00EL7_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL7_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL7_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL7_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00EL7_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EL7_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = H00EL7_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EL7_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EL7_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EL7_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL7_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00EL7_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00EL7_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL7_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL7_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL7_n52Contratada_AreaTrabalhoCod[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV24mQtdeDmn[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV22mPFBFM[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV23mPFBFS[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV19mCTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV25mRTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV21mPendencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV20mDivergencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            AV36SDT_Item.gxTpr_Nome = A509ContagemrResultado_SistemaSigla;
            while ( (pr_default.getStatus(1) != 101) && ( H00EL7_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00EL7_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRKEL4 = false;
               A484ContagemResultado_StatusDmn = H00EL7_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EL7_n484ContagemResultado_StatusDmn[0];
               A471ContagemResultado_DataDmn = H00EL7_A471ContagemResultado_DataDmn[0];
               A489ContagemResultado_SistemaCod = H00EL7_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00EL7_n489ContagemResultado_SistemaCod[0];
               A490ContagemResultado_ContratadaCod = H00EL7_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EL7_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = H00EL7_A456ContagemResultado_Codigo[0];
               if ( ! ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) )
               {
                  GXt_decimal1 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A574ContagemResultado_PFFinal = GXt_decimal1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                  AV18i = (short)(DateTimeUtil.Month( A471ContagemResultado_DataDmn));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
                  AV24mQtdeDmn[AV18i-1] = (long)(AV24mQtdeDmn[AV18i-1]+1);
                  AV22mPFBFM[AV18i-1] = (long)(AV22mPFBFM[AV18i-1]+A574ContagemResultado_PFFinal);
               }
               BRKEL4 = true;
               pr_default.readNext(1);
            }
            if ( (0==AV14Filtro_Mes) )
            {
               AV18i = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
               while ( AV18i <= 12 )
               {
                  if ( StringUtil.StrCmp(AV15Graficar, "DMN") == 0 )
                  {
                     AV36SDT_Item.gxTpr_Mes.Add(AV24mQtdeDmn[AV18i-1], 0);
                     AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+(AV24mQtdeDmn[AV18i-1]));
                  }
                  else if ( StringUtil.StrCmp(AV15Graficar, "PFB") == 0 )
                  {
                     AV36SDT_Item.gxTpr_Mes.Add(AV22mPFBFM[AV18i-1], 0);
                     AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+(AV22mPFBFM[AV18i-1]));
                  }
                  AV18i = (short)(AV18i+1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
               }
            }
            else
            {
               if ( StringUtil.StrCmp(AV15Graficar, "DMN") == 0 )
               {
                  AV36SDT_Item.gxTpr_Mes.Add(AV24mQtdeDmn[(int)(AV14Filtro_Mes)-1], 0);
                  AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+(AV24mQtdeDmn[(int)(AV14Filtro_Mes)-1]));
               }
               else if ( StringUtil.StrCmp(AV15Graficar, "PFB") == 0 )
               {
                  AV36SDT_Item.gxTpr_Mes.Add(AV22mPFBFM[(int)(AV14Filtro_Mes)-1], 0);
                  AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+(AV22mPFBFM[(int)(AV14Filtro_Mes)-1]));
               }
            }
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            if ( ! BRKEL4 )
            {
               BRKEL4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
         AV35SDT_Grafico.Sort("[Quantidade]");
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S172( )
      {
         /* 'FND' Routine */
         AV36SDT_Item.gxTpr_Mes.Clear();
         AV35SDT_Grafico.Clear();
         GxChartData.gxTpr_Categories.Clear();
         GxChartData.gxTpr_Series.Clear();
         GxChartData.gxTpr_Categories.Add("ALI", 0);
         GxChartData.gxTpr_Categories.Add("AIE", 0);
         Gxchartcontrol_Y_axistitle = "Quantidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
         Gxchartcontrol_X_axistitle = "Fun��es de Dados";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "X_AxisTitle", Gxchartcontrol_X_axistitle);
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV7contratada_Codigo ,
                                              AV6ContagemResultado_SistemaCod ,
                                              AV14Filtro_Mes ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV13Filtro_Ano },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00EL8 */
         pr_default.execute(2, new Object[] {AV13Filtro_Ano, AV5AreaTrabalho_Codigo, AV7contratada_Codigo, AV6ContagemResultado_SistemaCod, AV14Filtro_Mes});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKEL6 = false;
            A456ContagemResultado_Codigo = H00EL8_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00EL8_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL8_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL8_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL8_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00EL8_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EL8_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = H00EL8_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EL8_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EL8_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EL8_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL8_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = H00EL8_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL8_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL8_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL8_n52Contratada_AreaTrabalhoCod[0];
            AV36SDT_Item.gxTpr_Nome = A509ContagemrResultado_SistemaSigla;
            while ( (pr_default.getStatus(2) != 101) && ( H00EL8_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00EL8_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRKEL6 = false;
               A456ContagemResultado_Codigo = H00EL8_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = H00EL8_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EL8_n484ContagemResultado_StatusDmn[0];
               A471ContagemResultado_DataDmn = H00EL8_A471ContagemResultado_DataDmn[0];
               A489ContagemResultado_SistemaCod = H00EL8_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00EL8_n489ContagemResultado_SistemaCod[0];
               A490ContagemResultado_ContratadaCod = H00EL8_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EL8_n490ContagemResultado_ContratadaCod[0];
               if ( ! ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) )
               {
                  /* Using cursor H00EL9 */
                  pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A773ContagemResultadoItem_ContagemCod = H00EL9_A773ContagemResultadoItem_ContagemCod[0];
                     n773ContagemResultadoItem_ContagemCod = H00EL9_n773ContagemResultadoItem_ContagemCod[0];
                     A775ContagemResultadoItem_Tipo = H00EL9_A775ContagemResultadoItem_Tipo[0];
                     n775ContagemResultadoItem_Tipo = H00EL9_n775ContagemResultadoItem_Tipo[0];
                     if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "ALI") == 0 )
                     {
                        AV30QtdALI = (long)(AV30QtdALI+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30QtdALI", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30QtdALI), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "AIE") == 0 )
                     {
                        AV29QtdAIE = (long)(AV29QtdAIE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29QtdAIE", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29QtdAIE), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     pr_default.readNext(3);
                  }
                  pr_default.close(3);
               }
               BRKEL6 = true;
               pr_default.readNext(2);
            }
            AV36SDT_Item.gxTpr_Mes.Add(AV30QtdALI, 0);
            AV36SDT_Item.gxTpr_Mes.Add(AV29QtdAIE, 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            if ( ! BRKEL6 )
            {
               BRKEL6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
         AV35SDT_Grafico.Sort("[Quantidade]");
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S182( )
      {
         /* 'FNT' Routine */
         AV36SDT_Item.gxTpr_Mes.Clear();
         AV35SDT_Grafico.Clear();
         GxChartData.gxTpr_Categories.Clear();
         GxChartData.gxTpr_Series.Clear();
         GxChartData.gxTpr_Categories.Add("CE", 0);
         GxChartData.gxTpr_Categories.Add("EE", 0);
         GxChartData.gxTpr_Categories.Add("SE", 0);
         GxChartData.gxTpr_Categories.Add("INM", 0);
         Gxchartcontrol_Y_axistitle = "Quantidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
         Gxchartcontrol_X_axistitle = "Fun��es de Dados";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "X_AxisTitle", Gxchartcontrol_X_axistitle);
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV7contratada_Codigo ,
                                              AV6ContagemResultado_SistemaCod ,
                                              AV14Filtro_Mes ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV13Filtro_Ano },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00EL10 */
         pr_default.execute(4, new Object[] {AV13Filtro_Ano, AV5AreaTrabalho_Codigo, AV7contratada_Codigo, AV6ContagemResultado_SistemaCod, AV14Filtro_Mes});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKEL9 = false;
            A456ContagemResultado_Codigo = H00EL10_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00EL10_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL10_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL10_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL10_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00EL10_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EL10_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = H00EL10_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EL10_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EL10_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EL10_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL10_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = H00EL10_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL10_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL10_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL10_n52Contratada_AreaTrabalhoCod[0];
            AV36SDT_Item.gxTpr_Nome = A509ContagemrResultado_SistemaSigla;
            while ( (pr_default.getStatus(4) != 101) && ( H00EL10_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00EL10_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRKEL9 = false;
               A456ContagemResultado_Codigo = H00EL10_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = H00EL10_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EL10_n484ContagemResultado_StatusDmn[0];
               A471ContagemResultado_DataDmn = H00EL10_A471ContagemResultado_DataDmn[0];
               A489ContagemResultado_SistemaCod = H00EL10_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00EL10_n489ContagemResultado_SistemaCod[0];
               A490ContagemResultado_ContratadaCod = H00EL10_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EL10_n490ContagemResultado_ContratadaCod[0];
               if ( ! ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) )
               {
                  /* Using cursor H00EL11 */
                  pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A773ContagemResultadoItem_ContagemCod = H00EL11_A773ContagemResultadoItem_ContagemCod[0];
                     n773ContagemResultadoItem_ContagemCod = H00EL11_n773ContagemResultadoItem_ContagemCod[0];
                     A775ContagemResultadoItem_Tipo = H00EL11_A775ContagemResultadoItem_Tipo[0];
                     n775ContagemResultadoItem_Tipo = H00EL11_n775ContagemResultadoItem_Tipo[0];
                     if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "CE") == 0 )
                     {
                        AV31QtdCE = (long)(AV31QtdCE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31QtdCE", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31QtdCE), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "EE") == 0 )
                     {
                        AV32QtdEE = (long)(AV32QtdEE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32QtdEE", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32QtdEE), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "SE") == 0 )
                     {
                        AV34QtdSE = (long)(AV34QtdSE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34QtdSE", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34QtdSE), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "INM") == 0 )
                     {
                        AV33QtdINM = (long)(AV33QtdINM+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33QtdINM), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
               }
               BRKEL9 = true;
               pr_default.readNext(4);
            }
            AV36SDT_Item.gxTpr_Mes.Add(AV31QtdCE, 0);
            AV36SDT_Item.gxTpr_Mes.Add(AV32QtdEE, 0);
            AV36SDT_Item.gxTpr_Mes.Add(AV34QtdSE, 0);
            AV36SDT_Item.gxTpr_Mes.Add(AV33QtdINM, 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            if ( ! BRKEL9 )
            {
               BRKEL9 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
         AV35SDT_Grafico.Sort("[Quantidade]");
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S192( )
      {
         /* 'CP' Routine */
         AV36SDT_Item.gxTpr_Mes.Clear();
         AV35SDT_Grafico.Clear();
         GxChartData.gxTpr_Categories.Clear();
         GxChartData.gxTpr_Series.Clear();
         GxChartData.gxTpr_Categories.Add("ALI", 0);
         GxChartData.gxTpr_Categories.Add("AIE", 0);
         Gxchartcontrol_Y_axistitle = "Quantidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Y_AxisTitle", Gxchartcontrol_Y_axistitle);
         Gxchartcontrol_X_axistitle = "Fun��es de Dados";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "X_AxisTitle", Gxchartcontrol_X_axistitle);
         pr_default.dynParam(6, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV7contratada_Codigo ,
                                              AV6ContagemResultado_SistemaCod ,
                                              AV14Filtro_Mes ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV13Filtro_Ano },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00EL12 */
         pr_default.execute(6, new Object[] {AV13Filtro_Ano, AV5AreaTrabalho_Codigo, AV7contratada_Codigo, AV6ContagemResultado_SistemaCod, AV14Filtro_Mes});
         while ( (pr_default.getStatus(6) != 101) )
         {
            BRKEL12 = false;
            A456ContagemResultado_Codigo = H00EL12_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00EL12_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL12_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL12_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL12_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00EL12_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EL12_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = H00EL12_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EL12_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EL12_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EL12_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL12_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = H00EL12_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL12_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL12_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL12_n52Contratada_AreaTrabalhoCod[0];
            AV36SDT_Item.gxTpr_Nome = A509ContagemrResultado_SistemaSigla;
            while ( (pr_default.getStatus(6) != 101) && ( H00EL12_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00EL12_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRKEL12 = false;
               A456ContagemResultado_Codigo = H00EL12_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = H00EL12_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EL12_n484ContagemResultado_StatusDmn[0];
               A471ContagemResultado_DataDmn = H00EL12_A471ContagemResultado_DataDmn[0];
               A489ContagemResultado_SistemaCod = H00EL12_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00EL12_n489ContagemResultado_SistemaCod[0];
               A490ContagemResultado_ContratadaCod = H00EL12_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EL12_n490ContagemResultado_ContratadaCod[0];
               if ( ! ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) )
               {
                  /* Using cursor H00EL13 */
                  pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(7) != 101) )
                  {
                     A773ContagemResultadoItem_ContagemCod = H00EL13_A773ContagemResultadoItem_ContagemCod[0];
                     n773ContagemResultadoItem_ContagemCod = H00EL13_n773ContagemResultadoItem_ContagemCod[0];
                     A775ContagemResultadoItem_Tipo = H00EL13_A775ContagemResultadoItem_Tipo[0];
                     n775ContagemResultadoItem_Tipo = H00EL13_n775ContagemResultadoItem_Tipo[0];
                     if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "ALI") == 0 )
                     {
                        AV30QtdALI = (long)(AV30QtdALI+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30QtdALI", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30QtdALI), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     else if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "AIE") == 0 )
                     {
                        AV29QtdAIE = (long)(AV29QtdAIE+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29QtdAIE", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29QtdAIE), 12, 0)));
                        AV36SDT_Item.gxTpr_Quantidade = (long)(AV36SDT_Item.gxTpr_Quantidade+1);
                     }
                     pr_default.readNext(7);
                  }
                  pr_default.close(7);
               }
               BRKEL12 = true;
               pr_default.readNext(6);
            }
            AV36SDT_Item.gxTpr_Mes.Add(AV22mPFBFM[AV18i-1], 0);
            AV36SDT_Item.gxTpr_Mes.Add(AV22mPFBFM[AV18i-1], 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            if ( ! BRKEL12 )
            {
               BRKEL12 = true;
               pr_default.readNext(6);
            }
         }
         pr_default.close(6);
         AV35SDT_Grafico.Sort("[Quantidade]");
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S112( )
      {
         /* 'DXSOUPFS' Routine */
         pr_default.dynParam(8, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV7contratada_Codigo ,
                                              AV14Filtro_Mes ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV13Filtro_Ano },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00EL14 */
         pr_default.execute(8, new Object[] {AV13Filtro_Ano, AV5AreaTrabalho_Codigo, AV7contratada_Codigo, AV14Filtro_Mes});
         while ( (pr_default.getStatus(8) != 101) )
         {
            BRKEL15 = false;
            A52Contratada_AreaTrabalhoCod = H00EL14_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL14_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00EL14_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EL14_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = H00EL14_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = H00EL14_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL14_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00EL14_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00EL14_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL14_n52Contratada_AreaTrabalhoCod[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV36SDT_Item.gxTpr_Nome = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            GxChartData.gxTpr_Categories.Add(AV36SDT_Item.gxTpr_Nome, 0);
            AV28Qtd = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Qtd", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Qtd), 10, 0)));
            AV27PF = 0;
            while ( (pr_default.getStatus(8) != 101) && ( H00EL14_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00EL14_A484ContagemResultado_StatusDmn[0], A484ContagemResultado_StatusDmn) == 0 ) )
            {
               BRKEL15 = false;
               A471ContagemResultado_DataDmn = H00EL14_A471ContagemResultado_DataDmn[0];
               A490ContagemResultado_ContratadaCod = H00EL14_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EL14_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = H00EL14_A456ContagemResultado_Codigo[0];
               GXt_decimal1 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A574ContagemResultado_PFFinal = GXt_decimal1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
               AV28Qtd = (long)(AV28Qtd+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Qtd", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Qtd), 10, 0)));
               AV27PF = (long)(AV27PF+A574ContagemResultado_PFFinal);
               BRKEL15 = true;
               pr_default.readNext(8);
            }
            if ( StringUtil.StrCmp(AV15Graficar, "DXS") == 0 )
            {
               AV36SDT_Item.gxTpr_Mes.Add(AV28Qtd, 0);
               AV36SDT_Item.gxTpr_Quantidade = AV28Qtd;
            }
            else
            {
               AV36SDT_Item.gxTpr_Mes.Add(AV27PF, 0);
               AV36SDT_Item.gxTpr_Quantidade = AV27PF;
            }
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            if ( ! BRKEL15 )
            {
               BRKEL15 = true;
               pr_default.readNext(8);
            }
         }
         pr_default.close(8);
         AV35SDT_Grafico.Sort("[Quantidade]");
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'PXS' Routine */
         if ( (0==AV14Filtro_Mes) )
         {
            GxChartData.gxTpr_Categories.Add("Ano "+StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0), 0);
         }
         else
         {
            AV37String = "01/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(AV14Filtro_Mes), 10, 0)), 2, "0") + "/" + StringUtil.Trim( StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0));
            AV12Data = context.localUtil.CToD( AV37String, 2);
            GxChartData.gxTpr_Categories.Add(DateTimeUtil.CMonth( AV12Data, "por")+"/"+StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0), 0);
         }
         pr_default.dynParam(9, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV7contratada_Codigo ,
                                              AV6ContagemResultado_SistemaCod ,
                                              AV14Filtro_Mes ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV13Filtro_Ano },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00EL15 */
         pr_default.execute(9, new Object[] {AV13Filtro_Ano, AV5AreaTrabalho_Codigo, AV7contratada_Codigo, AV6ContagemResultado_SistemaCod, AV14Filtro_Mes});
         while ( (pr_default.getStatus(9) != 101) )
         {
            BRKEL17 = false;
            A52Contratada_AreaTrabalhoCod = H00EL15_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL15_n52Contratada_AreaTrabalhoCod[0];
            A509ContagemrResultado_SistemaSigla = H00EL15_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL15_n509ContagemrResultado_SistemaSigla[0];
            A471ContagemResultado_DataDmn = H00EL15_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EL15_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EL15_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EL15_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL15_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00EL15_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EL15_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = H00EL15_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = H00EL15_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00EL15_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = H00EL15_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL15_n52Contratada_AreaTrabalhoCod[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV36SDT_Item.gxTpr_Nome = A509ContagemrResultado_SistemaSigla;
            AV27PF = 0;
            while ( (pr_default.getStatus(9) != 101) && ( H00EL15_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( StringUtil.StrCmp(H00EL15_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRKEL17 = false;
               A471ContagemResultado_DataDmn = H00EL15_A471ContagemResultado_DataDmn[0];
               A489ContagemResultado_SistemaCod = H00EL15_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00EL15_n489ContagemResultado_SistemaCod[0];
               A490ContagemResultado_ContratadaCod = H00EL15_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EL15_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = H00EL15_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EL15_n484ContagemResultado_StatusDmn[0];
               A456ContagemResultado_Codigo = H00EL15_A456ContagemResultado_Codigo[0];
               GXt_decimal1 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A574ContagemResultado_PFFinal = GXt_decimal1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
               AV27PF = (long)(AV27PF+A574ContagemResultado_PFFinal);
               BRKEL17 = true;
               pr_default.readNext(9);
            }
            AV36SDT_Item.gxTpr_Mes.Add(AV27PF, 0);
            AV36SDT_Item.gxTpr_Quantidade = AV27PF;
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            if ( ! BRKEL17 )
            {
               BRKEL17 = true;
               pr_default.readNext(9);
            }
         }
         pr_default.close(9);
         AV35SDT_Grafico.Sort("[Quantidade]");
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S132( )
      {
         /* 'UDI' Routine */
         GxChartData.gxTpr_Categories.Add("", 0);
         pr_default.dynParam(10, new Object[]{ new Object[]{
                                              AV5AreaTrabalho_Codigo ,
                                              AV7contratada_Codigo ,
                                              AV6ContagemResultado_SistemaCod ,
                                              AV14Filtro_Mes ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV13Filtro_Ano },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.LONG, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00EL16 */
         pr_default.execute(10, new Object[] {AV13Filtro_Ano, AV5AreaTrabalho_Codigo, AV7contratada_Codigo, AV6ContagemResultado_SistemaCod, AV14Filtro_Mes});
         while ( (pr_default.getStatus(10) != 101) )
         {
            BRKEL19 = false;
            A456ContagemResultado_Codigo = H00EL16_A456ContagemResultado_Codigo[0];
            A489ContagemResultado_SistemaCod = H00EL16_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EL16_n489ContagemResultado_SistemaCod[0];
            A52Contratada_AreaTrabalhoCod = H00EL16_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL16_n52Contratada_AreaTrabalhoCod[0];
            A484ContagemResultado_StatusDmn = H00EL16_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00EL16_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = H00EL16_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = H00EL16_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL16_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = H00EL16_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00EL16_n52Contratada_AreaTrabalhoCod[0];
            while ( (pr_default.getStatus(10) != 101) && ( H00EL16_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( H00EL16_A489ContagemResultado_SistemaCod[0] == A489ContagemResultado_SistemaCod ) )
            {
               BRKEL19 = false;
               A456ContagemResultado_Codigo = H00EL16_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = H00EL16_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EL16_n484ContagemResultado_StatusDmn[0];
               A471ContagemResultado_DataDmn = H00EL16_A471ContagemResultado_DataDmn[0];
               A490ContagemResultado_ContratadaCod = H00EL16_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EL16_n490ContagemResultado_ContratadaCod[0];
               if ( ! ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) )
               {
                  /* Using cursor H00EL17 */
                  pr_default.execute(11, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(11) != 101) )
                  {
                     A773ContagemResultadoItem_ContagemCod = H00EL17_A773ContagemResultadoItem_ContagemCod[0];
                     n773ContagemResultadoItem_ContagemCod = H00EL17_n773ContagemResultadoItem_ContagemCod[0];
                     A775ContagemResultadoItem_Tipo = H00EL17_A775ContagemResultadoItem_Tipo[0];
                     n775ContagemResultadoItem_Tipo = H00EL17_n775ContagemResultadoItem_Tipo[0];
                     if ( StringUtil.StrCmp(A775ContagemResultadoItem_Tipo, "INM") == 0 )
                     {
                        AV33QtdINM = (long)(AV33QtdINM+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33QtdINM), 12, 0)));
                     }
                     else
                     {
                        AV28Qtd = (long)(AV28Qtd+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Qtd", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Qtd), 10, 0)));
                     }
                     pr_default.readNext(11);
                  }
                  pr_default.close(11);
               }
               BRKEL19 = true;
               pr_default.readNext(10);
            }
            if ( ! BRKEL19 )
            {
               BRKEL19 = true;
               pr_default.readNext(10);
            }
         }
         pr_default.close(10);
         if ( AV28Qtd == 0 )
         {
            AV36SDT_Item.gxTpr_Nome = "AD";
            AV36SDT_Item.gxTpr_Quantidade = 0;
            AV36SDT_Item.gxTpr_Mes.Add(0, 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            AV36SDT_Item.gxTpr_Nome = "Inclus�o";
            AV36SDT_Item.gxTpr_Quantidade = 100;
            AV36SDT_Item.gxTpr_Mes.Add(100, 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
         }
         else
         {
            AV26Percentual = (decimal)(AV33QtdINM/ (decimal)(AV28Qtd)*100);
            AV36SDT_Item.gxTpr_Nome = "AD";
            AV36SDT_Item.gxTpr_Quantidade = (long)(AV26Percentual);
            AV36SDT_Item.gxTpr_Mes.Add((double)(AV26Percentual), 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            AV36SDT_Item.gxTpr_Nome = "Inclus�o";
            AV36SDT_Item.gxTpr_Quantidade = 100;
            AV26Percentual = (decimal)(100-AV26Percentual);
            AV36SDT_Item.gxTpr_Mes.Add((double)(AV26Percentual), 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
         }
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S142( )
      {
         /* 'UDC' Routine */
         GxChartData.gxTpr_Categories.Add("", 0);
         pr_default.dynParam(12, new Object[]{ new Object[]{
                                              AV7contratada_Codigo ,
                                              AV6ContagemResultado_SistemaCod ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A489ContagemResultado_SistemaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV47DataIni ,
                                              AV48DataFim },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE
                                              }
         });
         /* Using cursor H00EL18 */
         pr_default.execute(12, new Object[] {AV47DataIni, AV48DataFim, AV7contratada_Codigo, AV6ContagemResultado_SistemaCod});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A471ContagemResultado_DataDmn = H00EL18_A471ContagemResultado_DataDmn[0];
            A489ContagemResultado_SistemaCod = H00EL18_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00EL18_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = H00EL18_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL18_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00EL18_A456ContagemResultado_Codigo[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV28Qtd = (long)(AV28Qtd+A574ContagemResultado_PFFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Qtd", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Qtd), 10, 0)));
            pr_default.readNext(12);
         }
         pr_default.close(12);
         if ( AV10Contrato_Quantidade == 0 )
         {
            AV36SDT_Item.gxTpr_Nome = "Executado";
            AV36SDT_Item.gxTpr_Quantidade = 0;
            AV36SDT_Item.gxTpr_Mes.Add(0, 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            AV36SDT_Item.gxTpr_Nome = "Para executar";
            AV36SDT_Item.gxTpr_Quantidade = 0;
            AV36SDT_Item.gxTpr_Mes.Add(100, 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
         }
         else
         {
            AV26Percentual = (decimal)(AV28Qtd/ (decimal)(AV10Contrato_Quantidade)*100);
            AV36SDT_Item.gxTpr_Nome = "Executado";
            AV36SDT_Item.gxTpr_Quantidade = (long)(AV26Percentual);
            AV36SDT_Item.gxTpr_Mes.Add((double)(AV26Percentual), 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            AV26Percentual = (decimal)(100-AV26Percentual);
            AV36SDT_Item.gxTpr_Nome = "Para Executar";
            AV36SDT_Item.gxTpr_Quantidade = (long)(AV26Percentual);
            AV36SDT_Item.gxTpr_Mes.Add((double)(AV26Percentual), 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
         }
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'PUC' Routine */
         pr_default.dynParam(13, new Object[]{ new Object[]{
                                              AV7contratada_Codigo ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A471ContagemResultado_DataDmn ,
                                              AV47DataIni ,
                                              AV48DataFim },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE
                                              }
         });
         /* Using cursor H00EL19 */
         pr_default.execute(13, new Object[] {AV47DataIni, AV48DataFim, AV7contratada_Codigo});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A471ContagemResultado_DataDmn = H00EL19_A471ContagemResultado_DataDmn[0];
            A490ContagemResultado_ContratadaCod = H00EL19_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00EL19_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00EL19_A456ContagemResultado_Codigo[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A574ContagemResultado_PFFinal = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
            AV18i = (short)(DateTimeUtil.Month( A471ContagemResultado_DataDmn));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
            AV22mPFBFM[AV18i-1] = (long)(AV22mPFBFM[AV18i-1]+A574ContagemResultado_PFFinal);
            AV49Usado = (long)(AV49Usado+A574ContagemResultado_PFFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49Usado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49Usado), 10, 0)));
            pr_default.readNext(13);
         }
         pr_default.close(13);
         AV37String = "15/" + StringUtil.PadL( StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( AV47DataIni)), 10, 0)), 2, "0") + "/" + StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV47DataIni)), 10, 0));
         AV12Data = context.localUtil.CToD( AV37String, 2);
         AV18i = (short)(DateTimeUtil.Month( AV47DataIni));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
         GxChartData.gxTpr_Categories.Add("Contratado "+StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato_Quantidade), 9, 0)), 0);
         AV12Data = context.localUtil.CToD( AV37String, 2);
         AV28Qtd = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Qtd", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Qtd), 10, 0)));
         AV18i = (short)(DateTimeUtil.Month( AV47DataIni));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
         AV46x = (short)(DateTimeUtil.Month( AV47DataIni));
         while ( AV46x <= DateTimeUtil.Month( AV47DataIni) + 11 )
         {
            AV36SDT_Item.gxTpr_Nome = DateTimeUtil.CMonth( AV12Data, "por")+"/"+StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( AV12Data)), 10, 0))+" - ";
            AV36SDT_Item.gxTpr_Quantidade = AV22mPFBFM[AV18i-1];
            AV36SDT_Item.gxTpr_Mes.Add(AV22mPFBFM[AV18i-1], 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            AV12Data = DateTimeUtil.DAdd(AV12Data,+((int)(30)));
            AV18i = (short)(AV18i+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
            if ( AV18i > 12 )
            {
               AV18i = (short)(AV18i-12);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
            }
            if ( ( DateTimeUtil.Month( AV12Data) == DateTimeUtil.Month( Gx_date) ) && ( DateTimeUtil.Year( AV12Data) == DateTimeUtil.Year( Gx_date) ) )
            {
               if (true) break;
            }
            AV28Qtd = (long)(AV28Qtd+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Qtd", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Qtd), 10, 0)));
            AV46x = (short)(AV46x+1);
         }
         AV50Faltam = (long)((AV10Contrato_Quantidade-AV49Usado)/ (decimal)((12-AV28Qtd)));
         AV46x = (short)(AV28Qtd);
         while ( AV46x <= 11 )
         {
            AV36SDT_Item.gxTpr_Nome = DateTimeUtil.CMonth( AV12Data, "por")+"/"+StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0)+" - ";
            AV36SDT_Item.gxTpr_Quantidade = AV50Faltam;
            AV36SDT_Item.gxTpr_Mes.Add(AV50Faltam, 0);
            AV35SDT_Grafico.Add(AV36SDT_Item, 0);
            AV36SDT_Item = new SdtSDT_Grafico_Item(context);
            AV12Data = DateTimeUtil.DAdd(AV12Data,+((int)(30)));
            AV46x = (short)(AV46x+1);
         }
         /* Execute user subroutine: 'MONTARGRAFICOBARRAS' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S202( )
      {
         /* 'MONTARGRAFICOBARRAS' Routine */
         if ( AV35SDT_Grafico.Count > 0 )
         {
            GxChartSerie.gxTpr_Name = "Status";
            GxChartSerie = new SdtGxChart_Serie(context);
            AV76GXV1 = 1;
            while ( AV76GXV1 <= AV35SDT_Grafico.Count )
            {
               AV36SDT_Item = ((SdtSDT_Grafico_Item)AV35SDT_Grafico.Item(AV76GXV1));
               AV46x = 1;
               while ( AV46x <= AV36SDT_Item.gxTpr_Mes.Count )
               {
                  GxChartSerie.gxTpr_Values.Add((decimal)(AV36SDT_Item.gxTpr_Mes.GetNumeric(AV46x)), 0);
                  AV46x = (short)(AV46x+1);
               }
               AV18i = (short)(GxChartData.gxTpr_Series.Count+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18i), 4, 0)));
               AV76GXV1 = (int)(AV76GXV1+1);
            }
         }
         else
         {
            Gxchartcontrol_Visible = false;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol_Internalname, "Visible", StringUtil.BoolToStr( Gxchartcontrol_Visible));
            lblTbsemdados_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbsemdados_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbsemdados_Visible), 5, 0)));
         }
         GxChartData.gxTpr_Series.Add(GxChartSerie, 0);
      }

      protected void nextLoad( )
      {
      }

      protected void E12EL2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_EL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXCHARTCONTROLContainer"+"\"></div>") ;
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsemdados_Internalname, "Sem dados para graficar", "", "", lblTbsemdados_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:20.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", lblTbsemdados_Visible, 1, 0, "HLP_WP_EstatisticaPieGrf.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EL2e( true) ;
         }
         else
         {
            wb_table1_2_EL2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5AreaTrabalho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5AreaTrabalho_Codigo), "ZZZZZ9")));
         AV7contratada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7contratada_Codigo), "ZZZZZ9")));
         AV6ContagemResultado_SistemaCod = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_SistemaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_SistemaCod), "ZZZZZ9")));
         AV13Filtro_Ano = Convert.ToInt16(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Filtro_Ano), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13Filtro_Ano), "ZZZ9")));
         AV14Filtro_Mes = Convert.ToInt64(getParm(obj,4));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Filtro_Mes), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_MES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14Filtro_Mes), "ZZZZZZZZZ9")));
         AV15Graficar = (String)getParm(obj,5);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Graficar", AV15Graficar);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV15Graficar, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEL2( ) ;
         WSEL2( ) ;
         WEEL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423455494");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_estatisticapiegrf.js", "?202032423455494");
            context.AddJavascriptSource("GxChart/gxChart.js", "");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         Gxchartcontrol_Internalname = "GXCHARTCONTROL";
         lblTbsemdados_Internalname = "TBSEMDADOS";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTbsemdados_Visible = 1;
         Gxchartcontrol_Visible = Convert.ToBoolean( -1);
         Gxchartcontrol_Graphcolor2 = (int)(0xD3D3D3);
         Gxchartcontrol_Backgroundcolor1 = (int)(0xD3D3D3);
         Gxchartcontrol_Y_axistitle = "Demandas";
         Gxchartcontrol_X_axistitle = "Per�odo";
         Gxchartcontrol_Charttype = "PIEEXPL";
         Gxchartcontrol_Opacity = 255;
         Gxchartcontrol_Height = "730";
         Gxchartcontrol_Width = "1150";
         Gxchartcontrol_Title = "Consulta de Sistemas";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Estatistica";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV15Graficar = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GxChartData = new SdtGxChart(context);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Gx_date = DateTime.MinValue;
         scmdbuf = "";
         H00EL6_A40Contratada_PessoaCod = new int[1] ;
         H00EL6_A74Contrato_Codigo = new int[1] ;
         H00EL6_n74Contrato_Codigo = new bool[] {false} ;
         H00EL6_A92Contrato_Ativo = new bool[] {false} ;
         H00EL6_A39Contratada_Codigo = new int[1] ;
         H00EL6_A81Contrato_Quantidade = new int[1] ;
         H00EL6_A41Contratada_PessoaNom = new String[] {""} ;
         H00EL6_n41Contratada_PessoaNom = new bool[] {false} ;
         H00EL6_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00EL6_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00EL6_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         H00EL6_n842Contrato_DataInicioTA = new bool[] {false} ;
         H00EL6_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00EL6_n843Contrato_DataFimTA = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A842Contrato_DataInicioTA = DateTime.MinValue;
         A843Contrato_DataFimTA = DateTime.MinValue;
         AV8Contratada_Nome = "";
         AV51Contrato_DataInicioTA = DateTime.MinValue;
         AV47DataIni = DateTime.MinValue;
         AV48DataFim = DateTime.MinValue;
         AV37String = "";
         AV12Data = DateTime.MinValue;
         AV35SDT_Grafico = new GxObjectCollection( context, "SDT_Grafico.Item", "GxEv3Up14_Meetrika", "SdtSDT_Grafico_Item", "GeneXus.Programs");
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         H00EL7_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EL7_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EL7_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EL7_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EL7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EL7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EL7_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL7_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EL7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EL7_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL7_A456ContagemResultado_Codigo = new int[1] ;
         A509ContagemrResultado_SistemaSigla = "";
         A484ContagemResultado_StatusDmn = "";
         AV24mQtdeDmn = new long [12] ;
         AV22mPFBFM = new long [12] ;
         AV23mPFBFS = new long [12] ;
         AV19mCTPF = new decimal [12] ;
         AV25mRTPF = new decimal [12] ;
         AV21mPendencias = new short [12] ;
         AV20mDivergencias = new short [12] ;
         AV36SDT_Item = new SdtSDT_Grafico_Item(context);
         H00EL8_A456ContagemResultado_Codigo = new int[1] ;
         H00EL8_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EL8_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EL8_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EL8_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EL8_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EL8_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EL8_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL8_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EL8_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EL8_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL8_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL9_A772ContagemResultadoItem_Codigo = new int[1] ;
         H00EL9_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         H00EL9_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         H00EL9_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         H00EL9_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         A775ContagemResultadoItem_Tipo = "";
         H00EL10_A456ContagemResultado_Codigo = new int[1] ;
         H00EL10_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EL10_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EL10_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EL10_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EL10_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EL10_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EL10_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL10_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EL10_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EL10_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL10_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL11_A772ContagemResultadoItem_Codigo = new int[1] ;
         H00EL11_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         H00EL11_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         H00EL11_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         H00EL11_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         H00EL12_A456ContagemResultado_Codigo = new int[1] ;
         H00EL12_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EL12_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EL12_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EL12_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EL12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EL12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EL12_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL12_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EL12_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EL12_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL12_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL13_A772ContagemResultadoItem_Codigo = new int[1] ;
         H00EL13_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         H00EL13_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         H00EL13_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         H00EL13_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         H00EL14_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EL14_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EL14_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EL14_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EL14_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL14_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL14_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL14_A456ContagemResultado_Codigo = new int[1] ;
         H00EL15_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EL15_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EL15_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EL15_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EL15_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL15_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EL15_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EL15_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL15_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EL15_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EL15_A456ContagemResultado_Codigo = new int[1] ;
         H00EL16_A456ContagemResultado_Codigo = new int[1] ;
         H00EL16_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EL16_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EL16_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EL16_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00EL16_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EL16_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EL16_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL16_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL16_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL17_A772ContagemResultadoItem_Codigo = new int[1] ;
         H00EL17_A773ContagemResultadoItem_ContagemCod = new int[1] ;
         H00EL17_n773ContagemResultadoItem_ContagemCod = new bool[] {false} ;
         H00EL17_A775ContagemResultadoItem_Tipo = new String[] {""} ;
         H00EL17_n775ContagemResultadoItem_Tipo = new bool[] {false} ;
         H00EL18_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL18_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EL18_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EL18_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL18_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL18_A456ContagemResultado_Codigo = new int[1] ;
         H00EL19_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EL19_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EL19_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EL19_A456ContagemResultado_Codigo = new int[1] ;
         GxChartSerie = new SdtGxChart_Serie(context);
         sStyleString = "";
         lblTbsemdados_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_estatisticapiegrf__default(),
            new Object[][] {
                new Object[] {
               H00EL6_A40Contratada_PessoaCod, H00EL6_A74Contrato_Codigo, H00EL6_A92Contrato_Ativo, H00EL6_A39Contratada_Codigo, H00EL6_A81Contrato_Quantidade, H00EL6_A41Contratada_PessoaNom, H00EL6_n41Contratada_PessoaNom, H00EL6_A82Contrato_DataVigenciaInicio, H00EL6_A83Contrato_DataVigenciaTermino, H00EL6_A842Contrato_DataInicioTA,
               H00EL6_n842Contrato_DataInicioTA, H00EL6_A843Contrato_DataFimTA, H00EL6_n843Contrato_DataFimTA
               }
               , new Object[] {
               H00EL7_A509ContagemrResultado_SistemaSigla, H00EL7_n509ContagemrResultado_SistemaSigla, H00EL7_A52Contratada_AreaTrabalhoCod, H00EL7_n52Contratada_AreaTrabalhoCod, H00EL7_A484ContagemResultado_StatusDmn, H00EL7_n484ContagemResultado_StatusDmn, H00EL7_A471ContagemResultado_DataDmn, H00EL7_A489ContagemResultado_SistemaCod, H00EL7_n489ContagemResultado_SistemaCod, H00EL7_A490ContagemResultado_ContratadaCod,
               H00EL7_n490ContagemResultado_ContratadaCod, H00EL7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00EL8_A456ContagemResultado_Codigo, H00EL8_A509ContagemrResultado_SistemaSigla, H00EL8_n509ContagemrResultado_SistemaSigla, H00EL8_A52Contratada_AreaTrabalhoCod, H00EL8_n52Contratada_AreaTrabalhoCod, H00EL8_A484ContagemResultado_StatusDmn, H00EL8_n484ContagemResultado_StatusDmn, H00EL8_A471ContagemResultado_DataDmn, H00EL8_A489ContagemResultado_SistemaCod, H00EL8_n489ContagemResultado_SistemaCod,
               H00EL8_A490ContagemResultado_ContratadaCod, H00EL8_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00EL9_A772ContagemResultadoItem_Codigo, H00EL9_A773ContagemResultadoItem_ContagemCod, H00EL9_n773ContagemResultadoItem_ContagemCod, H00EL9_A775ContagemResultadoItem_Tipo, H00EL9_n775ContagemResultadoItem_Tipo
               }
               , new Object[] {
               H00EL10_A456ContagemResultado_Codigo, H00EL10_A509ContagemrResultado_SistemaSigla, H00EL10_n509ContagemrResultado_SistemaSigla, H00EL10_A52Contratada_AreaTrabalhoCod, H00EL10_n52Contratada_AreaTrabalhoCod, H00EL10_A484ContagemResultado_StatusDmn, H00EL10_n484ContagemResultado_StatusDmn, H00EL10_A471ContagemResultado_DataDmn, H00EL10_A489ContagemResultado_SistemaCod, H00EL10_n489ContagemResultado_SistemaCod,
               H00EL10_A490ContagemResultado_ContratadaCod, H00EL10_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00EL11_A772ContagemResultadoItem_Codigo, H00EL11_A773ContagemResultadoItem_ContagemCod, H00EL11_n773ContagemResultadoItem_ContagemCod, H00EL11_A775ContagemResultadoItem_Tipo, H00EL11_n775ContagemResultadoItem_Tipo
               }
               , new Object[] {
               H00EL12_A456ContagemResultado_Codigo, H00EL12_A509ContagemrResultado_SistemaSigla, H00EL12_n509ContagemrResultado_SistemaSigla, H00EL12_A52Contratada_AreaTrabalhoCod, H00EL12_n52Contratada_AreaTrabalhoCod, H00EL12_A484ContagemResultado_StatusDmn, H00EL12_n484ContagemResultado_StatusDmn, H00EL12_A471ContagemResultado_DataDmn, H00EL12_A489ContagemResultado_SistemaCod, H00EL12_n489ContagemResultado_SistemaCod,
               H00EL12_A490ContagemResultado_ContratadaCod, H00EL12_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00EL13_A772ContagemResultadoItem_Codigo, H00EL13_A773ContagemResultadoItem_ContagemCod, H00EL13_n773ContagemResultadoItem_ContagemCod, H00EL13_A775ContagemResultadoItem_Tipo, H00EL13_n775ContagemResultadoItem_Tipo
               }
               , new Object[] {
               H00EL14_A52Contratada_AreaTrabalhoCod, H00EL14_n52Contratada_AreaTrabalhoCod, H00EL14_A484ContagemResultado_StatusDmn, H00EL14_n484ContagemResultado_StatusDmn, H00EL14_A471ContagemResultado_DataDmn, H00EL14_A490ContagemResultado_ContratadaCod, H00EL14_n490ContagemResultado_ContratadaCod, H00EL14_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00EL15_A52Contratada_AreaTrabalhoCod, H00EL15_n52Contratada_AreaTrabalhoCod, H00EL15_A509ContagemrResultado_SistemaSigla, H00EL15_n509ContagemrResultado_SistemaSigla, H00EL15_A471ContagemResultado_DataDmn, H00EL15_A489ContagemResultado_SistemaCod, H00EL15_n489ContagemResultado_SistemaCod, H00EL15_A490ContagemResultado_ContratadaCod, H00EL15_n490ContagemResultado_ContratadaCod, H00EL15_A484ContagemResultado_StatusDmn,
               H00EL15_n484ContagemResultado_StatusDmn, H00EL15_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00EL16_A456ContagemResultado_Codigo, H00EL16_A489ContagemResultado_SistemaCod, H00EL16_n489ContagemResultado_SistemaCod, H00EL16_A52Contratada_AreaTrabalhoCod, H00EL16_n52Contratada_AreaTrabalhoCod, H00EL16_A484ContagemResultado_StatusDmn, H00EL16_n484ContagemResultado_StatusDmn, H00EL16_A471ContagemResultado_DataDmn, H00EL16_A490ContagemResultado_ContratadaCod, H00EL16_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00EL17_A772ContagemResultadoItem_Codigo, H00EL17_A773ContagemResultadoItem_ContagemCod, H00EL17_n773ContagemResultadoItem_ContagemCod, H00EL17_A775ContagemResultadoItem_Tipo, H00EL17_n775ContagemResultadoItem_Tipo
               }
               , new Object[] {
               H00EL18_A471ContagemResultado_DataDmn, H00EL18_A489ContagemResultado_SistemaCod, H00EL18_n489ContagemResultado_SistemaCod, H00EL18_A490ContagemResultado_ContratadaCod, H00EL18_n490ContagemResultado_ContratadaCod, H00EL18_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00EL19_A471ContagemResultado_DataDmn, H00EL19_A490ContagemResultado_ContratadaCod, H00EL19_n490ContagemResultado_ContratadaCod, H00EL19_A456ContagemResultado_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV13Filtro_Ano ;
      private short wcpOAV13Filtro_Ano ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short [] AV21mPendencias ;
      private short [] AV20mDivergencias ;
      private short AV18i ;
      private short AV46x ;
      private int AV5AreaTrabalho_Codigo ;
      private int AV7contratada_Codigo ;
      private int AV6ContagemResultado_SistemaCod ;
      private int wcpOAV5AreaTrabalho_Codigo ;
      private int wcpOAV7contratada_Codigo ;
      private int wcpOAV6ContagemResultado_SistemaCod ;
      private int Gxchartcontrol_Opacity ;
      private int Gxchartcontrol_Backgroundcolor1 ;
      private int Gxchartcontrol_Graphcolor2 ;
      private int lblTbsemdados_Visible ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A74Contrato_Codigo ;
      private int A81Contrato_Quantidade ;
      private int AV10Contrato_Quantidade ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A456ContagemResultado_Codigo ;
      private int GX_I ;
      private int A773ContagemResultadoItem_ContagemCod ;
      private int AV76GXV1 ;
      private int idxLst ;
      private long AV14Filtro_Mes ;
      private long wcpOAV14Filtro_Mes ;
      private long [] AV24mQtdeDmn ;
      private long [] AV22mPFBFM ;
      private long [] AV23mPFBFS ;
      private long AV30QtdALI ;
      private long AV29QtdAIE ;
      private long AV31QtdCE ;
      private long AV32QtdEE ;
      private long AV34QtdSE ;
      private long AV33QtdINM ;
      private long AV28Qtd ;
      private long AV27PF ;
      private long AV49Usado ;
      private long AV50Faltam ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal [] AV19mCTPF ;
      private decimal [] AV25mRTPF ;
      private decimal AV26Percentual ;
      private decimal GXt_decimal1 ;
      private String AV15Graficar ;
      private String wcpOAV15Graficar ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gxchartcontrol_Title ;
      private String Gxchartcontrol_Width ;
      private String Gxchartcontrol_Height ;
      private String Gxchartcontrol_Charttype ;
      private String Gxchartcontrol_X_axistitle ;
      private String Gxchartcontrol_Y_axistitle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lblTbsemdados_Internalname ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String AV8Contratada_Nome ;
      private String Gxchartcontrol_Internalname ;
      private String AV37String ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTbsemdados_Jsonclick ;
      private DateTime Gx_date ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A842Contrato_DataInicioTA ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime AV51Contrato_DataInicioTA ;
      private DateTime AV47DataIni ;
      private DateTime AV48DataFim ;
      private DateTime AV12Data ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Gxchartcontrol_Visible ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A92Contrato_Ativo ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n842Contrato_DataInicioTA ;
      private bool n843Contrato_DataFimTA ;
      private bool BRKEL4 ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool BRKEL6 ;
      private bool n773ContagemResultadoItem_ContagemCod ;
      private bool n775ContagemResultadoItem_Tipo ;
      private bool BRKEL9 ;
      private bool BRKEL12 ;
      private bool BRKEL15 ;
      private bool BRKEL17 ;
      private bool BRKEL19 ;
      private String A775ContagemResultadoItem_Tipo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00EL6_A40Contratada_PessoaCod ;
      private int[] H00EL6_A74Contrato_Codigo ;
      private bool[] H00EL6_n74Contrato_Codigo ;
      private bool[] H00EL6_A92Contrato_Ativo ;
      private int[] H00EL6_A39Contratada_Codigo ;
      private int[] H00EL6_A81Contrato_Quantidade ;
      private String[] H00EL6_A41Contratada_PessoaNom ;
      private bool[] H00EL6_n41Contratada_PessoaNom ;
      private DateTime[] H00EL6_A82Contrato_DataVigenciaInicio ;
      private DateTime[] H00EL6_A83Contrato_DataVigenciaTermino ;
      private DateTime[] H00EL6_A842Contrato_DataInicioTA ;
      private bool[] H00EL6_n842Contrato_DataInicioTA ;
      private DateTime[] H00EL6_A843Contrato_DataFimTA ;
      private bool[] H00EL6_n843Contrato_DataFimTA ;
      private String[] H00EL7_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EL7_n509ContagemrResultado_SistemaSigla ;
      private int[] H00EL7_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EL7_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EL7_A484ContagemResultado_StatusDmn ;
      private bool[] H00EL7_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00EL7_A471ContagemResultado_DataDmn ;
      private int[] H00EL7_A489ContagemResultado_SistemaCod ;
      private bool[] H00EL7_n489ContagemResultado_SistemaCod ;
      private int[] H00EL7_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL7_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL7_A456ContagemResultado_Codigo ;
      private int[] H00EL8_A456ContagemResultado_Codigo ;
      private String[] H00EL8_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EL8_n509ContagemrResultado_SistemaSigla ;
      private int[] H00EL8_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EL8_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EL8_A484ContagemResultado_StatusDmn ;
      private bool[] H00EL8_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00EL8_A471ContagemResultado_DataDmn ;
      private int[] H00EL8_A489ContagemResultado_SistemaCod ;
      private bool[] H00EL8_n489ContagemResultado_SistemaCod ;
      private int[] H00EL8_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL8_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL9_A772ContagemResultadoItem_Codigo ;
      private int[] H00EL9_A773ContagemResultadoItem_ContagemCod ;
      private bool[] H00EL9_n773ContagemResultadoItem_ContagemCod ;
      private String[] H00EL9_A775ContagemResultadoItem_Tipo ;
      private bool[] H00EL9_n775ContagemResultadoItem_Tipo ;
      private int[] H00EL10_A456ContagemResultado_Codigo ;
      private String[] H00EL10_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EL10_n509ContagemrResultado_SistemaSigla ;
      private int[] H00EL10_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EL10_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EL10_A484ContagemResultado_StatusDmn ;
      private bool[] H00EL10_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00EL10_A471ContagemResultado_DataDmn ;
      private int[] H00EL10_A489ContagemResultado_SistemaCod ;
      private bool[] H00EL10_n489ContagemResultado_SistemaCod ;
      private int[] H00EL10_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL10_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL11_A772ContagemResultadoItem_Codigo ;
      private int[] H00EL11_A773ContagemResultadoItem_ContagemCod ;
      private bool[] H00EL11_n773ContagemResultadoItem_ContagemCod ;
      private String[] H00EL11_A775ContagemResultadoItem_Tipo ;
      private bool[] H00EL11_n775ContagemResultadoItem_Tipo ;
      private int[] H00EL12_A456ContagemResultado_Codigo ;
      private String[] H00EL12_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EL12_n509ContagemrResultado_SistemaSigla ;
      private int[] H00EL12_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EL12_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EL12_A484ContagemResultado_StatusDmn ;
      private bool[] H00EL12_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00EL12_A471ContagemResultado_DataDmn ;
      private int[] H00EL12_A489ContagemResultado_SistemaCod ;
      private bool[] H00EL12_n489ContagemResultado_SistemaCod ;
      private int[] H00EL12_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL12_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL13_A772ContagemResultadoItem_Codigo ;
      private int[] H00EL13_A773ContagemResultadoItem_ContagemCod ;
      private bool[] H00EL13_n773ContagemResultadoItem_ContagemCod ;
      private String[] H00EL13_A775ContagemResultadoItem_Tipo ;
      private bool[] H00EL13_n775ContagemResultadoItem_Tipo ;
      private int[] H00EL14_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EL14_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EL14_A484ContagemResultado_StatusDmn ;
      private bool[] H00EL14_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00EL14_A471ContagemResultado_DataDmn ;
      private int[] H00EL14_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL14_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL14_A456ContagemResultado_Codigo ;
      private int[] H00EL15_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EL15_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EL15_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EL15_n509ContagemrResultado_SistemaSigla ;
      private DateTime[] H00EL15_A471ContagemResultado_DataDmn ;
      private int[] H00EL15_A489ContagemResultado_SistemaCod ;
      private bool[] H00EL15_n489ContagemResultado_SistemaCod ;
      private int[] H00EL15_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL15_n490ContagemResultado_ContratadaCod ;
      private String[] H00EL15_A484ContagemResultado_StatusDmn ;
      private bool[] H00EL15_n484ContagemResultado_StatusDmn ;
      private int[] H00EL15_A456ContagemResultado_Codigo ;
      private int[] H00EL16_A456ContagemResultado_Codigo ;
      private int[] H00EL16_A489ContagemResultado_SistemaCod ;
      private bool[] H00EL16_n489ContagemResultado_SistemaCod ;
      private int[] H00EL16_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00EL16_n52Contratada_AreaTrabalhoCod ;
      private String[] H00EL16_A484ContagemResultado_StatusDmn ;
      private bool[] H00EL16_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00EL16_A471ContagemResultado_DataDmn ;
      private int[] H00EL16_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL16_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL17_A772ContagemResultadoItem_Codigo ;
      private int[] H00EL17_A773ContagemResultadoItem_ContagemCod ;
      private bool[] H00EL17_n773ContagemResultadoItem_ContagemCod ;
      private String[] H00EL17_A775ContagemResultadoItem_Tipo ;
      private bool[] H00EL17_n775ContagemResultadoItem_Tipo ;
      private DateTime[] H00EL18_A471ContagemResultado_DataDmn ;
      private int[] H00EL18_A489ContagemResultado_SistemaCod ;
      private bool[] H00EL18_n489ContagemResultado_SistemaCod ;
      private int[] H00EL18_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL18_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL18_A456ContagemResultado_Codigo ;
      private DateTime[] H00EL19_A471ContagemResultado_DataDmn ;
      private int[] H00EL19_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EL19_n490ContagemResultado_ContratadaCod ;
      private int[] H00EL19_A456ContagemResultado_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Grafico_Item ))]
      private IGxCollection AV35SDT_Grafico ;
      private GXWebForm Form ;
      private SdtGxChart GxChartData ;
      private SdtGxChart_Serie GxChartSerie ;
      private SdtSDT_Grafico_Item AV36SDT_Item ;
   }

   public class wp_estatisticapiegrf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00EL6( IGxContext context ,
                                             int AV7contratada_Codigo ,
                                             int A39Contratada_Codigo ,
                                             bool A92Contrato_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [1] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T1.[Contrato_Ativo], T1.[Contratada_Codigo], T1.[Contrato_Quantidade], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_DataVigenciaInicio], T1.[Contrato_DataVigenciaTermino], COALESCE( T2.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA, COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataInicio], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC4] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataFim], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC4] ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Ativo] = 1)";
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int2[0] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00EL7( IGxContext context ,
                                             int AV5AreaTrabalho_Codigo ,
                                             int AV7contratada_Codigo ,
                                             int AV6ContagemResultado_SistemaCod ,
                                             long AV14Filtro_Mes ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A489ContagemResultado_SistemaCod ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             short AV13Filtro_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (YEAR(T1.[ContagemResultado_DataDmn]) = @AV13Filtro_Ano)";
         if ( ! (0==AV5AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV6ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV6ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV14Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV14Filtro_Mes)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla], T1.[ContagemResultado_StatusDmn]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00EL8( IGxContext context ,
                                             int AV5AreaTrabalho_Codigo ,
                                             int AV7contratada_Codigo ,
                                             int AV6ContagemResultado_SistemaCod ,
                                             long AV14Filtro_Mes ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A489ContagemResultado_SistemaCod ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             short AV13Filtro_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [5] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_Codigo], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (YEAR(T1.[ContagemResultado_DataDmn]) = @AV13Filtro_Ano)";
         if ( ! (0==AV5AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ! (0==AV6ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV6ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( ! (0==AV14Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV14Filtro_Mes)";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla], T1.[ContagemResultado_StatusDmn]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_H00EL10( IGxContext context ,
                                              int AV5AreaTrabalho_Codigo ,
                                              int AV7contratada_Codigo ,
                                              int AV6ContagemResultado_SistemaCod ,
                                              long AV14Filtro_Mes ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int A489ContagemResultado_SistemaCod ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              short AV13Filtro_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [5] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_Codigo], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (YEAR(T1.[ContagemResultado_DataDmn]) = @AV13Filtro_Ano)";
         if ( ! (0==AV5AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int8[1] = 1;
         }
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int8[2] = 1;
         }
         if ( ! (0==AV6ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV6ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int8[3] = 1;
         }
         if ( ! (0==AV14Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV14Filtro_Mes)";
         }
         else
         {
            GXv_int8[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla], T1.[ContagemResultado_StatusDmn]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      protected Object[] conditional_H00EL12( IGxContext context ,
                                              int AV5AreaTrabalho_Codigo ,
                                              int AV7contratada_Codigo ,
                                              int AV6ContagemResultado_SistemaCod ,
                                              long AV14Filtro_Mes ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int A489ContagemResultado_SistemaCod ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              short AV13Filtro_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [5] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_Codigo], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (YEAR(T1.[ContagemResultado_DataDmn]) = @AV13Filtro_Ano)";
         if ( ! (0==AV5AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int10[1] = 1;
         }
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int10[2] = 1;
         }
         if ( ! (0==AV6ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV6ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int10[3] = 1;
         }
         if ( ! (0==AV14Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV14Filtro_Mes)";
         }
         else
         {
            GXv_int10[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla], T1.[ContagemResultado_StatusDmn]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      protected Object[] conditional_H00EL14( IGxContext context ,
                                              int AV5AreaTrabalho_Codigo ,
                                              int AV7contratada_Codigo ,
                                              long AV14Filtro_Mes ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              String A484ContagemResultado_StatusDmn ,
                                              short AV13Filtro_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int12 ;
         GXv_int12 = new short [4] ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not T1.[ContagemResultado_StatusDmn] = 'X')";
         scmdbuf = scmdbuf + " and (YEAR(T1.[ContagemResultado_DataDmn]) = @AV13Filtro_Ano)";
         if ( ! (0==AV5AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int12[1] = 1;
         }
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int12[2] = 1;
         }
         if ( ! (0==AV14Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV14Filtro_Mes)";
         }
         else
         {
            GXv_int12[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn]";
         GXv_Object13[0] = scmdbuf;
         GXv_Object13[1] = GXv_int12;
         return GXv_Object13 ;
      }

      protected Object[] conditional_H00EL15( IGxContext context ,
                                              int AV5AreaTrabalho_Codigo ,
                                              int AV7contratada_Codigo ,
                                              int AV6ContagemResultado_SistemaCod ,
                                              long AV14Filtro_Mes ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int A489ContagemResultado_SistemaCod ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              String A484ContagemResultado_StatusDmn ,
                                              short AV13Filtro_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int14 ;
         GXv_int14 = new short [5] ;
         Object[] GXv_Object15 ;
         GXv_Object15 = new Object [2] ;
         scmdbuf = "SELECT T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not T1.[ContagemResultado_StatusDmn] = 'X')";
         scmdbuf = scmdbuf + " and (YEAR(T1.[ContagemResultado_DataDmn]) = @AV13Filtro_Ano)";
         if ( ! (0==AV5AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int14[1] = 1;
         }
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int14[2] = 1;
         }
         if ( ! (0==AV6ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV6ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int14[3] = 1;
         }
         if ( ! (0==AV14Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV14Filtro_Mes)";
         }
         else
         {
            GXv_int14[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Contratada_AreaTrabalhoCod], T2.[Sistema_Sigla], T1.[ContagemResultado_StatusDmn]";
         GXv_Object15[0] = scmdbuf;
         GXv_Object15[1] = GXv_int14;
         return GXv_Object15 ;
      }

      protected Object[] conditional_H00EL16( IGxContext context ,
                                              int AV5AreaTrabalho_Codigo ,
                                              int AV7contratada_Codigo ,
                                              int AV6ContagemResultado_SistemaCod ,
                                              long AV14Filtro_Mes ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int A489ContagemResultado_SistemaCod ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              short AV13Filtro_Ano )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int16 ;
         GXv_int16 = new short [5] ;
         Object[] GXv_Object17 ;
         GXv_Object17 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (YEAR(T1.[ContagemResultado_DataDmn]) = @AV13Filtro_Ano)";
         if ( ! (0==AV5AreaTrabalho_Codigo) )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV5AreaTrabalho_Codigo)";
         }
         else
         {
            GXv_int16[1] = 1;
         }
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int16[2] = 1;
         }
         if ( ! (0==AV6ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV6ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int16[3] = 1;
         }
         if ( ! (0==AV14Filtro_Mes) )
         {
            sWhereString = sWhereString + " and (MONTH(T1.[ContagemResultado_DataDmn]) = @AV14Filtro_Mes)";
         }
         else
         {
            GXv_int16[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_StatusDmn]";
         GXv_Object17[0] = scmdbuf;
         GXv_Object17[1] = GXv_int16;
         return GXv_Object17 ;
      }

      protected Object[] conditional_H00EL18( IGxContext context ,
                                              int AV7contratada_Codigo ,
                                              int AV6ContagemResultado_SistemaCod ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int A489ContagemResultado_SistemaCod ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime AV47DataIni ,
                                              DateTime AV48DataFim )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int18 ;
         GXv_int18 = new short [4] ;
         Object[] GXv_Object19 ;
         GXv_Object19 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_DataDmn], [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContagemResultado_DataDmn] >= @AV47DataIni)";
         scmdbuf = scmdbuf + " and ([ContagemResultado_DataDmn] <= @AV48DataFim)";
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and ([ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int18[2] = 1;
         }
         if ( ! (0==AV6ContagemResultado_SistemaCod) )
         {
            sWhereString = sWhereString + " and ([ContagemResultado_SistemaCod] = @AV6ContagemResultado_SistemaCod)";
         }
         else
         {
            GXv_int18[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_ContratadaCod], [ContagemResultado_SistemaCod]";
         GXv_Object19[0] = scmdbuf;
         GXv_Object19[1] = GXv_int18;
         return GXv_Object19 ;
      }

      protected Object[] conditional_H00EL19( IGxContext context ,
                                              int AV7contratada_Codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime AV47DataIni ,
                                              DateTime AV48DataFim )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int20 ;
         GXv_int20 = new short [3] ;
         Object[] GXv_Object21 ;
         GXv_Object21 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_DataDmn], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContagemResultado_DataDmn] >= @AV47DataIni)";
         scmdbuf = scmdbuf + " and ([ContagemResultado_DataDmn] <= @AV48DataFim)";
         if ( ! (0==AV7contratada_Codigo) )
         {
            sWhereString = sWhereString + " and ([ContagemResultado_ContratadaCod] = @AV7contratada_Codigo)";
         }
         else
         {
            GXv_int20[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_ContratadaCod], [ContagemResultado_DataDmn]";
         GXv_Object21[0] = scmdbuf;
         GXv_Object21[1] = GXv_int20;
         return GXv_Object21 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00EL6(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (bool)dynConstraints[2] );
               case 1 :
                     return conditional_H00EL7(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (long)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (short)dynConstraints[8] );
               case 2 :
                     return conditional_H00EL8(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (long)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (short)dynConstraints[8] );
               case 4 :
                     return conditional_H00EL10(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (long)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (short)dynConstraints[8] );
               case 6 :
                     return conditional_H00EL12(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (long)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (short)dynConstraints[8] );
               case 8 :
                     return conditional_H00EL14(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (long)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] );
               case 9 :
                     return conditional_H00EL15(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (long)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] );
               case 10 :
                     return conditional_H00EL16(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (long)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (DateTime)dynConstraints[7] , (short)dynConstraints[8] );
               case 12 :
                     return conditional_H00EL18(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] );
               case 13 :
                     return conditional_H00EL19(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EL9 ;
          prmH00EL9 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EL11 ;
          prmH00EL11 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EL13 ;
          prmH00EL13 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EL17 ;
          prmH00EL17 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EL6 ;
          prmH00EL6 = new Object[] {
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EL7 ;
          prmH00EL7 = new Object[] {
          new Object[] {"@AV13Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Filtro_Mes",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00EL8 ;
          prmH00EL8 = new Object[] {
          new Object[] {"@AV13Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Filtro_Mes",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00EL10 ;
          prmH00EL10 = new Object[] {
          new Object[] {"@AV13Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Filtro_Mes",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00EL12 ;
          prmH00EL12 = new Object[] {
          new Object[] {"@AV13Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Filtro_Mes",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00EL14 ;
          prmH00EL14 = new Object[] {
          new Object[] {"@AV13Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Filtro_Mes",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00EL15 ;
          prmH00EL15 = new Object[] {
          new Object[] {"@AV13Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Filtro_Mes",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00EL16 ;
          prmH00EL16 = new Object[] {
          new Object[] {"@AV13Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14Filtro_Mes",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00EL18 ;
          prmH00EL18 = new Object[] {
          new Object[] {"@AV47DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EL19 ;
          prmH00EL19 = new Object[] {
          new Object[] {"@AV47DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV7contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EL6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL6,1,0,false,true )
             ,new CursorDef("H00EL7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL7,100,0,true,false )
             ,new CursorDef("H00EL8", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL8,100,0,true,false )
             ,new CursorDef("H00EL9", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Tipo] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_ContagemCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoItem_ContagemCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL9,100,0,false,false )
             ,new CursorDef("H00EL10", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL10,100,0,true,false )
             ,new CursorDef("H00EL11", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Tipo] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_ContagemCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoItem_ContagemCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL11,100,0,false,false )
             ,new CursorDef("H00EL12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL12,100,0,true,false )
             ,new CursorDef("H00EL13", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Tipo] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_ContagemCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoItem_ContagemCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL13,100,0,false,false )
             ,new CursorDef("H00EL14", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL14,100,0,true,false )
             ,new CursorDef("H00EL15", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL15,100,0,true,false )
             ,new CursorDef("H00EL16", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL16,100,0,true,false )
             ,new CursorDef("H00EL17", "SELECT [ContagemResultadoItem_Codigo], [ContagemResultadoItem_ContagemCod], [ContagemResultadoItem_Tipo] FROM [ContagemResultadoItem] WITH (NOLOCK) WHERE [ContagemResultadoItem_ContagemCod] = @ContagemResultado_Codigo ORDER BY [ContagemResultadoItem_ContagemCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL17,100,0,false,false )
             ,new CursorDef("H00EL18", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL18,100,0,true,false )
             ,new CursorDef("H00EL19", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EL19,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 12 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 13 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[7]);
                }
                return;
             case 9 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                return;
             case 10 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                return;
             case 13 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
       }
    }

 }

}
