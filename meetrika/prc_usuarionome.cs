/*
               File: PRC_UsuarioNome
        Description: Retorna o nome do usu�rio logado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:48.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarionome : GXProcedure
   {
      public prc_usuarionome( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarionome( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out String aP0_GAMUser_Name )
      {
         this.AV8GAMUser_Name = "" ;
         initialize();
         executePrivate();
         aP0_GAMUser_Name=this.AV8GAMUser_Name;
      }

      public String executeUdp( )
      {
         this.AV8GAMUser_Name = "" ;
         initialize();
         executePrivate();
         aP0_GAMUser_Name=this.AV8GAMUser_Name;
         return AV8GAMUser_Name ;
      }

      public void executeSubmit( out String aP0_GAMUser_Name )
      {
         prc_usuarionome objprc_usuarionome;
         objprc_usuarionome = new prc_usuarionome();
         objprc_usuarionome.AV8GAMUser_Name = "" ;
         objprc_usuarionome.context.SetSubmitInitialConfig(context);
         objprc_usuarionome.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarionome);
         aP0_GAMUser_Name=this.AV8GAMUser_Name;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarionome)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8GAMUser_Name = new SdtGAMUser(context).getname();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV8GAMUser_Name ;
      private String aP0_GAMUser_Name ;
   }

}
