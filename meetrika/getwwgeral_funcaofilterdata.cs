/*
               File: GetWWGeral_FuncaoFilterData
        Description: Get WWGeral_Funcao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:9.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwgeral_funcaofilterdata : GXProcedure
   {
      public getwwgeral_funcaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwgeral_funcaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
         return AV28OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwgeral_funcaofilterdata objgetwwgeral_funcaofilterdata;
         objgetwwgeral_funcaofilterdata = new getwwgeral_funcaofilterdata();
         objgetwwgeral_funcaofilterdata.AV19DDOName = aP0_DDOName;
         objgetwwgeral_funcaofilterdata.AV17SearchTxt = aP1_SearchTxt;
         objgetwwgeral_funcaofilterdata.AV18SearchTxtTo = aP2_SearchTxtTo;
         objgetwwgeral_funcaofilterdata.AV23OptionsJson = "" ;
         objgetwwgeral_funcaofilterdata.AV26OptionsDescJson = "" ;
         objgetwwgeral_funcaofilterdata.AV28OptionIndexesJson = "" ;
         objgetwwgeral_funcaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwgeral_funcaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwgeral_funcaofilterdata);
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwgeral_funcaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Options = (IGxCollection)(new GxSimpleCollection());
         AV25OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV27OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_FUNCAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_FUNCAO_UONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAO_UONOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV23OptionsJson = AV22Options.ToJSonString(false);
         AV26OptionsDescJson = AV25OptionsDesc.ToJSonString(false);
         AV28OptionIndexesJson = AV27OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get("WWGeral_FuncaoGridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWGeral_FuncaoGridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV30Session.Get("WWGeral_FuncaoGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV33GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME") == 0 )
            {
               AV10TFFuncao_Nome = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME_SEL") == 0 )
            {
               AV11TFFuncao_Nome_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFGRUPOFUNCAO_CODIGO") == 0 )
            {
               AV12TFGrupoFuncao_Codigo = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
               AV13TFGrupoFuncao_Codigo_To = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM") == 0 )
            {
               AV14TFFuncao_UONom = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM_SEL") == 0 )
            {
               AV15TFFuncao_UONom_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFFUNCAO_ATIVO_SEL") == 0 )
            {
               AV16TFFuncao_Ativo_Sel = (short)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV34GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "FUNCAO_NOME") == 0 )
            {
               AV36Funcao_Nome1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 )
            {
               AV37Funcao_UONom1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV34GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "FUNCAO_NOME") == 0 )
               {
                  AV40Funcao_Nome2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 )
               {
                  AV41Funcao_UONom2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV42DynamicFiltersEnabled3 = true;
                  AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(3));
                  AV43DynamicFiltersSelector3 = AV34GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "FUNCAO_NOME") == 0 )
                  {
                     AV44Funcao_Nome3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 )
                  {
                     AV45Funcao_UONom3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAO_NOMEOPTIONS' Routine */
         AV10TFFuncao_Nome = AV17SearchTxt;
         AV11TFFuncao_Nome_Sel = "";
         AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV51WWGeral_FuncaoDS_2_Funcao_nome1 = AV36Funcao_Nome1;
         AV52WWGeral_FuncaoDS_3_Funcao_uonom1 = AV37Funcao_UONom1;
         AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV55WWGeral_FuncaoDS_6_Funcao_nome2 = AV40Funcao_Nome2;
         AV56WWGeral_FuncaoDS_7_Funcao_uonom2 = AV41Funcao_UONom2;
         AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV42DynamicFiltersEnabled3;
         AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV43DynamicFiltersSelector3;
         AV59WWGeral_FuncaoDS_10_Funcao_nome3 = AV44Funcao_Nome3;
         AV60WWGeral_FuncaoDS_11_Funcao_uonom3 = AV45Funcao_UONom3;
         AV61WWGeral_FuncaoDS_12_Tffuncao_nome = AV10TFFuncao_Nome;
         AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV11TFFuncao_Nome_Sel;
         AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV12TFGrupoFuncao_Codigo;
         AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV13TFGrupoFuncao_Codigo_To;
         AV65WWGeral_FuncaoDS_16_Tffuncao_uonom = AV14TFFuncao_UONom;
         AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV15TFFuncao_UONom_Sel;
         AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV16TFFuncao_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                              AV51WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                              AV52WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                              AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                              AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                              AV55WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                              AV56WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                              AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                              AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                              AV59WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                              AV60WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                              AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                              AV61WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                              AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                              AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                              AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                              AV65WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                              AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                              A622Funcao_Nome ,
                                              A636Funcao_UONom ,
                                              A619GrupoFuncao_Codigo ,
                                              A630Funcao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV51WWGeral_FuncaoDS_2_Funcao_nome1 = StringUtil.Concat( StringUtil.RTrim( AV51WWGeral_FuncaoDS_2_Funcao_nome1), "%", "");
         lV52WWGeral_FuncaoDS_3_Funcao_uonom1 = StringUtil.PadR( StringUtil.RTrim( AV52WWGeral_FuncaoDS_3_Funcao_uonom1), 50, "%");
         lV55WWGeral_FuncaoDS_6_Funcao_nome2 = StringUtil.Concat( StringUtil.RTrim( AV55WWGeral_FuncaoDS_6_Funcao_nome2), "%", "");
         lV56WWGeral_FuncaoDS_7_Funcao_uonom2 = StringUtil.PadR( StringUtil.RTrim( AV56WWGeral_FuncaoDS_7_Funcao_uonom2), 50, "%");
         lV59WWGeral_FuncaoDS_10_Funcao_nome3 = StringUtil.Concat( StringUtil.RTrim( AV59WWGeral_FuncaoDS_10_Funcao_nome3), "%", "");
         lV60WWGeral_FuncaoDS_11_Funcao_uonom3 = StringUtil.PadR( StringUtil.RTrim( AV60WWGeral_FuncaoDS_11_Funcao_uonom3), 50, "%");
         lV61WWGeral_FuncaoDS_12_Tffuncao_nome = StringUtil.Concat( StringUtil.RTrim( AV61WWGeral_FuncaoDS_12_Tffuncao_nome), "%", "");
         lV65WWGeral_FuncaoDS_16_Tffuncao_uonom = StringUtil.PadR( StringUtil.RTrim( AV65WWGeral_FuncaoDS_16_Tffuncao_uonom), 50, "%");
         /* Using cursor P00MZ2 */
         pr_default.execute(0, new Object[] {lV51WWGeral_FuncaoDS_2_Funcao_nome1, lV52WWGeral_FuncaoDS_3_Funcao_uonom1, lV55WWGeral_FuncaoDS_6_Funcao_nome2, lV56WWGeral_FuncaoDS_7_Funcao_uonom2, lV59WWGeral_FuncaoDS_10_Funcao_nome3, lV60WWGeral_FuncaoDS_11_Funcao_uonom3, lV61WWGeral_FuncaoDS_12_Tffuncao_nome, AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel, AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo, AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to, lV65WWGeral_FuncaoDS_16_Tffuncao_uonom, AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKMZ2 = false;
            A635Funcao_UOCod = P00MZ2_A635Funcao_UOCod[0];
            n635Funcao_UOCod = P00MZ2_n635Funcao_UOCod[0];
            A622Funcao_Nome = P00MZ2_A622Funcao_Nome[0];
            A630Funcao_Ativo = P00MZ2_A630Funcao_Ativo[0];
            A619GrupoFuncao_Codigo = P00MZ2_A619GrupoFuncao_Codigo[0];
            A636Funcao_UONom = P00MZ2_A636Funcao_UONom[0];
            n636Funcao_UONom = P00MZ2_n636Funcao_UONom[0];
            A621Funcao_Codigo = P00MZ2_A621Funcao_Codigo[0];
            A636Funcao_UONom = P00MZ2_A636Funcao_UONom[0];
            n636Funcao_UONom = P00MZ2_n636Funcao_UONom[0];
            AV29count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00MZ2_A622Funcao_Nome[0], A622Funcao_Nome) == 0 ) )
            {
               BRKMZ2 = false;
               A621Funcao_Codigo = P00MZ2_A621Funcao_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKMZ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A622Funcao_Nome)) )
            {
               AV21Option = A622Funcao_Nome;
               AV24OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!")));
               AV22Options.Add(AV21Option, 0);
               AV25OptionsDesc.Add(AV24OptionDesc, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMZ2 )
            {
               BRKMZ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAO_UONOMOPTIONS' Routine */
         AV14TFFuncao_UONom = AV17SearchTxt;
         AV15TFFuncao_UONom_Sel = "";
         AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV51WWGeral_FuncaoDS_2_Funcao_nome1 = AV36Funcao_Nome1;
         AV52WWGeral_FuncaoDS_3_Funcao_uonom1 = AV37Funcao_UONom1;
         AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV55WWGeral_FuncaoDS_6_Funcao_nome2 = AV40Funcao_Nome2;
         AV56WWGeral_FuncaoDS_7_Funcao_uonom2 = AV41Funcao_UONom2;
         AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV42DynamicFiltersEnabled3;
         AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV43DynamicFiltersSelector3;
         AV59WWGeral_FuncaoDS_10_Funcao_nome3 = AV44Funcao_Nome3;
         AV60WWGeral_FuncaoDS_11_Funcao_uonom3 = AV45Funcao_UONom3;
         AV61WWGeral_FuncaoDS_12_Tffuncao_nome = AV10TFFuncao_Nome;
         AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV11TFFuncao_Nome_Sel;
         AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV12TFGrupoFuncao_Codigo;
         AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV13TFGrupoFuncao_Codigo_To;
         AV65WWGeral_FuncaoDS_16_Tffuncao_uonom = AV14TFFuncao_UONom;
         AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV15TFFuncao_UONom_Sel;
         AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV16TFFuncao_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                              AV51WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                              AV52WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                              AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                              AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                              AV55WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                              AV56WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                              AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                              AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                              AV59WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                              AV60WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                              AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                              AV61WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                              AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                              AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                              AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                              AV65WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                              AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                              A622Funcao_Nome ,
                                              A636Funcao_UONom ,
                                              A619GrupoFuncao_Codigo ,
                                              A630Funcao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV51WWGeral_FuncaoDS_2_Funcao_nome1 = StringUtil.Concat( StringUtil.RTrim( AV51WWGeral_FuncaoDS_2_Funcao_nome1), "%", "");
         lV52WWGeral_FuncaoDS_3_Funcao_uonom1 = StringUtil.PadR( StringUtil.RTrim( AV52WWGeral_FuncaoDS_3_Funcao_uonom1), 50, "%");
         lV55WWGeral_FuncaoDS_6_Funcao_nome2 = StringUtil.Concat( StringUtil.RTrim( AV55WWGeral_FuncaoDS_6_Funcao_nome2), "%", "");
         lV56WWGeral_FuncaoDS_7_Funcao_uonom2 = StringUtil.PadR( StringUtil.RTrim( AV56WWGeral_FuncaoDS_7_Funcao_uonom2), 50, "%");
         lV59WWGeral_FuncaoDS_10_Funcao_nome3 = StringUtil.Concat( StringUtil.RTrim( AV59WWGeral_FuncaoDS_10_Funcao_nome3), "%", "");
         lV60WWGeral_FuncaoDS_11_Funcao_uonom3 = StringUtil.PadR( StringUtil.RTrim( AV60WWGeral_FuncaoDS_11_Funcao_uonom3), 50, "%");
         lV61WWGeral_FuncaoDS_12_Tffuncao_nome = StringUtil.Concat( StringUtil.RTrim( AV61WWGeral_FuncaoDS_12_Tffuncao_nome), "%", "");
         lV65WWGeral_FuncaoDS_16_Tffuncao_uonom = StringUtil.PadR( StringUtil.RTrim( AV65WWGeral_FuncaoDS_16_Tffuncao_uonom), 50, "%");
         /* Using cursor P00MZ3 */
         pr_default.execute(1, new Object[] {lV51WWGeral_FuncaoDS_2_Funcao_nome1, lV52WWGeral_FuncaoDS_3_Funcao_uonom1, lV55WWGeral_FuncaoDS_6_Funcao_nome2, lV56WWGeral_FuncaoDS_7_Funcao_uonom2, lV59WWGeral_FuncaoDS_10_Funcao_nome3, lV60WWGeral_FuncaoDS_11_Funcao_uonom3, lV61WWGeral_FuncaoDS_12_Tffuncao_nome, AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel, AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo, AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to, lV65WWGeral_FuncaoDS_16_Tffuncao_uonom, AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKMZ4 = false;
            A635Funcao_UOCod = P00MZ3_A635Funcao_UOCod[0];
            n635Funcao_UOCod = P00MZ3_n635Funcao_UOCod[0];
            A630Funcao_Ativo = P00MZ3_A630Funcao_Ativo[0];
            A619GrupoFuncao_Codigo = P00MZ3_A619GrupoFuncao_Codigo[0];
            A636Funcao_UONom = P00MZ3_A636Funcao_UONom[0];
            n636Funcao_UONom = P00MZ3_n636Funcao_UONom[0];
            A622Funcao_Nome = P00MZ3_A622Funcao_Nome[0];
            A621Funcao_Codigo = P00MZ3_A621Funcao_Codigo[0];
            A636Funcao_UONom = P00MZ3_A636Funcao_UONom[0];
            n636Funcao_UONom = P00MZ3_n636Funcao_UONom[0];
            AV29count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00MZ3_A635Funcao_UOCod[0] == A635Funcao_UOCod ) )
            {
               BRKMZ4 = false;
               A621Funcao_Codigo = P00MZ3_A621Funcao_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKMZ4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A636Funcao_UONom)) )
            {
               AV21Option = A636Funcao_UONom;
               AV20InsertIndex = 1;
               while ( ( AV20InsertIndex <= AV22Options.Count ) && ( StringUtil.StrCmp(((String)AV22Options.Item(AV20InsertIndex)), AV21Option) < 0 ) )
               {
                  AV20InsertIndex = (int)(AV20InsertIndex+1);
               }
               AV22Options.Add(AV21Option, AV20InsertIndex);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), AV20InsertIndex);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMZ4 )
            {
               BRKMZ4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Options = new GxSimpleCollection();
         AV25OptionsDesc = new GxSimpleCollection();
         AV27OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV33GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncao_Nome = "";
         AV11TFFuncao_Nome_Sel = "";
         AV14TFFuncao_UONom = "";
         AV15TFFuncao_UONom_Sel = "";
         AV34GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV36Funcao_Nome1 = "";
         AV37Funcao_UONom1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV40Funcao_Nome2 = "";
         AV41Funcao_UONom2 = "";
         AV43DynamicFiltersSelector3 = "";
         AV44Funcao_Nome3 = "";
         AV45Funcao_UONom3 = "";
         AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = "";
         AV51WWGeral_FuncaoDS_2_Funcao_nome1 = "";
         AV52WWGeral_FuncaoDS_3_Funcao_uonom1 = "";
         AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = "";
         AV55WWGeral_FuncaoDS_6_Funcao_nome2 = "";
         AV56WWGeral_FuncaoDS_7_Funcao_uonom2 = "";
         AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = "";
         AV59WWGeral_FuncaoDS_10_Funcao_nome3 = "";
         AV60WWGeral_FuncaoDS_11_Funcao_uonom3 = "";
         AV61WWGeral_FuncaoDS_12_Tffuncao_nome = "";
         AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel = "";
         AV65WWGeral_FuncaoDS_16_Tffuncao_uonom = "";
         AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = "";
         scmdbuf = "";
         lV51WWGeral_FuncaoDS_2_Funcao_nome1 = "";
         lV52WWGeral_FuncaoDS_3_Funcao_uonom1 = "";
         lV55WWGeral_FuncaoDS_6_Funcao_nome2 = "";
         lV56WWGeral_FuncaoDS_7_Funcao_uonom2 = "";
         lV59WWGeral_FuncaoDS_10_Funcao_nome3 = "";
         lV60WWGeral_FuncaoDS_11_Funcao_uonom3 = "";
         lV61WWGeral_FuncaoDS_12_Tffuncao_nome = "";
         lV65WWGeral_FuncaoDS_16_Tffuncao_uonom = "";
         A622Funcao_Nome = "";
         A636Funcao_UONom = "";
         P00MZ2_A635Funcao_UOCod = new int[1] ;
         P00MZ2_n635Funcao_UOCod = new bool[] {false} ;
         P00MZ2_A622Funcao_Nome = new String[] {""} ;
         P00MZ2_A630Funcao_Ativo = new bool[] {false} ;
         P00MZ2_A619GrupoFuncao_Codigo = new int[1] ;
         P00MZ2_A636Funcao_UONom = new String[] {""} ;
         P00MZ2_n636Funcao_UONom = new bool[] {false} ;
         P00MZ2_A621Funcao_Codigo = new int[1] ;
         AV21Option = "";
         AV24OptionDesc = "";
         P00MZ3_A635Funcao_UOCod = new int[1] ;
         P00MZ3_n635Funcao_UOCod = new bool[] {false} ;
         P00MZ3_A630Funcao_Ativo = new bool[] {false} ;
         P00MZ3_A619GrupoFuncao_Codigo = new int[1] ;
         P00MZ3_A636Funcao_UONom = new String[] {""} ;
         P00MZ3_n636Funcao_UONom = new bool[] {false} ;
         P00MZ3_A622Funcao_Nome = new String[] {""} ;
         P00MZ3_A621Funcao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwgeral_funcaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00MZ2_A635Funcao_UOCod, P00MZ2_n635Funcao_UOCod, P00MZ2_A622Funcao_Nome, P00MZ2_A630Funcao_Ativo, P00MZ2_A619GrupoFuncao_Codigo, P00MZ2_A636Funcao_UONom, P00MZ2_n636Funcao_UONom, P00MZ2_A621Funcao_Codigo
               }
               , new Object[] {
               P00MZ3_A635Funcao_UOCod, P00MZ3_n635Funcao_UOCod, P00MZ3_A630Funcao_Ativo, P00MZ3_A619GrupoFuncao_Codigo, P00MZ3_A636Funcao_UONom, P00MZ3_n636Funcao_UONom, P00MZ3_A622Funcao_Nome, P00MZ3_A621Funcao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFFuncao_Ativo_Sel ;
      private short AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ;
      private int AV48GXV1 ;
      private int AV12TFGrupoFuncao_Codigo ;
      private int AV13TFGrupoFuncao_Codigo_To ;
      private int AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ;
      private int AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ;
      private int A619GrupoFuncao_Codigo ;
      private int A635Funcao_UOCod ;
      private int A621Funcao_Codigo ;
      private int AV20InsertIndex ;
      private long AV29count ;
      private String AV14TFFuncao_UONom ;
      private String AV15TFFuncao_UONom_Sel ;
      private String AV37Funcao_UONom1 ;
      private String AV41Funcao_UONom2 ;
      private String AV45Funcao_UONom3 ;
      private String AV52WWGeral_FuncaoDS_3_Funcao_uonom1 ;
      private String AV56WWGeral_FuncaoDS_7_Funcao_uonom2 ;
      private String AV60WWGeral_FuncaoDS_11_Funcao_uonom3 ;
      private String AV65WWGeral_FuncaoDS_16_Tffuncao_uonom ;
      private String AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ;
      private String scmdbuf ;
      private String lV52WWGeral_FuncaoDS_3_Funcao_uonom1 ;
      private String lV56WWGeral_FuncaoDS_7_Funcao_uonom2 ;
      private String lV60WWGeral_FuncaoDS_11_Funcao_uonom3 ;
      private String lV65WWGeral_FuncaoDS_16_Tffuncao_uonom ;
      private String A636Funcao_UONom ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV42DynamicFiltersEnabled3 ;
      private bool AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ;
      private bool AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ;
      private bool A630Funcao_Ativo ;
      private bool BRKMZ2 ;
      private bool n635Funcao_UOCod ;
      private bool n636Funcao_UONom ;
      private bool BRKMZ4 ;
      private String AV28OptionIndexesJson ;
      private String AV23OptionsJson ;
      private String AV26OptionsDescJson ;
      private String AV19DDOName ;
      private String AV17SearchTxt ;
      private String AV18SearchTxtTo ;
      private String AV10TFFuncao_Nome ;
      private String AV11TFFuncao_Nome_Sel ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV36Funcao_Nome1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV40Funcao_Nome2 ;
      private String AV43DynamicFiltersSelector3 ;
      private String AV44Funcao_Nome3 ;
      private String AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ;
      private String AV51WWGeral_FuncaoDS_2_Funcao_nome1 ;
      private String AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ;
      private String AV55WWGeral_FuncaoDS_6_Funcao_nome2 ;
      private String AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ;
      private String AV59WWGeral_FuncaoDS_10_Funcao_nome3 ;
      private String AV61WWGeral_FuncaoDS_12_Tffuncao_nome ;
      private String AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel ;
      private String lV51WWGeral_FuncaoDS_2_Funcao_nome1 ;
      private String lV55WWGeral_FuncaoDS_6_Funcao_nome2 ;
      private String lV59WWGeral_FuncaoDS_10_Funcao_nome3 ;
      private String lV61WWGeral_FuncaoDS_12_Tffuncao_nome ;
      private String A622Funcao_Nome ;
      private String AV21Option ;
      private String AV24OptionDesc ;
      private IGxSession AV30Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00MZ2_A635Funcao_UOCod ;
      private bool[] P00MZ2_n635Funcao_UOCod ;
      private String[] P00MZ2_A622Funcao_Nome ;
      private bool[] P00MZ2_A630Funcao_Ativo ;
      private int[] P00MZ2_A619GrupoFuncao_Codigo ;
      private String[] P00MZ2_A636Funcao_UONom ;
      private bool[] P00MZ2_n636Funcao_UONom ;
      private int[] P00MZ2_A621Funcao_Codigo ;
      private int[] P00MZ3_A635Funcao_UOCod ;
      private bool[] P00MZ3_n635Funcao_UOCod ;
      private bool[] P00MZ3_A630Funcao_Ativo ;
      private int[] P00MZ3_A619GrupoFuncao_Codigo ;
      private String[] P00MZ3_A636Funcao_UONom ;
      private bool[] P00MZ3_n636Funcao_UONom ;
      private String[] P00MZ3_A622Funcao_Nome ;
      private int[] P00MZ3_A621Funcao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV33GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV34GridStateDynamicFilter ;
   }

   public class getwwgeral_funcaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00MZ2( IGxContext context ,
                                             String AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                             String AV51WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                             String AV52WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                             bool AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                             String AV55WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                             String AV56WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                             bool AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                             String AV59WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                             String AV60WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                             String AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                             String AV61WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                             int AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                             int AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                             String AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                             String AV65WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                             short AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             int A619GrupoFuncao_Codigo ,
                                             bool A630Funcao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Funcao_UOCod] AS Funcao_UOCod, T1.[Funcao_Nome], T1.[Funcao_Ativo], T1.[GrupoFuncao_Codigo], T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, T1.[Funcao_Codigo] FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         if ( ( StringUtil.StrCmp(AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWGeral_FuncaoDS_2_Funcao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV51WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV51WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWGeral_FuncaoDS_3_Funcao_uonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV52WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV52WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWGeral_FuncaoDS_6_Funcao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV55WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV55WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGeral_FuncaoDS_7_Funcao_uonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV56WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV56WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGeral_FuncaoDS_10_Funcao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV59WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV59WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWGeral_FuncaoDS_11_Funcao_uonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV60WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV60WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_FuncaoDS_12_Tffuncao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV61WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like @lV61WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] = @AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] >= @AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] >= @AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] <= @AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] <= @AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGeral_FuncaoDS_16_Tffuncao_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV65WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV65WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 1)";
            }
         }
         if ( AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Funcao_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00MZ3( IGxContext context ,
                                             String AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                             String AV51WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                             String AV52WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                             bool AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                             String AV55WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                             String AV56WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                             bool AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                             String AV59WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                             String AV60WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                             String AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                             String AV61WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                             int AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                             int AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                             String AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                             String AV65WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                             short AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             int A619GrupoFuncao_Codigo ,
                                             bool A630Funcao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Funcao_UOCod] AS Funcao_UOCod, T1.[Funcao_Ativo], T1.[GrupoFuncao_Codigo], T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, T1.[Funcao_Nome], T1.[Funcao_Codigo] FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         if ( ( StringUtil.StrCmp(AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWGeral_FuncaoDS_2_Funcao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV51WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV51WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV50WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWGeral_FuncaoDS_3_Funcao_uonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV52WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV52WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWGeral_FuncaoDS_6_Funcao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV55WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV55WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV53WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWGeral_FuncaoDS_7_Funcao_uonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV56WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV56WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGeral_FuncaoDS_10_Funcao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV59WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV59WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV57WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV58WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWGeral_FuncaoDS_11_Funcao_uonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV60WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV60WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_FuncaoDS_12_Tffuncao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV61WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like @lV61WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] = @AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] >= @AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] >= @AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] <= @AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] <= @AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGeral_FuncaoDS_16_Tffuncao_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV65WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV65WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 1)";
            }
         }
         if ( AV67WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Funcao_UOCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00MZ2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_P00MZ3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00MZ2 ;
          prmP00MZ2 = new Object[] {
          new Object[] {"@lV51WWGeral_FuncaoDS_2_Funcao_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV52WWGeral_FuncaoDS_3_Funcao_uonom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWGeral_FuncaoDS_6_Funcao_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV56WWGeral_FuncaoDS_7_Funcao_uonom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWGeral_FuncaoDS_10_Funcao_nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV60WWGeral_FuncaoDS_11_Funcao_uonom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWGeral_FuncaoDS_12_Tffuncao_nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV65WWGeral_FuncaoDS_16_Tffuncao_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00MZ3 ;
          prmP00MZ3 = new Object[] {
          new Object[] {"@lV51WWGeral_FuncaoDS_2_Funcao_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV52WWGeral_FuncaoDS_3_Funcao_uonom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWGeral_FuncaoDS_6_Funcao_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV56WWGeral_FuncaoDS_7_Funcao_uonom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWGeral_FuncaoDS_10_Funcao_nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV60WWGeral_FuncaoDS_11_Funcao_uonom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWGeral_FuncaoDS_12_Tffuncao_nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV62WWGeral_FuncaoDS_13_Tffuncao_nome_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV63WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV65WWGeral_FuncaoDS_16_Tffuncao_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWGeral_FuncaoDS_17_Tffuncao_uonom_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00MZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MZ2,100,0,true,false )
             ,new CursorDef("P00MZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MZ3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwgeral_funcaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwgeral_funcaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwgeral_funcaofilterdata") )
          {
             return  ;
          }
          getwwgeral_funcaofilterdata worker = new getwwgeral_funcaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
