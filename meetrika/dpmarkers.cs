/*
               File: DPMarkers
        Description: DPMarkers
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:38.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dpmarkers : GXProcedure
   {
      public dpmarkers( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dpmarkers( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "GeoMarkers.GeoMarkersItem", "GxEv3Up14_Meetrika", "SdtGeoMarkers_GeoMarkersItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "GeoMarkers.GeoMarkersItem", "GxEv3Up14_Meetrika", "SdtGeoMarkers_GeoMarkersItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         dpmarkers objdpmarkers;
         objdpmarkers = new dpmarkers();
         objdpmarkers.Gxm2rootcol = new GxObjectCollection( context, "GeoMarkers.GeoMarkersItem", "GxEv3Up14_Meetrika", "SdtGeoMarkers_GeoMarkersItem", "GeneXus.Programs") ;
         objdpmarkers.context.SetSubmitInitialConfig(context);
         objdpmarkers.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdpmarkers);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dpmarkers)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 120;
         Gxm1geomarkers.gxTpr_Mcity = "Montevideo";
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 100;
         Gxm1geomarkers.gxTpr_Mcity = "Buenos Aires";
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 100;
         Gxm1geomarkers.gxTpr_Mcity = "Rosario";
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 100;
         Gxm1geomarkers.gxTpr_Mcity = "Rio Cuarto";
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 100;
         Gxm1geomarkers.gxTpr_Mcity = "Guayaquil";
         Gxm1geomarkers.gxTpr_Mvaltext = "Cuenca";
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 100;
         Gxm1geomarkers.gxTpr_Mcity = "Santiago de Chile";
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 100;
         Gxm1geomarkers.gxTpr_Mcity = "Bogota";
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         Gxm2rootcol.Add(Gxm1geomarkers, 0);
         Gxm1geomarkers.gxTpr_Mvalnum = 100;
         Gxm1geomarkers.gxTpr_Mcity = "Asuncion";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1geomarkers = new SdtGeoMarkers_GeoMarkersItem(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtGeoMarkers_GeoMarkersItem ))]
      private IGxCollection Gxm2rootcol ;
      private SdtGeoMarkers_GeoMarkersItem Gxm1geomarkers ;
   }

}
