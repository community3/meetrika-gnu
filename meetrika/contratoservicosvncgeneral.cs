/*
               File: ContratoServicosVncGeneral
        Description: Contrato Servicos Vnc General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:21:22.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosvncgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosvncgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosvncgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoSrvVnc_Codigo )
      {
         this.A917ContratoSrvVnc_Codigo = aP0_ContratoSrvVnc_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratoSrvVnc_DoStatusDmn = new GXCombobox();
         cmbContratoSrvVnc_StatusDmn = new GXCombobox();
         cmbContratoSrvVnc_PrestadoraCod = new GXCombobox();
         cmbContratoSrvVnc_SrvVncStatus = new GXCombobox();
         cmbContratoSrvVnc_VincularCom = new GXCombobox();
         cmbContratoSrvVnc_SrvVncRef = new GXCombobox();
         cmbContratoServicosVnc_Gestor = new GXCombobox();
         cmbContratoSrvVnc_NovoStatusDmn = new GXCombobox();
         cmbContratoSrvVnc_NovoRspDmn = new GXCombobox();
         cmbContratoServicosVnc_ClonaSrvOri = new GXCombobox();
         cmbContratoServicosVnc_ClonarLink = new GXCombobox();
         chkContratoServicosVnc_NaoClonaInfo = new GXCheckbox();
         cmbContratoSrvVnc_SemCusto = new GXCombobox();
         chkContratoServicosVnc_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A917ContratoSrvVnc_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A917ContratoSrvVnc_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAGE2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ContratoServicosVncGeneral";
               context.Gx_err = 0;
               WSGE2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Vnc General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120212218");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosvncgeneral.aspx") + "?" + UrlEncode("" +A917ContratoSrvVnc_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA917ContratoSrvVnc_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSRVVNC_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContratoSrvVnc_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_DOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1800ContratoSrvVnc_DoStatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_PRESTADORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_SRVVNCSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1663ContratoSrvVnc_SrvVncStatus, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_VINCULARCOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1745ContratoSrvVnc_VincularCom, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_SRVVNCREF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1821ContratoSrvVnc_SrvVncRef), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_GESTOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1438ContratoServicosVnc_Gestor), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_NOVOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1743ContratoSrvVnc_NovoStatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_NOVORSPDMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_CLONASRVORI", GetSecureSignedToken( sPrefix, A1145ContratoServicosVnc_ClonaSrvOri));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_CLONARLINK", GetSecureSignedToken( sPrefix, A1818ContratoServicosVnc_ClonarLink));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_NAOCLONAINFO", GetSecureSignedToken( sPrefix, A1437ContratoServicosVnc_NaoClonaInfo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSRVVNC_SEMCUSTO", GetSecureSignedToken( sPrefix, A1090ContratoSrvVnc_SemCusto));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSVNC_ATIVO", GetSecureSignedToken( sPrefix, A1453ContratoServicosVnc_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormGE2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosvncgeneral.js", "?20203120212223");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosVncGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Vnc General" ;
      }

      protected void WBGE0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosvncgeneral.aspx");
            }
            wb_table1_2_GE2( true) ;
         }
         else
         {
            wb_table1_2_GE2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GE2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoSrvVnc_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A917ContratoSrvVnc_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoSrvVnc_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoSrvVnc_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosVncGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTGE2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Vnc General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPGE0( ) ;
            }
         }
      }

      protected void WSGE2( )
      {
         STARTGE2( ) ;
         EVTGE2( ) ;
      }

      protected void EVTGE2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11GE2 */
                                    E11GE2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12GE2 */
                                    E12GE2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13GE2 */
                                    E13GE2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14GE2 */
                                    E14GE2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPGE0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGE2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormGE2( ) ;
            }
         }
      }

      protected void PAGE2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContratoSrvVnc_DoStatusDmn.Name = "CONTRATOSRVVNC_DOSTATUSDMN";
            cmbContratoSrvVnc_DoStatusDmn.WebTags = "";
            cmbContratoSrvVnc_DoStatusDmn.addItem("", "(Qualquer um)", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("D", "Rejeitada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_DoStatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_DoStatusDmn.ItemCount > 0 )
            {
               A1800ContratoSrvVnc_DoStatusDmn = cmbContratoSrvVnc_DoStatusDmn.getValidValue(A1800ContratoSrvVnc_DoStatusDmn);
               n1800ContratoSrvVnc_DoStatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_DOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1800ContratoSrvVnc_DoStatusDmn, ""))));
            }
            cmbContratoSrvVnc_StatusDmn.Name = "CONTRATOSRVVNC_STATUSDMN";
            cmbContratoSrvVnc_StatusDmn.WebTags = "";
            cmbContratoSrvVnc_StatusDmn.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("D", "Rejeitada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
            {
               A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
               n1084ContratoSrvVnc_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
            }
            cmbContratoSrvVnc_PrestadoraCod.Name = "CONTRATOSRVVNC_PRESTADORACOD";
            cmbContratoSrvVnc_PrestadoraCod.WebTags = "";
            cmbContratoSrvVnc_PrestadoraCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhuma)", 0);
            if ( cmbContratoSrvVnc_PrestadoraCod.ItemCount > 0 )
            {
               A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cmbContratoSrvVnc_PrestadoraCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0))), "."));
               n1088ContratoSrvVnc_PrestadoraCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_PRESTADORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), "ZZZZZ9")));
            }
            cmbContratoSrvVnc_SrvVncStatus.Name = "CONTRATOSRVVNC_SRVVNCSTATUS";
            cmbContratoSrvVnc_SrvVncStatus.WebTags = "";
            cmbContratoSrvVnc_SrvVncStatus.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("D", "Rejeitada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_SrvVncStatus.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_SrvVncStatus.ItemCount > 0 )
            {
               A1663ContratoSrvVnc_SrvVncStatus = cmbContratoSrvVnc_SrvVncStatus.getValidValue(A1663ContratoSrvVnc_SrvVncStatus);
               n1663ContratoSrvVnc_SrvVncStatus = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1663ContratoSrvVnc_SrvVncStatus, ""))));
            }
            cmbContratoSrvVnc_VincularCom.Name = "CONTRATOSRVVNC_VINCULARCOM";
            cmbContratoSrvVnc_VincularCom.WebTags = "";
            cmbContratoSrvVnc_VincularCom.addItem("D", "Quem dispara", 0);
            cmbContratoSrvVnc_VincularCom.addItem("O", "A Origem", 0);
            if ( cmbContratoSrvVnc_VincularCom.ItemCount > 0 )
            {
               A1745ContratoSrvVnc_VincularCom = cmbContratoSrvVnc_VincularCom.getValidValue(A1745ContratoSrvVnc_VincularCom);
               n1745ContratoSrvVnc_VincularCom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_VINCULARCOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1745ContratoSrvVnc_VincularCom, ""))));
            }
            cmbContratoSrvVnc_SrvVncRef.Name = "CONTRATOSRVVNC_SRVVNCREF";
            cmbContratoSrvVnc_SrvVncRef.WebTags = "";
            cmbContratoSrvVnc_SrvVncRef.addItem("0", "N� da OS Vinculada", 0);
            cmbContratoSrvVnc_SrvVncRef.addItem("1", "Refer�ncia da OS Vinculada", 0);
            if ( cmbContratoSrvVnc_SrvVncRef.ItemCount > 0 )
            {
               A1821ContratoSrvVnc_SrvVncRef = (short)(NumberUtil.Val( cmbContratoSrvVnc_SrvVncRef.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0))), "."));
               n1821ContratoSrvVnc_SrvVncRef = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCREF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1821ContratoSrvVnc_SrvVncRef), "ZZZ9")));
            }
            cmbContratoServicosVnc_Gestor.Name = "CONTRATOSERVICOSVNC_GESTOR";
            cmbContratoServicosVnc_Gestor.WebTags = "";
            cmbContratoServicosVnc_Gestor.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            cmbContratoServicosVnc_Gestor.addItem("700000", "Respons�vel atual", 0);
            cmbContratoServicosVnc_Gestor.addItem("800000", "Criador da OS", 0);
            cmbContratoServicosVnc_Gestor.addItem("900000", "Gestor do contrato", 0);
            if ( cmbContratoServicosVnc_Gestor.ItemCount > 0 )
            {
               A1438ContratoServicosVnc_Gestor = (int)(NumberUtil.Val( cmbContratoServicosVnc_Gestor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0))), "."));
               n1438ContratoServicosVnc_Gestor = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_GESTOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1438ContratoServicosVnc_Gestor), "ZZZZZ9")));
            }
            cmbContratoSrvVnc_NovoStatusDmn.Name = "CONTRATOSRVVNC_NOVOSTATUSDMN";
            cmbContratoSrvVnc_NovoStatusDmn.WebTags = "";
            cmbContratoSrvVnc_NovoStatusDmn.addItem("", "(N�o mudar)", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("B", "Stand by", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("S", "Solicitada", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("E", "Em An�lise", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("A", "Em execu��o", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("R", "Resolvida", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("C", "Conferida", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("D", "Rejeitada", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("H", "Homologada", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("O", "Aceite", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("P", "A Pagar", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("L", "Liquidada", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("X", "Cancelada", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("J", "Planejamento", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContratoSrvVnc_NovoStatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContratoSrvVnc_NovoStatusDmn.ItemCount > 0 )
            {
               A1743ContratoSrvVnc_NovoStatusDmn = cmbContratoSrvVnc_NovoStatusDmn.getValidValue(A1743ContratoSrvVnc_NovoStatusDmn);
               n1743ContratoSrvVnc_NovoStatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1743ContratoSrvVnc_NovoStatusDmn, ""))));
            }
            cmbContratoSrvVnc_NovoRspDmn.Name = "CONTRATOSRVVNC_NOVORSPDMN";
            cmbContratoSrvVnc_NovoRspDmn.WebTags = "";
            cmbContratoSrvVnc_NovoRspDmn.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(N�o mudar)", 0);
            cmbContratoSrvVnc_NovoRspDmn.addItem("910000", "Criador da SS", 0);
            cmbContratoSrvVnc_NovoRspDmn.addItem("920000", ".Criador da OS", 0);
            if ( cmbContratoSrvVnc_NovoRspDmn.ItemCount > 0 )
            {
               A1801ContratoSrvVnc_NovoRspDmn = (int)(NumberUtil.Val( cmbContratoSrvVnc_NovoRspDmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0))), "."));
               n1801ContratoSrvVnc_NovoRspDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVORSPDMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), "ZZZZZ9")));
            }
            cmbContratoServicosVnc_ClonaSrvOri.Name = "CONTRATOSERVICOSVNC_CLONASRVORI";
            cmbContratoServicosVnc_ClonaSrvOri.WebTags = "";
            cmbContratoServicosVnc_ClonaSrvOri.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratoServicosVnc_ClonaSrvOri.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratoServicosVnc_ClonaSrvOri.ItemCount > 0 )
            {
               A1145ContratoServicosVnc_ClonaSrvOri = StringUtil.StrToBool( cmbContratoServicosVnc_ClonaSrvOri.getValidValue(StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri)));
               n1145ContratoServicosVnc_ClonaSrvOri = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONASRVORI", GetSecureSignedToken( sPrefix, A1145ContratoServicosVnc_ClonaSrvOri));
            }
            cmbContratoServicosVnc_ClonarLink.Name = "CONTRATOSERVICOSVNC_CLONARLINK";
            cmbContratoServicosVnc_ClonarLink.WebTags = "";
            cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratoServicosVnc_ClonarLink.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratoServicosVnc_ClonarLink.ItemCount > 0 )
            {
               A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cmbContratoServicosVnc_ClonarLink.getValidValue(StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink)));
               n1818ContratoServicosVnc_ClonarLink = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONARLINK", GetSecureSignedToken( sPrefix, A1818ContratoServicosVnc_ClonarLink));
            }
            chkContratoServicosVnc_NaoClonaInfo.Name = "CONTRATOSERVICOSVNC_NAOCLONAINFO";
            chkContratoServicosVnc_NaoClonaInfo.WebTags = "";
            chkContratoServicosVnc_NaoClonaInfo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicosVnc_NaoClonaInfo_Internalname, "TitleCaption", chkContratoServicosVnc_NaoClonaInfo.Caption);
            chkContratoServicosVnc_NaoClonaInfo.CheckedValue = "false";
            cmbContratoSrvVnc_SemCusto.Name = "CONTRATOSRVVNC_SEMCUSTO";
            cmbContratoSrvVnc_SemCusto.WebTags = "";
            cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContratoSrvVnc_SemCusto.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
            {
               A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
               n1090ContratoSrvVnc_SemCusto = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SEMCUSTO", GetSecureSignedToken( sPrefix, A1090ContratoSrvVnc_SemCusto));
            }
            chkContratoServicosVnc_Ativo.Name = "CONTRATOSERVICOSVNC_ATIVO";
            chkContratoServicosVnc_Ativo.WebTags = "";
            chkContratoServicosVnc_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContratoServicosVnc_Ativo_Internalname, "TitleCaption", chkContratoServicosVnc_Ativo.Caption);
            chkContratoServicosVnc_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoSrvVnc_DoStatusDmn.ItemCount > 0 )
         {
            A1800ContratoSrvVnc_DoStatusDmn = cmbContratoSrvVnc_DoStatusDmn.getValidValue(A1800ContratoSrvVnc_DoStatusDmn);
            n1800ContratoSrvVnc_DoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_DOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1800ContratoSrvVnc_DoStatusDmn, ""))));
         }
         if ( cmbContratoSrvVnc_StatusDmn.ItemCount > 0 )
         {
            A1084ContratoSrvVnc_StatusDmn = cmbContratoSrvVnc_StatusDmn.getValidValue(A1084ContratoSrvVnc_StatusDmn);
            n1084ContratoSrvVnc_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
         }
         if ( cmbContratoSrvVnc_PrestadoraCod.ItemCount > 0 )
         {
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cmbContratoSrvVnc_PrestadoraCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0))), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_PRESTADORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), "ZZZZZ9")));
         }
         if ( cmbContratoSrvVnc_SrvVncStatus.ItemCount > 0 )
         {
            A1663ContratoSrvVnc_SrvVncStatus = cmbContratoSrvVnc_SrvVncStatus.getValidValue(A1663ContratoSrvVnc_SrvVncStatus);
            n1663ContratoSrvVnc_SrvVncStatus = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1663ContratoSrvVnc_SrvVncStatus, ""))));
         }
         if ( cmbContratoSrvVnc_VincularCom.ItemCount > 0 )
         {
            A1745ContratoSrvVnc_VincularCom = cmbContratoSrvVnc_VincularCom.getValidValue(A1745ContratoSrvVnc_VincularCom);
            n1745ContratoSrvVnc_VincularCom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_VINCULARCOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1745ContratoSrvVnc_VincularCom, ""))));
         }
         if ( cmbContratoSrvVnc_SrvVncRef.ItemCount > 0 )
         {
            A1821ContratoSrvVnc_SrvVncRef = (short)(NumberUtil.Val( cmbContratoSrvVnc_SrvVncRef.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0))), "."));
            n1821ContratoSrvVnc_SrvVncRef = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCREF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1821ContratoSrvVnc_SrvVncRef), "ZZZ9")));
         }
         if ( cmbContratoServicosVnc_Gestor.ItemCount > 0 )
         {
            A1438ContratoServicosVnc_Gestor = (int)(NumberUtil.Val( cmbContratoServicosVnc_Gestor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0))), "."));
            n1438ContratoServicosVnc_Gestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_GESTOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1438ContratoServicosVnc_Gestor), "ZZZZZ9")));
         }
         if ( cmbContratoSrvVnc_NovoStatusDmn.ItemCount > 0 )
         {
            A1743ContratoSrvVnc_NovoStatusDmn = cmbContratoSrvVnc_NovoStatusDmn.getValidValue(A1743ContratoSrvVnc_NovoStatusDmn);
            n1743ContratoSrvVnc_NovoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1743ContratoSrvVnc_NovoStatusDmn, ""))));
         }
         if ( cmbContratoSrvVnc_NovoRspDmn.ItemCount > 0 )
         {
            A1801ContratoSrvVnc_NovoRspDmn = (int)(NumberUtil.Val( cmbContratoSrvVnc_NovoRspDmn.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0))), "."));
            n1801ContratoSrvVnc_NovoRspDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVORSPDMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), "ZZZZZ9")));
         }
         if ( cmbContratoServicosVnc_ClonaSrvOri.ItemCount > 0 )
         {
            A1145ContratoServicosVnc_ClonaSrvOri = StringUtil.StrToBool( cmbContratoServicosVnc_ClonaSrvOri.getValidValue(StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri)));
            n1145ContratoServicosVnc_ClonaSrvOri = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONASRVORI", GetSecureSignedToken( sPrefix, A1145ContratoServicosVnc_ClonaSrvOri));
         }
         if ( cmbContratoServicosVnc_ClonarLink.ItemCount > 0 )
         {
            A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cmbContratoServicosVnc_ClonarLink.getValidValue(StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink)));
            n1818ContratoServicosVnc_ClonarLink = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONARLINK", GetSecureSignedToken( sPrefix, A1818ContratoServicosVnc_ClonarLink));
         }
         if ( cmbContratoSrvVnc_SemCusto.ItemCount > 0 )
         {
            A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cmbContratoSrvVnc_SemCusto.getValidValue(StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto)));
            n1090ContratoSrvVnc_SemCusto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SEMCUSTO", GetSecureSignedToken( sPrefix, A1090ContratoSrvVnc_SemCusto));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGE2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ContratoServicosVncGeneral";
         context.Gx_err = 0;
      }

      protected void RFGE2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00GE2 */
            pr_default.execute(0, new Object[] {A917ContratoSrvVnc_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A915ContratoSrvVnc_CntSrvCod = H00GE2_A915ContratoSrvVnc_CntSrvCod[0];
               A921ContratoSrvVnc_ServicoCod = H00GE2_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = H00GE2_n921ContratoSrvVnc_ServicoCod[0];
               A1453ContratoServicosVnc_Ativo = H00GE2_A1453ContratoServicosVnc_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_ATIVO", GetSecureSignedToken( sPrefix, A1453ContratoServicosVnc_Ativo));
               n1453ContratoServicosVnc_Ativo = H00GE2_n1453ContratoServicosVnc_Ativo[0];
               A1090ContratoSrvVnc_SemCusto = H00GE2_A1090ContratoSrvVnc_SemCusto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SEMCUSTO", GetSecureSignedToken( sPrefix, A1090ContratoSrvVnc_SemCusto));
               n1090ContratoSrvVnc_SemCusto = H00GE2_n1090ContratoSrvVnc_SemCusto[0];
               A1437ContratoServicosVnc_NaoClonaInfo = H00GE2_A1437ContratoServicosVnc_NaoClonaInfo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1437ContratoServicosVnc_NaoClonaInfo", A1437ContratoServicosVnc_NaoClonaInfo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_NAOCLONAINFO", GetSecureSignedToken( sPrefix, A1437ContratoServicosVnc_NaoClonaInfo));
               n1437ContratoServicosVnc_NaoClonaInfo = H00GE2_n1437ContratoServicosVnc_NaoClonaInfo[0];
               A1818ContratoServicosVnc_ClonarLink = H00GE2_A1818ContratoServicosVnc_ClonarLink[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONARLINK", GetSecureSignedToken( sPrefix, A1818ContratoServicosVnc_ClonarLink));
               n1818ContratoServicosVnc_ClonarLink = H00GE2_n1818ContratoServicosVnc_ClonarLink[0];
               A1145ContratoServicosVnc_ClonaSrvOri = H00GE2_A1145ContratoServicosVnc_ClonaSrvOri[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONASRVORI", GetSecureSignedToken( sPrefix, A1145ContratoServicosVnc_ClonaSrvOri));
               n1145ContratoServicosVnc_ClonaSrvOri = H00GE2_n1145ContratoServicosVnc_ClonaSrvOri[0];
               A1801ContratoSrvVnc_NovoRspDmn = H00GE2_A1801ContratoSrvVnc_NovoRspDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVORSPDMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), "ZZZZZ9")));
               n1801ContratoSrvVnc_NovoRspDmn = H00GE2_n1801ContratoSrvVnc_NovoRspDmn[0];
               A1743ContratoSrvVnc_NovoStatusDmn = H00GE2_A1743ContratoSrvVnc_NovoStatusDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1743ContratoSrvVnc_NovoStatusDmn, ""))));
               n1743ContratoSrvVnc_NovoStatusDmn = H00GE2_n1743ContratoSrvVnc_NovoStatusDmn[0];
               A1438ContratoServicosVnc_Gestor = H00GE2_A1438ContratoServicosVnc_Gestor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_GESTOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1438ContratoServicosVnc_Gestor), "ZZZZZ9")));
               n1438ContratoServicosVnc_Gestor = H00GE2_n1438ContratoServicosVnc_Gestor[0];
               A1821ContratoSrvVnc_SrvVncRef = H00GE2_A1821ContratoSrvVnc_SrvVncRef[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCREF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1821ContratoSrvVnc_SrvVncRef), "ZZZ9")));
               n1821ContratoSrvVnc_SrvVncRef = H00GE2_n1821ContratoSrvVnc_SrvVncRef[0];
               A1745ContratoSrvVnc_VincularCom = H00GE2_A1745ContratoSrvVnc_VincularCom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_VINCULARCOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1745ContratoSrvVnc_VincularCom, ""))));
               n1745ContratoSrvVnc_VincularCom = H00GE2_n1745ContratoSrvVnc_VincularCom[0];
               A1663ContratoSrvVnc_SrvVncStatus = H00GE2_A1663ContratoSrvVnc_SrvVncStatus[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1663ContratoSrvVnc_SrvVncStatus, ""))));
               n1663ContratoSrvVnc_SrvVncStatus = H00GE2_n1663ContratoSrvVnc_SrvVncStatus[0];
               A1088ContratoSrvVnc_PrestadoraCod = H00GE2_A1088ContratoSrvVnc_PrestadoraCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_PRESTADORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), "ZZZZZ9")));
               n1088ContratoSrvVnc_PrestadoraCod = H00GE2_n1088ContratoSrvVnc_PrestadoraCod[0];
               A1084ContratoSrvVnc_StatusDmn = H00GE2_A1084ContratoSrvVnc_StatusDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
               n1084ContratoSrvVnc_StatusDmn = H00GE2_n1084ContratoSrvVnc_StatusDmn[0];
               A1800ContratoSrvVnc_DoStatusDmn = H00GE2_A1800ContratoSrvVnc_DoStatusDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_DOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1800ContratoSrvVnc_DoStatusDmn, ""))));
               n1800ContratoSrvVnc_DoStatusDmn = H00GE2_n1800ContratoSrvVnc_DoStatusDmn[0];
               A2108ContratoServicosVnc_Descricao = H00GE2_A2108ContratoServicosVnc_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2108ContratoServicosVnc_Descricao", A2108ContratoServicosVnc_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!"))));
               n2108ContratoServicosVnc_Descricao = H00GE2_n2108ContratoServicosVnc_Descricao[0];
               A922ContratoSrvVnc_ServicoSigla = H00GE2_A922ContratoSrvVnc_ServicoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
               n922ContratoSrvVnc_ServicoSigla = H00GE2_n922ContratoSrvVnc_ServicoSigla[0];
               A921ContratoSrvVnc_ServicoCod = H00GE2_A921ContratoSrvVnc_ServicoCod[0];
               n921ContratoSrvVnc_ServicoCod = H00GE2_n921ContratoSrvVnc_ServicoCod[0];
               A922ContratoSrvVnc_ServicoSigla = H00GE2_A922ContratoSrvVnc_ServicoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
               n922ContratoSrvVnc_ServicoSigla = H00GE2_n922ContratoSrvVnc_ServicoSigla[0];
               /* Execute user event: E12GE2 */
               E12GE2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBGE0( ) ;
         }
      }

      protected void STRUPGE0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ContratoServicosVncGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11GE2 */
         E11GE2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A922ContratoSrvVnc_ServicoSigla = StringUtil.Upper( cgiGet( edtContratoSrvVnc_ServicoSigla_Internalname));
            n922ContratoSrvVnc_ServicoSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A922ContratoSrvVnc_ServicoSigla", A922ContratoSrvVnc_ServicoSigla);
            A2108ContratoServicosVnc_Descricao = StringUtil.Upper( cgiGet( edtContratoServicosVnc_Descricao_Internalname));
            n2108ContratoServicosVnc_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2108ContratoServicosVnc_Descricao", A2108ContratoServicosVnc_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!"))));
            cmbContratoSrvVnc_DoStatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_DoStatusDmn_Internalname);
            A1800ContratoSrvVnc_DoStatusDmn = cgiGet( cmbContratoSrvVnc_DoStatusDmn_Internalname);
            n1800ContratoSrvVnc_DoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1800ContratoSrvVnc_DoStatusDmn", A1800ContratoSrvVnc_DoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_DOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1800ContratoSrvVnc_DoStatusDmn, ""))));
            cmbContratoSrvVnc_StatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
            A1084ContratoSrvVnc_StatusDmn = cgiGet( cmbContratoSrvVnc_StatusDmn_Internalname);
            n1084ContratoSrvVnc_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1084ContratoSrvVnc_StatusDmn", A1084ContratoSrvVnc_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_STATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1084ContratoSrvVnc_StatusDmn, ""))));
            cmbContratoSrvVnc_PrestadoraCod.CurrentValue = cgiGet( cmbContratoSrvVnc_PrestadoraCod_Internalname);
            A1088ContratoSrvVnc_PrestadoraCod = (int)(NumberUtil.Val( cgiGet( cmbContratoSrvVnc_PrestadoraCod_Internalname), "."));
            n1088ContratoSrvVnc_PrestadoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1088ContratoSrvVnc_PrestadoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_PRESTADORACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), "ZZZZZ9")));
            cmbContratoSrvVnc_SrvVncStatus.CurrentValue = cgiGet( cmbContratoSrvVnc_SrvVncStatus_Internalname);
            A1663ContratoSrvVnc_SrvVncStatus = cgiGet( cmbContratoSrvVnc_SrvVncStatus_Internalname);
            n1663ContratoSrvVnc_SrvVncStatus = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1663ContratoSrvVnc_SrvVncStatus", A1663ContratoSrvVnc_SrvVncStatus);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1663ContratoSrvVnc_SrvVncStatus, ""))));
            cmbContratoSrvVnc_VincularCom.CurrentValue = cgiGet( cmbContratoSrvVnc_VincularCom_Internalname);
            A1745ContratoSrvVnc_VincularCom = cgiGet( cmbContratoSrvVnc_VincularCom_Internalname);
            n1745ContratoSrvVnc_VincularCom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1745ContratoSrvVnc_VincularCom", A1745ContratoSrvVnc_VincularCom);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_VINCULARCOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1745ContratoSrvVnc_VincularCom, ""))));
            cmbContratoSrvVnc_SrvVncRef.CurrentValue = cgiGet( cmbContratoSrvVnc_SrvVncRef_Internalname);
            A1821ContratoSrvVnc_SrvVncRef = (short)(NumberUtil.Val( cgiGet( cmbContratoSrvVnc_SrvVncRef_Internalname), "."));
            n1821ContratoSrvVnc_SrvVncRef = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1821ContratoSrvVnc_SrvVncRef", StringUtil.LTrim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SRVVNCREF", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1821ContratoSrvVnc_SrvVncRef), "ZZZ9")));
            cmbContratoServicosVnc_Gestor.CurrentValue = cgiGet( cmbContratoServicosVnc_Gestor_Internalname);
            A1438ContratoServicosVnc_Gestor = (int)(NumberUtil.Val( cgiGet( cmbContratoServicosVnc_Gestor_Internalname), "."));
            n1438ContratoServicosVnc_Gestor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1438ContratoServicosVnc_Gestor", StringUtil.LTrim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_GESTOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1438ContratoServicosVnc_Gestor), "ZZZZZ9")));
            cmbContratoSrvVnc_NovoStatusDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_NovoStatusDmn_Internalname);
            A1743ContratoSrvVnc_NovoStatusDmn = cgiGet( cmbContratoSrvVnc_NovoStatusDmn_Internalname);
            n1743ContratoSrvVnc_NovoStatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1743ContratoSrvVnc_NovoStatusDmn", A1743ContratoSrvVnc_NovoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVOSTATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1743ContratoSrvVnc_NovoStatusDmn, ""))));
            cmbContratoSrvVnc_NovoRspDmn.CurrentValue = cgiGet( cmbContratoSrvVnc_NovoRspDmn_Internalname);
            A1801ContratoSrvVnc_NovoRspDmn = (int)(NumberUtil.Val( cgiGet( cmbContratoSrvVnc_NovoRspDmn_Internalname), "."));
            n1801ContratoSrvVnc_NovoRspDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1801ContratoSrvVnc_NovoRspDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_NOVORSPDMN", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), "ZZZZZ9")));
            cmbContratoServicosVnc_ClonaSrvOri.CurrentValue = cgiGet( cmbContratoServicosVnc_ClonaSrvOri_Internalname);
            A1145ContratoServicosVnc_ClonaSrvOri = StringUtil.StrToBool( cgiGet( cmbContratoServicosVnc_ClonaSrvOri_Internalname));
            n1145ContratoServicosVnc_ClonaSrvOri = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1145ContratoServicosVnc_ClonaSrvOri", A1145ContratoServicosVnc_ClonaSrvOri);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONASRVORI", GetSecureSignedToken( sPrefix, A1145ContratoServicosVnc_ClonaSrvOri));
            cmbContratoServicosVnc_ClonarLink.CurrentValue = cgiGet( cmbContratoServicosVnc_ClonarLink_Internalname);
            A1818ContratoServicosVnc_ClonarLink = StringUtil.StrToBool( cgiGet( cmbContratoServicosVnc_ClonarLink_Internalname));
            n1818ContratoServicosVnc_ClonarLink = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1818ContratoServicosVnc_ClonarLink", A1818ContratoServicosVnc_ClonarLink);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_CLONARLINK", GetSecureSignedToken( sPrefix, A1818ContratoServicosVnc_ClonarLink));
            A1437ContratoServicosVnc_NaoClonaInfo = StringUtil.StrToBool( cgiGet( chkContratoServicosVnc_NaoClonaInfo_Internalname));
            n1437ContratoServicosVnc_NaoClonaInfo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1437ContratoServicosVnc_NaoClonaInfo", A1437ContratoServicosVnc_NaoClonaInfo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_NAOCLONAINFO", GetSecureSignedToken( sPrefix, A1437ContratoServicosVnc_NaoClonaInfo));
            cmbContratoSrvVnc_SemCusto.CurrentValue = cgiGet( cmbContratoSrvVnc_SemCusto_Internalname);
            A1090ContratoSrvVnc_SemCusto = StringUtil.StrToBool( cgiGet( cmbContratoSrvVnc_SemCusto_Internalname));
            n1090ContratoSrvVnc_SemCusto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1090ContratoSrvVnc_SemCusto", A1090ContratoSrvVnc_SemCusto);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSRVVNC_SEMCUSTO", GetSecureSignedToken( sPrefix, A1090ContratoSrvVnc_SemCusto));
            A1453ContratoServicosVnc_Ativo = StringUtil.StrToBool( cgiGet( chkContratoServicosVnc_Ativo_Internalname));
            n1453ContratoServicosVnc_Ativo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1453ContratoServicosVnc_Ativo", A1453ContratoServicosVnc_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSVNC_ATIVO", GetSecureSignedToken( sPrefix, A1453ContratoServicosVnc_Ativo));
            /* Read saved values. */
            wcpOA917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA917ContratoSrvVnc_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11GE2 */
         E11GE2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11GE2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12GE2( )
      {
         /* Load Routine */
         edtContratoSrvVnc_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoSrvVnc_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoSrvVnc_Codigo_Visible), 5, 0)));
      }

      protected void E13GE2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A917ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV12ContratoSrvVnc_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E14GE2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoservicosvnc.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A917ContratoSrvVnc_Codigo) + "," + UrlEncode("" +AV12ContratoSrvVnc_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosVnc";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoSrvVnc_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoSrvVnc_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_GE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_GE2( true) ;
         }
         else
         {
            wb_table2_8_GE2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_GE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_93_GE2( true) ;
         }
         else
         {
            wb_table3_93_GE2( false) ;
         }
         return  ;
      }

      protected void wb_table3_93_GE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GE2e( true) ;
         }
         else
         {
            wb_table1_2_GE2e( false) ;
         }
      }

      protected void wb_table3_93_GE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_93_GE2e( true) ;
         }
         else
         {
            wb_table3_93_GE2e( false) ;
         }
      }

      protected void wb_table2_8_GE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_servicosigla_Internalname, "Servi�o", "", "", lblTextblockcontratosrvvnc_servicosigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoSrvVnc_ServicoSigla_Internalname, StringUtil.RTrim( A922ContratoSrvVnc_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A922ContratoSrvVnc_ServicoSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoSrvVnc_ServicoSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_descricao_Internalname, "Descri��o", "", "", lblTextblockcontratoservicosvnc_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosVnc_Descricao_Internalname, A2108ContratoServicosVnc_Descricao, StringUtil.RTrim( context.localUtil.Format( A2108ContratoServicosVnc_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosVnc_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_dostatusdmn_Internalname, "Do Status", "", "", lblTextblockcontratosrvvnc_dostatusdmn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_DoStatusDmn, cmbContratoSrvVnc_DoStatusDmn_Internalname, StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn), 1, cmbContratoSrvVnc_DoStatusDmn_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_DoStatusDmn.CurrentValue = StringUtil.RTrim( A1800ContratoSrvVnc_DoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_DoStatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_DoStatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_statusdmn_Internalname, "Para o Status", "", "", lblTextblockcontratosrvvnc_statusdmn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_StatusDmn, cmbContratoSrvVnc_StatusDmn_Internalname, StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn), 1, cmbContratoSrvVnc_StatusDmn_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_StatusDmn.CurrentValue = StringUtil.RTrim( A1084ContratoSrvVnc_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_StatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_StatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_prestadoracod_Internalname, "A��o", "", "", lblTextblockcontratosrvvnc_prestadoracod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_PrestadoraCod, cmbContratoSrvVnc_PrestadoraCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0)), 1, cmbContratoSrvVnc_PrestadoraCod_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_PrestadoraCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1088ContratoSrvVnc_PrestadoraCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_PrestadoraCod_Internalname, "Values", (String)(cmbContratoSrvVnc_PrestadoraCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_srvvncstatus_Internalname, "Com o Status", "", "", lblTextblockcontratosrvvnc_srvvncstatus_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_SrvVncStatus, cmbContratoSrvVnc_SrvVncStatus_Internalname, StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus), 1, cmbContratoSrvVnc_SrvVncStatus_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_SrvVncStatus.CurrentValue = StringUtil.RTrim( A1663ContratoSrvVnc_SrvVncStatus);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_SrvVncStatus_Internalname, "Values", (String)(cmbContratoSrvVnc_SrvVncStatus.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_vincularcom_Internalname, "Vinculada com", "", "", lblTextblockcontratosrvvnc_vincularcom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_VincularCom, cmbContratoSrvVnc_VincularCom_Internalname, StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom), 1, cmbContratoSrvVnc_VincularCom_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_VincularCom.CurrentValue = StringUtil.RTrim( A1745ContratoSrvVnc_VincularCom);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_VincularCom_Internalname, "Values", (String)(cmbContratoSrvVnc_VincularCom.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_srvvncref_Internalname, "Refer�ncia", "", "", lblTextblockcontratosrvvnc_srvvncref_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_SrvVncRef, cmbContratoSrvVnc_SrvVncRef_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0)), 1, cmbContratoSrvVnc_SrvVncRef_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_SrvVncRef.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1821ContratoSrvVnc_SrvVncRef), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_SrvVncRef_Internalname, "Values", (String)(cmbContratoSrvVnc_SrvVncRef.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_gestor_Internalname, "Emissor", "", "", lblTextblockcontratoservicosvnc_gestor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosVnc_Gestor, cmbContratoServicosVnc_Gestor_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0)), 1, cmbContratoServicosVnc_Gestor_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoServicosVnc_Gestor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1438ContratoServicosVnc_Gestor), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosVnc_Gestor_Internalname, "Values", (String)(cmbContratoServicosVnc_Gestor.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_novostatusdmn_Internalname, "Deixando-a no Status", "", "", lblTextblockcontratosrvvnc_novostatusdmn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_NovoStatusDmn, cmbContratoSrvVnc_NovoStatusDmn_Internalname, StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn), 1, cmbContratoSrvVnc_NovoStatusDmn_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_NovoStatusDmn.CurrentValue = StringUtil.RTrim( A1743ContratoSrvVnc_NovoStatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_NovoStatusDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_NovoStatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_novorspdmn_Internalname, "Deixando-a com", "", "", lblTextblockcontratosrvvnc_novorspdmn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_NovoRspDmn, cmbContratoSrvVnc_NovoRspDmn_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0)), 1, cmbContratoSrvVnc_NovoRspDmn_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_NovoRspDmn.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1801ContratoSrvVnc_NovoRspDmn), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_NovoRspDmn_Internalname, "Values", (String)(cmbContratoSrvVnc_NovoRspDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_clonasrvori_Internalname, "Clonar resultado", "", "", lblTextblockcontratoservicosvnc_clonasrvori_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosVnc_ClonaSrvOri, cmbContratoServicosVnc_ClonaSrvOri_Internalname, StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri), 1, cmbContratoServicosVnc_ClonaSrvOri_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoServicosVnc_ClonaSrvOri.CurrentValue = StringUtil.BoolToStr( A1145ContratoServicosVnc_ClonaSrvOri);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosVnc_ClonaSrvOri_Internalname, "Values", (String)(cmbContratoServicosVnc_ClonaSrvOri.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_clonarlink_Internalname, "Clonar link", "", "", lblTextblockcontratoservicosvnc_clonarlink_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosVnc_ClonarLink, cmbContratoServicosVnc_ClonarLink_Internalname, StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink), 1, cmbContratoServicosVnc_ClonarLink_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoServicosVnc_ClonarLink.CurrentValue = StringUtil.BoolToStr( A1818ContratoServicosVnc_ClonarLink);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosVnc_ClonarLink_Internalname, "Values", (String)(cmbContratoServicosVnc_ClonarLink.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_naoclonainfo_Internalname, "N�o clonar info", "", "", lblTextblockcontratoservicosvnc_naoclonainfo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table4_74_GE2( true) ;
         }
         else
         {
            wb_table4_74_GE2( false) ;
         }
         return  ;
      }

      protected void wb_table4_74_GE2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratosrvvnc_semcusto_Internalname, "Sem custo", "", "", lblTextblockcontratosrvvnc_semcusto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoSrvVnc_SemCusto, cmbContratoSrvVnc_SemCusto_Internalname, StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto), 1, cmbContratoSrvVnc_SemCusto_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosVncGeneral.htm");
            cmbContratoSrvVnc_SemCusto.CurrentValue = StringUtil.BoolToStr( A1090ContratoSrvVnc_SemCusto);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoSrvVnc_SemCusto_Internalname, "Values", (String)(cmbContratoSrvVnc_SemCusto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosvnc_ativo_Internalname, "Ativa?", "", "", lblTextblockcontratoservicosvnc_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicosVnc_Ativo_Internalname, StringUtil.BoolToStr( A1453ContratoServicosVnc_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_GE2e( true) ;
         }
         else
         {
            wb_table2_8_GE2e( false) ;
         }
      }

      protected void wb_table4_74_GE2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname, tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicosVnc_NaoClonaInfo_Internalname, StringUtil.BoolToStr( A1437ContratoServicosVnc_NaoClonaInfo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosvnc_naoclonainfo_righttext_Internalname, "�(T�tulo, observa��o, evid�ncias)", "", "", lblContratoservicosvnc_naoclonainfo_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosVncGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_74_GE2e( true) ;
         }
         else
         {
            wb_table4_74_GE2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A917ContratoSrvVnc_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGE2( ) ;
         WSGE2( ) ;
         WEGE2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA917ContratoSrvVnc_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAGE2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosvncgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAGE2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A917ContratoSrvVnc_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
         }
         wcpOA917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA917ContratoSrvVnc_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A917ContratoSrvVnc_Codigo != wcpOA917ContratoSrvVnc_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA917ContratoSrvVnc_Codigo = A917ContratoSrvVnc_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA917ContratoSrvVnc_Codigo = cgiGet( sPrefix+"A917ContratoSrvVnc_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA917ContratoSrvVnc_Codigo) > 0 )
         {
            A917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA917ContratoSrvVnc_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A917ContratoSrvVnc_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0)));
         }
         else
         {
            A917ContratoSrvVnc_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A917ContratoSrvVnc_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAGE2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSGE2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSGE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A917ContratoSrvVnc_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A917ContratoSrvVnc_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA917ContratoSrvVnc_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A917ContratoSrvVnc_Codigo_CTRL", StringUtil.RTrim( sCtrlA917ContratoSrvVnc_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEGE2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120212334");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosvncgeneral.js", "?20203120212334");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratosrvvnc_servicosigla_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_SERVICOSIGLA";
         edtContratoSrvVnc_ServicoSigla_Internalname = sPrefix+"CONTRATOSRVVNC_SERVICOSIGLA";
         lblTextblockcontratoservicosvnc_descricao_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSVNC_DESCRICAO";
         edtContratoServicosVnc_Descricao_Internalname = sPrefix+"CONTRATOSERVICOSVNC_DESCRICAO";
         lblTextblockcontratosrvvnc_dostatusdmn_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_DOSTATUSDMN";
         cmbContratoSrvVnc_DoStatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_DOSTATUSDMN";
         lblTextblockcontratosrvvnc_statusdmn_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_STATUSDMN";
         cmbContratoSrvVnc_StatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_STATUSDMN";
         lblTextblockcontratosrvvnc_prestadoracod_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_PRESTADORACOD";
         cmbContratoSrvVnc_PrestadoraCod_Internalname = sPrefix+"CONTRATOSRVVNC_PRESTADORACOD";
         lblTextblockcontratosrvvnc_srvvncstatus_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_SRVVNCSTATUS";
         cmbContratoSrvVnc_SrvVncStatus_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCSTATUS";
         lblTextblockcontratosrvvnc_vincularcom_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_VINCULARCOM";
         cmbContratoSrvVnc_VincularCom_Internalname = sPrefix+"CONTRATOSRVVNC_VINCULARCOM";
         lblTextblockcontratosrvvnc_srvvncref_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_SRVVNCREF";
         cmbContratoSrvVnc_SrvVncRef_Internalname = sPrefix+"CONTRATOSRVVNC_SRVVNCREF";
         lblTextblockcontratoservicosvnc_gestor_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSVNC_GESTOR";
         cmbContratoServicosVnc_Gestor_Internalname = sPrefix+"CONTRATOSERVICOSVNC_GESTOR";
         lblTextblockcontratosrvvnc_novostatusdmn_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_NOVOSTATUSDMN";
         cmbContratoSrvVnc_NovoStatusDmn_Internalname = sPrefix+"CONTRATOSRVVNC_NOVOSTATUSDMN";
         lblTextblockcontratosrvvnc_novorspdmn_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_NOVORSPDMN";
         cmbContratoSrvVnc_NovoRspDmn_Internalname = sPrefix+"CONTRATOSRVVNC_NOVORSPDMN";
         lblTextblockcontratoservicosvnc_clonasrvori_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSVNC_CLONASRVORI";
         cmbContratoServicosVnc_ClonaSrvOri_Internalname = sPrefix+"CONTRATOSERVICOSVNC_CLONASRVORI";
         lblTextblockcontratoservicosvnc_clonarlink_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSVNC_CLONARLINK";
         cmbContratoServicosVnc_ClonarLink_Internalname = sPrefix+"CONTRATOSERVICOSVNC_CLONARLINK";
         lblTextblockcontratoservicosvnc_naoclonainfo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSVNC_NAOCLONAINFO";
         chkContratoServicosVnc_NaoClonaInfo_Internalname = sPrefix+"CONTRATOSERVICOSVNC_NAOCLONAINFO";
         lblContratoservicosvnc_naoclonainfo_righttext_Internalname = sPrefix+"CONTRATOSERVICOSVNC_NAOCLONAINFO_RIGHTTEXT";
         tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname = sPrefix+"TABLEMERGEDCONTRATOSERVICOSVNC_NAOCLONAINFO";
         lblTextblockcontratosrvvnc_semcusto_Internalname = sPrefix+"TEXTBLOCKCONTRATOSRVVNC_SEMCUSTO";
         cmbContratoSrvVnc_SemCusto_Internalname = sPrefix+"CONTRATOSRVVNC_SEMCUSTO";
         lblTextblockcontratoservicosvnc_ativo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSVNC_ATIVO";
         chkContratoServicosVnc_Ativo_Internalname = sPrefix+"CONTRATOSERVICOSVNC_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratoSrvVnc_Codigo_Internalname = sPrefix+"CONTRATOSRVVNC_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContratoSrvVnc_SemCusto_Jsonclick = "";
         cmbContratoServicosVnc_ClonarLink_Jsonclick = "";
         cmbContratoServicosVnc_ClonaSrvOri_Jsonclick = "";
         cmbContratoSrvVnc_NovoRspDmn_Jsonclick = "";
         cmbContratoSrvVnc_NovoStatusDmn_Jsonclick = "";
         cmbContratoServicosVnc_Gestor_Jsonclick = "";
         cmbContratoSrvVnc_SrvVncRef_Jsonclick = "";
         cmbContratoSrvVnc_VincularCom_Jsonclick = "";
         cmbContratoSrvVnc_SrvVncStatus_Jsonclick = "";
         cmbContratoSrvVnc_PrestadoraCod_Jsonclick = "";
         cmbContratoSrvVnc_StatusDmn_Jsonclick = "";
         cmbContratoSrvVnc_DoStatusDmn_Jsonclick = "";
         edtContratoServicosVnc_Descricao_Jsonclick = "";
         edtContratoSrvVnc_ServicoSigla_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         chkContratoServicosVnc_Ativo.Caption = "";
         chkContratoServicosVnc_NaoClonaInfo.Caption = "";
         edtContratoSrvVnc_Codigo_Jsonclick = "";
         edtContratoSrvVnc_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13GE2',iparms:[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14GE2',iparms:[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A2108ContratoServicosVnc_Descricao = "";
         A1800ContratoSrvVnc_DoStatusDmn = "";
         A1084ContratoSrvVnc_StatusDmn = "";
         A1663ContratoSrvVnc_SrvVncStatus = "";
         A1745ContratoSrvVnc_VincularCom = "";
         A1743ContratoSrvVnc_NovoStatusDmn = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00GE2_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         H00GE2_A921ContratoSrvVnc_ServicoCod = new int[1] ;
         H00GE2_n921ContratoSrvVnc_ServicoCod = new bool[] {false} ;
         H00GE2_A917ContratoSrvVnc_Codigo = new int[1] ;
         H00GE2_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00GE2_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         H00GE2_A1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         H00GE2_n1090ContratoSrvVnc_SemCusto = new bool[] {false} ;
         H00GE2_A1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         H00GE2_n1437ContratoServicosVnc_NaoClonaInfo = new bool[] {false} ;
         H00GE2_A1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         H00GE2_n1818ContratoServicosVnc_ClonarLink = new bool[] {false} ;
         H00GE2_A1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         H00GE2_n1145ContratoServicosVnc_ClonaSrvOri = new bool[] {false} ;
         H00GE2_A1801ContratoSrvVnc_NovoRspDmn = new int[1] ;
         H00GE2_n1801ContratoSrvVnc_NovoRspDmn = new bool[] {false} ;
         H00GE2_A1743ContratoSrvVnc_NovoStatusDmn = new String[] {""} ;
         H00GE2_n1743ContratoSrvVnc_NovoStatusDmn = new bool[] {false} ;
         H00GE2_A1438ContratoServicosVnc_Gestor = new int[1] ;
         H00GE2_n1438ContratoServicosVnc_Gestor = new bool[] {false} ;
         H00GE2_A1821ContratoSrvVnc_SrvVncRef = new short[1] ;
         H00GE2_n1821ContratoSrvVnc_SrvVncRef = new bool[] {false} ;
         H00GE2_A1745ContratoSrvVnc_VincularCom = new String[] {""} ;
         H00GE2_n1745ContratoSrvVnc_VincularCom = new bool[] {false} ;
         H00GE2_A1663ContratoSrvVnc_SrvVncStatus = new String[] {""} ;
         H00GE2_n1663ContratoSrvVnc_SrvVncStatus = new bool[] {false} ;
         H00GE2_A1088ContratoSrvVnc_PrestadoraCod = new int[1] ;
         H00GE2_n1088ContratoSrvVnc_PrestadoraCod = new bool[] {false} ;
         H00GE2_A1084ContratoSrvVnc_StatusDmn = new String[] {""} ;
         H00GE2_n1084ContratoSrvVnc_StatusDmn = new bool[] {false} ;
         H00GE2_A1800ContratoSrvVnc_DoStatusDmn = new String[] {""} ;
         H00GE2_n1800ContratoSrvVnc_DoStatusDmn = new bool[] {false} ;
         H00GE2_A2108ContratoServicosVnc_Descricao = new String[] {""} ;
         H00GE2_n2108ContratoServicosVnc_Descricao = new bool[] {false} ;
         H00GE2_A922ContratoSrvVnc_ServicoSigla = new String[] {""} ;
         H00GE2_n922ContratoSrvVnc_ServicoSigla = new bool[] {false} ;
         A922ContratoSrvVnc_ServicoSigla = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratosrvvnc_servicosigla_Jsonclick = "";
         lblTextblockcontratoservicosvnc_descricao_Jsonclick = "";
         lblTextblockcontratosrvvnc_dostatusdmn_Jsonclick = "";
         lblTextblockcontratosrvvnc_statusdmn_Jsonclick = "";
         lblTextblockcontratosrvvnc_prestadoracod_Jsonclick = "";
         lblTextblockcontratosrvvnc_srvvncstatus_Jsonclick = "";
         lblTextblockcontratosrvvnc_vincularcom_Jsonclick = "";
         lblTextblockcontratosrvvnc_srvvncref_Jsonclick = "";
         lblTextblockcontratoservicosvnc_gestor_Jsonclick = "";
         lblTextblockcontratosrvvnc_novostatusdmn_Jsonclick = "";
         lblTextblockcontratosrvvnc_novorspdmn_Jsonclick = "";
         lblTextblockcontratoservicosvnc_clonasrvori_Jsonclick = "";
         lblTextblockcontratoservicosvnc_clonarlink_Jsonclick = "";
         lblTextblockcontratoservicosvnc_naoclonainfo_Jsonclick = "";
         lblTextblockcontratosrvvnc_semcusto_Jsonclick = "";
         lblTextblockcontratoservicosvnc_ativo_Jsonclick = "";
         lblContratoservicosvnc_naoclonainfo_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA917ContratoSrvVnc_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosvncgeneral__default(),
            new Object[][] {
                new Object[] {
               H00GE2_A915ContratoSrvVnc_CntSrvCod, H00GE2_A921ContratoSrvVnc_ServicoCod, H00GE2_n921ContratoSrvVnc_ServicoCod, H00GE2_A917ContratoSrvVnc_Codigo, H00GE2_A1453ContratoServicosVnc_Ativo, H00GE2_n1453ContratoServicosVnc_Ativo, H00GE2_A1090ContratoSrvVnc_SemCusto, H00GE2_n1090ContratoSrvVnc_SemCusto, H00GE2_A1437ContratoServicosVnc_NaoClonaInfo, H00GE2_n1437ContratoServicosVnc_NaoClonaInfo,
               H00GE2_A1818ContratoServicosVnc_ClonarLink, H00GE2_n1818ContratoServicosVnc_ClonarLink, H00GE2_A1145ContratoServicosVnc_ClonaSrvOri, H00GE2_n1145ContratoServicosVnc_ClonaSrvOri, H00GE2_A1801ContratoSrvVnc_NovoRspDmn, H00GE2_n1801ContratoSrvVnc_NovoRspDmn, H00GE2_A1743ContratoSrvVnc_NovoStatusDmn, H00GE2_n1743ContratoSrvVnc_NovoStatusDmn, H00GE2_A1438ContratoServicosVnc_Gestor, H00GE2_n1438ContratoServicosVnc_Gestor,
               H00GE2_A1821ContratoSrvVnc_SrvVncRef, H00GE2_n1821ContratoSrvVnc_SrvVncRef, H00GE2_A1745ContratoSrvVnc_VincularCom, H00GE2_n1745ContratoSrvVnc_VincularCom, H00GE2_A1663ContratoSrvVnc_SrvVncStatus, H00GE2_n1663ContratoSrvVnc_SrvVncStatus, H00GE2_A1088ContratoSrvVnc_PrestadoraCod, H00GE2_n1088ContratoSrvVnc_PrestadoraCod, H00GE2_A1084ContratoSrvVnc_StatusDmn, H00GE2_n1084ContratoSrvVnc_StatusDmn,
               H00GE2_A1800ContratoSrvVnc_DoStatusDmn, H00GE2_n1800ContratoSrvVnc_DoStatusDmn, H00GE2_A2108ContratoServicosVnc_Descricao, H00GE2_n2108ContratoServicosVnc_Descricao, H00GE2_A922ContratoSrvVnc_ServicoSigla, H00GE2_n922ContratoSrvVnc_ServicoSigla
               }
            }
         );
         AV15Pgmname = "ContratoServicosVncGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ContratoServicosVncGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1821ContratoSrvVnc_SrvVncRef ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A917ContratoSrvVnc_Codigo ;
      private int wcpOA917ContratoSrvVnc_Codigo ;
      private int AV12ContratoSrvVnc_CntSrvCod ;
      private int A1088ContratoSrvVnc_PrestadoraCod ;
      private int A1438ContratoServicosVnc_Gestor ;
      private int A1801ContratoSrvVnc_NovoRspDmn ;
      private int edtContratoSrvVnc_Codigo_Visible ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A921ContratoSrvVnc_ServicoCod ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoSrvVnc_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1800ContratoSrvVnc_DoStatusDmn ;
      private String A1084ContratoSrvVnc_StatusDmn ;
      private String A1663ContratoSrvVnc_SrvVncStatus ;
      private String A1745ContratoSrvVnc_VincularCom ;
      private String A1743ContratoSrvVnc_NovoStatusDmn ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContratoSrvVnc_Codigo_Internalname ;
      private String edtContratoSrvVnc_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkContratoServicosVnc_NaoClonaInfo_Internalname ;
      private String chkContratoServicosVnc_Ativo_Internalname ;
      private String scmdbuf ;
      private String A922ContratoSrvVnc_ServicoSigla ;
      private String edtContratoSrvVnc_ServicoSigla_Internalname ;
      private String edtContratoServicosVnc_Descricao_Internalname ;
      private String cmbContratoSrvVnc_DoStatusDmn_Internalname ;
      private String cmbContratoSrvVnc_StatusDmn_Internalname ;
      private String cmbContratoSrvVnc_PrestadoraCod_Internalname ;
      private String cmbContratoSrvVnc_SrvVncStatus_Internalname ;
      private String cmbContratoSrvVnc_VincularCom_Internalname ;
      private String cmbContratoSrvVnc_SrvVncRef_Internalname ;
      private String cmbContratoServicosVnc_Gestor_Internalname ;
      private String cmbContratoSrvVnc_NovoStatusDmn_Internalname ;
      private String cmbContratoSrvVnc_NovoRspDmn_Internalname ;
      private String cmbContratoServicosVnc_ClonaSrvOri_Internalname ;
      private String cmbContratoServicosVnc_ClonarLink_Internalname ;
      private String cmbContratoSrvVnc_SemCusto_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratosrvvnc_servicosigla_Internalname ;
      private String lblTextblockcontratosrvvnc_servicosigla_Jsonclick ;
      private String edtContratoSrvVnc_ServicoSigla_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_descricao_Internalname ;
      private String lblTextblockcontratoservicosvnc_descricao_Jsonclick ;
      private String edtContratoServicosVnc_Descricao_Jsonclick ;
      private String lblTextblockcontratosrvvnc_dostatusdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_dostatusdmn_Jsonclick ;
      private String cmbContratoSrvVnc_DoStatusDmn_Jsonclick ;
      private String lblTextblockcontratosrvvnc_statusdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_statusdmn_Jsonclick ;
      private String cmbContratoSrvVnc_StatusDmn_Jsonclick ;
      private String lblTextblockcontratosrvvnc_prestadoracod_Internalname ;
      private String lblTextblockcontratosrvvnc_prestadoracod_Jsonclick ;
      private String cmbContratoSrvVnc_PrestadoraCod_Jsonclick ;
      private String lblTextblockcontratosrvvnc_srvvncstatus_Internalname ;
      private String lblTextblockcontratosrvvnc_srvvncstatus_Jsonclick ;
      private String cmbContratoSrvVnc_SrvVncStatus_Jsonclick ;
      private String lblTextblockcontratosrvvnc_vincularcom_Internalname ;
      private String lblTextblockcontratosrvvnc_vincularcom_Jsonclick ;
      private String cmbContratoSrvVnc_VincularCom_Jsonclick ;
      private String lblTextblockcontratosrvvnc_srvvncref_Internalname ;
      private String lblTextblockcontratosrvvnc_srvvncref_Jsonclick ;
      private String cmbContratoSrvVnc_SrvVncRef_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_gestor_Internalname ;
      private String lblTextblockcontratoservicosvnc_gestor_Jsonclick ;
      private String cmbContratoServicosVnc_Gestor_Jsonclick ;
      private String lblTextblockcontratosrvvnc_novostatusdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_novostatusdmn_Jsonclick ;
      private String cmbContratoSrvVnc_NovoStatusDmn_Jsonclick ;
      private String lblTextblockcontratosrvvnc_novorspdmn_Internalname ;
      private String lblTextblockcontratosrvvnc_novorspdmn_Jsonclick ;
      private String cmbContratoSrvVnc_NovoRspDmn_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_clonasrvori_Internalname ;
      private String lblTextblockcontratoservicosvnc_clonasrvori_Jsonclick ;
      private String cmbContratoServicosVnc_ClonaSrvOri_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_clonarlink_Internalname ;
      private String lblTextblockcontratoservicosvnc_clonarlink_Jsonclick ;
      private String cmbContratoServicosVnc_ClonarLink_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_naoclonainfo_Internalname ;
      private String lblTextblockcontratoservicosvnc_naoclonainfo_Jsonclick ;
      private String lblTextblockcontratosrvvnc_semcusto_Internalname ;
      private String lblTextblockcontratosrvvnc_semcusto_Jsonclick ;
      private String cmbContratoSrvVnc_SemCusto_Jsonclick ;
      private String lblTextblockcontratoservicosvnc_ativo_Internalname ;
      private String lblTextblockcontratoservicosvnc_ativo_Jsonclick ;
      private String tblTablemergedcontratoservicosvnc_naoclonainfo_Internalname ;
      private String lblContratoservicosvnc_naoclonainfo_righttext_Internalname ;
      private String lblContratoservicosvnc_naoclonainfo_righttext_Jsonclick ;
      private String sCtrlA917ContratoSrvVnc_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool A1818ContratoServicosVnc_ClonarLink ;
      private bool A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool A1090ContratoSrvVnc_SemCusto ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1800ContratoSrvVnc_DoStatusDmn ;
      private bool n1084ContratoSrvVnc_StatusDmn ;
      private bool n1088ContratoSrvVnc_PrestadoraCod ;
      private bool n1663ContratoSrvVnc_SrvVncStatus ;
      private bool n1745ContratoSrvVnc_VincularCom ;
      private bool n1821ContratoSrvVnc_SrvVncRef ;
      private bool n1438ContratoServicosVnc_Gestor ;
      private bool n1743ContratoSrvVnc_NovoStatusDmn ;
      private bool n1801ContratoSrvVnc_NovoRspDmn ;
      private bool n1145ContratoServicosVnc_ClonaSrvOri ;
      private bool n1818ContratoServicosVnc_ClonarLink ;
      private bool n1090ContratoSrvVnc_SemCusto ;
      private bool n921ContratoSrvVnc_ServicoCod ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private bool n1437ContratoServicosVnc_NaoClonaInfo ;
      private bool n2108ContratoServicosVnc_Descricao ;
      private bool n922ContratoSrvVnc_ServicoSigla ;
      private bool returnInSub ;
      private String A2108ContratoServicosVnc_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoSrvVnc_DoStatusDmn ;
      private GXCombobox cmbContratoSrvVnc_StatusDmn ;
      private GXCombobox cmbContratoSrvVnc_PrestadoraCod ;
      private GXCombobox cmbContratoSrvVnc_SrvVncStatus ;
      private GXCombobox cmbContratoSrvVnc_VincularCom ;
      private GXCombobox cmbContratoSrvVnc_SrvVncRef ;
      private GXCombobox cmbContratoServicosVnc_Gestor ;
      private GXCombobox cmbContratoSrvVnc_NovoStatusDmn ;
      private GXCombobox cmbContratoSrvVnc_NovoRspDmn ;
      private GXCombobox cmbContratoServicosVnc_ClonaSrvOri ;
      private GXCombobox cmbContratoServicosVnc_ClonarLink ;
      private GXCheckbox chkContratoServicosVnc_NaoClonaInfo ;
      private GXCombobox cmbContratoSrvVnc_SemCusto ;
      private GXCheckbox chkContratoServicosVnc_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00GE2_A915ContratoSrvVnc_CntSrvCod ;
      private int[] H00GE2_A921ContratoSrvVnc_ServicoCod ;
      private bool[] H00GE2_n921ContratoSrvVnc_ServicoCod ;
      private int[] H00GE2_A917ContratoSrvVnc_Codigo ;
      private bool[] H00GE2_A1453ContratoServicosVnc_Ativo ;
      private bool[] H00GE2_n1453ContratoServicosVnc_Ativo ;
      private bool[] H00GE2_A1090ContratoSrvVnc_SemCusto ;
      private bool[] H00GE2_n1090ContratoSrvVnc_SemCusto ;
      private bool[] H00GE2_A1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] H00GE2_n1437ContratoServicosVnc_NaoClonaInfo ;
      private bool[] H00GE2_A1818ContratoServicosVnc_ClonarLink ;
      private bool[] H00GE2_n1818ContratoServicosVnc_ClonarLink ;
      private bool[] H00GE2_A1145ContratoServicosVnc_ClonaSrvOri ;
      private bool[] H00GE2_n1145ContratoServicosVnc_ClonaSrvOri ;
      private int[] H00GE2_A1801ContratoSrvVnc_NovoRspDmn ;
      private bool[] H00GE2_n1801ContratoSrvVnc_NovoRspDmn ;
      private String[] H00GE2_A1743ContratoSrvVnc_NovoStatusDmn ;
      private bool[] H00GE2_n1743ContratoSrvVnc_NovoStatusDmn ;
      private int[] H00GE2_A1438ContratoServicosVnc_Gestor ;
      private bool[] H00GE2_n1438ContratoServicosVnc_Gestor ;
      private short[] H00GE2_A1821ContratoSrvVnc_SrvVncRef ;
      private bool[] H00GE2_n1821ContratoSrvVnc_SrvVncRef ;
      private String[] H00GE2_A1745ContratoSrvVnc_VincularCom ;
      private bool[] H00GE2_n1745ContratoSrvVnc_VincularCom ;
      private String[] H00GE2_A1663ContratoSrvVnc_SrvVncStatus ;
      private bool[] H00GE2_n1663ContratoSrvVnc_SrvVncStatus ;
      private int[] H00GE2_A1088ContratoSrvVnc_PrestadoraCod ;
      private bool[] H00GE2_n1088ContratoSrvVnc_PrestadoraCod ;
      private String[] H00GE2_A1084ContratoSrvVnc_StatusDmn ;
      private bool[] H00GE2_n1084ContratoSrvVnc_StatusDmn ;
      private String[] H00GE2_A1800ContratoSrvVnc_DoStatusDmn ;
      private bool[] H00GE2_n1800ContratoSrvVnc_DoStatusDmn ;
      private String[] H00GE2_A2108ContratoServicosVnc_Descricao ;
      private bool[] H00GE2_n2108ContratoServicosVnc_Descricao ;
      private String[] H00GE2_A922ContratoSrvVnc_ServicoSigla ;
      private bool[] H00GE2_n922ContratoSrvVnc_ServicoSigla ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratoservicosvncgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GE2 ;
          prmH00GE2 = new Object[] {
          new Object[] {"@ContratoSrvVnc_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GE2", "SELECT T1.[ContratoSrvVnc_CntSrvCod] AS ContratoSrvVnc_CntSrvCod, T2.[Servico_Codigo] AS ContratoSrvVnc_ServicoCod, T1.[ContratoSrvVnc_Codigo], T1.[ContratoServicosVnc_Ativo], T1.[ContratoSrvVnc_SemCusto], T1.[ContratoServicosVnc_NaoClonaInfo], T1.[ContratoServicosVnc_ClonarLink], T1.[ContratoServicosVnc_ClonaSrvOri], T1.[ContratoSrvVnc_NovoRspDmn], T1.[ContratoSrvVnc_NovoStatusDmn], T1.[ContratoServicosVnc_Gestor], T1.[ContratoSrvVnc_SrvVncRef], T1.[ContratoSrvVnc_VincularCom], T1.[ContratoSrvVnc_SrvVncStatus], T1.[ContratoSrvVnc_PrestadoraCod], T1.[ContratoSrvVnc_StatusDmn], T1.[ContratoSrvVnc_DoStatusDmn], T1.[ContratoServicosVnc_Descricao], T3.[Servico_Sigla] AS ContratoSrvVnc_ServicoSigla FROM (([ContratoServicosVnc] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoSrvVnc_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE T1.[ContratoSrvVnc_Codigo] = @ContratoSrvVnc_Codigo ORDER BY T1.[ContratoSrvVnc_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GE2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 20) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getString(19, 15) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
