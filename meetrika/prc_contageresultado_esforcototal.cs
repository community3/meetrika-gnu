/*
               File: PRC_ContageResultado_EsforcoTotal
        Description: Contage Resultado_Esforco Total
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contageresultado_esforcototal : GXProcedure
   {
      public prc_contageresultado_esforcototal( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contageresultado_esforcototal( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out String aP1_ContagemResultado_EsforcoTotal )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_EsforcoTotal = "" ;
         initialize();
         executePrivate();
         aP1_ContagemResultado_EsforcoTotal=this.AV10ContagemResultado_EsforcoTotal;
      }

      public String executeUdp( int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10ContagemResultado_EsforcoTotal = "" ;
         initialize();
         executePrivate();
         aP1_ContagemResultado_EsforcoTotal=this.AV10ContagemResultado_EsforcoTotal;
         return AV10ContagemResultado_EsforcoTotal ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out String aP1_ContagemResultado_EsforcoTotal )
      {
         prc_contageresultado_esforcototal objprc_contageresultado_esforcototal;
         objprc_contageresultado_esforcototal = new prc_contageresultado_esforcototal();
         objprc_contageresultado_esforcototal.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_contageresultado_esforcototal.AV10ContagemResultado_EsforcoTotal = "" ;
         objprc_contageresultado_esforcototal.context.SetSubmitInitialConfig(context);
         objprc_contageresultado_esforcototal.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contageresultado_esforcototal);
         aP1_ContagemResultado_EsforcoTotal=this.AV10ContagemResultado_EsforcoTotal;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contageresultado_esforcototal)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9i = 0;
         AV8c = 0;
         /* Optimized group. */
         /* Using cursor P002N2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         c482ContagemResultadoContagens_Esforco = P002N2_A482ContagemResultadoContagens_Esforco[0];
         cV8c = P002N2_AV8c[0];
         pr_default.close(0);
         AV9i = (short)(AV9i+c482ContagemResultadoContagens_Esforco);
         AV8c = (short)(AV8c+cV8c*1);
         /* End optimized group. */
         AV10ContagemResultado_EsforcoTotal = StringUtil.Str( (decimal)(AV9i), 4, 0) + " (" + StringUtil.Trim( StringUtil.Str( (decimal)(AV8c), 4, 0)) + ")";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9i = 0;
         scmdbuf = "";
         P002N2_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P002N2_AV8c = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contageresultado_esforcototal__default(),
            new Object[][] {
                new Object[] {
               P002N2_A482ContagemResultadoContagens_Esforco, P002N2_AV8c
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9i ;
      private short AV8c ;
      private short c482ContagemResultadoContagens_Esforco ;
      private short cV8c ;
      private int A456ContagemResultado_Codigo ;
      private String AV10ContagemResultado_EsforcoTotal ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P002N2_A482ContagemResultadoContagens_Esforco ;
      private short[] P002N2_AV8c ;
      private String aP1_ContagemResultado_EsforcoTotal ;
   }

   public class prc_contageresultado_esforcototal__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002N2 ;
          prmP002N2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002N2", "SELECT SUM([ContagemResultadoContagens_Esforco]), COUNT(*) FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002N2,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
