/*
               File: type_SdtSDT_Demandas_Demanda
        Description: SDT_Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 23:47:7.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Demandas.Demanda" )]
   [XmlType(TypeName =  "SDT_Demandas.Demanda" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_Demandas_Demanda : GxUserType
   {
      public SdtSDT_Demandas_Demanda( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Demandas_Demanda_Demanda = "";
         gxTv_SdtSDT_Demandas_Demanda_Sistemanom = "";
         gxTv_SdtSDT_Demandas_Demanda_Modulosigla = "";
         gxTv_SdtSDT_Demandas_Demanda_Demandafm = "";
         gxTv_SdtSDT_Demandas_Demanda_Datadmn = DateTime.MinValue;
         gxTv_SdtSDT_Demandas_Demanda_Datacnt = DateTime.MinValue;
         gxTv_SdtSDT_Demandas_Demanda_Dataent = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_Demandas_Demanda_Descricao = "";
         gxTv_SdtSDT_Demandas_Demanda_Observacao = "";
         gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom = "";
         gxTv_SdtSDT_Demandas_Demanda_Link = "";
         gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom = "";
         gxTv_SdtSDT_Demandas_Demanda_Tipomntn = "";
      }

      public SdtSDT_Demandas_Demanda( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Demandas_Demanda deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Demandas_Demanda)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Demandas_Demanda obj ;
         obj = this;
         obj.gxTpr_Demanda = deserialized.gxTpr_Demanda;
         obj.gxTpr_Contratada_codigo = deserialized.gxTpr_Contratada_codigo;
         obj.gxTpr_Contratadaorigem_codigo = deserialized.gxTpr_Contratadaorigem_codigo;
         obj.gxTpr_Sistemacod = deserialized.gxTpr_Sistemacod;
         obj.gxTpr_Sistemanom = deserialized.gxTpr_Sistemanom;
         obj.gxTpr_Modulocod = deserialized.gxTpr_Modulocod;
         obj.gxTpr_Modulosigla = deserialized.gxTpr_Modulosigla;
         obj.gxTpr_Demandafm = deserialized.gxTpr_Demandafm;
         obj.gxTpr_Datadmn = deserialized.gxTpr_Datadmn;
         obj.gxTpr_Datacnt = deserialized.gxTpr_Datacnt;
         obj.gxTpr_Dataent = deserialized.gxTpr_Dataent;
         obj.gxTpr_Descricao = deserialized.gxTpr_Descricao;
         obj.gxTpr_Observacao = deserialized.gxTpr_Observacao;
         obj.gxTpr_Contadorfscod = deserialized.gxTpr_Contadorfscod;
         obj.gxTpr_Contadorfsnom = deserialized.gxTpr_Contadorfsnom;
         obj.gxTpr_Link = deserialized.gxTpr_Link;
         obj.gxTpr_Pfbfm = deserialized.gxTpr_Pfbfm;
         obj.gxTpr_Pflfm = deserialized.gxTpr_Pflfm;
         obj.gxTpr_Pfbfs = deserialized.gxTpr_Pfbfs;
         obj.gxTpr_Pflfs = deserialized.gxTpr_Pflfs;
         obj.gxTpr_Contadorfmcod = deserialized.gxTpr_Contadorfmcod;
         obj.gxTpr_Contadorfmnom = deserialized.gxTpr_Contadorfmnom;
         obj.gxTpr_Servico_codigo = deserialized.gxTpr_Servico_codigo;
         obj.gxTpr_Naoconformidadedmn = deserialized.gxTpr_Naoconformidadedmn;
         obj.gxTpr_Naoconformidadecnt = deserialized.gxTpr_Naoconformidadecnt;
         obj.gxTpr_Naocadastrada = deserialized.gxTpr_Naocadastrada;
         obj.gxTpr_Deflator = deserialized.gxTpr_Deflator;
         obj.gxTpr_Custocrfm = deserialized.gxTpr_Custocrfm;
         obj.gxTpr_Tipomntn = deserialized.gxTpr_Tipomntn;
         obj.gxTpr_Evento = deserialized.gxTpr_Evento;
         obj.gxTpr_Owner = deserialized.gxTpr_Owner;
         obj.gxTpr_Cntsrvcod = deserialized.gxTpr_Cntsrvcod;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Demanda") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Demanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Contratada_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaOrigem_Codigo") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Contratadaorigem_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaCod") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaNom") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Sistemanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ModuloCod") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Modulocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ModuloSigla") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Modulosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DemandaFM") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataDmn") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_Demandas_Demanda_Datadmn = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_Demandas_Demanda_Datadmn = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataCnt") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_Demandas_Demanda_Datacnt = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_Demandas_Demanda_Datacnt = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataEnt") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_Demandas_Demanda_Dataent = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_Demandas_Demanda_Dataent = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Descricao") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Observacao") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Observacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContadorFSCod") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Contadorfscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContadorFSNom") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Link") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PFBFM") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Pfbfm = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PFLFM") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Pflfm = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PFBFS") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Pfbfs = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PFLFS") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Pflfs = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContadorFMCod") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Contadorfmcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContadorFMNom") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Servico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoConformidadeDmn") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Naoconformidadedmn = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoConformidadeCnt") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Naoconformidadecnt = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "NaoCadastrada") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Naocadastrada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Deflator") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Deflator = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CustoCrFM") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Custocrfm = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoMntn") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Tipomntn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Evento") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Evento = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Owner") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "CntSrvCod") )
               {
                  gxTv_SdtSDT_Demandas_Demanda_Cntsrvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Demandas.Demanda";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Demanda", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Demanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Contratada_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaOrigem_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Contratadaorigem_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SistemaNom", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Sistemanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ModuloCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Modulocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ModuloSigla", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Modulosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("DemandaFM", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_Demandas_Demanda_Datadmn) )
         {
            oWriter.WriteStartElement("DataDmn");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_Demandas_Demanda_Datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_Demandas_Demanda_Datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_Demandas_Demanda_Datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("DataDmn", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_Demandas_Demanda_Datacnt) )
         {
            oWriter.WriteStartElement("DataCnt");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_Demandas_Demanda_Datacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_Demandas_Demanda_Datacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_Demandas_Demanda_Datacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("DataCnt", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_Demandas_Demanda_Dataent) )
         {
            oWriter.WriteStartElement("DataEnt");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_Demandas_Demanda_Dataent)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_Demandas_Demanda_Dataent)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_Demandas_Demanda_Dataent)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_Demandas_Demanda_Dataent)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_Demandas_Demanda_Dataent)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_Demandas_Demanda_Dataent)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("DataEnt", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Descricao", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Observacao", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Observacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContadorFSCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Contadorfscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContadorFSNom", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Link", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PFBFM", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_Demandas_Demanda_Pfbfm, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PFLFM", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_Demandas_Demanda_Pflfm, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PFBFS", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_Demandas_Demanda_Pfbfs, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PFLFS", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_Demandas_Demanda_Pflfs, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContadorFMCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Contadorfmcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContadorFMNom", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Servico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Servico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("NaoConformidadeDmn", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Naoconformidadedmn), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("NaoConformidadeCnt", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Naoconformidadecnt), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("NaoCadastrada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_Demandas_Demanda_Naocadastrada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Deflator", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_Demandas_Demanda_Deflator, 6, 3)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("CustoCrFM", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_Demandas_Demanda_Custocrfm, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TipoMntn", StringUtil.RTrim( gxTv_SdtSDT_Demandas_Demanda_Tipomntn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Evento", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Evento), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("CntSrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Demandas_Demanda_Cntsrvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Demanda", gxTv_SdtSDT_Demandas_Demanda_Demanda, false);
         AddObjectProperty("Contratada_Codigo", gxTv_SdtSDT_Demandas_Demanda_Contratada_codigo, false);
         AddObjectProperty("ContratadaOrigem_Codigo", gxTv_SdtSDT_Demandas_Demanda_Contratadaorigem_codigo, false);
         AddObjectProperty("SistemaCod", gxTv_SdtSDT_Demandas_Demanda_Sistemacod, false);
         AddObjectProperty("SistemaNom", gxTv_SdtSDT_Demandas_Demanda_Sistemanom, false);
         AddObjectProperty("ModuloCod", gxTv_SdtSDT_Demandas_Demanda_Modulocod, false);
         AddObjectProperty("ModuloSigla", gxTv_SdtSDT_Demandas_Demanda_Modulosigla, false);
         AddObjectProperty("DemandaFM", gxTv_SdtSDT_Demandas_Demanda_Demandafm, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_Demandas_Demanda_Datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_Demandas_Demanda_Datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_Demandas_Demanda_Datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataDmn", sDateCnv, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_Demandas_Demanda_Datacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_Demandas_Demanda_Datacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_Demandas_Demanda_Datacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataCnt", sDateCnv, false);
         datetime_STZ = gxTv_SdtSDT_Demandas_Demanda_Dataent;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataEnt", sDateCnv, false);
         AddObjectProperty("Descricao", gxTv_SdtSDT_Demandas_Demanda_Descricao, false);
         AddObjectProperty("Observacao", gxTv_SdtSDT_Demandas_Demanda_Observacao, false);
         AddObjectProperty("ContadorFSCod", gxTv_SdtSDT_Demandas_Demanda_Contadorfscod, false);
         AddObjectProperty("ContadorFSNom", gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom, false);
         AddObjectProperty("Link", gxTv_SdtSDT_Demandas_Demanda_Link, false);
         AddObjectProperty("PFBFM", gxTv_SdtSDT_Demandas_Demanda_Pfbfm, false);
         AddObjectProperty("PFLFM", gxTv_SdtSDT_Demandas_Demanda_Pflfm, false);
         AddObjectProperty("PFBFS", gxTv_SdtSDT_Demandas_Demanda_Pfbfs, false);
         AddObjectProperty("PFLFS", gxTv_SdtSDT_Demandas_Demanda_Pflfs, false);
         AddObjectProperty("ContadorFMCod", gxTv_SdtSDT_Demandas_Demanda_Contadorfmcod, false);
         AddObjectProperty("ContadorFMNom", gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom, false);
         AddObjectProperty("Servico_Codigo", gxTv_SdtSDT_Demandas_Demanda_Servico_codigo, false);
         AddObjectProperty("NaoConformidadeDmn", gxTv_SdtSDT_Demandas_Demanda_Naoconformidadedmn, false);
         AddObjectProperty("NaoConformidadeCnt", gxTv_SdtSDT_Demandas_Demanda_Naoconformidadecnt, false);
         AddObjectProperty("NaoCadastrada", gxTv_SdtSDT_Demandas_Demanda_Naocadastrada, false);
         AddObjectProperty("Deflator", gxTv_SdtSDT_Demandas_Demanda_Deflator, false);
         AddObjectProperty("CustoCrFM", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSDT_Demandas_Demanda_Custocrfm, 18, 5)), false);
         AddObjectProperty("TipoMntn", gxTv_SdtSDT_Demandas_Demanda_Tipomntn, false);
         AddObjectProperty("Evento", gxTv_SdtSDT_Demandas_Demanda_Evento, false);
         AddObjectProperty("Owner", gxTv_SdtSDT_Demandas_Demanda_Owner, false);
         AddObjectProperty("CntSrvCod", gxTv_SdtSDT_Demandas_Demanda_Cntsrvcod, false);
         return  ;
      }

      [  SoapElement( ElementName = "Demanda" )]
      [  XmlElement( ElementName = "Demanda"   )]
      public String gxTpr_Demanda
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Demanda ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Demanda = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_Codigo" )]
      [  XmlElement( ElementName = "Contratada_Codigo"   )]
      public int gxTpr_Contratada_codigo
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Contratada_codigo ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Contratada_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratadaOrigem_Codigo" )]
      [  XmlElement( ElementName = "ContratadaOrigem_Codigo"   )]
      public int gxTpr_Contratadaorigem_codigo
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Contratadaorigem_codigo ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Contratadaorigem_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SistemaCod" )]
      [  XmlElement( ElementName = "SistemaCod"   )]
      public int gxTpr_Sistemacod
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Sistemacod ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Sistemacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "SistemaNom" )]
      [  XmlElement( ElementName = "SistemaNom"   )]
      public String gxTpr_Sistemanom
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Sistemanom ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Sistemanom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ModuloCod" )]
      [  XmlElement( ElementName = "ModuloCod"   )]
      public int gxTpr_Modulocod
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Modulocod ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Modulocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ModuloSigla" )]
      [  XmlElement( ElementName = "ModuloSigla"   )]
      public String gxTpr_Modulosigla
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Modulosigla ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Modulosigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DemandaFM" )]
      [  XmlElement( ElementName = "DemandaFM"   )]
      public String gxTpr_Demandafm
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Demandafm ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Demandafm = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DataDmn" )]
      [  XmlElement( ElementName = "DataDmn"  , IsNullable=true )]
      public string gxTpr_Datadmn_Nullable
      {
         get {
            if ( gxTv_SdtSDT_Demandas_Demanda_Datadmn == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_Demandas_Demanda_Datadmn).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_Demandas_Demanda_Datadmn = DateTime.MinValue;
            else
               gxTv_SdtSDT_Demandas_Demanda_Datadmn = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Datadmn
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Datadmn ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Datadmn = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "DataCnt" )]
      [  XmlElement( ElementName = "DataCnt"  , IsNullable=true )]
      public string gxTpr_Datacnt_Nullable
      {
         get {
            if ( gxTv_SdtSDT_Demandas_Demanda_Datacnt == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_Demandas_Demanda_Datacnt).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_Demandas_Demanda_Datacnt = DateTime.MinValue;
            else
               gxTv_SdtSDT_Demandas_Demanda_Datacnt = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Datacnt
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Datacnt ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Datacnt = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "DataEnt" )]
      [  XmlElement( ElementName = "DataEnt"  , IsNullable=true )]
      public string gxTpr_Dataent_Nullable
      {
         get {
            if ( gxTv_SdtSDT_Demandas_Demanda_Dataent == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_Demandas_Demanda_Dataent).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_Demandas_Demanda_Dataent = DateTime.MinValue;
            else
               gxTv_SdtSDT_Demandas_Demanda_Dataent = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Dataent
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Dataent ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Dataent = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Descricao" )]
      [  XmlElement( ElementName = "Descricao"   )]
      public String gxTpr_Descricao
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Descricao ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Observacao" )]
      [  XmlElement( ElementName = "Observacao"   )]
      public String gxTpr_Observacao
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Observacao ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Observacao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContadorFSCod" )]
      [  XmlElement( ElementName = "ContadorFSCod"   )]
      public int gxTpr_Contadorfscod
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Contadorfscod ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Contadorfscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContadorFSNom" )]
      [  XmlElement( ElementName = "ContadorFSNom"   )]
      public String gxTpr_Contadorfsnom
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Link" )]
      [  XmlElement( ElementName = "Link"   )]
      public String gxTpr_Link
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Link ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "PFBFM" )]
      [  XmlElement( ElementName = "PFBFM"   )]
      public double gxTpr_Pfbfm_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_Demandas_Demanda_Pfbfm) ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pfbfm = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Pfbfm
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Pfbfm ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pfbfm = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "PFLFM" )]
      [  XmlElement( ElementName = "PFLFM"   )]
      public double gxTpr_Pflfm_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_Demandas_Demanda_Pflfm) ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pflfm = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Pflfm
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Pflfm ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pflfm = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "PFBFS" )]
      [  XmlElement( ElementName = "PFBFS"   )]
      public double gxTpr_Pfbfs_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_Demandas_Demanda_Pfbfs) ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pfbfs = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Pfbfs
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Pfbfs ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pfbfs = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "PFLFS" )]
      [  XmlElement( ElementName = "PFLFS"   )]
      public double gxTpr_Pflfs_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_Demandas_Demanda_Pflfs) ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pflfs = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Pflfs
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Pflfs ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Pflfs = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContadorFMCod" )]
      [  XmlElement( ElementName = "ContadorFMCod"   )]
      public int gxTpr_Contadorfmcod
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Contadorfmcod ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Contadorfmcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContadorFMNom" )]
      [  XmlElement( ElementName = "ContadorFMNom"   )]
      public String gxTpr_Contadorfmnom
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Codigo" )]
      [  XmlElement( ElementName = "Servico_Codigo"   )]
      public int gxTpr_Servico_codigo
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Servico_codigo ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Servico_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "NaoConformidadeDmn" )]
      [  XmlElement( ElementName = "NaoConformidadeDmn"   )]
      public int gxTpr_Naoconformidadedmn
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Naoconformidadedmn ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Naoconformidadedmn = (int)(value);
         }

      }

      [  SoapElement( ElementName = "NaoConformidadeCnt" )]
      [  XmlElement( ElementName = "NaoConformidadeCnt"   )]
      public int gxTpr_Naoconformidadecnt
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Naoconformidadecnt ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Naoconformidadecnt = (int)(value);
         }

      }

      [  SoapElement( ElementName = "NaoCadastrada" )]
      [  XmlElement( ElementName = "NaoCadastrada"   )]
      public bool gxTpr_Naocadastrada
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Naocadastrada ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Naocadastrada = value;
         }

      }

      [  SoapElement( ElementName = "Deflator" )]
      [  XmlElement( ElementName = "Deflator"   )]
      public double gxTpr_Deflator_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_Demandas_Demanda_Deflator) ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Deflator = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Deflator
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Deflator ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Deflator = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "CustoCrFM" )]
      [  XmlElement( ElementName = "CustoCrFM"   )]
      public double gxTpr_Custocrfm_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_Demandas_Demanda_Custocrfm) ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Custocrfm = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Custocrfm
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Custocrfm ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Custocrfm = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "TipoMntn" )]
      [  XmlElement( ElementName = "TipoMntn"   )]
      public String gxTpr_Tipomntn
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Tipomntn ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Tipomntn = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Evento" )]
      [  XmlElement( ElementName = "Evento"   )]
      public short gxTpr_Evento
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Evento ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Evento = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Owner" )]
      [  XmlElement( ElementName = "Owner"   )]
      public int gxTpr_Owner
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Owner ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Owner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "CntSrvCod" )]
      [  XmlElement( ElementName = "CntSrvCod"   )]
      public int gxTpr_Cntsrvcod
      {
         get {
            return gxTv_SdtSDT_Demandas_Demanda_Cntsrvcod ;
         }

         set {
            gxTv_SdtSDT_Demandas_Demanda_Cntsrvcod = (int)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Demandas_Demanda_Demanda = "";
         gxTv_SdtSDT_Demandas_Demanda_Sistemanom = "";
         gxTv_SdtSDT_Demandas_Demanda_Modulosigla = "";
         gxTv_SdtSDT_Demandas_Demanda_Demandafm = "";
         gxTv_SdtSDT_Demandas_Demanda_Datadmn = DateTime.MinValue;
         gxTv_SdtSDT_Demandas_Demanda_Datacnt = DateTime.MinValue;
         gxTv_SdtSDT_Demandas_Demanda_Dataent = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_Demandas_Demanda_Descricao = "";
         gxTv_SdtSDT_Demandas_Demanda_Observacao = "";
         gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom = "";
         gxTv_SdtSDT_Demandas_Demanda_Link = "";
         gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom = "";
         gxTv_SdtSDT_Demandas_Demanda_Tipomntn = "";
         gxTv_SdtSDT_Demandas_Demanda_Evento = 1;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtSDT_Demandas_Demanda_Evento ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Contratada_codigo ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Contratadaorigem_codigo ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Sistemacod ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Modulocod ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Contadorfscod ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Contadorfmcod ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Servico_codigo ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Naoconformidadedmn ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Naoconformidadecnt ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Owner ;
      protected int gxTv_SdtSDT_Demandas_Demanda_Cntsrvcod ;
      protected decimal gxTv_SdtSDT_Demandas_Demanda_Pfbfm ;
      protected decimal gxTv_SdtSDT_Demandas_Demanda_Pflfm ;
      protected decimal gxTv_SdtSDT_Demandas_Demanda_Pfbfs ;
      protected decimal gxTv_SdtSDT_Demandas_Demanda_Pflfs ;
      protected decimal gxTv_SdtSDT_Demandas_Demanda_Deflator ;
      protected decimal gxTv_SdtSDT_Demandas_Demanda_Custocrfm ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Modulosigla ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Contadorfsnom ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Contadorfmnom ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_Demandas_Demanda_Dataent ;
      protected DateTime datetime_STZ ;
      protected DateTime gxTv_SdtSDT_Demandas_Demanda_Datadmn ;
      protected DateTime gxTv_SdtSDT_Demandas_Demanda_Datacnt ;
      protected bool gxTv_SdtSDT_Demandas_Demanda_Naocadastrada ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Observacao ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Link ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Tipomntn ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Demanda ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Sistemanom ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Demandafm ;
      protected String gxTv_SdtSDT_Demandas_Demanda_Descricao ;
   }

   [DataContract(Name = @"SDT_Demandas.Demanda", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_Demandas_Demanda_RESTInterface : GxGenericCollectionItem<SdtSDT_Demandas_Demanda>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Demandas_Demanda_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Demandas_Demanda_RESTInterface( SdtSDT_Demandas_Demanda psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Demanda" , Order = 0 )]
      public String gxTpr_Demanda
      {
         get {
            return sdt.gxTpr_Demanda ;
         }

         set {
            sdt.gxTpr_Demanda = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_Codigo" , Order = 1 )]
      public Nullable<int> gxTpr_Contratada_codigo
      {
         get {
            return sdt.gxTpr_Contratada_codigo ;
         }

         set {
            sdt.gxTpr_Contratada_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaOrigem_Codigo" , Order = 2 )]
      public Nullable<int> gxTpr_Contratadaorigem_codigo
      {
         get {
            return sdt.gxTpr_Contratadaorigem_codigo ;
         }

         set {
            sdt.gxTpr_Contratadaorigem_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SistemaCod" , Order = 3 )]
      public Nullable<int> gxTpr_Sistemacod
      {
         get {
            return sdt.gxTpr_Sistemacod ;
         }

         set {
            sdt.gxTpr_Sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SistemaNom" , Order = 4 )]
      public String gxTpr_Sistemanom
      {
         get {
            return sdt.gxTpr_Sistemanom ;
         }

         set {
            sdt.gxTpr_Sistemanom = (String)(value);
         }

      }

      [DataMember( Name = "ModuloCod" , Order = 5 )]
      public Nullable<int> gxTpr_Modulocod
      {
         get {
            return sdt.gxTpr_Modulocod ;
         }

         set {
            sdt.gxTpr_Modulocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ModuloSigla" , Order = 6 )]
      public String gxTpr_Modulosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Modulosigla) ;
         }

         set {
            sdt.gxTpr_Modulosigla = (String)(value);
         }

      }

      [DataMember( Name = "DemandaFM" , Order = 7 )]
      public String gxTpr_Demandafm
      {
         get {
            return sdt.gxTpr_Demandafm ;
         }

         set {
            sdt.gxTpr_Demandafm = (String)(value);
         }

      }

      [DataMember( Name = "DataDmn" , Order = 8 )]
      public String gxTpr_Datadmn
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Datadmn) ;
         }

         set {
            sdt.gxTpr_Datadmn = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "DataCnt" , Order = 9 )]
      public String gxTpr_Datacnt
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Datacnt) ;
         }

         set {
            sdt.gxTpr_Datacnt = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "DataEnt" , Order = 10 )]
      public String gxTpr_Dataent
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Dataent) ;
         }

         set {
            sdt.gxTpr_Dataent = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Descricao" , Order = 11 )]
      public String gxTpr_Descricao
      {
         get {
            return sdt.gxTpr_Descricao ;
         }

         set {
            sdt.gxTpr_Descricao = (String)(value);
         }

      }

      [DataMember( Name = "Observacao" , Order = 12 )]
      public String gxTpr_Observacao
      {
         get {
            return sdt.gxTpr_Observacao ;
         }

         set {
            sdt.gxTpr_Observacao = (String)(value);
         }

      }

      [DataMember( Name = "ContadorFSCod" , Order = 13 )]
      public Nullable<int> gxTpr_Contadorfscod
      {
         get {
            return sdt.gxTpr_Contadorfscod ;
         }

         set {
            sdt.gxTpr_Contadorfscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContadorFSNom" , Order = 14 )]
      public String gxTpr_Contadorfsnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contadorfsnom) ;
         }

         set {
            sdt.gxTpr_Contadorfsnom = (String)(value);
         }

      }

      [DataMember( Name = "Link" , Order = 15 )]
      public String gxTpr_Link
      {
         get {
            return sdt.gxTpr_Link ;
         }

         set {
            sdt.gxTpr_Link = (String)(value);
         }

      }

      [DataMember( Name = "PFBFM" , Order = 16 )]
      public String gxTpr_Pfbfm
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Pfbfm, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Pfbfm = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "PFLFM" , Order = 17 )]
      public String gxTpr_Pflfm
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Pflfm, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Pflfm = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "PFBFS" , Order = 18 )]
      public String gxTpr_Pfbfs
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Pfbfs, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Pfbfs = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "PFLFS" , Order = 19 )]
      public String gxTpr_Pflfs
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Pflfs, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Pflfs = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContadorFMCod" , Order = 20 )]
      public Nullable<int> gxTpr_Contadorfmcod
      {
         get {
            return sdt.gxTpr_Contadorfmcod ;
         }

         set {
            sdt.gxTpr_Contadorfmcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContadorFMNom" , Order = 21 )]
      public String gxTpr_Contadorfmnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contadorfmnom) ;
         }

         set {
            sdt.gxTpr_Contadorfmnom = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Codigo" , Order = 22 )]
      public Nullable<int> gxTpr_Servico_codigo
      {
         get {
            return sdt.gxTpr_Servico_codigo ;
         }

         set {
            sdt.gxTpr_Servico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "NaoConformidadeDmn" , Order = 23 )]
      public Nullable<int> gxTpr_Naoconformidadedmn
      {
         get {
            return sdt.gxTpr_Naoconformidadedmn ;
         }

         set {
            sdt.gxTpr_Naoconformidadedmn = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "NaoConformidadeCnt" , Order = 24 )]
      public Nullable<int> gxTpr_Naoconformidadecnt
      {
         get {
            return sdt.gxTpr_Naoconformidadecnt ;
         }

         set {
            sdt.gxTpr_Naoconformidadecnt = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "NaoCadastrada" , Order = 25 )]
      public bool gxTpr_Naocadastrada
      {
         get {
            return sdt.gxTpr_Naocadastrada ;
         }

         set {
            sdt.gxTpr_Naocadastrada = value;
         }

      }

      [DataMember( Name = "Deflator" , Order = 26 )]
      public Nullable<decimal> gxTpr_Deflator
      {
         get {
            return sdt.gxTpr_Deflator ;
         }

         set {
            sdt.gxTpr_Deflator = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "CustoCrFM" , Order = 27 )]
      public String gxTpr_Custocrfm
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Custocrfm, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Custocrfm = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "TipoMntn" , Order = 28 )]
      public String gxTpr_Tipomntn
      {
         get {
            return sdt.gxTpr_Tipomntn ;
         }

         set {
            sdt.gxTpr_Tipomntn = (String)(value);
         }

      }

      [DataMember( Name = "Evento" , Order = 29 )]
      public Nullable<short> gxTpr_Evento
      {
         get {
            return sdt.gxTpr_Evento ;
         }

         set {
            sdt.gxTpr_Evento = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Owner" , Order = 30 )]
      public Nullable<int> gxTpr_Owner
      {
         get {
            return sdt.gxTpr_Owner ;
         }

         set {
            sdt.gxTpr_Owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "CntSrvCod" , Order = 31 )]
      public Nullable<int> gxTpr_Cntsrvcod
      {
         get {
            return sdt.gxTpr_Cntsrvcod ;
         }

         set {
            sdt.gxTpr_Cntsrvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSDT_Demandas_Demanda sdt
      {
         get {
            return (SdtSDT_Demandas_Demanda)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Demandas_Demanda() ;
         }
      }

   }

}
