/*
               File: LoadAuditIndicador
        Description: Load Audit Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:41.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditindicador : GXProcedure
   {
      public loadauditindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Indicador_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Indicador_Codigo = aP2_Indicador_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Indicador_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditindicador objloadauditindicador;
         objloadauditindicador = new loadauditindicador();
         objloadauditindicador.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditindicador.AV10AuditingObject = aP1_AuditingObject;
         objloadauditindicador.AV16Indicador_Codigo = aP2_Indicador_Codigo;
         objloadauditindicador.AV14ActualMode = aP3_ActualMode;
         objloadauditindicador.context.SetSubmitInitialConfig(context);
         objloadauditindicador.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditindicador);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditindicador)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00WZ2 */
         pr_default.execute(0, new Object[] {AV16Indicador_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2058Indicador_Codigo = P00WZ2_A2058Indicador_Codigo[0];
            A2059Indicador_Nome = P00WZ2_A2059Indicador_Nome[0];
            A2061Indicador_AreaTrabalhoCod = P00WZ2_A2061Indicador_AreaTrabalhoCod[0];
            n2061Indicador_AreaTrabalhoCod = P00WZ2_n2061Indicador_AreaTrabalhoCod[0];
            A2063Indicador_Refer = P00WZ2_A2063Indicador_Refer[0];
            n2063Indicador_Refer = P00WZ2_n2063Indicador_Refer[0];
            A2064Indicador_Aplica = P00WZ2_A2064Indicador_Aplica[0];
            n2064Indicador_Aplica = P00WZ2_n2064Indicador_Aplica[0];
            A2062Indicador_NaoCnfCod = P00WZ2_A2062Indicador_NaoCnfCod[0];
            n2062Indicador_NaoCnfCod = P00WZ2_n2062Indicador_NaoCnfCod[0];
            A2065Indicador_Tipo = P00WZ2_A2065Indicador_Tipo[0];
            n2065Indicador_Tipo = P00WZ2_n2065Indicador_Tipo[0];
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Indicador";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Id";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2059Indicador_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Refer";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Refere-se a";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Aplica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Aplica-se a";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_NaoCnfCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�o Conformidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Associado a";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2065Indicador_Tipo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00WZ3 */
         pr_default.execute(1, new Object[] {AV16Indicador_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A2058Indicador_Codigo = P00WZ3_A2058Indicador_Codigo[0];
            A2059Indicador_Nome = P00WZ3_A2059Indicador_Nome[0];
            A2061Indicador_AreaTrabalhoCod = P00WZ3_A2061Indicador_AreaTrabalhoCod[0];
            n2061Indicador_AreaTrabalhoCod = P00WZ3_n2061Indicador_AreaTrabalhoCod[0];
            A2063Indicador_Refer = P00WZ3_A2063Indicador_Refer[0];
            n2063Indicador_Refer = P00WZ3_n2063Indicador_Refer[0];
            A2064Indicador_Aplica = P00WZ3_A2064Indicador_Aplica[0];
            n2064Indicador_Aplica = P00WZ3_n2064Indicador_Aplica[0];
            A2062Indicador_NaoCnfCod = P00WZ3_A2062Indicador_NaoCnfCod[0];
            n2062Indicador_NaoCnfCod = P00WZ3_n2062Indicador_NaoCnfCod[0];
            A2065Indicador_Tipo = P00WZ3_A2065Indicador_Tipo[0];
            n2065Indicador_Tipo = P00WZ3_n2065Indicador_Tipo[0];
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Indicador";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Id";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2059Indicador_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Refer";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Refere-se a";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Aplica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Aplica-se a";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_NaoCnfCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�o Conformidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Indicador_Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Associado a";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2065Indicador_Tipo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00WZ3_A2058Indicador_Codigo[0] == A2058Indicador_Codigo ) )
                  {
                     A2059Indicador_Nome = P00WZ3_A2059Indicador_Nome[0];
                     A2061Indicador_AreaTrabalhoCod = P00WZ3_A2061Indicador_AreaTrabalhoCod[0];
                     n2061Indicador_AreaTrabalhoCod = P00WZ3_n2061Indicador_AreaTrabalhoCod[0];
                     A2063Indicador_Refer = P00WZ3_A2063Indicador_Refer[0];
                     n2063Indicador_Refer = P00WZ3_n2063Indicador_Refer[0];
                     A2064Indicador_Aplica = P00WZ3_A2064Indicador_Aplica[0];
                     n2064Indicador_Aplica = P00WZ3_n2064Indicador_Aplica[0];
                     A2062Indicador_NaoCnfCod = P00WZ3_A2062Indicador_NaoCnfCod[0];
                     n2062Indicador_NaoCnfCod = P00WZ3_n2062Indicador_NaoCnfCod[0];
                     A2065Indicador_Tipo = P00WZ3_A2065Indicador_Tipo[0];
                     n2065Indicador_Tipo = P00WZ3_n2065Indicador_Tipo[0];
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Indicador_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2058Indicador_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Indicador_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2059Indicador_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Indicador_AreaTrabalhoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2061Indicador_AreaTrabalhoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Indicador_Refer") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2063Indicador_Refer), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Indicador_Aplica") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2064Indicador_Aplica), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Indicador_NaoCnfCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2062Indicador_NaoCnfCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Indicador_Tipo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2065Indicador_Tipo;
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WZ2_A2058Indicador_Codigo = new int[1] ;
         P00WZ2_A2059Indicador_Nome = new String[] {""} ;
         P00WZ2_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         P00WZ2_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         P00WZ2_A2063Indicador_Refer = new short[1] ;
         P00WZ2_n2063Indicador_Refer = new bool[] {false} ;
         P00WZ2_A2064Indicador_Aplica = new short[1] ;
         P00WZ2_n2064Indicador_Aplica = new bool[] {false} ;
         P00WZ2_A2062Indicador_NaoCnfCod = new int[1] ;
         P00WZ2_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         P00WZ2_A2065Indicador_Tipo = new String[] {""} ;
         P00WZ2_n2065Indicador_Tipo = new bool[] {false} ;
         A2059Indicador_Nome = "";
         A2065Indicador_Tipo = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00WZ3_A2058Indicador_Codigo = new int[1] ;
         P00WZ3_A2059Indicador_Nome = new String[] {""} ;
         P00WZ3_A2061Indicador_AreaTrabalhoCod = new int[1] ;
         P00WZ3_n2061Indicador_AreaTrabalhoCod = new bool[] {false} ;
         P00WZ3_A2063Indicador_Refer = new short[1] ;
         P00WZ3_n2063Indicador_Refer = new bool[] {false} ;
         P00WZ3_A2064Indicador_Aplica = new short[1] ;
         P00WZ3_n2064Indicador_Aplica = new bool[] {false} ;
         P00WZ3_A2062Indicador_NaoCnfCod = new int[1] ;
         P00WZ3_n2062Indicador_NaoCnfCod = new bool[] {false} ;
         P00WZ3_A2065Indicador_Tipo = new String[] {""} ;
         P00WZ3_n2065Indicador_Tipo = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditindicador__default(),
            new Object[][] {
                new Object[] {
               P00WZ2_A2058Indicador_Codigo, P00WZ2_A2059Indicador_Nome, P00WZ2_A2061Indicador_AreaTrabalhoCod, P00WZ2_n2061Indicador_AreaTrabalhoCod, P00WZ2_A2063Indicador_Refer, P00WZ2_n2063Indicador_Refer, P00WZ2_A2064Indicador_Aplica, P00WZ2_n2064Indicador_Aplica, P00WZ2_A2062Indicador_NaoCnfCod, P00WZ2_n2062Indicador_NaoCnfCod,
               P00WZ2_A2065Indicador_Tipo, P00WZ2_n2065Indicador_Tipo
               }
               , new Object[] {
               P00WZ3_A2058Indicador_Codigo, P00WZ3_A2059Indicador_Nome, P00WZ3_A2061Indicador_AreaTrabalhoCod, P00WZ3_n2061Indicador_AreaTrabalhoCod, P00WZ3_A2063Indicador_Refer, P00WZ3_n2063Indicador_Refer, P00WZ3_A2064Indicador_Aplica, P00WZ3_n2064Indicador_Aplica, P00WZ3_A2062Indicador_NaoCnfCod, P00WZ3_n2062Indicador_NaoCnfCod,
               P00WZ3_A2065Indicador_Tipo, P00WZ3_n2065Indicador_Tipo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A2063Indicador_Refer ;
      private short A2064Indicador_Aplica ;
      private int AV16Indicador_Codigo ;
      private int A2058Indicador_Codigo ;
      private int A2061Indicador_AreaTrabalhoCod ;
      private int A2062Indicador_NaoCnfCod ;
      private int AV21GXV1 ;
      private int AV23GXV2 ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A2059Indicador_Nome ;
      private String A2065Indicador_Tipo ;
      private bool returnInSub ;
      private bool n2061Indicador_AreaTrabalhoCod ;
      private bool n2063Indicador_Refer ;
      private bool n2064Indicador_Aplica ;
      private bool n2062Indicador_NaoCnfCod ;
      private bool n2065Indicador_Tipo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00WZ2_A2058Indicador_Codigo ;
      private String[] P00WZ2_A2059Indicador_Nome ;
      private int[] P00WZ2_A2061Indicador_AreaTrabalhoCod ;
      private bool[] P00WZ2_n2061Indicador_AreaTrabalhoCod ;
      private short[] P00WZ2_A2063Indicador_Refer ;
      private bool[] P00WZ2_n2063Indicador_Refer ;
      private short[] P00WZ2_A2064Indicador_Aplica ;
      private bool[] P00WZ2_n2064Indicador_Aplica ;
      private int[] P00WZ2_A2062Indicador_NaoCnfCod ;
      private bool[] P00WZ2_n2062Indicador_NaoCnfCod ;
      private String[] P00WZ2_A2065Indicador_Tipo ;
      private bool[] P00WZ2_n2065Indicador_Tipo ;
      private int[] P00WZ3_A2058Indicador_Codigo ;
      private String[] P00WZ3_A2059Indicador_Nome ;
      private int[] P00WZ3_A2061Indicador_AreaTrabalhoCod ;
      private bool[] P00WZ3_n2061Indicador_AreaTrabalhoCod ;
      private short[] P00WZ3_A2063Indicador_Refer ;
      private bool[] P00WZ3_n2063Indicador_Refer ;
      private short[] P00WZ3_A2064Indicador_Aplica ;
      private bool[] P00WZ3_n2064Indicador_Aplica ;
      private int[] P00WZ3_A2062Indicador_NaoCnfCod ;
      private bool[] P00WZ3_n2062Indicador_NaoCnfCod ;
      private String[] P00WZ3_A2065Indicador_Tipo ;
      private bool[] P00WZ3_n2065Indicador_Tipo ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditindicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WZ2 ;
          prmP00WZ2 = new Object[] {
          new Object[] {"@AV16Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WZ3 ;
          prmP00WZ3 = new Object[] {
          new Object[] {"@AV16Indicador_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WZ2", "SELECT [Indicador_Codigo], [Indicador_Nome], [Indicador_AreaTrabalhoCod], [Indicador_Refer], [Indicador_Aplica], [Indicador_NaoCnfCod], [Indicador_Tipo] FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @AV16Indicador_Codigo ORDER BY [Indicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WZ2,1,0,false,true )
             ,new CursorDef("P00WZ3", "SELECT [Indicador_Codigo], [Indicador_Nome], [Indicador_AreaTrabalhoCod], [Indicador_Refer], [Indicador_Aplica], [Indicador_NaoCnfCod], [Indicador_Tipo] FROM [Indicador] WITH (NOLOCK) WHERE [Indicador_Codigo] = @AV16Indicador_Codigo ORDER BY [Indicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WZ3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
