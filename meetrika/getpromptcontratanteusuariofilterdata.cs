/*
               File: GetPromptContratanteUsuarioFilterData
        Description: Get Prompt Contratante Usuario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:54.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratanteusuariofilterdata : GXProcedure
   {
      public getpromptcontratanteusuariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratanteusuariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratanteusuariofilterdata objgetpromptcontratanteusuariofilterdata;
         objgetpromptcontratanteusuariofilterdata = new getpromptcontratanteusuariofilterdata();
         objgetpromptcontratanteusuariofilterdata.AV24DDOName = aP0_DDOName;
         objgetpromptcontratanteusuariofilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetpromptcontratanteusuariofilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratanteusuariofilterdata.AV28OptionsJson = "" ;
         objgetpromptcontratanteusuariofilterdata.AV31OptionsDescJson = "" ;
         objgetpromptcontratanteusuariofilterdata.AV33OptionIndexesJson = "" ;
         objgetpromptcontratanteusuariofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratanteusuariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratanteusuariofilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratanteusuariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTEUSUARIO_CONTRATANTEFAN") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTEUSUARIO_CONTRATANTEFANOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTEUSUARIO_CONTRATANTERAZOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATANTEUSUARIO_USUARIOPESSOANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("PromptContratanteUsuarioGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratanteUsuarioGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("PromptContratanteUsuarioGridState"), "");
         }
         AV56GXV1 = 1;
         while ( AV56GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV56GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTECOD") == 0 )
            {
               AV10TFContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratanteUsuario_ContratanteCod_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTEFAN") == 0 )
            {
               AV12TFContratanteUsuario_ContratanteFan = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTEFAN_SEL") == 0 )
            {
               AV13TFContratanteUsuario_ContratanteFan_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
            {
               AV14TFContratanteUsuario_ContratanteRaz = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_CONTRATANTERAZ_SEL") == 0 )
            {
               AV15TFContratanteUsuario_ContratanteRaz_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOCOD") == 0 )
            {
               AV16TFContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContratanteUsuario_UsuarioCod_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOACOD") == 0 )
            {
               AV18TFContratanteUsuario_UsuarioPessoaCod = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContratanteUsuario_UsuarioPessoaCod_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV20TFContratanteUsuario_UsuarioPessoaNom = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATANTEUSUARIO_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV21TFContratanteUsuario_UsuarioPessoaNom_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            AV56GXV1 = (int)(AV56GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV42ContratanteUsuario_ContratanteRaz1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV43ContratanteUsuario_UsuarioPessoaNom1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV47ContratanteUsuario_ContratanteRaz2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV48ContratanteUsuario_UsuarioPessoaNom2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV52ContratanteUsuario_ContratanteRaz3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV53ContratanteUsuario_UsuarioPessoaNom3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATANTEUSUARIO_CONTRATANTEFANOPTIONS' Routine */
         AV12TFContratanteUsuario_ContratanteFan = AV22SearchTxt;
         AV13TFContratanteUsuario_ContratanteFan_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratanteUsuario_ContratanteRaz1 ,
                                              AV43ContratanteUsuario_UsuarioPessoaNom1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47ContratanteUsuario_ContratanteRaz2 ,
                                              AV48ContratanteUsuario_UsuarioPessoaNom2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52ContratanteUsuario_ContratanteRaz3 ,
                                              AV53ContratanteUsuario_UsuarioPessoaNom3 ,
                                              AV10TFContratanteUsuario_ContratanteCod ,
                                              AV11TFContratanteUsuario_ContratanteCod_To ,
                                              AV13TFContratanteUsuario_ContratanteFan_Sel ,
                                              AV12TFContratanteUsuario_ContratanteFan ,
                                              AV15TFContratanteUsuario_ContratanteRaz_Sel ,
                                              AV14TFContratanteUsuario_ContratanteRaz ,
                                              AV16TFContratanteUsuario_UsuarioCod ,
                                              AV17TFContratanteUsuario_UsuarioCod_To ,
                                              AV18TFContratanteUsuario_UsuarioPessoaCod ,
                                              AV19TFContratanteUsuario_UsuarioPessoaCod_To ,
                                              AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ,
                                              AV20TFContratanteUsuario_UsuarioPessoaNom ,
                                              A64ContratanteUsuario_ContratanteRaz ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A65ContratanteUsuario_ContratanteFan ,
                                              A60ContratanteUsuario_UsuarioCod ,
                                              A61ContratanteUsuario_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV42ContratanteUsuario_ContratanteRaz1 = StringUtil.PadR( StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1), 100, "%");
         lV42ContratanteUsuario_ContratanteRaz1 = StringUtil.PadR( StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1), 100, "%");
         lV43ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
         lV43ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
         lV47ContratanteUsuario_ContratanteRaz2 = StringUtil.PadR( StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2), 100, "%");
         lV47ContratanteUsuario_ContratanteRaz2 = StringUtil.PadR( StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2), 100, "%");
         lV48ContratanteUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2), 100, "%");
         lV48ContratanteUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2), 100, "%");
         lV52ContratanteUsuario_ContratanteRaz3 = StringUtil.PadR( StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3), 100, "%");
         lV52ContratanteUsuario_ContratanteRaz3 = StringUtil.PadR( StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3), 100, "%");
         lV53ContratanteUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3), 100, "%");
         lV53ContratanteUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3), 100, "%");
         lV12TFContratanteUsuario_ContratanteFan = StringUtil.PadR( StringUtil.RTrim( AV12TFContratanteUsuario_ContratanteFan), 100, "%");
         lV14TFContratanteUsuario_ContratanteRaz = StringUtil.PadR( StringUtil.RTrim( AV14TFContratanteUsuario_ContratanteRaz), 100, "%");
         lV20TFContratanteUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratanteUsuario_UsuarioPessoaNom), 100, "%");
         /* Using cursor P00L52 */
         pr_default.execute(0, new Object[] {lV42ContratanteUsuario_ContratanteRaz1, lV42ContratanteUsuario_ContratanteRaz1, lV43ContratanteUsuario_UsuarioPessoaNom1, lV43ContratanteUsuario_UsuarioPessoaNom1, lV47ContratanteUsuario_ContratanteRaz2, lV47ContratanteUsuario_ContratanteRaz2, lV48ContratanteUsuario_UsuarioPessoaNom2, lV48ContratanteUsuario_UsuarioPessoaNom2, lV52ContratanteUsuario_ContratanteRaz3, lV52ContratanteUsuario_ContratanteRaz3, lV53ContratanteUsuario_UsuarioPessoaNom3, lV53ContratanteUsuario_UsuarioPessoaNom3, AV10TFContratanteUsuario_ContratanteCod, AV11TFContratanteUsuario_ContratanteCod_To, lV12TFContratanteUsuario_ContratanteFan, AV13TFContratanteUsuario_ContratanteFan_Sel, lV14TFContratanteUsuario_ContratanteRaz, AV15TFContratanteUsuario_ContratanteRaz_Sel, AV16TFContratanteUsuario_UsuarioCod, AV17TFContratanteUsuario_UsuarioCod_To, AV18TFContratanteUsuario_UsuarioPessoaCod, AV19TFContratanteUsuario_UsuarioPessoaCod_To, lV20TFContratanteUsuario_UsuarioPessoaNom, AV21TFContratanteUsuario_UsuarioPessoaNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKL52 = false;
            A340ContratanteUsuario_ContratantePesCod = P00L52_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L52_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L52_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L52_n65ContratanteUsuario_ContratanteFan[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L52_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L52_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A60ContratanteUsuario_UsuarioCod = P00L52_A60ContratanteUsuario_UsuarioCod[0];
            A63ContratanteUsuario_ContratanteCod = P00L52_A63ContratanteUsuario_ContratanteCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L52_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L52_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A64ContratanteUsuario_ContratanteRaz = P00L52_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L52_n64ContratanteUsuario_ContratanteRaz[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L52_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L52_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L52_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L52_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A340ContratanteUsuario_ContratantePesCod = P00L52_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L52_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L52_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L52_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = P00L52_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L52_n64ContratanteUsuario_ContratanteRaz[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00L52_A65ContratanteUsuario_ContratanteFan[0], A65ContratanteUsuario_ContratanteFan) == 0 ) )
            {
               BRKL52 = false;
               A60ContratanteUsuario_UsuarioCod = P00L52_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = P00L52_A63ContratanteUsuario_ContratanteCod[0];
               AV34count = (long)(AV34count+1);
               BRKL52 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan)) )
            {
               AV26Option = A65ContratanteUsuario_ContratanteFan;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL52 )
            {
               BRKL52 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATANTEUSUARIO_CONTRATANTERAZOPTIONS' Routine */
         AV14TFContratanteUsuario_ContratanteRaz = AV22SearchTxt;
         AV15TFContratanteUsuario_ContratanteRaz_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratanteUsuario_ContratanteRaz1 ,
                                              AV43ContratanteUsuario_UsuarioPessoaNom1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47ContratanteUsuario_ContratanteRaz2 ,
                                              AV48ContratanteUsuario_UsuarioPessoaNom2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52ContratanteUsuario_ContratanteRaz3 ,
                                              AV53ContratanteUsuario_UsuarioPessoaNom3 ,
                                              AV10TFContratanteUsuario_ContratanteCod ,
                                              AV11TFContratanteUsuario_ContratanteCod_To ,
                                              AV13TFContratanteUsuario_ContratanteFan_Sel ,
                                              AV12TFContratanteUsuario_ContratanteFan ,
                                              AV15TFContratanteUsuario_ContratanteRaz_Sel ,
                                              AV14TFContratanteUsuario_ContratanteRaz ,
                                              AV16TFContratanteUsuario_UsuarioCod ,
                                              AV17TFContratanteUsuario_UsuarioCod_To ,
                                              AV18TFContratanteUsuario_UsuarioPessoaCod ,
                                              AV19TFContratanteUsuario_UsuarioPessoaCod_To ,
                                              AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ,
                                              AV20TFContratanteUsuario_UsuarioPessoaNom ,
                                              A64ContratanteUsuario_ContratanteRaz ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A65ContratanteUsuario_ContratanteFan ,
                                              A60ContratanteUsuario_UsuarioCod ,
                                              A61ContratanteUsuario_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV42ContratanteUsuario_ContratanteRaz1 = StringUtil.PadR( StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1), 100, "%");
         lV42ContratanteUsuario_ContratanteRaz1 = StringUtil.PadR( StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1), 100, "%");
         lV43ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
         lV43ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
         lV47ContratanteUsuario_ContratanteRaz2 = StringUtil.PadR( StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2), 100, "%");
         lV47ContratanteUsuario_ContratanteRaz2 = StringUtil.PadR( StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2), 100, "%");
         lV48ContratanteUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2), 100, "%");
         lV48ContratanteUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2), 100, "%");
         lV52ContratanteUsuario_ContratanteRaz3 = StringUtil.PadR( StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3), 100, "%");
         lV52ContratanteUsuario_ContratanteRaz3 = StringUtil.PadR( StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3), 100, "%");
         lV53ContratanteUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3), 100, "%");
         lV53ContratanteUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3), 100, "%");
         lV12TFContratanteUsuario_ContratanteFan = StringUtil.PadR( StringUtil.RTrim( AV12TFContratanteUsuario_ContratanteFan), 100, "%");
         lV14TFContratanteUsuario_ContratanteRaz = StringUtil.PadR( StringUtil.RTrim( AV14TFContratanteUsuario_ContratanteRaz), 100, "%");
         lV20TFContratanteUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratanteUsuario_UsuarioPessoaNom), 100, "%");
         /* Using cursor P00L53 */
         pr_default.execute(1, new Object[] {lV42ContratanteUsuario_ContratanteRaz1, lV42ContratanteUsuario_ContratanteRaz1, lV43ContratanteUsuario_UsuarioPessoaNom1, lV43ContratanteUsuario_UsuarioPessoaNom1, lV47ContratanteUsuario_ContratanteRaz2, lV47ContratanteUsuario_ContratanteRaz2, lV48ContratanteUsuario_UsuarioPessoaNom2, lV48ContratanteUsuario_UsuarioPessoaNom2, lV52ContratanteUsuario_ContratanteRaz3, lV52ContratanteUsuario_ContratanteRaz3, lV53ContratanteUsuario_UsuarioPessoaNom3, lV53ContratanteUsuario_UsuarioPessoaNom3, AV10TFContratanteUsuario_ContratanteCod, AV11TFContratanteUsuario_ContratanteCod_To, lV12TFContratanteUsuario_ContratanteFan, AV13TFContratanteUsuario_ContratanteFan_Sel, lV14TFContratanteUsuario_ContratanteRaz, AV15TFContratanteUsuario_ContratanteRaz_Sel, AV16TFContratanteUsuario_UsuarioCod, AV17TFContratanteUsuario_UsuarioCod_To, AV18TFContratanteUsuario_UsuarioPessoaCod, AV19TFContratanteUsuario_UsuarioPessoaCod_To, lV20TFContratanteUsuario_UsuarioPessoaNom, AV21TFContratanteUsuario_UsuarioPessoaNom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKL54 = false;
            A340ContratanteUsuario_ContratantePesCod = P00L53_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L53_n340ContratanteUsuario_ContratantePesCod[0];
            A64ContratanteUsuario_ContratanteRaz = P00L53_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L53_n64ContratanteUsuario_ContratanteRaz[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L53_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L53_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A60ContratanteUsuario_UsuarioCod = P00L53_A60ContratanteUsuario_UsuarioCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L53_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L53_n65ContratanteUsuario_ContratanteFan[0];
            A63ContratanteUsuario_ContratanteCod = P00L53_A63ContratanteUsuario_ContratanteCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L53_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L53_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L53_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L53_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L53_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L53_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A340ContratanteUsuario_ContratantePesCod = P00L53_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L53_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L53_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L53_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = P00L53_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L53_n64ContratanteUsuario_ContratanteRaz[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00L53_A64ContratanteUsuario_ContratanteRaz[0], A64ContratanteUsuario_ContratanteRaz) == 0 ) )
            {
               BRKL54 = false;
               A340ContratanteUsuario_ContratantePesCod = P00L53_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = P00L53_n340ContratanteUsuario_ContratantePesCod[0];
               A60ContratanteUsuario_UsuarioCod = P00L53_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = P00L53_A63ContratanteUsuario_ContratanteCod[0];
               A340ContratanteUsuario_ContratantePesCod = P00L53_A340ContratanteUsuario_ContratantePesCod[0];
               n340ContratanteUsuario_ContratantePesCod = P00L53_n340ContratanteUsuario_ContratantePesCod[0];
               AV34count = (long)(AV34count+1);
               BRKL54 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz)) )
            {
               AV26Option = A64ContratanteUsuario_ContratanteRaz;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL54 )
            {
               BRKL54 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATANTEUSUARIO_USUARIOPESSOANOMOPTIONS' Routine */
         AV20TFContratanteUsuario_UsuarioPessoaNom = AV22SearchTxt;
         AV21TFContratanteUsuario_UsuarioPessoaNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratanteUsuario_ContratanteRaz1 ,
                                              AV43ContratanteUsuario_UsuarioPessoaNom1 ,
                                              AV44DynamicFiltersEnabled2 ,
                                              AV45DynamicFiltersSelector2 ,
                                              AV46DynamicFiltersOperator2 ,
                                              AV47ContratanteUsuario_ContratanteRaz2 ,
                                              AV48ContratanteUsuario_UsuarioPessoaNom2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52ContratanteUsuario_ContratanteRaz3 ,
                                              AV53ContratanteUsuario_UsuarioPessoaNom3 ,
                                              AV10TFContratanteUsuario_ContratanteCod ,
                                              AV11TFContratanteUsuario_ContratanteCod_To ,
                                              AV13TFContratanteUsuario_ContratanteFan_Sel ,
                                              AV12TFContratanteUsuario_ContratanteFan ,
                                              AV15TFContratanteUsuario_ContratanteRaz_Sel ,
                                              AV14TFContratanteUsuario_ContratanteRaz ,
                                              AV16TFContratanteUsuario_UsuarioCod ,
                                              AV17TFContratanteUsuario_UsuarioCod_To ,
                                              AV18TFContratanteUsuario_UsuarioPessoaCod ,
                                              AV19TFContratanteUsuario_UsuarioPessoaCod_To ,
                                              AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ,
                                              AV20TFContratanteUsuario_UsuarioPessoaNom ,
                                              A64ContratanteUsuario_ContratanteRaz ,
                                              A62ContratanteUsuario_UsuarioPessoaNom ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A65ContratanteUsuario_ContratanteFan ,
                                              A60ContratanteUsuario_UsuarioCod ,
                                              A61ContratanteUsuario_UsuarioPessoaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV42ContratanteUsuario_ContratanteRaz1 = StringUtil.PadR( StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1), 100, "%");
         lV42ContratanteUsuario_ContratanteRaz1 = StringUtil.PadR( StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1), 100, "%");
         lV43ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
         lV43ContratanteUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1), 100, "%");
         lV47ContratanteUsuario_ContratanteRaz2 = StringUtil.PadR( StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2), 100, "%");
         lV47ContratanteUsuario_ContratanteRaz2 = StringUtil.PadR( StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2), 100, "%");
         lV48ContratanteUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2), 100, "%");
         lV48ContratanteUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2), 100, "%");
         lV52ContratanteUsuario_ContratanteRaz3 = StringUtil.PadR( StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3), 100, "%");
         lV52ContratanteUsuario_ContratanteRaz3 = StringUtil.PadR( StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3), 100, "%");
         lV53ContratanteUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3), 100, "%");
         lV53ContratanteUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3), 100, "%");
         lV12TFContratanteUsuario_ContratanteFan = StringUtil.PadR( StringUtil.RTrim( AV12TFContratanteUsuario_ContratanteFan), 100, "%");
         lV14TFContratanteUsuario_ContratanteRaz = StringUtil.PadR( StringUtil.RTrim( AV14TFContratanteUsuario_ContratanteRaz), 100, "%");
         lV20TFContratanteUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratanteUsuario_UsuarioPessoaNom), 100, "%");
         /* Using cursor P00L54 */
         pr_default.execute(2, new Object[] {lV42ContratanteUsuario_ContratanteRaz1, lV42ContratanteUsuario_ContratanteRaz1, lV43ContratanteUsuario_UsuarioPessoaNom1, lV43ContratanteUsuario_UsuarioPessoaNom1, lV47ContratanteUsuario_ContratanteRaz2, lV47ContratanteUsuario_ContratanteRaz2, lV48ContratanteUsuario_UsuarioPessoaNom2, lV48ContratanteUsuario_UsuarioPessoaNom2, lV52ContratanteUsuario_ContratanteRaz3, lV52ContratanteUsuario_ContratanteRaz3, lV53ContratanteUsuario_UsuarioPessoaNom3, lV53ContratanteUsuario_UsuarioPessoaNom3, AV10TFContratanteUsuario_ContratanteCod, AV11TFContratanteUsuario_ContratanteCod_To, lV12TFContratanteUsuario_ContratanteFan, AV13TFContratanteUsuario_ContratanteFan_Sel, lV14TFContratanteUsuario_ContratanteRaz, AV15TFContratanteUsuario_ContratanteRaz_Sel, AV16TFContratanteUsuario_UsuarioCod, AV17TFContratanteUsuario_UsuarioCod_To, AV18TFContratanteUsuario_UsuarioPessoaCod, AV19TFContratanteUsuario_UsuarioPessoaCod_To, lV20TFContratanteUsuario_UsuarioPessoaNom, AV21TFContratanteUsuario_UsuarioPessoaNom_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKL56 = false;
            A340ContratanteUsuario_ContratantePesCod = P00L54_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L54_n340ContratanteUsuario_ContratantePesCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L54_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L54_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L54_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L54_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A60ContratanteUsuario_UsuarioCod = P00L54_A60ContratanteUsuario_UsuarioCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L54_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L54_n65ContratanteUsuario_ContratanteFan[0];
            A63ContratanteUsuario_ContratanteCod = P00L54_A63ContratanteUsuario_ContratanteCod[0];
            A64ContratanteUsuario_ContratanteRaz = P00L54_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L54_n64ContratanteUsuario_ContratanteRaz[0];
            A61ContratanteUsuario_UsuarioPessoaCod = P00L54_A61ContratanteUsuario_UsuarioPessoaCod[0];
            n61ContratanteUsuario_UsuarioPessoaCod = P00L54_n61ContratanteUsuario_UsuarioPessoaCod[0];
            A62ContratanteUsuario_UsuarioPessoaNom = P00L54_A62ContratanteUsuario_UsuarioPessoaNom[0];
            n62ContratanteUsuario_UsuarioPessoaNom = P00L54_n62ContratanteUsuario_UsuarioPessoaNom[0];
            A340ContratanteUsuario_ContratantePesCod = P00L54_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = P00L54_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = P00L54_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = P00L54_n65ContratanteUsuario_ContratanteFan[0];
            A64ContratanteUsuario_ContratanteRaz = P00L54_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = P00L54_n64ContratanteUsuario_ContratanteRaz[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00L54_A62ContratanteUsuario_UsuarioPessoaNom[0], A62ContratanteUsuario_UsuarioPessoaNom) == 0 ) )
            {
               BRKL56 = false;
               A61ContratanteUsuario_UsuarioPessoaCod = P00L54_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = P00L54_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A60ContratanteUsuario_UsuarioCod = P00L54_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = P00L54_A63ContratanteUsuario_ContratanteCod[0];
               A61ContratanteUsuario_UsuarioPessoaCod = P00L54_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = P00L54_n61ContratanteUsuario_UsuarioPessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKL56 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom)) )
            {
               AV26Option = A62ContratanteUsuario_UsuarioPessoaNom;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKL56 )
            {
               BRKL56 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFContratanteUsuario_ContratanteFan = "";
         AV13TFContratanteUsuario_ContratanteFan_Sel = "";
         AV14TFContratanteUsuario_ContratanteRaz = "";
         AV15TFContratanteUsuario_ContratanteRaz_Sel = "";
         AV20TFContratanteUsuario_UsuarioPessoaNom = "";
         AV21TFContratanteUsuario_UsuarioPessoaNom_Sel = "";
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV42ContratanteUsuario_ContratanteRaz1 = "";
         AV43ContratanteUsuario_UsuarioPessoaNom1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV47ContratanteUsuario_ContratanteRaz2 = "";
         AV48ContratanteUsuario_UsuarioPessoaNom2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52ContratanteUsuario_ContratanteRaz3 = "";
         AV53ContratanteUsuario_UsuarioPessoaNom3 = "";
         scmdbuf = "";
         lV42ContratanteUsuario_ContratanteRaz1 = "";
         lV43ContratanteUsuario_UsuarioPessoaNom1 = "";
         lV47ContratanteUsuario_ContratanteRaz2 = "";
         lV48ContratanteUsuario_UsuarioPessoaNom2 = "";
         lV52ContratanteUsuario_ContratanteRaz3 = "";
         lV53ContratanteUsuario_UsuarioPessoaNom3 = "";
         lV12TFContratanteUsuario_ContratanteFan = "";
         lV14TFContratanteUsuario_ContratanteRaz = "";
         lV20TFContratanteUsuario_UsuarioPessoaNom = "";
         A64ContratanteUsuario_ContratanteRaz = "";
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         A65ContratanteUsuario_ContratanteFan = "";
         P00L52_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         P00L52_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         P00L52_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         P00L52_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         P00L52_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         P00L52_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00L52_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00L52_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00L52_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00L52_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00L52_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         P00L52_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         AV26Option = "";
         P00L53_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         P00L53_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         P00L53_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         P00L53_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         P00L53_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         P00L53_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00L53_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00L53_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         P00L53_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         P00L53_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00L53_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00L53_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00L54_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         P00L54_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         P00L54_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00L54_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00L54_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         P00L54_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00L54_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00L54_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         P00L54_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         P00L54_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00L54_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         P00L54_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratanteusuariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00L52_A340ContratanteUsuario_ContratantePesCod, P00L52_n340ContratanteUsuario_ContratantePesCod, P00L52_A65ContratanteUsuario_ContratanteFan, P00L52_n65ContratanteUsuario_ContratanteFan, P00L52_A61ContratanteUsuario_UsuarioPessoaCod, P00L52_n61ContratanteUsuario_UsuarioPessoaCod, P00L52_A60ContratanteUsuario_UsuarioCod, P00L52_A63ContratanteUsuario_ContratanteCod, P00L52_A62ContratanteUsuario_UsuarioPessoaNom, P00L52_n62ContratanteUsuario_UsuarioPessoaNom,
               P00L52_A64ContratanteUsuario_ContratanteRaz, P00L52_n64ContratanteUsuario_ContratanteRaz
               }
               , new Object[] {
               P00L53_A340ContratanteUsuario_ContratantePesCod, P00L53_n340ContratanteUsuario_ContratantePesCod, P00L53_A64ContratanteUsuario_ContratanteRaz, P00L53_n64ContratanteUsuario_ContratanteRaz, P00L53_A61ContratanteUsuario_UsuarioPessoaCod, P00L53_n61ContratanteUsuario_UsuarioPessoaCod, P00L53_A60ContratanteUsuario_UsuarioCod, P00L53_A65ContratanteUsuario_ContratanteFan, P00L53_n65ContratanteUsuario_ContratanteFan, P00L53_A63ContratanteUsuario_ContratanteCod,
               P00L53_A62ContratanteUsuario_UsuarioPessoaNom, P00L53_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               P00L54_A340ContratanteUsuario_ContratantePesCod, P00L54_n340ContratanteUsuario_ContratantePesCod, P00L54_A62ContratanteUsuario_UsuarioPessoaNom, P00L54_n62ContratanteUsuario_UsuarioPessoaNom, P00L54_A61ContratanteUsuario_UsuarioPessoaCod, P00L54_n61ContratanteUsuario_UsuarioPessoaCod, P00L54_A60ContratanteUsuario_UsuarioCod, P00L54_A65ContratanteUsuario_ContratanteFan, P00L54_n65ContratanteUsuario_ContratanteFan, P00L54_A63ContratanteUsuario_ContratanteCod,
               P00L54_A64ContratanteUsuario_ContratanteRaz, P00L54_n64ContratanteUsuario_ContratanteRaz
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV41DynamicFiltersOperator1 ;
      private short AV46DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private int AV56GXV1 ;
      private int AV10TFContratanteUsuario_ContratanteCod ;
      private int AV11TFContratanteUsuario_ContratanteCod_To ;
      private int AV16TFContratanteUsuario_UsuarioCod ;
      private int AV17TFContratanteUsuario_UsuarioCod_To ;
      private int AV18TFContratanteUsuario_UsuarioPessoaCod ;
      private int AV19TFContratanteUsuario_UsuarioPessoaCod_To ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private long AV34count ;
      private String AV12TFContratanteUsuario_ContratanteFan ;
      private String AV13TFContratanteUsuario_ContratanteFan_Sel ;
      private String AV14TFContratanteUsuario_ContratanteRaz ;
      private String AV15TFContratanteUsuario_ContratanteRaz_Sel ;
      private String AV20TFContratanteUsuario_UsuarioPessoaNom ;
      private String AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ;
      private String AV42ContratanteUsuario_ContratanteRaz1 ;
      private String AV43ContratanteUsuario_UsuarioPessoaNom1 ;
      private String AV47ContratanteUsuario_ContratanteRaz2 ;
      private String AV48ContratanteUsuario_UsuarioPessoaNom2 ;
      private String AV52ContratanteUsuario_ContratanteRaz3 ;
      private String AV53ContratanteUsuario_UsuarioPessoaNom3 ;
      private String scmdbuf ;
      private String lV42ContratanteUsuario_ContratanteRaz1 ;
      private String lV43ContratanteUsuario_UsuarioPessoaNom1 ;
      private String lV47ContratanteUsuario_ContratanteRaz2 ;
      private String lV48ContratanteUsuario_UsuarioPessoaNom2 ;
      private String lV52ContratanteUsuario_ContratanteRaz3 ;
      private String lV53ContratanteUsuario_UsuarioPessoaNom3 ;
      private String lV12TFContratanteUsuario_ContratanteFan ;
      private String lV14TFContratanteUsuario_ContratanteRaz ;
      private String lV20TFContratanteUsuario_UsuarioPessoaNom ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool BRKL52 ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool BRKL54 ;
      private bool BRKL56 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00L52_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] P00L52_n340ContratanteUsuario_ContratantePesCod ;
      private String[] P00L52_A65ContratanteUsuario_ContratanteFan ;
      private bool[] P00L52_n65ContratanteUsuario_ContratanteFan ;
      private int[] P00L52_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] P00L52_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] P00L52_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00L52_A63ContratanteUsuario_ContratanteCod ;
      private String[] P00L52_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] P00L52_n62ContratanteUsuario_UsuarioPessoaNom ;
      private String[] P00L52_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] P00L52_n64ContratanteUsuario_ContratanteRaz ;
      private int[] P00L53_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] P00L53_n340ContratanteUsuario_ContratantePesCod ;
      private String[] P00L53_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] P00L53_n64ContratanteUsuario_ContratanteRaz ;
      private int[] P00L53_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] P00L53_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] P00L53_A60ContratanteUsuario_UsuarioCod ;
      private String[] P00L53_A65ContratanteUsuario_ContratanteFan ;
      private bool[] P00L53_n65ContratanteUsuario_ContratanteFan ;
      private int[] P00L53_A63ContratanteUsuario_ContratanteCod ;
      private String[] P00L53_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] P00L53_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] P00L54_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] P00L54_n340ContratanteUsuario_ContratantePesCod ;
      private String[] P00L54_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] P00L54_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] P00L54_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] P00L54_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] P00L54_A60ContratanteUsuario_UsuarioCod ;
      private String[] P00L54_A65ContratanteUsuario_ContratanteFan ;
      private bool[] P00L54_n65ContratanteUsuario_ContratanteFan ;
      private int[] P00L54_A63ContratanteUsuario_ContratanteCod ;
      private String[] P00L54_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] P00L54_n64ContratanteUsuario_ContratanteRaz ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getpromptcontratanteusuariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00L52( IGxContext context ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             String AV42ContratanteUsuario_ContratanteRaz1 ,
                                             String AV43ContratanteUsuario_UsuarioPessoaNom1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47ContratanteUsuario_ContratanteRaz2 ,
                                             String AV48ContratanteUsuario_UsuarioPessoaNom2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52ContratanteUsuario_ContratanteRaz3 ,
                                             String AV53ContratanteUsuario_UsuarioPessoaNom3 ,
                                             int AV10TFContratanteUsuario_ContratanteCod ,
                                             int AV11TFContratanteUsuario_ContratanteCod_To ,
                                             String AV13TFContratanteUsuario_ContratanteFan_Sel ,
                                             String AV12TFContratanteUsuario_ContratanteFan ,
                                             String AV15TFContratanteUsuario_ContratanteRaz_Sel ,
                                             String AV14TFContratanteUsuario_ContratanteRaz ,
                                             int AV16TFContratanteUsuario_UsuarioCod ,
                                             int AV17TFContratanteUsuario_UsuarioCod_To ,
                                             int AV18TFContratanteUsuario_UsuarioPessoaCod ,
                                             int AV19TFContratanteUsuario_UsuarioPessoaCod_To ,
                                             String AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ,
                                             String AV20TFContratanteUsuario_UsuarioPessoaNom ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T4.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T4.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T5.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV42ContratanteUsuario_ContratanteRaz1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV42ContratanteUsuario_ContratanteRaz1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV42ContratanteUsuario_ContratanteRaz1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV42ContratanteUsuario_ContratanteRaz1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV47ContratanteUsuario_ContratanteRaz2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV47ContratanteUsuario_ContratanteRaz2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV47ContratanteUsuario_ContratanteRaz2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV47ContratanteUsuario_ContratanteRaz2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV52ContratanteUsuario_ContratanteRaz3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV52ContratanteUsuario_ContratanteRaz3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV52ContratanteUsuario_ContratanteRaz3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV52ContratanteUsuario_ContratanteRaz3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV10TFContratanteUsuario_ContratanteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV10TFContratanteUsuario_ContratanteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV10TFContratanteUsuario_ContratanteCod)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV11TFContratanteUsuario_ContratanteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV11TFContratanteUsuario_ContratanteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV11TFContratanteUsuario_ContratanteCod_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratanteUsuario_ContratanteFan_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratanteUsuario_ContratanteFan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] like @lV12TFContratanteUsuario_ContratanteFan)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] like @lV12TFContratanteUsuario_ContratanteFan)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratanteUsuario_ContratanteFan_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] = @AV13TFContratanteUsuario_ContratanteFan_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] = @AV13TFContratanteUsuario_ContratanteFan_Sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratanteUsuario_ContratanteRaz_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratanteUsuario_ContratanteRaz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV14TFContratanteUsuario_ContratanteRaz)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV14TFContratanteUsuario_ContratanteRaz)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratanteUsuario_ContratanteRaz_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV15TFContratanteUsuario_ContratanteRaz_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV15TFContratanteUsuario_ContratanteRaz_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV16TFContratanteUsuario_UsuarioCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV16TFContratanteUsuario_UsuarioCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV16TFContratanteUsuario_UsuarioCod)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV17TFContratanteUsuario_UsuarioCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV17TFContratanteUsuario_UsuarioCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV17TFContratanteUsuario_UsuarioCod_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (0==AV18TFContratanteUsuario_UsuarioPessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV18TFContratanteUsuario_UsuarioPessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV18TFContratanteUsuario_UsuarioPessoaCod)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (0==AV19TFContratanteUsuario_UsuarioPessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV19TFContratanteUsuario_UsuarioPessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV19TFContratanteUsuario_UsuarioPessoaCod_To)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratanteUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFContratanteUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV20TFContratanteUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Contratante_NomeFantasia]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00L53( IGxContext context ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             String AV42ContratanteUsuario_ContratanteRaz1 ,
                                             String AV43ContratanteUsuario_UsuarioPessoaNom1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47ContratanteUsuario_ContratanteRaz2 ,
                                             String AV48ContratanteUsuario_UsuarioPessoaNom2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52ContratanteUsuario_ContratanteRaz3 ,
                                             String AV53ContratanteUsuario_UsuarioPessoaNom3 ,
                                             int AV10TFContratanteUsuario_ContratanteCod ,
                                             int AV11TFContratanteUsuario_ContratanteCod_To ,
                                             String AV13TFContratanteUsuario_ContratanteFan_Sel ,
                                             String AV12TFContratanteUsuario_ContratanteFan ,
                                             String AV15TFContratanteUsuario_ContratanteRaz_Sel ,
                                             String AV14TFContratanteUsuario_ContratanteRaz ,
                                             int AV16TFContratanteUsuario_UsuarioCod ,
                                             int AV17TFContratanteUsuario_UsuarioCod_To ,
                                             int AV18TFContratanteUsuario_UsuarioPessoaCod ,
                                             int AV19TFContratanteUsuario_UsuarioPessoaCod_To ,
                                             String AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ,
                                             String AV20TFContratanteUsuario_UsuarioPessoaNom ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T4.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T5.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T4.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV42ContratanteUsuario_ContratanteRaz1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV42ContratanteUsuario_ContratanteRaz1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV42ContratanteUsuario_ContratanteRaz1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV42ContratanteUsuario_ContratanteRaz1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV47ContratanteUsuario_ContratanteRaz2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV47ContratanteUsuario_ContratanteRaz2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV47ContratanteUsuario_ContratanteRaz2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV47ContratanteUsuario_ContratanteRaz2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV52ContratanteUsuario_ContratanteRaz3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV52ContratanteUsuario_ContratanteRaz3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV52ContratanteUsuario_ContratanteRaz3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV52ContratanteUsuario_ContratanteRaz3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV10TFContratanteUsuario_ContratanteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV10TFContratanteUsuario_ContratanteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV10TFContratanteUsuario_ContratanteCod)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV11TFContratanteUsuario_ContratanteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV11TFContratanteUsuario_ContratanteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV11TFContratanteUsuario_ContratanteCod_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratanteUsuario_ContratanteFan_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratanteUsuario_ContratanteFan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] like @lV12TFContratanteUsuario_ContratanteFan)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] like @lV12TFContratanteUsuario_ContratanteFan)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratanteUsuario_ContratanteFan_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] = @AV13TFContratanteUsuario_ContratanteFan_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] = @AV13TFContratanteUsuario_ContratanteFan_Sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratanteUsuario_ContratanteRaz_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratanteUsuario_ContratanteRaz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV14TFContratanteUsuario_ContratanteRaz)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV14TFContratanteUsuario_ContratanteRaz)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratanteUsuario_ContratanteRaz_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV15TFContratanteUsuario_ContratanteRaz_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV15TFContratanteUsuario_ContratanteRaz_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV16TFContratanteUsuario_UsuarioCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV16TFContratanteUsuario_UsuarioCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV16TFContratanteUsuario_UsuarioCod)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV17TFContratanteUsuario_UsuarioCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV17TFContratanteUsuario_UsuarioCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV17TFContratanteUsuario_UsuarioCod_To)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV18TFContratanteUsuario_UsuarioPessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV18TFContratanteUsuario_UsuarioPessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV18TFContratanteUsuario_UsuarioPessoaCod)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV19TFContratanteUsuario_UsuarioPessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV19TFContratanteUsuario_UsuarioPessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV19TFContratanteUsuario_UsuarioPessoaCod_To)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratanteUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFContratanteUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV20TFContratanteUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00L54( IGxContext context ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             String AV42ContratanteUsuario_ContratanteRaz1 ,
                                             String AV43ContratanteUsuario_UsuarioPessoaNom1 ,
                                             bool AV44DynamicFiltersEnabled2 ,
                                             String AV45DynamicFiltersSelector2 ,
                                             short AV46DynamicFiltersOperator2 ,
                                             String AV47ContratanteUsuario_ContratanteRaz2 ,
                                             String AV48ContratanteUsuario_UsuarioPessoaNom2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52ContratanteUsuario_ContratanteRaz3 ,
                                             String AV53ContratanteUsuario_UsuarioPessoaNom3 ,
                                             int AV10TFContratanteUsuario_ContratanteCod ,
                                             int AV11TFContratanteUsuario_ContratanteCod_To ,
                                             String AV13TFContratanteUsuario_ContratanteFan_Sel ,
                                             String AV12TFContratanteUsuario_ContratanteFan ,
                                             String AV15TFContratanteUsuario_ContratanteRaz_Sel ,
                                             String AV14TFContratanteUsuario_ContratanteRaz ,
                                             int AV16TFContratanteUsuario_UsuarioCod ,
                                             int AV17TFContratanteUsuario_UsuarioCod_To ,
                                             int AV18TFContratanteUsuario_UsuarioPessoaCod ,
                                             int AV19TFContratanteUsuario_UsuarioPessoaCod_To ,
                                             String AV21TFContratanteUsuario_UsuarioPessoaNom_Sel ,
                                             String AV20TFContratanteUsuario_UsuarioPessoaNom ,
                                             String A64ContratanteUsuario_ContratanteRaz ,
                                             String A62ContratanteUsuario_UsuarioPessoaNom ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             String A65ContratanteUsuario_ContratanteFan ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             int A61ContratanteUsuario_UsuarioPessoaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T4.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T4.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T5.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz FROM (((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratante] T4 WITH (NOLOCK) ON T4.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratante_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV42ContratanteUsuario_ContratanteRaz1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV42ContratanteUsuario_ContratanteRaz1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContratanteUsuario_ContratanteRaz1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV42ContratanteUsuario_ContratanteRaz1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV42ContratanteUsuario_ContratanteRaz1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ContratanteUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV43ContratanteUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV47ContratanteUsuario_ContratanteRaz2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV47ContratanteUsuario_ContratanteRaz2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContratanteUsuario_ContratanteRaz2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV47ContratanteUsuario_ContratanteRaz2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV47ContratanteUsuario_ContratanteRaz2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV46DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV46DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContratanteUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV48ContratanteUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV52ContratanteUsuario_ContratanteRaz3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV52ContratanteUsuario_ContratanteRaz3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_CONTRATANTERAZ") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContratanteUsuario_ContratanteRaz3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV52ContratanteUsuario_ContratanteRaz3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV52ContratanteUsuario_ContratanteRaz3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTRATANTEUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContratanteUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV53ContratanteUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV10TFContratanteUsuario_ContratanteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] >= @AV10TFContratanteUsuario_ContratanteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] >= @AV10TFContratanteUsuario_ContratanteCod)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV11TFContratanteUsuario_ContratanteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] <= @AV11TFContratanteUsuario_ContratanteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_ContratanteCod] <= @AV11TFContratanteUsuario_ContratanteCod_To)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratanteUsuario_ContratanteFan_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratanteUsuario_ContratanteFan)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] like @lV12TFContratanteUsuario_ContratanteFan)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] like @lV12TFContratanteUsuario_ContratanteFan)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratanteUsuario_ContratanteFan_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contratante_NomeFantasia] = @AV13TFContratanteUsuario_ContratanteFan_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contratante_NomeFantasia] = @AV13TFContratanteUsuario_ContratanteFan_Sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratanteUsuario_ContratanteRaz_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratanteUsuario_ContratanteRaz)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV14TFContratanteUsuario_ContratanteRaz)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV14TFContratanteUsuario_ContratanteRaz)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratanteUsuario_ContratanteRaz_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV15TFContratanteUsuario_ContratanteRaz_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV15TFContratanteUsuario_ContratanteRaz_Sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV16TFContratanteUsuario_UsuarioCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] >= @AV16TFContratanteUsuario_UsuarioCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] >= @AV16TFContratanteUsuario_UsuarioCod)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV17TFContratanteUsuario_UsuarioCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratanteUsuario_UsuarioCod] <= @AV17TFContratanteUsuario_UsuarioCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratanteUsuario_UsuarioCod] <= @AV17TFContratanteUsuario_UsuarioCod_To)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (0==AV18TFContratanteUsuario_UsuarioPessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV18TFContratanteUsuario_UsuarioPessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] >= @AV18TFContratanteUsuario_UsuarioPessoaCod)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (0==AV19TFContratanteUsuario_UsuarioPessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV19TFContratanteUsuario_UsuarioPessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_PessoaCod] <= @AV19TFContratanteUsuario_UsuarioPessoaCod_To)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratanteUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFContratanteUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV20TFContratanteUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV21TFContratanteUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00L52(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] );
               case 1 :
                     return conditional_P00L53(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] );
               case 2 :
                     return conditional_P00L54(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00L52 ;
          prmP00L52 = new Object[] {
          new Object[] {"@lV42ContratanteUsuario_ContratanteRaz1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV42ContratanteUsuario_ContratanteRaz1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV47ContratanteUsuario_ContratanteRaz2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV47ContratanteUsuario_ContratanteRaz2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48ContratanteUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48ContratanteUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52ContratanteUsuario_ContratanteRaz3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52ContratanteUsuario_ContratanteRaz3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53ContratanteUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53ContratanteUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratanteUsuario_ContratanteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFContratanteUsuario_ContratanteFan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratanteUsuario_ContratanteFan_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratanteUsuario_ContratanteRaz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFContratanteUsuario_ContratanteRaz_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV16TFContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratanteUsuario_UsuarioCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratanteUsuario_UsuarioPessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratanteUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratanteUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00L53 ;
          prmP00L53 = new Object[] {
          new Object[] {"@lV42ContratanteUsuario_ContratanteRaz1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV42ContratanteUsuario_ContratanteRaz1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV47ContratanteUsuario_ContratanteRaz2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV47ContratanteUsuario_ContratanteRaz2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48ContratanteUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48ContratanteUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52ContratanteUsuario_ContratanteRaz3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52ContratanteUsuario_ContratanteRaz3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53ContratanteUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53ContratanteUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratanteUsuario_ContratanteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFContratanteUsuario_ContratanteFan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratanteUsuario_ContratanteFan_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratanteUsuario_ContratanteRaz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFContratanteUsuario_ContratanteRaz_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV16TFContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratanteUsuario_UsuarioCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratanteUsuario_UsuarioPessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratanteUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratanteUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00L54 ;
          prmP00L54 = new Object[] {
          new Object[] {"@lV42ContratanteUsuario_ContratanteRaz1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV42ContratanteUsuario_ContratanteRaz1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV43ContratanteUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV47ContratanteUsuario_ContratanteRaz2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV47ContratanteUsuario_ContratanteRaz2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48ContratanteUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV48ContratanteUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52ContratanteUsuario_ContratanteRaz3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV52ContratanteUsuario_ContratanteRaz3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53ContratanteUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV53ContratanteUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratanteUsuario_ContratanteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFContratanteUsuario_ContratanteFan",SqlDbType.Char,100,0} ,
          new Object[] {"@AV13TFContratanteUsuario_ContratanteFan_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV14TFContratanteUsuario_ContratanteRaz",SqlDbType.Char,100,0} ,
          new Object[] {"@AV15TFContratanteUsuario_ContratanteRaz_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV16TFContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratanteUsuario_UsuarioCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratanteUsuario_UsuarioPessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratanteUsuario_UsuarioPessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratanteUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratanteUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00L52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L52,100,0,true,false )
             ,new CursorDef("P00L53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L53,100,0,true,false )
             ,new CursorDef("P00L54", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00L54,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratanteusuariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratanteusuariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratanteusuariofilterdata") )
          {
             return  ;
          }
          getpromptcontratanteusuariofilterdata worker = new getpromptcontratanteusuariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
