/*
               File: WP_AlteraDataCnt
        Description: Alterar data da contagem:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:40:53.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_alteradatacnt : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_alteradatacnt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_alteradatacnt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_ContagemResultado_DataCnt ,
                           String aP2_ContagemResultado_HoraCnt ,
                           String aP3_ContagemResultado_OsFsOsFm ,
                           String aP4_ContagemResultado_ContratadaPessoaNom )
      {
         this.AV6ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV18ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.AV19ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         this.AV21ContagemResultado_OsFsOsFm = aP3_ContagemResultado_OsFsOsFm;
         this.AV22ContagemResultado_ContratadaPessoaNom = aP4_ContagemResultado_ContratadaPessoaNom;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV6ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV18ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_DataCnt", context.localUtil.Format(AV18ContagemResultado_DataCnt, "99/99/99"));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV18ContagemResultado_DataCnt));
                  AV19ContagemResultado_HoraCnt = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_HoraCnt", AV19ContagemResultado_HoraCnt);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV19ContagemResultado_HoraCnt, ""))));
                  AV21ContagemResultado_OsFsOsFm = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_OsFsOsFm", AV21ContagemResultado_OsFsOsFm);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV21ContagemResultado_OsFsOsFm, ""))));
                  AV22ContagemResultado_ContratadaPessoaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_ContratadaPessoaNom", AV22ContagemResultado_ContratadaPessoaNom);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PABO2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTBO2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117405342");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_alteradatacnt.aspx") + "?" + UrlEncode("" +AV6ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV18ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV19ContagemResultado_HoraCnt)) + "," + UrlEncode(StringUtil.RTrim(AV21ContagemResultado_OsFsOsFm)) + "," + UrlEncode(StringUtil.RTrim(AV22ContagemResultado_ContratadaPessoaNom))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_HORACNT", StringUtil.RTrim( AV19ContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV18ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV21ContagemResultado_OsFsOsFm, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV18ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV19ContagemResultado_HoraCnt, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV21ContagemResultado_OsFsOsFm, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV18ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV19ContagemResultado_HoraCnt, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV21ContagemResultado_OsFsOsFm, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEBO2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTBO2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_alteradatacnt.aspx") + "?" + UrlEncode("" +AV6ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV18ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(AV19ContagemResultado_HoraCnt)) + "," + UrlEncode(StringUtil.RTrim(AV21ContagemResultado_OsFsOsFm)) + "," + UrlEncode(StringUtil.RTrim(AV22ContagemResultado_ContratadaPessoaNom)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_AlteraDataCnt" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alterar data da contagem:" ;
      }

      protected void WBBO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            context.WriteHtmlText( "<p>") ;
            wb_table1_3_BO2( true) ;
         }
         else
         {
            wb_table1_3_BO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_BO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
         }
         wbLoad = true;
      }

      protected void STARTBO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alterar data da contagem:", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBO0( ) ;
      }

      protected void WSBO2( )
      {
         STARTBO2( ) ;
         EVTBO2( ) ;
      }

      protected void EVTBO2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11BO2 */
                                    E11BO2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12BO2 */
                              E12BO2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PABO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavDate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_datacnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datacnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datacnt_Enabled), 5, 0)));
         edtavContagemresultado_osfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm_Enabled), 5, 0)));
      }

      protected void RFBO2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12BO2 */
            E12BO2 ();
            WBBO0( ) ;
         }
      }

      protected void STRUPBO0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_datacnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datacnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datacnt_Enabled), 5, 0)));
         edtavContagemresultado_osfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osfsosfm_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( context.localUtil.VCDateTime( cgiGet( edtavDate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Date"}), 1, "vDATE");
               GX_FocusControl = edtavDate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5Date = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Date", context.localUtil.Format(AV5Date, "99/99/99"));
            }
            else
            {
               AV5Date = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Date", context.localUtil.Format(AV5Date, "99/99/99"));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E11BO2 */
         E11BO2 ();
         if (returnInSub) return;
      }

      protected void E11BO2( )
      {
         /* Enter Routine */
         if ( (DateTime.MinValue==AV5Date) || ( AV5Date > Gx_date ) )
         {
            GX_msglist.addItem("Data inv�lida!");
         }
         else if ( AV18ContagemResultado_DataCnt == AV5Date )
         {
            GX_msglist.addItem("A data � a mesma!");
         }
         else
         {
            new prc_alteradatacnt(context ).execute(  AV6ContagemResultado_Codigo,  AV18ContagemResultado_DataCnt,  AV19ContagemResultado_HoraCnt,  AV5Date) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_DataCnt", context.localUtil.Format(AV18ContagemResultado_DataCnt, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV18ContagemResultado_DataCnt));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_HoraCnt", AV19ContagemResultado_HoraCnt);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV19ContagemResultado_HoraCnt, ""))));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Date", context.localUtil.Format(AV5Date, "99/99/99"));
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12BO2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_BO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(280), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(400), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "left", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left;vertical-align:top")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table2_7_BO2( true) ;
         }
         else
         {
            wb_table2_7_BO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_BO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:bottom")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlteraDataCnt.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Fechar", bttButton2_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_BO2e( true) ;
         }
         else
         {
            wb_table1_3_BO2e( false) ;
         }
      }

      protected void wb_table2_7_BO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "De:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datacnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datacnt_Internalname, context.localUtil.Format(AV18ContagemResultado_DataCnt, "99/99/99"), context.localUtil.Format( AV18ContagemResultado_DataCnt, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datacnt_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_datacnt_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AlteraDataCnt.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datacnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datacnt_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Para:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDate_Internalname, context.localUtil.Format(AV5Date, "99/99/99"), context.localUtil.Format( AV5Date, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AlteraDataCnt.htm");
            GxWebStd.gx_bitmap( context, edtavDate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "OS FS/FM:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_osfsosfm_Internalname, AV21ContagemResultado_OsFsOsFm, StringUtil.RTrim( context.localUtil.Format( AV21ContagemResultado_OsFsOsFm, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_osfsosfm_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_osfsosfm_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Contratada:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_contratadapessoanom_Internalname, StringUtil.RTrim( AV22ContagemResultado_ContratadaPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV22ContagemResultado_ContratadaPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_contratadapessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AlteraDataCnt.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_BO2e( true) ;
         }
         else
         {
            wb_table2_7_BO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV6ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9")));
         AV18ContagemResultado_DataCnt = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_DataCnt", context.localUtil.Format(AV18ContagemResultado_DataCnt, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( "", AV18ContagemResultado_DataCnt));
         AV19ContagemResultado_HoraCnt = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_HoraCnt", AV19ContagemResultado_HoraCnt);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV19ContagemResultado_HoraCnt, ""))));
         AV21ContagemResultado_OsFsOsFm = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_OsFsOsFm", AV21ContagemResultado_OsFsOsFm);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV21ContagemResultado_OsFsOsFm, ""))));
         AV22ContagemResultado_ContratadaPessoaNom = (String)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_ContratadaPessoaNom", AV22ContagemResultado_ContratadaPessoaNom);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABO2( ) ;
         WSBO2( ) ;
         WEBO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117405355");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_alteradatacnt.js", "?20203117405355");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavContagemresultado_datacnt_Internalname = "vCONTAGEMRESULTADO_DATACNT";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDate_Internalname = "vDATE";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavContagemresultado_osfsosfm_Internalname = "vCONTAGEMRESULTADO_OSFSOSFM";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavContagemresultado_contratadapessoanom_Internalname = "vCONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         tblTable1_Internalname = "TABLE1";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable2_Internalname = "TABLE2";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_contratadapessoanom_Jsonclick = "";
         edtavContagemresultado_osfsosfm_Jsonclick = "";
         edtavContagemresultado_osfsosfm_Enabled = 0;
         edtavDate_Jsonclick = "";
         edtavContagemresultado_datacnt_Jsonclick = "";
         edtavContagemresultado_datacnt_Enabled = 0;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alterar data da contagem:";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E11BO2',iparms:[{av:'AV5Date',fld:'vDATE',pic:'',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''},{av:'AV18ContagemResultado_DataCnt',fld:'vCONTAGEMRESULTADO_DATACNT',pic:'',hsh:true,nv:''},{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV19ContagemResultado_HoraCnt',fld:'vCONTAGEMRESULTADO_HORACNT',pic:'',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV18ContagemResultado_DataCnt = DateTime.MinValue;
         wcpOAV19ContagemResultado_HoraCnt = "";
         wcpOAV21ContagemResultado_OsFsOsFm = "";
         wcpOAV22ContagemResultado_ContratadaPessoaNom = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Gx_date = DateTime.MinValue;
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV5Date = DateTime.MinValue;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_datacnt_Enabled = 0;
         edtavContagemresultado_osfsosfm_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV6ContagemResultado_Codigo ;
      private int wcpOAV6ContagemResultado_Codigo ;
      private int edtavContagemresultado_datacnt_Enabled ;
      private int edtavContagemresultado_osfsosfm_Enabled ;
      private int idxLst ;
      private String AV19ContagemResultado_HoraCnt ;
      private String AV22ContagemResultado_ContratadaPessoaNom ;
      private String wcpOAV19ContagemResultado_HoraCnt ;
      private String wcpOAV22ContagemResultado_ContratadaPessoaNom ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDate_Internalname ;
      private String edtavContagemresultado_datacnt_Internalname ;
      private String edtavContagemresultado_osfsosfm_Internalname ;
      private String sStyleString ;
      private String tblTable2_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String tblTable1_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavContagemresultado_datacnt_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavDate_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavContagemresultado_osfsosfm_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavContagemresultado_contratadapessoanom_Internalname ;
      private String edtavContagemresultado_contratadapessoanom_Jsonclick ;
      private DateTime AV18ContagemResultado_DataCnt ;
      private DateTime wcpOAV18ContagemResultado_DataCnt ;
      private DateTime Gx_date ;
      private DateTime AV5Date ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV21ContagemResultado_OsFsOsFm ;
      private String wcpOAV21ContagemResultado_OsFsOsFm ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

}
