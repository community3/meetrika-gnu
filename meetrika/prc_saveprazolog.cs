/*
               File: PRC_SavePrazoLog
        Description: Save Prazo Log
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:8:44.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saveprazolog : GXProcedure
   {
      public prc_saveprazolog( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saveprazolog( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref long aP0_LogResponsavel_Codigo ,
                           DateTime aP1_LogResponsavel_Prazo ,
                           short aP2_Dias )
      {
         this.A1797LogResponsavel_Codigo = aP0_LogResponsavel_Codigo;
         this.AV8LogResponsavel_Prazo = aP1_LogResponsavel_Prazo;
         this.AV9Dias = aP2_Dias;
         initialize();
         executePrivate();
         aP0_LogResponsavel_Codigo=this.A1797LogResponsavel_Codigo;
      }

      public void executeSubmit( ref long aP0_LogResponsavel_Codigo ,
                                 DateTime aP1_LogResponsavel_Prazo ,
                                 short aP2_Dias )
      {
         prc_saveprazolog objprc_saveprazolog;
         objprc_saveprazolog = new prc_saveprazolog();
         objprc_saveprazolog.A1797LogResponsavel_Codigo = aP0_LogResponsavel_Codigo;
         objprc_saveprazolog.AV8LogResponsavel_Prazo = aP1_LogResponsavel_Prazo;
         objprc_saveprazolog.AV9Dias = aP2_Dias;
         objprc_saveprazolog.context.SetSubmitInitialConfig(context);
         objprc_saveprazolog.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saveprazolog);
         aP0_LogResponsavel_Codigo=this.A1797LogResponsavel_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saveprazolog)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VQ2 */
         pr_default.execute(0, new Object[] {A1797LogResponsavel_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00VQ2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00VQ2_n1553ContagemResultado_CntSrvCod[0];
            A893LogResponsavel_DataHora = P00VQ2_A893LogResponsavel_DataHora[0];
            A1611ContagemResultado_PrzTpDias = P00VQ2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00VQ2_n1611ContagemResultado_PrzTpDias[0];
            A1177LogResponsavel_Prazo = P00VQ2_A1177LogResponsavel_Prazo[0];
            n1177LogResponsavel_Prazo = P00VQ2_n1177LogResponsavel_Prazo[0];
            A1227ContagemResultado_PrazoInicialDias = P00VQ2_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P00VQ2_n1227ContagemResultado_PrazoInicialDias[0];
            A894LogResponsavel_Acao = P00VQ2_A894LogResponsavel_Acao[0];
            A896LogResponsavel_Owner = P00VQ2_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P00VQ2_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00VQ2_n892LogResponsavel_DemandaCod[0];
            A1553ContagemResultado_CntSrvCod = P00VQ2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00VQ2_n1553ContagemResultado_CntSrvCod[0];
            A1227ContagemResultado_PrazoInicialDias = P00VQ2_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P00VQ2_n1227ContagemResultado_PrazoInicialDias[0];
            A1611ContagemResultado_PrzTpDias = P00VQ2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00VQ2_n1611ContagemResultado_PrzTpDias[0];
            GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
            A1177LogResponsavel_Prazo = AV8LogResponsavel_Prazo;
            n1177LogResponsavel_Prazo = false;
            if ( AV9Dias > 0 )
            {
               A1227ContagemResultado_PrazoInicialDias = AV9Dias;
               n1227ContagemResultado_PrazoInicialDias = false;
            }
            if ( ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "E") == 0 ) && A1149LogResponsavel_OwnerEhContratante ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "S") == 0 ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "I") == 0 ) )
            {
               AV13GXLvl9 = 0;
               /* Using cursor P00VQ3 */
               pr_default.execute(1, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod, A893LogResponsavel_DataHora});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A1404ContagemResultadoExecucao_OSCod = P00VQ3_A1404ContagemResultadoExecucao_OSCod[0];
                  A1406ContagemResultadoExecucao_Inicio = P00VQ3_A1406ContagemResultadoExecucao_Inicio[0];
                  A1408ContagemResultadoExecucao_Prevista = P00VQ3_A1408ContagemResultadoExecucao_Prevista[0];
                  n1408ContagemResultadoExecucao_Prevista = P00VQ3_n1408ContagemResultadoExecucao_Prevista[0];
                  A1411ContagemResultadoExecucao_PrazoDias = P00VQ3_A1411ContagemResultadoExecucao_PrazoDias[0];
                  n1411ContagemResultadoExecucao_PrazoDias = P00VQ3_n1411ContagemResultadoExecucao_PrazoDias[0];
                  A1405ContagemResultadoExecucao_Codigo = P00VQ3_A1405ContagemResultadoExecucao_Codigo[0];
                  AV13GXLvl9 = 1;
                  A1408ContagemResultadoExecucao_Prevista = AV8LogResponsavel_Prazo;
                  n1408ContagemResultadoExecucao_Prevista = false;
                  GXt_int2 = A1411ContagemResultadoExecucao_PrazoDias;
                  new prc_diasuteisentre(context ).execute(  A893LogResponsavel_DataHora,  AV8LogResponsavel_Prazo,  A1611ContagemResultado_PrzTpDias, out  GXt_int2) ;
                  A1411ContagemResultadoExecucao_PrazoDias = GXt_int2;
                  n1411ContagemResultadoExecucao_PrazoDias = false;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P00VQ4 */
                  pr_default.execute(2, new Object[] {n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, A1405ContagemResultadoExecucao_Codigo});
                  pr_default.close(2);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                  if (true) break;
                  /* Using cursor P00VQ5 */
                  pr_default.execute(3, new Object[] {n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, A1405ContagemResultadoExecucao_Codigo});
                  pr_default.close(3);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV13GXLvl9 == 0 )
               {
                  /* Using cursor P00VQ6 */
                  pr_default.execute(4, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
                  while ( (pr_default.getStatus(4) != 101) )
                  {
                     A1404ContagemResultadoExecucao_OSCod = P00VQ6_A1404ContagemResultadoExecucao_OSCod[0];
                     A1406ContagemResultadoExecucao_Inicio = P00VQ6_A1406ContagemResultadoExecucao_Inicio[0];
                     A1408ContagemResultadoExecucao_Prevista = P00VQ6_A1408ContagemResultadoExecucao_Prevista[0];
                     n1408ContagemResultadoExecucao_Prevista = P00VQ6_n1408ContagemResultadoExecucao_Prevista[0];
                     A1411ContagemResultadoExecucao_PrazoDias = P00VQ6_A1411ContagemResultadoExecucao_PrazoDias[0];
                     n1411ContagemResultadoExecucao_PrazoDias = P00VQ6_n1411ContagemResultadoExecucao_PrazoDias[0];
                     A1405ContagemResultadoExecucao_Codigo = P00VQ6_A1405ContagemResultadoExecucao_Codigo[0];
                     if ( DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio) >= DateTimeUtil.ResetTime( A893LogResponsavel_DataHora) )
                     {
                        A1408ContagemResultadoExecucao_Prevista = AV8LogResponsavel_Prazo;
                        n1408ContagemResultadoExecucao_Prevista = false;
                        GXt_int2 = A1411ContagemResultadoExecucao_PrazoDias;
                        new prc_diasuteisentre(context ).execute(  A893LogResponsavel_DataHora,  AV8LogResponsavel_Prazo,  A1611ContagemResultado_PrzTpDias, out  GXt_int2) ;
                        A1411ContagemResultadoExecucao_PrazoDias = GXt_int2;
                        n1411ContagemResultadoExecucao_PrazoDias = false;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        /* Using cursor P00VQ7 */
                        pr_default.execute(5, new Object[] {n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, A1405ContagemResultadoExecucao_Codigo});
                        pr_default.close(5);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                        if (true) break;
                        /* Using cursor P00VQ8 */
                        pr_default.execute(6, new Object[] {n1408ContagemResultadoExecucao_Prevista, A1408ContagemResultadoExecucao_Prevista, n1411ContagemResultadoExecucao_PrazoDias, A1411ContagemResultadoExecucao_PrazoDias, A1405ContagemResultadoExecucao_Codigo});
                        pr_default.close(6);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
                     }
                     pr_default.readNext(4);
                  }
                  pr_default.close(4);
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00VQ9 */
            pr_default.execute(7, new Object[] {n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P00VQ10 */
            pr_default.execute(8, new Object[] {n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A1797LogResponsavel_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
            if (true) break;
            /* Using cursor P00VQ11 */
            pr_default.execute(9, new Object[] {n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Using cursor P00VQ12 */
            pr_default.execute(10, new Object[] {n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A1797LogResponsavel_Codigo});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VQ2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00VQ2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00VQ2_A1797LogResponsavel_Codigo = new long[1] ;
         P00VQ2_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00VQ2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00VQ2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00VQ2_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00VQ2_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P00VQ2_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00VQ2_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00VQ2_A894LogResponsavel_Acao = new String[] {""} ;
         P00VQ2_A896LogResponsavel_Owner = new int[1] ;
         P00VQ2_A892LogResponsavel_DemandaCod = new int[1] ;
         P00VQ2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A894LogResponsavel_Acao = "";
         P00VQ3_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P00VQ3_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P00VQ3_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         P00VQ3_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         P00VQ3_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         P00VQ3_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         P00VQ3_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         P00VQ6_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P00VQ6_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P00VQ6_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         P00VQ6_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         P00VQ6_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         P00VQ6_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         P00VQ6_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saveprazolog__default(),
            new Object[][] {
                new Object[] {
               P00VQ2_A1553ContagemResultado_CntSrvCod, P00VQ2_n1553ContagemResultado_CntSrvCod, P00VQ2_A1797LogResponsavel_Codigo, P00VQ2_A893LogResponsavel_DataHora, P00VQ2_A1611ContagemResultado_PrzTpDias, P00VQ2_n1611ContagemResultado_PrzTpDias, P00VQ2_A1177LogResponsavel_Prazo, P00VQ2_n1177LogResponsavel_Prazo, P00VQ2_A1227ContagemResultado_PrazoInicialDias, P00VQ2_n1227ContagemResultado_PrazoInicialDias,
               P00VQ2_A894LogResponsavel_Acao, P00VQ2_A896LogResponsavel_Owner, P00VQ2_A892LogResponsavel_DemandaCod, P00VQ2_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P00VQ3_A1404ContagemResultadoExecucao_OSCod, P00VQ3_A1406ContagemResultadoExecucao_Inicio, P00VQ3_A1408ContagemResultadoExecucao_Prevista, P00VQ3_n1408ContagemResultadoExecucao_Prevista, P00VQ3_A1411ContagemResultadoExecucao_PrazoDias, P00VQ3_n1411ContagemResultadoExecucao_PrazoDias, P00VQ3_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00VQ6_A1404ContagemResultadoExecucao_OSCod, P00VQ6_A1406ContagemResultadoExecucao_Inicio, P00VQ6_A1408ContagemResultadoExecucao_Prevista, P00VQ6_n1408ContagemResultadoExecucao_Prevista, P00VQ6_A1411ContagemResultadoExecucao_PrazoDias, P00VQ6_n1411ContagemResultadoExecucao_PrazoDias, P00VQ6_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Dias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV13GXLvl9 ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short GXt_int2 ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A896LogResponsavel_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A894LogResponsavel_Acao ;
      private DateTime AV8LogResponsavel_Prazo ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean1 ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private long aP0_LogResponsavel_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00VQ2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00VQ2_n1553ContagemResultado_CntSrvCod ;
      private long[] P00VQ2_A1797LogResponsavel_Codigo ;
      private DateTime[] P00VQ2_A893LogResponsavel_DataHora ;
      private String[] P00VQ2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00VQ2_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P00VQ2_A1177LogResponsavel_Prazo ;
      private bool[] P00VQ2_n1177LogResponsavel_Prazo ;
      private short[] P00VQ2_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00VQ2_n1227ContagemResultado_PrazoInicialDias ;
      private String[] P00VQ2_A894LogResponsavel_Acao ;
      private int[] P00VQ2_A896LogResponsavel_Owner ;
      private int[] P00VQ2_A892LogResponsavel_DemandaCod ;
      private bool[] P00VQ2_n892LogResponsavel_DemandaCod ;
      private int[] P00VQ3_A1404ContagemResultadoExecucao_OSCod ;
      private DateTime[] P00VQ3_A1406ContagemResultadoExecucao_Inicio ;
      private DateTime[] P00VQ3_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] P00VQ3_n1408ContagemResultadoExecucao_Prevista ;
      private short[] P00VQ3_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] P00VQ3_n1411ContagemResultadoExecucao_PrazoDias ;
      private int[] P00VQ3_A1405ContagemResultadoExecucao_Codigo ;
      private int[] P00VQ6_A1404ContagemResultadoExecucao_OSCod ;
      private DateTime[] P00VQ6_A1406ContagemResultadoExecucao_Inicio ;
      private DateTime[] P00VQ6_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] P00VQ6_n1408ContagemResultadoExecucao_Prevista ;
      private short[] P00VQ6_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] P00VQ6_n1411ContagemResultadoExecucao_PrazoDias ;
      private int[] P00VQ6_A1405ContagemResultadoExecucao_Codigo ;
   }

   public class prc_saveprazolog__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VQ2 ;
          prmP00VQ2 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP00VQ3 ;
          prmP00VQ3 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_DataHora",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00VQ4 ;
          prmP00VQ4 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VQ5 ;
          prmP00VQ5 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VQ6 ;
          prmP00VQ6 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VQ7 ;
          prmP00VQ7 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VQ8 ;
          prmP00VQ8 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VQ9 ;
          prmP00VQ9 = new Object[] {
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VQ10 ;
          prmP00VQ10 = new Object[] {
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmP00VQ11 ;
          prmP00VQ11 = new Object[] {
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VQ12 ;
          prmP00VQ12 = new Object[] {
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VQ2", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[LogResponsavel_Codigo], T1.[LogResponsavel_DataHora], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[LogResponsavel_Prazo], T2.[ContagemResultado_PrazoInicialDias], T1.[LogResponsavel_Acao], T1.[LogResponsavel_Owner], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM (([LogResponsavel] T1 WITH (UPDLOCK) LEFT JOIN [ContagemResultado] T2 WITH (UPDLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE T1.[LogResponsavel_Codigo] = @LogResponsavel_Codigo ORDER BY T1.[LogResponsavel_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VQ2,1,0,true,true )
             ,new CursorDef("P00VQ3", "SELECT TOP 1 [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (UPDLOCK) WHERE ([ContagemResultadoExecucao_OSCod] = @LogResponsavel_DemandaCod and [ContagemResultadoExecucao_Inicio] >= @LogResponsavel_DataHora) AND ([ContagemResultadoExecucao_Inicio] <= DATEADD( ss,5, @LogResponsavel_DataHora)) ORDER BY [ContagemResultadoExecucao_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VQ3,1,0,true,true )
             ,new CursorDef("P00VQ4", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Prevista]=@ContagemResultadoExecucao_Prevista, [ContagemResultadoExecucao_PrazoDias]=@ContagemResultadoExecucao_PrazoDias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ4)
             ,new CursorDef("P00VQ5", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Prevista]=@ContagemResultadoExecucao_Prevista, [ContagemResultadoExecucao_PrazoDias]=@ContagemResultadoExecucao_PrazoDias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ5)
             ,new CursorDef("P00VQ6", "SELECT [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (UPDLOCK) WHERE [ContagemResultadoExecucao_OSCod] = @LogResponsavel_DemandaCod ORDER BY [ContagemResultadoExecucao_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VQ6,1,0,true,false )
             ,new CursorDef("P00VQ7", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Prevista]=@ContagemResultadoExecucao_Prevista, [ContagemResultadoExecucao_PrazoDias]=@ContagemResultadoExecucao_PrazoDias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ7)
             ,new CursorDef("P00VQ8", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Prevista]=@ContagemResultadoExecucao_Prevista, [ContagemResultadoExecucao_PrazoDias]=@ContagemResultadoExecucao_PrazoDias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ8)
             ,new CursorDef("P00VQ9", "UPDATE [ContagemResultado] SET [ContagemResultado_PrazoInicialDias]=@ContagemResultado_PrazoInicialDias  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ9)
             ,new CursorDef("P00VQ10", "UPDATE [LogResponsavel] SET [LogResponsavel_Prazo]=@LogResponsavel_Prazo  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ10)
             ,new CursorDef("P00VQ11", "UPDATE [ContagemResultado] SET [ContagemResultado_PrazoInicialDias]=@ContagemResultado_PrazoInicialDias  WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ11)
             ,new CursorDef("P00VQ12", "UPDATE [LogResponsavel] SET [LogResponsavel_Prazo]=@LogResponsavel_Prazo  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VQ12)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (long)parms[2]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (long)parms[2]);
                return;
       }
    }

 }

}
