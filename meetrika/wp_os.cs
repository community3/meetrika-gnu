/*
               File: WP_OS
        Description: Nova Ordem de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:45:56.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_os : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_os( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_os( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavRequisitadopor = new GXCheckbox();
         dynavRequisitante = new GXCombobox();
         cmbavContrato = new GXCombobox();
         cmbavServicogrupo_codigo = new GXCombobox();
         cmbavServico_codigo = new GXCombobox();
         cmbavComplexidade = new GXCombobox();
         cmbavPrioridade_codigo = new GXCombobox();
         cmbavRequisitado = new GXCombobox();
         cmbavUsuario_codigo = new GXCombobox();
         cmbavDmnvinculadas = new GXCombobox();
         dynavContratadaorigem = new GXCombobox();
         dynavContadorfs = new GXCombobox();
         cmbavContratoservicos_localexec = new GXCombobox();
         cmbavSistema_codigo = new GXCombobox();
         dynavModulo_codigo = new GXCombobox();
         dynavFuncaousuario_codigo = new GXCombobox();
         cmbavSdt_requisitos__requisito_prioridade = new GXCombobox();
         cmbavSdt_requisitos__requisito_status = new GXCombobox();
         cmbavServico_atende = new GXCombobox();
         cmbavServico_obrigavalores = new GXCombobox();
         cmbavServico_obrigavalores = new GXCombobox();
         chkavContratovencido = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vREQUISITANTE") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvREQUISITANTECG2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADAORIGEM") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADAORIGEMCG2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTADORFS") == 0 )
            {
               AV105ContratadaOrigem = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTADORFSCG2( AV105ContratadaOrigem) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vMODULO_CODIGO") == 0 )
            {
               AV38Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvMODULO_CODIGOCG2( AV38Sistema_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFUNCAOUSUARIO_CODIGO") == 0 )
            {
               AV38Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvFUNCAOUSUARIO_CODIGOCG2( AV38Sistema_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridrequisitos") == 0 )
            {
               nRC_GXsfl_219 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_219_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_219_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridrequisitos_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridrequisitos") == 0 )
            {
               subGridrequisitos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV134Contratante_UsaOSistema = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Contratante_UsaOSistema", AV134Contratante_UsaOSistema);
               AV13Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A58Usuario_PessoaNom = GetNextPar( );
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A1075Usuario_CargoUOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1075Usuario_CargoUOCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1075Usuario_CargoUOCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1075Usuario_CargoUOCod), 6, 0)));
               A1076Usuario_CargoUONom = GetNextPar( );
               n1076Usuario_CargoUONom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1076Usuario_CargoUONom", A1076Usuario_CargoUONom);
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               AV100AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100AreaTrabalho_Codigo), 6, 0)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n39Contratada_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV132ContratadasDaArea);
               A158ServicoGrupo_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
               A75Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n75Contrato_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0)));
               A92Contrato_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               n92Contrato_Ativo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92Contrato_Ativo", A92Contrato_Ativo);
               A638ContratoServicos_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A638ContratoServicos_Ativo", A638ContratoServicos_Ativo);
               A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               A129Sistema_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Sistema_Sigla", A129Sistema_Sigla);
               A135Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A135Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0)));
               A699Sistema_Tipo = GetNextPar( );
               n699Sistema_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A699Sistema_Tipo", A699Sistema_Tipo);
               A130Sistema_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Sistema_Ativo", A130Sistema_Ativo);
               A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV51Sistemas);
               AV172ContratoServicos_QntUntCns = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV172ContratoServicos_QntUntCns, 9, 4)));
               AV166Servico_IsOrigemReferencia = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166Servico_IsOrigemReferencia", AV166Servico_IsOrigemReferencia);
               AV168Servico_IsSolicitaGestorSistema = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168Servico_IsSolicitaGestorSistema", AV168Servico_IsSolicitaGestorSistema);
               A1446ContratoGestor_ContratadaAreaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1446ContratoGestor_ContratadaAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               AV102Requisitante = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0)));
               A843Contrato_DataFimTA = context.localUtil.ParseDateParm( GetNextPar( ));
               n843Contrato_DataFimTA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
               A83Contrato_DataVigenciaTermino = context.localUtil.ParseDateParm( GetNextPar( ));
               n83Contrato_DataVigenciaTermino = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               A43Contratada_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               n43Contratada_Ativo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43Contratada_Ativo", A43Contratada_Ativo);
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n74Contrato_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A1078ContratoGestor_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
               AV16ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
               A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A1825ContratoAuxiliar_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1825ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1825ContratoAuxiliar_UsuarioCod), 6, 0)));
               A1824ContratoAuxiliar_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1824ContratoAuxiliar_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1824ContratoAuxiliar_ContratoCod), 6, 0)));
               AV170ServicoCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV170ServicoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV170ServicoCodigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV136Servicos);
               A605Servico_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
               A608Servico_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV137SDT_Servicos);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV161SDT_Requisitos);
               AV146Agrupador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV146Agrupador);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PACG2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTCG2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423455797");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_os.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_requisitos", AV161SDT_Requisitos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_requisitos", AV161SDT_Requisitos);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_219", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_219), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_OBSERVACAO", AV19ContagemResultado_Observacao);
         GxWebStd.gx_boolean_hidden_field( context, "vCONTRATANTE_USAOSISTEMA", AV134Contratante_UsaOSistema);
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOUOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1075Usuario_CargoUOCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CARGOUONOM", StringUtil.RTrim( A1076Usuario_CargoUONom));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADASDAAREA", AV132ContratadasDaArea);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADASDAAREA", AV132ContratadasDaArea);
         }
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_DESCRICAO", A158ServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A75Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_ATIVO", A638ContratoServicos_Ativo);
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_SIGLA", StringUtil.RTrim( A129Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, "SISTEMA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A135Sistema_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_TIPO", StringUtil.RTrim( A699Sistema_Tipo));
         GxWebStd.gx_boolean_hidden_field( context, "SISTEMA_ATIVO", A130Sistema_Ativo);
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAS", AV51Sistemas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAS", AV51Sistemas);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vSERVICO_ISORIGEMREFERENCIA", AV166Servico_IsOrigemReferencia);
         GxWebStd.gx_boolean_hidden_field( context, "vSERVICO_ISSOLICITAGESTORSISTEMA", AV168Servico_IsSolicitaGestorSistema);
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADAAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAFIMTA", context.localUtil.DToC( A843Contrato_DataFimTA, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAVIGENCIATERMINO", context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 0, "/"));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADA_ATIVO", A43Contratada_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1825ContratoAuxiliar_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1824ContratoAuxiliar_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICOCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV170ServicoCodigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOS", AV136Servicos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOS", AV136Servicos);
         }
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_SERVICOS", AV137SDT_Servicos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_SERVICOS", AV137SDT_Servicos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_REQUISITOS", AV161SDT_Requisitos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_REQUISITOS", AV161SDT_Requisitos);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPRAZOTIPO", StringUtil.RTrim( AV84PrazoTipo));
         GxWebStd.gx_hidden_field( context, "vENTREGACALCULADA", context.localUtil.TToC( AV63EntregaCalculada, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vPRAZOMOMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV135PrazoMomento), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCALLER", StringUtil.RTrim( AV125Caller));
         GxWebStd.gx_boolean_hidden_field( context, "vREQUERORIGEM", AV163RequerOrigem);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_DEMANDA", AV5ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vSERVICOGRUPO_FLAGREQUERSOFT", AV108ServicoGrupo_FlagRequerSoft);
         GxWebStd.gx_boolean_hidden_field( context, "SERVICOGRUPO_FLAGREQUERSOFT", A1428ServicoGrupo_FlagRequerSoft);
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_VALORPF", StringUtil.LTrim( StringUtil.NToC( AV26ContagemResultado_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vCONTRATANTE_OSAUTOMATICA", AV53Contratante_OSAutomatica);
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vPERCPRAZO", StringUtil.LTrim( StringUtil.NToC( AV113PercPrazo, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPERCVALORB", StringUtil.LTrim( StringUtil.NToC( AV114PercValorB, 6, 2, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vCONTRATANTESEMEMAILSDA", AV61ContratanteSemEmailSda);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOS", AV42Usuarios);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOS", AV42Usuarios);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV158Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV158Codigos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATTACHMENTS", AV46Attachments);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATTACHMENTS", AV46Attachments);
         }
         GxWebStd.gx_hidden_field( context, "vRESULTADO", StringUtil.RTrim( AV44Resultado));
         GxWebStd.gx_hidden_field( context, "vMSG", StringUtil.RTrim( Gx_msg));
         GxWebStd.gx_hidden_field( context, "CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOGESTOR_USUARIOEHCONTRATANTE", A1135ContratoGestor_UsuarioEhContratante);
         GxWebStd.gx_boolean_hidden_field( context, "vLERSERVICO", AV85LerServico);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_LOCALEXEC", StringUtil.RTrim( A639ContratoServicos_LocalExec));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_TERCERIZA", A889Servico_Terceriza);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PRAZOTPDIAS", StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PRAZOANALISE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1152ContratoServicos_PrazoAnalise), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PRAZOINICIO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1649ContratoServicos_PrazoInicio), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV115TipoDias));
         GxWebStd.gx_hidden_field( context, "vDIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67Dias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPRAZOINICIO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV122PrazoInicio), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDIASANALISE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV98DiasAnalise), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "MODULO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A146Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_FNCUSRCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADATIPOFAB", StringUtil.RTrim( A1326ContagemResultado_ContratadaTipoFab));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A683ContagemResultado_PFLFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A685ContagemResultado_PFLFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSIMP", StringUtil.LTrim( StringUtil.NToC( A798ContagemResultado_PFBFSImp, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFSIMP", StringUtil.LTrim( StringUtil.NToC( A799ContagemResultado_PFLFSImp, 14, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_SISTEMAATIVO", A804ContagemResultado_SistemaAtivo);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADASIGLA", StringUtil.RTrim( A803ContagemResultado_ContratadaSigla));
         GxWebStd.gx_hidden_field( context, "SERVICO_ATENDE", StringUtil.RTrim( A1072Servico_Atende));
         GxWebStd.gx_hidden_field( context, "SERVICO_OBRIGAVALORES", StringUtil.RTrim( A1429Servico_ObrigaValores));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ISORIGEMREFERENCIA", A2092Servico_IsOrigemReferencia);
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATOSERVICOS_SOLICITAGESTORSISTEMA", A2094ContratoServicos_SolicitaGestorSistema);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_UNDCNTNOME", StringUtil.RTrim( A2132ContratoServicos_UndCntNome));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_QNTUNTCNS", StringUtil.LTrim( StringUtil.NToC( A1340ContratoServicos_QntUntCns, 9, 4, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PRAZOTIPO", StringUtil.RTrim( A913ContratoServicos_PrazoTipo));
         GxWebStd.gx_hidden_field( context, "SERVICO_VLRUNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( A557Servico_VlrUnidadeContratada, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_VALORUNIDADECONTRATACAO", StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSSISTEMAS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1067ContratoServicosSistemas_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSSISTEMAS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1063ContratoServicosSistemas_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_MOMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_DIASBAIXA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_DIASMEDIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_DIASALTA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRIORIDADE_NOME", StringUtil.RTrim( A1337ContratoServicosPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATATERMINO", context.localUtil.DToC( A1869Contrato_DataTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_PADRAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV155Contrato_Padrao), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vNOME", StringUtil.RTrim( AV162Nome));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATADAUSUARIO_USUARIOATIVO", A1394ContratadaUsuario_UsuarioAtivo);
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_DEFERIAS", A1908Usuario_DeFerias);
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A828UsuarioServicos_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A829UsuarioServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( A62ContratanteUsuario_UsuarioPessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_ATIVO", A54Usuario_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_DESCRICAO", A1751Artefatos_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOANOM", StringUtil.RTrim( A41Contratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV56Usuario_CargoUONom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Usuario_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV56Usuario_CargoUONom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Usuario_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV56Usuario_CargoUONom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Usuario_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV56Usuario_CargoUONom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Usuario_PessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultado_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Width", StringUtil.RTrim( Gxuitabspanel_tab_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Cls", StringUtil.RTrim( Gxuitabspanel_tab_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tab_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tab_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tab_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TAB_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tab_Designtimetabs));
         GxWebStd.gx_hidden_field( context, "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Title", StringUtil.RTrim( Dvelop_bootstrap_confirmpanel1_Title));
         GxWebStd.gx_hidden_field( context, "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Confirmationtext", StringUtil.RTrim( Dvelop_bootstrap_confirmpanel1_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Yesbuttoncaption", StringUtil.RTrim( Dvelop_bootstrap_confirmpanel1_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Confirmtype", StringUtil.RTrim( Dvelop_bootstrap_confirmpanel1_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Title", StringUtil.RTrim( Confirmpanel2_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Confirmationtext", StringUtil.RTrim( Confirmpanel2_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel2_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Confirmtype", StringUtil.RTrim( Confirmpanel2_Confirmtype));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_Text", StringUtil.RTrim( cmbavContrato.Description));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO_Text", StringUtil.RTrim( cmbavServico_codigo.Description));
         GxWebStd.gx_hidden_field( context, "vREQUISITANTE_Text", StringUtil.RTrim( dynavRequisitante.Description));
         GxWebStd.gx_hidden_field( context, "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Title", StringUtil.RTrim( Dvelop_bootstrap_confirmpanel1_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL2_Title", StringUtil.RTrim( Confirmpanel2_Title));
         GxWebStd.gx_hidden_field( context, "vCONTRATADAORIGEM_Text", StringUtil.RTrim( dynavContratadaorigem.Description));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wcarquivosevd == null ) )
         {
            WebComp_Wcarquivosevd.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WECG2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTCG2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_os.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_OS" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nova Ordem de Servi�o" ;
      }

      protected void WBCG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_CG2( true) ;
         }
         else
         {
            wb_table1_2_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 248,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV100AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV100AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,248);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavAreatrabalho_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 249,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_obrigavalores, cmbavServico_obrigavalores_Internalname, StringUtil.RTrim( AV110Servico_ObrigaValores), 1, cmbavServico_obrigavalores_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavServico_obrigavalores.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,249);\"", "", true, "HLP_WP_OS.htm");
            cmbavServico_obrigavalores.CurrentValue = StringUtil.RTrim( AV110Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_obrigavalores_Internalname, "Values", (String)(cmbavServico_obrigavalores.ToJavascriptSource()));
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 250,'',false,'" + sGXsfl_219_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavContratovencido_Internalname, StringUtil.BoolToStr( AV139ContratoVencido), "", "", chkavContratovencido.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(250, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,250);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 251,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisitadocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12RequisitadoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12RequisitadoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,251);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisitadocod_Jsonclick, 0, "Attribute", "", "", "", edtavRequisitadocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_OS.htm");
         }
         wbLoad = true;
      }

      protected void STARTCG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Nova Ordem de Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPCG0( ) ;
      }

      protected void WSCG2( )
      {
         STARTCG2( ) ;
         EVTCG2( ) ;
      }

      protected void EVTCG2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_BOOTSTRAP_CONFIRMPANEL1.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11CG2 */
                              E11CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL2.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12CG2 */
                              E12CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOARTEFATOS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13CG2 */
                              E13CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCONFIRMAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14CG2 */
                              E14CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSISTEMA_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15CG2 */
                              E15CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VPFBFSDAOSVNC.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16CG2 */
                              E16CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_DMNVINCULADA.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17CG2 */
                              E17CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_DMNVINCULADAREF.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18CG2 */
                              E18CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDMNVINCULADAS.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19CG2 */
                              E19CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VREQUISITADO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20CG2 */
                              E20CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21CG2 */
                              E21CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICOGRUPO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22CG2 */
                              E22CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VPRAZOENTREGA.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23CG2 */
                              E23CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCOMPLEXIDADE.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24CG2 */
                              E24CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VPRIORIDADE_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25CG2 */
                              E25CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_DATADMN.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26CG2 */
                              E26CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27CG2 */
                              E27CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VREQUISITANTE.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28CG2 */
                              E28CG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATADAORIGEM.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29CG2 */
                              E29CG2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDREQUISITOSPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDREQUISITOSPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridrequisitos_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridrequisitos_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridrequisitos_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridrequisitos_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "GRIDREQUISITOS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 22), "GRIDREQUISITOS.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 23), "VBTNEXCLUIRREQNEG.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOFECHAR'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 23), "VBTNEXCLUIRREQNEG.CLICK") == 0 ) )
                           {
                              nGXsfl_219_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
                              SubsflControlProps_2192( ) ;
                              AV176GXV1 = (short)(nGXsfl_219_idx+GRIDREQUISITOS_nFirstRecordOnPage);
                              if ( ( AV161SDT_Requisitos.Count >= AV176GXV1 ) && ( AV176GXV1 > 0 ) )
                              {
                                 AV161SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1));
                                 AV147btnExcluirReqNeg = cgiGet( edtavBtnexcluirreqneg_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnexcluirreqneg_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV147btnExcluirReqNeg)) ? AV182Btnexcluirreqneg_GXI : context.convertURL( context.PathToRelativeUrl( AV147btnExcluirReqNeg))));
                                 AV146Agrupador = cgiGet( edtavAgrupador_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV146Agrupador);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30CG2 */
                                    E30CG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDREQUISITOS.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31CG2 */
                                    E31CG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E32CG2 */
                                    E32CG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDREQUISITOS.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E33CG2 */
                                    E33CG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBTNEXCLUIRREQNEG.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E34CG2 */
                                    E34CG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E35CG2 */
                                    E35CG2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 207 )
                        {
                           OldWcarquivosevd = cgiGet( "W0207");
                           if ( ( StringUtil.Len( OldWcarquivosevd) == 0 ) || ( StringUtil.StrCmp(OldWcarquivosevd, WebComp_Wcarquivosevd_Component) != 0 ) )
                           {
                              WebComp_Wcarquivosevd = getWebComponent(GetType(), "GeneXus.Programs", OldWcarquivosevd, new Object[] {context} );
                              WebComp_Wcarquivosevd.ComponentInit();
                              WebComp_Wcarquivosevd.Name = "OldWcarquivosevd";
                              WebComp_Wcarquivosevd_Component = OldWcarquivosevd;
                           }
                           if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
                           {
                              WebComp_Wcarquivosevd.componentprocess("W0207", "", sEvt);
                           }
                           WebComp_Wcarquivosevd_Component = OldWcarquivosevd;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WECG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PACG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavRequisitadopor.Name = "vREQUISITADOPOR";
            chkavRequisitadopor.WebTags = "";
            chkavRequisitadopor.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequisitadopor_Internalname, "TitleCaption", chkavRequisitadopor.Caption);
            chkavRequisitadopor.CheckedValue = "false";
            dynavRequisitante.Name = "vREQUISITANTE";
            dynavRequisitante.WebTags = "";
            cmbavContrato.Name = "vCONTRATO";
            cmbavContrato.WebTags = "";
            cmbavContrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavContrato.ItemCount > 0 )
            {
               AV117Contrato = (int)(NumberUtil.Val( cmbavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)));
            }
            cmbavServicogrupo_codigo.Name = "vSERVICOGRUPO_CODIGO";
            cmbavServicogrupo_codigo.WebTags = "";
            cmbavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Todos", 0);
            if ( cmbavServicogrupo_codigo.ItemCount > 0 )
            {
               AV16ServicoGrupo_Codigo = (int)(NumberUtil.Val( cmbavServicogrupo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
            }
            cmbavServico_codigo.Name = "vSERVICO_CODIGO";
            cmbavServico_codigo.WebTags = "";
            cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavServico_codigo.ItemCount > 0 )
            {
               AV13Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
            }
            cmbavComplexidade.Name = "vCOMPLEXIDADE";
            cmbavComplexidade.WebTags = "";
            cmbavComplexidade.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 3, 0)), "(Nenhuma)", 0);
            if ( cmbavComplexidade.ItemCount > 0 )
            {
               AV97Complexidade = (short)(NumberUtil.Val( cmbavComplexidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)));
            }
            cmbavPrioridade_codigo.Name = "vPRIORIDADE_CODIGO";
            cmbavPrioridade_codigo.WebTags = "";
            cmbavPrioridade_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhuma)", 0);
            if ( cmbavPrioridade_codigo.ItemCount > 0 )
            {
               AV96Prioridade_Codigo = (int)(NumberUtil.Val( cmbavPrioridade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Prioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)));
            }
            cmbavRequisitado.Name = "vREQUISITADO";
            cmbavRequisitado.WebTags = "";
            cmbavRequisitado.addItem("", "(Nenhum)", 0);
            if ( cmbavRequisitado.ItemCount > 0 )
            {
               AV9Requisitado = cmbavRequisitado.getValidValue(AV9Requisitado);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
            }
            cmbavUsuario_codigo.Name = "vUSUARIO_CODIGO";
            cmbavUsuario_codigo.WebTags = "";
            cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavUsuario_codigo.ItemCount > 0 )
            {
               AV10Usuario_Codigo = (int)(NumberUtil.Val( cmbavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0)));
            }
            cmbavDmnvinculadas.Name = "vDMNVINCULADAS";
            cmbavDmnvinculadas.WebTags = "";
            cmbavDmnvinculadas.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Selecionar uma OS", 0);
            if ( cmbavDmnvinculadas.ItemCount > 0 )
            {
               AV123DmnVinculadas = (int)(NumberUtil.Val( cmbavDmnvinculadas.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            }
            dynavContratadaorigem.Name = "vCONTRATADAORIGEM";
            dynavContratadaorigem.WebTags = "";
            dynavContadorfs.Name = "vCONTADORFS";
            dynavContadorfs.WebTags = "";
            cmbavContratoservicos_localexec.Name = "vCONTRATOSERVICOS_LOCALEXEC";
            cmbavContratoservicos_localexec.WebTags = "";
            cmbavContratoservicos_localexec.addItem("A", "Contratada", 0);
            cmbavContratoservicos_localexec.addItem("E", "Contratante", 0);
            cmbavContratoservicos_localexec.addItem("O", "Opcional", 0);
            if ( cmbavContratoservicos_localexec.ItemCount > 0 )
            {
               AV18ContratoServicos_LocalExec = cmbavContratoservicos_localexec.getValidValue(AV18ContratoServicos_LocalExec);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicos_LocalExec", AV18ContratoServicos_LocalExec);
            }
            cmbavSistema_codigo.Name = "vSISTEMA_CODIGO";
            cmbavSistema_codigo.WebTags = "";
            cmbavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Nenhum", 0);
            if ( cmbavSistema_codigo.ItemCount > 0 )
            {
               AV38Sistema_Codigo = (int)(NumberUtil.Val( cmbavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
            }
            dynavModulo_codigo.Name = "vMODULO_CODIGO";
            dynavModulo_codigo.WebTags = "";
            dynavFuncaousuario_codigo.Name = "vFUNCAOUSUARIO_CODIGO";
            dynavFuncaousuario_codigo.WebTags = "";
            GXCCtl = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_" + sGXsfl_219_idx;
            cmbavSdt_requisitos__requisito_prioridade.Name = GXCCtl;
            cmbavSdt_requisitos__requisito_prioridade.WebTags = "";
            cmbavSdt_requisitos__requisito_prioridade.addItem("1", "Alta", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("2", "Alta M�dia", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("3", "Alta Baixa", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("4", "M�dia Alta", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("5", "M�dia M�dia", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("6", "M�dia Baixa", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("7", "Baixa Alta", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("8", "Baixa M�dia", 0);
            cmbavSdt_requisitos__requisito_prioridade.addItem("9", "Baixa Baixa", 0);
            if ( cmbavSdt_requisitos__requisito_prioridade.ItemCount > 0 )
            {
               if ( ( AV176GXV1 > 0 ) && ( AV161SDT_Requisitos.Count >= AV176GXV1 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade) )
               {
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade), 2, 0))), "."));
               }
            }
            GXCCtl = "SDT_REQUISITOS__REQUISITO_STATUS_" + sGXsfl_219_idx;
            cmbavSdt_requisitos__requisito_status.Name = GXCCtl;
            cmbavSdt_requisitos__requisito_status.WebTags = "";
            cmbavSdt_requisitos__requisito_status.addItem("0", "Rascunho", 0);
            cmbavSdt_requisitos__requisito_status.addItem("1", "Solicitado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("2", "Aprovado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("3", "N�o Aprovado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("4", "Pausado", 0);
            cmbavSdt_requisitos__requisito_status.addItem("5", "Cancelado", 0);
            if ( cmbavSdt_requisitos__requisito_status.ItemCount > 0 )
            {
               if ( ( AV176GXV1 > 0 ) && ( AV161SDT_Requisitos.Count >= AV176GXV1 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status) )
               {
                  ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status), 4, 0))), "."));
               }
            }
            cmbavServico_atende.Name = "vSERVICO_ATENDE";
            cmbavServico_atende.WebTags = "";
            cmbavServico_atende.addItem("S", "Software", 0);
            cmbavServico_atende.addItem("H", "Hardware", 0);
            if ( cmbavServico_atende.ItemCount > 0 )
            {
               AV109Servico_Atende = cmbavServico_atende.getValidValue(AV109Servico_Atende);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109Servico_Atende", AV109Servico_Atende);
            }
            cmbavServico_obrigavalores.Name = "vSERVICO_OBRIGAVALORES";
            cmbavServico_obrigavalores.WebTags = "";
            cmbavServico_obrigavalores.addItem("", "Nenhum", 0);
            cmbavServico_obrigavalores.addItem("L", "Valor L�quido", 0);
            cmbavServico_obrigavalores.addItem("B", "Valor Bruto", 0);
            cmbavServico_obrigavalores.addItem("A", "Ambos", 0);
            if ( cmbavServico_obrigavalores.ItemCount > 0 )
            {
               AV110Servico_ObrigaValores = cmbavServico_obrigavalores.getValidValue(AV110Servico_ObrigaValores);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Servico_ObrigaValores", AV110Servico_ObrigaValores);
            }
            cmbavServico_obrigavalores.Name = "vSERVICO_OBRIGAVALORES";
            cmbavServico_obrigavalores.WebTags = "";
            cmbavServico_obrigavalores.addItem("", "Nenhum", 0);
            cmbavServico_obrigavalores.addItem("L", "Valor L�quido", 0);
            cmbavServico_obrigavalores.addItem("B", "Valor Bruto", 0);
            cmbavServico_obrigavalores.addItem("A", "Ambos", 0);
            if ( cmbavServico_obrigavalores.ItemCount > 0 )
            {
               AV110Servico_ObrigaValores = cmbavServico_obrigavalores.getValidValue(AV110Servico_ObrigaValores);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Servico_ObrigaValores", AV110Servico_ObrigaValores);
            }
            chkavContratovencido.Name = "vCONTRATOVENCIDO";
            chkavContratovencido.WebTags = "";
            chkavContratovencido.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContratovencido_Internalname, "TitleCaption", chkavContratovencido.Caption);
            chkavContratovencido.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavRequisitadopor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTADORFS_htmlCG2( AV105ContratadaOrigem) ;
         GXVvMODULO_CODIGO_htmlCG2( AV38Sistema_Codigo) ;
         GXVvFUNCAOUSUARIO_CODIGO_htmlCG2( AV38Sistema_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvREQUISITANTECG2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvREQUISITANTE_dataCG2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvREQUISITANTE_htmlCG2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvREQUISITANTE_dataCG2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavRequisitante.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavRequisitante.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavRequisitante.ItemCount > 0 )
         {
            AV102Requisitante = (int)(NumberUtil.Val( dynavRequisitante.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0)));
         }
      }

      protected void GXDLVvREQUISITANTE_dataCG2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CG3 */
         pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CG3_A60ContratanteUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CG3_A62ContratanteUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATADAORIGEMCG2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADAORIGEM_dataCG2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADAORIGEM_htmlCG2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADAORIGEM_dataCG2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratadaorigem.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratadaorigem.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratadaorigem.ItemCount > 0 )
         {
            AV105ContratadaOrigem = (int)(NumberUtil.Val( dynavContratadaorigem.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADAORIGEM_dataCG2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00CG4 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CG4_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CG4_A41Contratada_PessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTADORFSCG2( int AV105ContratadaOrigem )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTADORFS_dataCG2( AV105ContratadaOrigem) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTADORFS_htmlCG2( int AV105ContratadaOrigem )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTADORFS_dataCG2( AV105ContratadaOrigem) ;
         gxdynajaxindex = 1;
         dynavContadorfs.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContadorfs.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContadorfs.ItemCount > 0 )
         {
            AV107ContadorFS = (int)(NumberUtil.Val( dynavContadorfs.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ContadorFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)));
         }
      }

      protected void GXDLVvCONTADORFS_dataCG2( int AV105ContratadaOrigem )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CG5 */
         pr_default.execute(2, new Object[] {AV105ContratadaOrigem});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CG5_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CG5_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvMODULO_CODIGOCG2( int AV38Sistema_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvMODULO_CODIGO_dataCG2( AV38Sistema_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvMODULO_CODIGO_htmlCG2( int AV38Sistema_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvMODULO_CODIGO_dataCG2( AV38Sistema_Codigo) ;
         gxdynajaxindex = 1;
         dynavModulo_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavModulo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV39Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvMODULO_CODIGO_dataCG2( int AV38Sistema_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00CG6 */
         pr_default.execute(3, new Object[] {AV38Sistema_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CG6_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00CG6_A145Modulo_Sigla[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvFUNCAOUSUARIO_CODIGOCG2( int AV38Sistema_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFUNCAOUSUARIO_CODIGO_dataCG2( AV38Sistema_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFUNCAOUSUARIO_CODIGO_htmlCG2( int AV38Sistema_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvFUNCAOUSUARIO_CODIGO_dataCG2( AV38Sistema_Codigo) ;
         gxdynajaxindex = 1;
         dynavFuncaousuario_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFuncaousuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavFuncaousuario_codigo.ItemCount > 0 )
         {
            AV41FuncaoUsuario_Codigo = (int)(NumberUtil.Val( dynavFuncaousuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvFUNCAOUSUARIO_CODIGO_dataCG2( int AV38Sistema_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00CG7 */
         pr_default.execute(4, new Object[] {AV38Sistema_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00CG7_A161FuncaoUsuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00CG7_A162FuncaoUsuario_Nome[0]);
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void gxnrGridrequisitos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_2192( ) ;
         while ( nGXsfl_219_idx <= nRC_GXsfl_219 )
         {
            sendrow_2192( ) ;
            nGXsfl_219_idx = (short)(((subGridrequisitos_Islastpage==1)&&(nGXsfl_219_idx+1>subGridrequisitos_Recordsperpage( )) ? 1 : nGXsfl_219_idx+1));
            sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
            SubsflControlProps_2192( ) ;
         }
         context.GX_webresponse.AddString(GridrequisitosContainer.ToJavascriptSource());
         /* End function gxnrGridrequisitos_newrow */
      }

      protected void gxgrGridrequisitos_refresh( int subGridrequisitos_Rows ,
                                                 wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                                 bool AV134Contratante_UsaOSistema ,
                                                 int AV13Servico_Codigo ,
                                                 int A1Usuario_Codigo ,
                                                 String A58Usuario_PessoaNom ,
                                                 int A1075Usuario_CargoUOCod ,
                                                 String A1076Usuario_CargoUONom ,
                                                 int A52Contratada_AreaTrabalhoCod ,
                                                 int AV100AreaTrabalho_Codigo ,
                                                 int A39Contratada_Codigo ,
                                                 IGxCollection AV132ContratadasDaArea ,
                                                 String A158ServicoGrupo_Descricao ,
                                                 int A75Contrato_AreaTrabalhoCod ,
                                                 bool A92Contrato_Ativo ,
                                                 bool A638ContratoServicos_Ativo ,
                                                 int A157ServicoGrupo_Codigo ,
                                                 String A129Sistema_Sigla ,
                                                 int A135Sistema_AreaTrabalhoCod ,
                                                 String A699Sistema_Tipo ,
                                                 bool A130Sistema_Ativo ,
                                                 int A127Sistema_Codigo ,
                                                 IGxCollection AV51Sistemas ,
                                                 decimal AV172ContratoServicos_QntUntCns ,
                                                 bool AV166Servico_IsOrigemReferencia ,
                                                 bool AV168Servico_IsSolicitaGestorSistema ,
                                                 int A1446ContratoGestor_ContratadaAreaCod ,
                                                 int A1079ContratoGestor_UsuarioCod ,
                                                 int AV102Requisitante ,
                                                 DateTime A843Contrato_DataFimTA ,
                                                 DateTime A83Contrato_DataVigenciaTermino ,
                                                 bool A43Contratada_Ativo ,
                                                 int A74Contrato_Codigo ,
                                                 int A1078ContratoGestor_ContratoCod ,
                                                 int AV16ServicoGrupo_Codigo ,
                                                 int A155Servico_Codigo ,
                                                 int A1825ContratoAuxiliar_UsuarioCod ,
                                                 int A1824ContratoAuxiliar_ContratoCod ,
                                                 int AV170ServicoCodigo ,
                                                 IGxCollection AV136Servicos ,
                                                 String A605Servico_Sigla ,
                                                 String A608Servico_Nome ,
                                                 IGxCollection AV137SDT_Servicos ,
                                                 IGxCollection AV161SDT_Requisitos ,
                                                 String AV146Agrupador )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Rows), 6, 0, ".", "")));
         GRIDREQUISITOS_nCurrentRecord = 0;
         RFCG2( ) ;
         /* End function gxgrGridrequisitos_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavRequisitante.ItemCount > 0 )
         {
            AV102Requisitante = (int)(NumberUtil.Val( dynavRequisitante.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0)));
         }
         if ( cmbavContrato.ItemCount > 0 )
         {
            AV117Contrato = (int)(NumberUtil.Val( cmbavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)));
         }
         if ( cmbavServicogrupo_codigo.ItemCount > 0 )
         {
            AV16ServicoGrupo_Codigo = (int)(NumberUtil.Val( cmbavServicogrupo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
         }
         if ( cmbavServico_codigo.ItemCount > 0 )
         {
            AV13Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
         }
         if ( cmbavComplexidade.ItemCount > 0 )
         {
            AV97Complexidade = (short)(NumberUtil.Val( cmbavComplexidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)));
         }
         if ( cmbavPrioridade_codigo.ItemCount > 0 )
         {
            AV96Prioridade_Codigo = (int)(NumberUtil.Val( cmbavPrioridade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Prioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)));
         }
         if ( cmbavRequisitado.ItemCount > 0 )
         {
            AV9Requisitado = cmbavRequisitado.getValidValue(AV9Requisitado);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
         }
         if ( cmbavUsuario_codigo.ItemCount > 0 )
         {
            AV10Usuario_Codigo = (int)(NumberUtil.Val( cmbavUsuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0)));
         }
         if ( cmbavDmnvinculadas.ItemCount > 0 )
         {
            AV123DmnVinculadas = (int)(NumberUtil.Val( cmbavDmnvinculadas.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
         }
         if ( dynavContratadaorigem.ItemCount > 0 )
         {
            AV105ContratadaOrigem = (int)(NumberUtil.Val( dynavContratadaorigem.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
         }
         if ( dynavContadorfs.ItemCount > 0 )
         {
            AV107ContadorFS = (int)(NumberUtil.Val( dynavContadorfs.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ContadorFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)));
         }
         if ( cmbavContratoservicos_localexec.ItemCount > 0 )
         {
            AV18ContratoServicos_LocalExec = cmbavContratoservicos_localexec.getValidValue(AV18ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicos_LocalExec", AV18ContratoServicos_LocalExec);
         }
         if ( cmbavSistema_codigo.ItemCount > 0 )
         {
            AV38Sistema_Codigo = (int)(NumberUtil.Val( cmbavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
         }
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV39Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
         }
         if ( dynavFuncaousuario_codigo.ItemCount > 0 )
         {
            AV41FuncaoUsuario_Codigo = (int)(NumberUtil.Val( dynavFuncaousuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
         }
         if ( cmbavServico_atende.ItemCount > 0 )
         {
            AV109Servico_Atende = cmbavServico_atende.getValidValue(AV109Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109Servico_Atende", AV109Servico_Atende);
         }
         if ( cmbavServico_obrigavalores.ItemCount > 0 )
         {
            AV110Servico_ObrigaValores = cmbavServico_obrigavalores.getValidValue(AV110Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Servico_ObrigaValores", AV110Servico_ObrigaValores);
         }
         if ( cmbavServico_obrigavalores.ItemCount > 0 )
         {
            AV110Servico_ObrigaValores = cmbavServico_obrigavalores.getValidValue(AV110Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Servico_ObrigaValores", AV110Servico_ObrigaValores);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         /* Execute user event: E32CG2 */
         E32CG2 ();
         RFCG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavUsuario_cargouonom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargouonom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargouonom_Enabled), 5, 0)));
         edtavUsuario_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
         edtavContagemresultado_datadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn_Enabled), 5, 0)));
         edtavContratoservicos_undcntnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_undcntnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_undcntnome_Enabled), 5, 0)));
         edtavContratoservicos_qntuntcns_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_qntuntcns_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_qntuntcns_Enabled), 5, 0)));
         edtavPrazoanalise_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoanalise_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoanalise_Enabled), 5, 0)));
         edtavSistema_gestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_gestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_identificador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_titulo_Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_prioridade.Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_status.Enabled), 5, 0)));
      }

      protected void RFCG2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridrequisitosContainer.ClearRows();
         }
         wbStart = 219;
         /* Execute user event: E33CG2 */
         E33CG2 ();
         nGXsfl_219_idx = 1;
         sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
         SubsflControlProps_2192( ) ;
         nGXsfl_219_Refreshing = 1;
         GridrequisitosContainer.AddObjectProperty("GridName", "Gridrequisitos");
         GridrequisitosContainer.AddObjectProperty("CmpContext", "");
         GridrequisitosContainer.AddObjectProperty("InMasterPage", "false");
         GridrequisitosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridrequisitosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridrequisitosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridrequisitosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Backcolorstyle), 1, 0, ".", "")));
         GridrequisitosContainer.PageSize = subGridrequisitos_Recordsperpage( );
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
               {
                  WebComp_Wcarquivosevd.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_2192( ) ;
            /* Execute user event: E31CG2 */
            E31CG2 ();
            if ( ( GRIDREQUISITOS_nCurrentRecord > 0 ) && ( GRIDREQUISITOS_nGridOutOfScope == 0 ) && ( nGXsfl_219_idx == 1 ) )
            {
               GRIDREQUISITOS_nCurrentRecord = 0;
               GRIDREQUISITOS_nGridOutOfScope = 1;
               subgridrequisitos_firstpage( ) ;
               /* Execute user event: E31CG2 */
               E31CG2 ();
            }
            wbEnd = 219;
            WBCG0( ) ;
         }
         nGXsfl_219_Refreshing = 0;
      }

      protected int subGridrequisitos_Pagecount( )
      {
         GRIDREQUISITOS_nRecordCount = subGridrequisitos_Recordcount( );
         if ( ((int)((GRIDREQUISITOS_nRecordCount) % (subGridrequisitos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDREQUISITOS_nRecordCount/ (decimal)(subGridrequisitos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDREQUISITOS_nRecordCount/ (decimal)(subGridrequisitos_Recordsperpage( ))))+1) ;
      }

      protected int subGridrequisitos_Recordcount( )
      {
         return AV161SDT_Requisitos.Count ;
      }

      protected int subGridrequisitos_Recordsperpage( )
      {
         if ( subGridrequisitos_Rows > 0 )
         {
            return subGridrequisitos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridrequisitos_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDREQUISITOS_nFirstRecordOnPage/ (decimal)(subGridrequisitos_Recordsperpage( ))))+1) ;
      }

      protected short subgridrequisitos_firstpage( )
      {
         GRIDREQUISITOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         }
         return 0 ;
      }

      protected short subgridrequisitos_nextpage( )
      {
         GRIDREQUISITOS_nRecordCount = subGridrequisitos_Recordcount( );
         if ( ( GRIDREQUISITOS_nRecordCount >= subGridrequisitos_Recordsperpage( ) ) && ( GRIDREQUISITOS_nEOF == 0 ) )
         {
            GRIDREQUISITOS_nFirstRecordOnPage = (long)(GRIDREQUISITOS_nFirstRecordOnPage+subGridrequisitos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         }
         return (short)(((GRIDREQUISITOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridrequisitos_previouspage( )
      {
         if ( GRIDREQUISITOS_nFirstRecordOnPage >= subGridrequisitos_Recordsperpage( ) )
         {
            GRIDREQUISITOS_nFirstRecordOnPage = (long)(GRIDREQUISITOS_nFirstRecordOnPage-subGridrequisitos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         }
         return 0 ;
      }

      protected short subgridrequisitos_lastpage( )
      {
         GRIDREQUISITOS_nRecordCount = subGridrequisitos_Recordcount( );
         if ( GRIDREQUISITOS_nRecordCount > subGridrequisitos_Recordsperpage( ) )
         {
            if ( ((int)((GRIDREQUISITOS_nRecordCount) % (subGridrequisitos_Recordsperpage( )))) == 0 )
            {
               GRIDREQUISITOS_nFirstRecordOnPage = (long)(GRIDREQUISITOS_nRecordCount-subGridrequisitos_Recordsperpage( ));
            }
            else
            {
               GRIDREQUISITOS_nFirstRecordOnPage = (long)(GRIDREQUISITOS_nRecordCount-((int)((GRIDREQUISITOS_nRecordCount) % (subGridrequisitos_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDREQUISITOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         }
         return 0 ;
      }

      protected int subgridrequisitos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDREQUISITOS_nFirstRecordOnPage = (long)(subGridrequisitos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDREQUISITOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         }
         return (int)(0) ;
      }

      protected void STRUPCG0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavUsuario_cargouonom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_cargouonom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_cargouonom_Enabled), 5, 0)));
         edtavUsuario_pessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom_Enabled), 5, 0)));
         edtavContagemresultado_datadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn_Enabled), 5, 0)));
         edtavContratoservicos_undcntnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_undcntnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_undcntnome_Enabled), 5, 0)));
         edtavContratoservicos_qntuntcns_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_qntuntcns_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_qntuntcns_Enabled), 5, 0)));
         edtavPrazoanalise_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoanalise_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoanalise_Enabled), 5, 0)));
         edtavSistema_gestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_gestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_identificador_Enabled), 5, 0)));
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_requisitos__requisito_titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_requisitos__requisito_titulo_Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_prioridade.Enabled), 5, 0)));
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSdt_requisitos__requisito_status.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E30CG2 */
         E30CG2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvREQUISITANTE_htmlCG2( AV6WWPContext) ;
         GXVvCONTRATADAORIGEM_htmlCG2( AV6WWPContext) ;
         GXVvCONTADORFS_htmlCG2( AV105ContratadaOrigem) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_requisitos"), AV161SDT_Requisitos);
            ajax_req_read_hidden_sdt(cgiGet( "vSDT_REQUISITOS"), AV161SDT_Requisitos);
            /* Read variables values. */
            AV101RequisitadoPor = StringUtil.StrToBool( cgiGet( chkavRequisitadopor_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101RequisitadoPor", AV101RequisitadoPor);
            dynavRequisitante.Name = dynavRequisitante_Internalname;
            dynavRequisitante.CurrentValue = cgiGet( dynavRequisitante_Internalname);
            AV102Requisitante = (int)(NumberUtil.Val( cgiGet( dynavRequisitante_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0)));
            AV56Usuario_CargoUONom = StringUtil.Upper( cgiGet( edtavUsuario_cargouonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Usuario_CargoUONom", AV56Usuario_CargoUONom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV56Usuario_CargoUONom, "@!"))));
            AV69ContagemResultado_DemandaFM = cgiGet( edtavContagemresultado_demandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_DemandaFM", AV69ContagemResultado_DemandaFM);
            AV7Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_PessoaNom", AV7Usuario_PessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Usuario_PessoaNom, "@!"))));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn"}), 1, "vCONTAGEMRESULTADO_DATADMN");
               GX_FocusControl = edtavContagemresultado_datadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8ContagemResultado_DataDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_DataDmn", context.localUtil.Format(AV8ContagemResultado_DataDmn, "99/99/99"));
            }
            else
            {
               AV8ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_DataDmn", context.localUtil.Format(AV8ContagemResultado_DataDmn, "99/99/99"));
            }
            AV88ContagemResultado_Descricao = cgiGet( edtavContagemresultado_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ContagemResultado_Descricao", AV88ContagemResultado_Descricao);
            cmbavContrato.Name = cmbavContrato_Internalname;
            cmbavContrato.CurrentValue = cgiGet( cmbavContrato_Internalname);
            AV117Contrato = (int)(NumberUtil.Val( cgiGet( cmbavContrato_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)));
            cmbavServicogrupo_codigo.Name = cmbavServicogrupo_codigo_Internalname;
            cmbavServicogrupo_codigo.CurrentValue = cgiGet( cmbavServicogrupo_codigo_Internalname);
            AV16ServicoGrupo_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavServicogrupo_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
            cmbavServico_codigo.Name = cmbavServico_codigo_Internalname;
            cmbavServico_codigo.CurrentValue = cgiGet( cmbavServico_codigo_Internalname);
            AV13Servico_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
            AV171ContratoServicos_UndCntNome = StringUtil.Upper( cgiGet( edtavContratoservicos_undcntnome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV171ContratoServicos_UndCntNome", AV171ContratoServicos_UndCntNome);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_qntuntcns_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_qntuntcns_Internalname), ",", ".") > 9999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOS_QNTUNTCNS");
               GX_FocusControl = edtavContratoservicos_qntuntcns_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV172ContratoServicos_QntUntCns = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV172ContratoServicos_QntUntCns, 9, 4)));
            }
            else
            {
               AV172ContratoServicos_QntUntCns = context.localUtil.CToN( cgiGet( edtavContratoservicos_qntuntcns_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV172ContratoServicos_QntUntCns, 9, 4)));
            }
            cmbavComplexidade.Name = cmbavComplexidade_Internalname;
            cmbavComplexidade.CurrentValue = cgiGet( cmbavComplexidade_Internalname);
            AV97Complexidade = (short)(NumberUtil.Val( cgiGet( cmbavComplexidade_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)));
            cmbavPrioridade_codigo.Name = cmbavPrioridade_codigo_Internalname;
            cmbavPrioridade_codigo.CurrentValue = cgiGet( cmbavPrioridade_codigo_Internalname);
            AV96Prioridade_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavPrioridade_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Prioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".") > 9999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_QUANTIDADESOLICITADA");
               GX_FocusControl = edtavContagemresultado_quantidadesolicitada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV173ContagemResultado_QuantidadeSolicitada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV173ContagemResultado_QuantidadeSolicitada, 9, 4)));
            }
            else
            {
               AV173ContagemResultado_QuantidadeSolicitada = context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV173ContagemResultado_QuantidadeSolicitada, 9, 4)));
            }
            cmbavRequisitado.Name = cmbavRequisitado_Internalname;
            cmbavRequisitado.CurrentValue = cgiGet( cmbavRequisitado_Internalname);
            AV9Requisitado = cgiGet( cmbavRequisitado_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
            cmbavUsuario_codigo.Name = cmbavUsuario_codigo_Internalname;
            cmbavUsuario_codigo.CurrentValue = cgiGet( cmbavUsuario_codigo_Internalname);
            AV10Usuario_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavUsuario_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0)));
            cmbavDmnvinculadas.Name = cmbavDmnvinculadas_Internalname;
            cmbavDmnvinculadas.CurrentValue = cgiGet( cmbavDmnvinculadas_Internalname);
            AV123DmnVinculadas = (int)(NumberUtil.Val( cgiGet( cmbavDmnvinculadas_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            AV22ContagemResultado_DmnVinculada = StringUtil.Upper( cgiGet( edtavContagemresultado_dmnvinculada_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
            AV133ContagemResultado_DmnVinculadaRef = StringUtil.Upper( cgiGet( edtavContagemresultado_dmnvinculadaref_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV133ContagemResultado_DmnVinculadaRef", AV133ContagemResultado_DmnVinculadaRef);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfsdaosvnc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfsdaosvnc_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFSDAOSVNC");
               GX_FocusControl = edtavPfbfsdaosvnc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66PFBFSdaOSVnc = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
            }
            else
            {
               AV66PFBFSdaOSVnc = context.localUtil.CToN( cgiGet( edtavPfbfsdaosvnc_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPflfsdaosvnc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPflfsdaosvnc_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFLFSDAOSVNC");
               GX_FocusControl = edtavPflfsdaosvnc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73PFLFSdaOSVnc = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
            }
            else
            {
               AV73PFLFSdaOSVnc = context.localUtil.CToN( cgiGet( edtavPflfsdaosvnc_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
            }
            dynavContratadaorigem.Name = dynavContratadaorigem_Internalname;
            dynavContratadaorigem.CurrentValue = cgiGet( dynavContratadaorigem_Internalname);
            AV105ContratadaOrigem = (int)(NumberUtil.Val( cgiGet( dynavContratadaorigem_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
            dynavContadorfs.Name = dynavContadorfs_Internalname;
            dynavContadorfs.CurrentValue = cgiGet( dynavContadorfs_Internalname);
            AV107ContadorFS = (int)(NumberUtil.Val( cgiGet( dynavContadorfs_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ContadorFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)));
            cmbavContratoservicos_localexec.Name = cmbavContratoservicos_localexec_Internalname;
            cmbavContratoservicos_localexec.CurrentValue = cgiGet( cmbavContratoservicos_localexec_Internalname);
            AV18ContratoServicos_LocalExec = cgiGet( cmbavContratoservicos_localexec_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicos_LocalExec", AV18ContratoServicos_LocalExec);
            if ( context.localUtil.VCDateTime( cgiGet( edtavPrazoanalise_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Prazo Analise"}), 1, "vPRAZOANALISE");
               GX_FocusControl = edtavPrazoanalise_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV70PrazoAnalise = context.localUtil.CToT( cgiGet( edtavPrazoanalise_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPrazoentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Prazo de entrega"}), 1, "vPRAZOENTREGA");
               GX_FocusControl = edtavPrazoentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV62PrazoEntrega = context.localUtil.CToT( cgiGet( edtavPrazoentrega_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavSistema_codigo.Name = cmbavSistema_codigo_Internalname;
            cmbavSistema_codigo.CurrentValue = cgiGet( cmbavSistema_codigo_Internalname);
            AV38Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
            dynavModulo_codigo.Name = dynavModulo_codigo_Internalname;
            dynavModulo_codigo.CurrentValue = cgiGet( dynavModulo_codigo_Internalname);
            AV39Modulo_Codigo = (int)(NumberUtil.Val( cgiGet( dynavModulo_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
            dynavFuncaousuario_codigo.Name = dynavFuncaousuario_codigo_Internalname;
            dynavFuncaousuario_codigo.CurrentValue = cgiGet( dynavFuncaousuario_codigo_Internalname);
            AV41FuncaoUsuario_Codigo = (int)(NumberUtil.Val( cgiGet( dynavFuncaousuario_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
            AV167Sistema_Gestor = cgiGet( edtavSistema_gestor_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV167Sistema_Gestor", AV167Sistema_Gestor);
            AV15ContagemResultado_Link = cgiGet( edtavContagemresultado_link_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Link", AV15ContagemResultado_Link);
            cmbavServico_atende.Name = cmbavServico_atende_Internalname;
            cmbavServico_atende.CurrentValue = cgiGet( cmbavServico_atende_Internalname);
            AV109Servico_Atende = cgiGet( cmbavServico_atende_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109Servico_Atende", AV109Servico_Atende);
            cmbavServico_obrigavalores.Name = cmbavServico_obrigavalores_Internalname;
            cmbavServico_obrigavalores.CurrentValue = cgiGet( cmbavServico_obrigavalores_Internalname);
            AV110Servico_ObrigaValores = cgiGet( cmbavServico_obrigavalores_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Servico_ObrigaValores", AV110Servico_ObrigaValores);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV100AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV100AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100AreaTrabalho_Codigo), 6, 0)));
            }
            cmbavServico_obrigavalores.Name = cmbavServico_obrigavalores_Internalname;
            cmbavServico_obrigavalores.CurrentValue = cgiGet( cmbavServico_obrigavalores_Internalname);
            AV110Servico_ObrigaValores = cgiGet( cmbavServico_obrigavalores_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Servico_ObrigaValores", AV110Servico_ObrigaValores);
            AV139ContratoVencido = StringUtil.StrToBool( cgiGet( chkavContratovencido_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV139ContratoVencido", AV139ContratoVencido);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRequisitadocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRequisitadocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREQUISITADOCOD");
               GX_FocusControl = edtavRequisitadocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12RequisitadoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
            }
            else
            {
               AV12RequisitadoCod = (int)(context.localUtil.CToN( cgiGet( edtavRequisitadocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_219 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_219"), ",", "."));
            AV19ContagemResultado_Observacao = cgiGet( "vCONTAGEMRESULTADO_OBSERVACAO");
            GRIDREQUISITOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDREQUISITOS_nFirstRecordOnPage"), ",", "."));
            GRIDREQUISITOS_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDREQUISITOS_nEOF"), ",", "."));
            subGridrequisitos_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDREQUISITOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Rows), 6, 0, ".", "")));
            Contagemresultado_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Enabled"));
            Gxuitabspanel_tab_Width = cgiGet( "GXUITABSPANEL_TAB_Width");
            Gxuitabspanel_tab_Cls = cgiGet( "GXUITABSPANEL_TAB_Cls");
            Gxuitabspanel_tab_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autowidth"));
            Gxuitabspanel_tab_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autoheight"));
            Gxuitabspanel_tab_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TAB_Autoscroll"));
            Gxuitabspanel_tab_Designtimetabs = cgiGet( "GXUITABSPANEL_TAB_Designtimetabs");
            Dvelop_bootstrap_confirmpanel1_Title = cgiGet( "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Title");
            Dvelop_bootstrap_confirmpanel1_Confirmationtext = cgiGet( "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Confirmationtext");
            Dvelop_bootstrap_confirmpanel1_Yesbuttoncaption = cgiGet( "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Yesbuttoncaption");
            Dvelop_bootstrap_confirmpanel1_Confirmtype = cgiGet( "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Confirmtype");
            Confirmpanel2_Title = cgiGet( "CONFIRMPANEL2_Title");
            Confirmpanel2_Confirmationtext = cgiGet( "CONFIRMPANEL2_Confirmationtext");
            Confirmpanel2_Yesbuttoncaption = cgiGet( "CONFIRMPANEL2_Yesbuttoncaption");
            Confirmpanel2_Confirmtype = cgiGet( "CONFIRMPANEL2_Confirmtype");
            Dvelop_bootstrap_confirmpanel1_Title = cgiGet( "DVELOP_BOOTSTRAP_CONFIRMPANEL1_Title");
            Confirmpanel2_Title = cgiGet( "CONFIRMPANEL2_Title");
            nRC_GXsfl_219 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_219"), ",", "."));
            nGXsfl_219_fel_idx = 0;
            while ( nGXsfl_219_fel_idx < nRC_GXsfl_219 )
            {
               nGXsfl_219_fel_idx = (short)(((subGridrequisitos_Islastpage==1)&&(nGXsfl_219_fel_idx+1>subGridrequisitos_Recordsperpage( )) ? 1 : nGXsfl_219_fel_idx+1));
               sGXsfl_219_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_2192( ) ;
               AV176GXV1 = (short)(nGXsfl_219_fel_idx+GRIDREQUISITOS_nFirstRecordOnPage);
               if ( ( AV161SDT_Requisitos.Count >= AV176GXV1 ) && ( AV176GXV1 > 0 ) )
               {
                  AV161SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1));
                  AV147btnExcluirReqNeg = cgiGet( edtavBtnexcluirreqneg_Internalname);
                  AV146Agrupador = cgiGet( edtavAgrupador_Internalname);
               }
            }
            if ( nGXsfl_219_fel_idx == 0 )
            {
               nGXsfl_219_idx = 1;
               sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
               SubsflControlProps_2192( ) ;
            }
            nGXsfl_219_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E30CG2 */
         E30CG2 ();
         if (returnInSub) return;
      }

      protected void E30CG2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.3 - Data: 15/03/2020", 0) ;
         Form.Meta.addItem("Content", "GeneXus", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         AV29Websession.Remove("ArquivosEvd");
         AV8ContagemResultado_DataDmn = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_DataDmn", context.localUtil.Format(AV8ContagemResultado_DataDmn, "99/99/99"));
         /* Using cursor H00CG8 */
         pr_default.execute(5, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A29Contratante_Codigo = H00CG8_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00CG8_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00CG8_A5AreaTrabalho_Codigo[0];
            A593Contratante_OSAutomatica = H00CG8_A593Contratante_OSAutomatica[0];
            A2085Contratante_RequerOrigem = H00CG8_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = H00CG8_n2085Contratante_RequerOrigem[0];
            A6AreaTrabalho_Descricao = H00CG8_A6AreaTrabalho_Descricao[0];
            A548Contratante_EmailSdaUser = H00CG8_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00CG8_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00CG8_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00CG8_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00CG8_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00CG8_n547Contratante_EmailSdaHost[0];
            A1822Contratante_UsaOSistema = H00CG8_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = H00CG8_n1822Contratante_UsaOSistema[0];
            A593Contratante_OSAutomatica = H00CG8_A593Contratante_OSAutomatica[0];
            A2085Contratante_RequerOrigem = H00CG8_A2085Contratante_RequerOrigem[0];
            n2085Contratante_RequerOrigem = H00CG8_n2085Contratante_RequerOrigem[0];
            A548Contratante_EmailSdaUser = H00CG8_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00CG8_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00CG8_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00CG8_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00CG8_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00CG8_n547Contratante_EmailSdaHost[0];
            A1822Contratante_UsaOSistema = H00CG8_A1822Contratante_UsaOSistema[0];
            n1822Contratante_UsaOSistema = H00CG8_n1822Contratante_UsaOSistema[0];
            dynavModulo_codigo.Enabled = ((0==AV39Modulo_Codigo) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Enabled), 5, 0)));
            cmbavSistema_codigo.Enabled = ((0==AV38Sistema_Codigo) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Enabled), 5, 0)));
            AV53Contratante_OSAutomatica = A593Contratante_OSAutomatica;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Contratante_OSAutomatica", AV53Contratante_OSAutomatica);
            AV163RequerOrigem = A2085Contratante_RequerOrigem;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV163RequerOrigem", AV163RequerOrigem);
            lblRequisitadopor_righttext_Caption = "�"+A6AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblRequisitadopor_righttext_Internalname, "Caption", lblRequisitadopor_righttext_Caption);
            if ( H00CG8_n547Contratante_EmailSdaHost[0] || H00CG8_n552Contratante_EmailSdaPort[0] || H00CG8_n548Contratante_EmailSdaUser[0] )
            {
               GX_msglist.addItem("Cadastro da Contratante sem dados configurados para envio de e-mails!");
               AV61ContratanteSemEmailSda = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ContratanteSemEmailSda", AV61ContratanteSemEmailSda);
               AV134Contratante_UsaOSistema = A1822Contratante_UsaOSistema;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Contratante_UsaOSistema", AV134Contratante_UsaOSistema);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
         lblTextblockprioridade_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockprioridade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockprioridade_codigo_Visible), 5, 0)));
         lblTextblockcomplexidade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcomplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcomplexidade_Visible), 5, 0)));
         cmbavPrioridade_codigo.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPrioridade_codigo.Visible), 5, 0)));
         cmbavComplexidade.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavComplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavComplexidade.Visible), 5, 0)));
         lblTextblockpfbfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
         lblTextblockpflfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
         edtavPfbfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
         edtavPflfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
         cmbavServico_atende.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_atende_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServico_atende.Visible), 5, 0)));
         cmbavServico_obrigavalores.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_obrigavalores_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServico_obrigavalores.Visible), 5, 0)));
         AV69ContagemResultado_DemandaFM = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_DemandaFM", AV69ContagemResultado_DemandaFM);
         if ( AV53Contratante_OSAutomatica )
         {
            AV69ContagemResultado_DemandaFM = "Gera��o autom�tica";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_DemandaFM", AV69ContagemResultado_DemandaFM);
         }
         edtavContagemresultado_demandafm_Enabled = (!AV53Contratante_OSAutomatica ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm_Enabled), 5, 0)));
         AV89FCKEditorMenuItem.gxTpr_Id = "upload";
         AV89FCKEditorMenuItem.gxTpr_Description = "Inserir imagem local";
         AV89FCKEditorMenuItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( ))));
         AV89FCKEditorMenuItem.gxTpr_Objectinterface = 1;
         AV89FCKEditorMenuItem.gxTpr_Link = formatLink("wp_fileupload.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV92Url));
         AV91FckEditorMenu.Add(AV89FCKEditorMenuItem, 0);
         this.executeUsercontrolMethod("", false, "CONTAGEMRESULTADO_OBSERVACAOContainer", "SetMenu", "", new Object[] {(IGxCollection)AV91FckEditorMenu});
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if (returnInSub) return;
         edtavAreatrabalho_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_codigo_Visible), 5, 0)));
         cmbavServico_obrigavalores.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_obrigavalores_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavServico_obrigavalores.Visible), 5, 0)));
         chkavContratovencido.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavContratovencido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavContratovencido.Visible), 5, 0)));
         edtavRequisitadocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisitadocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisitadocod_Visible), 5, 0)));
         subGridrequisitos_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Rows), 6, 0, ".", "")));
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcarquivosevd_Component), StringUtil.Lower( "WC_ContagemResultadoEvidencias")) != 0 )
         {
            WebComp_Wcarquivosevd = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wcarquivosevd.ComponentInit();
            WebComp_Wcarquivosevd.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wcarquivosevd_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
         {
            WebComp_Wcarquivosevd.setjustcreated();
            WebComp_Wcarquivosevd.componentprepare(new Object[] {(String)"W0207",(String)""});
            WebComp_Wcarquivosevd.componentbind(new Object[] {});
         }
         lblTextblockrequisitante_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockrequisitante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockrequisitante_Visible), 5, 0)));
         dynavRequisitante.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavRequisitante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavRequisitante.Visible), 5, 0)));
         lblTextblockcontrato_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_Visible), 5, 0)));
         cmbavContrato.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContrato.Visible), 5, 0)));
         cmbavDmnvinculadas.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDmnvinculadas.Visible), 5, 0)));
         bttBtnconfirmar_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirmar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirmar_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> document.getElementById(\"GXUITABSPANEL_TABContainer\").setAttribute(\"class\",\"gx_usercontrol tab-container\");</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         AV23ContagemResultado_OSVinculada = (int)(NumberUtil.Val( AV29Websession.Get("Codigo"), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
         if ( AV23ContagemResultado_OSVinculada > 0 )
         {
            AV29Websession.Remove("Codigo");
            /* Execute user subroutine: 'DADOSDOCALLER' */
            S122 ();
            if (returnInSub) return;
            /* Execute user subroutine: 'EXTERNALCALL' */
            S132 ();
            if (returnInSub) return;
         }
         else
         {
            if ( AV6WWPContext.gxTpr_Userehcontratante )
            {
               /* Execute user subroutine: 'CARREGASERVICOS' */
               S142 ();
               if (returnInSub) return;
            }
            GX_FocusControl = edtavContagemresultado_descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         AV126lastContagemResultado_Codigo = (int)(NumberUtil.Val( AV29Websession.Get("LastContagemResultado_Codigo"), "."));
         if ( AV126lastContagemResultado_Codigo > 0 )
         {
            lblLblultimaos_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblultimaos_Internalname, "Caption", lblLblultimaos_Caption);
            lblLblultimaos_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblultimaos_Internalname, "Link", lblLblultimaos_Link);
         }
         else
         {
            lblLblultimaos_Caption = "Ultima OS cadastrada: "+StringUtil.Trim( StringUtil.Str( (decimal)(AV126lastContagemResultado_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblultimaos_Internalname, "Caption", lblLblultimaos_Caption);
            lblLblultimaos_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV126lastContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("General"));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblultimaos_Internalname, "Link", lblLblultimaos_Link);
         }
         AV29Websession.Remove("LastContagemResultado_Codigo");
         bttBtnartefatos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnartefatos_Visible), 5, 0)));
      }

      private void E31CG2( )
      {
         /* Gridrequisitos_Load Routine */
         AV176GXV1 = 1;
         while ( AV176GXV1 <= AV161SDT_Requisitos.Count )
         {
            AV161SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1));
            AV147btnExcluirReqNeg = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnexcluirreqneg_Internalname, AV147btnExcluirReqNeg);
            AV182Btnexcluirreqneg_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavBtnexcluirreqneg_Tooltiptext = "";
            if ( StringUtil.StrCmp(AV146Agrupador, ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV161SDT_Requisitos.CurrentItem)).gxTpr_Requisito_agrupador) == 0 )
            {
               edtavAgrupador_Forecolor = GXUtil.RGB( 255, 255, 255);
            }
            else
            {
               edtavAgrupador_Forecolor = GXUtil.RGB( 0, 0, 0);
               AV146Agrupador = ((SdtSDT_Requisitos_SDT_RequisitosItem)(AV161SDT_Requisitos.CurrentItem)).gxTpr_Requisito_agrupador;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV146Agrupador);
            }
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 219;
            }
            if ( ( subGridrequisitos_Islastpage == 1 ) || ( subGridrequisitos_Rows == 0 ) || ( ( GRIDREQUISITOS_nCurrentRecord >= GRIDREQUISITOS_nFirstRecordOnPage ) && ( GRIDREQUISITOS_nCurrentRecord < GRIDREQUISITOS_nFirstRecordOnPage + subGridrequisitos_Recordsperpage( ) ) ) )
            {
               sendrow_2192( ) ;
               GRIDREQUISITOS_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nEOF), 1, 0, ".", "")));
               if ( GRIDREQUISITOS_nCurrentRecord + 1 >= subGridrequisitos_Recordcount( ) )
               {
                  GRIDREQUISITOS_nEOF = 1;
                  GxWebStd.gx_hidden_field( context, "GRIDREQUISITOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDREQUISITOS_nEOF), 1, 0, ".", "")));
               }
            }
            GRIDREQUISITOS_nCurrentRecord = (long)(GRIDREQUISITOS_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_219_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(219, GridrequisitosRow);
            }
            AV176GXV1 = (short)(AV176GXV1+1);
         }
      }

      protected void E13CG2( )
      {
         /* 'DoArtefatos' Routine */
         bttBtnconfirmar_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirmar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirmar_Visible), 5, 0)));
         /* Execute user subroutine: 'SOLICITACAOARTEFATOS' */
         S152 ();
         if (returnInSub) return;
      }

      protected void E14CG2( )
      {
         AV176GXV1 = (short)(nGXsfl_219_idx+GRIDREQUISITOS_nFirstRecordOnPage);
         if ( AV161SDT_Requisitos.Count >= AV176GXV1 )
         {
            AV161SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1));
         }
         /* 'DoConfirmar' Routine */
         /* Execute user subroutine: 'CHECKREQUIREDFIELDS' */
         S162 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV161SDT_Requisitos", AV161SDT_Requisitos);
         nGXsfl_219_bak_idx = nGXsfl_219_idx;
         gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         nGXsfl_219_idx = nGXsfl_219_bak_idx;
         sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
         SubsflControlProps_2192( ) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV158Codigos", AV158Codigos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Usuarios", AV42Usuarios);
      }

      protected void S162( )
      {
         /* 'CHECKREQUIREDFIELDS' Routine */
         AV17CheckRequiredFieldsResult = true;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69ContagemResultado_DemandaFM)) )
         {
            GX_msglist.addItem("N� OS � obrigat�rio.");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88ContagemResultado_Descricao)) )
         {
            GX_msglist.addItem("Titulo � obrigat�rio.");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( (0==AV13Servico_Codigo) )
         {
            GX_msglist.addItem("Servi�o � obrigat�rio.");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Requisitado)) )
         {
            GX_msglist.addItem("Requisitado � obrigat�rio.");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( ( cmbavContrato.ItemCount > 2 ) && (0==AV117Contrato) )
         {
            GX_msglist.addItem("Contrato do Requisitado � obrigat�rio!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( AV101RequisitadoPor && (0==AV102Requisitante) )
         {
            GX_msglist.addItem("Requisitante � obrigat�ria!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( (0==AV13Servico_Codigo) )
         {
            GX_msglist.addItem("Servi�o � obrigat�ria!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( AV139ContratoVencido )
         {
            GX_msglist.addItem("O Contrato "+cmbavContrato.Description+" do servi�o informado esta vencido!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( (DateTime.MinValue==AV62PrazoEntrega) )
         {
            GX_msglist.addItem("Data da Entrega � obrigat�ria!");
            AV17CheckRequiredFieldsResult = false;
         }
         else
         {
            if ( ( AV8ContagemResultado_DataDmn == DateTimeUtil.ServerDate( context, "DEFAULT") ) && ( AV62PrazoEntrega < DateTimeUtil.ServerNow( context, "DEFAULT") ) )
            {
               GX_msglist.addItem("Data da Entrega informada j� venceu (confira data e hor�rio)!");
               AV17CheckRequiredFieldsResult = false;
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84PrazoTipo)) && ( AV62PrazoEntrega < AV63EntregaCalculada ) )
            {
               GX_msglist.addItem("Prazo de Entrega informado � menor ao prazo calculado pelo Sistema "+context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " ")+"!");
               AV17CheckRequiredFieldsResult = false;
            }
            if ( AV62PrazoEntrega <= AV70PrazoAnalise )
            {
               GX_msglist.addItem("A Data da Entrega informada n�o respeita o prazo do an�lise configurado!");
               AV17CheckRequiredFieldsResult = false;
            }
         }
         if ( AV135PrazoMomento != 1 )
         {
            if ( ( StringUtil.StrCmp(AV84PrazoTipo, "A") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "V") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "P") == 0 ) )
            {
               if ( ( AV66PFBFSdaOSVnc + AV73PFLFSdaOSVnc == Convert.ToDecimal( 0 )) )
               {
                  GX_msglist.addItem("Valores de Refer�ncia s�o obrigat�rios (SLA)!");
                  AV17CheckRequiredFieldsResult = false;
               }
               else
               {
                  if ( (Convert.ToDecimal(0)==AV66PFBFSdaOSVnc) )
                  {
                     GX_msglist.addItem("Valor Bruto de Refer�ncia � obrigat�rio para o c�lculo do prazo (SLA)!");
                     AV17CheckRequiredFieldsResult = false;
                  }
                  else if ( (Convert.ToDecimal(0)==AV73PFLFSdaOSVnc) )
                  {
                     GX_msglist.addItem("Valor Liquido de Refer�ncia � obrigat�rio para o c�lculo do prazo (SLA)!");
                     AV17CheckRequiredFieldsResult = false;
                  }
               }
            }
         }
         if ( ! ( StringUtil.StrCmp(AV125Caller, "SS") == 0 ) && AV163RequerOrigem && ( dynavContratadaorigem.ItemCount > 2 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ContagemResultado_DmnVinculada)) && (0==AV105ContratadaOrigem) )
         {
            GX_msglist.addItem("A Origem do No. de OS Refer�ncia � obrigat�rio para o c�lculo do prazo!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV5ContagemResultado_Demanda)) || String.IsNullOrEmpty(StringUtil.RTrim( AV9Requisitado)) ) )
         {
            /* Using cursor H00CG9 */
            pr_default.execute(6, new Object[] {AV69ContagemResultado_DemandaFM, AV12RequisitadoCod});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A490ContagemResultado_ContratadaCod = H00CG9_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00CG9_n490ContagemResultado_ContratadaCod[0];
               A457ContagemResultado_Demanda = H00CG9_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00CG9_n457ContagemResultado_Demanda[0];
               GX_msglist.addItem("N� de OS "+StringUtil.Trim( AV69ContagemResultado_DemandaFM)+" j� cadastrada!");
               AV17CheckRequiredFieldsResult = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(6);
            }
            pr_default.close(6);
         }
         if ( ! AV108ServicoGrupo_FlagRequerSoft )
         {
            /* Using cursor H00CG10 */
            pr_default.execute(7, new Object[] {AV16ServicoGrupo_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A157ServicoGrupo_Codigo = H00CG10_A157ServicoGrupo_Codigo[0];
               A1428ServicoGrupo_FlagRequerSoft = H00CG10_A1428ServicoGrupo_FlagRequerSoft[0];
               n1428ServicoGrupo_FlagRequerSoft = H00CG10_n1428ServicoGrupo_FlagRequerSoft[0];
               AV108ServicoGrupo_FlagRequerSoft = A1428ServicoGrupo_FlagRequerSoft;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108ServicoGrupo_FlagRequerSoft", AV108ServicoGrupo_FlagRequerSoft);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(7);
         }
         if ( AV108ServicoGrupo_FlagRequerSoft && ( StringUtil.StrCmp(AV109Servico_Atende, "S") == 0 ) && (0==AV38Sistema_Codigo) )
         {
            GX_msglist.addItem("Sistema � Obrigat�rio para Servi�os de Software!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( ( StringUtil.StrCmp(AV110Servico_ObrigaValores, "B") == 0 ) && (Convert.ToDecimal(0)==AV66PFBFSdaOSVnc) )
         {
            GX_msglist.addItem("Este Servi�o no seu cadastro obriga valor Bruto!!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( ( StringUtil.StrCmp(AV110Servico_ObrigaValores, "L") == 0 ) && (Convert.ToDecimal(0)==AV73PFLFSdaOSVnc) )
         {
            GX_msglist.addItem("Este Servi�o no seu cadastro obriga valor Liquido!");
            AV17CheckRequiredFieldsResult = false;
         }
         if ( ( StringUtil.StrCmp(AV110Servico_ObrigaValores, "A") == 0 ) && ( AV66PFBFSdaOSVnc + AV73PFLFSdaOSVnc == Convert.ToDecimal( 0 )) )
         {
            GX_msglist.addItem("Este Servi�o no seu cadastro obriga valores Bruto e Liquido!");
            AV17CheckRequiredFieldsResult = false;
         }
         /* Execute user subroutine: 'CHECKSALDOCONTRATO' */
         S272 ();
         if (returnInSub) return;
         if ( AV17CheckRequiredFieldsResult )
         {
            if ( AV101RequisitadoPor )
            {
               AV19ContagemResultado_Observacao = StringUtil.Trim( AV19ContagemResultado_Observacao) + StringUtil.NewLine( ) + "(Cadastrado por " + StringUtil.Trim( AV6WWPContext.gxTpr_Username) + ")";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_Observacao", AV19ContagemResultado_Observacao);
               AV104UserId = AV102Requisitante;
            }
            else
            {
               AV104UserId = AV6WWPContext.gxTpr_Userid;
            }
            /* Execute user subroutine: 'SETREQUISITOS' */
            S282 ();
            if (returnInSub) return;
            AV29Websession.Set("Momento", "Solicitacao");
            GXt_dtime1 = DateTimeUtil.ResetTime( DateTime.MinValue ) ;
            new prc_novaos(context ).execute(  AV12RequisitadoCod,  AV10Usuario_Codigo,  AV8ContagemResultado_DataDmn, ref  AV22ContagemResultado_DmnVinculada, ref  AV69ContagemResultado_DemandaFM,  AV88ContagemResultado_Descricao,  AV19ContagemResultado_Observacao,  AV15ContagemResultado_Link,  AV23ContagemResultado_OSVinculada,  AV104UserId,  AV13Servico_Codigo,  AV26ContagemResultado_ValorPF,  AV38Sistema_Codigo,  AV39Modulo_Codigo,  AV40FuncaoDados_Codigo, ref  AV62PrazoEntrega,  AV53Contratante_OSAutomatica,  AV66PFBFSdaOSVnc,  AV73PFLFSdaOSVnc,  AV49ContratoServicos_Codigo,  AV70PrazoAnalise,  AV84PrazoTipo,  0,  0,  GXt_dtime1,  AV105ContratadaOrigem,  AV107ContadorFS,  AV96Prioridade_Codigo,  AV113PercPrazo,  AV114PercValorB,  AV97Complexidade,  1,  AV173ContagemResultado_QuantidadeSolicitada) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_DataDmn", context.localUtil.Format(AV8ContagemResultado_DataDmn, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_DemandaFM", AV69ContagemResultado_DemandaFM);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ContagemResultado_Descricao", AV88ContagemResultado_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_Observacao", AV19ContagemResultado_Observacao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Link", AV15ContagemResultado_Link);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( AV26ContagemResultado_ValorPF, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Contratante_OSAutomatica", AV53Contratante_OSAutomatica);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84PrazoTipo", AV84PrazoTipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ContadorFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Prioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113PercPrazo", StringUtil.LTrim( StringUtil.Str( AV113PercPrazo, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114PercValorB", StringUtil.LTrim( StringUtil.Str( AV114PercValorB, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV173ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV173ContagemResultado_QuantidadeSolicitada, 9, 4)));
            AV29Websession.Remove("Momento");
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22ContagemResultado_DmnVinculada)) )
            {
               Gx_msg = "Demanda " + StringUtil.Trim( AV69ContagemResultado_DemandaFM) + " solicitada com prazo " + context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " ");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
            }
            else
            {
               Gx_msg = "Demanda " + StringUtil.Trim( AV69ContagemResultado_DemandaFM) + " com refer�ncia � " + StringUtil.Trim( AV22ContagemResultado_DmnVinculada) + " solicitada com prazo " + context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " ");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
            }
            /* Execute user subroutine: 'ENVIONOTIFICACAO' */
            S292 ();
            if (returnInSub) return;
         }
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         if ( ! ( ( AV172ContratoServicos_QntUntCns > Convert.ToDecimal( 0 )) ) )
         {
            edtavContagemresultado_quantidadesolicitada_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_quantidadesolicitada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_quantidadesolicitada_Visible), 5, 0)));
            cellContagemresultado_quantidadesolicitada_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellContagemresultado_quantidadesolicitada_cell_Class);
            cellTextblockcontagemresultado_quantidadesolicitada_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellTextblockcontagemresultado_quantidadesolicitada_cell_Class);
         }
         else
         {
            edtavContagemresultado_quantidadesolicitada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_quantidadesolicitada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_quantidadesolicitada_Visible), 5, 0)));
            cellContagemresultado_quantidadesolicitada_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellContagemresultado_quantidadesolicitada_cell_Class);
            cellTextblockcontagemresultado_quantidadesolicitada_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname, "Class", cellTextblockcontagemresultado_quantidadesolicitada_cell_Class);
         }
         if ( ! ( ( AV166Servico_IsOrigemReferencia ) ) )
         {
            dynavContratadaorigem.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem.Visible), 5, 0)));
            cellContratadaorigem_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadaorigem_cell_Internalname, "Class", cellContratadaorigem_cell_Class);
            cellTextblockcontratadaorigem_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadaorigem_cell_Internalname, "Class", cellTextblockcontratadaorigem_cell_Class);
         }
         else
         {
            dynavContratadaorigem.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem.Visible), 5, 0)));
            cellContratadaorigem_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContratadaorigem_cell_Internalname, "Class", cellContratadaorigem_cell_Class);
            cellTextblockcontratadaorigem_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontratadaorigem_cell_Internalname, "Class", cellTextblockcontratadaorigem_cell_Class);
         }
         if ( ! ( ( AV166Servico_IsOrigemReferencia ) ) )
         {
            dynavContadorfs.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContadorfs.Visible), 5, 0)));
            cellContadorfs_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContadorfs_cell_Internalname, "Class", cellContadorfs_cell_Class);
            cellTextblockcontadorfs_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontadorfs_cell_Internalname, "Class", cellTextblockcontadorfs_cell_Class);
         }
         else
         {
            dynavContadorfs.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContadorfs.Visible), 5, 0)));
            cellContadorfs_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContadorfs_cell_Internalname, "Class", cellContadorfs_cell_Class);
            cellTextblockcontadorfs_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockcontadorfs_cell_Internalname, "Class", cellTextblockcontadorfs_cell_Class);
         }
         if ( ! ( ( AV168Servico_IsSolicitaGestorSistema ) ) )
         {
            edtavSistema_gestor_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_gestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Visible), 5, 0)));
            cellSistema_gestor_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSistema_gestor_cell_Internalname, "Class", cellSistema_gestor_cell_Class);
            cellTextblocksistema_gestor_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblocksistema_gestor_cell_Internalname, "Class", cellTextblocksistema_gestor_cell_Class);
         }
         else
         {
            edtavSistema_gestor_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_gestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Visible), 5, 0)));
            cellSistema_gestor_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSistema_gestor_cell_Internalname, "Class", cellSistema_gestor_cell_Class);
            cellTextblocksistema_gestor_cell_Class = "DataContentCell";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblocksistema_gestor_cell_Internalname, "Class", cellTextblocksistema_gestor_cell_Class);
         }
      }

      protected void E15CG2( )
      {
         /* Sistema_codigo_Click Routine */
         if ( AV168Servico_IsSolicitaGestorSistema )
         {
            GXt_char2 = AV167Sistema_Gestor;
            new prc_getsistemaresponsavelidentificacao(context ).execute(  AV38Sistema_Codigo,  AV6WWPContext.gxTpr_Areatrabalho_codigo, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
            AV167Sistema_Gestor = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV167Sistema_Gestor", AV167Sistema_Gestor);
         }
      }

      protected void E32CG2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV161SDT_Requisitos.FromXml(AV29Websession.Get("Requisitos"), "");
         gx_BV219 = true;
         AV19ContagemResultado_Observacao = AV29Websession.Get("Descricao");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_Observacao", AV19ContagemResultado_Observacao);
         AV29Websession.Remove("Requisitos");
         AV29Websession.Remove("Descricao");
         AV128sdt_Artefatos.FromXml(AV29Websession.Get("Artefatos"), "SDT_ArtefatosCollection");
         bttBtnartefatos_Caption = "Artefatos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV128sdt_Artefatos.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Caption", bttBtnartefatos_Caption);
         bttBtnconfirmar_Tooltiptext = "Solicitar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirmar_Internalname, "Tooltiptext", bttBtnconfirmar_Tooltiptext);
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABContainer", "HideTabById", "", new Object[] {(String)"TabRequisitos"});
         AV100AreaTrabalho_Codigo = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV100AreaTrabalho_Codigo), 6, 0)));
         /* Execute user subroutine: 'CONTRATADASDAAREA' */
         S172 ();
         if (returnInSub) return;
         if ( AV6WWPContext.gxTpr_Userehcontratante || AV134Contratante_UsaOSistema )
         {
            lblTextblockrequisitadopor_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockrequisitadopor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockrequisitadopor_Visible), 5, 0)));
            tblTablemergedrequisitadopor_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergedrequisitadopor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergedrequisitadopor_Visible), 5, 0)));
         }
         if ( (0==AV13Servico_Codigo) )
         {
            AV55Usuario_CargoUOCod = 0;
            /* Using cursor H00CG11 */
            pr_default.execute(8, new Object[] {AV6WWPContext.gxTpr_Userid});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A57Usuario_PessoaCod = H00CG11_A57Usuario_PessoaCod[0];
               A1073Usuario_CargoCod = H00CG11_A1073Usuario_CargoCod[0];
               n1073Usuario_CargoCod = H00CG11_n1073Usuario_CargoCod[0];
               A1Usuario_Codigo = H00CG11_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = H00CG11_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00CG11_n58Usuario_PessoaNom[0];
               A1075Usuario_CargoUOCod = H00CG11_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H00CG11_n1075Usuario_CargoUOCod[0];
               A1076Usuario_CargoUONom = H00CG11_A1076Usuario_CargoUONom[0];
               n1076Usuario_CargoUONom = H00CG11_n1076Usuario_CargoUONom[0];
               A58Usuario_PessoaNom = H00CG11_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H00CG11_n58Usuario_PessoaNom[0];
               A1075Usuario_CargoUOCod = H00CG11_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H00CG11_n1075Usuario_CargoUOCod[0];
               A1076Usuario_CargoUONom = H00CG11_A1076Usuario_CargoUONom[0];
               n1076Usuario_CargoUONom = H00CG11_n1076Usuario_CargoUONom[0];
               AV7Usuario_PessoaNom = A58Usuario_PessoaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuario_PessoaNom", AV7Usuario_PessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_PESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Usuario_PessoaNom, "@!"))));
               AV55Usuario_CargoUOCod = A1075Usuario_CargoUOCod;
               AV56Usuario_CargoUONom = A1076Usuario_CargoUONom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Usuario_CargoUONom", AV56Usuario_CargoUONom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIO_CARGOUONOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV56Usuario_CargoUONom, "@!"))));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(8);
            /* Execute user subroutine: 'CARREGAGRUPOSDESERVICOS' */
            S182 ();
            if (returnInSub) return;
            /* Execute user subroutine: 'CARREGASISTEMAS' */
            S192 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV161SDT_Requisitos", AV161SDT_Requisitos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV132ContratadasDaArea", AV132ContratadasDaArea);
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV136Servicos", AV136Servicos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV137SDT_Servicos", AV137SDT_Servicos);
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
      }

      protected void E33CG2( )
      {
         AV176GXV1 = (short)(nGXsfl_219_idx+GRIDREQUISITOS_nFirstRecordOnPage);
         if ( AV161SDT_Requisitos.Count >= AV176GXV1 )
         {
            AV161SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1));
         }
         /* Gridrequisitos_Refresh Routine */
         AV146Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV146Agrupador);
         if ( AV161SDT_Requisitos.Count > 0 )
         {
            this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABContainer", "ShowTab", "", new Object[] {(String)"TabRequisitos"});
            this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABContainer", "SetTabTitle", "", new Object[] {(String)"TabRequisitos","Requisitos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV161SDT_Requisitos.Count), 9, 0))+")"});
         }
         else
         {
            this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABContainer", "HideTabById", "", new Object[] {(String)"TabRascunho"});
         }
      }

      protected void E34CG2( )
      {
         AV176GXV1 = (short)(nGXsfl_219_idx+GRIDREQUISITOS_nFirstRecordOnPage);
         if ( AV161SDT_Requisitos.Count >= AV176GXV1 )
         {
            AV161SDT_Requisitos.CurrentItem = ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1));
         }
         /* Btnexcluirreqneg_Click Routine */
         AV161SDT_Requisitos.RemoveItem(AV161SDT_Requisitos.IndexOf(((SdtSDT_Requisitos_SDT_RequisitosItem)(AV161SDT_Requisitos.CurrentItem))));
         gx_BV219 = true;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV161SDT_Requisitos", AV161SDT_Requisitos);
         nGXsfl_219_bak_idx = nGXsfl_219_idx;
         gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         nGXsfl_219_idx = nGXsfl_219_bak_idx;
         sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
         SubsflControlProps_2192( ) ;
      }

      protected void E16CG2( )
      {
         /* Pfbfsdaosvnc_Isvalid Routine */
         GXt_char2 = "Event &PFBFSdaOSVnc.IsValid";
         new geralog(context ).execute( ref  GXt_char2) ;
         /* Execute user subroutine: 'CALCULARPRAZOS' */
         S202 ();
         if (returnInSub) return;
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
      }

      protected void E17CG2( )
      {
         /* Contagemresultado_dmnvinculada_Isvalid Routine */
         GXt_char2 = "entrou aqui Event &ContagemResultado_DmnVinculada.IsValid";
         new geralog(context ).execute( ref  GXt_char2) ;
         AV124OSPrompted = (bool)(((AV23ContagemResultado_OSVinculada>0)));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22ContagemResultado_DmnVinculada)) && String.IsNullOrEmpty(StringUtil.RTrim( AV133ContagemResultado_DmnVinculadaRef)) )
         {
            AV66PFBFSdaOSVnc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
            AV73PFLFSdaOSVnc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
            AV23ContagemResultado_OSVinculada = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
            AV123DmnVinculadas = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            AV38Sistema_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
            AV39Modulo_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
            AV41FuncaoUsuario_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
            AV105ContratadaOrigem = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
            if ( AV135PrazoMomento != 1 )
            {
               if ( ( StringUtil.StrCmp(AV84PrazoTipo, "A") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "V") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "P") == 0 ) )
               {
                  lblTextblockpfbfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
                  lblTextblockpflfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
                  edtavPfbfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
                  edtavPflfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
               }
            }
            else
            {
               lblTextblockpfbfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
               lblTextblockpflfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
               edtavPfbfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
               edtavPflfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
            }
         }
         else
         {
            lblTextblockpfbfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
            lblTextblockpflfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
            cmbavDmnvinculadas.removeAllItems();
            AV123DmnVinculadas = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            pr_default.dynParam(9, new Object[]{ new Object[]{
                                                 A490ContagemResultado_ContratadaCod ,
                                                 AV132ContratadasDaArea ,
                                                 A484ContagemResultado_StatusDmn ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 AV22ContagemResultado_DmnVinculada },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING
                                                 }
            });
            /* Using cursor H00CG13 */
            pr_default.execute(9, new Object[] {AV22ContagemResultado_DmnVinculada});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00CG13_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00CG13_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00CG13_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00CG13_n601ContagemResultado_Servico[0];
               A484ContagemResultado_StatusDmn = H00CG13_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00CG13_n484ContagemResultado_StatusDmn[0];
               A493ContagemResultado_DemandaFM = H00CG13_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00CG13_n493ContagemResultado_DemandaFM[0];
               A490ContagemResultado_ContratadaCod = H00CG13_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00CG13_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = H00CG13_A456ContagemResultado_Codigo[0];
               A801ContagemResultado_ServicoSigla = H00CG13_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00CG13_n801ContagemResultado_ServicoSigla[0];
               A489ContagemResultado_SistemaCod = H00CG13_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00CG13_n489ContagemResultado_SistemaCod[0];
               A146Modulo_Codigo = H00CG13_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00CG13_n146Modulo_Codigo[0];
               A1044ContagemResultado_FncUsrCod = H00CG13_A1044ContagemResultado_FncUsrCod[0];
               n1044ContagemResultado_FncUsrCod = H00CG13_n1044ContagemResultado_FncUsrCod[0];
               A1326ContagemResultado_ContratadaTipoFab = H00CG13_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00CG13_n1326ContagemResultado_ContratadaTipoFab[0];
               A798ContagemResultado_PFBFSImp = H00CG13_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = H00CG13_n798ContagemResultado_PFBFSImp[0];
               A799ContagemResultado_PFLFSImp = H00CG13_A799ContagemResultado_PFLFSImp[0];
               n799ContagemResultado_PFLFSImp = H00CG13_n799ContagemResultado_PFLFSImp[0];
               A804ContagemResultado_SistemaAtivo = H00CG13_A804ContagemResultado_SistemaAtivo[0];
               n804ContagemResultado_SistemaAtivo = H00CG13_n804ContagemResultado_SistemaAtivo[0];
               A803ContagemResultado_ContratadaSigla = H00CG13_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00CG13_n803ContagemResultado_ContratadaSigla[0];
               A682ContagemResultado_PFBFMUltima = H00CG13_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00CG13_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00CG13_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00CG13_A685ContagemResultado_PFLFSUltima[0];
               A601ContagemResultado_Servico = H00CG13_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00CG13_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = H00CG13_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00CG13_n801ContagemResultado_ServicoSigla[0];
               A1326ContagemResultado_ContratadaTipoFab = H00CG13_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00CG13_n1326ContagemResultado_ContratadaTipoFab[0];
               A803ContagemResultado_ContratadaSigla = H00CG13_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00CG13_n803ContagemResultado_ContratadaSigla[0];
               A682ContagemResultado_PFBFMUltima = H00CG13_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00CG13_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00CG13_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00CG13_A685ContagemResultado_PFLFSUltima[0];
               A804ContagemResultado_SistemaAtivo = H00CG13_A804ContagemResultado_SistemaAtivo[0];
               n804ContagemResultado_SistemaAtivo = H00CG13_n804ContagemResultado_SistemaAtivo[0];
               if ( (0==AV23ContagemResultado_OSVinculada) || ( A456ContagemResultado_Codigo == AV23ContagemResultado_OSVinculada ) )
               {
                  AV123DmnVinculadas = A456ContagemResultado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
                  AV60ServicoVnc_Sigla = A801ContagemResultado_ServicoSigla;
                  AV38Sistema_Codigo = A489ContagemResultado_SistemaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
                  AV39Modulo_Codigo = A146Modulo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
                  AV105ContratadaOrigem = A490ContagemResultado_ContratadaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
                  AV41FuncaoUsuario_Codigo = A1044ContagemResultado_FncUsrCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
                  AV23ContagemResultado_OSVinculada = A456ContagemResultado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
                  AV22ContagemResultado_DmnVinculada = A493ContagemResultado_DemandaFM;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
               }
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
               {
                  AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
               }
               else
               {
                  if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
                  {
                     AV66PFBFSdaOSVnc = A684ContagemResultado_PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A685ContagemResultado_PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
                  else if ( ( A798ContagemResultado_PFBFSImp > Convert.ToDecimal( 0 )) )
                  {
                     AV66PFBFSdaOSVnc = A798ContagemResultado_PFBFSImp;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A799ContagemResultado_PFLFSImp;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
                  else
                  {
                     AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
               }
               AV130SistemaAtivo = A804ContagemResultado_SistemaAtivo;
               cmbavDmnvinculadas.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)), A493ContagemResultado_DemandaFM+" de "+StringUtil.Trim( A801ContagemResultado_ServicoSigla)+" da "+A803ContagemResultado_ContratadaSigla, 0);
               pr_default.readNext(9);
            }
            pr_default.close(9);
            if ( cmbavDmnvinculadas.ItemCount > 1 )
            {
               cmbavDmnvinculadas.addItem("0", "Selecionar uma OS", 1);
               if ( ! AV124OSPrompted )
               {
                  AV66PFBFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  AV105ContratadaOrigem = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
                  AV38Sistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
                  AV39Modulo_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
                  AV41FuncaoUsuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
               }
            }
            else
            {
               if ( (0==AV123DmnVinculadas) )
               {
                  AV123DmnVinculadas = -1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
                  AV66PFBFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  AV105ContratadaOrigem = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
                  AV38Sistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
                  AV39Modulo_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
                  AV41FuncaoUsuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
                  if ( (Convert.ToDecimal(0)==AV66PFBFSdaOSVnc) )
                  {
                     GX_FocusControl = edtavPfbfsdaosvnc_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     context.DoAjaxSetFocus(GX_FocusControl);
                  }
               }
            }
            cmbavDmnvinculadas.addItem("-1", StringUtil.Trim( AV22ContagemResultado_DmnVinculada)+" OS EXTERNA", 0);
            if ( cmbavDmnvinculadas.ItemCount > 3 )
            {
               AV123DmnVinculadas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
               AV23ContagemResultado_OSVinculada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
               AV130SistemaAtivo = true;
            }
            cmbavDmnvinculadas.addItem("-2", "Alterar OS de Refer�ncia", 2);
            if ( AV124OSPrompted )
            {
               AV123DmnVinculadas = AV23ContagemResultado_OSVinculada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            }
            edtavPfbfsdaosvnc_Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Enabled), 5, 0)));
            edtavPflfsdaosvnc_Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Enabled), 5, 0)));
            dynavContratadaorigem.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem.Enabled), 5, 0)));
            cmbavSistema_codigo.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Enabled), 5, 0)));
            dynavModulo_codigo.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Enabled), 5, 0)));
            dynavFuncaousuario_codigo.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaousuario_codigo.Enabled), 5, 0)));
            edtavContagemresultado_dmnvinculada_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculada_Visible), 5, 0)));
            edtavContagemresultado_dmnvinculadaref_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculadaref_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculadaref_Visible), 5, 0)));
            cmbavDmnvinculadas.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDmnvinculadas.Visible), 5, 0)));
            edtavPfbfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
            edtavPflfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
            GXt_char2 = "2 - Event &ContagemResultado_DmnVinculada.IsValid";
            new geralog(context ).execute( ref  GXt_char2) ;
            if ( ( AV66PFBFSdaOSVnc > Convert.ToDecimal( 0 )) )
            {
               /* Execute user subroutine: 'CALCULARPRAZOS' */
               S202 ();
               if (returnInSub) return;
            }
         }
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", dynavModulo_codigo.ToJavascriptSource());
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
         dynavContratadaorigem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Values", dynavContratadaorigem.ToJavascriptSource());
         cmbavDmnvinculadas.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Values", cmbavDmnvinculadas.ToJavascriptSource());
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
      }

      protected void E18CG2( )
      {
         /* Contagemresultado_dmnvinculadaref_Isvalid Routine */
         GXt_char2 = "entrou aqui Event &ContagemResultado_DmnVinculadaRef.IsValid";
         new geralog(context ).execute( ref  GXt_char2) ;
         AV124OSPrompted = (bool)(((AV23ContagemResultado_OSVinculada>0)));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22ContagemResultado_DmnVinculada)) && String.IsNullOrEmpty(StringUtil.RTrim( AV133ContagemResultado_DmnVinculadaRef)) )
         {
            AV66PFBFSdaOSVnc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
            AV73PFLFSdaOSVnc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
            AV23ContagemResultado_OSVinculada = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
            AV123DmnVinculadas = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            AV38Sistema_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
            AV39Modulo_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
            AV41FuncaoUsuario_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
            AV105ContratadaOrigem = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
            if ( AV135PrazoMomento != 1 )
            {
               if ( ( StringUtil.StrCmp(AV84PrazoTipo, "A") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "V") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "P") == 0 ) )
               {
                  lblTextblockpfbfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
                  lblTextblockpflfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
                  edtavPfbfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
                  edtavPflfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
               }
            }
            else
            {
               lblTextblockpfbfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
               lblTextblockpflfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
               edtavPfbfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
               edtavPflfsdaosvnc_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
            }
         }
         else
         {
            lblTextblockpfbfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
            lblTextblockpflfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
            cmbavDmnvinculadas.removeAllItems();
            AV123DmnVinculadas = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            pr_default.dynParam(10, new Object[]{ new Object[]{
                                                 A490ContagemResultado_ContratadaCod ,
                                                 AV132ContratadasDaArea ,
                                                 A484ContagemResultado_StatusDmn ,
                                                 A457ContagemResultado_Demanda ,
                                                 AV133ContagemResultado_DmnVinculadaRef },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING
                                                 }
            });
            /* Using cursor H00CG15 */
            pr_default.execute(10, new Object[] {AV133ContagemResultado_DmnVinculadaRef});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00CG15_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00CG15_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00CG15_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00CG15_n601ContagemResultado_Servico[0];
               A484ContagemResultado_StatusDmn = H00CG15_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00CG15_n484ContagemResultado_StatusDmn[0];
               A457ContagemResultado_Demanda = H00CG15_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00CG15_n457ContagemResultado_Demanda[0];
               A490ContagemResultado_ContratadaCod = H00CG15_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00CG15_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = H00CG15_A456ContagemResultado_Codigo[0];
               A801ContagemResultado_ServicoSigla = H00CG15_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00CG15_n801ContagemResultado_ServicoSigla[0];
               A489ContagemResultado_SistemaCod = H00CG15_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00CG15_n489ContagemResultado_SistemaCod[0];
               A146Modulo_Codigo = H00CG15_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00CG15_n146Modulo_Codigo[0];
               A1044ContagemResultado_FncUsrCod = H00CG15_A1044ContagemResultado_FncUsrCod[0];
               n1044ContagemResultado_FncUsrCod = H00CG15_n1044ContagemResultado_FncUsrCod[0];
               A1326ContagemResultado_ContratadaTipoFab = H00CG15_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00CG15_n1326ContagemResultado_ContratadaTipoFab[0];
               A798ContagemResultado_PFBFSImp = H00CG15_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = H00CG15_n798ContagemResultado_PFBFSImp[0];
               A799ContagemResultado_PFLFSImp = H00CG15_A799ContagemResultado_PFLFSImp[0];
               n799ContagemResultado_PFLFSImp = H00CG15_n799ContagemResultado_PFLFSImp[0];
               A804ContagemResultado_SistemaAtivo = H00CG15_A804ContagemResultado_SistemaAtivo[0];
               n804ContagemResultado_SistemaAtivo = H00CG15_n804ContagemResultado_SistemaAtivo[0];
               A803ContagemResultado_ContratadaSigla = H00CG15_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00CG15_n803ContagemResultado_ContratadaSigla[0];
               A493ContagemResultado_DemandaFM = H00CG15_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00CG15_n493ContagemResultado_DemandaFM[0];
               A682ContagemResultado_PFBFMUltima = H00CG15_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00CG15_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00CG15_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00CG15_A685ContagemResultado_PFLFSUltima[0];
               A601ContagemResultado_Servico = H00CG15_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00CG15_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = H00CG15_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00CG15_n801ContagemResultado_ServicoSigla[0];
               A1326ContagemResultado_ContratadaTipoFab = H00CG15_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00CG15_n1326ContagemResultado_ContratadaTipoFab[0];
               A803ContagemResultado_ContratadaSigla = H00CG15_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00CG15_n803ContagemResultado_ContratadaSigla[0];
               A682ContagemResultado_PFBFMUltima = H00CG15_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00CG15_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00CG15_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00CG15_A685ContagemResultado_PFLFSUltima[0];
               A804ContagemResultado_SistemaAtivo = H00CG15_A804ContagemResultado_SistemaAtivo[0];
               n804ContagemResultado_SistemaAtivo = H00CG15_n804ContagemResultado_SistemaAtivo[0];
               if ( (0==AV23ContagemResultado_OSVinculada) || ( A456ContagemResultado_Codigo == AV23ContagemResultado_OSVinculada ) )
               {
                  AV123DmnVinculadas = A456ContagemResultado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
                  AV60ServicoVnc_Sigla = A801ContagemResultado_ServicoSigla;
                  AV38Sistema_Codigo = A489ContagemResultado_SistemaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
                  AV39Modulo_Codigo = A146Modulo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
                  AV105ContratadaOrigem = A490ContagemResultado_ContratadaCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
                  AV41FuncaoUsuario_Codigo = A1044ContagemResultado_FncUsrCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
                  AV23ContagemResultado_OSVinculada = A456ContagemResultado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
                  AV22ContagemResultado_DmnVinculada = A457ContagemResultado_Demanda;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
               }
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
               {
                  AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
               }
               else
               {
                  if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
                  {
                     AV66PFBFSdaOSVnc = A684ContagemResultado_PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A685ContagemResultado_PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
                  else if ( ( A798ContagemResultado_PFBFSImp > Convert.ToDecimal( 0 )) )
                  {
                     AV66PFBFSdaOSVnc = A798ContagemResultado_PFBFSImp;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A799ContagemResultado_PFLFSImp;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
                  else
                  {
                     AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
               }
               AV130SistemaAtivo = A804ContagemResultado_SistemaAtivo;
               cmbavDmnvinculadas.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)), A493ContagemResultado_DemandaFM+" de "+StringUtil.Trim( A801ContagemResultado_ServicoSigla)+" da "+A803ContagemResultado_ContratadaSigla, 0);
               pr_default.readNext(10);
            }
            pr_default.close(10);
            if ( cmbavDmnvinculadas.ItemCount > 1 )
            {
               cmbavDmnvinculadas.addItem("0", "Selecionar uma OS", 1);
               if ( ! AV124OSPrompted )
               {
                  AV66PFBFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  AV105ContratadaOrigem = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
                  AV38Sistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
                  AV39Modulo_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
                  AV41FuncaoUsuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
               }
            }
            else
            {
               if ( (0==AV123DmnVinculadas) )
               {
                  AV123DmnVinculadas = -1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
                  AV66PFBFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  AV105ContratadaOrigem = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
                  AV38Sistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
                  AV39Modulo_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
                  AV41FuncaoUsuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
                  AV22ContagemResultado_DmnVinculada = AV133ContagemResultado_DmnVinculadaRef;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
                  if ( (Convert.ToDecimal(0)==AV66PFBFSdaOSVnc) )
                  {
                     GX_FocusControl = edtavPfbfsdaosvnc_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     context.DoAjaxSetFocus(GX_FocusControl);
                  }
               }
            }
            cmbavDmnvinculadas.addItem("-1", StringUtil.Trim( AV133ContagemResultado_DmnVinculadaRef)+" OS EXTERNA", 0);
            if ( cmbavDmnvinculadas.ItemCount > 3 )
            {
               AV123DmnVinculadas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
               AV23ContagemResultado_OSVinculada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
               AV130SistemaAtivo = true;
            }
            cmbavDmnvinculadas.addItem("-2", "Alterar OS de Refer�ncia", 2);
            if ( AV124OSPrompted )
            {
               AV123DmnVinculadas = AV23ContagemResultado_OSVinculada;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            }
            edtavPfbfsdaosvnc_Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Enabled), 5, 0)));
            edtavPflfsdaosvnc_Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Enabled), 5, 0)));
            dynavContratadaorigem.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem.Enabled), 5, 0)));
            cmbavSistema_codigo.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Enabled), 5, 0)));
            dynavModulo_codigo.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Enabled), 5, 0)));
            dynavFuncaousuario_codigo.Enabled = (((AV123DmnVinculadas==-1))||!AV130SistemaAtivo ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaousuario_codigo.Enabled), 5, 0)));
            edtavContagemresultado_dmnvinculada_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculada_Visible), 5, 0)));
            edtavContagemresultado_dmnvinculadaref_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculadaref_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculadaref_Visible), 5, 0)));
            cmbavDmnvinculadas.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDmnvinculadas.Visible), 5, 0)));
            edtavPfbfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
            edtavPflfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
            if ( ( AV66PFBFSdaOSVnc > Convert.ToDecimal( 0 )) )
            {
               /* Execute user subroutine: 'CALCULARPRAZOS' */
               S202 ();
               if (returnInSub) return;
            }
         }
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", dynavModulo_codigo.ToJavascriptSource());
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
         dynavContratadaorigem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Values", dynavContratadaorigem.ToJavascriptSource());
         cmbavDmnvinculadas.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Values", cmbavDmnvinculadas.ToJavascriptSource());
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
      }

      protected void E19CG2( )
      {
         /* Dmnvinculadas_Click Routine */
         AV66PFBFSdaOSVnc = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
         AV73PFLFSdaOSVnc = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
         AV38Sistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
         AV39Modulo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
         AV41FuncaoUsuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
         AV105ContratadaOrigem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
         AV23ContagemResultado_OSVinculada = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
         dynavContratadaorigem.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem.Enabled), 5, 0)));
         cmbavSistema_codigo.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Enabled), 5, 0)));
         dynavModulo_codigo.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Enabled), 5, 0)));
         dynavFuncaousuario_codigo.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaousuario_codigo.Enabled), 5, 0)));
         if ( AV123DmnVinculadas > 0 )
         {
            AV23ContagemResultado_OSVinculada = AV123DmnVinculadas;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
            /* Using cursor H00CG17 */
            pr_default.execute(11, new Object[] {AV123DmnVinculadas});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00CG17_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00CG17_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00CG17_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00CG17_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = H00CG17_A456ContagemResultado_Codigo[0];
               A493ContagemResultado_DemandaFM = H00CG17_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00CG17_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00CG17_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00CG17_n457ContagemResultado_Demanda[0];
               A801ContagemResultado_ServicoSigla = H00CG17_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00CG17_n801ContagemResultado_ServicoSigla[0];
               A489ContagemResultado_SistemaCod = H00CG17_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00CG17_n489ContagemResultado_SistemaCod[0];
               A146Modulo_Codigo = H00CG17_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00CG17_n146Modulo_Codigo[0];
               A1044ContagemResultado_FncUsrCod = H00CG17_A1044ContagemResultado_FncUsrCod[0];
               n1044ContagemResultado_FncUsrCod = H00CG17_n1044ContagemResultado_FncUsrCod[0];
               A490ContagemResultado_ContratadaCod = H00CG17_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00CG17_n490ContagemResultado_ContratadaCod[0];
               A804ContagemResultado_SistemaAtivo = H00CG17_A804ContagemResultado_SistemaAtivo[0];
               n804ContagemResultado_SistemaAtivo = H00CG17_n804ContagemResultado_SistemaAtivo[0];
               A1326ContagemResultado_ContratadaTipoFab = H00CG17_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00CG17_n1326ContagemResultado_ContratadaTipoFab[0];
               A798ContagemResultado_PFBFSImp = H00CG17_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = H00CG17_n798ContagemResultado_PFBFSImp[0];
               A799ContagemResultado_PFLFSImp = H00CG17_A799ContagemResultado_PFLFSImp[0];
               n799ContagemResultado_PFLFSImp = H00CG17_n799ContagemResultado_PFLFSImp[0];
               A682ContagemResultado_PFBFMUltima = H00CG17_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00CG17_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00CG17_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00CG17_A685ContagemResultado_PFLFSUltima[0];
               A601ContagemResultado_Servico = H00CG17_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00CG17_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = H00CG17_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00CG17_n801ContagemResultado_ServicoSigla[0];
               A682ContagemResultado_PFBFMUltima = H00CG17_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00CG17_A683ContagemResultado_PFLFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00CG17_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00CG17_A685ContagemResultado_PFLFSUltima[0];
               A804ContagemResultado_SistemaAtivo = H00CG17_A804ContagemResultado_SistemaAtivo[0];
               n804ContagemResultado_SistemaAtivo = H00CG17_n804ContagemResultado_SistemaAtivo[0];
               A1326ContagemResultado_ContratadaTipoFab = H00CG17_A1326ContagemResultado_ContratadaTipoFab[0];
               n1326ContagemResultado_ContratadaTipoFab = H00CG17_n1326ContagemResultado_ContratadaTipoFab[0];
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV133ContagemResultado_DmnVinculadaRef)) )
               {
                  AV22ContagemResultado_DmnVinculada = A493ContagemResultado_DemandaFM;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
               }
               else
               {
                  AV22ContagemResultado_DmnVinculada = A457ContagemResultado_Demanda;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
               }
               AV60ServicoVnc_Sigla = A801ContagemResultado_ServicoSigla;
               AV38Sistema_Codigo = A489ContagemResultado_SistemaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
               AV39Modulo_Codigo = A146Modulo_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
               AV41FuncaoUsuario_Codigo = A1044ContagemResultado_FncUsrCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
               AV105ContratadaOrigem = A490ContagemResultado_ContratadaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
               cmbavSistema_codigo.Enabled = (!A804ContagemResultado_SistemaAtivo ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Enabled), 5, 0)));
               dynavModulo_codigo.Enabled = (!A804ContagemResultado_SistemaAtivo ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Enabled), 5, 0)));
               dynavFuncaousuario_codigo.Enabled = (!A804ContagemResultado_SistemaAtivo ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaousuario_codigo.Enabled), 5, 0)));
               AV130SistemaAtivo = A804ContagemResultado_SistemaAtivo;
               if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
               {
                  AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
               }
               else
               {
                  if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
                  {
                     AV66PFBFSdaOSVnc = A684ContagemResultado_PFBFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A685ContagemResultado_PFLFSUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
                  else if ( ( A798ContagemResultado_PFBFSImp > Convert.ToDecimal( 0 )) )
                  {
                     AV66PFBFSdaOSVnc = A798ContagemResultado_PFBFSImp;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A799ContagemResultado_PFLFSImp;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
                  else
                  {
                     AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                     AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  }
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(11);
            edtavPfbfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
            edtavPflfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
            edtavPfbfsdaosvnc_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Enabled), 5, 0)));
            edtavPflfsdaosvnc_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Enabled), 5, 0)));
            dynavContratadaorigem.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem.Enabled), 5, 0)));
         }
         else if ( AV123DmnVinculadas == -1 )
         {
            edtavPfbfsdaosvnc_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Enabled), 5, 0)));
            edtavPflfsdaosvnc_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Enabled), 5, 0)));
            lblTextblockpfbfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
            lblTextblockpflfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
            edtavPfbfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
            edtavPflfsdaosvnc_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
         }
         else if ( AV123DmnVinculadas == -2 )
         {
            AV22ContagemResultado_DmnVinculada = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
            AV133ContagemResultado_DmnVinculadaRef = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV133ContagemResultado_DmnVinculadaRef", AV133ContagemResultado_DmnVinculadaRef);
            cmbavDmnvinculadas.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDmnvinculadas.Visible), 5, 0)));
            edtavContagemresultado_dmnvinculada_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculada_Visible), 5, 0)));
            edtavContagemresultado_dmnvinculadaref_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculadaref_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculadaref_Visible), 5, 0)));
            GX_FocusControl = edtavContagemresultado_dmnvinculada_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
            lblTextblockpfbfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
            lblTextblockpflfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
            edtavPfbfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
            edtavPflfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
         }
         else
         {
            lblTextblockpfbfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
            lblTextblockpflfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
            edtavPfbfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
            edtavPflfsdaosvnc_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
         }
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", dynavModulo_codigo.ToJavascriptSource());
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
         dynavContratadaorigem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Values", dynavContratadaorigem.ToJavascriptSource());
      }

      protected void E20CG2( )
      {
         /* Requisitado_Click Routine */
         AV12RequisitadoCod = (int)(NumberUtil.Val( StringUtil.Substring( AV9Requisitado, 2, 6), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
         if ( (0==AV12RequisitadoCod) )
         {
            AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
            AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV63EntregaCalculada = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         }
         else
         {
            /* Execute user subroutine: 'REQUISITADOSELECIONADO' */
            S212 ();
            if (returnInSub) return;
            if ( AV13Servico_Codigo > 0 )
            {
               AV85LerServico = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85LerServico", AV85LerServico);
               /* Execute user subroutine: 'DADOSDOSERVICO' */
               S222 ();
               if (returnInSub) return;
            }
         }
         cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", cmbavContrato.ToJavascriptSource());
         cmbavServico_atende.CurrentValue = StringUtil.RTrim( AV109Servico_Atende);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_atende_Internalname, "Values", cmbavServico_atende.ToJavascriptSource());
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         cmbavServico_obrigavalores.CurrentValue = StringUtil.RTrim( AV110Servico_ObrigaValores);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_obrigavalores_Internalname, "Values", cmbavServico_obrigavalores.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Sistemas", AV51Sistemas);
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", dynavModulo_codigo.ToJavascriptSource());
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
         cmbavComplexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavComplexidade_Internalname, "Values", cmbavComplexidade.ToJavascriptSource());
         cmbavPrioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Values", cmbavPrioridade_codigo.ToJavascriptSource());
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
         cmbavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Values", cmbavUsuario_codigo.ToJavascriptSource());
      }

      protected void E21CG2( )
      {
         /* Servico_codigo_Click Routine */
         GXt_char2 = "01 - Event &Servico_Codigo.Click";
         new geralog(context ).execute( ref  GXt_char2) ;
         AV116Servico_Nome = "";
         AV166Servico_IsOrigemReferencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166Servico_IsOrigemReferencia", AV166Servico_IsOrigemReferencia);
         AV105ContratadaOrigem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
         AV107ContadorFS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ContadorFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)));
         AV168Servico_IsSolicitaGestorSistema = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168Servico_IsSolicitaGestorSistema", AV168Servico_IsSolicitaGestorSistema);
         AV18ContratoServicos_LocalExec = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicos_LocalExec", AV18ContratoServicos_LocalExec);
         if ( ! (DateTime.MinValue==AV63EntregaCalculada) )
         {
            AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
            AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV63EntregaCalculada = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( AV12RequisitadoCod > 0 )
         {
            AV85LerServico = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85LerServico", AV85LerServico);
            /* Execute user subroutine: 'REQUISITADOSELECIONADO' */
            S212 ();
            if (returnInSub) return;
         }
         else
         {
            /* Execute user subroutine: 'CARREGAREQUISITADOS' */
            S232 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'DADOSDOSERVICO' */
         S222 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'NAOTEMARTEFATOS' */
         S242 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if (returnInSub) return;
         dynavContratadaorigem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Values", dynavContratadaorigem.ToJavascriptSource());
         dynavContadorfs.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfs_Internalname, "Values", dynavContadorfs.ToJavascriptSource());
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
         cmbavRequisitado.CurrentValue = StringUtil.RTrim( AV9Requisitado);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisitado_Internalname, "Values", cmbavRequisitado.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
         cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", cmbavContrato.ToJavascriptSource());
         cmbavServico_atende.CurrentValue = StringUtil.RTrim( AV109Servico_Atende);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_atende_Internalname, "Values", cmbavServico_atende.ToJavascriptSource());
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         cmbavServico_obrigavalores.CurrentValue = StringUtil.RTrim( AV110Servico_ObrigaValores);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_obrigavalores_Internalname, "Values", cmbavServico_obrigavalores.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Sistemas", AV51Sistemas);
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", dynavModulo_codigo.ToJavascriptSource());
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
         cmbavComplexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavComplexidade_Internalname, "Values", cmbavComplexidade.ToJavascriptSource());
         cmbavPrioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Values", cmbavPrioridade_codigo.ToJavascriptSource());
         cmbavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Values", cmbavUsuario_codigo.ToJavascriptSource());
      }

      protected void E22CG2( )
      {
         /* Servicogrupo_codigo_Click Routine */
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S142 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV136Servicos", AV136Servicos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV137SDT_Servicos", AV137SDT_Servicos);
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
      }

      protected void E23CG2( )
      {
         /* Prazoentrega_Isvalid Routine */
         if ( ! (DateTime.MinValue==AV62PrazoEntrega) )
         {
            if ( (0==DateTimeUtil.Hour( AV62PrazoEntrega)) )
            {
               /* Execute user subroutine: 'ADDHORASENTREGA' */
               S252 ();
               if (returnInSub) return;
            }
            Dvelop_bootstrap_confirmpanel1_Title = "Aten��o";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_bootstrap_confirmpanel1_Internalname, "Title", Dvelop_bootstrap_confirmpanel1_Title);
            if ( StringUtil.StrCmp(AV115TipoDias, "C") == 0 )
            {
            }
            else if ( ( DateTimeUtil.Dow( AV62PrazoEntrega) == 1 ) || ( DateTimeUtil.Dow( AV62PrazoEntrega) == 7 ) )
            {
               Dvelop_bootstrap_confirmpanel1_Confirmationtext = "Prazo de entrega "+context.localUtil.DToC( DateTimeUtil.ResetTime( AV62PrazoEntrega), 2, "/")+" � "+DateTimeUtil.CDow( AV62PrazoEntrega, "por")+"!";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_bootstrap_confirmpanel1_Internalname, "ConfirmationText", Dvelop_bootstrap_confirmpanel1_Confirmationtext);
               AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
               this.executeUsercontrolMethod("", false, "DVELOP_BOOTSTRAP_CONFIRMPANEL1Container", "Confirm", "", new Object[] {});
            }
            else if ( new prc_ehferiado(context).executeUdp( ref  AV62PrazoEntrega) )
            {
               Dvelop_bootstrap_confirmpanel1_Confirmationtext = "Prazo de entrega "+context.localUtil.DToC( DateTimeUtil.ResetTime( AV62PrazoEntrega), 2, "/")+" � feriado!";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_bootstrap_confirmpanel1_Internalname, "ConfirmationText", Dvelop_bootstrap_confirmpanel1_Confirmationtext);
               AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
               this.executeUsercontrolMethod("", false, "DVELOP_BOOTSTRAP_CONFIRMPANEL1Container", "Confirm", "", new Object[] {});
            }
         }
      }

      protected void E11CG2( )
      {
         /* Dvelop_bootstrap_confirmpanel1_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_bootstrap_confirmpanel1_Title, "Confirma��o") == 0 )
         {
            if ( StringUtil.StrCmp(AV125Caller, "CxEnt") == 0 )
            {
               lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> self.close(); </script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            }
            else if ( StringUtil.StrCmp(AV125Caller, "SS") == 0 )
            {
               context.setWebReturnParms(new Object[] {});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
            else
            {
               /* Execute user subroutine: 'LIMPARDADOS' */
               S262 ();
               if (returnInSub) return;
            }
         }
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         cmbavRequisitado.CurrentValue = StringUtil.RTrim( AV9Requisitado);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisitado_Internalname, "Values", cmbavRequisitado.ToJavascriptSource());
         cmbavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Values", cmbavUsuario_codigo.ToJavascriptSource());
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", dynavModulo_codigo.ToJavascriptSource());
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
         cmbavComplexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavComplexidade_Internalname, "Values", cmbavComplexidade.ToJavascriptSource());
         cmbavPrioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Values", cmbavPrioridade_codigo.ToJavascriptSource());
         dynavRequisitante.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavRequisitante_Internalname, "Values", dynavRequisitante.ToJavascriptSource());
         dynavContratadaorigem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Values", dynavContratadaorigem.ToJavascriptSource());
         dynavContadorfs.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfs_Internalname, "Values", dynavContadorfs.ToJavascriptSource());
         cmbavDmnvinculadas.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Values", cmbavDmnvinculadas.ToJavascriptSource());
         cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", cmbavContrato.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV161SDT_Requisitos", AV161SDT_Requisitos);
         nGXsfl_219_bak_idx = nGXsfl_219_idx;
         gxgrGridrequisitos_refresh( subGridrequisitos_Rows, AV6WWPContext, AV134Contratante_UsaOSistema, AV13Servico_Codigo, A1Usuario_Codigo, A58Usuario_PessoaNom, A1075Usuario_CargoUOCod, A1076Usuario_CargoUONom, A52Contratada_AreaTrabalhoCod, AV100AreaTrabalho_Codigo, A39Contratada_Codigo, AV132ContratadasDaArea, A158ServicoGrupo_Descricao, A75Contrato_AreaTrabalhoCod, A92Contrato_Ativo, A638ContratoServicos_Ativo, A157ServicoGrupo_Codigo, A129Sistema_Sigla, A135Sistema_AreaTrabalhoCod, A699Sistema_Tipo, A130Sistema_Ativo, A127Sistema_Codigo, AV51Sistemas, AV172ContratoServicos_QntUntCns, AV166Servico_IsOrigemReferencia, AV168Servico_IsSolicitaGestorSistema, A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod, AV102Requisitante, A843Contrato_DataFimTA, A83Contrato_DataVigenciaTermino, A43Contratada_Ativo, A74Contrato_Codigo, A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo, A155Servico_Codigo, A1825ContratoAuxiliar_UsuarioCod, A1824ContratoAuxiliar_ContratoCod, AV170ServicoCodigo, AV136Servicos, A605Servico_Sigla, A608Servico_Nome, AV137SDT_Servicos, AV161SDT_Requisitos, AV146Agrupador) ;
         nGXsfl_219_idx = nGXsfl_219_bak_idx;
         sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
         SubsflControlProps_2192( ) ;
      }

      protected void E12CG2( )
      {
         /* Confirmpanel2_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel2_Title, "Prestadora") == 0 )
         {
            AV9Requisitado = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
            AV12RequisitadoCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
            GX_FocusControl = cmbavRequisitado_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         cmbavRequisitado.CurrentValue = StringUtil.RTrim( AV9Requisitado);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisitado_Internalname, "Values", cmbavRequisitado.ToJavascriptSource());
      }

      protected void E24CG2( )
      {
         /* Complexidade_Click Routine */
         GXt_char2 = "entrou aqui Event &Complexidade.Click ";
         new geralog(context ).execute( ref  GXt_char2) ;
         AV85LerServico = (bool)(((AV13Servico_Codigo>0)&&(AV12RequisitadoCod>0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85LerServico", AV85LerServico);
         /* Execute user subroutine: 'CALCULARPRAZOS' */
         S202 ();
         if (returnInSub) return;
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
      }

      protected void E25CG2( )
      {
         /* Prioridade_codigo_Click Routine */
         GXt_char2 = "entrou aqui Event &Prioridade_Codigo.Click";
         new geralog(context ).execute( ref  GXt_char2) ;
         if ( (0==AV96Prioridade_Codigo) )
         {
            AV113PercPrazo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113PercPrazo", StringUtil.LTrim( StringUtil.Str( AV113PercPrazo, 6, 2)));
            AV114PercValorB = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114PercValorB", StringUtil.LTrim( StringUtil.Str( AV114PercValorB, 6, 2)));
         }
         else
         {
            new prc_getdadosprioridade(context ).execute( ref  AV96Prioridade_Codigo, ref  AV113PercPrazo, ref  AV114PercValorB) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Prioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV113PercPrazo", StringUtil.LTrim( StringUtil.Str( AV113PercPrazo, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV114PercValorB", StringUtil.LTrim( StringUtil.Str( AV114PercValorB, 6, 2)));
            Dvelop_bootstrap_confirmpanel1_Title = "Aten��o";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_bootstrap_confirmpanel1_Internalname, "Title", Dvelop_bootstrap_confirmpanel1_Title);
            Dvelop_bootstrap_confirmpanel1_Confirmationtext = "A Prioridade escolhida ir� afetar o prazo em "+StringUtil.Trim( StringUtil.Str( AV113PercPrazo, 6, 2))+" % e o valor Bruto em "+StringUtil.Trim( StringUtil.Str( AV114PercValorB, 6, 2))+" %";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_bootstrap_confirmpanel1_Internalname, "ConfirmationText", Dvelop_bootstrap_confirmpanel1_Confirmationtext);
            this.executeUsercontrolMethod("", false, "DVELOP_BOOTSTRAP_CONFIRMPANEL1Container", "Confirm", "", new Object[] {});
         }
         AV85LerServico = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85LerServico", AV85LerServico);
         /* Execute user subroutine: 'CALCULARPRAZOS' */
         S202 ();
         if (returnInSub) return;
         cmbavPrioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Values", cmbavPrioridade_codigo.ToJavascriptSource());
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
      }

      protected void E26CG2( )
      {
         /* Contagemresultado_datadmn_Isvalid Routine */
         GXt_char2 = "9999 - &ContagemResultado_DataDmn.IsValid = " + context.localUtil.Format( AV8ContagemResultado_DataDmn, "99/99/99");
         new geralog(context ).execute( ref  GXt_char2) ;
         if ( (DateTime.MinValue==AV8ContagemResultado_DataDmn) )
         {
            GX_msglist.addItem("Data da OS � obrigat�ria!");
            GX_FocusControl = edtavContagemresultado_datadmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV8ContagemResultado_DataDmn > DateTimeUtil.ServerDate( context, "DEFAULT") )
         {
            GX_msglist.addItem("Data da OS posterior ao dia de hoje!");
            GX_FocusControl = edtavContagemresultado_datadmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( AV8ContagemResultado_DataDmn < DateTimeUtil.ServerDate( context, "DEFAULT") )
         {
            GXt_char2 = "888888 - &ContagemResultado_DataDmn.IsValid = " + context.localUtil.Format( AV8ContagemResultado_DataDmn, "99/99/99");
            new geralog(context ).execute( ref  GXt_char2) ;
            /* Execute user subroutine: 'CALCULARPRAZOS' */
            S202 ();
            if (returnInSub) return;
         }
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
      }

      protected void E27CG2( )
      {
         /* Contrato_Click Routine */
         if ( AV117Contrato > 0 )
         {
            /* Execute user subroutine: 'DADOSDOSERVICO' */
            S222 ();
            if (returnInSub) return;
         }
         cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", cmbavContrato.ToJavascriptSource());
         cmbavServico_atende.CurrentValue = StringUtil.RTrim( AV109Servico_Atende);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_atende_Internalname, "Values", cmbavServico_atende.ToJavascriptSource());
         cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", cmbavServicogrupo_codigo.ToJavascriptSource());
         cmbavServico_obrigavalores.CurrentValue = StringUtil.RTrim( AV110Servico_ObrigaValores);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_obrigavalores_Internalname, "Values", cmbavServico_obrigavalores.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Sistemas", AV51Sistemas);
         cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", cmbavSistema_codigo.ToJavascriptSource());
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", dynavModulo_codigo.ToJavascriptSource());
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
         cmbavComplexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavComplexidade_Internalname, "Values", cmbavComplexidade.ToJavascriptSource());
         cmbavPrioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Values", cmbavPrioridade_codigo.ToJavascriptSource());
         cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", cmbavContratoservicos_localexec.ToJavascriptSource());
      }

      protected void E35CG2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E28CG2( )
      {
         /* Requisitante_Click Routine */
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S142 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV136Servicos", AV136Servicos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV137SDT_Servicos", AV137SDT_Servicos);
         cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", cmbavServico_codigo.ToJavascriptSource());
      }

      protected void E29CG2( )
      {
         /* Contratadaorigem_Click Routine */
         AV191GXLvl939 = 0;
         /* Using cursor H00CG18 */
         pr_default.execute(12, new Object[] {AV105ContratadaOrigem});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A1013Contrato_PrepostoCod = H00CG18_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00CG18_n1013Contrato_PrepostoCod[0];
            A39Contratada_Codigo = H00CG18_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00CG18_n39Contratada_Codigo[0];
            AV191GXLvl939 = 1;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(12);
         }
         pr_default.close(12);
         if ( AV191GXLvl939 == 0 )
         {
            Confirmpanel2_Confirmationtext = "A "+dynavContratadaorigem.Description+" n�o tem Preposto estabelecido! A OS ser� solicitada sem esta refer�ncia.";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
            Confirmpanel2_Title = "Origem";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "Title", Confirmpanel2_Title);
            this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
         }
      }

      protected void S222( )
      {
         /* 'DADOSDOSERVICO' Routine */
         GXt_char2 = "02 - DadosDoServico";
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char3 = "02 - DadosDoServico - &Contrato = " + context.localUtil.Format( (decimal)(AV117Contrato), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char3) ;
         AV171ContratoServicos_UndCntNome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV171ContratoServicos_UndCntNome", AV171ContratoServicos_UndCntNome);
         AV172ContratoServicos_QntUntCns = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV172ContratoServicos_QntUntCns, 9, 4)));
         if ( (0==AV117Contrato) )
         {
            if ( AV12RequisitadoCod > 0 )
            {
               GX_msglist.addItem("Informe o Contrato a ser usado para este servi�o!");
               AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
               AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
               AV63EntregaCalculada = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
               GX_FocusControl = cmbavContrato_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
         }
         else
         {
            /* Using cursor H00CG21 */
            pr_default.execute(13, new Object[] {AV117Contrato, AV13Servico_Codigo});
            while ( (pr_default.getStatus(13) != 101) )
            {
               A39Contratada_Codigo = H00CG21_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00CG21_n39Contratada_Codigo[0];
               A1013Contrato_PrepostoCod = H00CG21_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00CG21_n1013Contrato_PrepostoCod[0];
               A1212ContratoServicos_UnidadeContratada = H00CG21_A1212ContratoServicos_UnidadeContratada[0];
               A903ContratoServicosPrazo_CntSrvCod = H00CG21_A903ContratoServicosPrazo_CntSrvCod[0];
               A160ContratoServicos_Codigo = H00CG21_A160ContratoServicos_Codigo[0];
               A155Servico_Codigo = H00CG21_A155Servico_Codigo[0];
               A52Contratada_AreaTrabalhoCod = H00CG21_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00CG21_n52Contratada_AreaTrabalhoCod[0];
               A43Contratada_Ativo = H00CG21_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG21_n43Contratada_Ativo[0];
               A54Usuario_Ativo = H00CG21_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00CG21_n54Usuario_Ativo[0];
               A74Contrato_Codigo = H00CG21_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00CG21_n74Contrato_Codigo[0];
               A1072Servico_Atende = H00CG21_A1072Servico_Atende[0];
               n1072Servico_Atende = H00CG21_n1072Servico_Atende[0];
               A157ServicoGrupo_Codigo = H00CG21_A157ServicoGrupo_Codigo[0];
               A1429Servico_ObrigaValores = H00CG21_A1429Servico_ObrigaValores[0];
               n1429Servico_ObrigaValores = H00CG21_n1429Servico_ObrigaValores[0];
               A2092Servico_IsOrigemReferencia = H00CG21_A2092Servico_IsOrigemReferencia[0];
               A2094ContratoServicos_SolicitaGestorSistema = H00CG21_A2094ContratoServicos_SolicitaGestorSistema[0];
               A2132ContratoServicos_UndCntNome = H00CG21_A2132ContratoServicos_UndCntNome[0];
               n2132ContratoServicos_UndCntNome = H00CG21_n2132ContratoServicos_UndCntNome[0];
               A1340ContratoServicos_QntUntCns = H00CG21_A1340ContratoServicos_QntUntCns[0];
               n1340ContratoServicos_QntUntCns = H00CG21_n1340ContratoServicos_QntUntCns[0];
               A1152ContratoServicos_PrazoAnalise = H00CG21_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = H00CG21_n1152ContratoServicos_PrazoAnalise[0];
               A1454ContratoServicos_PrazoTpDias = H00CG21_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = H00CG21_n1454ContratoServicos_PrazoTpDias[0];
               A557Servico_VlrUnidadeContratada = H00CG21_A557Servico_VlrUnidadeContratada[0];
               A116Contrato_ValorUnidadeContratacao = H00CG21_A116Contrato_ValorUnidadeContratacao[0];
               A1649ContratoServicos_PrazoInicio = H00CG21_A1649ContratoServicos_PrazoInicio[0];
               n1649ContratoServicos_PrazoInicio = H00CG21_n1649ContratoServicos_PrazoInicio[0];
               A913ContratoServicos_PrazoTipo = H00CG21_A913ContratoServicos_PrazoTipo[0];
               n913ContratoServicos_PrazoTipo = H00CG21_n913ContratoServicos_PrazoTipo[0];
               A843Contrato_DataFimTA = H00CG21_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00CG21_n843Contrato_DataFimTA[0];
               A83Contrato_DataVigenciaTermino = H00CG21_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00CG21_n83Contrato_DataVigenciaTermino[0];
               A2132ContratoServicos_UndCntNome = H00CG21_A2132ContratoServicos_UndCntNome[0];
               n2132ContratoServicos_UndCntNome = H00CG21_n2132ContratoServicos_UndCntNome[0];
               A903ContratoServicosPrazo_CntSrvCod = H00CG21_A903ContratoServicosPrazo_CntSrvCod[0];
               A913ContratoServicos_PrazoTipo = H00CG21_A913ContratoServicos_PrazoTipo[0];
               n913ContratoServicos_PrazoTipo = H00CG21_n913ContratoServicos_PrazoTipo[0];
               A1072Servico_Atende = H00CG21_A1072Servico_Atende[0];
               n1072Servico_Atende = H00CG21_n1072Servico_Atende[0];
               A157ServicoGrupo_Codigo = H00CG21_A157ServicoGrupo_Codigo[0];
               A1429Servico_ObrigaValores = H00CG21_A1429Servico_ObrigaValores[0];
               n1429Servico_ObrigaValores = H00CG21_n1429Servico_ObrigaValores[0];
               A2092Servico_IsOrigemReferencia = H00CG21_A2092Servico_IsOrigemReferencia[0];
               A39Contratada_Codigo = H00CG21_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00CG21_n39Contratada_Codigo[0];
               A1013Contrato_PrepostoCod = H00CG21_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00CG21_n1013Contrato_PrepostoCod[0];
               A116Contrato_ValorUnidadeContratacao = H00CG21_A116Contrato_ValorUnidadeContratacao[0];
               A83Contrato_DataVigenciaTermino = H00CG21_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00CG21_n83Contrato_DataVigenciaTermino[0];
               A52Contratada_AreaTrabalhoCod = H00CG21_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00CG21_n52Contratada_AreaTrabalhoCod[0];
               A43Contratada_Ativo = H00CG21_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG21_n43Contratada_Ativo[0];
               A54Usuario_Ativo = H00CG21_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00CG21_n54Usuario_Ativo[0];
               A843Contrato_DataFimTA = H00CG21_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00CG21_n843Contrato_DataFimTA[0];
               A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               AV49ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49ContratoServicos_Codigo), 6, 0)));
               AV109Servico_Atende = A1072Servico_Atende;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109Servico_Atende", AV109Servico_Atende);
               AV16ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
               AV110Servico_ObrigaValores = A1429Servico_ObrigaValores;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110Servico_ObrigaValores", AV110Servico_ObrigaValores);
               AV166Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166Servico_IsOrigemReferencia", AV166Servico_IsOrigemReferencia);
               AV168Servico_IsSolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168Servico_IsSolicitaGestorSistema", AV168Servico_IsSolicitaGestorSistema);
               AV171ContratoServicos_UndCntNome = A2132ContratoServicos_UndCntNome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV171ContratoServicos_UndCntNome", AV171ContratoServicos_UndCntNome);
               AV172ContratoServicos_QntUntCns = A1340ContratoServicos_QntUntCns;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV172ContratoServicos_QntUntCns, 9, 4)));
               AV98DiasAnalise = A1152ContratoServicos_PrazoAnalise;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98DiasAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV98DiasAnalise), 4, 0)));
               AV84PrazoTipo = A913ContratoServicos_PrazoTipo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84PrazoTipo", AV84PrazoTipo);
               AV115TipoDias = A1454ContratoServicos_PrazoTpDias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115TipoDias", AV115TipoDias);
               GXt_char3 = "03 - &DiasAnalise = " + context.localUtil.Format( (decimal)(AV98DiasAnalise), "ZZZ9");
               new geralog(context ).execute( ref  GXt_char3) ;
               GXt_char2 = "04 &PrazoTipo   =" + gxdomaintipodeprazo.getDescription(context,A913ContratoServicos_PrazoTipo);
               new geralog(context ).execute( ref  GXt_char2) ;
               GXt_char4 = "05 - &&TipoDias = " + StringUtil.RTrim( context.localUtil.Format( AV115TipoDias, ""));
               new geralog(context ).execute( ref  GXt_char4) ;
               if ( ( A557Servico_VlrUnidadeContratada > Convert.ToDecimal( 0 )) )
               {
                  AV26ContagemResultado_ValorPF = A557Servico_VlrUnidadeContratada;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( AV26ContagemResultado_ValorPF, 18, 5)));
               }
               else
               {
                  AV26ContagemResultado_ValorPF = A116Contrato_ValorUnidadeContratacao;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ValorPF", StringUtil.LTrim( StringUtil.Str( AV26ContagemResultado_ValorPF, 18, 5)));
               }
               if ( StringUtil.StrCmp(A1072Servico_Atende, "S") == 0 )
               {
                  lblTextblocksistema_codigo_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocksistema_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocksistema_codigo_Visible), 5, 0)));
                  lblTextblockmodulo_codigo_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockmodulo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockmodulo_codigo_Visible), 5, 0)));
                  lblTextblockfuncaousuario_codigo_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfuncaousuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfuncaousuario_codigo_Visible), 5, 0)));
                  cmbavSistema_codigo.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Visible), 5, 0)));
                  dynavModulo_codigo.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Visible), 5, 0)));
                  dynavFuncaousuario_codigo.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaousuario_codigo.Visible), 5, 0)));
               }
               else
               {
                  lblTextblocksistema_codigo_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocksistema_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocksistema_codigo_Visible), 5, 0)));
                  lblTextblockmodulo_codigo_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockmodulo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockmodulo_codigo_Visible), 5, 0)));
                  lblTextblockfuncaousuario_codigo_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfuncaousuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfuncaousuario_codigo_Visible), 5, 0)));
                  cmbavSistema_codigo.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Visible), 5, 0)));
                  dynavModulo_codigo.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Visible), 5, 0)));
                  dynavFuncaousuario_codigo.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaousuario_codigo.Visible), 5, 0)));
               }
               if ( ( StringUtil.StrCmp(AV84PrazoTipo, "V") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "A") == 0 ) || ( StringUtil.StrCmp(AV84PrazoTipo, "P") == 0 ) )
               {
                  lblTextblockpfbfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
                  lblTextblockpflfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
                  edtavPfbfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
                  edtavPflfsdaosvnc_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
               }
               else
               {
                  AV66PFBFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
                  lblTextblockpfbfsdaosvnc_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
                  lblTextblockpflfsdaosvnc_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
                  edtavPfbfsdaosvnc_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
                  edtavPflfsdaosvnc_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
               }
               if ( StringUtil.StrCmp(A1072Servico_Atende, "S") == 0 )
               {
                  AV51Sistemas.Clear();
                  /* Using cursor H00CG22 */
                  pr_default.execute(14, new Object[] {A160ContratoServicos_Codigo, A155Servico_Codigo});
                  while ( (pr_default.getStatus(14) != 101) )
                  {
                     A1067ContratoServicosSistemas_ServicoCod = H00CG22_A1067ContratoServicosSistemas_ServicoCod[0];
                     A1063ContratoServicosSistemas_SistemaCod = H00CG22_A1063ContratoServicosSistemas_SistemaCod[0];
                     AV51Sistemas.Add(A1063ContratoServicosSistemas_SistemaCod, 0);
                     pr_default.readNext(14);
                  }
                  pr_default.close(14);
               }
               else
               {
                  AV38Sistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
                  AV39Modulo_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
                  AV41FuncaoUsuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
               }
               AV52SistemasTinhaFiltro = (bool)(((AV51Sistemas.Count>0)));
               cmbavComplexidade.removeAllItems();
               AV97Complexidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)));
               cmbavComplexidade.addItem("0", "(Nenhuma)", 0);
               /* Using cursor H00CG23 */
               pr_default.execute(15, new Object[] {A160ContratoServicos_Codigo});
               while ( (pr_default.getStatus(15) != 101) )
               {
                  A903ContratoServicosPrazo_CntSrvCod = H00CG23_A903ContratoServicosPrazo_CntSrvCod[0];
                  A1823ContratoServicosPrazo_Momento = H00CG23_A1823ContratoServicosPrazo_Momento[0];
                  n1823ContratoServicosPrazo_Momento = H00CG23_n1823ContratoServicosPrazo_Momento[0];
                  A1265ContratoServicosPrazo_DiasBaixa = H00CG23_A1265ContratoServicosPrazo_DiasBaixa[0];
                  n1265ContratoServicosPrazo_DiasBaixa = H00CG23_n1265ContratoServicosPrazo_DiasBaixa[0];
                  A1264ContratoServicosPrazo_DiasMedia = H00CG23_A1264ContratoServicosPrazo_DiasMedia[0];
                  n1264ContratoServicosPrazo_DiasMedia = H00CG23_n1264ContratoServicosPrazo_DiasMedia[0];
                  A1263ContratoServicosPrazo_DiasAlta = H00CG23_A1263ContratoServicosPrazo_DiasAlta[0];
                  n1263ContratoServicosPrazo_DiasAlta = H00CG23_n1263ContratoServicosPrazo_DiasAlta[0];
                  AV135PrazoMomento = A1823ContratoServicosPrazo_Momento;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135PrazoMomento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135PrazoMomento), 4, 0)));
                  if ( A1265ContratoServicosPrazo_DiasBaixa > 0 )
                  {
                     cmbavComplexidade.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)), "Baixa ("+StringUtil.Trim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0))+" dias)", 0);
                  }
                  if ( A1264ContratoServicosPrazo_DiasMedia > 0 )
                  {
                     cmbavComplexidade.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)), "M�dia ("+StringUtil.Trim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0))+" dias)", 0);
                  }
                  if ( A1263ContratoServicosPrazo_DiasAlta > 0 )
                  {
                     cmbavComplexidade.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)), "Alta ("+StringUtil.Trim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0))+" dias)", 0);
                  }
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(15);
               cmbavPrioridade_codigo.removeAllItems();
               AV96Prioridade_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Prioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)));
               cmbavPrioridade_codigo.addItem("0", "Normal", 0);
               /* Using cursor H00CG24 */
               pr_default.execute(16, new Object[] {A160ContratoServicos_Codigo});
               while ( (pr_default.getStatus(16) != 101) )
               {
                  A1335ContratoServicosPrioridade_CntSrvCod = H00CG24_A1335ContratoServicosPrioridade_CntSrvCod[0];
                  A1337ContratoServicosPrioridade_Nome = H00CG24_A1337ContratoServicosPrioridade_Nome[0];
                  A1336ContratoServicosPrioridade_Codigo = H00CG24_A1336ContratoServicosPrioridade_Codigo[0];
                  cmbavPrioridade_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1336ContratoServicosPrioridade_Codigo), 6, 0)), A1337ContratoServicosPrioridade_Nome, 0);
                  pr_default.readNext(16);
               }
               pr_default.close(16);
               AV139ContratoVencido = (bool)((AV8ContagemResultado_DataDmn>A1869Contrato_DataTermino));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV139ContratoVencido", AV139ContratoVencido);
               AV115TipoDias = A1454ContratoServicos_PrazoTpDias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115TipoDias", AV115TipoDias);
               AV122PrazoInicio = A1649ContratoServicos_PrazoInicio;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(AV122PrazoInicio), 4, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(13);
            }
            pr_default.close(13);
            if ( AV139ContratoVencido )
            {
               GX_msglist.addItem("O Contrato "+cmbavContrato.Description+" do servi�o informado esta vencido!");
            }
            else
            {
               if ( cmbavComplexidade.ItemCount > 0 )
               {
                  GX_FocusControl = cmbavComplexidade_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  context.DoAjaxSetFocus(GX_FocusControl);
               }
               else
               {
                  if ( cmbavPrioridade_codigo.ItemCount > 0 )
                  {
                     GX_FocusControl = cmbavPrioridade_codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     context.DoAjaxSetFocus(GX_FocusControl);
                  }
                  else
                  {
                     GXt_char4 = "06 - entrou aqui";
                     new geralog(context ).execute( ref  GXt_char4) ;
                     /* Execute user subroutine: 'PRAZOANALISE' */
                     S302 ();
                     if (returnInSub) return;
                     GXt_char3 = "19 - entrou aqui";
                     new geralog(context ).execute( ref  GXt_char3) ;
                     /* Execute user subroutine: 'CALCULARPRAZOS' */
                     S202 ();
                     if (returnInSub) return;
                  }
               }
               lblTextblockprioridade_codigo_Visible = ((cmbavPrioridade_codigo.ItemCount>1) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockprioridade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockprioridade_codigo_Visible), 5, 0)));
               lblTextblockcomplexidade_Visible = ((cmbavComplexidade.ItemCount>1) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcomplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcomplexidade_Visible), 5, 0)));
               cmbavComplexidade.Visible = ((cmbavComplexidade.ItemCount>1) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavComplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavComplexidade.Visible), 5, 0)));
               cmbavPrioridade_codigo.Visible = ((cmbavPrioridade_codigo.ItemCount>1) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPrioridade_codigo.Visible), 5, 0)));
               if ( ( AV51Sistemas.Count > 0 ) || ( ( AV51Sistemas.Count == 0 ) && AV52SistemasTinhaFiltro ) )
               {
                  /* Execute user subroutine: 'CARREGASISTEMAS' */
                  S192 ();
                  if (returnInSub) return;
               }
               /* Execute user subroutine: 'TEMARTEFATOS' */
               S312 ();
               if (returnInSub) return;
               /* Execute user subroutine: 'CHECKSALDOCONTRATO' */
               S272 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S212( )
      {
         /* 'REQUISITADOSELECIONADO' Routine */
         GXt_char4 = "entrou aqui RequisitadoSelecionado";
         new geralog(context ).execute( ref  GXt_char4) ;
         AV12RequisitadoCod = (int)(NumberUtil.Val( StringUtil.Substring( AV9Requisitado, 2, 6), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
         /* Execute user subroutine: 'CARREGACONTRATOS' */
         S322 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'CALCULARPRAZOS' */
         S202 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'USUARIOSDAREQUISITADA' */
         S332 ();
         if (returnInSub) return;
      }

      protected void S202( )
      {
         /* 'CALCULARPRAZOS' Routine */
         GXt_char4 = "20 - CalcularPrazos";
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = "21 - &ContratoServicos_Codigo = " + context.localUtil.Format( (decimal)(AV49ContratoServicos_Codigo), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char3) ;
         if ( AV49ContratoServicos_Codigo > 0 )
         {
            if ( AV85LerServico )
            {
               AV85LerServico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85LerServico", AV85LerServico);
               /* Using cursor H00CG25 */
               pr_default.execute(17, new Object[] {AV49ContratoServicos_Codigo});
               while ( (pr_default.getStatus(17) != 101) )
               {
                  A155Servico_Codigo = H00CG25_A155Servico_Codigo[0];
                  A160ContratoServicos_Codigo = H00CG25_A160ContratoServicos_Codigo[0];
                  A639ContratoServicos_LocalExec = H00CG25_A639ContratoServicos_LocalExec[0];
                  A889Servico_Terceriza = H00CG25_A889Servico_Terceriza[0];
                  n889Servico_Terceriza = H00CG25_n889Servico_Terceriza[0];
                  A1454ContratoServicos_PrazoTpDias = H00CG25_A1454ContratoServicos_PrazoTpDias[0];
                  n1454ContratoServicos_PrazoTpDias = H00CG25_n1454ContratoServicos_PrazoTpDias[0];
                  A1152ContratoServicos_PrazoAnalise = H00CG25_A1152ContratoServicos_PrazoAnalise[0];
                  n1152ContratoServicos_PrazoAnalise = H00CG25_n1152ContratoServicos_PrazoAnalise[0];
                  A1649ContratoServicos_PrazoInicio = H00CG25_A1649ContratoServicos_PrazoInicio[0];
                  n1649ContratoServicos_PrazoInicio = H00CG25_n1649ContratoServicos_PrazoInicio[0];
                  A889Servico_Terceriza = H00CG25_A889Servico_Terceriza[0];
                  n889Servico_Terceriza = H00CG25_n889Servico_Terceriza[0];
                  AV18ContratoServicos_LocalExec = A639ContratoServicos_LocalExec;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicos_LocalExec", AV18ContratoServicos_LocalExec);
                  AV37Servico_Terceriza = A889Servico_Terceriza;
                  cmbavContratoservicos_localexec.Enabled = (((StringUtil.StrCmp(A639ContratoServicos_LocalExec, "O")==0)) ? 1 : 0);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContratoservicos_localexec.Enabled), 5, 0)));
                  AV115TipoDias = A1454ContratoServicos_PrazoTpDias;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115TipoDias", AV115TipoDias);
                  AV67Dias = A1152ContratoServicos_PrazoAnalise;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Dias), 4, 0)));
                  AV122PrazoInicio = A1649ContratoServicos_PrazoInicio;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122PrazoInicio", StringUtil.LTrim( StringUtil.Str( (decimal)(AV122PrazoInicio), 4, 0)));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(17);
            }
         }
         GXt_char4 = "22 - &TipoDias = " + gxdomaintipodias.getDescription(context,AV115TipoDias);
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_char3 = "23 - &&Dias = " + context.localUtil.Format( (decimal)(AV67Dias), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = "24 - PrazoInicio (1-Dia seguinte / 2-Dia �til seguinte / 3-Imediato) = " + context.localUtil.Format( (decimal)(AV122PrazoInicio), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char2) ;
         GXt_char5 = "25 - &PrazoTipo = " + gxdomaintipodeprazo.getDescription(context,AV84PrazoTipo);
         new geralog(context ).execute( ref  GXt_char5) ;
         if ( StringUtil.StrCmp(AV84PrazoTipo, "C") == 0 )
         {
            if ( (0==AV97Complexidade) )
            {
               AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
               AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
               AV63EntregaCalculada = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               GXt_char5 = "26 - entrou aqui";
               new geralog(context ).execute( ref  GXt_char5) ;
               /* Execute user subroutine: 'CALCULARPRAZOENTREGA' */
               S342 ();
               if (returnInSub) return;
            }
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84PrazoTipo)) || ( StringUtil.StrCmp(AV84PrazoTipo, "S") != 0 ) )
         {
            GXt_char5 = "30 - entrou aqui";
            new geralog(context ).execute( ref  GXt_char5) ;
            GXt_char4 = "31 - Case &PrazoTipo.IsEmpty() or &PrazoTipo <> TipodePrazo.Especifico";
            new geralog(context ).execute( ref  GXt_char4) ;
            /* Execute user subroutine: 'CALCULARPRAZOENTREGA' */
            S342 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'ADDHORASENTREGA' */
         S252 ();
         if (returnInSub) return;
         GXt_char3 = "CalcularPrazos";
         new geralog(context ).execute( ref  GXt_char3) ;
         GXt_char2 = "CalcularPrazos";
         new geralog(context ).execute( ref  GXt_char2) ;
      }

      protected void S342( )
      {
         /* 'CALCULARPRAZOENTREGA' Routine */
         GXt_char6 = "27 - CalcularPrazoEntrega";
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char7 = "28 - CalcularPrazoEntrega";
         new geralog(context ).execute( ref  GXt_char7) ;
         /* Execute user subroutine: 'PRAZOANALISE' */
         S302 ();
         if (returnInSub) return;
         AV29Websession.Set("Momento", "Solicitacao");
         GXt_int8 = AV67Dias;
         new prc_diasparaentrega(context ).execute( ref  AV49ContratoServicos_Codigo,  AV66PFBFSdaOSVnc,  AV97Complexidade, out  GXt_int8) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)));
         AV67Dias = GXt_int8;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Dias), 4, 0)));
         AV29Websession.Remove("Momento");
         GXt_dtime1 = AV63EntregaCalculada;
         new prc_adddiasuteis(context ).execute(  AV63EntregaCalculada,  AV67Dias,  AV115TipoDias, out  GXt_dtime1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Dias), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115TipoDias", AV115TipoDias);
         AV63EntregaCalculada = GXt_dtime1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         GXt_char7 = "29 - &EntregaCalculada = " + context.localUtil.Format( AV63EntregaCalculada, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char7) ;
         if ( AV63EntregaCalculada > AV70PrazoAnalise )
         {
            AV62PrazoEntrega = AV63EntregaCalculada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( (0==AV98DiasAnalise) )
         {
            AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         }
      }

      protected void S252( )
      {
         /* 'ADDHORASENTREGA' Routine */
         new prc_gethorasentrega(context ).execute(  AV6WWPContext.gxTpr_Areatrabalho_codigo,  0,  AV49ContratoServicos_Codigo, out  AV77Hours, out  AV78Minutes) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49ContratoServicos_Codigo), 6, 0)));
         AV70PrazoAnalise = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV70PrazoAnalise) ) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         AV70PrazoAnalise = DateTimeUtil.TAdd( AV70PrazoAnalise, 3600*(AV77Hours));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         AV70PrazoAnalise = DateTimeUtil.TAdd( AV70PrazoAnalise, 60*(AV78Minutes));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         if ( ! (DateTime.MinValue==AV62PrazoEntrega) )
         {
            AV62PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV62PrazoEntrega) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV62PrazoEntrega = DateTimeUtil.TAdd( AV62PrazoEntrega, 3600*(AV77Hours));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            AV62PrazoEntrega = DateTimeUtil.TAdd( AV62PrazoEntrega, 60*(AV78Minutes));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
      }

      protected void S292( )
      {
         /* 'ENVIONOTIFICACAO' Routine */
         if ( AV61ContratanteSemEmailSda )
         {
            GX_msglist.addItem("Cadastro da Contratante sem dados configurados para envio de e-mails!");
         }
         else
         {
            /* Execute user subroutine: 'GETEMAILS' */
            S352 ();
            if (returnInSub) return;
            if ( AV42Usuarios.Count > 0 )
            {
               AV45EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "H� uma nova solicita��o para a prestadora do servi�o de " + cmbavServico_codigo.Description + " conforme segue abaixo os detalhes." + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "No da OS�������������������" + AV69ContagemResultado_DemandaFM + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Servi�o��������������������" + cmbavServico_codigo.Description + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Descri��o������������������" + StringUtil.Trim( AV88ContagemResultado_Descricao) + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Data de Abertura�����������" + context.localUtil.DToC( AV8ContagemResultado_DataDmn, 2, "/") + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Expectativa de Atendimento�" + context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " ") + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Requisitante���������������" + AV6WWPContext.gxTpr_Username + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Telefone�������������������" + AV6WWPContext.gxTpr_Contratante_telefone + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Unidade Organizacional�����" + StringUtil.Trim( AV6WWPContext.gxTpr_Areatrabalho_descricao) + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + "Para atendimento da demanda acesse o MEETRIKA - Sistema de Gest�o Contratual e Automa��o de Processos " + StringUtil.NewLine( );
               AV45EmailText = AV45EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV6WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV6WWPContext.gxTpr_Username) + StringUtil.NewLine( );
               if ( AV102Requisitante > 0 )
               {
                  AV45EmailText = AV45EmailText + " (autorizado por " + dynavRequisitante.Description + ")" + StringUtil.NewLine( );
               }
               else
               {
                  AV45EmailText = AV45EmailText + StringUtil.NewLine( );
               }
               AV45EmailText = AV45EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
               AV119Demandante = AV6WWPContext.gxTpr_Areatrabalho_descricao;
               AV118NomeSistema = AV6WWPContext.gxTpr_Parametrossistema_nomesistema;
               AV43Subject = "[" + StringUtil.Trim( AV118NomeSistema) + " - " + StringUtil.Trim( AV119Demandante) + "] OS " + StringUtil.Trim( AV69ContagemResultado_DemandaFM) + " solicitada" + " (No reply)";
               AV158Codigos.Add((double)(NumberUtil.Val( AV29Websession.Get("LastContagemResultado_Codigo"), ".")), 0);
               AV29Websession.Set("DemandaCodigo", AV158Codigos.ToXml(false, true, "Collection", ""));
               new prc_enviaremail(context ).execute(  AV6WWPContext.gxTpr_Areatrabalho_codigo,  AV42Usuarios,  AV43Subject,  AV45EmailText,  AV46Attachments, ref  AV44Resultado) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Resultado", AV44Resultado);
               AV29Websession.Remove("DemandaCodigo");
            }
            else
            {
               AV44Resultado = "Sem usu�rio preposto no requisitado para envio da notifica��o!";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Resultado", AV44Resultado);
            }
            Gx_msg = Gx_msg + StringUtil.NewLine( ) + AV44Resultado;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_msg", Gx_msg);
         }
         Dvelop_bootstrap_confirmpanel1_Title = "Confirma��o";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_bootstrap_confirmpanel1_Internalname, "Title", Dvelop_bootstrap_confirmpanel1_Title);
         Dvelop_bootstrap_confirmpanel1_Confirmationtext = Gx_msg;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_bootstrap_confirmpanel1_Internalname, "ConfirmationText", Dvelop_bootstrap_confirmpanel1_Confirmationtext);
         this.executeUsercontrolMethod("", false, "DVELOP_BOOTSTRAP_CONFIRMPANEL1Container", "Confirm", "", new Object[] {});
      }

      protected void S262( )
      {
         /* 'LIMPARDADOS' Routine */
         AV5ContagemResultado_Demanda = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Demanda", AV5ContagemResultado_Demanda);
         AV23ContagemResultado_OSVinculada = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContagemResultado_OSVinculada), 6, 0)));
         AV13Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
         AV16ServicoGrupo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
         AV9Requisitado = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
         AV10Usuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0)));
         AV22ContagemResultado_DmnVinculada = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
         AV133ContagemResultado_DmnVinculadaRef = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV133ContagemResultado_DmnVinculadaRef", AV133ContagemResultado_DmnVinculadaRef);
         AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PrazoEntrega", context.localUtil.TToC( AV62PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         AV63EntregaCalculada = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         AV38Sistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
         AV39Modulo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
         AV41FuncaoUsuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
         AV15ContagemResultado_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContagemResultado_Link", AV15ContagemResultado_Link);
         AV19ContagemResultado_Observacao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_Observacao", AV19ContagemResultado_Observacao);
         AV97Complexidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV97Complexidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)));
         AV96Prioridade_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96Prioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)));
         AV88ContagemResultado_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ContagemResultado_Descricao", AV88ContagemResultado_Descricao);
         AV101RequisitadoPor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101RequisitadoPor", AV101RequisitadoPor);
         AV102Requisitante = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102Requisitante", StringUtil.LTrim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0)));
         AV105ContratadaOrigem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
         AV107ContadorFS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ContadorFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)));
         AV8ContagemResultado_DataDmn = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_DataDmn", context.localUtil.Format(AV8ContagemResultado_DataDmn, "99/99/99"));
         cmbavDmnvinculadas.removeAllItems();
         AV123DmnVinculadas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
         AV117Contrato = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)));
         AV139ContratoVencido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV139ContratoVencido", AV139ContratoVencido);
         AV66PFBFSdaOSVnc = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
         AV73PFLFSdaOSVnc = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
         lblTextblockrequisitante_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockrequisitante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockrequisitante_Visible), 5, 0)));
         lblTextblockpfbfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
         lblTextblockpflfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
         edtavPfbfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
         edtavPflfsdaosvnc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
         dynavRequisitante.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavRequisitante_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavRequisitante.Visible), 5, 0)));
         cmbavDmnvinculadas.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDmnvinculadas.Visible), 5, 0)));
         lblTextblockcontrato_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_Visible), 5, 0)));
         cmbavContrato.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContrato.Visible), 5, 0)));
         edtavContagemresultado_dmnvinculada_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculada_Visible), 5, 0)));
         edtavContagemresultado_dmnvinculadaref_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculadaref_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculadaref_Visible), 5, 0)));
         cmbavSistema_codigo.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_codigo.Enabled), 5, 0)));
         dynavModulo_codigo.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavModulo_codigo.Enabled), 5, 0)));
         dynavContadorfs.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContadorfs.Enabled), 5, 0)));
         dynavContratadaorigem.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContratadaorigem.Enabled), 5, 0)));
         dynavFuncaousuario_codigo.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaousuario_codigo.Enabled), 5, 0)));
         AV69ContagemResultado_DemandaFM = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_DemandaFM", AV69ContagemResultado_DemandaFM);
         if ( AV53Contratante_OSAutomatica )
         {
            AV69ContagemResultado_DemandaFM = "Gera��o autom�tica";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_DemandaFM", AV69ContagemResultado_DemandaFM);
         }
         edtavContagemresultado_demandafm_Enabled = (!AV53Contratante_OSAutomatica ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm_Enabled), 5, 0)));
         AV29Websession.Remove("ArquivosEvd");
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcarquivosevd_Component), StringUtil.Lower( "WC_ContagemResultadoEvidencias")) != 0 )
         {
            WebComp_Wcarquivosevd = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wcarquivosevd.ComponentInit();
            WebComp_Wcarquivosevd.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wcarquivosevd_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
         {
            WebComp_Wcarquivosevd.setjustcreated();
            WebComp_Wcarquivosevd.componentprepare(new Object[] {(String)"W0207",(String)""});
            WebComp_Wcarquivosevd.componentbind(new Object[] {});
         }
         if ( isFullAjaxMode( ) )
         {
            context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0207"+"");
            WebComp_Wcarquivosevd.componentdraw();
            context.httpAjaxContext.ajax_rspEndCmp();
         }
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABContainer", "SelectTab", "", new Object[] {(String)"Solicitacao"});
         GX_FocusControl = edtavContagemresultado_descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         AV126lastContagemResultado_Codigo = (int)(NumberUtil.Val( AV29Websession.Get("LastContagemResultado_Codigo"), "."));
         lblLblultimaos_Caption = "Ultima OS cadastrada: "+StringUtil.Trim( StringUtil.Str( (decimal)(AV126lastContagemResultado_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblultimaos_Internalname, "Caption", lblLblultimaos_Caption);
         lblLblultimaos_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV126lastContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("General"));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLblultimaos_Internalname, "Link", lblLblultimaos_Link);
         AV29Websession.Remove("LastContagemResultado_Codigo");
         AV161SDT_Requisitos.Clear();
         gx_BV219 = true;
         this.executeUsercontrolMethod("", false, "GXUITABSPANEL_TABContainer", "HideTabById", "", new Object[] {(String)"TabRequisitos"});
         /* Execute user subroutine: 'NAOTEMARTEFATOS' */
         S242 ();
         if (returnInSub) return;
      }

      protected void S352( )
      {
         /* 'GETEMAILS' Routine */
         AV42Usuarios.Clear();
         /* Using cursor H00CG26 */
         pr_default.execute(18, new Object[] {AV117Contrato});
         while ( (pr_default.getStatus(18) != 101) )
         {
            A39Contratada_Codigo = H00CG26_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00CG26_n39Contratada_Codigo[0];
            A74Contrato_Codigo = H00CG26_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00CG26_n74Contrato_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00CG26_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG26_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CG26_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG26_n43Contratada_Ativo[0];
            A54Usuario_Ativo = H00CG26_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG26_n54Usuario_Ativo[0];
            A1013Contrato_PrepostoCod = H00CG26_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00CG26_n1013Contrato_PrepostoCod[0];
            A52Contratada_AreaTrabalhoCod = H00CG26_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG26_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CG26_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG26_n43Contratada_Ativo[0];
            A54Usuario_Ativo = H00CG26_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG26_n54Usuario_Ativo[0];
            if ( A1013Contrato_PrepostoCod > 0 )
            {
               AV42Usuarios.Add(A1013Contrato_PrepostoCod, 0);
            }
            else
            {
               /* Using cursor H00CG27 */
               pr_default.execute(19, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
               while ( (pr_default.getStatus(19) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00CG27_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00CG27_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00CG27_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00CG27_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = H00CG27_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = H00CG27_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean9 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean9) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean9;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV42Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
                  }
                  pr_default.readNext(19);
               }
               pr_default.close(19);
            }
            /* Using cursor H00CG28 */
            pr_default.execute(20, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            while ( (pr_default.getStatus(20) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00CG28_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00CG28_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00CG28_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00CG28_n1446ContratoGestor_ContratadaAreaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = H00CG28_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = H00CG28_n1446ContratoGestor_ContratadaAreaCod[0];
               GXt_boolean9 = A1135ContratoGestor_UsuarioEhContratante;
               new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean9) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               A1135ContratoGestor_UsuarioEhContratante = GXt_boolean9;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
               if ( A1135ContratoGestor_UsuarioEhContratante )
               {
                  AV42Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
               }
               pr_default.readNext(20);
            }
            pr_default.close(20);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(18);
      }

      protected void S232( )
      {
         /* 'CARREGAREQUISITADOS' Routine */
         GXt_char7 = "RequisitadoSelecionado";
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "RequisitadoSelecionado";
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "RequisitadoSelecionado";
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "RequisitadoSelecionado";
         new geralog(context ).execute( ref  GXt_char4) ;
         cmbavRequisitado.removeAllItems();
         cmbavRequisitado.addItem("A0", "(Nenhum)", 0);
         AV131Contratadas.Clear();
         GXt_char7 = "&WWPContext.UserEhContratante = " + StringUtil.BoolToStr( AV6WWPContext.gxTpr_Userehcontratante);
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "&WWPContext.UserEhAdministradorGAM = " + StringUtil.BoolToStr( AV6WWPContext.gxTpr_Userehadministradorgam);
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "&WWPContext.AreaTrabalho_Codigo = " + context.localUtil.Format( (decimal)(AV6WWPContext.gxTpr_Areatrabalho_codigo), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "&&Servico_Codigo = " + context.localUtil.Format( (decimal)(AV13Servico_Codigo), "ZZZZZ9");
         new geralog(context ).execute( ref  GXt_char4) ;
         if ( AV6WWPContext.gxTpr_Userehcontratante || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            /* Using cursor H00CG31 */
            pr_default.execute(21, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV13Servico_Codigo});
            while ( (pr_default.getStatus(21) != 101) )
            {
               A74Contrato_Codigo = H00CG31_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00CG31_n74Contrato_Codigo[0];
               A40Contratada_PessoaCod = H00CG31_A40Contratada_PessoaCod[0];
               A75Contrato_AreaTrabalhoCod = H00CG31_A75Contrato_AreaTrabalhoCod[0];
               n75Contrato_AreaTrabalhoCod = H00CG31_n75Contrato_AreaTrabalhoCod[0];
               A155Servico_Codigo = H00CG31_A155Servico_Codigo[0];
               A43Contratada_Ativo = H00CG31_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG31_n43Contratada_Ativo[0];
               A92Contrato_Ativo = H00CG31_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00CG31_n92Contrato_Ativo[0];
               A638ContratoServicos_Ativo = H00CG31_A638ContratoServicos_Ativo[0];
               A83Contrato_DataVigenciaTermino = H00CG31_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00CG31_n83Contrato_DataVigenciaTermino[0];
               A39Contratada_Codigo = H00CG31_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00CG31_n39Contratada_Codigo[0];
               A41Contratada_PessoaNom = H00CG31_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00CG31_n41Contratada_PessoaNom[0];
               A843Contrato_DataFimTA = H00CG31_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00CG31_n843Contrato_DataFimTA[0];
               A75Contrato_AreaTrabalhoCod = H00CG31_A75Contrato_AreaTrabalhoCod[0];
               n75Contrato_AreaTrabalhoCod = H00CG31_n75Contrato_AreaTrabalhoCod[0];
               A92Contrato_Ativo = H00CG31_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00CG31_n92Contrato_Ativo[0];
               A83Contrato_DataVigenciaTermino = H00CG31_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00CG31_n83Contrato_DataVigenciaTermino[0];
               A39Contratada_Codigo = H00CG31_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00CG31_n39Contratada_Codigo[0];
               A40Contratada_PessoaCod = H00CG31_A40Contratada_PessoaCod[0];
               A43Contratada_Ativo = H00CG31_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG31_n43Contratada_Ativo[0];
               A41Contratada_PessoaNom = H00CG31_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00CG31_n41Contratada_PessoaNom[0];
               A843Contrato_DataFimTA = H00CG31_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00CG31_n843Contrato_DataFimTA[0];
               if ( ( A843Contrato_DataFimTA >= DateTimeUtil.ServerDate( context, "DEFAULT") ) || ( A83Contrato_DataVigenciaTermino >= DateTimeUtil.ServerDate( context, "DEFAULT") ) )
               {
                  cmbavRequisitado.addItem("A"+StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)), A41Contratada_PessoaNom, 0);
                  AV131Contratadas.Add(A39Contratada_Codigo, 0);
                  AV9Requisitado = "A" + StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
                  AV12RequisitadoCod = A39Contratada_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
               }
               pr_default.readNext(21);
            }
            pr_default.close(21);
            if ( cmbavRequisitado.ItemCount > 2 )
            {
               AV9Requisitado = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
               AV12RequisitadoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
            }
            else
            {
               if ( cmbavRequisitado.ItemCount > 1 )
               {
                  AV85LerServico = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85LerServico", AV85LerServico);
                  /* Execute user subroutine: 'REQUISITADOSELECIONADO' */
                  S212 ();
                  if (returnInSub) return;
               }
            }
         }
         else
         {
            /* Using cursor H00CG34 */
            pr_default.execute(22, new Object[] {AV13Servico_Codigo, AV6WWPContext.gxTpr_Contratada_codigo});
            while ( (pr_default.getStatus(22) != 101) )
            {
               A74Contrato_Codigo = H00CG34_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00CG34_n74Contrato_Codigo[0];
               A40Contratada_PessoaCod = H00CG34_A40Contratada_PessoaCod[0];
               A43Contratada_Ativo = H00CG34_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG34_n43Contratada_Ativo[0];
               A92Contrato_Ativo = H00CG34_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00CG34_n92Contrato_Ativo[0];
               A638ContratoServicos_Ativo = H00CG34_A638ContratoServicos_Ativo[0];
               A83Contrato_DataVigenciaTermino = H00CG34_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00CG34_n83Contrato_DataVigenciaTermino[0];
               A155Servico_Codigo = H00CG34_A155Servico_Codigo[0];
               A39Contratada_Codigo = H00CG34_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00CG34_n39Contratada_Codigo[0];
               A41Contratada_PessoaNom = H00CG34_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00CG34_n41Contratada_PessoaNom[0];
               A843Contrato_DataFimTA = H00CG34_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00CG34_n843Contrato_DataFimTA[0];
               A92Contrato_Ativo = H00CG34_A92Contrato_Ativo[0];
               n92Contrato_Ativo = H00CG34_n92Contrato_Ativo[0];
               A83Contrato_DataVigenciaTermino = H00CG34_A83Contrato_DataVigenciaTermino[0];
               n83Contrato_DataVigenciaTermino = H00CG34_n83Contrato_DataVigenciaTermino[0];
               A39Contratada_Codigo = H00CG34_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00CG34_n39Contratada_Codigo[0];
               A40Contratada_PessoaCod = H00CG34_A40Contratada_PessoaCod[0];
               A43Contratada_Ativo = H00CG34_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG34_n43Contratada_Ativo[0];
               A41Contratada_PessoaNom = H00CG34_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00CG34_n41Contratada_PessoaNom[0];
               A843Contrato_DataFimTA = H00CG34_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00CG34_n843Contrato_DataFimTA[0];
               if ( ( A843Contrato_DataFimTA >= DateTimeUtil.ServerDate( context, "DEFAULT") ) || ( A83Contrato_DataVigenciaTermino >= DateTimeUtil.ServerDate( context, "DEFAULT") ) )
               {
                  cmbavRequisitado.addItem("A"+StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)), A41Contratada_PessoaNom, 0);
                  AV131Contratadas.Add(A39Contratada_Codigo, 0);
                  AV9Requisitado = "A" + StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
                  AV12RequisitadoCod = A39Contratada_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
                  AV85LerServico = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85LerServico", AV85LerServico);
                  /* Execute user subroutine: 'REQUISITADOSELECIONADO' */
                  S212 ();
                  if ( returnInSub )
                  {
                     pr_default.close(22);
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(22);
            }
            pr_default.close(22);
         }
      }

      protected void S332( )
      {
         /* 'USUARIOSDAREQUISITADA' Routine */
         cmbavUsuario_codigo.removeAllItems();
         cmbavUsuario_codigo.addItem("0", "(Nenhum)", 0);
         if ( StringUtil.StrCmp(StringUtil.Substring( AV9Requisitado, 1, 1), "A") == 0 )
         {
            AV20Contratada_Codigo = AV12RequisitadoCod;
            AV21Contratante_Codigo = 0;
            /* Using cursor H00CG35 */
            pr_default.execute(23, new Object[] {AV12RequisitadoCod});
            while ( (pr_default.getStatus(23) != 101) )
            {
               A70ContratadaUsuario_UsuarioPessoaCod = H00CG35_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00CG35_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A66ContratadaUsuario_ContratadaCod = H00CG35_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = H00CG35_A69ContratadaUsuario_UsuarioCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00CG35_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00CG35_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00CG35_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00CG35_n1394ContratadaUsuario_UsuarioAtivo[0];
               A43Contratada_Ativo = H00CG35_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG35_n43Contratada_Ativo[0];
               A1908Usuario_DeFerias = H00CG35_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00CG35_n1908Usuario_DeFerias[0];
               A43Contratada_Ativo = H00CG35_A43Contratada_Ativo[0];
               n43Contratada_Ativo = H00CG35_n43Contratada_Ativo[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H00CG35_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H00CG35_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A1394ContratadaUsuario_UsuarioAtivo = H00CG35_A1394ContratadaUsuario_UsuarioAtivo[0];
               n1394ContratadaUsuario_UsuarioAtivo = H00CG35_n1394ContratadaUsuario_UsuarioAtivo[0];
               A1908Usuario_DeFerias = H00CG35_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00CG35_n1908Usuario_DeFerias[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H00CG35_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H00CG35_n71ContratadaUsuario_UsuarioPessoaNom[0];
               /* Using cursor H00CG36 */
               pr_default.execute(24, new Object[] {A69ContratadaUsuario_UsuarioCod, AV13Servico_Codigo});
               while ( (pr_default.getStatus(24) != 101) )
               {
                  A828UsuarioServicos_UsuarioCod = H00CG36_A828UsuarioServicos_UsuarioCod[0];
                  A829UsuarioServicos_ServicoCod = H00CG36_A829UsuarioServicos_ServicoCod[0];
                  cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)), A71ContratadaUsuario_UsuarioPessoaNom, 0);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(24);
               pr_default.readNext(23);
            }
            pr_default.close(23);
            GXt_boolean9 = AV165ContratanteSelUsrPrestadora;
            GXt_int10 = AV6WWPContext.gxTpr_Contratante_codigo;
            new prc_contratanteselusrprestadora(context ).execute( ref  GXt_int10, ref  GXt_boolean9) ;
            AV6WWPContext.gxTpr_Contratante_codigo = GXt_int10;
            AV165ContratanteSelUsrPrestadora = GXt_boolean9;
            GXt_boolean9 = AV164SelUsrPrestadora;
            GXt_int10 = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            new prc_selusrprestadora(context ).execute( ref  GXt_int10, ref  GXt_boolean9) ;
            AV6WWPContext.gxTpr_Areatrabalho_codigo = GXt_int10;
            AV164SelUsrPrestadora = GXt_boolean9;
            cmbavUsuario_codigo.Visible = ((AV164SelUsrPrestadora||AV6WWPContext.gxTpr_Userehcontratada||AV165ContratanteSelUsrPrestadora) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_codigo.Visible), 5, 0)));
            lblTextblockusuario_codigo_Visible = ((AV164SelUsrPrestadora||AV6WWPContext.gxTpr_Userehcontratada||AV165ContratanteSelUsrPrestadora) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockusuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockusuario_codigo_Visible), 5, 0)));
         }
         else
         {
            AV20Contratada_Codigo = 0;
            AV21Contratante_Codigo = AV12RequisitadoCod;
            /* Using cursor H00CG37 */
            pr_default.execute(25, new Object[] {AV12RequisitadoCod});
            while ( (pr_default.getStatus(25) != 101) )
            {
               A61ContratanteUsuario_UsuarioPessoaCod = H00CG37_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00CG37_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A63ContratanteUsuario_ContratanteCod = H00CG37_A63ContratanteUsuario_ContratanteCod[0];
               A1908Usuario_DeFerias = H00CG37_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00CG37_n1908Usuario_DeFerias[0];
               A54Usuario_Ativo = H00CG37_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00CG37_n54Usuario_Ativo[0];
               A60ContratanteUsuario_UsuarioCod = H00CG37_A60ContratanteUsuario_UsuarioCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00CG37_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00CG37_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00CG37_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00CG37_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A1908Usuario_DeFerias = H00CG37_A1908Usuario_DeFerias[0];
               n1908Usuario_DeFerias = H00CG37_n1908Usuario_DeFerias[0];
               A54Usuario_Ativo = H00CG37_A54Usuario_Ativo[0];
               n54Usuario_Ativo = H00CG37_n54Usuario_Ativo[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00CG37_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00CG37_n62ContratanteUsuario_UsuarioPessoaNom[0];
               cmbavUsuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0)), A62ContratanteUsuario_UsuarioPessoaNom, 0);
               pr_default.readNext(25);
            }
            pr_default.close(25);
            cmbavUsuario_codigo.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUsuario_codigo.Visible), 5, 0)));
            lblTextblockusuario_codigo_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockusuario_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockusuario_codigo_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'CARREGAGRUPOSDESERVICOS' Routine */
         cmbavServicogrupo_codigo.removeAllItems();
         cmbavServicogrupo_codigo.addItem("0", "Todos", 0);
         /* Using cursor H00CG38 */
         pr_default.execute(26, new Object[] {AV100AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(26) != 101) )
         {
            A155Servico_Codigo = H00CG38_A155Servico_Codigo[0];
            A74Contrato_Codigo = H00CG38_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00CG38_n74Contrato_Codigo[0];
            A75Contrato_AreaTrabalhoCod = H00CG38_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00CG38_n75Contrato_AreaTrabalhoCod[0];
            A638ContratoServicos_Ativo = H00CG38_A638ContratoServicos_Ativo[0];
            A92Contrato_Ativo = H00CG38_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00CG38_n92Contrato_Ativo[0];
            A157ServicoGrupo_Codigo = H00CG38_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = H00CG38_A158ServicoGrupo_Descricao[0];
            A157ServicoGrupo_Codigo = H00CG38_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = H00CG38_A158ServicoGrupo_Descricao[0];
            A75Contrato_AreaTrabalhoCod = H00CG38_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00CG38_n75Contrato_AreaTrabalhoCod[0];
            A92Contrato_Ativo = H00CG38_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00CG38_n92Contrato_Ativo[0];
            cmbavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)), A158ServicoGrupo_Descricao, 0);
            pr_default.readNext(26);
         }
         pr_default.close(26);
         /* Execute user subroutine: 'CARREGASERVICOS' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S142( )
      {
         /* 'CARREGASERVICOS' Routine */
         AV136Servicos.Clear();
         AV137SDT_Servicos.Clear();
         AV170ServicoCodigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV170ServicoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV170ServicoCodigo), 6, 0)));
         AV171ContratoServicos_UndCntNome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV171ContratoServicos_UndCntNome", AV171ContratoServicos_UndCntNome);
         AV172ContratoServicos_QntUntCns = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV172ContratoServicos_QntUntCns", StringUtil.LTrim( StringUtil.Str( AV172ContratoServicos_QntUntCns, 9, 4)));
         pr_default.dynParam(27, new Object[]{ new Object[]{
                                              AV6WWPContext.gxTpr_Userehcontratante ,
                                              AV134Contratante_UsaOSistema ,
                                              A1079ContratoGestor_UsuarioCod ,
                                              AV6WWPContext.gxTpr_Userid ,
                                              AV102Requisitante ,
                                              A843Contrato_DataFimTA ,
                                              A83Contrato_DataVigenciaTermino ,
                                              A92Contrato_Ativo ,
                                              A43Contratada_Ativo ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1446ContratoGestor_ContratadaAreaCod },
                                              new int[] {
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00CG39 */
         pr_default.execute(27, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Userid, AV102Requisitante});
         while ( (pr_default.getStatus(27) != 101) )
         {
            A1136ContratoGestor_ContratadaCod = H00CG39_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00CG39_n1136ContratoGestor_ContratadaCod[0];
            A1013Contrato_PrepostoCod = H00CG39_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00CG39_n1013Contrato_PrepostoCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H00CG39_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00CG39_n1446ContratoGestor_ContratadaAreaCod[0];
            A1078ContratoGestor_ContratoCod = H00CG39_A1078ContratoGestor_ContratoCod[0];
            A54Usuario_Ativo = H00CG39_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG39_n54Usuario_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00CG39_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG39_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CG39_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG39_n43Contratada_Ativo[0];
            A92Contrato_Ativo = H00CG39_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00CG39_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00CG39_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00CG39_n83Contrato_DataVigenciaTermino[0];
            A1079ContratoGestor_UsuarioCod = H00CG39_A1079ContratoGestor_UsuarioCod[0];
            A1136ContratoGestor_ContratadaCod = H00CG39_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = H00CG39_n1136ContratoGestor_ContratadaCod[0];
            A1013Contrato_PrepostoCod = H00CG39_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00CG39_n1013Contrato_PrepostoCod[0];
            A1446ContratoGestor_ContratadaAreaCod = H00CG39_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = H00CG39_n1446ContratoGestor_ContratadaAreaCod[0];
            A92Contrato_Ativo = H00CG39_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00CG39_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00CG39_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00CG39_n83Contrato_DataVigenciaTermino[0];
            A52Contratada_AreaTrabalhoCod = H00CG39_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG39_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CG39_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG39_n43Contratada_Ativo[0];
            A54Usuario_Ativo = H00CG39_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG39_n54Usuario_Ativo[0];
            /* Using cursor H00CG40 */
            pr_default.execute(28, new Object[] {A1078ContratoGestor_ContratoCod, AV16ServicoGrupo_Codigo});
            while ( (pr_default.getStatus(28) != 101) )
            {
               A74Contrato_Codigo = H00CG40_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00CG40_n74Contrato_Codigo[0];
               A157ServicoGrupo_Codigo = H00CG40_A157ServicoGrupo_Codigo[0];
               A638ContratoServicos_Ativo = H00CG40_A638ContratoServicos_Ativo[0];
               A155Servico_Codigo = H00CG40_A155Servico_Codigo[0];
               A157ServicoGrupo_Codigo = H00CG40_A157ServicoGrupo_Codigo[0];
               AV170ServicoCodigo = A155Servico_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV170ServicoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV170ServicoCodigo), 6, 0)));
               /* Execute user subroutine: 'BUSCA.IDENTIFICACAO.SERVICO' */
               S3626 ();
               if ( returnInSub )
               {
                  pr_default.close(28);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(28);
            }
            pr_default.close(28);
            pr_default.readNext(27);
         }
         pr_default.close(27);
         pr_default.dynParam(29, new Object[]{ new Object[]{
                                              AV6WWPContext.gxTpr_Userehcontratante ,
                                              AV134Contratante_UsaOSistema ,
                                              A1825ContratoAuxiliar_UsuarioCod ,
                                              AV6WWPContext.gxTpr_Userid ,
                                              AV102Requisitante ,
                                              A843Contrato_DataFimTA ,
                                              A83Contrato_DataVigenciaTermino ,
                                              A92Contrato_Ativo ,
                                              A43Contratada_Ativo ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A75Contrato_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00CG41 */
         pr_default.execute(29, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Userid, AV102Requisitante});
         while ( (pr_default.getStatus(29) != 101) )
         {
            A1013Contrato_PrepostoCod = H00CG41_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00CG41_n1013Contrato_PrepostoCod[0];
            A39Contratada_Codigo = H00CG41_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00CG41_n39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = H00CG41_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00CG41_n75Contrato_AreaTrabalhoCod[0];
            A1824ContratoAuxiliar_ContratoCod = H00CG41_A1824ContratoAuxiliar_ContratoCod[0];
            A52Contratada_AreaTrabalhoCod = H00CG41_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG41_n52Contratada_AreaTrabalhoCod[0];
            A54Usuario_Ativo = H00CG41_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG41_n54Usuario_Ativo[0];
            A43Contratada_Ativo = H00CG41_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG41_n43Contratada_Ativo[0];
            A92Contrato_Ativo = H00CG41_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00CG41_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00CG41_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00CG41_n83Contrato_DataVigenciaTermino[0];
            A1825ContratoAuxiliar_UsuarioCod = H00CG41_A1825ContratoAuxiliar_UsuarioCod[0];
            A1013Contrato_PrepostoCod = H00CG41_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00CG41_n1013Contrato_PrepostoCod[0];
            A39Contratada_Codigo = H00CG41_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00CG41_n39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = H00CG41_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = H00CG41_n75Contrato_AreaTrabalhoCod[0];
            A92Contrato_Ativo = H00CG41_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00CG41_n92Contrato_Ativo[0];
            A83Contrato_DataVigenciaTermino = H00CG41_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00CG41_n83Contrato_DataVigenciaTermino[0];
            A54Usuario_Ativo = H00CG41_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG41_n54Usuario_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00CG41_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG41_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CG41_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG41_n43Contratada_Ativo[0];
            /* Using cursor H00CG42 */
            pr_default.execute(30, new Object[] {A1824ContratoAuxiliar_ContratoCod, AV16ServicoGrupo_Codigo});
            while ( (pr_default.getStatus(30) != 101) )
            {
               A74Contrato_Codigo = H00CG42_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00CG42_n74Contrato_Codigo[0];
               A157ServicoGrupo_Codigo = H00CG42_A157ServicoGrupo_Codigo[0];
               A638ContratoServicos_Ativo = H00CG42_A638ContratoServicos_Ativo[0];
               A155Servico_Codigo = H00CG42_A155Servico_Codigo[0];
               A157ServicoGrupo_Codigo = H00CG42_A157ServicoGrupo_Codigo[0];
               AV170ServicoCodigo = A155Servico_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV170ServicoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV170ServicoCodigo), 6, 0)));
               /* Execute user subroutine: 'BUSCA.IDENTIFICACAO.SERVICO' */
               S3626 ();
               if ( returnInSub )
               {
                  pr_default.close(30);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(30);
            }
            pr_default.close(30);
            pr_default.readNext(29);
         }
         pr_default.close(29);
         AV137SDT_Servicos.Sort("Descricao");
         AV13Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
         cmbavServico_codigo.addItem("0", "(Nenhum)", 0);
         AV210GXV2 = 1;
         while ( AV210GXV2 <= AV137SDT_Servicos.Count )
         {
            AV138SDT_Servico = ((SdtSDT_Codigos)AV137SDT_Servicos.Item(AV210GXV2));
            cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV138SDT_Servico.gxTpr_Codigo), 6, 0)), AV138SDT_Servico.gxTpr_Descricao, 0);
            AV210GXV2 = (int)(AV210GXV2+1);
         }
      }

      protected void S3626( )
      {
         /* 'BUSCA.IDENTIFICACAO.SERVICO' Routine */
         pr_default.dynParam(31, new Object[]{ new Object[]{
                                              A155Servico_Codigo ,
                                              AV136Servicos ,
                                              AV170ServicoCodigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00CG43 */
         pr_default.execute(31, new Object[] {AV170ServicoCodigo});
         while ( (pr_default.getStatus(31) != 101) )
         {
            A155Servico_Codigo = H00CG43_A155Servico_Codigo[0];
            A608Servico_Nome = H00CG43_A608Servico_Nome[0];
            A605Servico_Sigla = H00CG43_A605Servico_Sigla[0];
            AV136Servicos.Add(A155Servico_Codigo, 0);
            AV138SDT_Servico = new SdtSDT_Codigos(context);
            AV138SDT_Servico.gxTpr_Codigo = A155Servico_Codigo;
            AV138SDT_Servico.gxTpr_Descricao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            AV137SDT_Servicos.Add(AV138SDT_Servico, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(31);
      }

      protected void S192( )
      {
         /* 'CARREGASISTEMAS' Routine */
         cmbavSistema_codigo.removeAllItems();
         cmbavSistema_codigo.addItem("0", "(Nenhum)", 0);
         pr_default.dynParam(32, new Object[]{ new Object[]{
                                              A127Sistema_Codigo ,
                                              AV51Sistemas ,
                                              AV51Sistemas.Count ,
                                              A130Sistema_Ativo ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A699Sistema_Tipo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00CG44 */
         pr_default.execute(32, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(32) != 101) )
         {
            A127Sistema_Codigo = H00CG44_A127Sistema_Codigo[0];
            A130Sistema_Ativo = H00CG44_A130Sistema_Ativo[0];
            A699Sistema_Tipo = H00CG44_A699Sistema_Tipo[0];
            n699Sistema_Tipo = H00CG44_n699Sistema_Tipo[0];
            A135Sistema_AreaTrabalhoCod = H00CG44_A135Sistema_AreaTrabalhoCod[0];
            A129Sistema_Sigla = H00CG44_A129Sistema_Sigla[0];
            cmbavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)), A129Sistema_Sigla, 0);
            pr_default.readNext(32);
         }
         pr_default.close(32);
      }

      protected void S322( )
      {
         /* 'CARREGACONTRATOS' Routine */
         cmbavContrato.removeAllItems();
         cmbavContrato.addItem("0", "(Nenhum)", 0);
         /* Using cursor H00CG47 */
         pr_default.execute(33, new Object[] {AV12RequisitadoCod, AV8ContagemResultado_DataDmn});
         while ( (pr_default.getStatus(33) != 101) )
         {
            A1013Contrato_PrepostoCod = H00CG47_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00CG47_n1013Contrato_PrepostoCod[0];
            A77Contrato_Numero = H00CG47_A77Contrato_Numero[0];
            A74Contrato_Codigo = H00CG47_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00CG47_n74Contrato_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00CG47_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG47_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CG47_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG47_n43Contratada_Ativo[0];
            A54Usuario_Ativo = H00CG47_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG47_n54Usuario_Ativo[0];
            A92Contrato_Ativo = H00CG47_A92Contrato_Ativo[0];
            n92Contrato_Ativo = H00CG47_n92Contrato_Ativo[0];
            A39Contratada_Codigo = H00CG47_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00CG47_n39Contratada_Codigo[0];
            A843Contrato_DataFimTA = H00CG47_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00CG47_n843Contrato_DataFimTA[0];
            A83Contrato_DataVigenciaTermino = H00CG47_A83Contrato_DataVigenciaTermino[0];
            n83Contrato_DataVigenciaTermino = H00CG47_n83Contrato_DataVigenciaTermino[0];
            A54Usuario_Ativo = H00CG47_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00CG47_n54Usuario_Ativo[0];
            A843Contrato_DataFimTA = H00CG47_A843Contrato_DataFimTA[0];
            n843Contrato_DataFimTA = H00CG47_n843Contrato_DataFimTA[0];
            A52Contratada_AreaTrabalhoCod = H00CG47_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG47_n52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00CG47_A43Contratada_Ativo[0];
            n43Contratada_Ativo = H00CG47_n43Contratada_Ativo[0];
            A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1869Contrato_DataTermino", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
            /* Using cursor H00CG48 */
            pr_default.execute(34, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo, AV13Servico_Codigo});
            while ( (pr_default.getStatus(34) != 101) )
            {
               A155Servico_Codigo = H00CG48_A155Servico_Codigo[0];
               AV139ContratoVencido = (bool)((AV8ContagemResultado_DataDmn>A1869Contrato_DataTermino));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV139ContratoVencido", AV139ContratoVencido);
               cmbavContrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)), A77Contrato_Numero, 0);
               AV117Contrato = A74Contrato_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)));
               pr_default.readNext(34);
            }
            pr_default.close(34);
            pr_default.readNext(33);
         }
         pr_default.close(33);
         if ( cmbavContrato.ItemCount > 2 )
         {
            AV117Contrato = AV155Contrato_Padrao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)));
            lblTextblockcontrato_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_Visible), 5, 0)));
            cmbavContrato.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContrato.Visible), 5, 0)));
         }
         else
         {
            lblTextblockcontrato_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontrato_Visible), 5, 0)));
            cmbavContrato.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContrato.Visible), 5, 0)));
            if ( new prc_naotempreposto(context).executeUdp(  0,  AV117Contrato) )
            {
               Confirmpanel2_Confirmationtext = "A "+AV162Nome+" n�o tem Preposto estabelecido! A OS n�o poder ser solicitada.";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
               Confirmpanel2_Title = "Prestadora";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "Title", Confirmpanel2_Title);
               this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
            }
            if ( AV139ContratoVencido )
            {
               Confirmpanel2_Confirmationtext = "O Contrato "+cmbavContrato.Description+" do servi�o informado esta vencido!";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "ConfirmationText", Confirmpanel2_Confirmationtext);
               Confirmpanel2_Title = "Prestadora";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel2_Internalname, "Title", Confirmpanel2_Title);
               this.executeUsercontrolMethod("", false, "CONFIRMPANEL2Container", "Confirm", "", new Object[] {});
            }
         }
      }

      protected void S302( )
      {
         /* 'PRAZOANALISE' Routine */
         GXt_char7 = "07 - PrazoAnalise";
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "08 - PrazoAnalise";
         new geralog(context ).execute( ref  GXt_char6) ;
         AV63EntregaCalculada = DateTimeUtil.ResetTime( AV8ContagemResultado_DataDmn ) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         GXt_char7 = "09 - &ContagemResultado_DataDmn = " + context.localUtil.Format( AV8ContagemResultado_DataDmn, "99/99/99");
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "10 - &EntregaCalculada 		  = " + context.localUtil.Format( AV63EntregaCalculada, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "11 - &PrazoInicio 		      = " + context.localUtil.Format( (decimal)(AV122PrazoInicio), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char5) ;
         if ( AV122PrazoInicio == 2 )
         {
            GXt_char7 = "12 - entrou aqui";
            new geralog(context ).execute( ref  GXt_char7) ;
            new prc_datadeinicioutil(context ).execute( ref  AV63EntregaCalculada) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         }
         GXt_char6 = "13- &&EntregaCalculada = " + context.localUtil.Format( AV63EntregaCalculada, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "14 - &&DiasAnalise = " + context.localUtil.Format( (decimal)(AV98DiasAnalise), "ZZZ9");
         new geralog(context ).execute( ref  GXt_char5) ;
         GXt_char4 = "15 - &&TipoDias = " + gxdomaintipodias.getDescription(context,AV115TipoDias);
         new geralog(context ).execute( ref  GXt_char4) ;
         GXt_dtime1 = AV70PrazoAnalise;
         new prc_adddiasuteis(context ).execute(  AV63EntregaCalculada,  AV98DiasAnalise,  AV115TipoDias, out  GXt_dtime1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63EntregaCalculada", context.localUtil.TToC( AV63EntregaCalculada, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98DiasAnalise", StringUtil.LTrim( StringUtil.Str( (decimal)(AV98DiasAnalise), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115TipoDias", AV115TipoDias);
         AV70PrazoAnalise = GXt_dtime1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PrazoAnalise", context.localUtil.TToC( AV70PrazoAnalise, 8, 5, 0, 3, "/", ":", " "));
         GXt_char7 = "16 - &&PrazoAnalise = " + context.localUtil.Format( AV70PrazoAnalise, "99/99/99 99:99");
         new geralog(context ).execute( ref  GXt_char7) ;
         GXt_char6 = "17 - PrazoAnalise";
         new geralog(context ).execute( ref  GXt_char6) ;
         GXt_char5 = "18 - PrazoAnalise";
         new geralog(context ).execute( ref  GXt_char5) ;
      }

      protected void S272( )
      {
         /* 'CHECKSALDOCONTRATO' Routine */
         GXt_decimal11 = AV121SaldoContrato_Saldo;
         new prc_contratosaldo(context ).execute(  AV117Contrato, out  GXt_decimal11) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)));
         AV121SaldoContrato_Saldo = GXt_decimal11;
         if ( (Convert.ToDecimal(0)==AV121SaldoContrato_Saldo) )
         {
            GX_msglist.addItem("Saldo vigente do Contrato insuficiente para cria��o da OS ");
            AV17CheckRequiredFieldsResult = false;
         }
      }

      protected void S132( )
      {
         /* 'EXTERNALCALL' Routine */
         /* Using cursor H00CG50 */
         pr_default.execute(35, new Object[] {AV23ContagemResultado_OSVinculada});
         while ( (pr_default.getStatus(35) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = H00CG50_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00CG50_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = H00CG50_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00CG50_n601ContagemResultado_Servico[0];
            A456ContagemResultado_Codigo = H00CG50_A456ContagemResultado_Codigo[0];
            A1636ContagemResultado_ServicoSS = H00CG50_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = H00CG50_n1636ContagemResultado_ServicoSS[0];
            A1452ContagemResultado_SS = H00CG50_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = H00CG50_n1452ContagemResultado_SS[0];
            A1637ContagemResultado_ServicoSSSgl = H00CG50_A1637ContagemResultado_ServicoSSSgl[0];
            n1637ContagemResultado_ServicoSSSgl = H00CG50_n1637ContagemResultado_ServicoSSSgl[0];
            A493ContagemResultado_DemandaFM = H00CG50_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00CG50_n493ContagemResultado_DemandaFM[0];
            A803ContagemResultado_ContratadaSigla = H00CG50_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00CG50_n803ContagemResultado_ContratadaSigla[0];
            A801ContagemResultado_ServicoSigla = H00CG50_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00CG50_n801ContagemResultado_ServicoSigla[0];
            A489ContagemResultado_SistemaCod = H00CG50_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00CG50_n489ContagemResultado_SistemaCod[0];
            A804ContagemResultado_SistemaAtivo = H00CG50_A804ContagemResultado_SistemaAtivo[0];
            n804ContagemResultado_SistemaAtivo = H00CG50_n804ContagemResultado_SistemaAtivo[0];
            A146Modulo_Codigo = H00CG50_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00CG50_n146Modulo_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00CG50_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00CG50_n490ContagemResultado_ContratadaCod[0];
            A1044ContagemResultado_FncUsrCod = H00CG50_A1044ContagemResultado_FncUsrCod[0];
            n1044ContagemResultado_FncUsrCod = H00CG50_n1044ContagemResultado_FncUsrCod[0];
            A1326ContagemResultado_ContratadaTipoFab = H00CG50_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00CG50_n1326ContagemResultado_ContratadaTipoFab[0];
            A798ContagemResultado_PFBFSImp = H00CG50_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = H00CG50_n798ContagemResultado_PFBFSImp[0];
            A799ContagemResultado_PFLFSImp = H00CG50_A799ContagemResultado_PFLFSImp[0];
            n799ContagemResultado_PFLFSImp = H00CG50_n799ContagemResultado_PFLFSImp[0];
            A584ContagemResultado_ContadorFM = H00CG50_A584ContagemResultado_ContadorFM[0];
            A682ContagemResultado_PFBFMUltima = H00CG50_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00CG50_A683ContagemResultado_PFLFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00CG50_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00CG50_A685ContagemResultado_PFLFSUltima[0];
            A601ContagemResultado_Servico = H00CG50_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00CG50_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00CG50_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00CG50_n801ContagemResultado_ServicoSigla[0];
            A584ContagemResultado_ContadorFM = H00CG50_A584ContagemResultado_ContadorFM[0];
            A682ContagemResultado_PFBFMUltima = H00CG50_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00CG50_A683ContagemResultado_PFLFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00CG50_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00CG50_A685ContagemResultado_PFLFSUltima[0];
            A1637ContagemResultado_ServicoSSSgl = H00CG50_A1637ContagemResultado_ServicoSSSgl[0];
            n1637ContagemResultado_ServicoSSSgl = H00CG50_n1637ContagemResultado_ServicoSSSgl[0];
            A804ContagemResultado_SistemaAtivo = H00CG50_A804ContagemResultado_SistemaAtivo[0];
            n804ContagemResultado_SistemaAtivo = H00CG50_n804ContagemResultado_SistemaAtivo[0];
            A803ContagemResultado_ContratadaSigla = H00CG50_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00CG50_n803ContagemResultado_ContratadaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = H00CG50_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00CG50_n1326ContagemResultado_ContratadaTipoFab[0];
            if ( A1636ContagemResultado_ServicoSS > 0 )
            {
               AV22ContagemResultado_DmnVinculada = "SS " + StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
               cmbavDmnvinculadas.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)), AV22ContagemResultado_DmnVinculada, 0);
               AV60ServicoVnc_Sigla = A1637ContagemResultado_ServicoSSSgl;
            }
            else
            {
               AV22ContagemResultado_DmnVinculada = A493ContagemResultado_DemandaFM;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_DmnVinculada", AV22ContagemResultado_DmnVinculada);
               cmbavDmnvinculadas.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)), A493ContagemResultado_DemandaFM+" de "+StringUtil.Trim( A801ContagemResultado_ServicoSigla)+" da "+A803ContagemResultado_ContratadaSigla, 0);
               AV60ServicoVnc_Sigla = A801ContagemResultado_ServicoSigla;
            }
            AV123DmnVinculadas = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123DmnVinculadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)));
            AV38Sistema_Codigo = A489ContagemResultado_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
            AV130SistemaAtivo = A804ContagemResultado_SistemaAtivo;
            AV39Modulo_Codigo = A146Modulo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)));
            AV105ContratadaOrigem = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105ContratadaOrigem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)));
            AV107ContadorFS = A584ContagemResultado_ContadorFM;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV107ContadorFS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)));
            AV41FuncaoUsuario_Codigo = A1044ContagemResultado_FncUsrCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)));
            if ( StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M") == 0 )
            {
               AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
               AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
            }
            else
            {
               if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
               {
                  AV66PFBFSdaOSVnc = A684ContagemResultado_PFBFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = A685ContagemResultado_PFLFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
               }
               else if ( ( A798ContagemResultado_PFBFSImp > Convert.ToDecimal( 0 )) )
               {
                  AV66PFBFSdaOSVnc = A798ContagemResultado_PFBFSImp;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = A799ContagemResultado_PFLFSImp;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
               }
               else
               {
                  AV66PFBFSdaOSVnc = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PFBFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV66PFBFSdaOSVnc, 14, 5)));
                  AV73PFLFSdaOSVnc = A683ContagemResultado_PFLFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PFLFSdaOSVnc", StringUtil.LTrim( StringUtil.Str( AV73PFLFSdaOSVnc, 14, 5)));
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(35);
         edtavContagemresultado_dmnvinculada_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculada_Visible), 5, 0)));
         edtavContagemresultado_dmnvinculadaref_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_dmnvinculadaref_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_dmnvinculadaref_Visible), 5, 0)));
         lblTextblockpfbfsdaosvnc_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpfbfsdaosvnc_Visible), 5, 0)));
         lblTextblockpflfsdaosvnc_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockpflfsdaosvnc_Visible), 5, 0)));
         edtavPfbfsdaosvnc_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Visible), 5, 0)));
         edtavPflfsdaosvnc_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Visible), 5, 0)));
         edtavPfbfsdaosvnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Enabled), 5, 0)));
         edtavPflfsdaosvnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Enabled), 5, 0)));
         cmbavDmnvinculadas.Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDmnvinculadas.Visible), 5, 0)));
         cmbavDmnvinculadas.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDmnvinculadas.Enabled), 5, 0)));
         edtavPfbfsdaosvnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfsdaosvnc_Enabled), 5, 0)));
         edtavPflfsdaosvnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPflfsdaosvnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfsdaosvnc_Enabled), 5, 0)));
      }

      protected void S312( )
      {
         /* 'TEMARTEFATOS' Routine */
         AV128sdt_Artefatos.Clear();
         /* Using cursor H00CG51 */
         pr_default.execute(36, new Object[] {AV49ContratoServicos_Codigo});
         while ( (pr_default.getStatus(36) != 101) )
         {
            A160ContratoServicos_Codigo = H00CG51_A160ContratoServicos_Codigo[0];
            A1749Artefatos_Codigo = H00CG51_A1749Artefatos_Codigo[0];
            A1751Artefatos_Descricao = H00CG51_A1751Artefatos_Descricao[0];
            A1751Artefatos_Descricao = H00CG51_A1751Artefatos_Descricao[0];
            AV127sdt_Artefato = new SdtSDT_Artefatos(context);
            AV127sdt_Artefato.gxTpr_Artefatos_codigo = A1749Artefatos_Codigo;
            AV127sdt_Artefato.gxTpr_Artefatos_descricao = A1751Artefatos_Descricao;
            AV128sdt_Artefatos.Add(AV127sdt_Artefato, 0);
            pr_default.readNext(36);
         }
         pr_default.close(36);
         AV129ServicoTemArtefatos = (bool)(((AV128sdt_Artefatos.Count>0)));
         bttBtnartefatos_Visible = (AV129ServicoTemArtefatos ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnartefatos_Visible), 5, 0)));
         bttBtnconfirmar_Visible = (!AV129ServicoTemArtefatos&&AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirmar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirmar_Visible), 5, 0)));
         bttBtnartefatos_Caption = "Artefatos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV128sdt_Artefatos.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Caption", bttBtnartefatos_Caption);
         AV29Websession.Set("Artefatos", AV128sdt_Artefatos.ToXml(false, true, "SDT_ArtefatosCollection", "GxEv3Up14_Meetrika"));
      }

      protected void S242( )
      {
         /* 'NAOTEMARTEFATOS' Routine */
         AV128sdt_Artefatos.Clear();
         AV129ServicoTemArtefatos = false;
         bttBtnartefatos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnartefatos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnartefatos_Visible), 5, 0)));
         bttBtnconfirmar_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirmar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirmar_Visible), 5, 0)));
         AV29Websession.Remove("Artefatos");
      }

      protected void S152( )
      {
         /* 'SOLICITACAOARTEFATOS' Routine */
         context.PopUp(formatLink("wp_solicitacaoartefatos.aspx") + "?" + UrlEncode("" +AV49ContratoServicos_Codigo), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void S172( )
      {
         /* 'CONTRATADASDAAREA' Routine */
         /* Using cursor H00CG52 */
         pr_default.execute(37, new Object[] {AV100AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(37) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = H00CG52_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00CG52_n52Contratada_AreaTrabalhoCod[0];
            A39Contratada_Codigo = H00CG52_A39Contratada_Codigo[0];
            n39Contratada_Codigo = H00CG52_n39Contratada_Codigo[0];
            AV132ContratadasDaArea.Add(A39Contratada_Codigo, 0);
            pr_default.readNext(37);
         }
         pr_default.close(37);
      }

      protected void S122( )
      {
         /* 'DADOSDOCALLER' Routine */
         AV12RequisitadoCod = (int)(NumberUtil.Val( AV29Websession.Get("Contratada"), "."));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12RequisitadoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)));
         if ( (0==AV12RequisitadoCod) )
         {
            AV125Caller = "CxEnt";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125Caller", AV125Caller);
            bttBtn_cancel_Jsonclick = "self.close()";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Jsonclick", bttBtn_cancel_Jsonclick);
         }
         else
         {
            AV125Caller = "SS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125Caller", AV125Caller);
            AV9Requisitado = "A" + StringUtil.Trim( AV29Websession.Get("Contratada"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
            /* Using cursor H00CG53 */
            pr_default.execute(38, new Object[] {AV12RequisitadoCod});
            while ( (pr_default.getStatus(38) != 101) )
            {
               A40Contratada_PessoaCod = H00CG53_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = H00CG53_A39Contratada_Codigo[0];
               n39Contratada_Codigo = H00CG53_n39Contratada_Codigo[0];
               A41Contratada_PessoaNom = H00CG53_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00CG53_n41Contratada_PessoaNom[0];
               A41Contratada_PessoaNom = H00CG53_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00CG53_n41Contratada_PessoaNom[0];
               cmbavRequisitado.addItem("A"+StringUtil.Trim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0)), A41Contratada_PessoaNom, 0);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(38);
            AV9Requisitado = "A" + StringUtil.Trim( StringUtil.Str( (decimal)(AV12RequisitadoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Requisitado", AV9Requisitado);
            AV29Websession.Remove("Contratada");
            AV49ContratoServicos_Codigo = (int)(NumberUtil.Val( AV29Websession.Get("Servico"), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49ContratoServicos_Codigo), 6, 0)));
            cmbavServicogrupo_codigo.removeAllItems();
            /* Using cursor H00CG54 */
            pr_default.execute(39, new Object[] {AV49ContratoServicos_Codigo});
            while ( (pr_default.getStatus(39) != 101) )
            {
               A160ContratoServicos_Codigo = H00CG54_A160ContratoServicos_Codigo[0];
               A158ServicoGrupo_Descricao = H00CG54_A158ServicoGrupo_Descricao[0];
               A157ServicoGrupo_Codigo = H00CG54_A157ServicoGrupo_Codigo[0];
               A608Servico_Nome = H00CG54_A608Servico_Nome[0];
               A605Servico_Sigla = H00CG54_A605Servico_Sigla[0];
               A155Servico_Codigo = H00CG54_A155Servico_Codigo[0];
               A157ServicoGrupo_Codigo = H00CG54_A157ServicoGrupo_Codigo[0];
               A608Servico_Nome = H00CG54_A608Servico_Nome[0];
               A605Servico_Sigla = H00CG54_A605Servico_Sigla[0];
               A158ServicoGrupo_Descricao = H00CG54_A158ServicoGrupo_Descricao[0];
               cmbavServicogrupo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)), A158ServicoGrupo_Descricao, 0);
               AV16ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
               cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), StringUtil.Concat( A605Servico_Sigla, A608Servico_Nome, " - "), 0);
               AV13Servico_Codigo = A155Servico_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(39);
            AV29Websession.Remove("Servico");
            cmbavSistema_codigo.removeAllItems();
            /* Using cursor H00CG55 */
            pr_default.execute(40, new Object[] {AV23ContagemResultado_OSVinculada});
            while ( (pr_default.getStatus(40) != 101) )
            {
               A456ContagemResultado_Codigo = H00CG55_A456ContagemResultado_Codigo[0];
               A494ContagemResultado_Descricao = H00CG55_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = H00CG55_n494ContagemResultado_Descricao[0];
               A509ContagemrResultado_SistemaSigla = H00CG55_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00CG55_n509ContagemrResultado_SistemaSigla[0];
               A489ContagemResultado_SistemaCod = H00CG55_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00CG55_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = H00CG55_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00CG55_n509ContagemrResultado_SistemaSigla[0];
               AV88ContagemResultado_Descricao = A494ContagemResultado_Descricao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ContagemResultado_Descricao", AV88ContagemResultado_Descricao);
               cmbavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)), A509ContagemrResultado_SistemaSigla, 0);
               AV38Sistema_Codigo = A489ContagemResultado_SistemaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(40);
            /* Execute user subroutine: 'CARREGACONTRATOS' */
            S322 ();
            if (returnInSub) return;
            /* Execute user subroutine: 'DADOSDOSERVICO' */
            S222 ();
            if (returnInSub) return;
         }
      }

      protected void S282( )
      {
         /* 'SETREQUISITOS' Routine */
         if ( (0==AV161SDT_Requisitos.Count) )
         {
            AV160SDT_RequisitosItem = new SdtSDT_Requisitos_SDT_RequisitosItem(context);
            AV160SDT_RequisitosItem.gxTpr_Requisito_identificador = "1";
            AV160SDT_RequisitosItem.gxTpr_Requisito_titulo = AV88ContagemResultado_Descricao;
            AV160SDT_RequisitosItem.gxTpr_Requisito_descricao = AV19ContagemResultado_Observacao;
            AV160SDT_RequisitosItem.gxTpr_Requisito_ordem = 1;
            AV160SDT_RequisitosItem.gxTpr_Requisito_prioridade = 5;
            AV160SDT_RequisitosItem.gxTpr_Requisito_status = 1;
            AV160SDT_RequisitosItem.gxTpr_Requisito_tipo = 1;
            AV161SDT_Requisitos.Add(AV160SDT_RequisitosItem, 0);
            gx_BV219 = true;
         }
         AV29Websession.Set("Requisitos", AV161SDT_Requisitos.ToXml(false, true, "SDT_Requisitos", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Nova Ordem de Servi�o", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblultimaos_Internalname, lblLblultimaos_Caption, lblLblultimaos_Link, "", lblLblultimaos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_14_CG2( true) ;
         }
         else
         {
            wb_table2_14_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table2_14_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_240_CG2( true) ;
         }
         else
         {
            wb_table3_240_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table3_240_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CG2e( true) ;
         }
         else
         {
            wb_table1_2_CG2e( false) ;
         }
      }

      protected void wb_table3_240_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableaction_Internalname, tblTableaction_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 243,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnartefatos_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(219), 3, 0)+","+"null"+");", bttBtnartefatos_Caption, bttBtnartefatos_Jsonclick, 5, "Artefatos", "", StyleString, ClassString, bttBtnartefatos_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOARTEFATOS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 245,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirmar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(219), 3, 0)+","+"null"+");", "Solicitar", bttBtnconfirmar_Jsonclick, 5, bttBtnconfirmar_Tooltiptext, "", StyleString, ClassString, bttBtnconfirmar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOCONFIRMAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 247,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(219), 3, 0)+","+"null"+");", "Fechar", bttBtn_cancel_Jsonclick, 1, "Voltar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_240_CG2e( true) ;
         }
         else
         {
            wb_table3_240_CG2e( false) ;
         }
      }

      protected void wb_table2_14_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleSolicitacao"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Solicita��o") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"Solicitacao"+"\" style=\"display:none;\">") ;
            wb_table4_20_CG2( true) ;
         }
         else
         {
            wb_table4_20_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table4_20_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleDescricao"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Descri��o complementar") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"Descricao"+"\" style=\"display:none;\">") ;
            wb_table5_210_CG2( true) ;
         }
         else
         {
            wb_table5_210_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table5_210_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TitleTabRequisitos"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "&nbsp;") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABContainer"+"TabRequisitos"+"\" style=\"display:none;\">") ;
            wb_table6_216_CG2( true) ;
         }
         else
         {
            wb_table6_216_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table6_216_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table7_228_CG2( true) ;
         }
         else
         {
            wb_table7_228_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table7_228_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_14_CG2e( true) ;
         }
         else
         {
            wb_table2_14_CG2e( false) ;
         }
      }

      protected void wb_table7_228_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(40), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUsrtable_Internalname, tblUsrtable_Internalname, "", "Table", 0, "center", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody></tbody>") ;
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_OS.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVELOP_BOOTSTRAP_CONFIRMPANEL1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVELOP_BOOTSTRAP_CONFIRMPANEL1Container"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANEL2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANEL2Container"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 236,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_atende, cmbavServico_atende_Internalname, StringUtil.RTrim( AV109Servico_Atende), 1, cmbavServico_atende_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavServico_atende.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,236);\"", "", true, "HLP_WP_OS.htm");
            cmbavServico_atende.CurrentValue = StringUtil.RTrim( AV109Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_atende_Internalname, "Values", (String)(cmbavServico_atende.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 237,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_obrigavalores, cmbavServico_obrigavalores_Internalname, StringUtil.RTrim( AV110Servico_ObrigaValores), 1, cmbavServico_obrigavalores_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavServico_obrigavalores.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,237);\"", "", true, "HLP_WP_OS.htm");
            cmbavServico_obrigavalores.CurrentValue = StringUtil.RTrim( AV110Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_obrigavalores_Internalname, "Values", (String)(cmbavServico_obrigavalores.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_228_CG2e( true) ;
         }
         else
         {
            wb_table7_228_CG2e( false) ;
         }
      }

      protected void wb_table6_216_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridrequisitosContainer.SetWrapped(nGXWrapped);
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridrequisitosContainer"+"DivS\" data-gxgridid=\"219\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridrequisitos_Internalname, subGridrequisitos_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridrequisitos_Backcolorstyle == 0 )
               {
                  subGridrequisitos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridrequisitos_Class) > 0 )
                  {
                     subGridrequisitos_Linesclass = subGridrequisitos_Class+"Title";
                  }
               }
               else
               {
                  subGridrequisitos_Titlebackstyle = 1;
                  if ( subGridrequisitos_Backcolorstyle == 1 )
                  {
                     subGridrequisitos_Titlebackcolor = subGridrequisitos_Allbackcolor;
                     if ( StringUtil.Len( subGridrequisitos_Class) > 0 )
                     {
                        subGridrequisitos_Linesclass = subGridrequisitos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridrequisitos_Class) > 0 )
                     {
                        subGridrequisitos_Linesclass = subGridrequisitos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Agrupador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(100), 4, 0))+"px"+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Identificador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(150), 4, 0))+"px"+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prioridade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Situa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridrequisitosContainer.AddObjectProperty("GridName", "Gridrequisitos");
            }
            else
            {
               GridrequisitosContainer.AddObjectProperty("GridName", "Gridrequisitos");
               GridrequisitosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridrequisitosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Backcolorstyle), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("CmpContext", "");
               GridrequisitosContainer.AddObjectProperty("InMasterPage", "false");
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", context.convertURL( AV147btnExcluirReqNeg));
               GridrequisitosColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnexcluirreqneg_Tooltiptext));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", AV146Agrupador);
               GridrequisitosColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAgrupador_Forecolor), 9, 0, ".", "")));
               GridrequisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAgrupador_Enabled), 5, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_identificador_Enabled), 5, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSdt_requisitos__requisito_titulo_Enabled), 5, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavSdt_requisitos__requisito_prioridade.Enabled), 5, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavSdt_requisitos__requisito_status.Enabled), 5, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Allowselection), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Selectioncolor), 9, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Allowhovering), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Hoveringcolor), 9, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Allowcollapsing), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 219 )
         {
            wbEnd = 0;
            nRC_GXsfl_219 = (short)(nGXsfl_219_idx-1);
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridrequisitosContainer.AddObjectProperty("GRIDREQUISITOS_nEOF", GRIDREQUISITOS_nEOF);
               GridrequisitosContainer.AddObjectProperty("GRIDREQUISITOS_nFirstRecordOnPage", GRIDREQUISITOS_nFirstRecordOnPage);
               AV176GXV1 = nGXsfl_219_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridrequisitosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridrequisitos", GridrequisitosContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrequisitosContainerData", GridrequisitosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrequisitosContainerData"+"V", GridrequisitosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridrequisitosContainerData"+"V"+"\" value='"+GridrequisitosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_216_CG2e( true) ;
         }
         else
         {
            wb_table6_216_CG2e( false) ;
         }
      }

      protected void wb_table5_210_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEMRESULTADO_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_210_CG2e( true) ;
         }
         else
         {
            wb_table5_210_CG2e( false) ;
         }
      }

      protected void wb_table4_20_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisitadopor_Internalname, "Requisitado por", "", "", lblTextblockrequisitadopor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockrequisitadopor_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_25_CG2( true) ;
         }
         else
         {
            wb_table8_25_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table8_25_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisitante_Internalname, "Requisitante", "", "", lblTextblockrequisitante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockrequisitante_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavRequisitante, dynavRequisitante_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0)), 1, dynavRequisitante_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVREQUISITANTE.CLICK."+"'", "int", "", dynavRequisitante.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_WP_OS.htm");
            dynavRequisitante.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV102Requisitante), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavRequisitante_Internalname, "Values", (String)(dynavRequisitante.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_cargouonom_Internalname, "Unidade organizacional", "", "", lblTextblockusuario_cargouonom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockusuario_cargouonom_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_cargouonom_Internalname, StringUtil.RTrim( AV56Usuario_CargoUONom), StringUtil.RTrim( context.localUtil.Format( AV56Usuario_CargoUONom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_cargouonom_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_cargouonom_Visible, edtavUsuario_cargouonom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_demandafm_Internalname, "N� OS", "", "", lblTextblockcontagemresultado_demandafm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm_Internalname, AV69ContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV69ContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContagemresultado_demandafm_Enabled, 1, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_pessoanom_Internalname, "Requisitante", "", "", lblTextblockusuario_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockusuario_pessoanom_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom_Internalname, StringUtil.RTrim( AV7Usuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV7Usuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom_Visible, edtavUsuario_pessoanom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datadmn_Internalname, "Data da OS", "", "", lblTextblockcontagemresultado_datadmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_219_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_Internalname, context.localUtil.Format(AV8ContagemResultado_DataDmn, "99/99/99"), context.localUtil.Format( AV8ContagemResultado_DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavContagemresultado_datadmn_Enabled, 1, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmn_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_descricao_Internalname, "Titulo", "", "", lblTextblockcontagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao_Internalname, StringUtil.RTrim( AV88ContagemResultado_Descricao), StringUtil.RTrim( context.localUtil.Format( AV88ContagemResultado_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_Internalname, "Contrato", "", "", lblTextblockcontrato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontrato_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContrato, cmbavContrato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0)), 1, cmbavContrato_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATO.CLICK."+"'", "int", "", cmbavContrato.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WP_OS.htm");
            cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV117Contrato), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", (String)(cmbavContrato.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_codigo_Internalname, "Grupo de Servi�os", "", "", lblTextblockservicogrupo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServicogrupo_codigo, cmbavServicogrupo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)), 1, cmbavServicogrupo_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICOGRUPO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "", true, "HLP_WP_OS.htm");
            cmbavServicogrupo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServicogrupo_codigo_Internalname, "Values", (String)(cmbavServicogrupo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Servi�o", "", "", lblTextblockservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_codigo, cmbavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0)), 1, cmbavServico_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"", "", true, "HLP_WP_OS.htm");
            cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", (String)(cmbavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_undcntnome_Internalname, "Unidade de Contrata��o", "", "", lblTextblockcontratoservicos_undcntnome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_undcntnome_Internalname, StringUtil.RTrim( AV171ContratoServicos_UndCntNome), StringUtil.RTrim( context.localUtil.Format( AV171ContratoServicos_UndCntNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_undcntnome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratoservicos_undcntnome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_qntuntcns_Internalname, "Qtde Unit de Consumo", "", "", lblTextblockcontratoservicos_qntuntcns_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_qntuntcns_Internalname, StringUtil.LTrim( StringUtil.NToC( AV172ContratoServicos_QntUntCns, 9, 4, ",", "")), ((edtavContratoservicos_qntuntcns_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV172ContratoServicos_QntUntCns, "ZZZ9.9999")) : context.localUtil.Format( AV172ContratoServicos_QntUntCns, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_qntuntcns_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavContratoservicos_qntuntcns_Enabled, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcomplexidade_Internalname, "Complexidade", "", "", lblTextblockcomplexidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcomplexidade_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table9_102_CG2( true) ;
         }
         else
         {
            wb_table9_102_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table9_102_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname+"\"  class='"+cellTextblockcontagemresultado_quantidadesolicitada_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_quantidadesolicitada_Internalname, "Quantidade Solicitada", "", "", lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_quantidadesolicitada_cell_Internalname+"\" colspan=\"7\"  class='"+cellContagemresultado_quantidadesolicitada_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_quantidadesolicitada_Internalname, StringUtil.LTrim( StringUtil.NToC( AV173ContagemResultado_QuantidadeSolicitada, 9, 4, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV173ContagemResultado_QuantidadeSolicitada, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_quantidadesolicitada_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_quantidadesolicitada_Visible, 1, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisitado_Internalname, "Requisitado", "", "", lblTextblockrequisitado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRequisitado, cmbavRequisitado_Internalname, StringUtil.RTrim( AV9Requisitado), 1, cmbavRequisitado_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVREQUISITADO.CLICK."+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", "", true, "HLP_WP_OS.htm");
            cmbavRequisitado.CurrentValue = StringUtil.RTrim( AV9Requisitado);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisitado_Internalname, "Values", (String)(cmbavRequisitado.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockusuario_codigo_Internalname, "Respons�vel", "", "", lblTextblockusuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockusuario_codigo_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUsuario_codigo, cmbavUsuario_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0)), 1, cmbavUsuario_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavUsuario_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", "", true, "HLP_WP_OS.htm");
            cmbavUsuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Usuario_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUsuario_codigo_Internalname, "Values", (String)(cmbavUsuario_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvincularcom_Internalname, "Vincular com", "", "", lblLblvincularcom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table10_128_CG2( true) ;
         }
         else
         {
            wb_table10_128_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table10_128_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dmnvinculadaref_Internalname, AV133ContagemResultado_DmnVinculadaRef, StringUtil.RTrim( context.localUtil.Format( AV133ContagemResultado_DmnVinculadaRef, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS Refer�ncia", edtavContagemresultado_dmnvinculadaref_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_dmnvinculadaref_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpfbfsdaosvnc_Internalname, "Bruto", "", "", lblTextblockpfbfsdaosvnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockpfbfsdaosvnc_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPfbfsdaosvnc_Internalname, StringUtil.LTrim( StringUtil.NToC( AV66PFBFSdaOSVnc, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV66PFBFSdaOSVnc, "ZZZZZZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPfbfsdaosvnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPfbfsdaosvnc_Visible, edtavPfbfsdaosvnc_Enabled, 1, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpflfsdaosvnc_Internalname, "Liquido", "", "", lblTextblockpflfsdaosvnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockpflfsdaosvnc_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPflfsdaosvnc_Internalname, StringUtil.LTrim( StringUtil.NToC( AV73PFLFSdaOSVnc, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV73PFLFSdaOSVnc, "ZZZZZZZ9.99999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPflfsdaosvnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPflfsdaosvnc_Visible, edtavPflfsdaosvnc_Enabled, 1, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontratadaorigem_cell_Internalname+"\"  class='"+cellTextblockcontratadaorigem_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratadaorigem_Internalname, "Origem da Refer�ncia", "", "", lblTextblockcontratadaorigem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContratadaorigem_cell_Internalname+"\"  class='"+cellContratadaorigem_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratadaorigem, dynavContratadaorigem_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0)), 1, dynavContratadaorigem_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATADAORIGEM.CLICK."+"'", "int", "", dynavContratadaorigem.Visible, dynavContratadaorigem.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", "", true, "HLP_WP_OS.htm");
            dynavContratadaorigem.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV105ContratadaOrigem), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratadaorigem_Internalname, "Values", (String)(dynavContratadaorigem.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockcontadorfs_cell_Internalname+"\"  class='"+cellTextblockcontadorfs_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontadorfs_Internalname, "Respons�vel na Origem", "", "", lblTextblockcontadorfs_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContadorfs_cell_Internalname+"\" colspan=\"5\"  class='"+cellContadorfs_cell_Class+"'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContadorfs, dynavContadorfs_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0)), 1, dynavContadorfs_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContadorfs.Visible, dynavContadorfs.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", "", true, "HLP_WP_OS.htm");
            dynavContadorfs.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfs_Internalname, "Values", (String)(dynavContadorfs.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicos_localexec_Internalname, "Local de Execu��o", "", "", lblTextblockcontratoservicos_localexec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContratoservicos_localexec, cmbavContratoservicos_localexec_Internalname, StringUtil.RTrim( AV18ContratoServicos_LocalExec), 1, cmbavContratoservicos_localexec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavContratoservicos_localexec.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"", "", true, "HLP_WP_OS.htm");
            cmbavContratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV18ContratoServicos_LocalExec);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContratoservicos_localexec_Internalname, "Values", (String)(cmbavContratoservicos_localexec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprazoanalise_Internalname, "Prazo de An�lise", "", "", lblTextblockprazoanalise_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'" + sGXsfl_219_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPrazoanalise_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPrazoanalise_Internalname, context.localUtil.TToC( AV70PrazoAnalise, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV70PrazoAnalise, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,163);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrazoanalise_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtavPrazoanalise_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            GxWebStd.gx_bitmap( context, edtavPrazoanalise_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavPrazoanalise_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprazoentrega_Internalname, "Prazo de Entrega", "", "", lblTextblockprazoentrega_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 168,'',false,'" + sGXsfl_219_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPrazoentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPrazoentrega_Internalname, context.localUtil.TToC( AV62PrazoEntrega, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV62PrazoEntrega, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,168);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrazoentrega_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS.htm");
            GxWebStd.gx_bitmap( context, edtavPrazoentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_codigo_Internalname, "Sistema", "", "", lblTextblocksistema_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblocksistema_codigo_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 179,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_codigo, cmbavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0)), 1, cmbavSistema_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSISTEMA_CODIGO.CLICK."+"'", "int", "", cmbavSistema_codigo.Visible, cmbavSistema_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,179);\"", "", true, "HLP_WP_OS.htm");
            cmbavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_codigo_Internalname, "Values", (String)(cmbavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_codigo_Internalname, "M�dulo", "", "", lblTextblockmodulo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockmodulo_codigo_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 183,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavModulo_codigo, dynavModulo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0)), 1, dynavModulo_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavModulo_codigo.Visible, dynavModulo_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,183);\"", "", true, "HLP_WP_OS.htm");
            dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavModulo_codigo_Internalname, "Values", (String)(dynavModulo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaousuario_codigo_Internalname, "Fun��o de Usu�rio", "", "", lblTextblockfuncaousuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockfuncaousuario_codigo_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 187,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavFuncaousuario_codigo, dynavFuncaousuario_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0)), 1, dynavFuncaousuario_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavFuncaousuario_codigo.Visible, dynavFuncaousuario_codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,187);\"", "", true, "HLP_WP_OS.htm");
            dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", (String)(dynavFuncaousuario_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblocksistema_gestor_cell_Internalname+"\"  class='"+cellTextblocksistema_gestor_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_gestor_Internalname, "Gestor Respons�vel pelo Sistema", "", "", lblTextblocksistema_gestor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellSistema_gestor_cell_Internalname+"\" colspan=\"7\"  class='"+cellSistema_gestor_cell_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 194,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_gestor_Internalname, AV167Sistema_Gestor, StringUtil.RTrim( context.localUtil.Format( AV167Sistema_Gestor, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,194);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_gestor_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_gestor_Visible, edtavSistema_gestor_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_link_Internalname, "Link", "", "", lblTextblockcontagemresultado_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 199,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_link_Internalname, AV15ContagemResultado_Link, AV15ContagemResultado_Link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,199);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_link_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 360, "px", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "", "left", false, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"8\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup5_Internalname, "Anexos", 1, 0, "px", 0, "px", "Group", "", "HLP_WP_OS.htm");
            wb_table11_203_CG2( true) ;
         }
         else
         {
            wb_table11_203_CG2( false) ;
         }
         return  ;
      }

      protected void wb_table11_203_CG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_20_CG2e( true) ;
         }
         else
         {
            wb_table4_20_CG2e( false) ;
         }
      }

      protected void wb_table11_203_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0207"+"", StringUtil.RTrim( WebComp_Wcarquivosevd_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0207"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcarquivosevd), StringUtil.Lower( WebComp_Wcarquivosevd_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0207"+"");
                  }
                  WebComp_Wcarquivosevd.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcarquivosevd), StringUtil.Lower( WebComp_Wcarquivosevd_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_203_CG2e( true) ;
         }
         else
         {
            wb_table11_203_CG2e( false) ;
         }
      }

      protected void wb_table10_128_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddmnvinculadas_Internalname, tblTablemergeddmnvinculadas_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDmnvinculadas, cmbavDmnvinculadas_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0)), 1, cmbavDmnvinculadas_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDMNVINCULADAS.CLICK."+"'", "int", "", cmbavDmnvinculadas.Visible, cmbavDmnvinculadas.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", "", true, "HLP_WP_OS.htm");
            cmbavDmnvinculadas.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV123DmnVinculadas), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDmnvinculadas_Internalname, "Values", (String)(cmbavDmnvinculadas.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_219_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dmnvinculada_Internalname, AV22ContagemResultado_DmnVinculada, StringUtil.RTrim( context.localUtil.Format( AV22ContagemResultado_DmnVinculada, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS", edtavContagemresultado_dmnvinculada_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_dmnvinculada_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_128_CG2e( true) ;
         }
         else
         {
            wb_table10_128_CG2e( false) ;
         }
      }

      protected void wb_table9_102_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcomplexidade_Internalname, tblTablemergedcomplexidade_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavComplexidade, cmbavComplexidade_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0)), 1, cmbavComplexidade_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCOMPLEXIDADE.CLICK."+"'", "int", "", cmbavComplexidade.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", "", true, "HLP_WP_OS.htm");
            cmbavComplexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV97Complexidade), 3, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavComplexidade_Internalname, "Values", (String)(cmbavComplexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprioridade_codigo_Internalname, "Prioridade", "", "", lblTextblockprioridade_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockprioridade_codigo_Visible, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_219_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavPrioridade_codigo, cmbavPrioridade_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0)), 1, cmbavPrioridade_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVPRIORIDADE_CODIGO.CLICK."+"'", "int", "", cmbavPrioridade_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "", true, "HLP_WP_OS.htm");
            cmbavPrioridade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV96Prioridade_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPrioridade_codigo_Internalname, "Values", (String)(cmbavPrioridade_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_102_CG2e( true) ;
         }
         else
         {
            wb_table9_102_CG2e( false) ;
         }
      }

      protected void wb_table8_25_CG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergedrequisitadopor_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergedrequisitadopor_Internalname, tblTablemergedrequisitadopor_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_219_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequisitadopor_Internalname, StringUtil.BoolToStr( AV101RequisitadoPor), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(28, this, 'true', 'false');gx.ajax.executeCliEvent('e36cg1_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblRequisitadopor_righttext_Internalname, lblRequisitadopor_righttext_Caption, "", "", lblRequisitadopor_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_25_CG2e( true) ;
         }
         else
         {
            wb_table8_25_CG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACG2( ) ;
         WSCG2( ) ;
         WECG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Wcarquivosevd == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcarquivosevd_Component) != 0 )
            {
               WebComp_Wcarquivosevd.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423461617");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_os.js", "?202032423461617");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_2192( )
      {
         edtavBtnexcluirreqneg_Internalname = "vBTNEXCLUIRREQNEG_"+sGXsfl_219_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_219_idx;
         edtavSdt_requisitos__requisito_identificador_Internalname = "SDT_REQUISITOS__REQUISITO_IDENTIFICADOR_"+sGXsfl_219_idx;
         edtavSdt_requisitos__requisito_titulo_Internalname = "SDT_REQUISITOS__REQUISITO_TITULO_"+sGXsfl_219_idx;
         cmbavSdt_requisitos__requisito_prioridade_Internalname = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_"+sGXsfl_219_idx;
         cmbavSdt_requisitos__requisito_status_Internalname = "SDT_REQUISITOS__REQUISITO_STATUS_"+sGXsfl_219_idx;
      }

      protected void SubsflControlProps_fel_2192( )
      {
         edtavBtnexcluirreqneg_Internalname = "vBTNEXCLUIRREQNEG_"+sGXsfl_219_fel_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_219_fel_idx;
         edtavSdt_requisitos__requisito_identificador_Internalname = "SDT_REQUISITOS__REQUISITO_IDENTIFICADOR_"+sGXsfl_219_fel_idx;
         edtavSdt_requisitos__requisito_titulo_Internalname = "SDT_REQUISITOS__REQUISITO_TITULO_"+sGXsfl_219_fel_idx;
         cmbavSdt_requisitos__requisito_prioridade_Internalname = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_"+sGXsfl_219_fel_idx;
         cmbavSdt_requisitos__requisito_status_Internalname = "SDT_REQUISITOS__REQUISITO_STATUS_"+sGXsfl_219_fel_idx;
      }

      protected void sendrow_2192( )
      {
         SubsflControlProps_2192( ) ;
         WBCG0( ) ;
         if ( ( subGridrequisitos_Rows * 1 == 0 ) || ( nGXsfl_219_idx <= subGridrequisitos_Recordsperpage( ) * 1 ) )
         {
            GridrequisitosRow = GXWebRow.GetNew(context,GridrequisitosContainer);
            if ( subGridrequisitos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridrequisitos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
               {
                  subGridrequisitos_Linesclass = subGridrequisitos_Class+"Odd";
               }
            }
            else if ( subGridrequisitos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridrequisitos_Backstyle = 0;
               subGridrequisitos_Backcolor = subGridrequisitos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
               {
                  subGridrequisitos_Linesclass = subGridrequisitos_Class+"Uniform";
               }
            }
            else if ( subGridrequisitos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridrequisitos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
               {
                  subGridrequisitos_Linesclass = subGridrequisitos_Class+"Odd";
               }
               subGridrequisitos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridrequisitos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridrequisitos_Backstyle = 1;
               if ( ((int)((nGXsfl_219_idx) % (2))) == 0 )
               {
                  subGridrequisitos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
                  {
                     subGridrequisitos_Linesclass = subGridrequisitos_Class+"Even";
                  }
               }
               else
               {
                  subGridrequisitos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
                  {
                     subGridrequisitos_Linesclass = subGridrequisitos_Class+"Odd";
                  }
               }
            }
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridrequisitos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_219_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnexcluirreqneg_Enabled!=0)&&(edtavBtnexcluirreqneg_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 220,'',false,'',219)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV147btnExcluirReqNeg_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV147btnExcluirReqNeg))&&String.IsNullOrEmpty(StringUtil.RTrim( AV182Btnexcluirreqneg_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV147btnExcluirReqNeg)));
            GridrequisitosRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnexcluirreqneg_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV147btnExcluirReqNeg)) ? AV182Btnexcluirreqneg_GXI : context.PathToRelativeUrl( AV147btnExcluirReqNeg)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtnexcluirreqneg_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnexcluirreqneg_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNEXCLUIRREQNEG.CLICK."+sGXsfl_219_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV147btnExcluirReqNeg_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 221,'',false,'"+sGXsfl_219_idx+"',219)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAgrupador_Internalname,(String)AV146Agrupador,(String)"",TempTags+((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,221);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAgrupador_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavAgrupador_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavAgrupador_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)219,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSdt_requisitos__requisito_identificador_Internalname,((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_identificador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSdt_requisitos__requisito_identificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavSdt_requisitos__requisito_identificador_Enabled,(short)0,(String)"text",(String)"",(short)100,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)219,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSdt_requisitos__requisito_titulo_Internalname,((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_titulo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSdt_requisitos__requisito_titulo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavSdt_requisitos__requisito_titulo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)250,(short)0,(short)0,(short)219,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_219_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SDT_REQUISITOS__REQUISITO_PRIORIDADE_" + sGXsfl_219_idx;
               cmbavSdt_requisitos__requisito_prioridade.Name = GXCCtl;
               cmbavSdt_requisitos__requisito_prioridade.WebTags = "";
               cmbavSdt_requisitos__requisito_prioridade.addItem("1", "Alta", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("2", "Alta M�dia", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("3", "Alta Baixa", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("4", "M�dia Alta", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("5", "M�dia M�dia", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("6", "M�dia Baixa", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("7", "Baixa Alta", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("8", "Baixa M�dia", 0);
               cmbavSdt_requisitos__requisito_prioridade.addItem("9", "Baixa Baixa", 0);
               if ( cmbavSdt_requisitos__requisito_prioridade.ItemCount > 0 )
               {
                  if ( ( AV176GXV1 > 0 ) && ( AV161SDT_Requisitos.Count >= AV176GXV1 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade) )
                  {
                     ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade), 2, 0))), "."));
                  }
               }
            }
            /* ComboBox */
            GridrequisitosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavSdt_requisitos__requisito_prioridade,(String)cmbavSdt_requisitos__requisito_prioridade_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade), 2, 0)),(short)1,(String)cmbavSdt_requisitos__requisito_prioridade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavSdt_requisitos__requisito_prioridade.Enabled,(short)0,(short)0,(short)150,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavSdt_requisitos__requisito_prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_prioridade), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_prioridade_Internalname, "Values", (String)(cmbavSdt_requisitos__requisito_prioridade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_219_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SDT_REQUISITOS__REQUISITO_STATUS_" + sGXsfl_219_idx;
               cmbavSdt_requisitos__requisito_status.Name = GXCCtl;
               cmbavSdt_requisitos__requisito_status.WebTags = "";
               cmbavSdt_requisitos__requisito_status.addItem("0", "Rascunho", 0);
               cmbavSdt_requisitos__requisito_status.addItem("1", "Solicitado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("2", "Aprovado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("3", "N�o Aprovado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("4", "Pausado", 0);
               cmbavSdt_requisitos__requisito_status.addItem("5", "Cancelado", 0);
               if ( cmbavSdt_requisitos__requisito_status.ItemCount > 0 )
               {
                  if ( ( AV176GXV1 > 0 ) && ( AV161SDT_Requisitos.Count >= AV176GXV1 ) && (0==((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status) )
                  {
                     ((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status = (short)(NumberUtil.Val( cmbavSdt_requisitos__requisito_status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status), 4, 0))), "."));
                  }
               }
            }
            /* ComboBox */
            GridrequisitosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavSdt_requisitos__requisito_status,(String)cmbavSdt_requisitos__requisito_status_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status), 4, 0)),(short)1,(String)cmbavSdt_requisitos__requisito_status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavSdt_requisitos__requisito_status.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbavSdt_requisitos__requisito_status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(((SdtSDT_Requisitos_SDT_RequisitosItem)AV161SDT_Requisitos.Item(AV176GXV1)).gxTpr_Requisito_status), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_requisitos__requisito_status_Internalname, "Values", (String)(cmbavSdt_requisitos__requisito_status.ToJavascriptSource()));
            GridrequisitosContainer.AddRow(GridrequisitosRow);
            nGXsfl_219_idx = (short)(((subGridrequisitos_Islastpage==1)&&(nGXsfl_219_idx+1>subGridrequisitos_Recordsperpage( )) ? 1 : nGXsfl_219_idx+1));
            sGXsfl_219_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_219_idx), 4, 0)), 4, "0");
            SubsflControlProps_2192( ) ;
         }
         /* End function sendrow_2192 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblLblultimaos_Internalname = "LBLULTIMAOS";
         lblTextblockrequisitadopor_Internalname = "TEXTBLOCKREQUISITADOPOR";
         chkavRequisitadopor_Internalname = "vREQUISITADOPOR";
         lblRequisitadopor_righttext_Internalname = "REQUISITADOPOR_RIGHTTEXT";
         tblTablemergedrequisitadopor_Internalname = "TABLEMERGEDREQUISITADOPOR";
         lblTextblockrequisitante_Internalname = "TEXTBLOCKREQUISITANTE";
         dynavRequisitante_Internalname = "vREQUISITANTE";
         lblTextblockusuario_cargouonom_Internalname = "TEXTBLOCKUSUARIO_CARGOUONOM";
         edtavUsuario_cargouonom_Internalname = "vUSUARIO_CARGOUONOM";
         lblTextblockcontagemresultado_demandafm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DEMANDAFM";
         edtavContagemresultado_demandafm_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM";
         lblTextblockusuario_pessoanom_Internalname = "TEXTBLOCKUSUARIO_PESSOANOM";
         edtavUsuario_pessoanom_Internalname = "vUSUARIO_PESSOANOM";
         lblTextblockcontagemresultado_datadmn_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATADMN";
         edtavContagemresultado_datadmn_Internalname = "vCONTAGEMRESULTADO_DATADMN";
         lblTextblockcontagemresultado_descricao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DESCRICAO";
         edtavContagemresultado_descricao_Internalname = "vCONTAGEMRESULTADO_DESCRICAO";
         lblTextblockcontrato_Internalname = "TEXTBLOCKCONTRATO";
         cmbavContrato_Internalname = "vCONTRATO";
         lblTextblockservicogrupo_codigo_Internalname = "TEXTBLOCKSERVICOGRUPO_CODIGO";
         cmbavServicogrupo_codigo_Internalname = "vSERVICOGRUPO_CODIGO";
         lblTextblockservico_codigo_Internalname = "TEXTBLOCKSERVICO_CODIGO";
         cmbavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblockcontratoservicos_undcntnome_Internalname = "TEXTBLOCKCONTRATOSERVICOS_UNDCNTNOME";
         edtavContratoservicos_undcntnome_Internalname = "vCONTRATOSERVICOS_UNDCNTNOME";
         lblTextblockcontratoservicos_qntuntcns_Internalname = "TEXTBLOCKCONTRATOSERVICOS_QNTUNTCNS";
         edtavContratoservicos_qntuntcns_Internalname = "vCONTRATOSERVICOS_QNTUNTCNS";
         lblTextblockcomplexidade_Internalname = "TEXTBLOCKCOMPLEXIDADE";
         cmbavComplexidade_Internalname = "vCOMPLEXIDADE";
         lblTextblockprioridade_codigo_Internalname = "TEXTBLOCKPRIORIDADE_CODIGO";
         cmbavPrioridade_codigo_Internalname = "vPRIORIDADE_CODIGO";
         tblTablemergedcomplexidade_Internalname = "TABLEMERGEDCOMPLEXIDADE";
         lblTextblockcontagemresultado_quantidadesolicitada_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL";
         edtavContagemresultado_quantidadesolicitada_Internalname = "vCONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         cellContagemresultado_quantidadesolicitada_cell_Internalname = "CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL";
         lblTextblockrequisitado_Internalname = "TEXTBLOCKREQUISITADO";
         cmbavRequisitado_Internalname = "vREQUISITADO";
         lblTextblockusuario_codigo_Internalname = "TEXTBLOCKUSUARIO_CODIGO";
         cmbavUsuario_codigo_Internalname = "vUSUARIO_CODIGO";
         lblLblvincularcom_Internalname = "LBLVINCULARCOM";
         cmbavDmnvinculadas_Internalname = "vDMNVINCULADAS";
         edtavContagemresultado_dmnvinculada_Internalname = "vCONTAGEMRESULTADO_DMNVINCULADA";
         tblTablemergeddmnvinculadas_Internalname = "TABLEMERGEDDMNVINCULADAS";
         edtavContagemresultado_dmnvinculadaref_Internalname = "vCONTAGEMRESULTADO_DMNVINCULADAREF";
         lblTextblockpfbfsdaosvnc_Internalname = "TEXTBLOCKPFBFSDAOSVNC";
         edtavPfbfsdaosvnc_Internalname = "vPFBFSDAOSVNC";
         lblTextblockpflfsdaosvnc_Internalname = "TEXTBLOCKPFLFSDAOSVNC";
         edtavPflfsdaosvnc_Internalname = "vPFLFSDAOSVNC";
         lblTextblockcontratadaorigem_Internalname = "TEXTBLOCKCONTRATADAORIGEM";
         cellTextblockcontratadaorigem_cell_Internalname = "TEXTBLOCKCONTRATADAORIGEM_CELL";
         dynavContratadaorigem_Internalname = "vCONTRATADAORIGEM";
         cellContratadaorigem_cell_Internalname = "CONTRATADAORIGEM_CELL";
         lblTextblockcontadorfs_Internalname = "TEXTBLOCKCONTADORFS";
         cellTextblockcontadorfs_cell_Internalname = "TEXTBLOCKCONTADORFS_CELL";
         dynavContadorfs_Internalname = "vCONTADORFS";
         cellContadorfs_cell_Internalname = "CONTADORFS_CELL";
         lblTextblockcontratoservicos_localexec_Internalname = "TEXTBLOCKCONTRATOSERVICOS_LOCALEXEC";
         cmbavContratoservicos_localexec_Internalname = "vCONTRATOSERVICOS_LOCALEXEC";
         lblTextblockprazoanalise_Internalname = "TEXTBLOCKPRAZOANALISE";
         edtavPrazoanalise_Internalname = "vPRAZOANALISE";
         lblTextblockprazoentrega_Internalname = "TEXTBLOCKPRAZOENTREGA";
         edtavPrazoentrega_Internalname = "vPRAZOENTREGA";
         lblTextblocksistema_codigo_Internalname = "TEXTBLOCKSISTEMA_CODIGO";
         cmbavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         lblTextblockmodulo_codigo_Internalname = "TEXTBLOCKMODULO_CODIGO";
         dynavModulo_codigo_Internalname = "vMODULO_CODIGO";
         lblTextblockfuncaousuario_codigo_Internalname = "TEXTBLOCKFUNCAOUSUARIO_CODIGO";
         dynavFuncaousuario_codigo_Internalname = "vFUNCAOUSUARIO_CODIGO";
         lblTextblocksistema_gestor_Internalname = "TEXTBLOCKSISTEMA_GESTOR";
         cellTextblocksistema_gestor_cell_Internalname = "TEXTBLOCKSISTEMA_GESTOR_CELL";
         edtavSistema_gestor_Internalname = "vSISTEMA_GESTOR";
         cellSistema_gestor_cell_Internalname = "SISTEMA_GESTOR_CELL";
         lblTextblockcontagemresultado_link_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_LINK";
         edtavContagemresultado_link_Internalname = "vCONTAGEMRESULTADO_LINK";
         tblUnnamedtable4_Internalname = "UNNAMEDTABLE4";
         grpUnnamedgroup5_Internalname = "UNNAMEDGROUP5";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         Contagemresultado_observacao_Internalname = "CONTAGEMRESULTADO_OBSERVACAO";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         edtavBtnexcluirreqneg_Internalname = "vBTNEXCLUIRREQNEG";
         edtavAgrupador_Internalname = "vAGRUPADOR";
         edtavSdt_requisitos__requisito_identificador_Internalname = "SDT_REQUISITOS__REQUISITO_IDENTIFICADOR";
         edtavSdt_requisitos__requisito_titulo_Internalname = "SDT_REQUISITOS__REQUISITO_TITULO";
         cmbavSdt_requisitos__requisito_prioridade_Internalname = "SDT_REQUISITOS__REQUISITO_PRIORIDADE";
         cmbavSdt_requisitos__requisito_status_Internalname = "SDT_REQUISITOS__REQUISITO_STATUS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Gxuitabspanel_tab_Internalname = "GXUITABSPANEL_TAB";
         lblTbjava_Internalname = "TBJAVA";
         Dvelop_bootstrap_confirmpanel1_Internalname = "DVELOP_BOOTSTRAP_CONFIRMPANEL1";
         Confirmpanel2_Internalname = "CONFIRMPANEL2";
         cmbavServico_atende_Internalname = "vSERVICO_ATENDE";
         cmbavServico_obrigavalores_Internalname = "vSERVICO_OBRIGAVALORES";
         tblUsrtable_Internalname = "USRTABLE";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnartefatos_Internalname = "BTNARTEFATOS";
         bttBtnconfirmar_Internalname = "BTNCONFIRMAR";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableaction_Internalname = "TABLEACTION";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         cmbavServico_obrigavalores_Internalname = "vSERVICO_OBRIGAVALORES";
         chkavContratovencido_Internalname = "vCONTRATOVENCIDO";
         edtavRequisitadocod_Internalname = "vREQUISITADOCOD";
         Form.Internalname = "FORM";
         subGridrequisitos_Internalname = "GRIDREQUISITOS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavSdt_requisitos__requisito_status_Jsonclick = "";
         cmbavSdt_requisitos__requisito_prioridade_Jsonclick = "";
         edtavSdt_requisitos__requisito_titulo_Jsonclick = "";
         edtavSdt_requisitos__requisito_identificador_Jsonclick = "";
         edtavAgrupador_Jsonclick = "";
         edtavAgrupador_Visible = -1;
         edtavBtnexcluirreqneg_Jsonclick = "";
         edtavBtnexcluirreqneg_Visible = -1;
         edtavBtnexcluirreqneg_Enabled = 1;
         cmbavPrioridade_codigo_Jsonclick = "";
         lblTextblockprioridade_codigo_Visible = 1;
         cmbavComplexidade_Jsonclick = "";
         edtavContagemresultado_dmnvinculada_Jsonclick = "";
         cmbavDmnvinculadas_Jsonclick = "";
         edtavContagemresultado_link_Jsonclick = "";
         edtavSistema_gestor_Jsonclick = "";
         edtavSistema_gestor_Enabled = 1;
         cellSistema_gestor_cell_Class = "";
         cellTextblocksistema_gestor_cell_Class = "";
         dynavFuncaousuario_codigo_Jsonclick = "";
         lblTextblockfuncaousuario_codigo_Visible = 1;
         dynavModulo_codigo_Jsonclick = "";
         lblTextblockmodulo_codigo_Visible = 1;
         cmbavSistema_codigo_Jsonclick = "";
         lblTextblocksistema_codigo_Visible = 1;
         edtavPrazoentrega_Jsonclick = "";
         edtavPrazoanalise_Jsonclick = "";
         edtavPrazoanalise_Enabled = 1;
         cmbavContratoservicos_localexec_Jsonclick = "";
         dynavContadorfs_Jsonclick = "";
         cellContadorfs_cell_Class = "";
         cellTextblockcontadorfs_cell_Class = "";
         dynavContratadaorigem_Jsonclick = "";
         cellContratadaorigem_cell_Class = "";
         cellTextblockcontratadaorigem_cell_Class = "";
         edtavPflfsdaosvnc_Jsonclick = "";
         lblTextblockpflfsdaosvnc_Visible = 1;
         edtavPfbfsdaosvnc_Jsonclick = "";
         lblTextblockpfbfsdaosvnc_Visible = 1;
         edtavContagemresultado_dmnvinculadaref_Jsonclick = "";
         cmbavUsuario_codigo_Jsonclick = "";
         lblTextblockusuario_codigo_Visible = 1;
         cmbavRequisitado_Jsonclick = "";
         edtavContagemresultado_quantidadesolicitada_Jsonclick = "";
         cellContagemresultado_quantidadesolicitada_cell_Class = "";
         cellTextblockcontagemresultado_quantidadesolicitada_cell_Class = "";
         lblTextblockcomplexidade_Visible = 1;
         edtavContratoservicos_qntuntcns_Jsonclick = "";
         edtavContratoservicos_qntuntcns_Enabled = 1;
         edtavContratoservicos_undcntnome_Jsonclick = "";
         edtavContratoservicos_undcntnome_Enabled = 1;
         cmbavServico_codigo_Jsonclick = "";
         cmbavServicogrupo_codigo_Jsonclick = "";
         cmbavContrato_Jsonclick = "";
         lblTextblockcontrato_Visible = 1;
         edtavContagemresultado_descricao_Jsonclick = "";
         edtavContagemresultado_datadmn_Jsonclick = "";
         edtavContagemresultado_datadmn_Enabled = 1;
         edtavUsuario_pessoanom_Jsonclick = "";
         edtavUsuario_pessoanom_Enabled = 1;
         edtavUsuario_pessoanom_Visible = 1;
         lblTextblockusuario_pessoanom_Visible = 1;
         edtavContagemresultado_demandafm_Jsonclick = "";
         edtavUsuario_cargouonom_Jsonclick = "";
         edtavUsuario_cargouonom_Enabled = 1;
         edtavUsuario_cargouonom_Visible = 1;
         lblTextblockusuario_cargouonom_Visible = 1;
         dynavRequisitante_Jsonclick = "";
         lblTextblockrequisitante_Visible = 1;
         lblTextblockrequisitadopor_Visible = 1;
         Contagemresultado_observacao_Enabled = Convert.ToBoolean( 1);
         subGridrequisitos_Allowcollapsing = 0;
         subGridrequisitos_Allowselection = 0;
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         edtavAgrupador_Enabled = 1;
         edtavAgrupador_Forecolor = (int)(0xFFFFFF);
         edtavBtnexcluirreqneg_Tooltiptext = "";
         subGridrequisitos_Class = "WorkWithBorder WorkWith";
         cmbavServico_atende_Jsonclick = "";
         lblTbjava_Visible = 1;
         bttBtnconfirmar_Visible = 1;
         bttBtnartefatos_Visible = 1;
         lblLblultimaos_Link = "";
         cmbavDmnvinculadas.Enabled = 1;
         cmbavUsuario_codigo.Visible = 1;
         dynavContadorfs.Enabled = 1;
         cmbavContratoservicos_localexec.Enabled = 1;
         dynavFuncaousuario_codigo.Visible = 1;
         dynavModulo_codigo.Visible = 1;
         cmbavSistema_codigo.Visible = 1;
         edtavContagemresultado_dmnvinculadaref_Visible = 1;
         edtavContagemresultado_dmnvinculada_Visible = 1;
         dynavFuncaousuario_codigo.Enabled = 1;
         dynavModulo_codigo.Enabled = 1;
         cmbavSistema_codigo.Enabled = 1;
         dynavContratadaorigem.Enabled = 1;
         edtavPflfsdaosvnc_Enabled = 1;
         edtavPfbfsdaosvnc_Enabled = 1;
         tblTablemergedrequisitadopor_Visible = 1;
         bttBtnconfirmar_Tooltiptext = "Solicitar";
         bttBtnartefatos_Caption = "Artefatos";
         edtavSistema_gestor_Visible = 1;
         dynavContadorfs.Visible = 1;
         dynavContratadaorigem.Visible = 1;
         edtavContagemresultado_quantidadesolicitada_Visible = 1;
         lblLblultimaos_Caption = "Ultima OS cadastrada: ";
         lblTbjava_Caption = "tbJava";
         cmbavDmnvinculadas.Visible = 1;
         cmbavContrato.Visible = 1;
         dynavRequisitante.Visible = 1;
         edtavContagemresultado_demandafm_Enabled = 1;
         cmbavServico_atende.Visible = 1;
         edtavPflfsdaosvnc_Visible = 1;
         edtavPfbfsdaosvnc_Visible = 1;
         cmbavComplexidade.Visible = 1;
         cmbavPrioridade_codigo.Visible = 1;
         lblRequisitadopor_righttext_Caption = "�(Contratante)";
         subGridrequisitos_Backcolorstyle = 3;
         cmbavSdt_requisitos__requisito_status.Enabled = -1;
         cmbavSdt_requisitos__requisito_prioridade.Enabled = -1;
         edtavSdt_requisitos__requisito_titulo_Enabled = -1;
         edtavSdt_requisitos__requisito_identificador_Enabled = -1;
         chkavContratovencido.Caption = "";
         chkavRequisitadopor.Caption = "";
         edtavRequisitadocod_Jsonclick = "";
         edtavRequisitadocod_Visible = 1;
         chkavContratovencido.Visible = 1;
         cmbavServico_obrigavalores_Jsonclick = "";
         cmbavServico_obrigavalores.Visible = 1;
         edtavAreatrabalho_codigo_Jsonclick = "";
         edtavAreatrabalho_codigo_Visible = 1;
         dynavContratadaorigem.Description = "";
         dynavRequisitante.Description = "";
         cmbavServico_codigo.Description = "";
         cmbavContrato.Description = "";
         Confirmpanel2_Confirmtype = "0";
         Confirmpanel2_Yesbuttoncaption = "Entendi";
         Confirmpanel2_Confirmationtext = "A Prestadora n�o tem Preposto estabelecido!";
         Confirmpanel2_Title = "Prestadora";
         Dvelop_bootstrap_confirmpanel1_Confirmtype = "0";
         Dvelop_bootstrap_confirmpanel1_Yesbuttoncaption = "Fechar";
         Dvelop_bootstrap_confirmpanel1_Confirmationtext = "";
         Dvelop_bootstrap_confirmpanel1_Title = "Confirma��o";
         Gxuitabspanel_tab_Designtimetabs = "[{\"id\":\"Solicitacao\"},{\"id\":\"Descricao\"},{\"id\":\"TabRequisitos\"}]";
         Gxuitabspanel_tab_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tab_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tab_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tab_Cls = "Tabs";
         Gxuitabspanel_tab_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Nova Ordem de Servi�o";
         subGridrequisitos_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contratadaorigem( GXCombobox dynGX_Parm1 ,
                                           GXCombobox dynGX_Parm2 )
      {
         dynavContratadaorigem = dynGX_Parm1;
         AV105ContratadaOrigem = (int)(NumberUtil.Val( dynavContratadaorigem.CurrentValue, "."));
         dynavContadorfs = dynGX_Parm2;
         AV107ContadorFS = (int)(NumberUtil.Val( dynavContadorfs.CurrentValue, "."));
         GXVvCONTADORFS_htmlCG2( AV105ContratadaOrigem) ;
         dynload_actions( ) ;
         if ( dynavContadorfs.ItemCount > 0 )
         {
            AV107ContadorFS = (int)(NumberUtil.Val( dynavContadorfs.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0))), "."));
         }
         dynavContadorfs.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV107ContadorFS), 6, 0));
         isValidOutput.Add(dynavContadorfs);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Sistema_codigo( GXCombobox cmbGX_Parm1 ,
                                         GXCombobox dynGX_Parm2 ,
                                         GXCombobox dynGX_Parm3 )
      {
         cmbavSistema_codigo = cmbGX_Parm1;
         AV38Sistema_Codigo = (int)(NumberUtil.Val( cmbavSistema_codigo.CurrentValue, "."));
         dynavModulo_codigo = dynGX_Parm2;
         AV39Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.CurrentValue, "."));
         dynavFuncaousuario_codigo = dynGX_Parm3;
         AV41FuncaoUsuario_Codigo = (int)(NumberUtil.Val( dynavFuncaousuario_codigo.CurrentValue, "."));
         GXVvMODULO_CODIGO_htmlCG2( AV38Sistema_Codigo) ;
         GXVvFUNCAOUSUARIO_CODIGO_htmlCG2( AV38Sistema_Codigo) ;
         dynload_actions( ) ;
         if ( dynavModulo_codigo.ItemCount > 0 )
         {
            AV39Modulo_Codigo = (int)(NumberUtil.Val( dynavModulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0))), "."));
         }
         dynavModulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39Modulo_Codigo), 6, 0));
         isValidOutput.Add(dynavModulo_codigo);
         if ( dynavFuncaousuario_codigo.ItemCount > 0 )
         {
            AV41FuncaoUsuario_Codigo = (int)(NumberUtil.Val( dynavFuncaousuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0))), "."));
         }
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41FuncaoUsuario_Codigo), 6, 0));
         isValidOutput.Add(dynavFuncaousuario_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null}],oparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{ctrl:'BTNARTEFATOS',prop:'Caption'},{ctrl:'BTNCONFIRMAR',prop:'Tooltiptext'},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTextblockrequisitadopor_Visible',ctrl:'TEXTBLOCKREQUISITADOPOR',prop:'Visible'},{av:'tblTablemergedrequisitadopor_Visible',ctrl:'TABLEMERGEDREQUISITADOPOR',prop:'Visible'},{av:'AV7Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',hsh:true,nv:''},{av:'AV56Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',hsh:true,nv:''},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavContagemresultado_quantidadesolicitada_Visible',ctrl:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',prop:'Visible'},{av:'cellContagemresultado_quantidadesolicitada_cell_Class',ctrl:'CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_quantidadesolicitada_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'dynavContratadaorigem'},{av:'cellContratadaorigem_cell_Class',ctrl:'CONTRATADAORIGEM_CELL',prop:'Class'},{av:'cellTextblockcontratadaorigem_cell_Class',ctrl:'TEXTBLOCKCONTRATADAORIGEM_CELL',prop:'Class'},{av:'dynavContadorfs'},{av:'cellContadorfs_cell_Class',ctrl:'CONTADORFS_CELL',prop:'Class'},{av:'cellTextblockcontadorfs_cell_Class',ctrl:'TEXTBLOCKCONTADORFS_CELL',prop:'Class'},{av:'edtavSistema_gestor_Visible',ctrl:'vSISTEMA_GESTOR',prop:'Visible'},{av:'cellSistema_gestor_cell_Class',ctrl:'SISTEMA_GESTOR_CELL',prop:'Class'},{av:'cellTextblocksistema_gestor_cell_Class',ctrl:'TEXTBLOCKSISTEMA_GESTOR_CELL',prop:'Class'},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDREQUISITOS.LOAD","{handler:'E31CG2',iparms:[{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null}],oparms:[{av:'AV147btnExcluirReqNeg',fld:'vBTNEXCLUIRREQNEG',pic:'',nv:''},{av:'edtavBtnexcluirreqneg_Tooltiptext',ctrl:'vBTNEXCLUIRREQNEG',prop:'Tooltiptext'},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'edtavAgrupador_Forecolor',ctrl:'vAGRUPADOR',prop:'Forecolor'}]}");
         setEventMetadata("'DOARTEFATOS'","{handler:'E13CG2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'BTNCONFIRMAR',prop:'Visible'}]}");
         setEventMetadata("'DOCONFIRMAR'","{handler:'E14CG2',iparms:[{av:'AV69ContagemResultado_DemandaFM',fld:'vCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88ContagemResultado_Descricao',fld:'vCONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Requisitado',fld:'vREQUISITADO',pic:'',nv:''},{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV101RequisitadoPor',fld:'vREQUISITADOPOR',pic:'',nv:false},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'AV139ContratoVencido',fld:'vCONTRATOVENCIDO',pic:'',nv:false},{av:'cmbavContrato'},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV135PrazoMomento',fld:'vPRAZOMOMENTO',pic:'ZZZ9',nv:0},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'AV125Caller',fld:'vCALLER',pic:'',nv:''},{av:'AV163RequerOrigem',fld:'vREQUERORIGEM',pic:'',nv:false},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'AV105ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV108ServicoGrupo_FlagRequerSoft',fld:'vSERVICOGRUPO_FLAGREQUERSOFT',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1428ServicoGrupo_FlagRequerSoft',fld:'SERVICOGRUPO_FLAGREQUERSOFT',pic:'',nv:false},{av:'AV109Servico_Atende',fld:'vSERVICO_ATENDE',pic:'',nv:''},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV110Servico_ObrigaValores',fld:'vSERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV10Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_Link',fld:'vCONTAGEMRESULTADO_LINK',pic:'',nv:''},{av:'AV23ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV26ContagemResultado_ValorPF',fld:'vCONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV53Contratante_OSAutomatica',fld:'vCONTRATANTE_OSAUTOMATICA',pic:'',nv:false},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''},{av:'AV107ContadorFS',fld:'vCONTADORFS',pic:'ZZZZZ9',nv:0},{av:'AV96Prioridade_Codigo',fld:'vPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV113PercPrazo',fld:'vPERCPRAZO',pic:'ZZ9.99',nv:0.0},{av:'AV114PercValorB',fld:'vPERCVALORB',pic:'ZZ9.99',nv:0.0},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV173ContagemResultado_QuantidadeSolicitada',fld:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',pic:'ZZZ9.9999',nv:0.0},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV61ContratanteSemEmailSda',fld:'vCONTRATANTESEMEMAILSDA',pic:'',nv:false},{av:'AV42Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'cmbavServico_codigo'},{av:'dynavRequisitante'},{av:'AV158Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV46Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV44Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'Gx_msg',fld:'vMSG',pic:'',nv:''},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1135ContratoGestor_UsuarioEhContratante',fld:'CONTRATOGESTOR_USUARIOEHCONTRATANTE',pic:'',nv:false},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],oparms:[{av:'AV108ServicoGrupo_FlagRequerSoft',fld:'vSERVICOGRUPO_FLAGREQUERSOFT',pic:'',nv:false},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV69ContagemResultado_DemandaFM',fld:'vCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'Gx_msg',fld:'vMSG',pic:'',nv:''},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV158Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV44Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'Dvelop_bootstrap_confirmpanel1_Title',ctrl:'DVELOP_BOOTSTRAP_CONFIRMPANEL1',prop:'Title'},{av:'Dvelop_bootstrap_confirmpanel1_Confirmationtext',ctrl:'DVELOP_BOOTSTRAP_CONFIRMPANEL1',prop:'ConfirmationText'},{av:'AV42Usuarios',fld:'vUSUARIOS',pic:'',nv:null}]}");
         setEventMetadata("VSISTEMA_CODIGO.CLICK","{handler:'E15CG2',iparms:[{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'AV167Sistema_Gestor',fld:'vSISTEMA_GESTOR',pic:'',nv:''}]}");
         setEventMetadata("GRIDREQUISITOS.REFRESH","{handler:'E33CG2',iparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null}],oparms:[{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}]}");
         setEventMetadata("VBTNEXCLUIRREQNEG.CLICK","{handler:'E34CG2',iparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],oparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null}]}");
         setEventMetadata("VPFBFSDAOSVNC.ISVALID","{handler:'E16CG2',iparms:[{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''}],oparms:[{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VCONTAGEMRESULTADO_DMNVINCULADA.ISVALID","{handler:'E17CG2',iparms:[{av:'AV23ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'AV133ContagemResultado_DmnVinculadaRef',fld:'vCONTAGEMRESULTADO_DMNVINCULADAREF',pic:'@!',nv:''},{av:'AV135PrazoMomento',fld:'vPRAZOMOMENTO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1044ContagemResultado_FncUsrCod',fld:'CONTAGEMRESULTADO_FNCUSRCOD',pic:'ZZZZZ9',nv:0},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A683ContagemResultado_PFLFMUltima',fld:'CONTAGEMRESULTADO_PFLFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A799ContagemResultado_PFLFSImp',fld:'CONTAGEMRESULTADO_PFLFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A804ContagemResultado_SistemaAtivo',fld:'CONTAGEMRESULTADO_SISTEMAATIVO',pic:'',nv:false},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''}],oparms:[{av:'AV23ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV105ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'lblTextblockpfbfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFBFSDAOSVNC',prop:'Visible'},{av:'lblTextblockpflfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFLFSDAOSVNC',prop:'Visible'},{av:'edtavPfbfsdaosvnc_Visible',ctrl:'vPFBFSDAOSVNC',prop:'Visible'},{av:'edtavPflfsdaosvnc_Visible',ctrl:'vPFLFSDAOSVNC',prop:'Visible'},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'edtavPfbfsdaosvnc_Enabled',ctrl:'vPFBFSDAOSVNC',prop:'Enabled'},{av:'edtavPflfsdaosvnc_Enabled',ctrl:'vPFLFSDAOSVNC',prop:'Enabled'},{av:'dynavContratadaorigem'},{av:'cmbavSistema_codigo'},{av:'dynavModulo_codigo'},{av:'dynavFuncaousuario_codigo'},{av:'edtavContagemresultado_dmnvinculada_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADA',prop:'Visible'},{av:'edtavContagemresultado_dmnvinculadaref_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADAREF',prop:'Visible'},{av:'cmbavDmnvinculadas'},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'AV123DmnVinculadas',fld:'vDMNVINCULADAS',pic:'ZZZZZ9',nv:0},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VCONTAGEMRESULTADO_DMNVINCULADAREF.ISVALID","{handler:'E18CG2',iparms:[{av:'AV23ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'AV133ContagemResultado_DmnVinculadaRef',fld:'vCONTAGEMRESULTADO_DMNVINCULADAREF',pic:'@!',nv:''},{av:'AV135PrazoMomento',fld:'vPRAZOMOMENTO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1044ContagemResultado_FncUsrCod',fld:'CONTAGEMRESULTADO_FNCUSRCOD',pic:'ZZZZZ9',nv:0},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A683ContagemResultado_PFLFMUltima',fld:'CONTAGEMRESULTADO_PFLFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A799ContagemResultado_PFLFSImp',fld:'CONTAGEMRESULTADO_PFLFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A804ContagemResultado_SistemaAtivo',fld:'CONTAGEMRESULTADO_SISTEMAATIVO',pic:'',nv:false},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''}],oparms:[{av:'AV23ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV105ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'lblTextblockpfbfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFBFSDAOSVNC',prop:'Visible'},{av:'lblTextblockpflfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFLFSDAOSVNC',prop:'Visible'},{av:'edtavPfbfsdaosvnc_Visible',ctrl:'vPFBFSDAOSVNC',prop:'Visible'},{av:'edtavPflfsdaosvnc_Visible',ctrl:'vPFLFSDAOSVNC',prop:'Visible'},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'edtavPfbfsdaosvnc_Enabled',ctrl:'vPFBFSDAOSVNC',prop:'Enabled'},{av:'edtavPflfsdaosvnc_Enabled',ctrl:'vPFLFSDAOSVNC',prop:'Enabled'},{av:'dynavContratadaorigem'},{av:'cmbavSistema_codigo'},{av:'dynavModulo_codigo'},{av:'dynavFuncaousuario_codigo'},{av:'edtavContagemresultado_dmnvinculada_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADA',prop:'Visible'},{av:'edtavContagemresultado_dmnvinculadaref_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADAREF',prop:'Visible'},{av:'cmbavDmnvinculadas'},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'AV123DmnVinculadas',fld:'vDMNVINCULADAS',pic:'ZZZZZ9',nv:0},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VDMNVINCULADAS.CLICK","{handler:'E19CG2',iparms:[{av:'AV123DmnVinculadas',fld:'vDMNVINCULADAS',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV133ContagemResultado_DmnVinculadaRef',fld:'vCONTAGEMRESULTADO_DMNVINCULADAREF',pic:'@!',nv:''},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A146Modulo_Codigo',fld:'MODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1044ContagemResultado_FncUsrCod',fld:'CONTAGEMRESULTADO_FNCUSRCOD',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A804ContagemResultado_SistemaAtivo',fld:'CONTAGEMRESULTADO_SISTEMAATIVO',pic:'',nv:false},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A799ContagemResultado_PFLFSImp',fld:'CONTAGEMRESULTADO_PFLFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A683ContagemResultado_PFLFMUltima',fld:'CONTAGEMRESULTADO_PFLFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV105ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'AV23ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'dynavContratadaorigem'},{av:'cmbavSistema_codigo'},{av:'dynavModulo_codigo'},{av:'dynavFuncaousuario_codigo'},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'edtavPfbfsdaosvnc_Visible',ctrl:'vPFBFSDAOSVNC',prop:'Visible'},{av:'edtavPflfsdaosvnc_Visible',ctrl:'vPFLFSDAOSVNC',prop:'Visible'},{av:'edtavPfbfsdaosvnc_Enabled',ctrl:'vPFBFSDAOSVNC',prop:'Enabled'},{av:'edtavPflfsdaosvnc_Enabled',ctrl:'vPFLFSDAOSVNC',prop:'Enabled'},{av:'AV133ContagemResultado_DmnVinculadaRef',fld:'vCONTAGEMRESULTADO_DMNVINCULADAREF',pic:'@!',nv:''},{av:'cmbavDmnvinculadas'},{av:'edtavContagemresultado_dmnvinculada_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADA',prop:'Visible'},{av:'edtavContagemresultado_dmnvinculadaref_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADAREF',prop:'Visible'},{av:'lblTextblockpfbfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFBFSDAOSVNC',prop:'Visible'},{av:'lblTextblockpflfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFLFSDAOSVNC',prop:'Visible'}]}");
         setEventMetadata("VREQUISITADOPOR.CLICK","{handler:'E36CG1',iparms:[{av:'AV101RequisitadoPor',fld:'vREQUISITADOPOR',pic:'',nv:false}],oparms:[{av:'edtavContagemresultado_datadmn_Enabled',ctrl:'vCONTAGEMRESULTADO_DATADMN',prop:'Enabled'},{av:'lblTextblockusuario_pessoanom_Visible',ctrl:'TEXTBLOCKUSUARIO_PESSOANOM',prop:'Visible'},{av:'edtavUsuario_pessoanom_Visible',ctrl:'vUSUARIO_PESSOANOM',prop:'Visible'},{av:'lblTextblockusuario_cargouonom_Visible',ctrl:'TEXTBLOCKUSUARIO_CARGOUONOM',prop:'Visible'},{av:'edtavUsuario_cargouonom_Visible',ctrl:'vUSUARIO_CARGOUONOM',prop:'Visible'},{av:'lblTextblockrequisitante_Visible',ctrl:'TEXTBLOCKREQUISITANTE',prop:'Visible'},{av:'dynavRequisitante'},{ctrl:'BTNCONFIRMAR',prop:'Tooltiptext'}]}");
         setEventMetadata("VREQUISITADO.CLICK","{handler:'E20CG2',iparms:[{av:'AV9Requisitado',fld:'vREQUISITADO',pic:'',nv:''},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1072Servico_Atende',fld:'SERVICO_ATENDE',pic:'',nv:''},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1429Servico_ObrigaValores',fld:'SERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'A2092Servico_IsOrigemReferencia',fld:'SERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'A2094ContratoServicos_SolicitaGestorSistema',fld:'CONTRATOSERVICOS_SOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A2132ContratoServicos_UndCntNome',fld:'CONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'A1340ContratoServicos_QntUntCns',fld:'CONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A557Servico_VlrUnidadeContratada',fld:'SERVICO_VLRUNIDADECONTRATADA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1067ContratoServicosSistemas_ServicoCod',fld:'CONTRATOSERVICOSSISTEMAS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1063ContratoServicosSistemas_SistemaCod',fld:'CONTRATOSERVICOSSISTEMAS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'A1823ContratoServicosPrazo_Momento',fld:'CONTRATOSERVICOSPRAZO_MOMENTO',pic:'ZZZ9',nv:0},{av:'A1265ContratoServicosPrazo_DiasBaixa',fld:'CONTRATOSERVICOSPRAZO_DIASBAIXA',pic:'ZZ9',nv:0},{av:'A1264ContratoServicosPrazo_DiasMedia',fld:'CONTRATOSERVICOSPRAZO_DIASMEDIA',pic:'ZZ9',nv:0},{av:'A1263ContratoServicosPrazo_DiasAlta',fld:'CONTRATOSERVICOSPRAZO_DIASALTA',pic:'ZZ9',nv:0},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1337ContratoServicosPrioridade_Nome',fld:'CONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A1869Contrato_DataTermino',fld:'CONTRATO_DATATERMINO',pic:'',nv:''},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'AV155Contrato_Padrao',fld:'vCONTRATO_PADRAO',pic:'ZZZZZ9',nv:0},{av:'AV162Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A828UsuarioServicos_UsuarioCod',fld:'USUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A829UsuarioServicos_ServicoCod',fld:'USUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109Servico_Atende',fld:'vSERVICO_ATENDE',pic:'',nv:''},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV110Servico_ObrigaValores',fld:'vSERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'AV26ContagemResultado_ValorPF',fld:'vCONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblTextblocksistema_codigo_Visible',ctrl:'TEXTBLOCKSISTEMA_CODIGO',prop:'Visible'},{av:'lblTextblockmodulo_codigo_Visible',ctrl:'TEXTBLOCKMODULO_CODIGO',prop:'Visible'},{av:'lblTextblockfuncaousuario_codigo_Visible',ctrl:'TEXTBLOCKFUNCAOUSUARIO_CODIGO',prop:'Visible'},{av:'cmbavSistema_codigo'},{av:'dynavModulo_codigo'},{av:'dynavFuncaousuario_codigo'},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'lblTextblockpfbfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFBFSDAOSVNC',prop:'Visible'},{av:'lblTextblockpflfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFLFSDAOSVNC',prop:'Visible'},{av:'edtavPfbfsdaosvnc_Visible',ctrl:'vPFBFSDAOSVNC',prop:'Visible'},{av:'edtavPflfsdaosvnc_Visible',ctrl:'vPFLFSDAOSVNC',prop:'Visible'},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV135PrazoMomento',fld:'vPRAZOMOMENTO',pic:'ZZZ9',nv:0},{av:'AV96Prioridade_Codigo',fld:'vPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV139ContratoVencido',fld:'vCONTRATOVENCIDO',pic:'',nv:false},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'lblTextblockprioridade_codigo_Visible',ctrl:'TEXTBLOCKPRIORIDADE_CODIGO',prop:'Visible'},{av:'lblTextblockcomplexidade_Visible',ctrl:'TEXTBLOCKCOMPLEXIDADE',prop:'Visible'},{av:'cmbavComplexidade'},{av:'cmbavPrioridade_codigo'},{av:'Confirmpanel2_Confirmationtext',ctrl:'CONFIRMPANEL2',prop:'ConfirmationText'},{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'},{av:'lblTextblockcontrato_Visible',ctrl:'TEXTBLOCKCONTRATO',prop:'Visible'},{av:'cmbavContrato'},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV10Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'cmbavUsuario_codigo'},{av:'lblTextblockusuario_codigo_Visible',ctrl:'TEXTBLOCKUSUARIO_CODIGO',prop:'Visible'},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{ctrl:'BTNARTEFATOS',prop:'Visible'},{ctrl:'BTNCONFIRMAR',prop:'Visible'},{ctrl:'BTNARTEFATOS',prop:'Caption'}]}");
         setEventMetadata("VSERVICO_CODIGO.CLICK","{handler:'E21CG2',iparms:[{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV9Requisitado',fld:'vREQUISITADO',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A41Contratada_PessoaNom',fld:'CONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1072Servico_Atende',fld:'SERVICO_ATENDE',pic:'',nv:''},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1429Servico_ObrigaValores',fld:'SERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'A2092Servico_IsOrigemReferencia',fld:'SERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'A2094ContratoServicos_SolicitaGestorSistema',fld:'CONTRATOSERVICOS_SOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A2132ContratoServicos_UndCntNome',fld:'CONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'A1340ContratoServicos_QntUntCns',fld:'CONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A557Servico_VlrUnidadeContratada',fld:'SERVICO_VLRUNIDADECONTRATADA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1067ContratoServicosSistemas_ServicoCod',fld:'CONTRATOSERVICOSSISTEMAS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1063ContratoServicosSistemas_SistemaCod',fld:'CONTRATOSERVICOSSISTEMAS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'A1823ContratoServicosPrazo_Momento',fld:'CONTRATOSERVICOSPRAZO_MOMENTO',pic:'ZZZ9',nv:0},{av:'A1265ContratoServicosPrazo_DiasBaixa',fld:'CONTRATOSERVICOSPRAZO_DIASBAIXA',pic:'ZZ9',nv:0},{av:'A1264ContratoServicosPrazo_DiasMedia',fld:'CONTRATOSERVICOSPRAZO_DIASMEDIA',pic:'ZZ9',nv:0},{av:'A1263ContratoServicosPrazo_DiasAlta',fld:'CONTRATOSERVICOSPRAZO_DIASALTA',pic:'ZZ9',nv:0},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1337ContratoServicosPrioridade_Nome',fld:'CONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A1869Contrato_DataTermino',fld:'CONTRATO_DATATERMINO',pic:'',nv:''},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'AV155Contrato_Padrao',fld:'vCONTRATO_PADRAO',pic:'ZZZZZ9',nv:0},{av:'AV162Nome',fld:'vNOME',pic:'@!',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'A71ContratadaUsuario_UsuarioPessoaNom',fld:'CONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1394ContratadaUsuario_UsuarioAtivo',fld:'CONTRATADAUSUARIO_USUARIOATIVO',pic:'',nv:false},{av:'A1908Usuario_DeFerias',fld:'USUARIO_DEFERIAS',pic:'',nv:false},{av:'A828UsuarioServicos_UsuarioCod',fld:'USUARIOSERVICOS_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A829UsuarioServicos_ServicoCod',fld:'USUARIOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A62ContratanteUsuario_UsuarioPessoaNom',fld:'CONTRATANTEUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV105ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'AV107ContadorFS',fld:'vCONTADORFS',pic:'ZZZZZ9',nv:0},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV9Requisitado',fld:'vREQUISITADO',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109Servico_Atende',fld:'vSERVICO_ATENDE',pic:'',nv:''},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV110Servico_ObrigaValores',fld:'vSERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'AV26ContagemResultado_ValorPF',fld:'vCONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblTextblocksistema_codigo_Visible',ctrl:'TEXTBLOCKSISTEMA_CODIGO',prop:'Visible'},{av:'lblTextblockmodulo_codigo_Visible',ctrl:'TEXTBLOCKMODULO_CODIGO',prop:'Visible'},{av:'lblTextblockfuncaousuario_codigo_Visible',ctrl:'TEXTBLOCKFUNCAOUSUARIO_CODIGO',prop:'Visible'},{av:'cmbavSistema_codigo'},{av:'dynavModulo_codigo'},{av:'dynavFuncaousuario_codigo'},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'lblTextblockpfbfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFBFSDAOSVNC',prop:'Visible'},{av:'lblTextblockpflfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFLFSDAOSVNC',prop:'Visible'},{av:'edtavPfbfsdaosvnc_Visible',ctrl:'vPFBFSDAOSVNC',prop:'Visible'},{av:'edtavPflfsdaosvnc_Visible',ctrl:'vPFLFSDAOSVNC',prop:'Visible'},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV135PrazoMomento',fld:'vPRAZOMOMENTO',pic:'ZZZ9',nv:0},{av:'AV96Prioridade_Codigo',fld:'vPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV139ContratoVencido',fld:'vCONTRATOVENCIDO',pic:'',nv:false},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'lblTextblockprioridade_codigo_Visible',ctrl:'TEXTBLOCKPRIORIDADE_CODIGO',prop:'Visible'},{av:'lblTextblockcomplexidade_Visible',ctrl:'TEXTBLOCKCOMPLEXIDADE',prop:'Visible'},{av:'cmbavComplexidade'},{av:'cmbavPrioridade_codigo'},{ctrl:'BTNARTEFATOS',prop:'Visible'},{ctrl:'BTNCONFIRMAR',prop:'Visible'},{av:'edtavContagemresultado_quantidadesolicitada_Visible',ctrl:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',prop:'Visible'},{av:'cellContagemresultado_quantidadesolicitada_cell_Class',ctrl:'CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_quantidadesolicitada_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'dynavContratadaorigem'},{av:'cellContratadaorigem_cell_Class',ctrl:'CONTRATADAORIGEM_CELL',prop:'Class'},{av:'cellTextblockcontratadaorigem_cell_Class',ctrl:'TEXTBLOCKCONTRATADAORIGEM_CELL',prop:'Class'},{av:'dynavContadorfs'},{av:'cellContadorfs_cell_Class',ctrl:'CONTADORFS_CELL',prop:'Class'},{av:'cellTextblockcontadorfs_cell_Class',ctrl:'TEXTBLOCKCONTADORFS_CELL',prop:'Class'},{av:'edtavSistema_gestor_Visible',ctrl:'vSISTEMA_GESTOR',prop:'Visible'},{av:'cellSistema_gestor_cell_Class',ctrl:'SISTEMA_GESTOR_CELL',prop:'Class'},{av:'cellTextblocksistema_gestor_cell_Class',ctrl:'TEXTBLOCKSISTEMA_GESTOR_CELL',prop:'Class'},{av:'Confirmpanel2_Confirmationtext',ctrl:'CONFIRMPANEL2',prop:'ConfirmationText'},{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'},{av:'lblTextblockcontrato_Visible',ctrl:'TEXTBLOCKCONTRATO',prop:'Visible'},{av:'cmbavContrato'},{av:'cmbavContratoservicos_localexec'},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV10Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'cmbavUsuario_codigo'},{av:'lblTextblockusuario_codigo_Visible',ctrl:'TEXTBLOCKUSUARIO_CODIGO',prop:'Visible'},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{ctrl:'BTNARTEFATOS',prop:'Caption'}]}");
         setEventMetadata("VSERVICOGRUPO_CODIGO.CLICK","{handler:'E22CG2',iparms:[{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null}],oparms:[{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VPRAZOENTREGA.ISVALID","{handler:'E23CG2',iparms:[{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'Dvelop_bootstrap_confirmpanel1_Title',ctrl:'DVELOP_BOOTSTRAP_CONFIRMPANEL1',prop:'Title'},{av:'Dvelop_bootstrap_confirmpanel1_Confirmationtext',ctrl:'DVELOP_BOOTSTRAP_CONFIRMPANEL1',prop:'ConfirmationText'},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("DVELOP_BOOTSTRAP_CONFIRMPANEL1.CLOSE","{handler:'E11CG2',iparms:[{av:'Dvelop_bootstrap_confirmpanel1_Title',ctrl:'DVELOP_BOOTSTRAP_CONFIRMPANEL1',prop:'Title'},{av:'AV125Caller',fld:'vCALLER',pic:'',nv:''},{av:'AV53Contratante_OSAutomatica',fld:'vCONTRATANTE_OSAUTOMATICA',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],oparms:[{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'},{av:'AV5ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV23ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Requisitado',fld:'vREQUISITADO',pic:'',nv:''},{av:'AV10Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22ContagemResultado_DmnVinculada',fld:'vCONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'AV133ContagemResultado_DmnVinculadaRef',fld:'vCONTAGEMRESULTADO_DMNVINCULADAREF',pic:'@!',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ContagemResultado_Link',fld:'vCONTAGEMRESULTADO_LINK',pic:'',nv:''},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV96Prioridade_Codigo',fld:'vPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV88ContagemResultado_Descricao',fld:'vCONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'AV101RequisitadoPor',fld:'vREQUISITADOPOR',pic:'',nv:false},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'AV105ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'AV107ContadorFS',fld:'vCONTADORFS',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV123DmnVinculadas',fld:'vDMNVINCULADAS',pic:'ZZZZZ9',nv:0},{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV139ContratoVencido',fld:'vCONTRATOVENCIDO',pic:'',nv:false},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'lblTextblockrequisitante_Visible',ctrl:'TEXTBLOCKREQUISITANTE',prop:'Visible'},{av:'lblTextblockpfbfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFBFSDAOSVNC',prop:'Visible'},{av:'lblTextblockpflfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFLFSDAOSVNC',prop:'Visible'},{av:'edtavPfbfsdaosvnc_Visible',ctrl:'vPFBFSDAOSVNC',prop:'Visible'},{av:'edtavPflfsdaosvnc_Visible',ctrl:'vPFLFSDAOSVNC',prop:'Visible'},{av:'dynavRequisitante'},{av:'cmbavDmnvinculadas'},{av:'lblTextblockcontrato_Visible',ctrl:'TEXTBLOCKCONTRATO',prop:'Visible'},{av:'cmbavContrato'},{av:'edtavContagemresultado_dmnvinculada_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADA',prop:'Visible'},{av:'edtavContagemresultado_dmnvinculadaref_Visible',ctrl:'vCONTAGEMRESULTADO_DMNVINCULADAREF',prop:'Visible'},{av:'cmbavSistema_codigo'},{av:'dynavModulo_codigo'},{av:'dynavContadorfs'},{av:'dynavContratadaorigem'},{av:'dynavFuncaousuario_codigo'},{av:'AV69ContagemResultado_DemandaFM',fld:'vCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'edtavContagemresultado_demandafm_Enabled',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM',prop:'Enabled'},{ctrl:'WCARQUIVOSEVD'},{av:'lblLblultimaos_Caption',ctrl:'LBLULTIMAOS',prop:'Caption'},{av:'lblLblultimaos_Link',ctrl:'LBLULTIMAOS',prop:'Link'},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{ctrl:'BTNARTEFATOS',prop:'Visible'},{ctrl:'BTNCONFIRMAR',prop:'Visible'}]}");
         setEventMetadata("CONFIRMPANEL2.CLOSE","{handler:'E12CG2',iparms:[{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'}],oparms:[{av:'AV9Requisitado',fld:'vREQUISITADO',pic:'',nv:''},{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCOMPLEXIDADE.CLICK","{handler:'E24CG2',iparms:[{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''}],oparms:[{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VPRIORIDADE_CODIGO.CLICK","{handler:'E25CG2',iparms:[{av:'AV96Prioridade_Codigo',fld:'vPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV113PercPrazo',fld:'vPERCPRAZO',pic:'ZZ9.99',nv:0.0},{av:'AV114PercValorB',fld:'vPERCVALORB',pic:'ZZ9.99',nv:0.0},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''}],oparms:[{av:'AV96Prioridade_Codigo',fld:'vPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Dvelop_bootstrap_confirmpanel1_Title',ctrl:'DVELOP_BOOTSTRAP_CONFIRMPANEL1',prop:'Title'},{av:'Dvelop_bootstrap_confirmpanel1_Confirmationtext',ctrl:'DVELOP_BOOTSTRAP_CONFIRMPANEL1',prop:'ConfirmationText'},{av:'AV113PercPrazo',fld:'vPERCPRAZO',pic:'ZZ9.99',nv:0.0},{av:'AV114PercValorB',fld:'vPERCVALORB',pic:'ZZ9.99',nv:0.0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VCONTAGEMRESULTADO_DATADMN.ISVALID","{handler:'E26CG2',iparms:[{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VCONTRATO.CLICK","{handler:'E27CG2',iparms:[{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV12RequisitadoCod',fld:'vREQUISITADOCOD',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1072Servico_Atende',fld:'SERVICO_ATENDE',pic:'',nv:''},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1429Servico_ObrigaValores',fld:'SERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'A2092Servico_IsOrigemReferencia',fld:'SERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'A2094ContratoServicos_SolicitaGestorSistema',fld:'CONTRATOSERVICOS_SOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A2132ContratoServicos_UndCntNome',fld:'CONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'A1340ContratoServicos_QntUntCns',fld:'CONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'A1152ContratoServicos_PrazoAnalise',fld:'CONTRATOSERVICOS_PRAZOANALISE',pic:'ZZZ9',nv:0},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A557Servico_VlrUnidadeContratada',fld:'SERVICO_VLRUNIDADECONTRATADA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A116Contrato_ValorUnidadeContratacao',fld:'CONTRATO_VALORUNIDADECONTRATACAO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1067ContratoServicosSistemas_ServicoCod',fld:'CONTRATOSERVICOSSISTEMAS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1063ContratoServicosSistemas_SistemaCod',fld:'CONTRATOSERVICOSSISTEMAS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'A1823ContratoServicosPrazo_Momento',fld:'CONTRATOSERVICOSPRAZO_MOMENTO',pic:'ZZZ9',nv:0},{av:'A1265ContratoServicosPrazo_DiasBaixa',fld:'CONTRATOSERVICOSPRAZO_DIASBAIXA',pic:'ZZ9',nv:0},{av:'A1264ContratoServicosPrazo_DiasMedia',fld:'CONTRATOSERVICOSPRAZO_DIASMEDIA',pic:'ZZ9',nv:0},{av:'A1263ContratoServicosPrazo_DiasAlta',fld:'CONTRATOSERVICOSPRAZO_DIASALTA',pic:'ZZ9',nv:0},{av:'A1336ContratoServicosPrioridade_Codigo',fld:'CONTRATOSERVICOSPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1337ContratoServicosPrioridade_Nome',fld:'CONTRATOSERVICOSPRIORIDADE_NOME',pic:'@!',nv:''},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A1869Contrato_DataTermino',fld:'CONTRATO_DATATERMINO',pic:'',nv:''},{av:'A1649ContratoServicos_PrazoInicio',fld:'CONTRATOSERVICOS_PRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'A639ContratoServicos_LocalExec',fld:'CONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'A889Servico_Terceriza',fld:'SERVICO_TERCERIZA',pic:'',nv:false},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1751Artefatos_Descricao',fld:'ARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV117Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV70PrazoAnalise',fld:'vPRAZOANALISE',pic:'99/99/99 99:99',nv:''},{av:'AV62PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV63EntregaCalculada',fld:'vENTREGACALCULADA',pic:'99/99/99 99:99',nv:''},{av:'AV49ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV109Servico_Atende',fld:'vSERVICO_ATENDE',pic:'',nv:''},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV110Servico_ObrigaValores',fld:'vSERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'AV98DiasAnalise',fld:'vDIASANALISE',pic:'ZZZ9',nv:0},{av:'AV84PrazoTipo',fld:'vPRAZOTIPO',pic:'',nv:''},{av:'AV115TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'A913ContratoServicos_PrazoTipo',fld:'CONTRATOSERVICOS_PRAZOTIPO',pic:'',nv:''},{av:'AV26ContagemResultado_ValorPF',fld:'vCONTAGEMRESULTADO_VALORPF',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblTextblocksistema_codigo_Visible',ctrl:'TEXTBLOCKSISTEMA_CODIGO',prop:'Visible'},{av:'lblTextblockmodulo_codigo_Visible',ctrl:'TEXTBLOCKMODULO_CODIGO',prop:'Visible'},{av:'lblTextblockfuncaousuario_codigo_Visible',ctrl:'TEXTBLOCKFUNCAOUSUARIO_CODIGO',prop:'Visible'},{av:'cmbavSistema_codigo'},{av:'dynavModulo_codigo'},{av:'dynavFuncaousuario_codigo'},{av:'AV66PFBFSdaOSVnc',fld:'vPFBFSDAOSVNC',pic:'ZZZZZZZ9.999',nv:0.0},{av:'AV73PFLFSdaOSVnc',fld:'vPFLFSDAOSVNC',pic:'ZZZZZZZ9.99999',nv:0.0},{av:'lblTextblockpfbfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFBFSDAOSVNC',prop:'Visible'},{av:'lblTextblockpflfsdaosvnc_Visible',ctrl:'TEXTBLOCKPFLFSDAOSVNC',prop:'Visible'},{av:'edtavPfbfsdaosvnc_Visible',ctrl:'vPFBFSDAOSVNC',prop:'Visible'},{av:'edtavPflfsdaosvnc_Visible',ctrl:'vPFLFSDAOSVNC',prop:'Visible'},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39Modulo_Codigo',fld:'vMODULO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV97Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV135PrazoMomento',fld:'vPRAZOMOMENTO',pic:'ZZZ9',nv:0},{av:'AV96Prioridade_Codigo',fld:'vPRIORIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV139ContratoVencido',fld:'vCONTRATOVENCIDO',pic:'',nv:false},{av:'AV122PrazoInicio',fld:'vPRAZOINICIO',pic:'ZZZ9',nv:0},{av:'lblTextblockprioridade_codigo_Visible',ctrl:'TEXTBLOCKPRIORIDADE_CODIGO',prop:'Visible'},{av:'lblTextblockcomplexidade_Visible',ctrl:'TEXTBLOCKCOMPLEXIDADE',prop:'Visible'},{av:'cmbavComplexidade'},{av:'cmbavPrioridade_codigo'},{av:'AV8ContagemResultado_DataDmn',fld:'vCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV85LerServico',fld:'vLERSERVICO',pic:'',nv:false},{av:'AV18ContratoServicos_LocalExec',fld:'vCONTRATOSERVICOS_LOCALEXEC',pic:'',nv:''},{av:'cmbavContratoservicos_localexec'},{av:'AV67Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{ctrl:'BTNARTEFATOS',prop:'Visible'},{ctrl:'BTNCONFIRMAR',prop:'Visible'},{ctrl:'BTNARTEFATOS',prop:'Caption'}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E35CG2',iparms:[],oparms:[]}");
         setEventMetadata("VREQUISITANTE.CLICK","{handler:'E28CG2',iparms:[{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null}],oparms:[{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTRATADAORIGEM.CLICK","{handler:'E29CG2',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV105ContratadaOrigem',fld:'vCONTRATADAORIGEM',pic:'ZZZZZ9',nv:0},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'dynavContratadaorigem'}],oparms:[{av:'Confirmpanel2_Confirmationtext',ctrl:'CONFIRMPANEL2',prop:'ConfirmationText'},{av:'Confirmpanel2_Title',ctrl:'CONFIRMPANEL2',prop:'Title'}]}");
         setEventMetadata("GRIDREQUISITOS_FIRSTPAGE","{handler:'subgridrequisitos_firstpage',iparms:[{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null}],oparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{ctrl:'BTNARTEFATOS',prop:'Caption'},{ctrl:'BTNCONFIRMAR',prop:'Tooltiptext'},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTextblockrequisitadopor_Visible',ctrl:'TEXTBLOCKREQUISITADOPOR',prop:'Visible'},{av:'tblTablemergedrequisitadopor_Visible',ctrl:'TABLEMERGEDREQUISITADOPOR',prop:'Visible'},{av:'AV7Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',hsh:true,nv:''},{av:'AV56Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',hsh:true,nv:''},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavContagemresultado_quantidadesolicitada_Visible',ctrl:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',prop:'Visible'},{av:'cellContagemresultado_quantidadesolicitada_cell_Class',ctrl:'CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_quantidadesolicitada_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'dynavContratadaorigem'},{av:'cellContratadaorigem_cell_Class',ctrl:'CONTRATADAORIGEM_CELL',prop:'Class'},{av:'cellTextblockcontratadaorigem_cell_Class',ctrl:'TEXTBLOCKCONTRATADAORIGEM_CELL',prop:'Class'},{av:'dynavContadorfs'},{av:'cellContadorfs_cell_Class',ctrl:'CONTADORFS_CELL',prop:'Class'},{av:'cellTextblockcontadorfs_cell_Class',ctrl:'TEXTBLOCKCONTADORFS_CELL',prop:'Class'},{av:'edtavSistema_gestor_Visible',ctrl:'vSISTEMA_GESTOR',prop:'Visible'},{av:'cellSistema_gestor_cell_Class',ctrl:'SISTEMA_GESTOR_CELL',prop:'Class'},{av:'cellTextblocksistema_gestor_cell_Class',ctrl:'TEXTBLOCKSISTEMA_GESTOR_CELL',prop:'Class'},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDREQUISITOS_PREVPAGE","{handler:'subgridrequisitos_previouspage',iparms:[{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null}],oparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{ctrl:'BTNARTEFATOS',prop:'Caption'},{ctrl:'BTNCONFIRMAR',prop:'Tooltiptext'},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTextblockrequisitadopor_Visible',ctrl:'TEXTBLOCKREQUISITADOPOR',prop:'Visible'},{av:'tblTablemergedrequisitadopor_Visible',ctrl:'TABLEMERGEDREQUISITADOPOR',prop:'Visible'},{av:'AV7Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',hsh:true,nv:''},{av:'AV56Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',hsh:true,nv:''},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavContagemresultado_quantidadesolicitada_Visible',ctrl:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',prop:'Visible'},{av:'cellContagemresultado_quantidadesolicitada_cell_Class',ctrl:'CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_quantidadesolicitada_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'dynavContratadaorigem'},{av:'cellContratadaorigem_cell_Class',ctrl:'CONTRATADAORIGEM_CELL',prop:'Class'},{av:'cellTextblockcontratadaorigem_cell_Class',ctrl:'TEXTBLOCKCONTRATADAORIGEM_CELL',prop:'Class'},{av:'dynavContadorfs'},{av:'cellContadorfs_cell_Class',ctrl:'CONTADORFS_CELL',prop:'Class'},{av:'cellTextblockcontadorfs_cell_Class',ctrl:'TEXTBLOCKCONTADORFS_CELL',prop:'Class'},{av:'edtavSistema_gestor_Visible',ctrl:'vSISTEMA_GESTOR',prop:'Visible'},{av:'cellSistema_gestor_cell_Class',ctrl:'SISTEMA_GESTOR_CELL',prop:'Class'},{av:'cellTextblocksistema_gestor_cell_Class',ctrl:'TEXTBLOCKSISTEMA_GESTOR_CELL',prop:'Class'},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDREQUISITOS_NEXTPAGE","{handler:'subgridrequisitos_nextpage',iparms:[{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null}],oparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{ctrl:'BTNARTEFATOS',prop:'Caption'},{ctrl:'BTNCONFIRMAR',prop:'Tooltiptext'},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTextblockrequisitadopor_Visible',ctrl:'TEXTBLOCKREQUISITADOPOR',prop:'Visible'},{av:'tblTablemergedrequisitadopor_Visible',ctrl:'TABLEMERGEDREQUISITADOPOR',prop:'Visible'},{av:'AV7Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',hsh:true,nv:''},{av:'AV56Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',hsh:true,nv:''},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavContagemresultado_quantidadesolicitada_Visible',ctrl:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',prop:'Visible'},{av:'cellContagemresultado_quantidadesolicitada_cell_Class',ctrl:'CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_quantidadesolicitada_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'dynavContratadaorigem'},{av:'cellContratadaorigem_cell_Class',ctrl:'CONTRATADAORIGEM_CELL',prop:'Class'},{av:'cellTextblockcontratadaorigem_cell_Class',ctrl:'TEXTBLOCKCONTRATADAORIGEM_CELL',prop:'Class'},{av:'dynavContadorfs'},{av:'cellContadorfs_cell_Class',ctrl:'CONTADORFS_CELL',prop:'Class'},{av:'cellTextblockcontadorfs_cell_Class',ctrl:'TEXTBLOCKCONTADORFS_CELL',prop:'Class'},{av:'edtavSistema_gestor_Visible',ctrl:'vSISTEMA_GESTOR',prop:'Visible'},{av:'cellSistema_gestor_cell_Class',ctrl:'SISTEMA_GESTOR_CELL',prop:'Class'},{av:'cellTextblocksistema_gestor_cell_Class',ctrl:'TEXTBLOCKSISTEMA_GESTOR_CELL',prop:'Class'},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDREQUISITOS_LASTPAGE","{handler:'subgridrequisitos_lastpage',iparms:[{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'subGridrequisitos_Rows',nv:0},{av:'AV146Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV134Contratante_UsaOSistema',fld:'vCONTRATANTE_USAOSISTEMA',pic:'',nv:false},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A75Contrato_AreaTrabalhoCod',fld:'CONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A638ContratoServicos_Ativo',fld:'CONTRATOSERVICOS_ATIVO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A135Sistema_AreaTrabalhoCod',fld:'SISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A699Sistema_Tipo',fld:'SISTEMA_TIPO',pic:'',nv:''},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV51Sistemas',fld:'vSISTEMAS',pic:'',nv:null},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV166Servico_IsOrigemReferencia',fld:'vSERVICO_ISORIGEMREFERENCIA',pic:'',nv:false},{av:'AV168Servico_IsSolicitaGestorSistema',fld:'vSERVICO_ISSOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A1446ContratoGestor_ContratadaAreaCod',fld:'CONTRATOGESTOR_CONTRATADAAREACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV102Requisitante',fld:'vREQUISITANTE',pic:'ZZZZZ9',nv:0},{av:'A843Contrato_DataFimTA',fld:'CONTRATO_DATAFIMTA',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A43Contratada_Ativo',fld:'CONTRATADA_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null}],oparms:[{av:'AV161SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:219,pic:'',nv:null},{av:'AV19ContagemResultado_Observacao',fld:'vCONTAGEMRESULTADO_OBSERVACAO',pic:'',nv:''},{ctrl:'BTNARTEFATOS',prop:'Caption'},{ctrl:'BTNCONFIRMAR',prop:'Tooltiptext'},{av:'AV100AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTextblockrequisitadopor_Visible',ctrl:'TEXTBLOCKREQUISITADOPOR',prop:'Visible'},{av:'tblTablemergedrequisitadopor_Visible',ctrl:'TABLEMERGEDREQUISITADOPOR',prop:'Visible'},{av:'AV7Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',hsh:true,nv:''},{av:'AV56Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',hsh:true,nv:''},{av:'AV132ContratadasDaArea',fld:'vCONTRATADASDAAREA',pic:'',nv:null},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtavContagemresultado_quantidadesolicitada_Visible',ctrl:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',prop:'Visible'},{av:'cellContagemresultado_quantidadesolicitada_cell_Class',ctrl:'CONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'cellTextblockcontagemresultado_quantidadesolicitada_cell_Class',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA_CELL',prop:'Class'},{av:'dynavContratadaorigem'},{av:'cellContratadaorigem_cell_Class',ctrl:'CONTRATADAORIGEM_CELL',prop:'Class'},{av:'cellTextblockcontratadaorigem_cell_Class',ctrl:'TEXTBLOCKCONTRATADAORIGEM_CELL',prop:'Class'},{av:'dynavContadorfs'},{av:'cellContadorfs_cell_Class',ctrl:'CONTADORFS_CELL',prop:'Class'},{av:'cellTextblockcontadorfs_cell_Class',ctrl:'TEXTBLOCKCONTADORFS_CELL',prop:'Class'},{av:'edtavSistema_gestor_Visible',ctrl:'vSISTEMA_GESTOR',prop:'Visible'},{av:'cellSistema_gestor_cell_Class',ctrl:'SISTEMA_GESTOR_CELL',prop:'Class'},{av:'cellTextblocksistema_gestor_cell_Class',ctrl:'TEXTBLOCKSISTEMA_GESTOR_CELL',prop:'Class'},{av:'AV136Servicos',fld:'vSERVICOS',pic:'',nv:null},{av:'AV137SDT_Servicos',fld:'vSDT_SERVICOS',pic:'',nv:null},{av:'AV170ServicoCodigo',fld:'vSERVICOCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV171ContratoServicos_UndCntNome',fld:'vCONTRATOSERVICOS_UNDCNTNOME',pic:'@!',nv:''},{av:'AV172ContratoServicos_QntUntCns',fld:'vCONTRATOSERVICOS_QNTUNTCNS',pic:'ZZZ9.9999',nv:0.0},{av:'AV13Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV134Contratante_UsaOSistema = false;
         A58Usuario_PessoaNom = "";
         A1076Usuario_CargoUONom = "";
         AV132ContratadasDaArea = new GxSimpleCollection();
         A158ServicoGrupo_Descricao = "";
         A129Sistema_Sigla = "";
         A699Sistema_Tipo = "";
         AV51Sistemas = new GxSimpleCollection();
         AV168Servico_IsSolicitaGestorSistema = false;
         A843Contrato_DataFimTA = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         AV136Servicos = new GxSimpleCollection();
         A605Servico_Sigla = "";
         A608Servico_Nome = "";
         AV137SDT_Servicos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs");
         AV161SDT_Requisitos = new GxObjectCollection( context, "SDT_Requisitos.SDT_RequisitosItem", "GxEv3Up14_Meetrika", "SdtSDT_Requisitos_SDT_RequisitosItem", "GeneXus.Programs");
         AV146Agrupador = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV19ContagemResultado_Observacao = "";
         AV84PrazoTipo = "";
         AV63EntregaCalculada = (DateTime)(DateTime.MinValue);
         AV125Caller = "";
         AV163RequerOrigem = true;
         AV5ContagemResultado_Demanda = "";
         A457ContagemResultado_Demanda = "";
         Gx_date = DateTime.MinValue;
         AV42Usuarios = new GxSimpleCollection();
         AV158Codigos = new GxSimpleCollection();
         AV46Attachments = new GxSimpleCollection();
         AV44Resultado = "";
         Gx_msg = "";
         A639ContratoServicos_LocalExec = "";
         A1454ContratoServicos_PrazoTpDias = "";
         AV115TipoDias = "";
         AV122PrazoInicio = 1;
         A493ContagemResultado_DemandaFM = "";
         A484ContagemResultado_StatusDmn = "";
         A801ContagemResultado_ServicoSigla = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         A803ContagemResultado_ContratadaSigla = "";
         A1072Servico_Atende = "";
         A1429Servico_ObrigaValores = "";
         A2132ContratoServicos_UndCntNome = "";
         A913ContratoServicos_PrazoTipo = "";
         A1337ContratoServicosPrioridade_Nome = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         A77Contrato_Numero = "";
         AV162Nome = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         A1751Artefatos_Descricao = "";
         A41Contratada_PessoaNom = "";
         AV56Usuario_CargoUONom = "";
         AV7Usuario_PessoaNom = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV110Servico_ObrigaValores = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV147btnExcluirReqNeg = "";
         AV182Btnexcluirreqneg_GXI = "";
         OldWcarquivosevd = "";
         WebComp_Wcarquivosevd_Component = "";
         AV9Requisitado = "";
         AV18ContratoServicos_LocalExec = "";
         GXCCtl = "";
         AV109Servico_Atende = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00CG3_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00CG3_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00CG3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00CG3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00CG3_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00CG3_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00CG3_A54Usuario_Ativo = new bool[] {false} ;
         H00CG3_n54Usuario_Ativo = new bool[] {false} ;
         H00CG3_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H00CG3_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00CG4_A40Contratada_PessoaCod = new int[1] ;
         H00CG4_A39Contratada_Codigo = new int[1] ;
         H00CG4_n39Contratada_Codigo = new bool[] {false} ;
         H00CG4_A41Contratada_PessoaNom = new String[] {""} ;
         H00CG4_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CG4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CG4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CG4_A43Contratada_Ativo = new bool[] {false} ;
         H00CG4_n43Contratada_Ativo = new bool[] {false} ;
         H00CG5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00CG5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00CG5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00CG5_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00CG5_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00CG5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00CG5_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00CG5_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00CG6_A146Modulo_Codigo = new int[1] ;
         H00CG6_n146Modulo_Codigo = new bool[] {false} ;
         H00CG6_A145Modulo_Sigla = new String[] {""} ;
         H00CG6_A127Sistema_Codigo = new int[1] ;
         H00CG7_A161FuncaoUsuario_Codigo = new int[1] ;
         H00CG7_A162FuncaoUsuario_Nome = new String[] {""} ;
         H00CG7_A127Sistema_Codigo = new int[1] ;
         H00CG7_A164FuncaoUsuario_Ativo = new bool[] {false} ;
         GridrequisitosContainer = new GXWebGrid( context);
         AV69ContagemResultado_DemandaFM = "";
         AV8ContagemResultado_DataDmn = DateTime.MinValue;
         AV88ContagemResultado_Descricao = "";
         AV171ContratoServicos_UndCntNome = "";
         AV22ContagemResultado_DmnVinculada = "";
         AV133ContagemResultado_DmnVinculadaRef = "";
         AV70PrazoAnalise = (DateTime)(DateTime.MinValue);
         AV62PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV167Sistema_Gestor = "";
         AV15ContagemResultado_Link = "";
         AV29Websession = context.GetSession();
         H00CG8_A29Contratante_Codigo = new int[1] ;
         H00CG8_n29Contratante_Codigo = new bool[] {false} ;
         H00CG8_A5AreaTrabalho_Codigo = new int[1] ;
         H00CG8_A593Contratante_OSAutomatica = new bool[] {false} ;
         H00CG8_A2085Contratante_RequerOrigem = new bool[] {false} ;
         H00CG8_n2085Contratante_RequerOrigem = new bool[] {false} ;
         H00CG8_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00CG8_A548Contratante_EmailSdaUser = new String[] {""} ;
         H00CG8_n548Contratante_EmailSdaUser = new bool[] {false} ;
         H00CG8_A552Contratante_EmailSdaPort = new short[1] ;
         H00CG8_n552Contratante_EmailSdaPort = new bool[] {false} ;
         H00CG8_A547Contratante_EmailSdaHost = new String[] {""} ;
         H00CG8_n547Contratante_EmailSdaHost = new bool[] {false} ;
         H00CG8_A1822Contratante_UsaOSistema = new bool[] {false} ;
         H00CG8_n1822Contratante_UsaOSistema = new bool[] {false} ;
         A6AreaTrabalho_Descricao = "";
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         AV89FCKEditorMenuItem = new SdtFckEditorMenu_FckEditorMenuItem(context);
         AV92Url = "";
         AV91FckEditorMenu = new GxObjectCollection( context, "FckEditorMenu.FckEditorMenuItem", "GxEv3Up14_Meetrika", "SdtFckEditorMenu_FckEditorMenuItem", "GeneXus.Programs");
         GridrequisitosRow = new GXWebRow();
         H00CG9_A456ContagemResultado_Codigo = new int[1] ;
         H00CG9_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CG9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CG9_A457ContagemResultado_Demanda = new String[] {""} ;
         H00CG9_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00CG10_A157ServicoGrupo_Codigo = new int[1] ;
         H00CG10_A1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         H00CG10_n1428ServicoGrupo_FlagRequerSoft = new bool[] {false} ;
         AV128sdt_Artefatos = new GxObjectCollection( context, "SDT_Artefatos", "GxEv3Up14_Meetrika", "SdtSDT_Artefatos", "GeneXus.Programs");
         H00CG11_A57Usuario_PessoaCod = new int[1] ;
         H00CG11_A1073Usuario_CargoCod = new int[1] ;
         H00CG11_n1073Usuario_CargoCod = new bool[] {false} ;
         H00CG11_A1Usuario_Codigo = new int[1] ;
         H00CG11_A58Usuario_PessoaNom = new String[] {""} ;
         H00CG11_n58Usuario_PessoaNom = new bool[] {false} ;
         H00CG11_A1075Usuario_CargoUOCod = new int[1] ;
         H00CG11_n1075Usuario_CargoUOCod = new bool[] {false} ;
         H00CG11_A1076Usuario_CargoUONom = new String[] {""} ;
         H00CG11_n1076Usuario_CargoUONom = new bool[] {false} ;
         H00CG13_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00CG13_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00CG13_A601ContagemResultado_Servico = new int[1] ;
         H00CG13_n601ContagemResultado_Servico = new bool[] {false} ;
         H00CG13_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00CG13_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00CG13_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00CG13_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00CG13_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CG13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CG13_A456ContagemResultado_Codigo = new int[1] ;
         H00CG13_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00CG13_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00CG13_A489ContagemResultado_SistemaCod = new int[1] ;
         H00CG13_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00CG13_A146Modulo_Codigo = new int[1] ;
         H00CG13_n146Modulo_Codigo = new bool[] {false} ;
         H00CG13_A1044ContagemResultado_FncUsrCod = new int[1] ;
         H00CG13_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         H00CG13_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00CG13_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00CG13_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00CG13_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00CG13_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00CG13_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00CG13_A804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG13_n804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG13_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00CG13_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00CG13_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00CG13_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00CG13_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00CG13_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         AV60ServicoVnc_Sigla = "";
         H00CG15_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00CG15_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00CG15_A601ContagemResultado_Servico = new int[1] ;
         H00CG15_n601ContagemResultado_Servico = new bool[] {false} ;
         H00CG15_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00CG15_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00CG15_A457ContagemResultado_Demanda = new String[] {""} ;
         H00CG15_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00CG15_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CG15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CG15_A456ContagemResultado_Codigo = new int[1] ;
         H00CG15_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00CG15_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00CG15_A489ContagemResultado_SistemaCod = new int[1] ;
         H00CG15_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00CG15_A146Modulo_Codigo = new int[1] ;
         H00CG15_n146Modulo_Codigo = new bool[] {false} ;
         H00CG15_A1044ContagemResultado_FncUsrCod = new int[1] ;
         H00CG15_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         H00CG15_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00CG15_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00CG15_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00CG15_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00CG15_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00CG15_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00CG15_A804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG15_n804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG15_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00CG15_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00CG15_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00CG15_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00CG15_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00CG15_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00CG15_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00CG15_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00CG17_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00CG17_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00CG17_A601ContagemResultado_Servico = new int[1] ;
         H00CG17_n601ContagemResultado_Servico = new bool[] {false} ;
         H00CG17_A456ContagemResultado_Codigo = new int[1] ;
         H00CG17_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00CG17_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00CG17_A457ContagemResultado_Demanda = new String[] {""} ;
         H00CG17_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00CG17_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00CG17_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00CG17_A489ContagemResultado_SistemaCod = new int[1] ;
         H00CG17_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00CG17_A146Modulo_Codigo = new int[1] ;
         H00CG17_n146Modulo_Codigo = new bool[] {false} ;
         H00CG17_A1044ContagemResultado_FncUsrCod = new int[1] ;
         H00CG17_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         H00CG17_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CG17_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CG17_A804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG17_n804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG17_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00CG17_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00CG17_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00CG17_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00CG17_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00CG17_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00CG17_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00CG17_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00CG17_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00CG17_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         AV116Servico_Nome = "";
         H00CG18_A74Contrato_Codigo = new int[1] ;
         H00CG18_n74Contrato_Codigo = new bool[] {false} ;
         H00CG18_A1013Contrato_PrepostoCod = new int[1] ;
         H00CG18_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00CG18_A39Contratada_Codigo = new int[1] ;
         H00CG18_n39Contratada_Codigo = new bool[] {false} ;
         H00CG21_A39Contratada_Codigo = new int[1] ;
         H00CG21_n39Contratada_Codigo = new bool[] {false} ;
         H00CG21_A1013Contrato_PrepostoCod = new int[1] ;
         H00CG21_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00CG21_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         H00CG21_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00CG21_A160ContratoServicos_Codigo = new int[1] ;
         H00CG21_A155Servico_Codigo = new int[1] ;
         H00CG21_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CG21_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CG21_A43Contratada_Ativo = new bool[] {false} ;
         H00CG21_n43Contratada_Ativo = new bool[] {false} ;
         H00CG21_A54Usuario_Ativo = new bool[] {false} ;
         H00CG21_n54Usuario_Ativo = new bool[] {false} ;
         H00CG21_A74Contrato_Codigo = new int[1] ;
         H00CG21_n74Contrato_Codigo = new bool[] {false} ;
         H00CG21_A1072Servico_Atende = new String[] {""} ;
         H00CG21_n1072Servico_Atende = new bool[] {false} ;
         H00CG21_A157ServicoGrupo_Codigo = new int[1] ;
         H00CG21_A1429Servico_ObrigaValores = new String[] {""} ;
         H00CG21_n1429Servico_ObrigaValores = new bool[] {false} ;
         H00CG21_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         H00CG21_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00CG21_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         H00CG21_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         H00CG21_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         H00CG21_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         H00CG21_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         H00CG21_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         H00CG21_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         H00CG21_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         H00CG21_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         H00CG21_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H00CG21_A1649ContratoServicos_PrazoInicio = new short[1] ;
         H00CG21_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         H00CG21_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         H00CG21_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         H00CG21_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00CG21_n843Contrato_DataFimTA = new bool[] {false} ;
         H00CG21_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00CG21_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00CG22_A160ContratoServicos_Codigo = new int[1] ;
         H00CG22_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         H00CG22_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         H00CG23_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00CG23_A1823ContratoServicosPrazo_Momento = new short[1] ;
         H00CG23_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         H00CG23_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         H00CG23_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         H00CG23_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         H00CG23_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         H00CG23_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         H00CG23_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         H00CG24_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         H00CG24_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         H00CG24_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         H00CG25_A155Servico_Codigo = new int[1] ;
         H00CG25_A160ContratoServicos_Codigo = new int[1] ;
         H00CG25_A639ContratoServicos_LocalExec = new String[] {""} ;
         H00CG25_A889Servico_Terceriza = new bool[] {false} ;
         H00CG25_n889Servico_Terceriza = new bool[] {false} ;
         H00CG25_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         H00CG25_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         H00CG25_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         H00CG25_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         H00CG25_A1649ContratoServicos_PrazoInicio = new short[1] ;
         H00CG25_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         GXt_char3 = "";
         GXt_char2 = "";
         AV45EmailText = "";
         AV119Demandante = "";
         AV118NomeSistema = "";
         AV43Subject = "";
         H00CG26_A39Contratada_Codigo = new int[1] ;
         H00CG26_n39Contratada_Codigo = new bool[] {false} ;
         H00CG26_A74Contrato_Codigo = new int[1] ;
         H00CG26_n74Contrato_Codigo = new bool[] {false} ;
         H00CG26_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CG26_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CG26_A43Contratada_Ativo = new bool[] {false} ;
         H00CG26_n43Contratada_Ativo = new bool[] {false} ;
         H00CG26_A54Usuario_Ativo = new bool[] {false} ;
         H00CG26_n54Usuario_Ativo = new bool[] {false} ;
         H00CG26_A1013Contrato_PrepostoCod = new int[1] ;
         H00CG26_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00CG27_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00CG27_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00CG27_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00CG27_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00CG28_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00CG28_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00CG28_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00CG28_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         AV131Contratadas = new GxSimpleCollection();
         H00CG31_A160ContratoServicos_Codigo = new int[1] ;
         H00CG31_A74Contrato_Codigo = new int[1] ;
         H00CG31_n74Contrato_Codigo = new bool[] {false} ;
         H00CG31_A40Contratada_PessoaCod = new int[1] ;
         H00CG31_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00CG31_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00CG31_A155Servico_Codigo = new int[1] ;
         H00CG31_A43Contratada_Ativo = new bool[] {false} ;
         H00CG31_n43Contratada_Ativo = new bool[] {false} ;
         H00CG31_A92Contrato_Ativo = new bool[] {false} ;
         H00CG31_n92Contrato_Ativo = new bool[] {false} ;
         H00CG31_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00CG31_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00CG31_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00CG31_A39Contratada_Codigo = new int[1] ;
         H00CG31_n39Contratada_Codigo = new bool[] {false} ;
         H00CG31_A41Contratada_PessoaNom = new String[] {""} ;
         H00CG31_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CG31_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00CG31_n843Contrato_DataFimTA = new bool[] {false} ;
         H00CG34_A160ContratoServicos_Codigo = new int[1] ;
         H00CG34_A74Contrato_Codigo = new int[1] ;
         H00CG34_n74Contrato_Codigo = new bool[] {false} ;
         H00CG34_A40Contratada_PessoaCod = new int[1] ;
         H00CG34_A43Contratada_Ativo = new bool[] {false} ;
         H00CG34_n43Contratada_Ativo = new bool[] {false} ;
         H00CG34_A92Contrato_Ativo = new bool[] {false} ;
         H00CG34_n92Contrato_Ativo = new bool[] {false} ;
         H00CG34_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00CG34_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00CG34_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00CG34_A155Servico_Codigo = new int[1] ;
         H00CG34_A39Contratada_Codigo = new int[1] ;
         H00CG34_n39Contratada_Codigo = new bool[] {false} ;
         H00CG34_A41Contratada_PessoaNom = new String[] {""} ;
         H00CG34_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CG34_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00CG34_n843Contrato_DataFimTA = new bool[] {false} ;
         H00CG35_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00CG35_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00CG35_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00CG35_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00CG35_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00CG35_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00CG35_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00CG35_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00CG35_A43Contratada_Ativo = new bool[] {false} ;
         H00CG35_n43Contratada_Ativo = new bool[] {false} ;
         H00CG35_A1908Usuario_DeFerias = new bool[] {false} ;
         H00CG35_n1908Usuario_DeFerias = new bool[] {false} ;
         H00CG36_A828UsuarioServicos_UsuarioCod = new int[1] ;
         H00CG36_A829UsuarioServicos_ServicoCod = new int[1] ;
         H00CG37_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00CG37_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00CG37_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00CG37_A1908Usuario_DeFerias = new bool[] {false} ;
         H00CG37_n1908Usuario_DeFerias = new bool[] {false} ;
         H00CG37_A54Usuario_Ativo = new bool[] {false} ;
         H00CG37_n54Usuario_Ativo = new bool[] {false} ;
         H00CG37_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00CG37_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00CG37_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00CG38_A155Servico_Codigo = new int[1] ;
         H00CG38_A74Contrato_Codigo = new int[1] ;
         H00CG38_n74Contrato_Codigo = new bool[] {false} ;
         H00CG38_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00CG38_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00CG38_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00CG38_A92Contrato_Ativo = new bool[] {false} ;
         H00CG38_n92Contrato_Ativo = new bool[] {false} ;
         H00CG38_A157ServicoGrupo_Codigo = new int[1] ;
         H00CG38_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00CG39_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H00CG39_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H00CG39_A1013Contrato_PrepostoCod = new int[1] ;
         H00CG39_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00CG39_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         H00CG39_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         H00CG39_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00CG39_A54Usuario_Ativo = new bool[] {false} ;
         H00CG39_n54Usuario_Ativo = new bool[] {false} ;
         H00CG39_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CG39_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CG39_A43Contratada_Ativo = new bool[] {false} ;
         H00CG39_n43Contratada_Ativo = new bool[] {false} ;
         H00CG39_A92Contrato_Ativo = new bool[] {false} ;
         H00CG39_n92Contrato_Ativo = new bool[] {false} ;
         H00CG39_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00CG39_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00CG39_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00CG40_A160ContratoServicos_Codigo = new int[1] ;
         H00CG40_A74Contrato_Codigo = new int[1] ;
         H00CG40_n74Contrato_Codigo = new bool[] {false} ;
         H00CG40_A157ServicoGrupo_Codigo = new int[1] ;
         H00CG40_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00CG40_A155Servico_Codigo = new int[1] ;
         H00CG41_A1013Contrato_PrepostoCod = new int[1] ;
         H00CG41_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00CG41_A39Contratada_Codigo = new int[1] ;
         H00CG41_n39Contratada_Codigo = new bool[] {false} ;
         H00CG41_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00CG41_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         H00CG41_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         H00CG41_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CG41_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CG41_A54Usuario_Ativo = new bool[] {false} ;
         H00CG41_n54Usuario_Ativo = new bool[] {false} ;
         H00CG41_A43Contratada_Ativo = new bool[] {false} ;
         H00CG41_n43Contratada_Ativo = new bool[] {false} ;
         H00CG41_A92Contrato_Ativo = new bool[] {false} ;
         H00CG41_n92Contrato_Ativo = new bool[] {false} ;
         H00CG41_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00CG41_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00CG41_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00CG42_A160ContratoServicos_Codigo = new int[1] ;
         H00CG42_A74Contrato_Codigo = new int[1] ;
         H00CG42_n74Contrato_Codigo = new bool[] {false} ;
         H00CG42_A157ServicoGrupo_Codigo = new int[1] ;
         H00CG42_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00CG42_A155Servico_Codigo = new int[1] ;
         AV138SDT_Servico = new SdtSDT_Codigos(context);
         H00CG43_A155Servico_Codigo = new int[1] ;
         H00CG43_A608Servico_Nome = new String[] {""} ;
         H00CG43_A605Servico_Sigla = new String[] {""} ;
         H00CG44_A127Sistema_Codigo = new int[1] ;
         H00CG44_A130Sistema_Ativo = new bool[] {false} ;
         H00CG44_A699Sistema_Tipo = new String[] {""} ;
         H00CG44_n699Sistema_Tipo = new bool[] {false} ;
         H00CG44_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00CG44_A129Sistema_Sigla = new String[] {""} ;
         H00CG47_A1013Contrato_PrepostoCod = new int[1] ;
         H00CG47_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00CG47_A77Contrato_Numero = new String[] {""} ;
         H00CG47_A74Contrato_Codigo = new int[1] ;
         H00CG47_n74Contrato_Codigo = new bool[] {false} ;
         H00CG47_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CG47_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CG47_A43Contratada_Ativo = new bool[] {false} ;
         H00CG47_n43Contratada_Ativo = new bool[] {false} ;
         H00CG47_A54Usuario_Ativo = new bool[] {false} ;
         H00CG47_n54Usuario_Ativo = new bool[] {false} ;
         H00CG47_A92Contrato_Ativo = new bool[] {false} ;
         H00CG47_n92Contrato_Ativo = new bool[] {false} ;
         H00CG47_A39Contratada_Codigo = new int[1] ;
         H00CG47_n39Contratada_Codigo = new bool[] {false} ;
         H00CG47_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00CG47_n843Contrato_DataFimTA = new bool[] {false} ;
         H00CG47_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00CG47_n83Contrato_DataVigenciaTermino = new bool[] {false} ;
         H00CG48_A160ContratoServicos_Codigo = new int[1] ;
         H00CG48_A74Contrato_Codigo = new int[1] ;
         H00CG48_n74Contrato_Codigo = new bool[] {false} ;
         H00CG48_A155Servico_Codigo = new int[1] ;
         GXt_char4 = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         GXt_char7 = "";
         GXt_char6 = "";
         GXt_char5 = "";
         H00CG50_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00CG50_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00CG50_A601ContagemResultado_Servico = new int[1] ;
         H00CG50_n601ContagemResultado_Servico = new bool[] {false} ;
         H00CG50_A456ContagemResultado_Codigo = new int[1] ;
         H00CG50_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00CG50_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00CG50_A1452ContagemResultado_SS = new int[1] ;
         H00CG50_n1452ContagemResultado_SS = new bool[] {false} ;
         H00CG50_A1637ContagemResultado_ServicoSSSgl = new String[] {""} ;
         H00CG50_n1637ContagemResultado_ServicoSSSgl = new bool[] {false} ;
         H00CG50_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00CG50_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00CG50_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00CG50_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00CG50_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00CG50_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00CG50_A489ContagemResultado_SistemaCod = new int[1] ;
         H00CG50_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00CG50_A804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG50_n804ContagemResultado_SistemaAtivo = new bool[] {false} ;
         H00CG50_A146Modulo_Codigo = new int[1] ;
         H00CG50_n146Modulo_Codigo = new bool[] {false} ;
         H00CG50_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CG50_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CG50_A1044ContagemResultado_FncUsrCod = new int[1] ;
         H00CG50_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         H00CG50_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00CG50_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00CG50_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00CG50_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00CG50_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00CG50_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00CG50_A584ContagemResultado_ContadorFM = new int[1] ;
         H00CG50_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00CG50_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00CG50_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00CG50_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         A1637ContagemResultado_ServicoSSSgl = "";
         H00CG51_A160ContratoServicos_Codigo = new int[1] ;
         H00CG51_A1749Artefatos_Codigo = new int[1] ;
         H00CG51_A1751Artefatos_Descricao = new String[] {""} ;
         AV127sdt_Artefato = new SdtSDT_Artefatos(context);
         H00CG52_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00CG52_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00CG52_A39Contratada_Codigo = new int[1] ;
         H00CG52_n39Contratada_Codigo = new bool[] {false} ;
         bttBtn_cancel_Jsonclick = "";
         H00CG53_A40Contratada_PessoaCod = new int[1] ;
         H00CG53_A39Contratada_Codigo = new int[1] ;
         H00CG53_n39Contratada_Codigo = new bool[] {false} ;
         H00CG53_A41Contratada_PessoaNom = new String[] {""} ;
         H00CG53_n41Contratada_PessoaNom = new bool[] {false} ;
         H00CG54_A160ContratoServicos_Codigo = new int[1] ;
         H00CG54_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00CG54_A157ServicoGrupo_Codigo = new int[1] ;
         H00CG54_A608Servico_Nome = new String[] {""} ;
         H00CG54_A605Servico_Sigla = new String[] {""} ;
         H00CG54_A155Servico_Codigo = new int[1] ;
         H00CG55_A456ContagemResultado_Codigo = new int[1] ;
         H00CG55_A494ContagemResultado_Descricao = new String[] {""} ;
         H00CG55_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00CG55_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00CG55_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00CG55_A489ContagemResultado_SistemaCod = new int[1] ;
         H00CG55_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         A494ContagemResultado_Descricao = "";
         A509ContagemrResultado_SistemaSigla = "";
         AV160SDT_RequisitosItem = new SdtSDT_Requisitos_SDT_RequisitosItem(context);
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         lblLblultimaos_Jsonclick = "";
         bttBtnartefatos_Jsonclick = "";
         bttBtnconfirmar_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         subGridrequisitos_Linesclass = "";
         GridrequisitosColumn = new GXWebColumn();
         lblTextblockrequisitadopor_Jsonclick = "";
         lblTextblockrequisitante_Jsonclick = "";
         lblTextblockusuario_cargouonom_Jsonclick = "";
         lblTextblockcontagemresultado_demandafm_Jsonclick = "";
         lblTextblockusuario_pessoanom_Jsonclick = "";
         lblTextblockcontagemresultado_datadmn_Jsonclick = "";
         lblTextblockcontagemresultado_descricao_Jsonclick = "";
         lblTextblockcontrato_Jsonclick = "";
         lblTextblockservicogrupo_codigo_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblockcontratoservicos_undcntnome_Jsonclick = "";
         lblTextblockcontratoservicos_qntuntcns_Jsonclick = "";
         lblTextblockcomplexidade_Jsonclick = "";
         lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick = "";
         lblTextblockrequisitado_Jsonclick = "";
         lblTextblockusuario_codigo_Jsonclick = "";
         lblLblvincularcom_Jsonclick = "";
         lblTextblockpfbfsdaosvnc_Jsonclick = "";
         lblTextblockpflfsdaosvnc_Jsonclick = "";
         lblTextblockcontratadaorigem_Jsonclick = "";
         lblTextblockcontadorfs_Jsonclick = "";
         lblTextblockcontratoservicos_localexec_Jsonclick = "";
         lblTextblockprazoanalise_Jsonclick = "";
         lblTextblockprazoentrega_Jsonclick = "";
         lblTextblocksistema_codigo_Jsonclick = "";
         lblTextblockmodulo_codigo_Jsonclick = "";
         lblTextblockfuncaousuario_codigo_Jsonclick = "";
         lblTextblocksistema_gestor_Jsonclick = "";
         lblTextblockcontagemresultado_link_Jsonclick = "";
         lblTextblockprioridade_codigo_Jsonclick = "";
         lblRequisitadopor_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_os__default(),
            new Object[][] {
                new Object[] {
               H00CG3_A61ContratanteUsuario_UsuarioPessoaCod, H00CG3_n61ContratanteUsuario_UsuarioPessoaCod, H00CG3_A63ContratanteUsuario_ContratanteCod, H00CG3_A60ContratanteUsuario_UsuarioCod, H00CG3_A62ContratanteUsuario_UsuarioPessoaNom, H00CG3_n62ContratanteUsuario_UsuarioPessoaNom, H00CG3_A54Usuario_Ativo, H00CG3_n54Usuario_Ativo, H00CG3_A1020ContratanteUsuario_AreaTrabalhoCod, H00CG3_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00CG4_A40Contratada_PessoaCod, H00CG4_A39Contratada_Codigo, H00CG4_A41Contratada_PessoaNom, H00CG4_n41Contratada_PessoaNom, H00CG4_A52Contratada_AreaTrabalhoCod, H00CG4_A43Contratada_Ativo
               }
               , new Object[] {
               H00CG5_A70ContratadaUsuario_UsuarioPessoaCod, H00CG5_n70ContratadaUsuario_UsuarioPessoaCod, H00CG5_A69ContratadaUsuario_UsuarioCod, H00CG5_A71ContratadaUsuario_UsuarioPessoaNom, H00CG5_n71ContratadaUsuario_UsuarioPessoaNom, H00CG5_A66ContratadaUsuario_ContratadaCod, H00CG5_A1394ContratadaUsuario_UsuarioAtivo, H00CG5_n1394ContratadaUsuario_UsuarioAtivo
               }
               , new Object[] {
               H00CG6_A146Modulo_Codigo, H00CG6_A145Modulo_Sigla, H00CG6_A127Sistema_Codigo
               }
               , new Object[] {
               H00CG7_A161FuncaoUsuario_Codigo, H00CG7_A162FuncaoUsuario_Nome, H00CG7_A127Sistema_Codigo, H00CG7_A164FuncaoUsuario_Ativo
               }
               , new Object[] {
               H00CG8_A29Contratante_Codigo, H00CG8_n29Contratante_Codigo, H00CG8_A5AreaTrabalho_Codigo, H00CG8_A593Contratante_OSAutomatica, H00CG8_A2085Contratante_RequerOrigem, H00CG8_n2085Contratante_RequerOrigem, H00CG8_A6AreaTrabalho_Descricao, H00CG8_A548Contratante_EmailSdaUser, H00CG8_n548Contratante_EmailSdaUser, H00CG8_A552Contratante_EmailSdaPort,
               H00CG8_n552Contratante_EmailSdaPort, H00CG8_A547Contratante_EmailSdaHost, H00CG8_n547Contratante_EmailSdaHost, H00CG8_A1822Contratante_UsaOSistema, H00CG8_n1822Contratante_UsaOSistema
               }
               , new Object[] {
               H00CG9_A456ContagemResultado_Codigo, H00CG9_A490ContagemResultado_ContratadaCod, H00CG9_n490ContagemResultado_ContratadaCod, H00CG9_A457ContagemResultado_Demanda, H00CG9_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00CG10_A157ServicoGrupo_Codigo, H00CG10_A1428ServicoGrupo_FlagRequerSoft, H00CG10_n1428ServicoGrupo_FlagRequerSoft
               }
               , new Object[] {
               H00CG11_A57Usuario_PessoaCod, H00CG11_A1073Usuario_CargoCod, H00CG11_n1073Usuario_CargoCod, H00CG11_A1Usuario_Codigo, H00CG11_A58Usuario_PessoaNom, H00CG11_n58Usuario_PessoaNom, H00CG11_A1075Usuario_CargoUOCod, H00CG11_n1075Usuario_CargoUOCod, H00CG11_A1076Usuario_CargoUONom, H00CG11_n1076Usuario_CargoUONom
               }
               , new Object[] {
               H00CG13_A1553ContagemResultado_CntSrvCod, H00CG13_n1553ContagemResultado_CntSrvCod, H00CG13_A601ContagemResultado_Servico, H00CG13_n601ContagemResultado_Servico, H00CG13_A484ContagemResultado_StatusDmn, H00CG13_n484ContagemResultado_StatusDmn, H00CG13_A493ContagemResultado_DemandaFM, H00CG13_n493ContagemResultado_DemandaFM, H00CG13_A490ContagemResultado_ContratadaCod, H00CG13_n490ContagemResultado_ContratadaCod,
               H00CG13_A456ContagemResultado_Codigo, H00CG13_A801ContagemResultado_ServicoSigla, H00CG13_n801ContagemResultado_ServicoSigla, H00CG13_A489ContagemResultado_SistemaCod, H00CG13_n489ContagemResultado_SistemaCod, H00CG13_A146Modulo_Codigo, H00CG13_n146Modulo_Codigo, H00CG13_A1044ContagemResultado_FncUsrCod, H00CG13_n1044ContagemResultado_FncUsrCod, H00CG13_A1326ContagemResultado_ContratadaTipoFab,
               H00CG13_n1326ContagemResultado_ContratadaTipoFab, H00CG13_A798ContagemResultado_PFBFSImp, H00CG13_n798ContagemResultado_PFBFSImp, H00CG13_A799ContagemResultado_PFLFSImp, H00CG13_n799ContagemResultado_PFLFSImp, H00CG13_A804ContagemResultado_SistemaAtivo, H00CG13_n804ContagemResultado_SistemaAtivo, H00CG13_A803ContagemResultado_ContratadaSigla, H00CG13_n803ContagemResultado_ContratadaSigla, H00CG13_A682ContagemResultado_PFBFMUltima,
               H00CG13_A683ContagemResultado_PFLFMUltima, H00CG13_A684ContagemResultado_PFBFSUltima, H00CG13_A685ContagemResultado_PFLFSUltima
               }
               , new Object[] {
               H00CG15_A1553ContagemResultado_CntSrvCod, H00CG15_n1553ContagemResultado_CntSrvCod, H00CG15_A601ContagemResultado_Servico, H00CG15_n601ContagemResultado_Servico, H00CG15_A484ContagemResultado_StatusDmn, H00CG15_n484ContagemResultado_StatusDmn, H00CG15_A457ContagemResultado_Demanda, H00CG15_n457ContagemResultado_Demanda, H00CG15_A490ContagemResultado_ContratadaCod, H00CG15_n490ContagemResultado_ContratadaCod,
               H00CG15_A456ContagemResultado_Codigo, H00CG15_A801ContagemResultado_ServicoSigla, H00CG15_n801ContagemResultado_ServicoSigla, H00CG15_A489ContagemResultado_SistemaCod, H00CG15_n489ContagemResultado_SistemaCod, H00CG15_A146Modulo_Codigo, H00CG15_n146Modulo_Codigo, H00CG15_A1044ContagemResultado_FncUsrCod, H00CG15_n1044ContagemResultado_FncUsrCod, H00CG15_A1326ContagemResultado_ContratadaTipoFab,
               H00CG15_n1326ContagemResultado_ContratadaTipoFab, H00CG15_A798ContagemResultado_PFBFSImp, H00CG15_n798ContagemResultado_PFBFSImp, H00CG15_A799ContagemResultado_PFLFSImp, H00CG15_n799ContagemResultado_PFLFSImp, H00CG15_A804ContagemResultado_SistemaAtivo, H00CG15_n804ContagemResultado_SistemaAtivo, H00CG15_A803ContagemResultado_ContratadaSigla, H00CG15_n803ContagemResultado_ContratadaSigla, H00CG15_A493ContagemResultado_DemandaFM,
               H00CG15_n493ContagemResultado_DemandaFM, H00CG15_A682ContagemResultado_PFBFMUltima, H00CG15_A683ContagemResultado_PFLFMUltima, H00CG15_A684ContagemResultado_PFBFSUltima, H00CG15_A685ContagemResultado_PFLFSUltima
               }
               , new Object[] {
               H00CG17_A1553ContagemResultado_CntSrvCod, H00CG17_n1553ContagemResultado_CntSrvCod, H00CG17_A601ContagemResultado_Servico, H00CG17_n601ContagemResultado_Servico, H00CG17_A456ContagemResultado_Codigo, H00CG17_A493ContagemResultado_DemandaFM, H00CG17_n493ContagemResultado_DemandaFM, H00CG17_A457ContagemResultado_Demanda, H00CG17_n457ContagemResultado_Demanda, H00CG17_A801ContagemResultado_ServicoSigla,
               H00CG17_n801ContagemResultado_ServicoSigla, H00CG17_A489ContagemResultado_SistemaCod, H00CG17_n489ContagemResultado_SistemaCod, H00CG17_A146Modulo_Codigo, H00CG17_n146Modulo_Codigo, H00CG17_A1044ContagemResultado_FncUsrCod, H00CG17_n1044ContagemResultado_FncUsrCod, H00CG17_A490ContagemResultado_ContratadaCod, H00CG17_n490ContagemResultado_ContratadaCod, H00CG17_A804ContagemResultado_SistemaAtivo,
               H00CG17_n804ContagemResultado_SistemaAtivo, H00CG17_A1326ContagemResultado_ContratadaTipoFab, H00CG17_n1326ContagemResultado_ContratadaTipoFab, H00CG17_A798ContagemResultado_PFBFSImp, H00CG17_n798ContagemResultado_PFBFSImp, H00CG17_A799ContagemResultado_PFLFSImp, H00CG17_n799ContagemResultado_PFLFSImp, H00CG17_A682ContagemResultado_PFBFMUltima, H00CG17_A683ContagemResultado_PFLFMUltima, H00CG17_A684ContagemResultado_PFBFSUltima,
               H00CG17_A685ContagemResultado_PFLFSUltima
               }
               , new Object[] {
               H00CG18_A74Contrato_Codigo, H00CG18_A1013Contrato_PrepostoCod, H00CG18_n1013Contrato_PrepostoCod, H00CG18_A39Contratada_Codigo
               }
               , new Object[] {
               H00CG21_A39Contratada_Codigo, H00CG21_A1013Contrato_PrepostoCod, H00CG21_n1013Contrato_PrepostoCod, H00CG21_A1212ContratoServicos_UnidadeContratada, H00CG21_A903ContratoServicosPrazo_CntSrvCod, H00CG21_A160ContratoServicos_Codigo, H00CG21_A155Servico_Codigo, H00CG21_A52Contratada_AreaTrabalhoCod, H00CG21_A43Contratada_Ativo, H00CG21_A54Usuario_Ativo,
               H00CG21_n54Usuario_Ativo, H00CG21_A74Contrato_Codigo, H00CG21_n74Contrato_Codigo, H00CG21_A1072Servico_Atende, H00CG21_n1072Servico_Atende, H00CG21_A157ServicoGrupo_Codigo, H00CG21_A1429Servico_ObrigaValores, H00CG21_n1429Servico_ObrigaValores, H00CG21_A2092Servico_IsOrigemReferencia, H00CG21_A2094ContratoServicos_SolicitaGestorSistema,
               H00CG21_A2132ContratoServicos_UndCntNome, H00CG21_n2132ContratoServicos_UndCntNome, H00CG21_A1340ContratoServicos_QntUntCns, H00CG21_n1340ContratoServicos_QntUntCns, H00CG21_A1152ContratoServicos_PrazoAnalise, H00CG21_n1152ContratoServicos_PrazoAnalise, H00CG21_A1454ContratoServicos_PrazoTpDias, H00CG21_n1454ContratoServicos_PrazoTpDias, H00CG21_A557Servico_VlrUnidadeContratada, H00CG21_A116Contrato_ValorUnidadeContratacao,
               H00CG21_A1649ContratoServicos_PrazoInicio, H00CG21_n1649ContratoServicos_PrazoInicio, H00CG21_A913ContratoServicos_PrazoTipo, H00CG21_n913ContratoServicos_PrazoTipo, H00CG21_A843Contrato_DataFimTA, H00CG21_n843Contrato_DataFimTA, H00CG21_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00CG22_A160ContratoServicos_Codigo, H00CG22_A1067ContratoServicosSistemas_ServicoCod, H00CG22_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               H00CG23_A903ContratoServicosPrazo_CntSrvCod, H00CG23_A1823ContratoServicosPrazo_Momento, H00CG23_n1823ContratoServicosPrazo_Momento, H00CG23_A1265ContratoServicosPrazo_DiasBaixa, H00CG23_n1265ContratoServicosPrazo_DiasBaixa, H00CG23_A1264ContratoServicosPrazo_DiasMedia, H00CG23_n1264ContratoServicosPrazo_DiasMedia, H00CG23_A1263ContratoServicosPrazo_DiasAlta, H00CG23_n1263ContratoServicosPrazo_DiasAlta
               }
               , new Object[] {
               H00CG24_A1335ContratoServicosPrioridade_CntSrvCod, H00CG24_A1337ContratoServicosPrioridade_Nome, H00CG24_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               H00CG25_A155Servico_Codigo, H00CG25_A160ContratoServicos_Codigo, H00CG25_A639ContratoServicos_LocalExec, H00CG25_A889Servico_Terceriza, H00CG25_n889Servico_Terceriza, H00CG25_A1454ContratoServicos_PrazoTpDias, H00CG25_n1454ContratoServicos_PrazoTpDias, H00CG25_A1152ContratoServicos_PrazoAnalise, H00CG25_n1152ContratoServicos_PrazoAnalise, H00CG25_A1649ContratoServicos_PrazoInicio,
               H00CG25_n1649ContratoServicos_PrazoInicio
               }
               , new Object[] {
               H00CG26_A39Contratada_Codigo, H00CG26_A74Contrato_Codigo, H00CG26_A52Contratada_AreaTrabalhoCod, H00CG26_A43Contratada_Ativo, H00CG26_A54Usuario_Ativo, H00CG26_n54Usuario_Ativo, H00CG26_A1013Contrato_PrepostoCod, H00CG26_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               H00CG27_A1078ContratoGestor_ContratoCod, H00CG27_A1079ContratoGestor_UsuarioCod, H00CG27_A1446ContratoGestor_ContratadaAreaCod, H00CG27_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00CG28_A1078ContratoGestor_ContratoCod, H00CG28_A1079ContratoGestor_UsuarioCod, H00CG28_A1446ContratoGestor_ContratadaAreaCod, H00CG28_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               H00CG31_A160ContratoServicos_Codigo, H00CG31_A74Contrato_Codigo, H00CG31_n74Contrato_Codigo, H00CG31_A40Contratada_PessoaCod, H00CG31_A75Contrato_AreaTrabalhoCod, H00CG31_A155Servico_Codigo, H00CG31_A43Contratada_Ativo, H00CG31_A92Contrato_Ativo, H00CG31_A638ContratoServicos_Ativo, H00CG31_A83Contrato_DataVigenciaTermino,
               H00CG31_A39Contratada_Codigo, H00CG31_A41Contratada_PessoaNom, H00CG31_n41Contratada_PessoaNom, H00CG31_A843Contrato_DataFimTA, H00CG31_n843Contrato_DataFimTA
               }
               , new Object[] {
               H00CG34_A160ContratoServicos_Codigo, H00CG34_A74Contrato_Codigo, H00CG34_n74Contrato_Codigo, H00CG34_A40Contratada_PessoaCod, H00CG34_A43Contratada_Ativo, H00CG34_A92Contrato_Ativo, H00CG34_A638ContratoServicos_Ativo, H00CG34_A83Contrato_DataVigenciaTermino, H00CG34_A155Servico_Codigo, H00CG34_A39Contratada_Codigo,
               H00CG34_A41Contratada_PessoaNom, H00CG34_n41Contratada_PessoaNom, H00CG34_A843Contrato_DataFimTA, H00CG34_n843Contrato_DataFimTA
               }
               , new Object[] {
               H00CG35_A70ContratadaUsuario_UsuarioPessoaCod, H00CG35_n70ContratadaUsuario_UsuarioPessoaCod, H00CG35_A66ContratadaUsuario_ContratadaCod, H00CG35_A69ContratadaUsuario_UsuarioCod, H00CG35_A71ContratadaUsuario_UsuarioPessoaNom, H00CG35_n71ContratadaUsuario_UsuarioPessoaNom, H00CG35_A1394ContratadaUsuario_UsuarioAtivo, H00CG35_n1394ContratadaUsuario_UsuarioAtivo, H00CG35_A43Contratada_Ativo, H00CG35_n43Contratada_Ativo,
               H00CG35_A1908Usuario_DeFerias, H00CG35_n1908Usuario_DeFerias
               }
               , new Object[] {
               H00CG36_A828UsuarioServicos_UsuarioCod, H00CG36_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               H00CG37_A61ContratanteUsuario_UsuarioPessoaCod, H00CG37_n61ContratanteUsuario_UsuarioPessoaCod, H00CG37_A63ContratanteUsuario_ContratanteCod, H00CG37_A1908Usuario_DeFerias, H00CG37_n1908Usuario_DeFerias, H00CG37_A54Usuario_Ativo, H00CG37_n54Usuario_Ativo, H00CG37_A60ContratanteUsuario_UsuarioCod, H00CG37_A62ContratanteUsuario_UsuarioPessoaNom, H00CG37_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00CG38_A155Servico_Codigo, H00CG38_A74Contrato_Codigo, H00CG38_A75Contrato_AreaTrabalhoCod, H00CG38_A638ContratoServicos_Ativo, H00CG38_A92Contrato_Ativo, H00CG38_A157ServicoGrupo_Codigo, H00CG38_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H00CG39_A1136ContratoGestor_ContratadaCod, H00CG39_n1136ContratoGestor_ContratadaCod, H00CG39_A1013Contrato_PrepostoCod, H00CG39_n1013Contrato_PrepostoCod, H00CG39_A1446ContratoGestor_ContratadaAreaCod, H00CG39_n1446ContratoGestor_ContratadaAreaCod, H00CG39_A1078ContratoGestor_ContratoCod, H00CG39_A54Usuario_Ativo, H00CG39_n54Usuario_Ativo, H00CG39_A52Contratada_AreaTrabalhoCod,
               H00CG39_n52Contratada_AreaTrabalhoCod, H00CG39_A43Contratada_Ativo, H00CG39_n43Contratada_Ativo, H00CG39_A92Contrato_Ativo, H00CG39_n92Contrato_Ativo, H00CG39_A83Contrato_DataVigenciaTermino, H00CG39_n83Contrato_DataVigenciaTermino, H00CG39_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00CG40_A160ContratoServicos_Codigo, H00CG40_A74Contrato_Codigo, H00CG40_A157ServicoGrupo_Codigo, H00CG40_A638ContratoServicos_Ativo, H00CG40_A155Servico_Codigo
               }
               , new Object[] {
               H00CG41_A1013Contrato_PrepostoCod, H00CG41_n1013Contrato_PrepostoCod, H00CG41_A39Contratada_Codigo, H00CG41_n39Contratada_Codigo, H00CG41_A75Contrato_AreaTrabalhoCod, H00CG41_n75Contrato_AreaTrabalhoCod, H00CG41_A1824ContratoAuxiliar_ContratoCod, H00CG41_A52Contratada_AreaTrabalhoCod, H00CG41_A54Usuario_Ativo, H00CG41_n54Usuario_Ativo,
               H00CG41_A43Contratada_Ativo, H00CG41_A92Contrato_Ativo, H00CG41_n92Contrato_Ativo, H00CG41_A83Contrato_DataVigenciaTermino, H00CG41_n83Contrato_DataVigenciaTermino, H00CG41_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               H00CG42_A160ContratoServicos_Codigo, H00CG42_A74Contrato_Codigo, H00CG42_A157ServicoGrupo_Codigo, H00CG42_A638ContratoServicos_Ativo, H00CG42_A155Servico_Codigo
               }
               , new Object[] {
               H00CG43_A155Servico_Codigo, H00CG43_A608Servico_Nome, H00CG43_A605Servico_Sigla
               }
               , new Object[] {
               H00CG44_A127Sistema_Codigo, H00CG44_A130Sistema_Ativo, H00CG44_A699Sistema_Tipo, H00CG44_n699Sistema_Tipo, H00CG44_A135Sistema_AreaTrabalhoCod, H00CG44_A129Sistema_Sigla
               }
               , new Object[] {
               H00CG47_A1013Contrato_PrepostoCod, H00CG47_n1013Contrato_PrepostoCod, H00CG47_A77Contrato_Numero, H00CG47_A74Contrato_Codigo, H00CG47_A52Contratada_AreaTrabalhoCod, H00CG47_A43Contratada_Ativo, H00CG47_A54Usuario_Ativo, H00CG47_n54Usuario_Ativo, H00CG47_A92Contrato_Ativo, H00CG47_A39Contratada_Codigo,
               H00CG47_A843Contrato_DataFimTA, H00CG47_n843Contrato_DataFimTA, H00CG47_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00CG48_A160ContratoServicos_Codigo, H00CG48_A74Contrato_Codigo, H00CG48_A155Servico_Codigo
               }
               , new Object[] {
               H00CG50_A1553ContagemResultado_CntSrvCod, H00CG50_n1553ContagemResultado_CntSrvCod, H00CG50_A601ContagemResultado_Servico, H00CG50_n601ContagemResultado_Servico, H00CG50_A456ContagemResultado_Codigo, H00CG50_A1636ContagemResultado_ServicoSS, H00CG50_n1636ContagemResultado_ServicoSS, H00CG50_A1452ContagemResultado_SS, H00CG50_n1452ContagemResultado_SS, H00CG50_A1637ContagemResultado_ServicoSSSgl,
               H00CG50_n1637ContagemResultado_ServicoSSSgl, H00CG50_A493ContagemResultado_DemandaFM, H00CG50_n493ContagemResultado_DemandaFM, H00CG50_A803ContagemResultado_ContratadaSigla, H00CG50_n803ContagemResultado_ContratadaSigla, H00CG50_A801ContagemResultado_ServicoSigla, H00CG50_n801ContagemResultado_ServicoSigla, H00CG50_A489ContagemResultado_SistemaCod, H00CG50_n489ContagemResultado_SistemaCod, H00CG50_A804ContagemResultado_SistemaAtivo,
               H00CG50_n804ContagemResultado_SistemaAtivo, H00CG50_A146Modulo_Codigo, H00CG50_n146Modulo_Codigo, H00CG50_A490ContagemResultado_ContratadaCod, H00CG50_n490ContagemResultado_ContratadaCod, H00CG50_A1044ContagemResultado_FncUsrCod, H00CG50_n1044ContagemResultado_FncUsrCod, H00CG50_A1326ContagemResultado_ContratadaTipoFab, H00CG50_n1326ContagemResultado_ContratadaTipoFab, H00CG50_A798ContagemResultado_PFBFSImp,
               H00CG50_n798ContagemResultado_PFBFSImp, H00CG50_A799ContagemResultado_PFLFSImp, H00CG50_n799ContagemResultado_PFLFSImp, H00CG50_A584ContagemResultado_ContadorFM, H00CG50_A682ContagemResultado_PFBFMUltima, H00CG50_A683ContagemResultado_PFLFMUltima, H00CG50_A684ContagemResultado_PFBFSUltima, H00CG50_A685ContagemResultado_PFLFSUltima
               }
               , new Object[] {
               H00CG51_A160ContratoServicos_Codigo, H00CG51_A1749Artefatos_Codigo, H00CG51_A1751Artefatos_Descricao
               }
               , new Object[] {
               H00CG52_A52Contratada_AreaTrabalhoCod, H00CG52_A39Contratada_Codigo
               }
               , new Object[] {
               H00CG53_A40Contratada_PessoaCod, H00CG53_A39Contratada_Codigo, H00CG53_A41Contratada_PessoaNom, H00CG53_n41Contratada_PessoaNom
               }
               , new Object[] {
               H00CG54_A160ContratoServicos_Codigo, H00CG54_A158ServicoGrupo_Descricao, H00CG54_A157ServicoGrupo_Codigo, H00CG54_A608Servico_Nome, H00CG54_A605Servico_Sigla, H00CG54_A155Servico_Codigo
               }
               , new Object[] {
               H00CG55_A456ContagemResultado_Codigo, H00CG55_A494ContagemResultado_Descricao, H00CG55_n494ContagemResultado_Descricao, H00CG55_A509ContagemrResultado_SistemaSigla, H00CG55_n509ContagemrResultado_SistemaSigla, H00CG55_A489ContagemResultado_SistemaCod, H00CG55_n489ContagemResultado_SistemaCod
               }
            }
         );
         WebComp_Wcarquivosevd = new GeneXus.Http.GXNullWebComponent();
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavUsuario_cargouonom_Enabled = 0;
         edtavUsuario_pessoanom_Enabled = 0;
         edtavContagemresultado_datadmn_Enabled = 0;
         edtavContratoservicos_undcntnome_Enabled = 0;
         edtavContratoservicos_qntuntcns_Enabled = 0;
         edtavPrazoanalise_Enabled = 0;
         edtavSistema_gestor_Enabled = 0;
         edtavAgrupador_Enabled = 0;
         edtavSdt_requisitos__requisito_identificador_Enabled = 0;
         edtavSdt_requisitos__requisito_titulo_Enabled = 0;
         cmbavSdt_requisitos__requisito_prioridade.Enabled = 0;
         cmbavSdt_requisitos__requisito_status.Enabled = 0;
      }

      private short nRcdExists_10 ;
      private short nIsMod_10 ;
      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_219 ;
      private short nGXsfl_219_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRIDREQUISITOS_nEOF ;
      private short AV135PrazoMomento ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short AV67Dias ;
      private short AV122PrazoInicio ;
      private short AV98DiasAnalise ;
      private short A1823ContratoServicosPrazo_Momento ;
      private short A1265ContratoServicosPrazo_DiasBaixa ;
      private short A1264ContratoServicosPrazo_DiasMedia ;
      private short A1263ContratoServicosPrazo_DiasAlta ;
      private short wbEnd ;
      private short wbStart ;
      private short AV176GXV1 ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV97Complexidade ;
      private short nGXsfl_219_Refreshing=0 ;
      private short subGridrequisitos_Backcolorstyle ;
      private short nGXsfl_219_fel_idx=1 ;
      private short A552Contratante_EmailSdaPort ;
      private short nGXsfl_219_bak_idx=1 ;
      private short AV191GXLvl939 ;
      private short GXt_int8 ;
      private short AV77Hours ;
      private short AV78Minutes ;
      private short AV6WWPContext_gxTpr_Userid ;
      private short subGridrequisitos_Titlebackstyle ;
      private short subGridrequisitos_Allowselection ;
      private short subGridrequisitos_Allowhovering ;
      private short subGridrequisitos_Allowcollapsing ;
      private short subGridrequisitos_Collapsed ;
      private short nGXWrapped ;
      private short subGridrequisitos_Backstyle ;
      private short wbTemp ;
      private int AV105ContratadaOrigem ;
      private int AV38Sistema_Codigo ;
      private int subGridrequisitos_Rows ;
      private int AV13Servico_Codigo ;
      private int A1Usuario_Codigo ;
      private int A1075Usuario_CargoUOCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV100AreaTrabalho_Codigo ;
      private int A39Contratada_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A157ServicoGrupo_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int AV102Requisitante ;
      private int A74Contrato_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int AV16ServicoGrupo_Codigo ;
      private int A155Servico_Codigo ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private int A1824ContratoAuxiliar_ContratoCod ;
      private int AV170ServicoCodigo ;
      private int AV49ContratoServicos_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV23ContagemResultado_OSVinculada ;
      private int AV40FuncaoDados_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A160ContratoServicos_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A146Modulo_Codigo ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int A1067ContratoServicosSistemas_ServicoCod ;
      private int A1063ContratoServicosSistemas_SistemaCod ;
      private int A1336ContratoServicosPrioridade_Codigo ;
      private int AV155Contrato_Padrao ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1749Artefatos_Codigo ;
      private int edtavAreatrabalho_codigo_Visible ;
      private int AV12RequisitadoCod ;
      private int edtavRequisitadocod_Visible ;
      private int AV117Contrato ;
      private int AV96Prioridade_Codigo ;
      private int AV10Usuario_Codigo ;
      private int AV123DmnVinculadas ;
      private int gxdynajaxindex ;
      private int AV107ContadorFS ;
      private int AV39Modulo_Codigo ;
      private int AV41FuncaoUsuario_Codigo ;
      private int subGridrequisitos_Islastpage ;
      private int edtavUsuario_cargouonom_Enabled ;
      private int edtavUsuario_pessoanom_Enabled ;
      private int edtavContagemresultado_datadmn_Enabled ;
      private int edtavContratoservicos_undcntnome_Enabled ;
      private int edtavContratoservicos_qntuntcns_Enabled ;
      private int edtavPrazoanalise_Enabled ;
      private int edtavSistema_gestor_Enabled ;
      private int edtavAgrupador_Enabled ;
      private int edtavSdt_requisitos__requisito_identificador_Enabled ;
      private int edtavSdt_requisitos__requisito_titulo_Enabled ;
      private int GRIDREQUISITOS_nGridOutOfScope ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int lblTextblockprioridade_codigo_Visible ;
      private int lblTextblockcomplexidade_Visible ;
      private int lblTextblockpfbfsdaosvnc_Visible ;
      private int lblTextblockpflfsdaosvnc_Visible ;
      private int edtavPfbfsdaosvnc_Visible ;
      private int edtavPflfsdaosvnc_Visible ;
      private int edtavContagemresultado_demandafm_Enabled ;
      private int lblTextblockrequisitante_Visible ;
      private int lblTextblockcontrato_Visible ;
      private int bttBtnconfirmar_Visible ;
      private int lblTbjava_Visible ;
      private int AV126lastContagemResultado_Codigo ;
      private int bttBtnartefatos_Visible ;
      private int edtavAgrupador_Forecolor ;
      private int AV104UserId ;
      private int edtavContagemresultado_quantidadesolicitada_Visible ;
      private int edtavSistema_gestor_Visible ;
      private int lblTextblockrequisitadopor_Visible ;
      private int tblTablemergedrequisitadopor_Visible ;
      private int AV55Usuario_CargoUOCod ;
      private int A57Usuario_PessoaCod ;
      private int A1073Usuario_CargoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int edtavPfbfsdaosvnc_Enabled ;
      private int edtavPflfsdaosvnc_Enabled ;
      private int edtavContagemresultado_dmnvinculada_Visible ;
      private int edtavContagemresultado_dmnvinculadaref_Visible ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int lblTextblocksistema_codigo_Visible ;
      private int lblTextblockmodulo_codigo_Visible ;
      private int lblTextblockfuncaousuario_codigo_Visible ;
      private int A1335ContratoServicosPrioridade_CntSrvCod ;
      private int A40Contratada_PessoaCod ;
      private int AV20Contratada_Codigo ;
      private int AV21Contratante_Codigo ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int GXt_int10 ;
      private int lblTextblockusuario_codigo_Visible ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int AV210GXV2 ;
      private int AV51Sistemas_Count ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1452ContagemResultado_SS ;
      private int A584ContagemResultado_ContadorFM ;
      private int subGridrequisitos_Titlebackcolor ;
      private int subGridrequisitos_Allbackcolor ;
      private int subGridrequisitos_Selectioncolor ;
      private int subGridrequisitos_Hoveringcolor ;
      private int lblTextblockusuario_cargouonom_Visible ;
      private int edtavUsuario_cargouonom_Visible ;
      private int lblTextblockusuario_pessoanom_Visible ;
      private int edtavUsuario_pessoanom_Visible ;
      private int idxLst ;
      private int subGridrequisitos_Backcolor ;
      private int edtavBtnexcluirreqneg_Enabled ;
      private int edtavBtnexcluirreqneg_Visible ;
      private int edtavAgrupador_Visible ;
      private long GRIDREQUISITOS_nFirstRecordOnPage ;
      private long GRIDREQUISITOS_nCurrentRecord ;
      private long GRIDREQUISITOS_nRecordCount ;
      private decimal AV172ContratoServicos_QntUntCns ;
      private decimal AV26ContagemResultado_ValorPF ;
      private decimal AV113PercPrazo ;
      private decimal AV114PercValorB ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal AV173ContagemResultado_QuantidadeSolicitada ;
      private decimal AV66PFBFSdaOSVnc ;
      private decimal AV73PFLFSdaOSVnc ;
      private decimal AV121SaldoContrato_Saldo ;
      private decimal GXt_decimal11 ;
      private String Dvelop_bootstrap_confirmpanel1_Title ;
      private String Confirmpanel2_Title ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_219_idx="0001" ;
      private String A58Usuario_PessoaNom ;
      private String A1076Usuario_CargoUONom ;
      private String A129Sistema_Sigla ;
      private String A699Sistema_Tipo ;
      private String A605Servico_Sigla ;
      private String A608Servico_Nome ;
      private String edtavAgrupador_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV84PrazoTipo ;
      private String AV125Caller ;
      private String AV44Resultado ;
      private String Gx_msg ;
      private String A639ContratoServicos_LocalExec ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV115TipoDias ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A1072Servico_Atende ;
      private String A1429Servico_ObrigaValores ;
      private String A2132ContratoServicos_UndCntNome ;
      private String A913ContratoServicos_PrazoTipo ;
      private String A1337ContratoServicosPrioridade_Nome ;
      private String A77Contrato_Numero ;
      private String AV162Nome ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A41Contratada_PessoaNom ;
      private String AV56Usuario_CargoUONom ;
      private String AV7Usuario_PessoaNom ;
      private String Gxuitabspanel_tab_Width ;
      private String Gxuitabspanel_tab_Cls ;
      private String Gxuitabspanel_tab_Designtimetabs ;
      private String Dvelop_bootstrap_confirmpanel1_Confirmationtext ;
      private String Dvelop_bootstrap_confirmpanel1_Yesbuttoncaption ;
      private String Dvelop_bootstrap_confirmpanel1_Confirmtype ;
      private String Confirmpanel2_Confirmationtext ;
      private String Confirmpanel2_Yesbuttoncaption ;
      private String Confirmpanel2_Confirmtype ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String cmbavServico_obrigavalores_Internalname ;
      private String AV110Servico_ObrigaValores ;
      private String cmbavServico_obrigavalores_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String chkavContratovencido_Internalname ;
      private String edtavRequisitadocod_Internalname ;
      private String edtavRequisitadocod_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBtnexcluirreqneg_Internalname ;
      private String OldWcarquivosevd ;
      private String WebComp_Wcarquivosevd_Component ;
      private String chkavRequisitadopor_Internalname ;
      private String AV9Requisitado ;
      private String AV18ContratoServicos_LocalExec ;
      private String GXCCtl ;
      private String AV109Servico_Atende ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavUsuario_cargouonom_Internalname ;
      private String edtavUsuario_pessoanom_Internalname ;
      private String edtavContagemresultado_datadmn_Internalname ;
      private String edtavContratoservicos_undcntnome_Internalname ;
      private String edtavContratoservicos_qntuntcns_Internalname ;
      private String edtavPrazoanalise_Internalname ;
      private String edtavSistema_gestor_Internalname ;
      private String edtavSdt_requisitos__requisito_identificador_Internalname ;
      private String edtavSdt_requisitos__requisito_titulo_Internalname ;
      private String cmbavSdt_requisitos__requisito_prioridade_Internalname ;
      private String cmbavSdt_requisitos__requisito_status_Internalname ;
      private String dynavRequisitante_Internalname ;
      private String edtavContagemresultado_demandafm_Internalname ;
      private String AV88ContagemResultado_Descricao ;
      private String edtavContagemresultado_descricao_Internalname ;
      private String cmbavContrato_Internalname ;
      private String cmbavServicogrupo_codigo_Internalname ;
      private String cmbavServico_codigo_Internalname ;
      private String AV171ContratoServicos_UndCntNome ;
      private String cmbavComplexidade_Internalname ;
      private String cmbavPrioridade_codigo_Internalname ;
      private String edtavContagemresultado_quantidadesolicitada_Internalname ;
      private String cmbavRequisitado_Internalname ;
      private String cmbavUsuario_codigo_Internalname ;
      private String cmbavDmnvinculadas_Internalname ;
      private String edtavContagemresultado_dmnvinculada_Internalname ;
      private String edtavContagemresultado_dmnvinculadaref_Internalname ;
      private String edtavPfbfsdaosvnc_Internalname ;
      private String edtavPflfsdaosvnc_Internalname ;
      private String dynavContratadaorigem_Internalname ;
      private String dynavContadorfs_Internalname ;
      private String cmbavContratoservicos_localexec_Internalname ;
      private String edtavPrazoentrega_Internalname ;
      private String cmbavSistema_codigo_Internalname ;
      private String dynavModulo_codigo_Internalname ;
      private String dynavFuncaousuario_codigo_Internalname ;
      private String edtavContagemresultado_link_Internalname ;
      private String cmbavServico_atende_Internalname ;
      private String sGXsfl_219_fel_idx="0001" ;
      private String lblRequisitadopor_righttext_Caption ;
      private String lblRequisitadopor_righttext_Internalname ;
      private String lblTextblockprioridade_codigo_Internalname ;
      private String lblTextblockcomplexidade_Internalname ;
      private String lblTextblockpfbfsdaosvnc_Internalname ;
      private String lblTextblockpflfsdaosvnc_Internalname ;
      private String AV92Url ;
      private String lblTextblockrequisitante_Internalname ;
      private String lblTextblockcontrato_Internalname ;
      private String bttBtnconfirmar_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblLblultimaos_Caption ;
      private String lblLblultimaos_Internalname ;
      private String lblLblultimaos_Link ;
      private String bttBtnartefatos_Internalname ;
      private String edtavBtnexcluirreqneg_Tooltiptext ;
      private String cellContagemresultado_quantidadesolicitada_cell_Class ;
      private String cellContagemresultado_quantidadesolicitada_cell_Internalname ;
      private String cellTextblockcontagemresultado_quantidadesolicitada_cell_Class ;
      private String cellTextblockcontagemresultado_quantidadesolicitada_cell_Internalname ;
      private String cellContratadaorigem_cell_Class ;
      private String cellContratadaorigem_cell_Internalname ;
      private String cellTextblockcontratadaorigem_cell_Class ;
      private String cellTextblockcontratadaorigem_cell_Internalname ;
      private String cellContadorfs_cell_Class ;
      private String cellContadorfs_cell_Internalname ;
      private String cellTextblockcontadorfs_cell_Class ;
      private String cellTextblockcontadorfs_cell_Internalname ;
      private String cellSistema_gestor_cell_Class ;
      private String cellSistema_gestor_cell_Internalname ;
      private String cellTextblocksistema_gestor_cell_Class ;
      private String cellTextblocksistema_gestor_cell_Internalname ;
      private String bttBtnartefatos_Caption ;
      private String bttBtnconfirmar_Tooltiptext ;
      private String lblTextblockrequisitadopor_Internalname ;
      private String tblTablemergedrequisitadopor_Internalname ;
      private String AV60ServicoVnc_Sigla ;
      private String AV116Servico_Nome ;
      private String Dvelop_bootstrap_confirmpanel1_Internalname ;
      private String Confirmpanel2_Internalname ;
      private String lblTextblocksistema_codigo_Internalname ;
      private String lblTextblockmodulo_codigo_Internalname ;
      private String lblTextblockfuncaousuario_codigo_Internalname ;
      private String GXt_char3 ;
      private String GXt_char2 ;
      private String AV45EmailText ;
      private String AV43Subject ;
      private String lblTextblockusuario_codigo_Internalname ;
      private String GXt_char4 ;
      private String GXt_char7 ;
      private String GXt_char6 ;
      private String GXt_char5 ;
      private String A1637ContagemResultado_ServicoSSSgl ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String lblLblultimaos_Jsonclick ;
      private String tblTableaction_Internalname ;
      private String bttBtnartefatos_Jsonclick ;
      private String bttBtnconfirmar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblUsrtable_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String cmbavServico_atende_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String subGridrequisitos_Internalname ;
      private String subGridrequisitos_Class ;
      private String subGridrequisitos_Linesclass ;
      private String tblUnnamedtable2_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockrequisitadopor_Jsonclick ;
      private String lblTextblockrequisitante_Jsonclick ;
      private String dynavRequisitante_Jsonclick ;
      private String lblTextblockusuario_cargouonom_Internalname ;
      private String lblTextblockusuario_cargouonom_Jsonclick ;
      private String edtavUsuario_cargouonom_Jsonclick ;
      private String lblTextblockcontagemresultado_demandafm_Internalname ;
      private String lblTextblockcontagemresultado_demandafm_Jsonclick ;
      private String edtavContagemresultado_demandafm_Jsonclick ;
      private String lblTextblockusuario_pessoanom_Internalname ;
      private String lblTextblockusuario_pessoanom_Jsonclick ;
      private String edtavUsuario_pessoanom_Jsonclick ;
      private String lblTextblockcontagemresultado_datadmn_Internalname ;
      private String lblTextblockcontagemresultado_datadmn_Jsonclick ;
      private String edtavContagemresultado_datadmn_Jsonclick ;
      private String lblTextblockcontagemresultado_descricao_Internalname ;
      private String lblTextblockcontagemresultado_descricao_Jsonclick ;
      private String edtavContagemresultado_descricao_Jsonclick ;
      private String lblTextblockcontrato_Jsonclick ;
      private String cmbavContrato_Jsonclick ;
      private String lblTextblockservicogrupo_codigo_Internalname ;
      private String lblTextblockservicogrupo_codigo_Jsonclick ;
      private String cmbavServicogrupo_codigo_Jsonclick ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String cmbavServico_codigo_Jsonclick ;
      private String lblTextblockcontratoservicos_undcntnome_Internalname ;
      private String lblTextblockcontratoservicos_undcntnome_Jsonclick ;
      private String edtavContratoservicos_undcntnome_Jsonclick ;
      private String lblTextblockcontratoservicos_qntuntcns_Internalname ;
      private String lblTextblockcontratoservicos_qntuntcns_Jsonclick ;
      private String edtavContratoservicos_qntuntcns_Jsonclick ;
      private String lblTextblockcomplexidade_Jsonclick ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Internalname ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick ;
      private String edtavContagemresultado_quantidadesolicitada_Jsonclick ;
      private String lblTextblockrequisitado_Internalname ;
      private String lblTextblockrequisitado_Jsonclick ;
      private String cmbavRequisitado_Jsonclick ;
      private String lblTextblockusuario_codigo_Jsonclick ;
      private String cmbavUsuario_codigo_Jsonclick ;
      private String lblLblvincularcom_Internalname ;
      private String lblLblvincularcom_Jsonclick ;
      private String edtavContagemresultado_dmnvinculadaref_Jsonclick ;
      private String lblTextblockpfbfsdaosvnc_Jsonclick ;
      private String edtavPfbfsdaosvnc_Jsonclick ;
      private String lblTextblockpflfsdaosvnc_Jsonclick ;
      private String edtavPflfsdaosvnc_Jsonclick ;
      private String lblTextblockcontratadaorigem_Internalname ;
      private String lblTextblockcontratadaorigem_Jsonclick ;
      private String dynavContratadaorigem_Jsonclick ;
      private String lblTextblockcontadorfs_Internalname ;
      private String lblTextblockcontadorfs_Jsonclick ;
      private String dynavContadorfs_Jsonclick ;
      private String lblTextblockcontratoservicos_localexec_Internalname ;
      private String lblTextblockcontratoservicos_localexec_Jsonclick ;
      private String cmbavContratoservicos_localexec_Jsonclick ;
      private String lblTextblockprazoanalise_Internalname ;
      private String lblTextblockprazoanalise_Jsonclick ;
      private String edtavPrazoanalise_Jsonclick ;
      private String lblTextblockprazoentrega_Internalname ;
      private String lblTextblockprazoentrega_Jsonclick ;
      private String edtavPrazoentrega_Jsonclick ;
      private String lblTextblocksistema_codigo_Jsonclick ;
      private String cmbavSistema_codigo_Jsonclick ;
      private String lblTextblockmodulo_codigo_Jsonclick ;
      private String dynavModulo_codigo_Jsonclick ;
      private String lblTextblockfuncaousuario_codigo_Jsonclick ;
      private String dynavFuncaousuario_codigo_Jsonclick ;
      private String lblTextblocksistema_gestor_Internalname ;
      private String lblTextblocksistema_gestor_Jsonclick ;
      private String edtavSistema_gestor_Jsonclick ;
      private String lblTextblockcontagemresultado_link_Internalname ;
      private String lblTextblockcontagemresultado_link_Jsonclick ;
      private String edtavContagemresultado_link_Jsonclick ;
      private String grpUnnamedgroup5_Internalname ;
      private String tblUnnamedtable4_Internalname ;
      private String tblTablemergeddmnvinculadas_Internalname ;
      private String cmbavDmnvinculadas_Jsonclick ;
      private String edtavContagemresultado_dmnvinculada_Jsonclick ;
      private String tblTablemergedcomplexidade_Internalname ;
      private String cmbavComplexidade_Jsonclick ;
      private String lblTextblockprioridade_codigo_Jsonclick ;
      private String cmbavPrioridade_codigo_Jsonclick ;
      private String lblRequisitadopor_righttext_Jsonclick ;
      private String edtavBtnexcluirreqneg_Jsonclick ;
      private String ROClassString ;
      private String edtavAgrupador_Jsonclick ;
      private String edtavSdt_requisitos__requisito_identificador_Jsonclick ;
      private String edtavSdt_requisitos__requisito_titulo_Jsonclick ;
      private String cmbavSdt_requisitos__requisito_prioridade_Jsonclick ;
      private String cmbavSdt_requisitos__requisito_status_Jsonclick ;
      private String Contagemresultado_observacao_Internalname ;
      private String Gxuitabspanel_tab_Internalname ;
      private DateTime AV63EntregaCalculada ;
      private DateTime AV70PrazoAnalise ;
      private DateTime AV62PrazoEntrega ;
      private DateTime GXt_dtime1 ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime Gx_date ;
      private DateTime A1869Contrato_DataTermino ;
      private DateTime AV8ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool AV134Contratante_UsaOSistema ;
      private bool n58Usuario_PessoaNom ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n1076Usuario_CargoUONom ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n39Contratada_Codigo ;
      private bool n75Contrato_AreaTrabalhoCod ;
      private bool A92Contrato_Ativo ;
      private bool n92Contrato_Ativo ;
      private bool A638ContratoServicos_Ativo ;
      private bool n699Sistema_Tipo ;
      private bool A130Sistema_Ativo ;
      private bool AV166Servico_IsOrigemReferencia ;
      private bool AV168Servico_IsSolicitaGestorSistema ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool n843Contrato_DataFimTA ;
      private bool n83Contrato_DataVigenciaTermino ;
      private bool A43Contratada_Ativo ;
      private bool n43Contratada_Ativo ;
      private bool n74Contrato_Codigo ;
      private bool toggleJsOutput ;
      private bool AV163RequerOrigem ;
      private bool AV108ServicoGrupo_FlagRequerSoft ;
      private bool A1428ServicoGrupo_FlagRequerSoft ;
      private bool AV53Contratante_OSAutomatica ;
      private bool AV61ContratanteSemEmailSda ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool AV85LerServico ;
      private bool A889Servico_Terceriza ;
      private bool A804ContagemResultado_SistemaAtivo ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool A1908Usuario_DeFerias ;
      private bool A54Usuario_Ativo ;
      private bool Contagemresultado_observacao_Enabled ;
      private bool Gxuitabspanel_tab_Autowidth ;
      private bool Gxuitabspanel_tab_Autoheight ;
      private bool Gxuitabspanel_tab_Autoscroll ;
      private bool wbLoad ;
      private bool AV139ContratoVencido ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV101RequisitadoPor ;
      private bool returnInSub ;
      private bool n29Contratante_Codigo ;
      private bool A593Contratante_OSAutomatica ;
      private bool A2085Contratante_RequerOrigem ;
      private bool n2085Contratante_RequerOrigem ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n547Contratante_EmailSdaHost ;
      private bool A1822Contratante_UsaOSistema ;
      private bool n1822Contratante_UsaOSistema ;
      private bool AV17CheckRequiredFieldsResult ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n1428ServicoGrupo_FlagRequerSoft ;
      private bool gx_refresh_fired ;
      private bool gx_BV219 ;
      private bool n1073Usuario_CargoCod ;
      private bool AV124OSPrompted ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n146Modulo_Codigo ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n804ContagemResultado_SistemaAtivo ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool AV130SistemaAtivo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n54Usuario_Ativo ;
      private bool n1072Servico_Atende ;
      private bool n1429Servico_ObrigaValores ;
      private bool n2132ContratoServicos_UndCntNome ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n913ContratoServicos_PrazoTipo ;
      private bool AV52SistemasTinhaFiltro ;
      private bool n1823ContratoServicosPrazo_Momento ;
      private bool n1265ContratoServicosPrazo_DiasBaixa ;
      private bool n1264ContratoServicosPrazo_DiasMedia ;
      private bool n1263ContratoServicosPrazo_DiasAlta ;
      private bool n889Servico_Terceriza ;
      private bool AV37Servico_Terceriza ;
      private bool n41Contratada_PessoaNom ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1908Usuario_DeFerias ;
      private bool AV165ContratanteSelUsrPrestadora ;
      private bool AV164SelUsrPrestadora ;
      private bool GXt_boolean9 ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool AV6WWPContext_gxTpr_Userehcontratante ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1452ContagemResultado_SS ;
      private bool n1637ContagemResultado_ServicoSSSgl ;
      private bool AV129ServicoTemArtefatos ;
      private bool n494ContagemResultado_Descricao ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool AV147btnExcluirReqNeg_IsBlob ;
      private String AV19ContagemResultado_Observacao ;
      private String AV15ContagemResultado_Link ;
      private String A158ServicoGrupo_Descricao ;
      private String AV146Agrupador ;
      private String AV5ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A1751Artefatos_Descricao ;
      private String AV182Btnexcluirreqneg_GXI ;
      private String AV69ContagemResultado_DemandaFM ;
      private String AV22ContagemResultado_DmnVinculada ;
      private String AV133ContagemResultado_DmnVinculadaRef ;
      private String AV167Sistema_Gestor ;
      private String A6AreaTrabalho_Descricao ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private String AV119Demandante ;
      private String AV118NomeSistema ;
      private String A494ContagemResultado_Descricao ;
      private String AV147btnExcluirReqNeg ;
      private GXWebComponent WebComp_Wcarquivosevd ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridrequisitosContainer ;
      private GXWebRow GridrequisitosRow ;
      private GXWebColumn GridrequisitosColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavRequisitadopor ;
      private GXCombobox dynavRequisitante ;
      private GXCombobox cmbavContrato ;
      private GXCombobox cmbavServicogrupo_codigo ;
      private GXCombobox cmbavServico_codigo ;
      private GXCombobox cmbavComplexidade ;
      private GXCombobox cmbavPrioridade_codigo ;
      private GXCombobox cmbavRequisitado ;
      private GXCombobox cmbavUsuario_codigo ;
      private GXCombobox cmbavDmnvinculadas ;
      private GXCombobox dynavContratadaorigem ;
      private GXCombobox dynavContadorfs ;
      private GXCombobox cmbavContratoservicos_localexec ;
      private GXCombobox cmbavSistema_codigo ;
      private GXCombobox dynavModulo_codigo ;
      private GXCombobox dynavFuncaousuario_codigo ;
      private GXCombobox cmbavSdt_requisitos__requisito_prioridade ;
      private GXCombobox cmbavSdt_requisitos__requisito_status ;
      private GXCombobox cmbavServico_atende ;
      private GXCombobox cmbavServico_obrigavalores ;
      private GXCheckbox chkavContratovencido ;
      private IDataStoreProvider pr_default ;
      private int[] H00CG3_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00CG3_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00CG3_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00CG3_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00CG3_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00CG3_n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00CG3_A54Usuario_Ativo ;
      private bool[] H00CG3_n54Usuario_Ativo ;
      private int[] H00CG3_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H00CG3_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] H00CG4_A40Contratada_PessoaCod ;
      private int[] H00CG4_A39Contratada_Codigo ;
      private bool[] H00CG4_n39Contratada_Codigo ;
      private String[] H00CG4_A41Contratada_PessoaNom ;
      private bool[] H00CG4_n41Contratada_PessoaNom ;
      private int[] H00CG4_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG4_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG4_A43Contratada_Ativo ;
      private bool[] H00CG4_n43Contratada_Ativo ;
      private int[] H00CG5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00CG5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00CG5_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00CG5_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00CG5_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00CG5_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00CG5_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00CG5_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00CG6_A146Modulo_Codigo ;
      private bool[] H00CG6_n146Modulo_Codigo ;
      private String[] H00CG6_A145Modulo_Sigla ;
      private int[] H00CG6_A127Sistema_Codigo ;
      private int[] H00CG7_A161FuncaoUsuario_Codigo ;
      private String[] H00CG7_A162FuncaoUsuario_Nome ;
      private int[] H00CG7_A127Sistema_Codigo ;
      private bool[] H00CG7_A164FuncaoUsuario_Ativo ;
      private int[] H00CG8_A29Contratante_Codigo ;
      private bool[] H00CG8_n29Contratante_Codigo ;
      private int[] H00CG8_A5AreaTrabalho_Codigo ;
      private bool[] H00CG8_A593Contratante_OSAutomatica ;
      private bool[] H00CG8_A2085Contratante_RequerOrigem ;
      private bool[] H00CG8_n2085Contratante_RequerOrigem ;
      private String[] H00CG8_A6AreaTrabalho_Descricao ;
      private String[] H00CG8_A548Contratante_EmailSdaUser ;
      private bool[] H00CG8_n548Contratante_EmailSdaUser ;
      private short[] H00CG8_A552Contratante_EmailSdaPort ;
      private bool[] H00CG8_n552Contratante_EmailSdaPort ;
      private String[] H00CG8_A547Contratante_EmailSdaHost ;
      private bool[] H00CG8_n547Contratante_EmailSdaHost ;
      private bool[] H00CG8_A1822Contratante_UsaOSistema ;
      private bool[] H00CG8_n1822Contratante_UsaOSistema ;
      private int[] H00CG9_A456ContagemResultado_Codigo ;
      private int[] H00CG9_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CG9_n490ContagemResultado_ContratadaCod ;
      private String[] H00CG9_A457ContagemResultado_Demanda ;
      private bool[] H00CG9_n457ContagemResultado_Demanda ;
      private int[] H00CG10_A157ServicoGrupo_Codigo ;
      private bool[] H00CG10_A1428ServicoGrupo_FlagRequerSoft ;
      private bool[] H00CG10_n1428ServicoGrupo_FlagRequerSoft ;
      private int[] H00CG11_A57Usuario_PessoaCod ;
      private int[] H00CG11_A1073Usuario_CargoCod ;
      private bool[] H00CG11_n1073Usuario_CargoCod ;
      private int[] H00CG11_A1Usuario_Codigo ;
      private String[] H00CG11_A58Usuario_PessoaNom ;
      private bool[] H00CG11_n58Usuario_PessoaNom ;
      private int[] H00CG11_A1075Usuario_CargoUOCod ;
      private bool[] H00CG11_n1075Usuario_CargoUOCod ;
      private String[] H00CG11_A1076Usuario_CargoUONom ;
      private bool[] H00CG11_n1076Usuario_CargoUONom ;
      private int[] H00CG13_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00CG13_n1553ContagemResultado_CntSrvCod ;
      private int[] H00CG13_A601ContagemResultado_Servico ;
      private bool[] H00CG13_n601ContagemResultado_Servico ;
      private String[] H00CG13_A484ContagemResultado_StatusDmn ;
      private bool[] H00CG13_n484ContagemResultado_StatusDmn ;
      private String[] H00CG13_A493ContagemResultado_DemandaFM ;
      private bool[] H00CG13_n493ContagemResultado_DemandaFM ;
      private int[] H00CG13_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CG13_n490ContagemResultado_ContratadaCod ;
      private int[] H00CG13_A456ContagemResultado_Codigo ;
      private String[] H00CG13_A801ContagemResultado_ServicoSigla ;
      private bool[] H00CG13_n801ContagemResultado_ServicoSigla ;
      private int[] H00CG13_A489ContagemResultado_SistemaCod ;
      private bool[] H00CG13_n489ContagemResultado_SistemaCod ;
      private int[] H00CG13_A146Modulo_Codigo ;
      private bool[] H00CG13_n146Modulo_Codigo ;
      private int[] H00CG13_A1044ContagemResultado_FncUsrCod ;
      private bool[] H00CG13_n1044ContagemResultado_FncUsrCod ;
      private String[] H00CG13_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00CG13_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00CG13_A798ContagemResultado_PFBFSImp ;
      private bool[] H00CG13_n798ContagemResultado_PFBFSImp ;
      private decimal[] H00CG13_A799ContagemResultado_PFLFSImp ;
      private bool[] H00CG13_n799ContagemResultado_PFLFSImp ;
      private bool[] H00CG13_A804ContagemResultado_SistemaAtivo ;
      private bool[] H00CG13_n804ContagemResultado_SistemaAtivo ;
      private String[] H00CG13_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00CG13_n803ContagemResultado_ContratadaSigla ;
      private decimal[] H00CG13_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00CG13_A683ContagemResultado_PFLFMUltima ;
      private decimal[] H00CG13_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00CG13_A685ContagemResultado_PFLFSUltima ;
      private int[] H00CG15_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00CG15_n1553ContagemResultado_CntSrvCod ;
      private int[] H00CG15_A601ContagemResultado_Servico ;
      private bool[] H00CG15_n601ContagemResultado_Servico ;
      private String[] H00CG15_A484ContagemResultado_StatusDmn ;
      private bool[] H00CG15_n484ContagemResultado_StatusDmn ;
      private String[] H00CG15_A457ContagemResultado_Demanda ;
      private bool[] H00CG15_n457ContagemResultado_Demanda ;
      private int[] H00CG15_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CG15_n490ContagemResultado_ContratadaCod ;
      private int[] H00CG15_A456ContagemResultado_Codigo ;
      private String[] H00CG15_A801ContagemResultado_ServicoSigla ;
      private bool[] H00CG15_n801ContagemResultado_ServicoSigla ;
      private int[] H00CG15_A489ContagemResultado_SistemaCod ;
      private bool[] H00CG15_n489ContagemResultado_SistemaCod ;
      private int[] H00CG15_A146Modulo_Codigo ;
      private bool[] H00CG15_n146Modulo_Codigo ;
      private int[] H00CG15_A1044ContagemResultado_FncUsrCod ;
      private bool[] H00CG15_n1044ContagemResultado_FncUsrCod ;
      private String[] H00CG15_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00CG15_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00CG15_A798ContagemResultado_PFBFSImp ;
      private bool[] H00CG15_n798ContagemResultado_PFBFSImp ;
      private decimal[] H00CG15_A799ContagemResultado_PFLFSImp ;
      private bool[] H00CG15_n799ContagemResultado_PFLFSImp ;
      private bool[] H00CG15_A804ContagemResultado_SistemaAtivo ;
      private bool[] H00CG15_n804ContagemResultado_SistemaAtivo ;
      private String[] H00CG15_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00CG15_n803ContagemResultado_ContratadaSigla ;
      private String[] H00CG15_A493ContagemResultado_DemandaFM ;
      private bool[] H00CG15_n493ContagemResultado_DemandaFM ;
      private decimal[] H00CG15_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00CG15_A683ContagemResultado_PFLFMUltima ;
      private decimal[] H00CG15_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00CG15_A685ContagemResultado_PFLFSUltima ;
      private int[] H00CG17_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00CG17_n1553ContagemResultado_CntSrvCod ;
      private int[] H00CG17_A601ContagemResultado_Servico ;
      private bool[] H00CG17_n601ContagemResultado_Servico ;
      private int[] H00CG17_A456ContagemResultado_Codigo ;
      private String[] H00CG17_A493ContagemResultado_DemandaFM ;
      private bool[] H00CG17_n493ContagemResultado_DemandaFM ;
      private String[] H00CG17_A457ContagemResultado_Demanda ;
      private bool[] H00CG17_n457ContagemResultado_Demanda ;
      private String[] H00CG17_A801ContagemResultado_ServicoSigla ;
      private bool[] H00CG17_n801ContagemResultado_ServicoSigla ;
      private int[] H00CG17_A489ContagemResultado_SistemaCod ;
      private bool[] H00CG17_n489ContagemResultado_SistemaCod ;
      private int[] H00CG17_A146Modulo_Codigo ;
      private bool[] H00CG17_n146Modulo_Codigo ;
      private int[] H00CG17_A1044ContagemResultado_FncUsrCod ;
      private bool[] H00CG17_n1044ContagemResultado_FncUsrCod ;
      private int[] H00CG17_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CG17_n490ContagemResultado_ContratadaCod ;
      private bool[] H00CG17_A804ContagemResultado_SistemaAtivo ;
      private bool[] H00CG17_n804ContagemResultado_SistemaAtivo ;
      private String[] H00CG17_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00CG17_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00CG17_A798ContagemResultado_PFBFSImp ;
      private bool[] H00CG17_n798ContagemResultado_PFBFSImp ;
      private decimal[] H00CG17_A799ContagemResultado_PFLFSImp ;
      private bool[] H00CG17_n799ContagemResultado_PFLFSImp ;
      private decimal[] H00CG17_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00CG17_A683ContagemResultado_PFLFMUltima ;
      private decimal[] H00CG17_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00CG17_A685ContagemResultado_PFLFSUltima ;
      private int[] H00CG18_A74Contrato_Codigo ;
      private bool[] H00CG18_n74Contrato_Codigo ;
      private int[] H00CG18_A1013Contrato_PrepostoCod ;
      private bool[] H00CG18_n1013Contrato_PrepostoCod ;
      private int[] H00CG18_A39Contratada_Codigo ;
      private bool[] H00CG18_n39Contratada_Codigo ;
      private int[] H00CG21_A39Contratada_Codigo ;
      private bool[] H00CG21_n39Contratada_Codigo ;
      private int[] H00CG21_A1013Contrato_PrepostoCod ;
      private bool[] H00CG21_n1013Contrato_PrepostoCod ;
      private int[] H00CG21_A1212ContratoServicos_UnidadeContratada ;
      private int[] H00CG21_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] H00CG21_A160ContratoServicos_Codigo ;
      private int[] H00CG21_A155Servico_Codigo ;
      private int[] H00CG21_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG21_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG21_A43Contratada_Ativo ;
      private bool[] H00CG21_n43Contratada_Ativo ;
      private bool[] H00CG21_A54Usuario_Ativo ;
      private bool[] H00CG21_n54Usuario_Ativo ;
      private int[] H00CG21_A74Contrato_Codigo ;
      private bool[] H00CG21_n74Contrato_Codigo ;
      private String[] H00CG21_A1072Servico_Atende ;
      private bool[] H00CG21_n1072Servico_Atende ;
      private int[] H00CG21_A157ServicoGrupo_Codigo ;
      private String[] H00CG21_A1429Servico_ObrigaValores ;
      private bool[] H00CG21_n1429Servico_ObrigaValores ;
      private bool[] H00CG21_A2092Servico_IsOrigemReferencia ;
      private bool[] H00CG21_A2094ContratoServicos_SolicitaGestorSistema ;
      private String[] H00CG21_A2132ContratoServicos_UndCntNome ;
      private bool[] H00CG21_n2132ContratoServicos_UndCntNome ;
      private decimal[] H00CG21_A1340ContratoServicos_QntUntCns ;
      private bool[] H00CG21_n1340ContratoServicos_QntUntCns ;
      private short[] H00CG21_A1152ContratoServicos_PrazoAnalise ;
      private bool[] H00CG21_n1152ContratoServicos_PrazoAnalise ;
      private String[] H00CG21_A1454ContratoServicos_PrazoTpDias ;
      private bool[] H00CG21_n1454ContratoServicos_PrazoTpDias ;
      private decimal[] H00CG21_A557Servico_VlrUnidadeContratada ;
      private decimal[] H00CG21_A116Contrato_ValorUnidadeContratacao ;
      private short[] H00CG21_A1649ContratoServicos_PrazoInicio ;
      private bool[] H00CG21_n1649ContratoServicos_PrazoInicio ;
      private String[] H00CG21_A913ContratoServicos_PrazoTipo ;
      private bool[] H00CG21_n913ContratoServicos_PrazoTipo ;
      private DateTime[] H00CG21_A843Contrato_DataFimTA ;
      private bool[] H00CG21_n843Contrato_DataFimTA ;
      private DateTime[] H00CG21_A83Contrato_DataVigenciaTermino ;
      private bool[] H00CG21_n83Contrato_DataVigenciaTermino ;
      private int[] H00CG22_A160ContratoServicos_Codigo ;
      private int[] H00CG22_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] H00CG22_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] H00CG23_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] H00CG23_A1823ContratoServicosPrazo_Momento ;
      private bool[] H00CG23_n1823ContratoServicosPrazo_Momento ;
      private short[] H00CG23_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] H00CG23_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] H00CG23_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] H00CG23_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] H00CG23_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] H00CG23_n1263ContratoServicosPrazo_DiasAlta ;
      private int[] H00CG24_A1335ContratoServicosPrioridade_CntSrvCod ;
      private String[] H00CG24_A1337ContratoServicosPrioridade_Nome ;
      private int[] H00CG24_A1336ContratoServicosPrioridade_Codigo ;
      private int[] H00CG25_A155Servico_Codigo ;
      private int[] H00CG25_A160ContratoServicos_Codigo ;
      private String[] H00CG25_A639ContratoServicos_LocalExec ;
      private bool[] H00CG25_A889Servico_Terceriza ;
      private bool[] H00CG25_n889Servico_Terceriza ;
      private String[] H00CG25_A1454ContratoServicos_PrazoTpDias ;
      private bool[] H00CG25_n1454ContratoServicos_PrazoTpDias ;
      private short[] H00CG25_A1152ContratoServicos_PrazoAnalise ;
      private bool[] H00CG25_n1152ContratoServicos_PrazoAnalise ;
      private short[] H00CG25_A1649ContratoServicos_PrazoInicio ;
      private bool[] H00CG25_n1649ContratoServicos_PrazoInicio ;
      private int[] H00CG26_A39Contratada_Codigo ;
      private bool[] H00CG26_n39Contratada_Codigo ;
      private int[] H00CG26_A74Contrato_Codigo ;
      private bool[] H00CG26_n74Contrato_Codigo ;
      private int[] H00CG26_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG26_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG26_A43Contratada_Ativo ;
      private bool[] H00CG26_n43Contratada_Ativo ;
      private bool[] H00CG26_A54Usuario_Ativo ;
      private bool[] H00CG26_n54Usuario_Ativo ;
      private int[] H00CG26_A1013Contrato_PrepostoCod ;
      private bool[] H00CG26_n1013Contrato_PrepostoCod ;
      private int[] H00CG27_A1078ContratoGestor_ContratoCod ;
      private int[] H00CG27_A1079ContratoGestor_UsuarioCod ;
      private int[] H00CG27_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00CG27_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00CG28_A1078ContratoGestor_ContratoCod ;
      private int[] H00CG28_A1079ContratoGestor_UsuarioCod ;
      private int[] H00CG28_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00CG28_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00CG31_A160ContratoServicos_Codigo ;
      private int[] H00CG31_A74Contrato_Codigo ;
      private bool[] H00CG31_n74Contrato_Codigo ;
      private int[] H00CG31_A40Contratada_PessoaCod ;
      private int[] H00CG31_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00CG31_n75Contrato_AreaTrabalhoCod ;
      private int[] H00CG31_A155Servico_Codigo ;
      private bool[] H00CG31_A43Contratada_Ativo ;
      private bool[] H00CG31_n43Contratada_Ativo ;
      private bool[] H00CG31_A92Contrato_Ativo ;
      private bool[] H00CG31_n92Contrato_Ativo ;
      private bool[] H00CG31_A638ContratoServicos_Ativo ;
      private DateTime[] H00CG31_A83Contrato_DataVigenciaTermino ;
      private bool[] H00CG31_n83Contrato_DataVigenciaTermino ;
      private int[] H00CG31_A39Contratada_Codigo ;
      private bool[] H00CG31_n39Contratada_Codigo ;
      private String[] H00CG31_A41Contratada_PessoaNom ;
      private bool[] H00CG31_n41Contratada_PessoaNom ;
      private DateTime[] H00CG31_A843Contrato_DataFimTA ;
      private bool[] H00CG31_n843Contrato_DataFimTA ;
      private int[] H00CG34_A160ContratoServicos_Codigo ;
      private int[] H00CG34_A74Contrato_Codigo ;
      private bool[] H00CG34_n74Contrato_Codigo ;
      private int[] H00CG34_A40Contratada_PessoaCod ;
      private bool[] H00CG34_A43Contratada_Ativo ;
      private bool[] H00CG34_n43Contratada_Ativo ;
      private bool[] H00CG34_A92Contrato_Ativo ;
      private bool[] H00CG34_n92Contrato_Ativo ;
      private bool[] H00CG34_A638ContratoServicos_Ativo ;
      private DateTime[] H00CG34_A83Contrato_DataVigenciaTermino ;
      private bool[] H00CG34_n83Contrato_DataVigenciaTermino ;
      private int[] H00CG34_A155Servico_Codigo ;
      private int[] H00CG34_A39Contratada_Codigo ;
      private bool[] H00CG34_n39Contratada_Codigo ;
      private String[] H00CG34_A41Contratada_PessoaNom ;
      private bool[] H00CG34_n41Contratada_PessoaNom ;
      private DateTime[] H00CG34_A843Contrato_DataFimTA ;
      private bool[] H00CG34_n843Contrato_DataFimTA ;
      private int[] H00CG35_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00CG35_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00CG35_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00CG35_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00CG35_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00CG35_n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00CG35_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00CG35_n1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00CG35_A43Contratada_Ativo ;
      private bool[] H00CG35_n43Contratada_Ativo ;
      private bool[] H00CG35_A1908Usuario_DeFerias ;
      private bool[] H00CG35_n1908Usuario_DeFerias ;
      private int[] H00CG36_A828UsuarioServicos_UsuarioCod ;
      private int[] H00CG36_A829UsuarioServicos_ServicoCod ;
      private int[] H00CG37_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00CG37_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00CG37_A63ContratanteUsuario_ContratanteCod ;
      private bool[] H00CG37_A1908Usuario_DeFerias ;
      private bool[] H00CG37_n1908Usuario_DeFerias ;
      private bool[] H00CG37_A54Usuario_Ativo ;
      private bool[] H00CG37_n54Usuario_Ativo ;
      private int[] H00CG37_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00CG37_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00CG37_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00CG38_A155Servico_Codigo ;
      private int[] H00CG38_A74Contrato_Codigo ;
      private bool[] H00CG38_n74Contrato_Codigo ;
      private int[] H00CG38_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00CG38_n75Contrato_AreaTrabalhoCod ;
      private bool[] H00CG38_A638ContratoServicos_Ativo ;
      private bool[] H00CG38_A92Contrato_Ativo ;
      private bool[] H00CG38_n92Contrato_Ativo ;
      private int[] H00CG38_A157ServicoGrupo_Codigo ;
      private String[] H00CG38_A158ServicoGrupo_Descricao ;
      private int[] H00CG39_A1136ContratoGestor_ContratadaCod ;
      private bool[] H00CG39_n1136ContratoGestor_ContratadaCod ;
      private int[] H00CG39_A1013Contrato_PrepostoCod ;
      private bool[] H00CG39_n1013Contrato_PrepostoCod ;
      private int[] H00CG39_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] H00CG39_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] H00CG39_A1078ContratoGestor_ContratoCod ;
      private bool[] H00CG39_A54Usuario_Ativo ;
      private bool[] H00CG39_n54Usuario_Ativo ;
      private int[] H00CG39_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG39_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG39_A43Contratada_Ativo ;
      private bool[] H00CG39_n43Contratada_Ativo ;
      private bool[] H00CG39_A92Contrato_Ativo ;
      private bool[] H00CG39_n92Contrato_Ativo ;
      private DateTime[] H00CG39_A83Contrato_DataVigenciaTermino ;
      private bool[] H00CG39_n83Contrato_DataVigenciaTermino ;
      private int[] H00CG39_A1079ContratoGestor_UsuarioCod ;
      private int[] H00CG40_A160ContratoServicos_Codigo ;
      private int[] H00CG40_A74Contrato_Codigo ;
      private bool[] H00CG40_n74Contrato_Codigo ;
      private int[] H00CG40_A157ServicoGrupo_Codigo ;
      private bool[] H00CG40_A638ContratoServicos_Ativo ;
      private int[] H00CG40_A155Servico_Codigo ;
      private int[] H00CG41_A1013Contrato_PrepostoCod ;
      private bool[] H00CG41_n1013Contrato_PrepostoCod ;
      private int[] H00CG41_A39Contratada_Codigo ;
      private bool[] H00CG41_n39Contratada_Codigo ;
      private int[] H00CG41_A75Contrato_AreaTrabalhoCod ;
      private bool[] H00CG41_n75Contrato_AreaTrabalhoCod ;
      private int[] H00CG41_A1824ContratoAuxiliar_ContratoCod ;
      private int[] H00CG41_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG41_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG41_A54Usuario_Ativo ;
      private bool[] H00CG41_n54Usuario_Ativo ;
      private bool[] H00CG41_A43Contratada_Ativo ;
      private bool[] H00CG41_n43Contratada_Ativo ;
      private bool[] H00CG41_A92Contrato_Ativo ;
      private bool[] H00CG41_n92Contrato_Ativo ;
      private DateTime[] H00CG41_A83Contrato_DataVigenciaTermino ;
      private bool[] H00CG41_n83Contrato_DataVigenciaTermino ;
      private int[] H00CG41_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] H00CG42_A160ContratoServicos_Codigo ;
      private int[] H00CG42_A74Contrato_Codigo ;
      private bool[] H00CG42_n74Contrato_Codigo ;
      private int[] H00CG42_A157ServicoGrupo_Codigo ;
      private bool[] H00CG42_A638ContratoServicos_Ativo ;
      private int[] H00CG42_A155Servico_Codigo ;
      private int[] H00CG43_A155Servico_Codigo ;
      private String[] H00CG43_A608Servico_Nome ;
      private String[] H00CG43_A605Servico_Sigla ;
      private int[] H00CG44_A127Sistema_Codigo ;
      private bool[] H00CG44_A130Sistema_Ativo ;
      private String[] H00CG44_A699Sistema_Tipo ;
      private bool[] H00CG44_n699Sistema_Tipo ;
      private int[] H00CG44_A135Sistema_AreaTrabalhoCod ;
      private String[] H00CG44_A129Sistema_Sigla ;
      private int[] H00CG47_A1013Contrato_PrepostoCod ;
      private bool[] H00CG47_n1013Contrato_PrepostoCod ;
      private String[] H00CG47_A77Contrato_Numero ;
      private int[] H00CG47_A74Contrato_Codigo ;
      private bool[] H00CG47_n74Contrato_Codigo ;
      private int[] H00CG47_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG47_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG47_A43Contratada_Ativo ;
      private bool[] H00CG47_n43Contratada_Ativo ;
      private bool[] H00CG47_A54Usuario_Ativo ;
      private bool[] H00CG47_n54Usuario_Ativo ;
      private bool[] H00CG47_A92Contrato_Ativo ;
      private bool[] H00CG47_n92Contrato_Ativo ;
      private int[] H00CG47_A39Contratada_Codigo ;
      private bool[] H00CG47_n39Contratada_Codigo ;
      private DateTime[] H00CG47_A843Contrato_DataFimTA ;
      private bool[] H00CG47_n843Contrato_DataFimTA ;
      private DateTime[] H00CG47_A83Contrato_DataVigenciaTermino ;
      private bool[] H00CG47_n83Contrato_DataVigenciaTermino ;
      private int[] H00CG48_A160ContratoServicos_Codigo ;
      private int[] H00CG48_A74Contrato_Codigo ;
      private bool[] H00CG48_n74Contrato_Codigo ;
      private int[] H00CG48_A155Servico_Codigo ;
      private int[] H00CG50_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00CG50_n1553ContagemResultado_CntSrvCod ;
      private int[] H00CG50_A601ContagemResultado_Servico ;
      private bool[] H00CG50_n601ContagemResultado_Servico ;
      private int[] H00CG50_A456ContagemResultado_Codigo ;
      private int[] H00CG50_A1636ContagemResultado_ServicoSS ;
      private bool[] H00CG50_n1636ContagemResultado_ServicoSS ;
      private int[] H00CG50_A1452ContagemResultado_SS ;
      private bool[] H00CG50_n1452ContagemResultado_SS ;
      private String[] H00CG50_A1637ContagemResultado_ServicoSSSgl ;
      private bool[] H00CG50_n1637ContagemResultado_ServicoSSSgl ;
      private String[] H00CG50_A493ContagemResultado_DemandaFM ;
      private bool[] H00CG50_n493ContagemResultado_DemandaFM ;
      private String[] H00CG50_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00CG50_n803ContagemResultado_ContratadaSigla ;
      private String[] H00CG50_A801ContagemResultado_ServicoSigla ;
      private bool[] H00CG50_n801ContagemResultado_ServicoSigla ;
      private int[] H00CG50_A489ContagemResultado_SistemaCod ;
      private bool[] H00CG50_n489ContagemResultado_SistemaCod ;
      private bool[] H00CG50_A804ContagemResultado_SistemaAtivo ;
      private bool[] H00CG50_n804ContagemResultado_SistemaAtivo ;
      private int[] H00CG50_A146Modulo_Codigo ;
      private bool[] H00CG50_n146Modulo_Codigo ;
      private int[] H00CG50_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CG50_n490ContagemResultado_ContratadaCod ;
      private int[] H00CG50_A1044ContagemResultado_FncUsrCod ;
      private bool[] H00CG50_n1044ContagemResultado_FncUsrCod ;
      private String[] H00CG50_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00CG50_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00CG50_A798ContagemResultado_PFBFSImp ;
      private bool[] H00CG50_n798ContagemResultado_PFBFSImp ;
      private decimal[] H00CG50_A799ContagemResultado_PFLFSImp ;
      private bool[] H00CG50_n799ContagemResultado_PFLFSImp ;
      private int[] H00CG50_A584ContagemResultado_ContadorFM ;
      private decimal[] H00CG50_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00CG50_A683ContagemResultado_PFLFMUltima ;
      private decimal[] H00CG50_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00CG50_A685ContagemResultado_PFLFSUltima ;
      private int[] H00CG51_A160ContratoServicos_Codigo ;
      private int[] H00CG51_A1749Artefatos_Codigo ;
      private String[] H00CG51_A1751Artefatos_Descricao ;
      private int[] H00CG52_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00CG52_n52Contratada_AreaTrabalhoCod ;
      private int[] H00CG52_A39Contratada_Codigo ;
      private bool[] H00CG52_n39Contratada_Codigo ;
      private int[] H00CG53_A40Contratada_PessoaCod ;
      private int[] H00CG53_A39Contratada_Codigo ;
      private bool[] H00CG53_n39Contratada_Codigo ;
      private String[] H00CG53_A41Contratada_PessoaNom ;
      private bool[] H00CG53_n41Contratada_PessoaNom ;
      private int[] H00CG54_A160ContratoServicos_Codigo ;
      private String[] H00CG54_A158ServicoGrupo_Descricao ;
      private int[] H00CG54_A157ServicoGrupo_Codigo ;
      private String[] H00CG54_A608Servico_Nome ;
      private String[] H00CG54_A605Servico_Sigla ;
      private int[] H00CG54_A155Servico_Codigo ;
      private int[] H00CG55_A456ContagemResultado_Codigo ;
      private String[] H00CG55_A494ContagemResultado_Descricao ;
      private bool[] H00CG55_n494ContagemResultado_Descricao ;
      private String[] H00CG55_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00CG55_n509ContagemrResultado_SistemaSigla ;
      private int[] H00CG55_A489ContagemResultado_SistemaCod ;
      private bool[] H00CG55_n489ContagemResultado_SistemaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV29Websession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV132ContratadasDaArea ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV51Sistemas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV136Servicos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV42Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV158Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV131Contratadas ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV46Attachments ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Requisitos_SDT_RequisitosItem ))]
      private IGxCollection AV161SDT_Requisitos ;
      [ObjectCollection(ItemType=typeof( SdtFckEditorMenu_FckEditorMenuItem ))]
      private IGxCollection AV91FckEditorMenu ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Artefatos ))]
      private IGxCollection AV128sdt_Artefatos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV137SDT_Servicos ;
      private GXWebForm Form ;
      private SdtSDT_Requisitos_SDT_RequisitosItem AV160SDT_RequisitosItem ;
      private SdtFckEditorMenu_FckEditorMenuItem AV89FCKEditorMenuItem ;
      private SdtSDT_Artefatos AV127sdt_Artefato ;
      private SdtSDT_Codigos AV138SDT_Servico ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wp_os__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00CG13( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV132ContratadasDaArea ,
                                              String A484ContagemResultado_StatusDmn ,
                                              String A493ContagemResultado_DemandaFM ,
                                              String AV22ContagemResultado_DmnVinculada )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int12 ;
         GXv_int12 = new short [1] ;
         Object[] GXv_Object13 ;
         GXv_Object13 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[Modulo_Codigo], T1.[ContagemResultado_FncUsrCod], T4.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], T6.[Sistema_Ativo] AS ContagemResultado_SistemaAtivo, T4.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo]";
         scmdbuf = scmdbuf + " ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV132ContratadasDaArea, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_DemandaFM] = @AV22ContagemResultado_DmnVinculada)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod]";
         GXv_Object13[0] = scmdbuf;
         GXv_Object13[1] = GXv_int12;
         return GXv_Object13 ;
      }

      protected Object[] conditional_H00CG15( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV132ContratadasDaArea ,
                                              String A484ContagemResultado_StatusDmn ,
                                              String A457ContagemResultado_Demanda ,
                                              String AV133ContagemResultado_DmnVinculadaRef )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int14 ;
         GXv_int14 = new short [1] ;
         Object[] GXv_Object15 ;
         GXv_Object15 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[Modulo_Codigo], T1.[ContagemResultado_FncUsrCod], T4.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], T6.[Sistema_Ativo] AS ContagemResultado_SistemaAtivo, T4.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_DemandaFM], COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima]";
         scmdbuf = scmdbuf + " = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV132ContratadasDaArea, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Demanda] = @AV133ContagemResultado_DmnVinculadaRef)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod]";
         GXv_Object15[0] = scmdbuf;
         GXv_Object15[1] = GXv_int14;
         return GXv_Object15 ;
      }

      protected Object[] conditional_H00CG39( IGxContext context ,
                                              bool AV6WWPContext_gxTpr_Userehcontratante ,
                                              bool AV134Contratante_UsaOSistema ,
                                              int A1079ContratoGestor_UsuarioCod ,
                                              short AV6WWPContext_gxTpr_Userid ,
                                              int AV102Requisitante ,
                                              DateTime A843Contrato_DataFimTA ,
                                              DateTime A83Contrato_DataVigenciaTermino ,
                                              bool A92Contrato_Ativo ,
                                              bool A43Contratada_Ativo ,
                                              int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A1446ContratoGestor_ContratadaAreaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int16 ;
         GXv_int16 = new short [3] ;
         Object[] GXv_Object17 ;
         GXv_Object17 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T2.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T4.[Usuario_Ativo], T3.[Contratada_AreaTrabalhoCod], T3.[Contratada_Ativo], T2.[Contrato_Ativo], T2.[Contrato_DataVigenciaTermino], T1.[ContratoGestor_UsuarioCod] FROM ((([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T2.[Contrato_PrepostoCod])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contrato_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T2.[Contrato_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T3.[Contratada_Ativo] = 1)";
         if ( AV6WWPContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (T1.[ContratoGestor_UsuarioCod] = @AV6WWPContext__Userid)";
         }
         else
         {
            GXv_int16[1] = 1;
         }
         if ( ! AV6WWPContext_gxTpr_Userehcontratante && ! AV134Contratante_UsaOSistema )
         {
            sWhereString = sWhereString + " and (T1.[ContratoGestor_UsuarioCod] = @AV102Requisitante)";
         }
         else
         {
            GXv_int16[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_AreaTrabalhoCod], T1.[ContratoGestor_UsuarioCod]";
         GXv_Object17[0] = scmdbuf;
         GXv_Object17[1] = GXv_int16;
         return GXv_Object17 ;
      }

      protected Object[] conditional_H00CG41( IGxContext context ,
                                              bool AV6WWPContext_gxTpr_Userehcontratante ,
                                              bool AV134Contratante_UsaOSistema ,
                                              int A1825ContratoAuxiliar_UsuarioCod ,
                                              short AV6WWPContext_gxTpr_Userid ,
                                              int AV102Requisitante ,
                                              DateTime A843Contrato_DataFimTA ,
                                              DateTime A83Contrato_DataVigenciaTermino ,
                                              bool A92Contrato_Ativo ,
                                              bool A43Contratada_Ativo ,
                                              int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A75Contrato_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int18 ;
         GXv_int18 = new short [3] ;
         Object[] GXv_Object19 ;
         GXv_Object19 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T2.[Contratada_Codigo], T2.[Contrato_AreaTrabalhoCod], T1.[ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, T4.[Contratada_AreaTrabalhoCod], T3.[Usuario_Ativo], T4.[Contratada_Ativo], T2.[Contrato_Ativo], T2.[Contrato_DataVigenciaTermino], T1.[ContratoAuxiliar_UsuarioCod] FROM ((([ContratoAuxiliar] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoAuxiliar_ContratoCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T2.[Contrato_PrepostoCod]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T2.[Contratada_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contrato_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T2.[Contrato_Ativo] = 1)";
         scmdbuf = scmdbuf + " and (T4.[Contratada_Ativo] = 1)";
         if ( AV6WWPContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (T1.[ContratoAuxiliar_UsuarioCod] = @AV6WWPContext__Userid)";
         }
         else
         {
            GXv_int18[1] = 1;
         }
         if ( ! AV6WWPContext_gxTpr_Userehcontratante && ! AV134Contratante_UsaOSistema )
         {
            sWhereString = sWhereString + " and (T1.[ContratoAuxiliar_UsuarioCod] = @AV102Requisitante)";
         }
         else
         {
            GXv_int18[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_AreaTrabalhoCod], T1.[ContratoAuxiliar_UsuarioCod]";
         GXv_Object19[0] = scmdbuf;
         GXv_Object19[1] = GXv_int18;
         return GXv_Object19 ;
      }

      protected Object[] conditional_H00CG43( IGxContext context ,
                                              int A155Servico_Codigo ,
                                              IGxCollection AV136Servicos ,
                                              int AV170ServicoCodigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int20 ;
         GXv_int20 = new short [1] ;
         Object[] GXv_Object21 ;
         GXv_Object21 = new Object [2] ;
         scmdbuf = "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Sigla] FROM [Servico] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Servico_Codigo] = @AV170ServicoCodigo)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV136Servicos, "[Servico_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Servico_Codigo]";
         GXv_Object21[0] = scmdbuf;
         GXv_Object21[1] = GXv_int20;
         return GXv_Object21 ;
      }

      protected Object[] conditional_H00CG44( IGxContext context ,
                                              int A127Sistema_Codigo ,
                                              IGxCollection AV51Sistemas ,
                                              int AV51Sistemas_Count ,
                                              bool A130Sistema_Ativo ,
                                              int A135Sistema_AreaTrabalhoCod ,
                                              int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                              String A699Sistema_Tipo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int22 ;
         GXv_int22 = new short [1] ;
         Object[] GXv_Object23 ;
         GXv_Object23 = new Object [2] ;
         scmdbuf = "SELECT [Sistema_Codigo], [Sistema_Ativo], [Sistema_Tipo], [Sistema_AreaTrabalhoCod], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Sistema_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ([Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and ([Sistema_Tipo] = 'A')";
         if ( AV51Sistemas_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV51Sistemas, "[Sistema_Codigo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Sistema_Sigla]";
         GXv_Object23[0] = scmdbuf;
         GXv_Object23[1] = GXv_int22;
         return GXv_Object23 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 9 :
                     return conditional_H00CG13(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] );
               case 10 :
                     return conditional_H00CG15(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] );
               case 27 :
                     return conditional_H00CG39(context, (bool)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (bool)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
               case 29 :
                     return conditional_H00CG41(context, (bool)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (bool)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
               case 31 :
                     return conditional_H00CG43(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] );
               case 32 :
                     return conditional_H00CG44(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CG3 ;
          prmH00CG3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG4 ;
          prmH00CG4 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG5 ;
          prmH00CG5 = new Object[] {
          new Object[] {"@AV105ContratadaOrigem",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG6 ;
          prmH00CG6 = new Object[] {
          new Object[] {"@AV38Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG7 ;
          prmH00CG7 = new Object[] {
          new Object[] {"@AV38Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG8 ;
          prmH00CG8 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG9 ;
          prmH00CG9 = new Object[] {
          new Object[] {"@AV69ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV12RequisitadoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG10 ;
          prmH00CG10 = new Object[] {
          new Object[] {"@AV16ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG11 ;
          prmH00CG11 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00CG17 ;
          prmH00CG17 = new Object[] {
          new Object[] {"@AV123DmnVinculadas",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00CG17 ;
          cmdBufferH00CG17=" SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[Modulo_Codigo], T1.[ContagemResultado_FncUsrCod], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Sistema_Ativo] AS ContagemResultado_SistemaAtivo, T6.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T4.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) "
          + " LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV123DmnVinculadas ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmH00CG18 ;
          prmH00CG18 = new Object[] {
          new Object[] {"@AV105ContratadaOrigem",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG21 ;
          prmH00CG21 = new Object[] {
          new Object[] {"@AV117Contrato",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00CG21 ;
          cmdBufferH00CG21=" SELECT TOP 1 T5.[Contratada_Codigo], T5.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T3.[ContratoServicosPrazo_CntSrvCod], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T6.[Contratada_AreaTrabalhoCod], T6.[Contratada_Ativo], T7.[Usuario_Ativo], T1.[Contrato_Codigo], T4.[Servico_Atende], T4.[ServicoGrupo_Codigo], T4.[Servico_ObrigaValores], T4.[Servico_IsOrigemReferencia], T1.[ContratoServicos_SolicitaGestorSistema], T2.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T1.[ContratoServicos_QntUntCns], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_PrazoTpDias], T1.[Servico_VlrUnidadeContratada], T5.[Contrato_ValorUnidadeContratacao], T1.[ContratoServicos_PrazoInicio], COALESCE( T3.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( T8.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T5.[Contrato_DataVigenciaTermino] FROM ((((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicos_UnidadeContratada]) LEFT JOIN [ContratoServicosPrazo] T3 WITH (NOLOCK) ON T3.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicos_Codigo]) INNER JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T5.[Contratada_Codigo]) LEFT JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = T5.[Contrato_PrepostoCod]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataFim], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T9 WITH "
          + " (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC5] ) T8 ON T8.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV117Contrato) AND (T1.[Servico_Codigo] = @AV13Servico_Codigo) ORDER BY T1.[Contrato_Codigo]" ;
          Object[] prmH00CG22 ;
          prmH00CG22 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG23 ;
          prmH00CG23 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG24 ;
          prmH00CG24 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG25 ;
          prmH00CG25 = new Object[] {
          new Object[] {"@AV49ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG26 ;
          prmH00CG26 = new Object[] {
          new Object[] {"@AV117Contrato",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG27 ;
          prmH00CG27 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG28 ;
          prmH00CG28 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG31 ;
          prmH00CG31 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG34 ;
          prmH00CG34 = new Object[] {
          new Object[] {"@AV13Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG35 ;
          prmH00CG35 = new Object[] {
          new Object[] {"@AV12RequisitadoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG36 ;
          prmH00CG36 = new Object[] {
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG37 ;
          prmH00CG37 = new Object[] {
          new Object[] {"@AV12RequisitadoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG38 ;
          prmH00CG38 = new Object[] {
          new Object[] {"@AV100AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG40 ;
          prmH00CG40 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG42 ;
          prmH00CG42 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG47 ;
          prmH00CG47 = new Object[] {
          new Object[] {"@AV12RequisitadoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8ContagemResultado_DataDmn",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmH00CG48 ;
          prmH00CG48 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG50 ;
          prmH00CG50 = new Object[] {
          new Object[] {"@AV23ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00CG50 ;
          cmdBufferH00CG50=" SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, T1.[ContagemResultado_SS], T5.[Servico_Sigla] AS ContagemResultado_ServicoSSSgl, T1.[ContagemResultado_DemandaFM], T7.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T6.[Sistema_Ativo] AS ContagemResultado_SistemaAtivo, T1.[Modulo_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_FncUsrCod], T7.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T1.[ContagemResultado_PFBFSImp], T1.[ContagemResultado_PFLFSImp], COALESCE( T4.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T4.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM (((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, "
          + " MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T1.[ContagemResultado_ServicoSS]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T7 WITH (NOLOCK) ON T7.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV23ContagemResultado_OSVinculada ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmH00CG51 ;
          prmH00CG51 = new Object[] {
          new Object[] {"@AV49ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG52 ;
          prmH00CG52 = new Object[] {
          new Object[] {"@AV100AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG53 ;
          prmH00CG53 = new Object[] {
          new Object[] {"@AV12RequisitadoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG54 ;
          prmH00CG54 = new Object[] {
          new Object[] {"@AV49ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG55 ;
          prmH00CG55 = new Object[] {
          new Object[] {"@AV23ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG13 ;
          prmH00CG13 = new Object[] {
          new Object[] {"@AV22ContagemResultado_DmnVinculada",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmH00CG15 ;
          prmH00CG15 = new Object[] {
          new Object[] {"@AV133ContagemResultado_DmnVinculadaRef",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmH00CG39 ;
          prmH00CG39 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV102Requisitante",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG41 ;
          prmH00CG41 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV102Requisitante",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG43 ;
          prmH00CG43 = new Object[] {
          new Object[] {"@AV170ServicoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00CG44 ;
          prmH00CG44 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CG3", "SELECT T3.[Pessoa_Codigo] AS ContratanteUsuario_UsuarioPess, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess, T2.[Usuario_Ativo], COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE T5.[Contratante_Codigo] = T6.[ContratanteUsuario_ContratanteCod] GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG3,0,0,true,false )
             ,new CursorDef("H00CG4", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG4,0,0,true,false )
             ,new CursorDef("H00CG5", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV105ContratadaOrigem) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG5,0,0,true,false )
             ,new CursorDef("H00CG6", "SELECT [Modulo_Codigo], [Modulo_Sigla], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV38Sistema_Codigo ORDER BY [Modulo_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG6,0,0,true,false )
             ,new CursorDef("H00CG7", "SELECT [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome], [Sistema_Codigo], [FuncaoUsuario_Ativo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE ([FuncaoUsuario_Ativo] = 1) AND ([Sistema_Codigo] = @AV38Sistema_Codigo) ORDER BY [FuncaoUsuario_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG7,0,0,true,false )
             ,new CursorDef("H00CG8", "SELECT T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_OSAutomatica], T2.[Contratante_RequerOrigem], T1.[AreaTrabalho_Descricao], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaHost], T2.[Contratante_UsaOSistema] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG8,1,0,true,true )
             ,new CursorDef("H00CG9", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_Demanda] = @AV69ContagemResultado_DemandaFM) AND ([ContagemResultado_ContratadaCod] = @AV12RequisitadoCod) ORDER BY [ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG9,1,0,true,true )
             ,new CursorDef("H00CG10", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_FlagRequerSoft] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @AV16ServicoGrupo_Codigo ORDER BY [ServicoGrupo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG10,1,0,true,true )
             ,new CursorDef("H00CG11", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_CargoCod] AS Usuario_CargoCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T3.[Cargo_UOCod] AS Usuario_CargoUOCod, T4.[UnidadeOrganizacional_Nome] AS Usuario_CargoUONom FROM ((([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) LEFT JOIN [Geral_Cargo] T3 WITH (NOLOCK) ON T3.[Cargo_Codigo] = T1.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T4 WITH (NOLOCK) ON T4.[UnidadeOrganizacional_Codigo] = T3.[Cargo_UOCod]) WHERE T1.[Usuario_Codigo] = @AV6WWPContext__Userid ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG11,1,0,true,true )
             ,new CursorDef("H00CG13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG13,100,0,true,false )
             ,new CursorDef("H00CG15", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG15,100,0,true,false )
             ,new CursorDef("H00CG17", cmdBufferH00CG17,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG17,1,0,true,true )
             ,new CursorDef("H00CG18", "SELECT TOP 1 [Contrato_Codigo], [Contrato_PrepostoCod] AS Contrato_PrepostoCod, [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE ([Contratada_Codigo] = @AV105ContratadaOrigem) AND ([Contrato_PrepostoCod] > 0) ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG18,1,0,true,true )
             ,new CursorDef("H00CG21", cmdBufferH00CG21,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG21,1,0,true,true )
             ,new CursorDef("H00CG22", "SELECT [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosSistemas_ServicoCod] = @Servico_Codigo ORDER BY [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG22,100,0,true,false )
             ,new CursorDef("H00CG23", "SELECT [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazo_Momento], [ContratoServicosPrazo_DiasBaixa], [ContratoServicosPrazo_DiasMedia], [ContratoServicosPrazo_DiasAlta] FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG23,1,0,true,true )
             ,new CursorDef("H00CG24", "SELECT [ContratoServicosPrioridade_CntSrvCod], [ContratoServicosPrioridade_Nome], [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_CntSrvCod] = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrioridade_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG24,100,0,true,false )
             ,new CursorDef("H00CG25", "SELECT TOP 1 T1.[Servico_Codigo], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_LocalExec], T2.[Servico_Terceriza], T1.[ContratoServicos_PrazoTpDias], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_PrazoInicio] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV49ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG25,1,0,true,true )
             ,new CursorDef("H00CG26", "SELECT TOP 1 T1.[Contratada_Codigo], T1.[Contrato_Codigo], T2.[Contratada_AreaTrabalhoCod], T2.[Contratada_Ativo], T3.[Usuario_Ativo], T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) WHERE T1.[Contrato_Codigo] = @AV117Contrato ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG26,1,0,true,true )
             ,new CursorDef("H00CG27", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG27,100,0,true,false )
             ,new CursorDef("H00CG28", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG28,100,0,true,false )
             ,new CursorDef("H00CG31", "SELECT T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contrato_AreaTrabalhoCod], T1.[Servico_Codigo], T3.[Contratada_Ativo], T2.[Contrato_Ativo], T1.[ContratoServicos_Ativo], T2.[Contrato_DataVigenciaTermino], T2.[Contratada_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, COALESCE( T5.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataFim], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC5] ) T5 ON T5.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) AND (T2.[Contrato_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) AND (T1.[Servico_Codigo] = @AV13Servico_Codigo) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG31,100,0,true,false )
             ,new CursorDef("H00CG34", "SELECT T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Contratada_Ativo], T2.[Contrato_Ativo], T1.[ContratoServicos_Ativo], T2.[Contrato_DataVigenciaTermino], T1.[Servico_Codigo], T2.[Contratada_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, COALESCE( T5.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataFim], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC5] ) T5 ON T5.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @AV13Servico_Codigo) AND (T1.[ContratoServicos_Ativo] = 1) AND (T2.[Contrato_Ativo] = 1) AND (T3.[Contratada_Ativo] = 1) AND (T2.[Contratada_Codigo] = @AV6WWPCo_2Contratada_codigo) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG34,100,0,true,false )
             ,new CursorDef("H00CG35", "SELECT T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T2.[Contratada_Ativo], T3.[Usuario_DeFerias] FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (T3.[Usuario_Ativo] = 1) AND (Not T3.[Usuario_DeFerias] = 1) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV12RequisitadoCod) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG35,100,0,true,false )
             ,new CursorDef("H00CG36", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @ContratadaUsuario_UsuarioCod and [UsuarioServicos_ServicoCod] = @AV13Servico_Codigo ORDER BY [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG36,1,0,true,true )
             ,new CursorDef("H00CG37", "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPess, T1.[ContratanteUsuario_ContratanteCod], T2.[Usuario_DeFerias], T2.[Usuario_Ativo], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPess FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE (T2.[Usuario_Ativo] = 1) AND (Not T2.[Usuario_DeFerias] = 1) AND (T1.[ContratanteUsuario_ContratanteCod] = @AV12RequisitadoCod) ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG37,100,0,true,false )
             ,new CursorDef("H00CG38", "SELECT DISTINCT NULL AS [Servico_Codigo], NULL AS [Contrato_Codigo], NULL AS [Contrato_AreaTrabalhoCod], NULL AS [ContratoServicos_Ativo], NULL AS [Contrato_Ativo], [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM ( SELECT TOP(100) PERCENT T1.[Servico_Codigo], T1.[Contrato_Codigo], T4.[Contrato_AreaTrabalhoCod], T1.[ContratoServicos_Ativo], T4.[Contrato_Ativo], T2.[ServicoGrupo_Codigo], T3.[ServicoGrupo_Descricao] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T4.[Contrato_Ativo] = 1) AND (T1.[ContratoServicos_Ativo] = 1) AND (T4.[Contrato_AreaTrabalhoCod] = @AV100AreaTrabalho_Codigo) ORDER BY T3.[ServicoGrupo_Descricao]) DistinctT ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG38,100,0,true,false )
             ,new CursorDef("H00CG39", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG39,100,0,true,false )
             ,new CursorDef("H00CG40", "SELECT T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Ativo], T1.[Servico_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T1.[Contrato_Codigo] = @ContratoGestor_ContratoCod) AND (T1.[ContratoServicos_Ativo] = 1) AND (T2.[ServicoGrupo_Codigo] = @AV16ServicoGrupo_Codigo) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG40,100,0,true,false )
             ,new CursorDef("H00CG41", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG41,100,0,true,false )
             ,new CursorDef("H00CG42", "SELECT T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Ativo], T1.[Servico_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T1.[Contrato_Codigo] = @ContratoAuxiliar_ContratoCod) AND (T1.[ContratoServicos_Ativo] = 1) AND (T2.[ServicoGrupo_Codigo] = @AV16ServicoGrupo_Codigo) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG42,100,0,true,false )
             ,new CursorDef("H00CG43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG43,1,0,true,true )
             ,new CursorDef("H00CG44", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG44,100,0,true,false )
             ,new CursorDef("H00CG47", "SELECT T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[Contrato_Numero], T1.[Contrato_Codigo], T4.[Contratada_AreaTrabalhoCod], T4.[Contratada_Ativo], T2.[Usuario_Ativo], T1.[Contrato_Ativo], T1.[Contratada_Codigo], COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino] FROM ((([Contrato] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN (SELECT T5.[ContratoTermoAditivo_DataFim], T5.[Contrato_Codigo], T5.[ContratoTermoAditivo_Codigo], T6.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T5 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) WHERE T5.[ContratoTermoAditivo_Codigo] = T6.[GXC5] ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE (T1.[Contratada_Codigo] = @AV12RequisitadoCod) AND (T1.[Contrato_Ativo] = 1) AND (COALESCE( T3.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV8ContagemResultado_DataDmn or T1.[Contrato_DataVigenciaTermino] >= @AV8ContagemResultado_DataDmn) ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG47,100,0,true,false )
             ,new CursorDef("H00CG48", "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([Servico_Codigo] = @AV13Servico_Codigo) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG48,100,0,true,false )
             ,new CursorDef("H00CG50", cmdBufferH00CG50,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG50,1,0,true,true )
             ,new CursorDef("H00CG51", "SELECT T1.[ContratoServicos_Codigo], T1.[Artefatos_Codigo], T2.[Artefatos_Descricao] FROM ([ContratoServicosArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[Artefatos_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV49ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG51,100,0,true,false )
             ,new CursorDef("H00CG52", "SELECT [Contratada_AreaTrabalhoCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV100AreaTrabalho_Codigo ORDER BY [Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG52,100,0,true,false )
             ,new CursorDef("H00CG53", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV12RequisitadoCod ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG53,1,0,true,true )
             ,new CursorDef("H00CG54", "SELECT TOP 1 T1.[ContratoServicos_Codigo], T3.[ServicoGrupo_Descricao], T2.[ServicoGrupo_Codigo], T2.[Servico_Nome], T2.[Servico_Sigla], T1.[Servico_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV49ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG54,1,0,true,true )
             ,new CursorDef("H00CG55", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Descricao], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV23ContagemResultado_OSVinculada ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CG55,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((bool[]) buf[13])[0] = rslt.getBool(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((bool[]) buf[25])[0] = rslt.getBool(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(16) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(19) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((bool[]) buf[25])[0] = rslt.getBool(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((String[]) buf[29])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((decimal[]) buf[31])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((decimal[]) buf[33])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(20) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((decimal[]) buf[27])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(16) ;
                ((decimal[]) buf[29])[0] = rslt.getDecimal(17) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(18) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((bool[]) buf[9])[0] = rslt.getBool(9) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(9);
                ((int[]) buf[11])[0] = rslt.getInt(10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                ((String[]) buf[13])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((int[]) buf[15])[0] = rslt.getInt(12) ;
                ((String[]) buf[16])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(13);
                ((bool[]) buf[18])[0] = rslt.getBool(14) ;
                ((bool[]) buf[19])[0] = rslt.getBool(15) ;
                ((String[]) buf[20])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(16);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(17);
                ((short[]) buf[24])[0] = rslt.getShort(18) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(18);
                ((String[]) buf[26])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(19);
                ((decimal[]) buf[28])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[29])[0] = rslt.getDecimal(21) ;
                ((short[]) buf[30])[0] = rslt.getShort(22) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(22);
                ((String[]) buf[32])[0] = rslt.getString(23, 20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(23);
                ((DateTime[]) buf[34])[0] = rslt.getGXDate(24) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(24);
                ((DateTime[]) buf[36])[0] = rslt.getGXDate(25) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((bool[]) buf[8])[0] = rslt.getBool(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((String[]) buf[11])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(12);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((String[]) buf[10])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(11);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((bool[]) buf[11])[0] = rslt.getBool(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 25) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(10) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((decimal[]) buf[31])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((int[]) buf[33])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[35])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[36])[0] = rslt.getDecimal(21) ;
                ((decimal[]) buf[37])[0] = rslt.getDecimal(22) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 25) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[1]);
                }
                return;
             case 10 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 29 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 31 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 32 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
