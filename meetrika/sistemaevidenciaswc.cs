/*
               File: SistemaEvidenciasWC
        Description: Sistema Evidencias WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:9.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemaevidenciaswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemaevidenciaswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemaevidenciaswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_SistemaCod )
      {
         this.AV7FuncaoAPF_SistemaCod = aP0_FuncaoAPF_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7FuncaoAPF_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV22TFFuncaoAPFEvidencia_NomeArq = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFFuncaoAPFEvidencia_NomeArq", AV22TFFuncaoAPFEvidencia_NomeArq);
                  AV23TFFuncaoAPFEvidencia_NomeArq_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFFuncaoAPFEvidencia_NomeArq_Sel", AV23TFFuncaoAPFEvidencia_NomeArq_Sel);
                  AV7FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
                  AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
                  AV29Pgmname = GetNextPar( );
                  A386FuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel, AV7FuncaoAPF_SistemaCod, AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV29Pgmname, A386FuncaoAPF_PF, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAL62( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV29Pgmname = "SistemaEvidenciasWC";
               context.Gx_err = 0;
               edtavQuantidade_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
               edtavPontosdef_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPontosdef_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPontosdef_Enabled), 5, 0)));
               WSL62( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Evidencias WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020326912943");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemaevidenciaswc.aspx") + "?" + UrlEncode("" +AV7FuncaoAPF_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ", StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL", StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV25DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV25DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA", AV21FuncaoAPFEvidencia_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA", AV21FuncaoAPFEvidencia_NomeArqTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV29Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Caption", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cls", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfevidencia_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormL62( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemaevidenciaswc.js", "?2020326912978");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaEvidenciasWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Evidencias WC" ;
      }

      protected void WBL60( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemaevidenciaswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_L62( true) ;
         }
         else
         {
            wb_table1_2_L62( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_L62e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_SistemaCod_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPF_SistemaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaEvidenciasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_nomearq_Internalname, StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV22TFFuncaoAPFEvidencia_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaEvidenciasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfevidencia_nomearq_sel_Internalname, StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV23TFFuncaoAPFEvidencia_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfevidencia_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaEvidenciasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname, AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", 0, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaEvidenciasWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTL62( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Evidencias WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPL60( ) ;
            }
         }
      }

      protected void WSL62( )
      {
         STARTL62( ) ;
         EVTL62( ) ;
      }

      protected void EVTL62( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11L62 */
                                    E11L62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTffuncaoapfevidencia_nomearq_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL60( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL60( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              A409FuncaoAPFEvidencia_NomeArq = cgiGet( edtFuncaoAPFEvidencia_NomeArq_Internalname);
                              n409FuncaoAPFEvidencia_NomeArq = false;
                              AV15Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Quantidade), 3, 0)));
                              AV16PontosDeF = context.localUtil.CToN( cgiGet( edtavPontosdef_Internalname), ",", ".");
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPontosdef_Internalname, StringUtil.LTrim( StringUtil.Str( AV16PontosDeF, 14, 5)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTffuncaoapfevidencia_nomearq_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12L62 */
                                          E12L62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTffuncaoapfevidencia_nomearq_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13L62 */
                                          E13L62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTffuncaoapfevidencia_nomearq_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14L62 */
                                          E14L62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Tffuncaoapfevidencia_nomearq Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ"), AV22TFFuncaoAPFEvidencia_NomeArq) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfevidencia_nomearq_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL"), AV23TFFuncaoAPFEvidencia_NomeArq_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTffuncaoapfevidencia_nomearq_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPL60( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTffuncaoapfevidencia_nomearq_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEL62( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormL62( ) ;
            }
         }
      }

      protected void PAL62( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTffuncaoapfevidencia_nomearq_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV22TFFuncaoAPFEvidencia_NomeArq ,
                                       String AV23TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                       int AV7FuncaoAPF_SistemaCod ,
                                       String AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace ,
                                       String AV29Pgmname ,
                                       decimal A386FuncaoAPF_PF ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFL62( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ", StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFL62( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV29Pgmname = "SistemaEvidenciasWC";
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavPontosdef_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPontosdef_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPontosdef_Enabled), 5, 0)));
      }

      protected void RFL62( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E13L62 */
         E13L62 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV23TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                                 AV22TFFuncaoAPFEvidencia_NomeArq ,
                                                 A409FuncaoAPFEvidencia_NomeArq ,
                                                 AV7FuncaoAPF_SistemaCod ,
                                                 A360FuncaoAPF_SistemaCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            lV22TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFFuncaoAPFEvidencia_NomeArq", AV22TFFuncaoAPFEvidencia_NomeArq);
            /* Using cursor H00L62 */
            pr_default.execute(0, new Object[] {AV7FuncaoAPF_SistemaCod, lV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel});
            nGXsfl_5_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               BRKL62 = false;
               A360FuncaoAPF_SistemaCod = H00L62_A360FuncaoAPF_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
               n360FuncaoAPF_SistemaCod = H00L62_n360FuncaoAPF_SistemaCod[0];
               A409FuncaoAPFEvidencia_NomeArq = H00L62_A409FuncaoAPFEvidencia_NomeArq[0];
               n409FuncaoAPFEvidencia_NomeArq = H00L62_n409FuncaoAPFEvidencia_NomeArq[0];
               A165FuncaoAPF_Codigo = H00L62_A165FuncaoAPF_Codigo[0];
               A360FuncaoAPF_SistemaCod = H00L62_A360FuncaoAPF_SistemaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
               n360FuncaoAPF_SistemaCod = H00L62_n360FuncaoAPF_SistemaCod[0];
               GXt_decimal1 = A386FuncaoAPF_PF;
               new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A386FuncaoAPF_PF = GXt_decimal1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
               W360FuncaoAPF_SistemaCod = A360FuncaoAPF_SistemaCod;
               n360FuncaoAPF_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
               /* Execute user event: E14L62 */
               E14L62 ();
               A360FuncaoAPF_SistemaCod = W360FuncaoAPF_SistemaCod;
               n360FuncaoAPF_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
               if ( ! BRKL62 )
               {
                  BRKL62 = true;
                  pr_default.readNext(0);
               }
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            wbEnd = 5;
            WBL60( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV23TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                              AV22TFFuncaoAPFEvidencia_NomeArq ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              AV7FuncaoAPF_SistemaCod ,
                                              A360FuncaoAPF_SistemaCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV22TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFFuncaoAPFEvidencia_NomeArq", AV22TFFuncaoAPFEvidencia_NomeArq);
         /* Using cursor H00L63 */
         pr_default.execute(1, new Object[] {AV7FuncaoAPF_SistemaCod, lV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel});
         GRID_nRecordCount = H00L63_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel, AV7FuncaoAPF_SistemaCod, AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV29Pgmname, A386FuncaoAPF_PF, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel, AV7FuncaoAPF_SistemaCod, AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV29Pgmname, A386FuncaoAPF_PF, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel, AV7FuncaoAPF_SistemaCod, AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV29Pgmname, A386FuncaoAPF_PF, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel, AV7FuncaoAPF_SistemaCod, AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV29Pgmname, A386FuncaoAPF_PF, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV22TFFuncaoAPFEvidencia_NomeArq, AV23TFFuncaoAPFEvidencia_NomeArq_Sel, AV7FuncaoAPF_SistemaCod, AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, AV29Pgmname, A386FuncaoAPF_PF, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPL60( )
      {
         /* Before Start, stand alone formulas. */
         AV29Pgmname = "SistemaEvidenciasWC";
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavPontosdef_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPontosdef_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPontosdef_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12L62 */
         E12L62 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV25DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA"), AV21FuncaoAPFEvidencia_NomeArqTitleFilterData);
            /* Read variables values. */
            A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_SistemaCod_Internalname), ",", "."));
            n360FuncaoAPF_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
            AV22TFFuncaoAPFEvidencia_NomeArq = cgiGet( edtavTffuncaoapfevidencia_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFFuncaoAPFEvidencia_NomeArq", AV22TFFuncaoAPFEvidencia_NomeArq);
            AV23TFFuncaoAPFEvidencia_NomeArq_Sel = cgiGet( edtavTffuncaoapfevidencia_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFFuncaoAPFEvidencia_NomeArq_Sel", AV23TFFuncaoAPFEvidencia_NomeArq_Sel);
            AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOAV7FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoAPF_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_funcaoapfevidencia_nomearq_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Caption");
            Ddo_funcaoapfevidencia_nomearq_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Tooltip");
            Ddo_funcaoapfevidencia_nomearq_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cls");
            Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_set");
            Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_set");
            Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Dropdownoptionstype");
            Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_funcaoapfevidencia_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortasc"));
            Ddo_funcaoapfevidencia_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includesortdsc"));
            Ddo_funcaoapfevidencia_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includefilter"));
            Ddo_funcaoapfevidencia_nomearq_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filtertype");
            Ddo_funcaoapfevidencia_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filterisrange"));
            Ddo_funcaoapfevidencia_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Includedatalist"));
            Ddo_funcaoapfevidencia_nomearq_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalisttype");
            Ddo_funcaoapfevidencia_nomearq_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistproc");
            Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfevidencia_nomearq_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Loadingdata");
            Ddo_funcaoapfevidencia_nomearq_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Cleanfilter");
            Ddo_funcaoapfevidencia_nomearq_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Noresultsfound");
            Ddo_funcaoapfevidencia_nomearq_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Searchbuttontext");
            Ddo_funcaoapfevidencia_nomearq_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Activeeventkey");
            Ddo_funcaoapfevidencia_nomearq_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Filteredtext_get");
            Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ"), AV22TFFuncaoAPFEvidencia_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL"), AV23TFFuncaoAPFEvidencia_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12L62 */
         E12L62 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12L62( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTffuncaoapfevidencia_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_nomearq_Visible), 5, 0)));
         edtavTffuncaoapfevidencia_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfevidencia_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfevidencia_nomearq_sel_Visible), 5, 0)));
         Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFEvidencia_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace);
         AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace", AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace);
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         edtFuncaoAPF_SistemaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_SistemaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_SistemaCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            if (true) return;
         }
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV25DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV25DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
      }

      protected void E13L62( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV21FuncaoAPFEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            pr_default.close(0);
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPFEvidencia_NomeArq_Titleformat = 2;
         edtFuncaoAPFEvidencia_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFEvidencia_NomeArq_Internalname, "Title", edtFuncaoAPFEvidencia_NomeArq_Title);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV21FuncaoAPFEvidencia_NomeArqTitleFilterData", AV21FuncaoAPFEvidencia_NomeArqTitleFilterData);
      }

      protected void E11L62( )
      {
         /* Ddo_funcaoapfevidencia_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfevidencia_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV22TFFuncaoAPFEvidencia_NomeArq = Ddo_funcaoapfevidencia_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFFuncaoAPFEvidencia_NomeArq", AV22TFFuncaoAPFEvidencia_NomeArq);
            AV23TFFuncaoAPFEvidencia_NomeArq_Sel = Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFFuncaoAPFEvidencia_NomeArq_Sel", AV23TFFuncaoAPFEvidencia_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E14L62( )
      {
         /* Grid_Load Routine */
         AV15Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Quantidade), 3, 0)));
         AV16PontosDeF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPontosdef_Internalname, StringUtil.LTrim( StringUtil.Str( AV16PontosDeF, 14, 5)));
         GRID_nEOF = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         while ( ( (pr_default.getStatus(0) != 101) && ( H00L62_A360FuncaoAPF_SistemaCod[0] == A360FuncaoAPF_SistemaCod ) && ( StringUtil.StrCmp(H00L62_A409FuncaoAPFEvidencia_NomeArq[0], A409FuncaoAPFEvidencia_NomeArq) == 0 ) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
         {
            BRKL62 = false;
            A165FuncaoAPF_Codigo = H00L62_A165FuncaoAPF_Codigo[0];
            if ( A360FuncaoAPF_SistemaCod == AV7FuncaoAPF_SistemaCod )
            {
               GXt_decimal1 = A386FuncaoAPF_PF;
               new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A386FuncaoAPF_PF = GXt_decimal1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
               AV15Quantidade = (short)(AV15Quantidade+1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Quantidade), 3, 0)));
               AV16PontosDeF = (decimal)(AV16PontosDeF+A386FuncaoAPF_PF);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPontosdef_Internalname, StringUtil.LTrim( StringUtil.Str( AV16PontosDeF, 14, 5)));
            }
            BRKL62 = true;
            pr_default.readNext(0);
         }
         GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 5;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_52( ) ;
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV14Session.Get(AV29Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV29Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV14Session.Get(AV29Pgmname+"GridState"), "");
         }
         AV30GXV1 = 1;
         while ( AV30GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV30GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
            {
               AV22TFFuncaoAPFEvidencia_NomeArq = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFFuncaoAPFEvidencia_NomeArq", AV22TFFuncaoAPFEvidencia_NomeArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq)) )
               {
                  Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = AV22TFFuncaoAPFEvidencia_NomeArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "FilteredText_set", Ddo_funcaoapfevidencia_nomearq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV23TFFuncaoAPFEvidencia_NomeArq_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFFuncaoAPFEvidencia_NomeArq_Sel", AV23TFFuncaoAPFEvidencia_NomeArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel)) )
               {
                  Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = AV23TFFuncaoAPFEvidencia_NomeArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfevidencia_nomearq_Internalname, "SelectedValue_set", Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set);
               }
            }
            AV30GXV1 = (int)(AV30GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV14Session.Get(AV29Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_NOMEARQ";
            AV12GridStateFilterValue.gxTpr_Value = AV22TFFuncaoAPFEvidencia_NomeArq;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV23TFFuncaoAPFEvidencia_NomeArq_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7FuncaoAPF_SistemaCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAOAPF_SISTEMACOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV29Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV29Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Sistema";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_SistemaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV14Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_L62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFEvidencia_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFEvidencia_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFEvidencia_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Transa��es") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFEvidencia_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFEvidencia_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Quantidade), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQuantidade_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV16PontosDeF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPontosdef_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_L62e( true) ;
         }
         else
         {
            wb_table1_2_L62e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAL62( ) ;
         WSL62( ) ;
         WEL62( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7FuncaoAPF_SistemaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAL62( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemaevidenciaswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAL62( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
         }
         wcpOAV7FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7FuncaoAPF_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7FuncaoAPF_SistemaCod != wcpOAV7FuncaoAPF_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7FuncaoAPF_SistemaCod = AV7FuncaoAPF_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7FuncaoAPF_SistemaCod = cgiGet( sPrefix+"AV7FuncaoAPF_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7FuncaoAPF_SistemaCod) > 0 )
         {
            AV7FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7FuncaoAPF_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0)));
         }
         else
         {
            AV7FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7FuncaoAPF_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAL62( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSL62( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSL62( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoAPF_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7FuncaoAPF_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7FuncaoAPF_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV7FuncaoAPF_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEL62( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203269121097");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemaevidenciaswc.js", "?20203269121097");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtFuncaoAPFEvidencia_NomeArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ_"+sGXsfl_5_idx;
         edtavQuantidade_Internalname = sPrefix+"vQUANTIDADE_"+sGXsfl_5_idx;
         edtavPontosdef_Internalname = sPrefix+"vPONTOSDEF_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtFuncaoAPFEvidencia_NomeArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ_"+sGXsfl_5_fel_idx;
         edtavQuantidade_Internalname = sPrefix+"vQUANTIDADE_"+sGXsfl_5_fel_idx;
         edtavPontosdef_Internalname = sPrefix+"vPONTOSDEF_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBL60( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_5_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFEvidencia_NomeArq_Internalname,StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFEvidencia_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQuantidade_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Quantidade), 3, 0, ",", "")),((edtavQuantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15Quantidade), "ZZ9")) : context.localUtil.Format( (decimal)(AV15Quantidade), "ZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQuantidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQuantidade_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPontosdef_Internalname,StringUtil.LTrim( StringUtil.NToC( AV16PontosDeF, 14, 5, ",", "")),((edtavPontosdef_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV16PontosDeF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV16PontosDeF, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPontosdef_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPontosdef_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFEVIDENCIA_NOMEARQ"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( A409FuncaoAPFEvidencia_NomeArq, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtFuncaoAPFEvidencia_NomeArq_Internalname = sPrefix+"FUNCAOAPFEVIDENCIA_NOMEARQ";
         edtavQuantidade_Internalname = sPrefix+"vQUANTIDADE";
         edtavPontosdef_Internalname = sPrefix+"vPONTOSDEF";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD";
         edtavTffuncaoapfevidencia_nomearq_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_NOMEARQ";
         edtavTffuncaoapfevidencia_nomearq_sel_Internalname = sPrefix+"vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL";
         Ddo_funcaoapfevidencia_nomearq_Internalname = sPrefix+"DDO_FUNCAOAPFEVIDENCIA_NOMEARQ";
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavPontosdef_Jsonclick = "";
         edtavQuantidade_Jsonclick = "";
         edtFuncaoAPFEvidencia_NomeArq_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavPontosdef_Enabled = 0;
         edtavQuantidade_Enabled = 0;
         edtFuncaoAPFEvidencia_NomeArq_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtFuncaoAPFEvidencia_NomeArq_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick = "";
         edtavTffuncaoapfevidencia_nomearq_sel_Visible = 1;
         edtavTffuncaoapfevidencia_nomearq_Jsonclick = "";
         edtavTffuncaoapfevidencia_nomearq_Visible = 1;
         edtFuncaoAPF_SistemaCod_Jsonclick = "";
         edtFuncaoAPF_SistemaCod_Visible = 1;
         Ddo_funcaoapfevidencia_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfevidencia_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfevidencia_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfevidencia_nomearq_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfevidencia_nomearq_Datalistproc = "GetSistemaEvidenciasWCFilterData";
         Ddo_funcaoapfevidencia_nomearq_Datalisttype = "Dynamic";
         Ddo_funcaoapfevidencia_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_nomearq_Filtertype = "Character";
         Ddo_funcaoapfevidencia_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfevidencia_nomearq_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_nomearq_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfevidencia_nomearq_Cls = "ColumnSettings";
         Ddo_funcaoapfevidencia_nomearq_Tooltip = "Op��es";
         Ddo_funcaoapfevidencia_nomearq_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV23TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21FuncaoAPFEvidencia_NomeArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFEvidencia_NomeArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_NomeArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Title'}]}");
         setEventMetadata("DDO_FUNCAOAPFEVIDENCIA_NOMEARQ.ONOPTIONCLICKED","{handler:'E11L62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV22TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV23TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapfevidencia_nomearq_Activeeventkey',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfevidencia_nomearq_Filteredtext_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV22TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV23TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E14L62',iparms:[{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV15Quantidade',fld:'vQUANTIDADE',pic:'ZZ9',nv:0},{av:'AV16PontosDeF',fld:'vPONTOSDEF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV23TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21FuncaoAPFEvidencia_NomeArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFEvidencia_NomeArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_NomeArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Title'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV23TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21FuncaoAPFEvidencia_NomeArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFEvidencia_NomeArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_NomeArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Title'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV23TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21FuncaoAPFEvidencia_NomeArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFEvidencia_NomeArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_NomeArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Title'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22TFFuncaoAPFEvidencia_NomeArq',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV23TFFuncaoAPFEvidencia_NomeArq_Sel',fld:'vTFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV7FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21FuncaoAPFEvidencia_NomeArqTitleFilterData',fld:'vFUNCAOAPFEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFEvidencia_NomeArq_Titleformat',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtFuncaoAPFEvidencia_NomeArq_Title',ctrl:'FUNCAOAPFEVIDENCIA_NOMEARQ',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(0);
      }

      public override void initialize( )
      {
         Ddo_funcaoapfevidencia_nomearq_Activeeventkey = "";
         Ddo_funcaoapfevidencia_nomearq_Filteredtext_get = "";
         Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV22TFFuncaoAPFEvidencia_NomeArq = "";
         AV23TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace = "";
         AV29Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV25DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV21FuncaoAPFEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapfevidencia_nomearq_Filteredtext_set = "";
         Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV22TFFuncaoAPFEvidencia_NomeArq = "";
         H00L62_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         H00L62_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00L62_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00L62_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         H00L62_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         H00L62_A165FuncaoAPF_Codigo = new int[1] ;
         H00L63_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV14Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7FuncaoAPF_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemaevidenciaswc__default(),
            new Object[][] {
                new Object[] {
               H00L62_A406FuncaoAPFEvidencia_Codigo, H00L62_A360FuncaoAPF_SistemaCod, H00L62_n360FuncaoAPF_SistemaCod, H00L62_A409FuncaoAPFEvidencia_NomeArq, H00L62_n409FuncaoAPFEvidencia_NomeArq, H00L62_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00L63_AGRID_nRecordCount
               }
            }
         );
         AV29Pgmname = "SistemaEvidenciasWC";
         /* GeneXus formulas. */
         AV29Pgmname = "SistemaEvidenciasWC";
         context.Gx_err = 0;
         edtavQuantidade_Enabled = 0;
         edtavPontosdef_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short AV15Quantidade ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtFuncaoAPFEvidencia_NomeArq_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7FuncaoAPF_SistemaCod ;
      private int wcpOAV7FuncaoAPF_SistemaCod ;
      private int subGrid_Rows ;
      private int edtavQuantidade_Enabled ;
      private int edtavPontosdef_Enabled ;
      private int A165FuncaoAPF_Codigo ;
      private int Ddo_funcaoapfevidencia_nomearq_Datalistupdateminimumcharacters ;
      private int A360FuncaoAPF_SistemaCod ;
      private int edtFuncaoAPF_SistemaCod_Visible ;
      private int edtavTffuncaoapfevidencia_nomearq_Visible ;
      private int edtavTffuncaoapfevidencia_nomearq_sel_Visible ;
      private int edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int W360FuncaoAPF_SistemaCod ;
      private int AV30GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A386FuncaoAPF_PF ;
      private decimal AV16PontosDeF ;
      private decimal GXt_decimal1 ;
      private String Ddo_funcaoapfevidencia_nomearq_Activeeventkey ;
      private String Ddo_funcaoapfevidencia_nomearq_Filteredtext_get ;
      private String Ddo_funcaoapfevidencia_nomearq_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String AV22TFFuncaoAPFEvidencia_NomeArq ;
      private String AV23TFFuncaoAPFEvidencia_NomeArq_Sel ;
      private String AV29Pgmname ;
      private String GXKey ;
      private String edtavQuantidade_Internalname ;
      private String edtavPontosdef_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_funcaoapfevidencia_nomearq_Caption ;
      private String Ddo_funcaoapfevidencia_nomearq_Tooltip ;
      private String Ddo_funcaoapfevidencia_nomearq_Cls ;
      private String Ddo_funcaoapfevidencia_nomearq_Filteredtext_set ;
      private String Ddo_funcaoapfevidencia_nomearq_Selectedvalue_set ;
      private String Ddo_funcaoapfevidencia_nomearq_Dropdownoptionstype ;
      private String Ddo_funcaoapfevidencia_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfevidencia_nomearq_Filtertype ;
      private String Ddo_funcaoapfevidencia_nomearq_Datalisttype ;
      private String Ddo_funcaoapfevidencia_nomearq_Datalistproc ;
      private String Ddo_funcaoapfevidencia_nomearq_Loadingdata ;
      private String Ddo_funcaoapfevidencia_nomearq_Cleanfilter ;
      private String Ddo_funcaoapfevidencia_nomearq_Noresultsfound ;
      private String Ddo_funcaoapfevidencia_nomearq_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtFuncaoAPF_SistemaCod_Internalname ;
      private String edtFuncaoAPF_SistemaCod_Jsonclick ;
      private String TempTags ;
      private String edtavTffuncaoapfevidencia_nomearq_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_Jsonclick ;
      private String edtavTffuncaoapfevidencia_nomearq_sel_Internalname ;
      private String edtavTffuncaoapfevidencia_nomearq_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_funcaoapfevidencia_nomearqtitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String edtFuncaoAPFEvidencia_NomeArq_Internalname ;
      private String scmdbuf ;
      private String lV22TFFuncaoAPFEvidencia_NomeArq ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapfevidencia_nomearq_Internalname ;
      private String edtFuncaoAPFEvidencia_NomeArq_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7FuncaoAPF_SistemaCod ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoAPFEvidencia_NomeArq_Jsonclick ;
      private String edtavQuantidade_Jsonclick ;
      private String edtavPontosdef_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includesortasc ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includesortdsc ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includefilter ;
      private bool Ddo_funcaoapfevidencia_nomearq_Filterisrange ;
      private bool Ddo_funcaoapfevidencia_nomearq_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool BRKL62 ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV24ddo_FuncaoAPFEvidencia_NomeArqTitleControlIdToReplace ;
      private IGxSession AV14Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00L62_A406FuncaoAPFEvidencia_Codigo ;
      private int[] H00L62_A360FuncaoAPF_SistemaCod ;
      private bool[] H00L62_n360FuncaoAPF_SistemaCod ;
      private String[] H00L62_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] H00L62_n409FuncaoAPFEvidencia_NomeArq ;
      private int[] H00L62_A165FuncaoAPF_Codigo ;
      private long[] H00L63_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV21FuncaoAPFEvidencia_NomeArqTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV25DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
   }

   public class sistemaevidenciaswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00L62( IGxContext context ,
                                             String AV23TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV22TFFuncaoAPFEvidencia_NomeArq ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             int AV7FuncaoAPF_SistemaCod ,
                                             int A360FuncaoAPF_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [3] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPFEvidencia_Codigo], T2.[FuncaoAPF_SistemaCod], T1.[FuncaoAPFEvidencia_NomeArq], T1.[FuncaoAPF_Codigo] FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[FuncaoAPF_SistemaCod] = @AV7FuncaoAPF_SistemaCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] like @lV22TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] = @AV23TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[FuncaoAPF_SistemaCod], T1.[FuncaoAPFEvidencia_NomeArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00L63( IGxContext context ,
                                             String AV23TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV22TFFuncaoAPFEvidencia_NomeArq ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             int AV7FuncaoAPF_SistemaCod ,
                                             int A360FuncaoAPF_SistemaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [3] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([FuncaoAPFEvidencia] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[FuncaoAPF_SistemaCod] = @AV7FuncaoAPF_SistemaCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] like @lV22TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPFEvidencia_NomeArq] = @AV23TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00L62(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
               case 1 :
                     return conditional_H00L63(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00L62 ;
          prmH00L62 = new Object[] {
          new Object[] {"@AV7FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV23TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmH00L63 ;
          prmH00L63 = new Object[] {
          new Object[] {"@AV7FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV23TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00L62", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L62,11,0,true,false )
             ,new CursorDef("H00L63", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L63,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

}
