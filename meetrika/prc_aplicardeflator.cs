/*
               File: PRC_AplicarDeflator
        Description: Aplicar Deflator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:28.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_aplicardeflator : GXProcedure
   {
      public prc_aplicardeflator( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_aplicardeflator( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           int aP1_Servico_Codigo ,
                           decimal aP2_Servico_Percentual ,
                           DateTime aP3_DataIni ,
                           DateTime aP4_DataFim ,
                           String aP5_ContagemResultado_StatusDmn ,
                           ref int aP6_ContadorFMCod )
      {
         this.AV8Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV9Servico_Codigo = aP1_Servico_Codigo;
         this.AV10Servico_Percentual = aP2_Servico_Percentual;
         this.AV11DataIni = aP3_DataIni;
         this.AV12DataFim = aP4_DataFim;
         this.AV13ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         this.AV15ContadorFMCod = aP6_ContadorFMCod;
         initialize();
         executePrivate();
         aP6_ContadorFMCod=this.AV15ContadorFMCod;
      }

      public int executeUdp( int aP0_Contratada_Codigo ,
                             int aP1_Servico_Codigo ,
                             decimal aP2_Servico_Percentual ,
                             DateTime aP3_DataIni ,
                             DateTime aP4_DataFim ,
                             String aP5_ContagemResultado_StatusDmn )
      {
         this.AV8Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV9Servico_Codigo = aP1_Servico_Codigo;
         this.AV10Servico_Percentual = aP2_Servico_Percentual;
         this.AV11DataIni = aP3_DataIni;
         this.AV12DataFim = aP4_DataFim;
         this.AV13ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         this.AV15ContadorFMCod = aP6_ContadorFMCod;
         initialize();
         executePrivate();
         aP6_ContadorFMCod=this.AV15ContadorFMCod;
         return AV15ContadorFMCod ;
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 int aP1_Servico_Codigo ,
                                 decimal aP2_Servico_Percentual ,
                                 DateTime aP3_DataIni ,
                                 DateTime aP4_DataFim ,
                                 String aP5_ContagemResultado_StatusDmn ,
                                 ref int aP6_ContadorFMCod )
      {
         prc_aplicardeflator objprc_aplicardeflator;
         objprc_aplicardeflator = new prc_aplicardeflator();
         objprc_aplicardeflator.AV8Contratada_Codigo = aP0_Contratada_Codigo;
         objprc_aplicardeflator.AV9Servico_Codigo = aP1_Servico_Codigo;
         objprc_aplicardeflator.AV10Servico_Percentual = aP2_Servico_Percentual;
         objprc_aplicardeflator.AV11DataIni = aP3_DataIni;
         objprc_aplicardeflator.AV12DataFim = aP4_DataFim;
         objprc_aplicardeflator.AV13ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         objprc_aplicardeflator.AV15ContadorFMCod = aP6_ContadorFMCod;
         objprc_aplicardeflator.context.SetSubmitInitialConfig(context);
         objprc_aplicardeflator.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_aplicardeflator);
         aP6_ContadorFMCod=this.AV15ContadorFMCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_aplicardeflator)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P005M3 */
         pr_default.execute(0, new Object[] {AV8Contratada_Codigo, AV11DataIni, AV12DataFim, AV15ContadorFMCod, AV9Servico_Codigo, AV13ContagemResultado_StatusDmn});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P005M3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P005M3_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P005M3_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P005M3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P005M3_n484ContagemResultado_StatusDmn[0];
            A601ContagemResultado_Servico = P005M3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P005M3_n601ContagemResultado_Servico[0];
            A805ContagemResultado_ContratadaOrigemCod = P005M3_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P005M3_n805ContagemResultado_ContratadaOrigemCod[0];
            A584ContagemResultado_ContadorFM = P005M3_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P005M3_A566ContagemResultado_DataUltCnt[0];
            A601ContagemResultado_Servico = P005M3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P005M3_n601ContagemResultado_Servico[0];
            A584ContagemResultado_ContadorFM = P005M3_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P005M3_A566ContagemResultado_DataUltCnt[0];
            /* Using cursor P005M4 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A459ContagemResultado_PFLFS = P005M4_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P005M4_n459ContagemResultado_PFLFS[0];
               A460ContagemResultado_PFBFM = P005M4_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P005M4_n460ContagemResultado_PFBFM[0];
               A461ContagemResultado_PFLFM = P005M4_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = P005M4_n461ContagemResultado_PFLFM[0];
               A800ContagemResultado_Deflator = P005M4_A800ContagemResultado_Deflator[0];
               n800ContagemResultado_Deflator = P005M4_n800ContagemResultado_Deflator[0];
               A511ContagemResultado_HoraCnt = P005M4_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = P005M4_A473ContagemResultado_DataCnt[0];
               if ( P005M4_n459ContagemResultado_PFLFS[0] || (Convert.ToDecimal(0)==A459ContagemResultado_PFLFS) )
               {
                  A461ContagemResultado_PFLFM = (decimal)(A460ContagemResultado_PFBFM*AV10Servico_Percentual);
                  n461ContagemResultado_PFLFM = false;
               }
               if ( A461ContagemResultado_PFLFM < 0.001m )
               {
                  A461ContagemResultado_PFLFM = 0.001m;
                  n461ContagemResultado_PFLFM = false;
               }
               A800ContagemResultado_Deflator = AV10Servico_Percentual;
               n800ContagemResultado_Deflator = false;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P005M5 */
               pr_default.execute(2, new Object[] {n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if (true) break;
               /* Using cursor P005M6 */
               pr_default.execute(3, new Object[] {n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AplicarDeflator");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P005M3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P005M3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P005M3_A456ContagemResultado_Codigo = new int[1] ;
         P005M3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P005M3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P005M3_A601ContagemResultado_Servico = new int[1] ;
         P005M3_n601ContagemResultado_Servico = new bool[] {false} ;
         P005M3_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P005M3_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P005M3_A584ContagemResultado_ContadorFM = new int[1] ;
         P005M3_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         A484ContagemResultado_StatusDmn = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P005M4_A456ContagemResultado_Codigo = new int[1] ;
         P005M4_A459ContagemResultado_PFLFS = new decimal[1] ;
         P005M4_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P005M4_A460ContagemResultado_PFBFM = new decimal[1] ;
         P005M4_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P005M4_A461ContagemResultado_PFLFM = new decimal[1] ;
         P005M4_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P005M4_A800ContagemResultado_Deflator = new decimal[1] ;
         P005M4_n800ContagemResultado_Deflator = new bool[] {false} ;
         P005M4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P005M4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_aplicardeflator__default(),
            new Object[][] {
                new Object[] {
               P005M3_A1553ContagemResultado_CntSrvCod, P005M3_n1553ContagemResultado_CntSrvCod, P005M3_A456ContagemResultado_Codigo, P005M3_A484ContagemResultado_StatusDmn, P005M3_n484ContagemResultado_StatusDmn, P005M3_A601ContagemResultado_Servico, P005M3_n601ContagemResultado_Servico, P005M3_A805ContagemResultado_ContratadaOrigemCod, P005M3_n805ContagemResultado_ContratadaOrigemCod, P005M3_A584ContagemResultado_ContadorFM,
               P005M3_A566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P005M4_A456ContagemResultado_Codigo, P005M4_A459ContagemResultado_PFLFS, P005M4_n459ContagemResultado_PFLFS, P005M4_A460ContagemResultado_PFBFM, P005M4_n460ContagemResultado_PFBFM, P005M4_A461ContagemResultado_PFLFM, P005M4_n461ContagemResultado_PFLFM, P005M4_A800ContagemResultado_Deflator, P005M4_n800ContagemResultado_Deflator, P005M4_A511ContagemResultado_HoraCnt,
               P005M4_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contratada_Codigo ;
      private int AV9Servico_Codigo ;
      private int AV15ContadorFMCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A584ContagemResultado_ContadorFM ;
      private decimal AV10Servico_Percentual ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A800ContagemResultado_Deflator ;
      private String AV13ContagemResultado_StatusDmn ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime AV11DataIni ;
      private DateTime AV12DataFim ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n601ContagemResultado_Servico ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n800ContagemResultado_Deflator ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP6_ContadorFMCod ;
      private IDataStoreProvider pr_default ;
      private int[] P005M3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P005M3_n1553ContagemResultado_CntSrvCod ;
      private int[] P005M3_A456ContagemResultado_Codigo ;
      private String[] P005M3_A484ContagemResultado_StatusDmn ;
      private bool[] P005M3_n484ContagemResultado_StatusDmn ;
      private int[] P005M3_A601ContagemResultado_Servico ;
      private bool[] P005M3_n601ContagemResultado_Servico ;
      private int[] P005M3_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P005M3_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P005M3_A584ContagemResultado_ContadorFM ;
      private DateTime[] P005M3_A566ContagemResultado_DataUltCnt ;
      private int[] P005M4_A456ContagemResultado_Codigo ;
      private decimal[] P005M4_A459ContagemResultado_PFLFS ;
      private bool[] P005M4_n459ContagemResultado_PFLFS ;
      private decimal[] P005M4_A460ContagemResultado_PFBFM ;
      private bool[] P005M4_n460ContagemResultado_PFBFM ;
      private decimal[] P005M4_A461ContagemResultado_PFLFM ;
      private bool[] P005M4_n461ContagemResultado_PFLFM ;
      private decimal[] P005M4_A800ContagemResultado_Deflator ;
      private bool[] P005M4_n800ContagemResultado_Deflator ;
      private String[] P005M4_A511ContagemResultado_HoraCnt ;
      private DateTime[] P005M4_A473ContagemResultado_DataCnt ;
   }

   public class prc_aplicardeflator__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005M3 ;
          prmP005M3 = new Object[] {
          new Object[] {"@AV8Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV12DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13ContagemResultado_StatusDmn",SqlDbType.Char,1,0}
          } ;
          Object[] prmP005M4 ;
          prmP005M4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005M5 ;
          prmP005M5 = new Object[] {
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP005M6 ;
          prmP005M6 = new Object[] {
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005M3", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaOrigemCod], COALESCE( T3.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo], MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_ContratadaOrigemCod] = @AV8Contratada_Codigo) AND (COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV11DataIni) AND (COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV12DataFim) AND ((@AV15ContadorFMCod = convert(int, 0)) or ( COALESCE( T3.[ContagemResultado_ContadorFM], 0) = @AV15ContadorFMCod)) AND (T2.[Servico_Codigo] = @AV9Servico_Codigo) AND (T1.[ContagemResultado_StatusDmn] = @AV13ContagemResultado_StatusDmn) ORDER BY T1.[ContagemResultado_ContratadaOrigemCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005M3,100,0,true,false )
             ,new CursorDef("P005M4", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Deflator], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP005M4,1,0,true,true )
             ,new CursorDef("P005M5", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_Deflator]=@ContagemResultado_Deflator  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005M5)
             ,new CursorDef("P005M6", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFLFM]=@ContagemResultado_PFLFM, [ContagemResultado_Deflator]=@ContagemResultado_Deflator  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005M6)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 5) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (DateTime)parms[5]);
                stmt.SetParameter(5, (String)parms[6]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (DateTime)parms[5]);
                stmt.SetParameter(5, (String)parms[6]);
                return;
       }
    }

 }

}
