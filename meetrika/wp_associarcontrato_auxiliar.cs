/*
               File: WP_AssociarContrato_Auxiliar
        Description: Associar Contrato com Auxiliares
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:6:18.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarcontrato_auxiliar : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarcontrato_auxiliar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarcontrato_auxiliar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoAuxiliar_ContratoCod )
      {
         this.AV11ContratoAuxiliar_ContratoCod = aP0_ContratoAuxiliar_ContratoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV11ContratoAuxiliar_ContratoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoAuxiliar_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoAuxiliar_ContratoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOAUXILIAR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoAuxiliar_ContratoCod), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PANN2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSNN2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WENN2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar Contrato com Auxiliares") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311961835");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarcontrato_auxiliar.aspx") + "?" + UrlEncode("" +AV11ContratoAuxiliar_ContratoCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDKEYLIST", AV7AddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDKEYLIST", AV7AddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDDSCLIST", AV5AddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDDSCLIST", AV5AddedDscList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDKEYLIST", AV22NotAddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDKEYLIST", AV22NotAddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDDSCLIST", AV20NotAddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDDSCLIST", AV20NotAddedDscList);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1824ContratoAuxiliar_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOAUXILIAR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContratoAuxiliar_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOAUXILIAR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1825ContratoAuxiliar_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOAUXILIAR", AV29ContratoAuxiliar);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOAUXILIAR", AV29ContratoAuxiliar);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOAUXILIAR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOAUXILIAR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoAuxiliar_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOAUXILIAR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoAuxiliar_ContratoCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormNN2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarContrato_Auxiliar" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar Contrato com Auxiliares" ;
      }

      protected void WBNN0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_NN2( true) ;
         }
         else
         {
            wb_table1_2_NN2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_NN2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV8AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Auxiliar.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV23NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Auxiliar.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV6AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Auxiliar.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV21NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarContrato_Auxiliar.htm");
         }
         wbLoad = true;
      }

      protected void STARTNN2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar Contrato com Auxiliares", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPNN0( ) ;
      }

      protected void WSNN2( )
      {
         STARTNN2( ) ;
         EVTNN2( ) ;
      }

      protected void EVTNN2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11NN2 */
                           E11NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12NN2 */
                           E12NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13NN2 */
                                 E13NN2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14NN2 */
                           E14NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15NN2 */
                           E15NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16NN2 */
                           E16NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17NN2 */
                           E17NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18NN2 */
                           E18NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19NN2 */
                           E19NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20NN2 */
                           E20NN2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18NN2 */
                           E18NN2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19NN2 */
                           E19NN2 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WENN2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormNN2( ) ;
            }
         }
      }

      protected void PANN2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV9AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV9AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFNN2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFNN2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12NN2 */
         E12NN2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00NN2 */
            pr_default.execute(0, new Object[] {AV11ContratoAuxiliar_ContratoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = H00NN2_A74Contrato_Codigo[0];
               A77Contrato_Numero = H00NN2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
               /* Execute user event: E20NN2 */
               E20NN2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBNN0( ) ;
         }
      }

      protected void STRUPNN0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11NN2 */
         E11NN2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV9AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
            AV8AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV23NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedKeyListXml", AV23NotAddedKeyListXml);
            AV6AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
            AV21NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NotAddedDscListXml", AV21NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11NN2 */
         E11NN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11NN2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV28WWPContext) ;
         if ( StringUtil.StrCmp(AV15HTTPRequest.Method, "GET") == 0 )
         {
            AV33GXLvl6 = 0;
            /* Using cursor H00NN3 */
            pr_default.execute(1, new Object[] {AV11ContratoAuxiliar_ContratoCod});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = H00NN3_A74Contrato_Codigo[0];
               A92Contrato_Ativo = H00NN3_A92Contrato_Ativo[0];
               AV33GXLvl6 = 1;
               lblTbinativo_Visible = (!A92Contrato_Ativo ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinativo_Visible), 5, 0)));
               bttBtn_confirm_Visible = (A92Contrato_Ativo&&(AV28WWPContext.gxTpr_Insert||AV28WWPContext.gxTpr_Update) ? 1 : 0);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( AV33GXLvl6 == 0 )
            {
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            /* Using cursor H00NN4 */
            pr_default.execute(2, new Object[] {AV11ContratoAuxiliar_ContratoCod});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00NN4_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00NN4_A1079ContratoGestor_UsuarioCod[0];
               AV30Gestores.Add(A1079ContratoGestor_UsuarioCod, 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( AV28WWPContext.gxTpr_Userehadministradorgam || AV28WWPContext.gxTpr_Userehcontratante )
            {
               /* Using cursor H00NN5 */
               pr_default.execute(3, new Object[] {AV28WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A5AreaTrabalho_Codigo = H00NN5_A5AreaTrabalho_Codigo[0];
                  A29Contratante_Codigo = H00NN5_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = H00NN5_n29Contratante_Codigo[0];
                  AV10Contratante_Codigo = A29Contratante_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contratante_Codigo), 6, 0)));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               pr_default.dynParam(4, new Object[]{ new Object[]{
                                                    A60ContratanteUsuario_UsuarioCod ,
                                                    AV30Gestores ,
                                                    A63ContratanteUsuario_ContratanteCod ,
                                                    AV10Contratante_Codigo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                    }
               });
               /* Using cursor H00NN6 */
               pr_default.execute(4, new Object[] {AV10Contratante_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A61ContratanteUsuario_UsuarioPessoaCod = H00NN6_A61ContratanteUsuario_UsuarioPessoaCod[0];
                  n61ContratanteUsuario_UsuarioPessoaCod = H00NN6_n61ContratanteUsuario_UsuarioPessoaCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00NN6_A63ContratanteUsuario_ContratanteCod[0];
                  A60ContratanteUsuario_UsuarioCod = H00NN6_A60ContratanteUsuario_UsuarioCod[0];
                  A62ContratanteUsuario_UsuarioPessoaNom = H00NN6_A62ContratanteUsuario_UsuarioPessoaNom[0];
                  n62ContratanteUsuario_UsuarioPessoaNom = H00NN6_n62ContratanteUsuario_UsuarioPessoaNom[0];
                  A61ContratanteUsuario_UsuarioPessoaCod = H00NN6_A61ContratanteUsuario_UsuarioPessoaCod[0];
                  n61ContratanteUsuario_UsuarioPessoaCod = H00NN6_n61ContratanteUsuario_UsuarioPessoaCod[0];
                  A62ContratanteUsuario_UsuarioPessoaNom = H00NN6_A62ContratanteUsuario_UsuarioPessoaNom[0];
                  n62ContratanteUsuario_UsuarioPessoaNom = H00NN6_n62ContratanteUsuario_UsuarioPessoaNom[0];
                  AV12ContratoAuxiliar_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
                  AV14Exist = false;
                  /* Using cursor H00NN7 */
                  pr_default.execute(5, new Object[] {AV11ContratoAuxiliar_ContratoCod, AV12ContratoAuxiliar_UsuarioCod});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A1825ContratoAuxiliar_UsuarioCod = H00NN7_A1825ContratoAuxiliar_UsuarioCod[0];
                     A1824ContratoAuxiliar_ContratoCod = H00NN7_A1824ContratoAuxiliar_ContratoCod[0];
                     AV14Exist = true;
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  pr_default.close(5);
                  AV13Description = A62ContratanteUsuario_UsuarioPessoaNom;
                  if ( AV14Exist )
                  {
                     AV7AddedKeyList.Add(A60ContratanteUsuario_UsuarioCod, 0);
                     AV5AddedDscList.Add(AV13Description, 0);
                  }
                  else
                  {
                     AV22NotAddedKeyList.Add(A60ContratanteUsuario_UsuarioCod, 0);
                     AV20NotAddedDscList.Add(AV13Description, 0);
                  }
                  pr_default.readNext(4);
               }
               pr_default.close(4);
            }
            if ( AV28WWPContext.gxTpr_Userehadministradorgam || AV28WWPContext.gxTpr_Userehcontratada )
            {
               /* Using cursor H00NN8 */
               pr_default.execute(6, new Object[] {AV11ContratoAuxiliar_ContratoCod});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A74Contrato_Codigo = H00NN8_A74Contrato_Codigo[0];
                  A39Contratada_Codigo = H00NN8_A39Contratada_Codigo[0];
                  pr_default.dynParam(7, new Object[]{ new Object[]{
                                                       A69ContratadaUsuario_UsuarioCod ,
                                                       AV30Gestores ,
                                                       AV7AddedKeyList ,
                                                       AV22NotAddedKeyList ,
                                                       A66ContratadaUsuario_ContratadaCod ,
                                                       A39Contratada_Codigo },
                                                       new int[] {
                                                       TypeConstants.INT, TypeConstants.INT, TypeConstants.INT
                                                       }
                  });
                  /* Using cursor H00NN9 */
                  pr_default.execute(7, new Object[] {A39Contratada_Codigo});
                  while ( (pr_default.getStatus(7) != 101) )
                  {
                     A70ContratadaUsuario_UsuarioPessoaCod = H00NN9_A70ContratadaUsuario_UsuarioPessoaCod[0];
                     n70ContratadaUsuario_UsuarioPessoaCod = H00NN9_n70ContratadaUsuario_UsuarioPessoaCod[0];
                     A66ContratadaUsuario_ContratadaCod = H00NN9_A66ContratadaUsuario_ContratadaCod[0];
                     A69ContratadaUsuario_UsuarioCod = H00NN9_A69ContratadaUsuario_UsuarioCod[0];
                     A71ContratadaUsuario_UsuarioPessoaNom = H00NN9_A71ContratadaUsuario_UsuarioPessoaNom[0];
                     n71ContratadaUsuario_UsuarioPessoaNom = H00NN9_n71ContratadaUsuario_UsuarioPessoaNom[0];
                     A70ContratadaUsuario_UsuarioPessoaCod = H00NN9_A70ContratadaUsuario_UsuarioPessoaCod[0];
                     n70ContratadaUsuario_UsuarioPessoaCod = H00NN9_n70ContratadaUsuario_UsuarioPessoaCod[0];
                     A71ContratadaUsuario_UsuarioPessoaNom = H00NN9_A71ContratadaUsuario_UsuarioPessoaNom[0];
                     n71ContratadaUsuario_UsuarioPessoaNom = H00NN9_n71ContratadaUsuario_UsuarioPessoaNom[0];
                     AV12ContratoAuxiliar_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
                     AV14Exist = false;
                     /* Using cursor H00NN10 */
                     pr_default.execute(8, new Object[] {AV11ContratoAuxiliar_ContratoCod, AV12ContratoAuxiliar_UsuarioCod});
                     while ( (pr_default.getStatus(8) != 101) )
                     {
                        A1825ContratoAuxiliar_UsuarioCod = H00NN10_A1825ContratoAuxiliar_UsuarioCod[0];
                        A1824ContratoAuxiliar_ContratoCod = H00NN10_A1824ContratoAuxiliar_ContratoCod[0];
                        AV14Exist = true;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(8);
                     AV13Description = A71ContratadaUsuario_UsuarioPessoaNom;
                     if ( AV14Exist )
                     {
                        AV7AddedKeyList.Add(A69ContratadaUsuario_UsuarioCod, 0);
                        AV5AddedDscList.Add(AV13Description, 0);
                     }
                     else
                     {
                        AV22NotAddedKeyList.Add(A69ContratadaUsuario_UsuarioCod, 0);
                        AV20NotAddedDscList.Add(AV13Description, 0);
                     }
                     pr_default.readNext(7);
                  }
                  pr_default.close(7);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(6);
            }
            /* Execute user subroutine: 'ORDENARCOLLECTIONS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'SAVELISTS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
      }

      protected void E12NN2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV28WWPContext) ;
         imgImageassociateselected_Visible = (AV28WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV28WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV28WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV28WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV28WWPContext", AV28WWPContext);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
      }

      public void GXEnter( )
      {
         /* Execute user event: E13NN2 */
         E13NN2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13NN2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV16i = 1;
         AV27Success = true;
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV7AddedKeyList.Count )
         {
            AV12ContratoAuxiliar_UsuarioCod = (int)(AV7AddedKeyList.GetNumeric(AV41GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
            if ( AV27Success )
            {
               AV14Exist = false;
               /* Using cursor H00NN11 */
               pr_default.execute(9, new Object[] {AV11ContratoAuxiliar_ContratoCod, AV12ContratoAuxiliar_UsuarioCod});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A1825ContratoAuxiliar_UsuarioCod = H00NN11_A1825ContratoAuxiliar_UsuarioCod[0];
                  A1824ContratoAuxiliar_ContratoCod = H00NN11_A1824ContratoAuxiliar_ContratoCod[0];
                  AV14Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(9);
               if ( ! AV14Exist )
               {
                  AV29ContratoAuxiliar = new SdtContratoAuxiliar(context);
                  AV29ContratoAuxiliar.gxTpr_Contratoauxiliar_contratocod = AV11ContratoAuxiliar_ContratoCod;
                  AV29ContratoAuxiliar.gxTpr_Contratoauxiliar_usuariocod = AV12ContratoAuxiliar_UsuarioCod;
                  AV29ContratoAuxiliar.Save();
                  if ( ! AV29ContratoAuxiliar.Success() )
                  {
                     AV27Success = false;
                  }
               }
            }
            AV16i = (int)(AV16i+1);
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         AV16i = 1;
         AV43GXV2 = 1;
         while ( AV43GXV2 <= AV22NotAddedKeyList.Count )
         {
            AV12ContratoAuxiliar_UsuarioCod = (int)(AV22NotAddedKeyList.GetNumeric(AV43GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
            if ( AV27Success )
            {
               AV14Exist = false;
               /* Using cursor H00NN12 */
               pr_default.execute(10, new Object[] {AV11ContratoAuxiliar_ContratoCod, AV12ContratoAuxiliar_UsuarioCod});
               while ( (pr_default.getStatus(10) != 101) )
               {
                  A1825ContratoAuxiliar_UsuarioCod = H00NN12_A1825ContratoAuxiliar_UsuarioCod[0];
                  A1824ContratoAuxiliar_ContratoCod = H00NN12_A1824ContratoAuxiliar_ContratoCod[0];
                  AV14Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(10);
               if ( AV14Exist )
               {
                  AV29ContratoAuxiliar = new SdtContratoAuxiliar(context);
                  AV29ContratoAuxiliar.Load(AV11ContratoAuxiliar_ContratoCod, AV12ContratoAuxiliar_UsuarioCod);
                  if ( AV29ContratoAuxiliar.Success() )
                  {
                     AV29ContratoAuxiliar.Delete();
                  }
                  if ( ! AV29ContratoAuxiliar.Success() )
                  {
                     AV27Success = false;
                  }
               }
            }
            AV16i = (int)(AV16i+1);
            AV43GXV2 = (int)(AV43GXV2+1);
         }
         if ( AV27Success )
         {
            context.CommitDataStores( "WP_AssociarContrato_Auxiliar");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29ContratoAuxiliar", AV29ContratoAuxiliar);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
      }

      protected void E14NN2( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E15NN2( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E16NN2( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E17NN2( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22NotAddedKeyList = (IGxCollection)(AV7AddedKeyList.Clone());
         AV20NotAddedDscList = (IGxCollection)(AV5AddedDscList.Clone());
         AV5AddedDscList.Clear();
         AV7AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E18NN2( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E19NN2( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void S132( )
      {
         /* 'UPDATEASSOCIATIONVARIABLES' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV16i = 1;
         AV45GXV3 = 1;
         while ( AV45GXV3 <= AV7AddedKeyList.Count )
         {
            AV12ContratoAuxiliar_UsuarioCod = (int)(AV7AddedKeyList.GetNumeric(AV45GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
            AV13Description = ((String)AV5AddedDscList.Item(AV16i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV16i = (int)(AV16i+1);
            AV45GXV3 = (int)(AV45GXV3+1);
         }
         AV16i = 1;
         AV46GXV4 = 1;
         while ( AV46GXV4 <= AV22NotAddedKeyList.Count )
         {
            AV12ContratoAuxiliar_UsuarioCod = (int)(AV22NotAddedKeyList.GetNumeric(AV46GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
            AV13Description = ((String)AV20NotAddedDscList.Item(AV16i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV16i = (int)(AV16i+1);
            AV46GXV4 = (int)(AV46GXV4+1);
         }
      }

      protected void S152( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV48GXV6 = 1;
         AV47GXV5 = AV29ContratoAuxiliar.GetMessages();
         while ( AV48GXV6 <= AV47GXV5.Count )
         {
            AV19Message = ((SdtMessages_Message)AV47GXV5.Item(AV48GXV6));
            if ( AV19Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV19Message.gxTpr_Description);
            }
            AV48GXV6 = (int)(AV48GXV6+1);
         }
      }

      protected void S142( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8AddedKeyListXml)) )
         {
            AV5AddedDscList.FromXml(AV6AddedDscListXml, "Collection");
            AV7AddedKeyList.FromXml(AV8AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23NotAddedKeyListXml)) )
         {
            AV22NotAddedKeyList.FromXml(AV23NotAddedKeyListXml, "Collection");
            AV20NotAddedDscList.FromXml(AV21NotAddedDscListXml, "Collection");
         }
      }

      protected void S122( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV7AddedKeyList.Count > 0 )
         {
            AV8AddedKeyListXml = AV7AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = AV5AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         else
         {
            AV8AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         if ( AV22NotAddedKeyList.Count > 0 )
         {
            AV23NotAddedKeyListXml = AV22NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedKeyListXml", AV23NotAddedKeyListXml);
            AV21NotAddedDscListXml = AV20NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NotAddedDscListXml", AV21NotAddedDscListXml);
         }
         else
         {
            AV23NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedKeyListXml", AV23NotAddedKeyListXml);
            AV21NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NotAddedDscListXml", AV21NotAddedDscListXml);
         }
      }

      protected void S182( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV16i = 1;
         AV17InsertIndex = 1;
         AV49GXV7 = 1;
         while ( AV49GXV7 <= AV22NotAddedKeyList.Count )
         {
            AV12ContratoAuxiliar_UsuarioCod = (int)(AV22NotAddedKeyList.GetNumeric(AV49GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
            AV13Description = ((String)AV20NotAddedDscList.Item(AV16i));
            while ( ( AV17InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV17InsertIndex)), AV13Description) < 0 ) )
            {
               AV17InsertIndex = (int)(AV17InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV12ContratoAuxiliar_UsuarioCod, AV17InsertIndex);
            AV5AddedDscList.Add(AV13Description, AV17InsertIndex);
            AV16i = (int)(AV16i+1);
            AV49GXV7 = (int)(AV49GXV7+1);
         }
         AV22NotAddedKeyList.Clear();
         AV20NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV16i = 1;
         AV50GXV8 = 1;
         while ( AV50GXV8 <= AV22NotAddedKeyList.Count )
         {
            AV12ContratoAuxiliar_UsuarioCod = (int)(AV22NotAddedKeyList.GetNumeric(AV50GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
            if ( AV12ContratoAuxiliar_UsuarioCod == AV24NotAssociatedRecords )
            {
               if (true) break;
            }
            AV16i = (int)(AV16i+1);
            AV50GXV8 = (int)(AV50GXV8+1);
         }
         if ( AV16i <= AV22NotAddedKeyList.Count )
         {
            AV13Description = ((String)AV20NotAddedDscList.Item(AV16i));
            AV17InsertIndex = 1;
            while ( ( AV17InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV17InsertIndex)), AV13Description) < 0 ) )
            {
               AV17InsertIndex = (int)(AV17InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV24NotAssociatedRecords, AV17InsertIndex);
            AV5AddedDscList.Add(AV13Description, AV17InsertIndex);
            AV22NotAddedKeyList.RemoveItem(AV16i);
            AV20NotAddedDscList.RemoveItem(AV16i);
            /* Execute user subroutine: 'SAVELISTS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV16i = 1;
         AV51GXV9 = 1;
         while ( AV51GXV9 <= AV7AddedKeyList.Count )
         {
            AV12ContratoAuxiliar_UsuarioCod = (int)(AV7AddedKeyList.GetNumeric(AV51GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ContratoAuxiliar_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ContratoAuxiliar_UsuarioCod), 6, 0)));
            if ( AV12ContratoAuxiliar_UsuarioCod == AV9AssociatedRecords )
            {
               if (true) break;
            }
            AV16i = (int)(AV16i+1);
            AV51GXV9 = (int)(AV51GXV9+1);
         }
         if ( AV16i <= AV7AddedKeyList.Count )
         {
            AV13Description = ((String)AV5AddedDscList.Item(AV16i));
            AV17InsertIndex = 1;
            while ( ( AV17InsertIndex <= AV20NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV20NotAddedDscList.Item(AV17InsertIndex)), AV13Description) < 0 ) )
            {
               AV17InsertIndex = (int)(AV17InsertIndex+1);
            }
            AV22NotAddedKeyList.Add(AV9AssociatedRecords, AV17InsertIndex);
            AV20NotAddedDscList.Add(AV13Description, AV17InsertIndex);
            AV7AddedKeyList.RemoveItem(AV16i);
            AV5AddedDscList.RemoveItem(AV16i);
            /* Execute user subroutine: 'SAVELISTS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'ORDENARCOLLECTIONS' Routine */
         AV25sdt_associar.Clear();
         AV16i = 1;
         while ( AV16i <= AV7AddedKeyList.Count )
         {
            AV26sdt_associaritem.gxTpr_Key = (int)(AV7AddedKeyList.GetNumeric(AV16i));
            AV26sdt_associaritem.gxTpr_Descricao = ((String)AV5AddedDscList.Item(AV16i));
            AV25sdt_associar.Add(AV26sdt_associaritem, 0);
            AV26sdt_associaritem = new SdtSDT_Associar(context);
            AV16i = (int)(AV16i+1);
         }
         AV25sdt_associar.Sort("Descricao");
         AV7AddedKeyList.Clear();
         AV5AddedDscList.Clear();
         AV52GXV10 = 1;
         while ( AV52GXV10 <= AV25sdt_associar.Count )
         {
            AV26sdt_associaritem = ((SdtSDT_Associar)AV25sdt_associar.Item(AV52GXV10));
            AV7AddedKeyList.Add(AV26sdt_associaritem.gxTpr_Key, 0);
            AV5AddedDscList.Add(AV26sdt_associaritem.gxTpr_Descricao, 0);
            AV52GXV10 = (int)(AV52GXV10+1);
         }
         AV25sdt_associar.Clear();
         AV16i = 1;
         while ( AV16i <= AV22NotAddedKeyList.Count )
         {
            AV26sdt_associaritem.gxTpr_Key = (int)(AV22NotAddedKeyList.GetNumeric(AV16i));
            AV26sdt_associaritem.gxTpr_Descricao = ((String)AV20NotAddedDscList.Item(AV16i));
            AV25sdt_associar.Add(AV26sdt_associaritem, 0);
            AV26sdt_associaritem = new SdtSDT_Associar(context);
            AV16i = (int)(AV16i+1);
         }
         AV25sdt_associar.Sort("Descricao");
         AV22NotAddedKeyList.Clear();
         AV20NotAddedDscList.Clear();
         AV53GXV11 = 1;
         while ( AV53GXV11 <= AV25sdt_associar.Count )
         {
            AV26sdt_associaritem = ((SdtSDT_Associar)AV25sdt_associar.Item(AV53GXV11));
            AV22NotAddedKeyList.Add(AV26sdt_associaritem.gxTpr_Key, 0);
            AV20NotAddedDscList.Add(AV26sdt_associaritem.gxTpr_Descricao, 0);
            AV53GXV11 = (int)(AV53GXV11+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E20NN2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_NN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_NN2( true) ;
         }
         else
         {
            wb_table2_8_NN2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_NN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_18_NN2( true) ;
         }
         else
         {
            wb_table3_18_NN2( false) ;
         }
         return  ;
      }

      protected void wb_table3_18_NN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_50_NN2( true) ;
         }
         else
         {
            wb_table4_50_NN2( false) ;
         }
         return  ;
      }

      protected void wb_table4_50_NN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_NN2e( true) ;
         }
         else
         {
            wb_table1_2_NN2e( false) ;
         }
      }

      protected void wb_table4_50_NN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_50_NN2e( true) ;
         }
         else
         {
            wb_table4_50_NN2e( false) ;
         }
      }

      protected void wb_table3_18_NN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_21_NN2( true) ;
         }
         else
         {
            wb_table5_21_NN2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_NN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_18_NN2e( true) ;
         }
         else
         {
            wb_table3_18_NN2e( false) ;
         }
      }

      protected void wb_table5_21_NN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Usu�rios N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Usu�rios Associados", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WP_AssociarContrato_Auxiliar.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_33_NN2( true) ;
         }
         else
         {
            wb_table6_33_NN2( false) ;
         }
         return  ;
      }

      protected void wb_table6_33_NN2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WP_AssociarContrato_Auxiliar.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_NN2e( true) ;
         }
         else
         {
            wb_table5_21_NN2e( false) ;
         }
      }

      protected void wb_table6_33_NN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_33_NN2e( true) ;
         }
         else
         {
            wb_table6_33_NN2e( false) ;
         }
      }

      protected void wb_table2_8_NN2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Associar Auxiliares ao Contrato :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinativo_Internalname, " (Inativo)", "", "", lblTbinativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", lblTbinativo_Visible, 1, 0, "HLP_WP_AssociarContrato_Auxiliar.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_NN2e( true) ;
         }
         else
         {
            wb_table2_8_NN2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV11ContratoAuxiliar_ContratoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoAuxiliar_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoAuxiliar_ContratoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOAUXILIAR_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratoAuxiliar_ContratoCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PANN2( ) ;
         WSNN2( ) ;
         WENN2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311961934");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarcontrato_auxiliar.js", "?2020311961934");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTbinativo_Internalname = "TBINATIVO";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         lblTbinativo_Visible = 1;
         edtContrato_Numero_Jsonclick = "";
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV28WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E13NN2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'A1824ContratoAuxiliar_ContratoCod',fld:'CONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11ContratoAuxiliar_ContratoCod',fld:'vCONTRATOAUXILIAR_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1825ContratoAuxiliar_UsuarioCod',fld:'CONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV29ContratoAuxiliar',fld:'vCONTRATOAUXILIAR',pic:'',nv:null},{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV29ContratoAuxiliar',fld:'vCONTRATOAUXILIAR',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14NN2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15NN2',iparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16NN2',iparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17NN2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VASSOCIATEDRECORDS.DBLCLICK","{handler:'E18NN2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VNOTASSOCIATEDRECORDS.DBLCLICK","{handler:'E19NN2',iparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV12ContratoAuxiliar_UsuarioCod',fld:'vCONTRATOAUXILIAR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7AddedKeyList = new GxSimpleCollection();
         AV5AddedDscList = new GxSimpleCollection();
         AV22NotAddedKeyList = new GxSimpleCollection();
         AV20NotAddedDscList = new GxSimpleCollection();
         AV29ContratoAuxiliar = new SdtContratoAuxiliar(context);
         A77Contrato_Numero = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV8AddedKeyListXml = "";
         AV23NotAddedKeyListXml = "";
         AV6AddedDscListXml = "";
         AV21NotAddedDscListXml = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00NN2_A74Contrato_Codigo = new int[1] ;
         H00NN2_A77Contrato_Numero = new String[] {""} ;
         AV28WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15HTTPRequest = new GxHttpRequest( context);
         H00NN3_A74Contrato_Codigo = new int[1] ;
         H00NN3_A92Contrato_Ativo = new bool[] {false} ;
         H00NN4_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00NN4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         AV30Gestores = new GxSimpleCollection();
         H00NN5_A5AreaTrabalho_Codigo = new int[1] ;
         H00NN5_A29Contratante_Codigo = new int[1] ;
         H00NN5_n29Contratante_Codigo = new bool[] {false} ;
         H00NN6_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00NN6_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00NN6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00NN6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00NN6_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00NN6_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         H00NN7_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00NN7_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         AV13Description = "";
         H00NN8_A74Contrato_Codigo = new int[1] ;
         H00NN8_A39Contratada_Codigo = new int[1] ;
         H00NN9_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00NN9_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00NN9_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00NN9_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00NN9_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00NN9_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         H00NN10_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00NN10_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         H00NN11_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00NN11_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         H00NN12_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         H00NN12_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         AV47GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV19Message = new SdtMessages_Message(context);
         AV25sdt_associar = new GxObjectCollection( context, "SDT_Associar", "GxEv3Up14_Meetrika", "SdtSDT_Associar", "GeneXus.Programs");
         AV26sdt_associaritem = new SdtSDT_Associar(context);
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         lblTbinativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarcontrato_auxiliar__default(),
            new Object[][] {
                new Object[] {
               H00NN2_A74Contrato_Codigo, H00NN2_A77Contrato_Numero
               }
               , new Object[] {
               H00NN3_A74Contrato_Codigo, H00NN3_A92Contrato_Ativo
               }
               , new Object[] {
               H00NN4_A1078ContratoGestor_ContratoCod, H00NN4_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00NN5_A5AreaTrabalho_Codigo, H00NN5_A29Contratante_Codigo, H00NN5_n29Contratante_Codigo
               }
               , new Object[] {
               H00NN6_A61ContratanteUsuario_UsuarioPessoaCod, H00NN6_n61ContratanteUsuario_UsuarioPessoaCod, H00NN6_A63ContratanteUsuario_ContratanteCod, H00NN6_A60ContratanteUsuario_UsuarioCod, H00NN6_A62ContratanteUsuario_UsuarioPessoaNom, H00NN6_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00NN7_A1825ContratoAuxiliar_UsuarioCod, H00NN7_A1824ContratoAuxiliar_ContratoCod
               }
               , new Object[] {
               H00NN8_A74Contrato_Codigo, H00NN8_A39Contratada_Codigo
               }
               , new Object[] {
               H00NN9_A70ContratadaUsuario_UsuarioPessoaCod, H00NN9_n70ContratadaUsuario_UsuarioPessoaCod, H00NN9_A66ContratadaUsuario_ContratadaCod, H00NN9_A69ContratadaUsuario_UsuarioCod, H00NN9_A71ContratadaUsuario_UsuarioPessoaNom, H00NN9_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00NN10_A1825ContratoAuxiliar_UsuarioCod, H00NN10_A1824ContratoAuxiliar_ContratoCod
               }
               , new Object[] {
               H00NN11_A1825ContratoAuxiliar_UsuarioCod, H00NN11_A1824ContratoAuxiliar_ContratoCod
               }
               , new Object[] {
               H00NN12_A1825ContratoAuxiliar_UsuarioCod, H00NN12_A1824ContratoAuxiliar_ContratoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_12 ;
      private short nIsMod_12 ;
      private short nRcdExists_11 ;
      private short nIsMod_11 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV33GXLvl6 ;
      private short nGXWrapped ;
      private int AV11ContratoAuxiliar_ContratoCod ;
      private int wcpOAV11ContratoAuxiliar_ContratoCod ;
      private int A1824ContratoAuxiliar_ContratoCod ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private int AV12ContratoAuxiliar_UsuarioCod ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int AV24NotAssociatedRecords ;
      private int AV9AssociatedRecords ;
      private int A74Contrato_Codigo ;
      private int lblTbinativo_Visible ;
      private int bttBtn_confirm_Visible ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int AV10Contratante_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A39Contratada_Codigo ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int AV16i ;
      private int AV41GXV1 ;
      private int AV43GXV2 ;
      private int AV45GXV3 ;
      private int AV46GXV4 ;
      private int AV48GXV6 ;
      private int AV17InsertIndex ;
      private int AV49GXV7 ;
      private int AV50GXV8 ;
      private int AV51GXV9 ;
      private int AV52GXV10 ;
      private int AV53GXV11 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A77Contrato_Numero ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String scmdbuf ;
      private String edtContrato_Numero_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String lblTbinativo_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTbinativo_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A92Contrato_Ativo ;
      private bool n29Contratante_Codigo ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool AV14Exist ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool AV27Success ;
      private String AV8AddedKeyListXml ;
      private String AV23NotAddedKeyListXml ;
      private String AV6AddedDscListXml ;
      private String AV21NotAddedDscListXml ;
      private String AV13Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H00NN2_A74Contrato_Codigo ;
      private String[] H00NN2_A77Contrato_Numero ;
      private int[] H00NN3_A74Contrato_Codigo ;
      private bool[] H00NN3_A92Contrato_Ativo ;
      private int[] H00NN4_A1078ContratoGestor_ContratoCod ;
      private int[] H00NN4_A1079ContratoGestor_UsuarioCod ;
      private int[] H00NN5_A5AreaTrabalho_Codigo ;
      private int[] H00NN5_A29Contratante_Codigo ;
      private bool[] H00NN5_n29Contratante_Codigo ;
      private int[] H00NN6_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00NN6_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00NN6_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00NN6_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00NN6_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00NN6_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00NN7_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] H00NN7_A1824ContratoAuxiliar_ContratoCod ;
      private int[] H00NN8_A74Contrato_Codigo ;
      private int[] H00NN8_A39Contratada_Codigo ;
      private int[] H00NN9_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00NN9_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00NN9_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00NN9_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00NN9_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00NN9_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00NN10_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] H00NN10_A1824ContratoAuxiliar_ContratoCod ;
      private int[] H00NN11_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] H00NN11_A1824ContratoAuxiliar_ContratoCod ;
      private int[] H00NN12_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] H00NN12_A1824ContratoAuxiliar_ContratoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV15HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV7AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV22NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV30Gestores ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV5AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV47GXV5 ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Associar ))]
      private IGxCollection AV25sdt_associar ;
      private SdtMessages_Message AV19Message ;
      private SdtSDT_Associar AV26sdt_associaritem ;
      private wwpbaseobjects.SdtWWPContext AV28WWPContext ;
      private SdtContratoAuxiliar AV29ContratoAuxiliar ;
   }

   public class wp_associarcontrato_auxiliar__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00NN6( IGxContext context ,
                                             int A60ContratanteUsuario_UsuarioCod ,
                                             IGxCollection AV30Gestores ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             int AV10Contratante_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPessoaNom FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30Gestores, "T1.[ContratanteUsuario_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContratanteUsuario_ContratanteCod] = @AV10Contratante_Codigo)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00NN9( IGxContext context ,
                                             int A69ContratadaUsuario_UsuarioCod ,
                                             IGxCollection AV30Gestores ,
                                             IGxCollection AV7AddedKeyList ,
                                             IGxCollection AV22NotAddedKeyList ,
                                             int A66ContratadaUsuario_ContratadaCod ,
                                             int A39Contratada_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [1] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratadaUsuario_ContratadaCod] = @Contratada_Codigo)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30Gestores, "T1.[ContratadaUsuario_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV7AddedKeyList, "T1.[ContratadaUsuario_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV22NotAddedKeyList, "T1.[ContratadaUsuario_UsuarioCod] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 4 :
                     return conditional_H00NN6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 7 :
                     return conditional_H00NN9(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (IGxCollection)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00NN2 ;
          prmH00NN2 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN3 ;
          prmH00NN3 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN4 ;
          prmH00NN4 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN5 ;
          prmH00NN5 = new Object[] {
          new Object[] {"@AV28WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN7 ;
          prmH00NN7 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN8 ;
          prmH00NN8 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN10 ;
          prmH00NN10 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN11 ;
          prmH00NN11 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN12 ;
          prmH00NN12 = new Object[] {
          new Object[] {"@AV11ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN6 ;
          prmH00NN6 = new Object[] {
          new Object[] {"@AV10Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NN9 ;
          prmH00NN9 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00NN2", "SELECT [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV11ContratoAuxiliar_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN2,1,0,true,true )
             ,new CursorDef("H00NN3", "SELECT TOP 1 [Contrato_Codigo], [Contrato_Ativo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV11ContratoAuxiliar_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN3,1,0,false,true )
             ,new CursorDef("H00NN4", "SELECT [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV11ContratoAuxiliar_ContratoCod ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN4,100,0,false,false )
             ,new CursorDef("H00NN5", "SELECT TOP 1 [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV28WWPC_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN5,1,0,false,true )
             ,new CursorDef("H00NN6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN6,100,0,true,false )
             ,new CursorDef("H00NN7", "SELECT [ContratoAuxiliar_UsuarioCod], [ContratoAuxiliar_ContratoCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @AV11ContratoAuxiliar_ContratoCod and [ContratoAuxiliar_UsuarioCod] = @AV12ContratoAuxiliar_UsuarioCod ORDER BY [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN7,1,0,false,true )
             ,new CursorDef("H00NN8", "SELECT [Contrato_Codigo], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV11ContratoAuxiliar_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN8,1,0,true,true )
             ,new CursorDef("H00NN9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN9,100,0,true,false )
             ,new CursorDef("H00NN10", "SELECT [ContratoAuxiliar_UsuarioCod], [ContratoAuxiliar_ContratoCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @AV11ContratoAuxiliar_ContratoCod and [ContratoAuxiliar_UsuarioCod] = @AV12ContratoAuxiliar_UsuarioCod ORDER BY [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN10,1,0,false,true )
             ,new CursorDef("H00NN11", "SELECT [ContratoAuxiliar_UsuarioCod], [ContratoAuxiliar_ContratoCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @AV11ContratoAuxiliar_ContratoCod and [ContratoAuxiliar_UsuarioCod] = @AV12ContratoAuxiliar_UsuarioCod ORDER BY [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN11,1,0,false,true )
             ,new CursorDef("H00NN12", "SELECT [ContratoAuxiliar_UsuarioCod], [ContratoAuxiliar_ContratoCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @AV11ContratoAuxiliar_ContratoCod and [ContratoAuxiliar_UsuarioCod] = @AV12ContratoAuxiliar_UsuarioCod ORDER BY [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NN12,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
