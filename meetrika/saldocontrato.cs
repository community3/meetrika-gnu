/*
               File: SaldoContrato
        Description: Saldo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:45:1.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class saldocontrato : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_21") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_21( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_22") == 0 )
         {
            A1783SaldoContrato_UnidadeMedicao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1783SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_22( A1783SaldoContrato_UnidadeMedicao_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7SaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SaldoContrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SaldoContrato_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Saldo Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public saldocontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public saldocontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_SaldoContrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7SaldoContrato_Codigo = aP1_SaldoContrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3Z179( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3Z179e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3Z179( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3Z179( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3Z179e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_61_3Z179( true) ;
         }
         return  ;
      }

      protected void wb_table3_61_3Z179e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3Z179e( true) ;
         }
         else
         {
            wb_table1_2_3Z179e( false) ;
         }
      }

      protected void wb_table3_61_3Z179( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_61_3Z179e( true) ;
         }
         else
         {
            wb_table3_61_3Z179e( false) ;
         }
      }

      protected void wb_table2_5_3Z179( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3Z179( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3Z179e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3Z179e( true) ;
         }
         else
         {
            wb_table2_5_3Z179e( false) ;
         }
      }

      protected void wb_table4_13_3Z179( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_codigo_Internalname, "Saldo Contrato", "", "", lblTextblocksaldocontrato_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")), ((edtSaldoContrato_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSaldoContrato_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_codigo_Internalname, "Contrato", "", "", lblTextblockcontrato_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), ((edtContrato_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_vigenciainicio_Internalname, "Inicial", "", "", lblTextblocksaldocontrato_vigenciainicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSaldoContrato_VigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_VigenciaInicio_Internalname, context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"), context.localUtil.Format( A1571SaldoContrato_VigenciaInicio, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_VigenciaInicio_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtSaldoContrato_VigenciaInicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtSaldoContrato_VigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtSaldoContrato_VigenciaInicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_vigenciafim_Internalname, "Final", "", "", lblTextblocksaldocontrato_vigenciafim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSaldoContrato_VigenciaFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_VigenciaFim_Internalname, context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"), context.localUtil.Format( A1572SaldoContrato_VigenciaFim, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_VigenciaFim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtSaldoContrato_VigenciaFim_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SaldoContrato.htm");
            GxWebStd.gx_bitmap( context, edtSaldoContrato_VigenciaFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtSaldoContrato_VigenciaFim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SaldoContrato.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_credito_Internalname, "Cr�dito", "", "", lblTextblocksaldocontrato_credito_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Credito_Internalname, StringUtil.LTrim( StringUtil.NToC( A1573SaldoContrato_Credito, 18, 5, ",", "")), ((edtSaldoContrato_Credito_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Credito_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSaldoContrato_Credito_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_reservado_Internalname, "Reservado", "", "", lblTextblocksaldocontrato_reservado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Reservado_Internalname, StringUtil.LTrim( StringUtil.NToC( A1574SaldoContrato_Reservado, 18, 5, ",", "")), ((edtSaldoContrato_Reservado_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Reservado_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSaldoContrato_Reservado_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_estimado_Internalname, "Estimado", "", "", lblTextblocksaldocontrato_estimado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Estimado_Internalname, StringUtil.LTrim( StringUtil.NToC( A1786SaldoContrato_Estimado, 18, 5, ",", "")), ((edtSaldoContrato_Estimado_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Estimado_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSaldoContrato_Estimado_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_executado_Internalname, "Executado", "", "", lblTextblocksaldocontrato_executado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Executado_Internalname, StringUtil.LTrim( StringUtil.NToC( A1575SaldoContrato_Executado, 18, 5, ",", "")), ((edtSaldoContrato_Executado_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Executado_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSaldoContrato_Executado_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_saldo_Internalname, "Saldo", "", "", lblTextblocksaldocontrato_saldo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSaldoContrato_Saldo_Internalname, StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ",", "")), ((edtSaldoContrato_Saldo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSaldoContrato_Saldo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSaldoContrato_Saldo_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SaldoContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3Z179e( true) ;
         }
         else
         {
            wb_table4_13_3Z179e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113Z2 */
         E113Z2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSaldoContrato_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A1571SaldoContrato_VigenciaInicio = context.localUtil.CToD( cgiGet( edtSaldoContrato_VigenciaInicio_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAINICIO", GetSecureSignedToken( "", A1571SaldoContrato_VigenciaInicio));
               A1572SaldoContrato_VigenciaFim = context.localUtil.CToD( cgiGet( edtSaldoContrato_VigenciaFim_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAFIM", GetSecureSignedToken( "", A1572SaldoContrato_VigenciaFim));
               A1573SaldoContrato_Credito = context.localUtil.CToN( cgiGet( edtSaldoContrato_Credito_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_CREDITO", GetSecureSignedToken( "", context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A1574SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Reservado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A1786SaldoContrato_Estimado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Estimado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1786SaldoContrato_Estimado", StringUtil.LTrim( StringUtil.Str( A1786SaldoContrato_Estimado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_ESTIMADO", GetSecureSignedToken( "", context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A1575SaldoContrato_Executado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Executado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_EXECUTADO", GetSecureSignedToken( "", context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A1576SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtSaldoContrato_Saldo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               /* Read saved values. */
               Z1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1561SaldoContrato_Codigo"), ",", "."));
               Z1571SaldoContrato_VigenciaInicio = context.localUtil.CToD( cgiGet( "Z1571SaldoContrato_VigenciaInicio"), 0);
               Z1572SaldoContrato_VigenciaFim = context.localUtil.CToD( cgiGet( "Z1572SaldoContrato_VigenciaFim"), 0);
               Z1573SaldoContrato_Credito = context.localUtil.CToN( cgiGet( "Z1573SaldoContrato_Credito"), ",", ".");
               Z1574SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( "Z1574SaldoContrato_Reservado"), ",", ".");
               Z1786SaldoContrato_Estimado = context.localUtil.CToN( cgiGet( "Z1786SaldoContrato_Estimado"), ",", ".");
               Z1575SaldoContrato_Executado = context.localUtil.CToN( cgiGet( "Z1575SaldoContrato_Executado"), ",", ".");
               Z1576SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( "Z1576SaldoContrato_Saldo"), ",", ".");
               Z1781SaldoContrato_Ativo = StringUtil.StrToBool( cgiGet( "Z1781SaldoContrato_Ativo"));
               Z74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               Z1783SaldoContrato_UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1783SaldoContrato_UnidadeMedicao_Codigo"), ",", "."));
               A1781SaldoContrato_Ativo = StringUtil.StrToBool( cgiGet( "Z1781SaldoContrato_Ativo"));
               A1783SaldoContrato_UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1783SaldoContrato_UnidadeMedicao_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1783SaldoContrato_UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( "N1783SaldoContrato_UnidadeMedicao_Codigo"), ",", "."));
               AV7SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSALDOCONTRATO_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               AV13Insert_SaldoContrato_UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SALDOCONTRATO_UNIDADEMEDICAO_CODIGO"), ",", "."));
               A1783SaldoContrato_UnidadeMedicao_Codigo = (int)(context.localUtil.CToN( cgiGet( "SALDOCONTRATO_UNIDADEMEDICAO_CODIGO"), ",", "."));
               A1781SaldoContrato_Ativo = StringUtil.StrToBool( cgiGet( "SALDOCONTRATO_ATIVO"));
               A1784SaldoContrato_UnidadeMedicao_Nome = cgiGet( "SALDOCONTRATO_UNIDADEMEDICAO_NOME");
               n1784SaldoContrato_UnidadeMedicao_Nome = false;
               A1785SaldoContrato_UnidadeMedicao_Sigla = cgiGet( "SALDOCONTRATO_UNIDADEMEDICAO_SIGLA");
               n1785SaldoContrato_UnidadeMedicao_Sigla = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "SaldoContrato";
               A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSaldoContrato_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9");
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
               A1571SaldoContrato_VigenciaInicio = context.localUtil.CToD( cgiGet( edtSaldoContrato_VigenciaInicio_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAINICIO", GetSecureSignedToken( "", A1571SaldoContrato_VigenciaInicio));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99");
               A1572SaldoContrato_VigenciaFim = context.localUtil.CToD( cgiGet( edtSaldoContrato_VigenciaFim_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAFIM", GetSecureSignedToken( "", A1572SaldoContrato_VigenciaFim));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99");
               A1573SaldoContrato_Credito = context.localUtil.CToN( cgiGet( edtSaldoContrato_Credito_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_CREDITO", GetSecureSignedToken( "", context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               A1574SaldoContrato_Reservado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Reservado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               A1786SaldoContrato_Estimado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Estimado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1786SaldoContrato_Estimado", StringUtil.LTrim( StringUtil.Str( A1786SaldoContrato_Estimado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_ESTIMADO", GetSecureSignedToken( "", context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               A1575SaldoContrato_Executado = context.localUtil.CToN( cgiGet( edtSaldoContrato_Executado_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_EXECUTADO", GetSecureSignedToken( "", context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               A1576SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtSaldoContrato_Saldo_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1781SaldoContrato_Ativo);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1561SaldoContrato_Codigo != Z1561SaldoContrato_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_Codigo:"+context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_VigenciaInicio:"+context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_VigenciaFim:"+context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_Credito:"+context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_Reservado:"+context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_Estimado:"+context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_Executado:"+context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_Saldo:"+context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_UnidadeMedicao_Codigo:"+context.localUtil.Format( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("saldocontrato:[SecurityCheckFailed value for]"+"SaldoContrato_Ativo:"+StringUtil.BoolToStr( A1781SaldoContrato_Ativo));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode179 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode179;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound179 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3Z0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SALDOCONTRATO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtSaldoContrato_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113Z2 */
                           E113Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123Z2 */
                           E123Z2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123Z2 */
            E123Z2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3Z179( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3Z179( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3Z0( )
      {
         BeforeValidate3Z179( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3Z179( ) ;
            }
            else
            {
               CheckExtendedTable3Z179( ) ;
               CloseExtendedTableCursors3Z179( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3Z0( )
      {
      }

      protected void E113Z2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "SaldoContrato_UnidadeMedicao_Codigo") == 0 )
               {
                  AV13Insert_SaldoContrato_UnidadeMedicao_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E123Z2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwsaldocontrato.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3Z179( short GX_JID )
      {
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1571SaldoContrato_VigenciaInicio = T003Z3_A1571SaldoContrato_VigenciaInicio[0];
               Z1572SaldoContrato_VigenciaFim = T003Z3_A1572SaldoContrato_VigenciaFim[0];
               Z1573SaldoContrato_Credito = T003Z3_A1573SaldoContrato_Credito[0];
               Z1574SaldoContrato_Reservado = T003Z3_A1574SaldoContrato_Reservado[0];
               Z1786SaldoContrato_Estimado = T003Z3_A1786SaldoContrato_Estimado[0];
               Z1575SaldoContrato_Executado = T003Z3_A1575SaldoContrato_Executado[0];
               Z1576SaldoContrato_Saldo = T003Z3_A1576SaldoContrato_Saldo[0];
               Z1781SaldoContrato_Ativo = T003Z3_A1781SaldoContrato_Ativo[0];
               Z74Contrato_Codigo = T003Z3_A74Contrato_Codigo[0];
               Z1783SaldoContrato_UnidadeMedicao_Codigo = T003Z3_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            }
            else
            {
               Z1571SaldoContrato_VigenciaInicio = A1571SaldoContrato_VigenciaInicio;
               Z1572SaldoContrato_VigenciaFim = A1572SaldoContrato_VigenciaFim;
               Z1573SaldoContrato_Credito = A1573SaldoContrato_Credito;
               Z1574SaldoContrato_Reservado = A1574SaldoContrato_Reservado;
               Z1786SaldoContrato_Estimado = A1786SaldoContrato_Estimado;
               Z1575SaldoContrato_Executado = A1575SaldoContrato_Executado;
               Z1576SaldoContrato_Saldo = A1576SaldoContrato_Saldo;
               Z1781SaldoContrato_Ativo = A1781SaldoContrato_Ativo;
               Z74Contrato_Codigo = A74Contrato_Codigo;
               Z1783SaldoContrato_UnidadeMedicao_Codigo = A1783SaldoContrato_UnidadeMedicao_Codigo;
            }
         }
         if ( GX_JID == -20 )
         {
            Z1561SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            Z1571SaldoContrato_VigenciaInicio = A1571SaldoContrato_VigenciaInicio;
            Z1572SaldoContrato_VigenciaFim = A1572SaldoContrato_VigenciaFim;
            Z1573SaldoContrato_Credito = A1573SaldoContrato_Credito;
            Z1574SaldoContrato_Reservado = A1574SaldoContrato_Reservado;
            Z1786SaldoContrato_Estimado = A1786SaldoContrato_Estimado;
            Z1575SaldoContrato_Executado = A1575SaldoContrato_Executado;
            Z1576SaldoContrato_Saldo = A1576SaldoContrato_Saldo;
            Z1781SaldoContrato_Ativo = A1781SaldoContrato_Ativo;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z1783SaldoContrato_UnidadeMedicao_Codigo = A1783SaldoContrato_UnidadeMedicao_Codigo;
            Z1784SaldoContrato_UnidadeMedicao_Nome = A1784SaldoContrato_UnidadeMedicao_Nome;
            Z1785SaldoContrato_UnidadeMedicao_Sigla = A1785SaldoContrato_UnidadeMedicao_Sigla;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         edtSaldoContrato_VigenciaInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_VigenciaInicio_Enabled), 5, 0)));
         edtSaldoContrato_VigenciaFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_VigenciaFim_Enabled), 5, 0)));
         edtSaldoContrato_Credito_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Credito_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Credito_Enabled), 5, 0)));
         edtSaldoContrato_Reservado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Reservado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Reservado_Enabled), 5, 0)));
         edtSaldoContrato_Estimado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Estimado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Estimado_Enabled), 5, 0)));
         edtSaldoContrato_Executado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Executado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Executado_Enabled), 5, 0)));
         edtSaldoContrato_Saldo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Saldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Saldo_Enabled), 5, 0)));
         edtSaldoContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "SaldoContrato";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         edtSaldoContrato_VigenciaInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_VigenciaInicio_Enabled), 5, 0)));
         edtSaldoContrato_VigenciaFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_VigenciaFim_Enabled), 5, 0)));
         edtSaldoContrato_Credito_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Credito_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Credito_Enabled), 5, 0)));
         edtSaldoContrato_Reservado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Reservado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Reservado_Enabled), 5, 0)));
         edtSaldoContrato_Estimado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Estimado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Estimado_Enabled), 5, 0)));
         edtSaldoContrato_Executado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Executado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Executado_Enabled), 5, 0)));
         edtSaldoContrato_Saldo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Saldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Saldo_Enabled), 5, 0)));
         edtSaldoContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7SaldoContrato_Codigo) )
         {
            A1561SaldoContrato_Codigo = AV7SaldoContrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_SaldoContrato_UnidadeMedicao_Codigo) )
         {
            A1783SaldoContrato_UnidadeMedicao_Codigo = AV13Insert_SaldoContrato_UnidadeMedicao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1783SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003Z5 */
            pr_default.execute(3, new Object[] {A1783SaldoContrato_UnidadeMedicao_Codigo});
            A1784SaldoContrato_UnidadeMedicao_Nome = T003Z5_A1784SaldoContrato_UnidadeMedicao_Nome[0];
            n1784SaldoContrato_UnidadeMedicao_Nome = T003Z5_n1784SaldoContrato_UnidadeMedicao_Nome[0];
            A1785SaldoContrato_UnidadeMedicao_Sigla = T003Z5_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
            n1785SaldoContrato_UnidadeMedicao_Sigla = T003Z5_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
            pr_default.close(3);
         }
      }

      protected void Load3Z179( )
      {
         /* Using cursor T003Z6 */
         pr_default.execute(4, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound179 = 1;
            A1571SaldoContrato_VigenciaInicio = T003Z6_A1571SaldoContrato_VigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAINICIO", GetSecureSignedToken( "", A1571SaldoContrato_VigenciaInicio));
            A1572SaldoContrato_VigenciaFim = T003Z6_A1572SaldoContrato_VigenciaFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAFIM", GetSecureSignedToken( "", A1572SaldoContrato_VigenciaFim));
            A1573SaldoContrato_Credito = T003Z6_A1573SaldoContrato_Credito[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_CREDITO", GetSecureSignedToken( "", context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1574SaldoContrato_Reservado = T003Z6_A1574SaldoContrato_Reservado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1786SaldoContrato_Estimado = T003Z6_A1786SaldoContrato_Estimado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1786SaldoContrato_Estimado", StringUtil.LTrim( StringUtil.Str( A1786SaldoContrato_Estimado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_ESTIMADO", GetSecureSignedToken( "", context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1575SaldoContrato_Executado = T003Z6_A1575SaldoContrato_Executado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_EXECUTADO", GetSecureSignedToken( "", context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1576SaldoContrato_Saldo = T003Z6_A1576SaldoContrato_Saldo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1781SaldoContrato_Ativo = T003Z6_A1781SaldoContrato_Ativo[0];
            A1784SaldoContrato_UnidadeMedicao_Nome = T003Z6_A1784SaldoContrato_UnidadeMedicao_Nome[0];
            n1784SaldoContrato_UnidadeMedicao_Nome = T003Z6_n1784SaldoContrato_UnidadeMedicao_Nome[0];
            A1785SaldoContrato_UnidadeMedicao_Sigla = T003Z6_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
            n1785SaldoContrato_UnidadeMedicao_Sigla = T003Z6_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
            A74Contrato_Codigo = T003Z6_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A1783SaldoContrato_UnidadeMedicao_Codigo = T003Z6_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            ZM3Z179( -20) ;
         }
         pr_default.close(4);
         OnLoadActions3Z179( ) ;
      }

      protected void OnLoadActions3Z179( )
      {
      }

      protected void CheckExtendedTable3Z179( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( (0==A1561SaldoContrato_Codigo) )
         {
            GX_msglist.addItem("Saldo Contrato � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( (DateTime.MinValue==A1571SaldoContrato_VigenciaInicio) )
         {
            GX_msglist.addItem("Vigencia Inicial � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( (DateTime.MinValue==A1572SaldoContrato_VigenciaFim) )
         {
            GX_msglist.addItem("Vigencia Final � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor T003Z4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( (0==A74Contrato_Codigo) )
         {
            GX_msglist.addItem("Contrato � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor T003Z5 */
         pr_default.execute(3, new Object[] {A1783SaldoContrato_UnidadeMedicao_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Saldo Contrato Unidade Medicao'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1784SaldoContrato_UnidadeMedicao_Nome = T003Z5_A1784SaldoContrato_UnidadeMedicao_Nome[0];
         n1784SaldoContrato_UnidadeMedicao_Nome = T003Z5_n1784SaldoContrato_UnidadeMedicao_Nome[0];
         A1785SaldoContrato_UnidadeMedicao_Sigla = T003Z5_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
         n1785SaldoContrato_UnidadeMedicao_Sigla = T003Z5_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3Z179( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_21( int A74Contrato_Codigo )
      {
         /* Using cursor T003Z7 */
         pr_default.execute(5, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_22( int A1783SaldoContrato_UnidadeMedicao_Codigo )
      {
         /* Using cursor T003Z8 */
         pr_default.execute(6, new Object[] {A1783SaldoContrato_UnidadeMedicao_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Saldo Contrato Unidade Medicao'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1784SaldoContrato_UnidadeMedicao_Nome = T003Z8_A1784SaldoContrato_UnidadeMedicao_Nome[0];
         n1784SaldoContrato_UnidadeMedicao_Nome = T003Z8_n1784SaldoContrato_UnidadeMedicao_Nome[0];
         A1785SaldoContrato_UnidadeMedicao_Sigla = T003Z8_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
         n1785SaldoContrato_UnidadeMedicao_Sigla = T003Z8_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1784SaldoContrato_UnidadeMedicao_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1785SaldoContrato_UnidadeMedicao_Sigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey3Z179( )
      {
         /* Using cursor T003Z9 */
         pr_default.execute(7, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound179 = 1;
         }
         else
         {
            RcdFound179 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003Z3 */
         pr_default.execute(1, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3Z179( 20) ;
            RcdFound179 = 1;
            A1561SaldoContrato_Codigo = T003Z3_A1561SaldoContrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            A1571SaldoContrato_VigenciaInicio = T003Z3_A1571SaldoContrato_VigenciaInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAINICIO", GetSecureSignedToken( "", A1571SaldoContrato_VigenciaInicio));
            A1572SaldoContrato_VigenciaFim = T003Z3_A1572SaldoContrato_VigenciaFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAFIM", GetSecureSignedToken( "", A1572SaldoContrato_VigenciaFim));
            A1573SaldoContrato_Credito = T003Z3_A1573SaldoContrato_Credito[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_CREDITO", GetSecureSignedToken( "", context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1574SaldoContrato_Reservado = T003Z3_A1574SaldoContrato_Reservado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1786SaldoContrato_Estimado = T003Z3_A1786SaldoContrato_Estimado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1786SaldoContrato_Estimado", StringUtil.LTrim( StringUtil.Str( A1786SaldoContrato_Estimado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_ESTIMADO", GetSecureSignedToken( "", context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1575SaldoContrato_Executado = T003Z3_A1575SaldoContrato_Executado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_EXECUTADO", GetSecureSignedToken( "", context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1576SaldoContrato_Saldo = T003Z3_A1576SaldoContrato_Saldo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1781SaldoContrato_Ativo = T003Z3_A1781SaldoContrato_Ativo[0];
            A74Contrato_Codigo = T003Z3_A74Contrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            A1783SaldoContrato_UnidadeMedicao_Codigo = T003Z3_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            Z1561SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            sMode179 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3Z179( ) ;
            if ( AnyError == 1 )
            {
               RcdFound179 = 0;
               InitializeNonKey3Z179( ) ;
            }
            Gx_mode = sMode179;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound179 = 0;
            InitializeNonKey3Z179( ) ;
            sMode179 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode179;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3Z179( ) ;
         if ( RcdFound179 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound179 = 0;
         /* Using cursor T003Z10 */
         pr_default.execute(8, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003Z10_A1561SaldoContrato_Codigo[0] < A1561SaldoContrato_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003Z10_A1561SaldoContrato_Codigo[0] > A1561SaldoContrato_Codigo ) ) )
            {
               A1561SaldoContrato_Codigo = T003Z10_A1561SaldoContrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
               RcdFound179 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound179 = 0;
         /* Using cursor T003Z11 */
         pr_default.execute(9, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T003Z11_A1561SaldoContrato_Codigo[0] > A1561SaldoContrato_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T003Z11_A1561SaldoContrato_Codigo[0] < A1561SaldoContrato_Codigo ) ) )
            {
               A1561SaldoContrato_Codigo = T003Z11_A1561SaldoContrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
               RcdFound179 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3Z179( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3Z179( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound179 == 1 )
            {
               if ( A1561SaldoContrato_Codigo != Z1561SaldoContrato_Codigo )
               {
                  A1561SaldoContrato_Codigo = Z1561SaldoContrato_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SALDOCONTRATO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSaldoContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  /* Update record */
                  Update3Z179( ) ;
               }
            }
            else
            {
               if ( A1561SaldoContrato_Codigo != Z1561SaldoContrato_Codigo )
               {
                  /* Insert record */
                  Insert3Z179( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SALDOCONTRATO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSaldoContrato_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     Insert3Z179( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1561SaldoContrato_Codigo != Z1561SaldoContrato_Codigo )
         {
            A1561SaldoContrato_Codigo = Z1561SaldoContrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SALDOCONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSaldoContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3Z179( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003Z2 */
            pr_default.execute(0, new Object[] {A1561SaldoContrato_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SaldoContrato"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1571SaldoContrato_VigenciaInicio != T003Z2_A1571SaldoContrato_VigenciaInicio[0] ) || ( Z1572SaldoContrato_VigenciaFim != T003Z2_A1572SaldoContrato_VigenciaFim[0] ) || ( Z1573SaldoContrato_Credito != T003Z2_A1573SaldoContrato_Credito[0] ) || ( Z1574SaldoContrato_Reservado != T003Z2_A1574SaldoContrato_Reservado[0] ) || ( Z1786SaldoContrato_Estimado != T003Z2_A1786SaldoContrato_Estimado[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1575SaldoContrato_Executado != T003Z2_A1575SaldoContrato_Executado[0] ) || ( Z1576SaldoContrato_Saldo != T003Z2_A1576SaldoContrato_Saldo[0] ) || ( Z1781SaldoContrato_Ativo != T003Z2_A1781SaldoContrato_Ativo[0] ) || ( Z74Contrato_Codigo != T003Z2_A74Contrato_Codigo[0] ) || ( Z1783SaldoContrato_UnidadeMedicao_Codigo != T003Z2_A1783SaldoContrato_UnidadeMedicao_Codigo[0] ) )
            {
               if ( Z1571SaldoContrato_VigenciaInicio != T003Z2_A1571SaldoContrato_VigenciaInicio[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_VigenciaInicio");
                  GXUtil.WriteLogRaw("Old: ",Z1571SaldoContrato_VigenciaInicio);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1571SaldoContrato_VigenciaInicio[0]);
               }
               if ( Z1572SaldoContrato_VigenciaFim != T003Z2_A1572SaldoContrato_VigenciaFim[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_VigenciaFim");
                  GXUtil.WriteLogRaw("Old: ",Z1572SaldoContrato_VigenciaFim);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1572SaldoContrato_VigenciaFim[0]);
               }
               if ( Z1573SaldoContrato_Credito != T003Z2_A1573SaldoContrato_Credito[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_Credito");
                  GXUtil.WriteLogRaw("Old: ",Z1573SaldoContrato_Credito);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1573SaldoContrato_Credito[0]);
               }
               if ( Z1574SaldoContrato_Reservado != T003Z2_A1574SaldoContrato_Reservado[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_Reservado");
                  GXUtil.WriteLogRaw("Old: ",Z1574SaldoContrato_Reservado);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1574SaldoContrato_Reservado[0]);
               }
               if ( Z1786SaldoContrato_Estimado != T003Z2_A1786SaldoContrato_Estimado[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_Estimado");
                  GXUtil.WriteLogRaw("Old: ",Z1786SaldoContrato_Estimado);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1786SaldoContrato_Estimado[0]);
               }
               if ( Z1575SaldoContrato_Executado != T003Z2_A1575SaldoContrato_Executado[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_Executado");
                  GXUtil.WriteLogRaw("Old: ",Z1575SaldoContrato_Executado);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1575SaldoContrato_Executado[0]);
               }
               if ( Z1576SaldoContrato_Saldo != T003Z2_A1576SaldoContrato_Saldo[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_Saldo");
                  GXUtil.WriteLogRaw("Old: ",Z1576SaldoContrato_Saldo);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1576SaldoContrato_Saldo[0]);
               }
               if ( Z1781SaldoContrato_Ativo != T003Z2_A1781SaldoContrato_Ativo[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1781SaldoContrato_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1781SaldoContrato_Ativo[0]);
               }
               if ( Z74Contrato_Codigo != T003Z2_A74Contrato_Codigo[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"Contrato_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z74Contrato_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A74Contrato_Codigo[0]);
               }
               if ( Z1783SaldoContrato_UnidadeMedicao_Codigo != T003Z2_A1783SaldoContrato_UnidadeMedicao_Codigo[0] )
               {
                  GXUtil.WriteLog("saldocontrato:[seudo value changed for attri]"+"SaldoContrato_UnidadeMedicao_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1783SaldoContrato_UnidadeMedicao_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T003Z2_A1783SaldoContrato_UnidadeMedicao_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SaldoContrato"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3Z179( )
      {
         BeforeValidate3Z179( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3Z179( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3Z179( 0) ;
            CheckOptimisticConcurrency3Z179( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3Z179( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3Z179( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003Z12 */
                     pr_default.execute(10, new Object[] {A1571SaldoContrato_VigenciaInicio, A1572SaldoContrato_VigenciaFim, A1573SaldoContrato_Credito, A1574SaldoContrato_Reservado, A1786SaldoContrato_Estimado, A1575SaldoContrato_Executado, A1576SaldoContrato_Saldo, A1781SaldoContrato_Ativo, A74Contrato_Codigo, A1783SaldoContrato_UnidadeMedicao_Codigo});
                     A1561SaldoContrato_Codigo = T003Z12_A1561SaldoContrato_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3Z0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3Z179( ) ;
            }
            EndLevel3Z179( ) ;
         }
         CloseExtendedTableCursors3Z179( ) ;
      }

      protected void Update3Z179( )
      {
         BeforeValidate3Z179( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3Z179( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3Z179( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3Z179( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3Z179( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003Z13 */
                     pr_default.execute(11, new Object[] {A1571SaldoContrato_VigenciaInicio, A1572SaldoContrato_VigenciaFim, A1573SaldoContrato_Credito, A1574SaldoContrato_Reservado, A1786SaldoContrato_Estimado, A1575SaldoContrato_Executado, A1576SaldoContrato_Saldo, A1781SaldoContrato_Ativo, A74Contrato_Codigo, A1783SaldoContrato_UnidadeMedicao_Codigo, A1561SaldoContrato_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SaldoContrato"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3Z179( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3Z179( ) ;
         }
         CloseExtendedTableCursors3Z179( ) ;
      }

      protected void DeferredUpdate3Z179( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3Z179( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3Z179( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3Z179( ) ;
            AfterConfirm3Z179( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3Z179( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003Z14 */
                  pr_default.execute(12, new Object[] {A1561SaldoContrato_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode179 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3Z179( ) ;
         Gx_mode = sMode179;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3Z179( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003Z15 */
            pr_default.execute(13, new Object[] {A1783SaldoContrato_UnidadeMedicao_Codigo});
            A1784SaldoContrato_UnidadeMedicao_Nome = T003Z15_A1784SaldoContrato_UnidadeMedicao_Nome[0];
            n1784SaldoContrato_UnidadeMedicao_Nome = T003Z15_n1784SaldoContrato_UnidadeMedicao_Nome[0];
            A1785SaldoContrato_UnidadeMedicao_Sigla = T003Z15_A1785SaldoContrato_UnidadeMedicao_Sigla[0];
            n1785SaldoContrato_UnidadeMedicao_Sigla = T003Z15_n1785SaldoContrato_UnidadeMedicao_Sigla[0];
            pr_default.close(13);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T003Z16 */
            pr_default.execute(14, new Object[] {A1561SaldoContrato_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {" T178"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
         }
      }

      protected void EndLevel3Z179( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3Z179( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "SaldoContrato");
            if ( AnyError == 0 )
            {
               ConfirmValues3Z0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "SaldoContrato");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3Z179( )
      {
         /* Scan By routine */
         /* Using cursor T003Z17 */
         pr_default.execute(15);
         RcdFound179 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound179 = 1;
            A1561SaldoContrato_Codigo = T003Z17_A1561SaldoContrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3Z179( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound179 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound179 = 1;
            A1561SaldoContrato_Codigo = T003Z17_A1561SaldoContrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3Z179( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm3Z179( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3Z179( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3Z179( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3Z179( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3Z179( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3Z179( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3Z179( )
      {
         edtSaldoContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Codigo_Enabled), 5, 0)));
         edtContrato_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Enabled), 5, 0)));
         edtSaldoContrato_VigenciaInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_VigenciaInicio_Enabled), 5, 0)));
         edtSaldoContrato_VigenciaFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_VigenciaFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_VigenciaFim_Enabled), 5, 0)));
         edtSaldoContrato_Credito_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Credito_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Credito_Enabled), 5, 0)));
         edtSaldoContrato_Reservado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Reservado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Reservado_Enabled), 5, 0)));
         edtSaldoContrato_Estimado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Estimado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Estimado_Enabled), 5, 0)));
         edtSaldoContrato_Executado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Executado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Executado_Enabled), 5, 0)));
         edtSaldoContrato_Saldo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Saldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSaldoContrato_Saldo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3Z0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020312045210");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("saldocontrato.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SaldoContrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1561SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1571SaldoContrato_VigenciaInicio", context.localUtil.DToC( Z1571SaldoContrato_VigenciaInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1572SaldoContrato_VigenciaFim", context.localUtil.DToC( Z1572SaldoContrato_VigenciaFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.NToC( Z1573SaldoContrato_Credito, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.NToC( Z1574SaldoContrato_Reservado, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1786SaldoContrato_Estimado", StringUtil.LTrim( StringUtil.NToC( Z1786SaldoContrato_Estimado, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.NToC( Z1575SaldoContrato_Executado, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.NToC( Z1576SaldoContrato_Saldo, 18, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1781SaldoContrato_Ativo", Z1781SaldoContrato_Ativo);
         GxWebStd.gx_hidden_field( context, "Z74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1783SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1783SaldoContrato_UnidadeMedicao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1783SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SALDOCONTRATO_UNIDADEMEDICAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_SaldoContrato_UnidadeMedicao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SALDOCONTRATO_UNIDADEMEDICAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "SALDOCONTRATO_ATIVO", A1781SaldoContrato_Ativo);
         GxWebStd.gx_hidden_field( context, "SALDOCONTRATO_UNIDADEMEDICAO_NOME", StringUtil.RTrim( A1784SaldoContrato_UnidadeMedicao_Nome));
         GxWebStd.gx_hidden_field( context, "SALDOCONTRATO_UNIDADEMEDICAO_SIGLA", StringUtil.RTrim( A1785SaldoContrato_UnidadeMedicao_Sigla));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_VIGENCIAINICIO", GetSecureSignedToken( "", A1571SaldoContrato_VigenciaInicio));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_VIGENCIAFIM", GetSecureSignedToken( "", A1572SaldoContrato_VigenciaFim));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_CREDITO", GetSecureSignedToken( "", context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_ESTIMADO", GetSecureSignedToken( "", context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_EXECUTADO", GetSecureSignedToken( "", context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SaldoContrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "SaldoContrato";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1781SaldoContrato_Ativo);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_Codigo:"+context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_VigenciaInicio:"+context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_VigenciaFim:"+context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_Credito:"+context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_Reservado:"+context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_Estimado:"+context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_Executado:"+context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_Saldo:"+context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_UnidadeMedicao_Codigo:"+context.localUtil.Format( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("saldocontrato:[SendSecurityCheck value for]"+"SaldoContrato_Ativo:"+StringUtil.BoolToStr( A1781SaldoContrato_Ativo));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("saldocontrato.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SaldoContrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "SaldoContrato" ;
      }

      public override String GetPgmdesc( )
      {
         return "Saldo Contrato" ;
      }

      protected void InitializeNonKey3Z179( )
      {
         A74Contrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         A1783SaldoContrato_UnidadeMedicao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1783SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1783SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1571SaldoContrato_VigenciaInicio", context.localUtil.Format(A1571SaldoContrato_VigenciaInicio, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAINICIO", GetSecureSignedToken( "", A1571SaldoContrato_VigenciaInicio));
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1572SaldoContrato_VigenciaFim", context.localUtil.Format(A1572SaldoContrato_VigenciaFim, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_VIGENCIAFIM", GetSecureSignedToken( "", A1572SaldoContrato_VigenciaFim));
         A1573SaldoContrato_Credito = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1573SaldoContrato_Credito", StringUtil.LTrim( StringUtil.Str( A1573SaldoContrato_Credito, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_CREDITO", GetSecureSignedToken( "", context.localUtil.Format( A1573SaldoContrato_Credito, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         A1574SaldoContrato_Reservado = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1574SaldoContrato_Reservado", StringUtil.LTrim( StringUtil.Str( A1574SaldoContrato_Reservado, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_RESERVADO", GetSecureSignedToken( "", context.localUtil.Format( A1574SaldoContrato_Reservado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         A1786SaldoContrato_Estimado = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1786SaldoContrato_Estimado", StringUtil.LTrim( StringUtil.Str( A1786SaldoContrato_Estimado, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_ESTIMADO", GetSecureSignedToken( "", context.localUtil.Format( A1786SaldoContrato_Estimado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         A1575SaldoContrato_Executado = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1575SaldoContrato_Executado", StringUtil.LTrim( StringUtil.Str( A1575SaldoContrato_Executado, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_EXECUTADO", GetSecureSignedToken( "", context.localUtil.Format( A1575SaldoContrato_Executado, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         A1576SaldoContrato_Saldo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SALDOCONTRATO_SALDO", GetSecureSignedToken( "", context.localUtil.Format( A1576SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         A1781SaldoContrato_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1781SaldoContrato_Ativo", A1781SaldoContrato_Ativo);
         A1784SaldoContrato_UnidadeMedicao_Nome = "";
         n1784SaldoContrato_UnidadeMedicao_Nome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1784SaldoContrato_UnidadeMedicao_Nome", A1784SaldoContrato_UnidadeMedicao_Nome);
         A1785SaldoContrato_UnidadeMedicao_Sigla = "";
         n1785SaldoContrato_UnidadeMedicao_Sigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1785SaldoContrato_UnidadeMedicao_Sigla", A1785SaldoContrato_UnidadeMedicao_Sigla);
         Z1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         Z1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         Z1573SaldoContrato_Credito = 0;
         Z1574SaldoContrato_Reservado = 0;
         Z1786SaldoContrato_Estimado = 0;
         Z1575SaldoContrato_Executado = 0;
         Z1576SaldoContrato_Saldo = 0;
         Z1781SaldoContrato_Ativo = false;
         Z74Contrato_Codigo = 0;
         Z1783SaldoContrato_UnidadeMedicao_Codigo = 0;
      }

      protected void InitAll3Z179( )
      {
         A1561SaldoContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         InitializeNonKey3Z179( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020312045230");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("saldocontrato.js", "?2020312045230");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksaldocontrato_codigo_Internalname = "TEXTBLOCKSALDOCONTRATO_CODIGO";
         edtSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO";
         lblTextblockcontrato_codigo_Internalname = "TEXTBLOCKCONTRATO_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         lblTextblocksaldocontrato_vigenciainicio_Internalname = "TEXTBLOCKSALDOCONTRATO_VIGENCIAINICIO";
         edtSaldoContrato_VigenciaInicio_Internalname = "SALDOCONTRATO_VIGENCIAINICIO";
         lblTextblocksaldocontrato_vigenciafim_Internalname = "TEXTBLOCKSALDOCONTRATO_VIGENCIAFIM";
         edtSaldoContrato_VigenciaFim_Internalname = "SALDOCONTRATO_VIGENCIAFIM";
         lblTextblocksaldocontrato_credito_Internalname = "TEXTBLOCKSALDOCONTRATO_CREDITO";
         edtSaldoContrato_Credito_Internalname = "SALDOCONTRATO_CREDITO";
         lblTextblocksaldocontrato_reservado_Internalname = "TEXTBLOCKSALDOCONTRATO_RESERVADO";
         edtSaldoContrato_Reservado_Internalname = "SALDOCONTRATO_RESERVADO";
         lblTextblocksaldocontrato_estimado_Internalname = "TEXTBLOCKSALDOCONTRATO_ESTIMADO";
         edtSaldoContrato_Estimado_Internalname = "SALDOCONTRATO_ESTIMADO";
         lblTextblocksaldocontrato_executado_Internalname = "TEXTBLOCKSALDOCONTRATO_EXECUTADO";
         edtSaldoContrato_Executado_Internalname = "SALDOCONTRATO_EXECUTADO";
         lblTextblocksaldocontrato_saldo_Internalname = "TEXTBLOCKSALDOCONTRATO_SALDO";
         edtSaldoContrato_Saldo_Internalname = "SALDOCONTRATO_SALDO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Saldo Contrato";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Saldo Contrato";
         edtSaldoContrato_Saldo_Jsonclick = "";
         edtSaldoContrato_Saldo_Enabled = 0;
         edtSaldoContrato_Executado_Jsonclick = "";
         edtSaldoContrato_Executado_Enabled = 0;
         edtSaldoContrato_Estimado_Jsonclick = "";
         edtSaldoContrato_Estimado_Enabled = 0;
         edtSaldoContrato_Reservado_Jsonclick = "";
         edtSaldoContrato_Reservado_Enabled = 0;
         edtSaldoContrato_Credito_Jsonclick = "";
         edtSaldoContrato_Credito_Enabled = 0;
         edtSaldoContrato_VigenciaFim_Jsonclick = "";
         edtSaldoContrato_VigenciaFim_Enabled = 0;
         edtSaldoContrato_VigenciaInicio_Jsonclick = "";
         edtSaldoContrato_VigenciaInicio_Enabled = 0;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Enabled = 0;
         edtSaldoContrato_Codigo_Jsonclick = "";
         edtSaldoContrato_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Contrato_codigo( int GX_Parm1 )
      {
         A74Contrato_Codigo = GX_Parm1;
         /* Using cursor T003Z18 */
         pr_default.execute(16, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(16);
         if ( (0==A74Contrato_Codigo) )
         {
            GX_msglist.addItem("Contrato � obrigat�rio.", 1, "CONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContrato_Codigo_Internalname;
         }
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7SaldoContrato_Codigo',fld:'vSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123Z2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         Z1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocksaldocontrato_codigo_Jsonclick = "";
         lblTextblockcontrato_codigo_Jsonclick = "";
         lblTextblocksaldocontrato_vigenciainicio_Jsonclick = "";
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         lblTextblocksaldocontrato_vigenciafim_Jsonclick = "";
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         lblTextblocksaldocontrato_credito_Jsonclick = "";
         lblTextblocksaldocontrato_reservado_Jsonclick = "";
         lblTextblocksaldocontrato_estimado_Jsonclick = "";
         lblTextblocksaldocontrato_executado_Jsonclick = "";
         lblTextblocksaldocontrato_saldo_Jsonclick = "";
         A1784SaldoContrato_UnidadeMedicao_Nome = "";
         A1785SaldoContrato_UnidadeMedicao_Sigla = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode179 = "";
         GX_FocusControl = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1784SaldoContrato_UnidadeMedicao_Nome = "";
         Z1785SaldoContrato_UnidadeMedicao_Sigla = "";
         T003Z5_A1784SaldoContrato_UnidadeMedicao_Nome = new String[] {""} ;
         T003Z5_n1784SaldoContrato_UnidadeMedicao_Nome = new bool[] {false} ;
         T003Z5_A1785SaldoContrato_UnidadeMedicao_Sigla = new String[] {""} ;
         T003Z5_n1785SaldoContrato_UnidadeMedicao_Sigla = new bool[] {false} ;
         T003Z6_A1561SaldoContrato_Codigo = new int[1] ;
         T003Z6_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T003Z6_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         T003Z6_A1573SaldoContrato_Credito = new decimal[1] ;
         T003Z6_A1574SaldoContrato_Reservado = new decimal[1] ;
         T003Z6_A1786SaldoContrato_Estimado = new decimal[1] ;
         T003Z6_A1575SaldoContrato_Executado = new decimal[1] ;
         T003Z6_A1576SaldoContrato_Saldo = new decimal[1] ;
         T003Z6_A1781SaldoContrato_Ativo = new bool[] {false} ;
         T003Z6_A1784SaldoContrato_UnidadeMedicao_Nome = new String[] {""} ;
         T003Z6_n1784SaldoContrato_UnidadeMedicao_Nome = new bool[] {false} ;
         T003Z6_A1785SaldoContrato_UnidadeMedicao_Sigla = new String[] {""} ;
         T003Z6_n1785SaldoContrato_UnidadeMedicao_Sigla = new bool[] {false} ;
         T003Z6_A74Contrato_Codigo = new int[1] ;
         T003Z6_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         T003Z4_A74Contrato_Codigo = new int[1] ;
         T003Z7_A74Contrato_Codigo = new int[1] ;
         T003Z8_A1784SaldoContrato_UnidadeMedicao_Nome = new String[] {""} ;
         T003Z8_n1784SaldoContrato_UnidadeMedicao_Nome = new bool[] {false} ;
         T003Z8_A1785SaldoContrato_UnidadeMedicao_Sigla = new String[] {""} ;
         T003Z8_n1785SaldoContrato_UnidadeMedicao_Sigla = new bool[] {false} ;
         T003Z9_A1561SaldoContrato_Codigo = new int[1] ;
         T003Z3_A1561SaldoContrato_Codigo = new int[1] ;
         T003Z3_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T003Z3_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         T003Z3_A1573SaldoContrato_Credito = new decimal[1] ;
         T003Z3_A1574SaldoContrato_Reservado = new decimal[1] ;
         T003Z3_A1786SaldoContrato_Estimado = new decimal[1] ;
         T003Z3_A1575SaldoContrato_Executado = new decimal[1] ;
         T003Z3_A1576SaldoContrato_Saldo = new decimal[1] ;
         T003Z3_A1781SaldoContrato_Ativo = new bool[] {false} ;
         T003Z3_A74Contrato_Codigo = new int[1] ;
         T003Z3_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         T003Z10_A1561SaldoContrato_Codigo = new int[1] ;
         T003Z11_A1561SaldoContrato_Codigo = new int[1] ;
         T003Z2_A1561SaldoContrato_Codigo = new int[1] ;
         T003Z2_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         T003Z2_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         T003Z2_A1573SaldoContrato_Credito = new decimal[1] ;
         T003Z2_A1574SaldoContrato_Reservado = new decimal[1] ;
         T003Z2_A1786SaldoContrato_Estimado = new decimal[1] ;
         T003Z2_A1575SaldoContrato_Executado = new decimal[1] ;
         T003Z2_A1576SaldoContrato_Saldo = new decimal[1] ;
         T003Z2_A1781SaldoContrato_Ativo = new bool[] {false} ;
         T003Z2_A74Contrato_Codigo = new int[1] ;
         T003Z2_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         T003Z12_A1561SaldoContrato_Codigo = new int[1] ;
         T003Z15_A1784SaldoContrato_UnidadeMedicao_Nome = new String[] {""} ;
         T003Z15_n1784SaldoContrato_UnidadeMedicao_Nome = new bool[] {false} ;
         T003Z15_A1785SaldoContrato_UnidadeMedicao_Sigla = new String[] {""} ;
         T003Z15_n1785SaldoContrato_UnidadeMedicao_Sigla = new bool[] {false} ;
         T003Z16_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Z17_A1561SaldoContrato_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T003Z18_A74Contrato_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.saldocontrato__default(),
            new Object[][] {
                new Object[] {
               T003Z2_A1561SaldoContrato_Codigo, T003Z2_A1571SaldoContrato_VigenciaInicio, T003Z2_A1572SaldoContrato_VigenciaFim, T003Z2_A1573SaldoContrato_Credito, T003Z2_A1574SaldoContrato_Reservado, T003Z2_A1786SaldoContrato_Estimado, T003Z2_A1575SaldoContrato_Executado, T003Z2_A1576SaldoContrato_Saldo, T003Z2_A1781SaldoContrato_Ativo, T003Z2_A74Contrato_Codigo,
               T003Z2_A1783SaldoContrato_UnidadeMedicao_Codigo
               }
               , new Object[] {
               T003Z3_A1561SaldoContrato_Codigo, T003Z3_A1571SaldoContrato_VigenciaInicio, T003Z3_A1572SaldoContrato_VigenciaFim, T003Z3_A1573SaldoContrato_Credito, T003Z3_A1574SaldoContrato_Reservado, T003Z3_A1786SaldoContrato_Estimado, T003Z3_A1575SaldoContrato_Executado, T003Z3_A1576SaldoContrato_Saldo, T003Z3_A1781SaldoContrato_Ativo, T003Z3_A74Contrato_Codigo,
               T003Z3_A1783SaldoContrato_UnidadeMedicao_Codigo
               }
               , new Object[] {
               T003Z4_A74Contrato_Codigo
               }
               , new Object[] {
               T003Z5_A1784SaldoContrato_UnidadeMedicao_Nome, T003Z5_n1784SaldoContrato_UnidadeMedicao_Nome, T003Z5_A1785SaldoContrato_UnidadeMedicao_Sigla, T003Z5_n1785SaldoContrato_UnidadeMedicao_Sigla
               }
               , new Object[] {
               T003Z6_A1561SaldoContrato_Codigo, T003Z6_A1571SaldoContrato_VigenciaInicio, T003Z6_A1572SaldoContrato_VigenciaFim, T003Z6_A1573SaldoContrato_Credito, T003Z6_A1574SaldoContrato_Reservado, T003Z6_A1786SaldoContrato_Estimado, T003Z6_A1575SaldoContrato_Executado, T003Z6_A1576SaldoContrato_Saldo, T003Z6_A1781SaldoContrato_Ativo, T003Z6_A1784SaldoContrato_UnidadeMedicao_Nome,
               T003Z6_n1784SaldoContrato_UnidadeMedicao_Nome, T003Z6_A1785SaldoContrato_UnidadeMedicao_Sigla, T003Z6_n1785SaldoContrato_UnidadeMedicao_Sigla, T003Z6_A74Contrato_Codigo, T003Z6_A1783SaldoContrato_UnidadeMedicao_Codigo
               }
               , new Object[] {
               T003Z7_A74Contrato_Codigo
               }
               , new Object[] {
               T003Z8_A1784SaldoContrato_UnidadeMedicao_Nome, T003Z8_n1784SaldoContrato_UnidadeMedicao_Nome, T003Z8_A1785SaldoContrato_UnidadeMedicao_Sigla, T003Z8_n1785SaldoContrato_UnidadeMedicao_Sigla
               }
               , new Object[] {
               T003Z9_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003Z10_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003Z11_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003Z12_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003Z15_A1784SaldoContrato_UnidadeMedicao_Nome, T003Z15_n1784SaldoContrato_UnidadeMedicao_Nome, T003Z15_A1785SaldoContrato_UnidadeMedicao_Sigla, T003Z15_n1785SaldoContrato_UnidadeMedicao_Sigla
               }
               , new Object[] {
               T003Z16_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               T003Z17_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003Z18_A74Contrato_Codigo
               }
            }
         );
         AV14Pgmname = "SaldoContrato";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound179 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7SaldoContrato_Codigo ;
      private int Z1561SaldoContrato_Codigo ;
      private int Z74Contrato_Codigo ;
      private int Z1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int N1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int AV7SaldoContrato_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A1561SaldoContrato_Codigo ;
      private int edtSaldoContrato_Codigo_Enabled ;
      private int edtContrato_Codigo_Enabled ;
      private int edtSaldoContrato_VigenciaInicio_Enabled ;
      private int edtSaldoContrato_VigenciaFim_Enabled ;
      private int edtSaldoContrato_Credito_Enabled ;
      private int edtSaldoContrato_Reservado_Enabled ;
      private int edtSaldoContrato_Estimado_Enabled ;
      private int edtSaldoContrato_Executado_Enabled ;
      private int edtSaldoContrato_Saldo_Enabled ;
      private int AV11Insert_Contrato_Codigo ;
      private int AV13Insert_SaldoContrato_UnidadeMedicao_Codigo ;
      private int AV15GXV1 ;
      private int idxLst ;
      private decimal Z1573SaldoContrato_Credito ;
      private decimal Z1574SaldoContrato_Reservado ;
      private decimal Z1786SaldoContrato_Estimado ;
      private decimal Z1575SaldoContrato_Executado ;
      private decimal Z1576SaldoContrato_Saldo ;
      private decimal A1573SaldoContrato_Credito ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1786SaldoContrato_Estimado ;
      private decimal A1575SaldoContrato_Executado ;
      private decimal A1576SaldoContrato_Saldo ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksaldocontrato_codigo_Internalname ;
      private String lblTextblocksaldocontrato_codigo_Jsonclick ;
      private String edtSaldoContrato_Codigo_Internalname ;
      private String edtSaldoContrato_Codigo_Jsonclick ;
      private String lblTextblockcontrato_codigo_Internalname ;
      private String lblTextblockcontrato_codigo_Jsonclick ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String lblTextblocksaldocontrato_vigenciainicio_Internalname ;
      private String lblTextblocksaldocontrato_vigenciainicio_Jsonclick ;
      private String edtSaldoContrato_VigenciaInicio_Internalname ;
      private String edtSaldoContrato_VigenciaInicio_Jsonclick ;
      private String lblTextblocksaldocontrato_vigenciafim_Internalname ;
      private String lblTextblocksaldocontrato_vigenciafim_Jsonclick ;
      private String edtSaldoContrato_VigenciaFim_Internalname ;
      private String edtSaldoContrato_VigenciaFim_Jsonclick ;
      private String lblTextblocksaldocontrato_credito_Internalname ;
      private String lblTextblocksaldocontrato_credito_Jsonclick ;
      private String edtSaldoContrato_Credito_Internalname ;
      private String edtSaldoContrato_Credito_Jsonclick ;
      private String lblTextblocksaldocontrato_reservado_Internalname ;
      private String lblTextblocksaldocontrato_reservado_Jsonclick ;
      private String edtSaldoContrato_Reservado_Internalname ;
      private String edtSaldoContrato_Reservado_Jsonclick ;
      private String lblTextblocksaldocontrato_estimado_Internalname ;
      private String lblTextblocksaldocontrato_estimado_Jsonclick ;
      private String edtSaldoContrato_Estimado_Internalname ;
      private String edtSaldoContrato_Estimado_Jsonclick ;
      private String lblTextblocksaldocontrato_executado_Internalname ;
      private String lblTextblocksaldocontrato_executado_Jsonclick ;
      private String edtSaldoContrato_Executado_Internalname ;
      private String edtSaldoContrato_Executado_Jsonclick ;
      private String lblTextblocksaldocontrato_saldo_Internalname ;
      private String lblTextblocksaldocontrato_saldo_Jsonclick ;
      private String edtSaldoContrato_Saldo_Internalname ;
      private String edtSaldoContrato_Saldo_Jsonclick ;
      private String A1784SaldoContrato_UnidadeMedicao_Nome ;
      private String A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode179 ;
      private String GX_FocusControl ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1784SaldoContrato_UnidadeMedicao_Nome ;
      private String Z1785SaldoContrato_UnidadeMedicao_Sigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z1571SaldoContrato_VigenciaInicio ;
      private DateTime Z1572SaldoContrato_VigenciaFim ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private bool Z1781SaldoContrato_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1781SaldoContrato_Ativo ;
      private bool n1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T003Z5_A1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool[] T003Z5_n1784SaldoContrato_UnidadeMedicao_Nome ;
      private String[] T003Z5_A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] T003Z5_n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private int[] T003Z6_A1561SaldoContrato_Codigo ;
      private DateTime[] T003Z6_A1571SaldoContrato_VigenciaInicio ;
      private DateTime[] T003Z6_A1572SaldoContrato_VigenciaFim ;
      private decimal[] T003Z6_A1573SaldoContrato_Credito ;
      private decimal[] T003Z6_A1574SaldoContrato_Reservado ;
      private decimal[] T003Z6_A1786SaldoContrato_Estimado ;
      private decimal[] T003Z6_A1575SaldoContrato_Executado ;
      private decimal[] T003Z6_A1576SaldoContrato_Saldo ;
      private bool[] T003Z6_A1781SaldoContrato_Ativo ;
      private String[] T003Z6_A1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool[] T003Z6_n1784SaldoContrato_UnidadeMedicao_Nome ;
      private String[] T003Z6_A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] T003Z6_n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private int[] T003Z6_A74Contrato_Codigo ;
      private int[] T003Z6_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] T003Z4_A74Contrato_Codigo ;
      private int[] T003Z7_A74Contrato_Codigo ;
      private String[] T003Z8_A1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool[] T003Z8_n1784SaldoContrato_UnidadeMedicao_Nome ;
      private String[] T003Z8_A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] T003Z8_n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private int[] T003Z9_A1561SaldoContrato_Codigo ;
      private int[] T003Z3_A1561SaldoContrato_Codigo ;
      private DateTime[] T003Z3_A1571SaldoContrato_VigenciaInicio ;
      private DateTime[] T003Z3_A1572SaldoContrato_VigenciaFim ;
      private decimal[] T003Z3_A1573SaldoContrato_Credito ;
      private decimal[] T003Z3_A1574SaldoContrato_Reservado ;
      private decimal[] T003Z3_A1786SaldoContrato_Estimado ;
      private decimal[] T003Z3_A1575SaldoContrato_Executado ;
      private decimal[] T003Z3_A1576SaldoContrato_Saldo ;
      private bool[] T003Z3_A1781SaldoContrato_Ativo ;
      private int[] T003Z3_A74Contrato_Codigo ;
      private int[] T003Z3_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] T003Z10_A1561SaldoContrato_Codigo ;
      private int[] T003Z11_A1561SaldoContrato_Codigo ;
      private int[] T003Z2_A1561SaldoContrato_Codigo ;
      private DateTime[] T003Z2_A1571SaldoContrato_VigenciaInicio ;
      private DateTime[] T003Z2_A1572SaldoContrato_VigenciaFim ;
      private decimal[] T003Z2_A1573SaldoContrato_Credito ;
      private decimal[] T003Z2_A1574SaldoContrato_Reservado ;
      private decimal[] T003Z2_A1786SaldoContrato_Estimado ;
      private decimal[] T003Z2_A1575SaldoContrato_Executado ;
      private decimal[] T003Z2_A1576SaldoContrato_Saldo ;
      private bool[] T003Z2_A1781SaldoContrato_Ativo ;
      private int[] T003Z2_A74Contrato_Codigo ;
      private int[] T003Z2_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] T003Z12_A1561SaldoContrato_Codigo ;
      private String[] T003Z15_A1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool[] T003Z15_n1784SaldoContrato_UnidadeMedicao_Nome ;
      private String[] T003Z15_A1785SaldoContrato_UnidadeMedicao_Sigla ;
      private bool[] T003Z15_n1785SaldoContrato_UnidadeMedicao_Sigla ;
      private int[] T003Z16_A1560NotaEmpenho_Codigo ;
      private int[] T003Z17_A1561SaldoContrato_Codigo ;
      private int[] T003Z18_A74Contrato_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class saldocontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003Z6 ;
          prmT003Z6 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z4 ;
          prmT003Z4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z5 ;
          prmT003Z5 = new Object[] {
          new Object[] {"@SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z7 ;
          prmT003Z7 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z8 ;
          prmT003Z8 = new Object[] {
          new Object[] {"@SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z9 ;
          prmT003Z9 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z3 ;
          prmT003Z3 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z10 ;
          prmT003Z10 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z11 ;
          prmT003Z11 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z2 ;
          prmT003Z2 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z12 ;
          prmT003Z12 = new Object[] {
          new Object[] {"@SaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Estimado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z13 ;
          prmT003Z13 = new Object[] {
          new Object[] {"@SaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Estimado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z14 ;
          prmT003Z14 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z15 ;
          prmT003Z15 = new Object[] {
          new Object[] {"@SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z16 ;
          prmT003Z16 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Z17 ;
          prmT003Z17 = new Object[] {
          } ;
          Object[] prmT003Z18 ;
          prmT003Z18 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003Z2", "SELECT [SaldoContrato_Codigo], [SaldoContrato_VigenciaInicio], [SaldoContrato_VigenciaFim], [SaldoContrato_Credito], [SaldoContrato_Reservado], [SaldoContrato_Estimado], [SaldoContrato_Executado], [SaldoContrato_Saldo], [SaldoContrato_Ativo], [Contrato_Codigo], [SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo FROM [SaldoContrato] WITH (UPDLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z2,1,0,true,false )
             ,new CursorDef("T003Z3", "SELECT [SaldoContrato_Codigo], [SaldoContrato_VigenciaInicio], [SaldoContrato_VigenciaFim], [SaldoContrato_Credito], [SaldoContrato_Reservado], [SaldoContrato_Estimado], [SaldoContrato_Executado], [SaldoContrato_Saldo], [SaldoContrato_Ativo], [Contrato_Codigo], [SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z3,1,0,true,false )
             ,new CursorDef("T003Z4", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z4,1,0,true,false )
             ,new CursorDef("T003Z5", "SELECT [UnidadeMedicao_Nome] AS SaldoContrato_UnidadeMedicao_Nome, [UnidadeMedicao_Sigla] AS SaldoContrato_UnidadeMedicao_Sigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @SaldoContrato_UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z5,1,0,true,false )
             ,new CursorDef("T003Z6", "SELECT TM1.[SaldoContrato_Codigo], TM1.[SaldoContrato_VigenciaInicio], TM1.[SaldoContrato_VigenciaFim], TM1.[SaldoContrato_Credito], TM1.[SaldoContrato_Reservado], TM1.[SaldoContrato_Estimado], TM1.[SaldoContrato_Executado], TM1.[SaldoContrato_Saldo], TM1.[SaldoContrato_Ativo], T2.[UnidadeMedicao_Nome] AS SaldoContrato_UnidadeMedicao_Nome, T2.[UnidadeMedicao_Sigla] AS SaldoContrato_UnidadeMedicao_Sigla, TM1.[Contrato_Codigo], TM1.[SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo FROM ([SaldoContrato] TM1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = TM1.[SaldoContrato_UnidadeMedicao_Codigo]) WHERE TM1.[SaldoContrato_Codigo] = @SaldoContrato_Codigo ORDER BY TM1.[SaldoContrato_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z6,100,0,true,false )
             ,new CursorDef("T003Z7", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z7,1,0,true,false )
             ,new CursorDef("T003Z8", "SELECT [UnidadeMedicao_Nome] AS SaldoContrato_UnidadeMedicao_Nome, [UnidadeMedicao_Sigla] AS SaldoContrato_UnidadeMedicao_Sigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @SaldoContrato_UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z8,1,0,true,false )
             ,new CursorDef("T003Z9", "SELECT [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z9,1,0,true,false )
             ,new CursorDef("T003Z10", "SELECT TOP 1 [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE ( [SaldoContrato_Codigo] > @SaldoContrato_Codigo) ORDER BY [SaldoContrato_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z10,1,0,true,true )
             ,new CursorDef("T003Z11", "SELECT TOP 1 [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE ( [SaldoContrato_Codigo] < @SaldoContrato_Codigo) ORDER BY [SaldoContrato_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z11,1,0,true,true )
             ,new CursorDef("T003Z12", "INSERT INTO [SaldoContrato]([SaldoContrato_VigenciaInicio], [SaldoContrato_VigenciaFim], [SaldoContrato_Credito], [SaldoContrato_Reservado], [SaldoContrato_Estimado], [SaldoContrato_Executado], [SaldoContrato_Saldo], [SaldoContrato_Ativo], [Contrato_Codigo], [SaldoContrato_UnidadeMedicao_Codigo]) VALUES(@SaldoContrato_VigenciaInicio, @SaldoContrato_VigenciaFim, @SaldoContrato_Credito, @SaldoContrato_Reservado, @SaldoContrato_Estimado, @SaldoContrato_Executado, @SaldoContrato_Saldo, @SaldoContrato_Ativo, @Contrato_Codigo, @SaldoContrato_UnidadeMedicao_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003Z12)
             ,new CursorDef("T003Z13", "UPDATE [SaldoContrato] SET [SaldoContrato_VigenciaInicio]=@SaldoContrato_VigenciaInicio, [SaldoContrato_VigenciaFim]=@SaldoContrato_VigenciaFim, [SaldoContrato_Credito]=@SaldoContrato_Credito, [SaldoContrato_Reservado]=@SaldoContrato_Reservado, [SaldoContrato_Estimado]=@SaldoContrato_Estimado, [SaldoContrato_Executado]=@SaldoContrato_Executado, [SaldoContrato_Saldo]=@SaldoContrato_Saldo, [SaldoContrato_Ativo]=@SaldoContrato_Ativo, [Contrato_Codigo]=@Contrato_Codigo, [SaldoContrato_UnidadeMedicao_Codigo]=@SaldoContrato_UnidadeMedicao_Codigo  WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo", GxErrorMask.GX_NOMASK,prmT003Z13)
             ,new CursorDef("T003Z14", "DELETE FROM [SaldoContrato]  WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo", GxErrorMask.GX_NOMASK,prmT003Z14)
             ,new CursorDef("T003Z15", "SELECT [UnidadeMedicao_Nome] AS SaldoContrato_UnidadeMedicao_Nome, [UnidadeMedicao_Sigla] AS SaldoContrato_UnidadeMedicao_Sigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @SaldoContrato_UnidadeMedicao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z15,1,0,true,false )
             ,new CursorDef("T003Z16", "SELECT TOP 1 [NotaEmpenho_Codigo] FROM [NotaEmpenho] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z16,1,0,true,true )
             ,new CursorDef("T003Z17", "SELECT [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) ORDER BY [SaldoContrato_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z17,100,0,true,false )
             ,new CursorDef("T003Z18", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Z18,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[8])[0] = rslt.getBool(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                ((int[]) buf[10])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[8])[0] = rslt.getBool(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                ((int[]) buf[10])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[8])[0] = rslt.getBool(9) ;
                ((String[]) buf[9])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                ((String[]) buf[11])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(11);
                ((int[]) buf[13])[0] = rslt.getInt(12) ;
                ((int[]) buf[14])[0] = rslt.getInt(13) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (bool)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                stmt.SetParameter(10, (int)parms[9]);
                return;
             case 11 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (bool)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                stmt.SetParameter(10, (int)parms[9]);
                stmt.SetParameter(11, (int)parms[10]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
