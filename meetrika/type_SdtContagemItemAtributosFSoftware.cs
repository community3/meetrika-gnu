/*
               File: type_SdtContagemItemAtributosFSoftware
        Description: Contagem Item Atributos FSoftware
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:17.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemItemAtributosFSoftware" )]
   [XmlType(TypeName =  "ContagemItemAtributosFSoftware" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContagemItemAtributosFSoftware : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemItemAtributosFSoftware( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom = "";
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom = "";
         gxTv_SdtContagemItemAtributosFSoftware_Mode = "";
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z = "";
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z = "";
      }

      public SdtContagemItemAtributosFSoftware( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV224ContagemItem_Lancamento ,
                        int AV379ContagemItem_AtributosCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV224ContagemItem_Lancamento,(int)AV379ContagemItem_AtributosCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemItem_Lancamento", typeof(int)}, new Object[]{"ContagemItem_AtributosCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemItemAtributosFSoftware");
         metadata.Set("BT", "ContagemItemAtributosFSoftware");
         metadata.Set("PK", "[ \"ContagemItem_Lancamento\",\"ContagemItem_AtributosCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemItem_AtributosCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Atributos_Codigo\" ],\"FKMap\":[ \"ContagemItem_AtributosCod-Atributos_Codigo\" ] },{ \"FK\":[ \"ContagemItem_Lancamento\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_lancamento_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_atributoscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_atributosnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_atrtabelacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_atrtabelanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_atributosnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_atrtabelacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemitem_atrtabelanom_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemItemAtributosFSoftware deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemItemAtributosFSoftware)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemItemAtributosFSoftware obj ;
         obj = this;
         obj.gxTpr_Contagemitem_lancamento = deserialized.gxTpr_Contagemitem_lancamento;
         obj.gxTpr_Contagemitem_atributoscod = deserialized.gxTpr_Contagemitem_atributoscod;
         obj.gxTpr_Contagemitem_atributosnom = deserialized.gxTpr_Contagemitem_atributosnom;
         obj.gxTpr_Contagemitem_atrtabelacod = deserialized.gxTpr_Contagemitem_atrtabelacod;
         obj.gxTpr_Contagemitem_atrtabelanom = deserialized.gxTpr_Contagemitem_atrtabelanom;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemitem_lancamento_Z = deserialized.gxTpr_Contagemitem_lancamento_Z;
         obj.gxTpr_Contagemitem_atributoscod_Z = deserialized.gxTpr_Contagemitem_atributoscod_Z;
         obj.gxTpr_Contagemitem_atributosnom_Z = deserialized.gxTpr_Contagemitem_atributosnom_Z;
         obj.gxTpr_Contagemitem_atrtabelacod_Z = deserialized.gxTpr_Contagemitem_atrtabelacod_Z;
         obj.gxTpr_Contagemitem_atrtabelanom_Z = deserialized.gxTpr_Contagemitem_atrtabelanom_Z;
         obj.gxTpr_Contagemitem_atributosnom_N = deserialized.gxTpr_Contagemitem_atributosnom_N;
         obj.gxTpr_Contagemitem_atrtabelacod_N = deserialized.gxTpr_Contagemitem_atrtabelacod_N;
         obj.gxTpr_Contagemitem_atrtabelanom_N = deserialized.gxTpr_Contagemitem_atrtabelanom_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_Lancamento") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtributosCod") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtributosNom") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtrTabelaCod") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtrTabelaNom") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_Lancamento_Z") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtributosCod_Z") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtributosNom_Z") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtrTabelaCod_Z") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtrTabelaNom_Z") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtributosNom_N") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtrTabelaCod_N") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemItem_AtrTabelaNom_N") )
               {
                  gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemItemAtributosFSoftware";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemItem_Lancamento", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemItem_AtributosCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemItem_AtributosNom", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemItem_AtrTabelaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemItem_AtrTabelaNom", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFSoftware_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_Lancamento_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_AtributosCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_AtributosNom_Z", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_AtrTabelaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_AtrTabelaNom_Z", StringUtil.RTrim( gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_AtributosNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_AtrTabelaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemItem_AtrTabelaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemItem_Lancamento", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento, false);
         AddObjectProperty("ContagemItem_AtributosCod", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod, false);
         AddObjectProperty("ContagemItem_AtributosNom", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom, false);
         AddObjectProperty("ContagemItem_AtrTabelaCod", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod, false);
         AddObjectProperty("ContagemItem_AtrTabelaNom", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemItemAtributosFSoftware_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemItemAtributosFSoftware_Initialized, false);
            AddObjectProperty("ContagemItem_Lancamento_Z", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z, false);
            AddObjectProperty("ContagemItem_AtributosCod_Z", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z, false);
            AddObjectProperty("ContagemItem_AtributosNom_Z", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z, false);
            AddObjectProperty("ContagemItem_AtrTabelaCod_Z", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z, false);
            AddObjectProperty("ContagemItem_AtrTabelaNom_Z", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z, false);
            AddObjectProperty("ContagemItem_AtributosNom_N", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N, false);
            AddObjectProperty("ContagemItem_AtrTabelaCod_N", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N, false);
            AddObjectProperty("ContagemItem_AtrTabelaNom_N", gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemItem_Lancamento" )]
      [  XmlElement( ElementName = "ContagemItem_Lancamento"   )]
      public int gxTpr_Contagemitem_lancamento
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento ;
         }

         set {
            if ( gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento != value )
            {
               gxTv_SdtContagemItemAtributosFSoftware_Mode = "INS";
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z_SetNull( );
            }
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemItem_AtributosCod" )]
      [  XmlElement( ElementName = "ContagemItem_AtributosCod"   )]
      public int gxTpr_Contagemitem_atributoscod
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod ;
         }

         set {
            if ( gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod != value )
            {
               gxTv_SdtContagemItemAtributosFSoftware_Mode = "INS";
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z_SetNull( );
               this.gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z_SetNull( );
            }
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemItem_AtributosNom" )]
      [  XmlElement( ElementName = "ContagemItem_AtributosNom"   )]
      public String gxTpr_Contagemitem_atributosnom
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N = 0;
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N = 1;
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtrTabelaCod" )]
      [  XmlElement( ElementName = "ContagemItem_AtrTabelaCod"   )]
      public int gxTpr_Contagemitem_atrtabelacod
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N = 0;
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N = 1;
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtrTabelaNom" )]
      [  XmlElement( ElementName = "ContagemItem_AtrTabelaNom"   )]
      public String gxTpr_Contagemitem_atrtabelanom
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N = 0;
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N = 1;
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Mode ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Mode_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Initialized ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Initialized_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_Lancamento_Z" )]
      [  XmlElement( ElementName = "ContagemItem_Lancamento_Z"   )]
      public int gxTpr_Contagemitem_lancamento_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtributosCod_Z" )]
      [  XmlElement( ElementName = "ContagemItem_AtributosCod_Z"   )]
      public int gxTpr_Contagemitem_atributoscod_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtributosNom_Z" )]
      [  XmlElement( ElementName = "ContagemItem_AtributosNom_Z"   )]
      public String gxTpr_Contagemitem_atributosnom_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtrTabelaCod_Z" )]
      [  XmlElement( ElementName = "ContagemItem_AtrTabelaCod_Z"   )]
      public int gxTpr_Contagemitem_atrtabelacod_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtrTabelaNom_Z" )]
      [  XmlElement( ElementName = "ContagemItem_AtrTabelaNom_Z"   )]
      public String gxTpr_Contagemitem_atrtabelanom_Z
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtributosNom_N" )]
      [  XmlElement( ElementName = "ContagemItem_AtributosNom_N"   )]
      public short gxTpr_Contagemitem_atributosnom_N
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtrTabelaCod_N" )]
      [  XmlElement( ElementName = "ContagemItem_AtrTabelaCod_N"   )]
      public short gxTpr_Contagemitem_atrtabelacod_N
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemItem_AtrTabelaNom_N" )]
      [  XmlElement( ElementName = "ContagemItem_AtrTabelaNom_N"   )]
      public short gxTpr_Contagemitem_atrtabelanom_N
      {
         get {
            return gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N ;
         }

         set {
            gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N_SetNull( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom = "";
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom = "";
         gxTv_SdtContagemItemAtributosFSoftware_Mode = "";
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z = "";
         gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemitematributosfsoftware", "GeneXus.Programs.contagemitematributosfsoftware_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemItemAtributosFSoftware_Initialized ;
      private short gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_N ;
      private short gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_N ;
      private short gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento ;
      private int gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod ;
      private int gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod ;
      private int gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_lancamento_Z ;
      private int gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributoscod_Z ;
      private int gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelacod_Z ;
      private String gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom ;
      private String gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom ;
      private String gxTv_SdtContagemItemAtributosFSoftware_Mode ;
      private String gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atributosnom_Z ;
      private String gxTv_SdtContagemItemAtributosFSoftware_Contagemitem_atrtabelanom_Z ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemItemAtributosFSoftware", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContagemItemAtributosFSoftware_RESTInterface : GxGenericCollectionItem<SdtContagemItemAtributosFSoftware>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemItemAtributosFSoftware_RESTInterface( ) : base()
      {
      }

      public SdtContagemItemAtributosFSoftware_RESTInterface( SdtContagemItemAtributosFSoftware psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemItem_Lancamento" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemitem_lancamento
      {
         get {
            return sdt.gxTpr_Contagemitem_lancamento ;
         }

         set {
            sdt.gxTpr_Contagemitem_lancamento = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemItem_AtributosCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemitem_atributoscod
      {
         get {
            return sdt.gxTpr_Contagemitem_atributoscod ;
         }

         set {
            sdt.gxTpr_Contagemitem_atributoscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemItem_AtributosNom" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemitem_atributosnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemitem_atributosnom) ;
         }

         set {
            sdt.gxTpr_Contagemitem_atributosnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemItem_AtrTabelaCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemitem_atrtabelacod
      {
         get {
            return sdt.gxTpr_Contagemitem_atrtabelacod ;
         }

         set {
            sdt.gxTpr_Contagemitem_atrtabelacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemItem_AtrTabelaNom" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Contagemitem_atrtabelanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemitem_atrtabelanom) ;
         }

         set {
            sdt.gxTpr_Contagemitem_atrtabelanom = (String)(value);
         }

      }

      public SdtContagemItemAtributosFSoftware sdt
      {
         get {
            return (SdtContagemItemAtributosFSoftware)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemItemAtributosFSoftware() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 15 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
