/*
               File: PRC_MaxPrioridadeOrdem
        Description: Max Prioridade Ordem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:42.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_maxprioridadeordem : GXProcedure
   {
      public prc_maxprioridadeordem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_maxprioridadeordem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           out short aP1_Ordem )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV9Ordem = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Ordem=this.AV9Ordem;
      }

      public short executeUdp( ref int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV9Ordem = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Ordem=this.AV9Ordem;
         return AV9Ordem ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 out short aP1_Ordem )
      {
         prc_maxprioridadeordem objprc_maxprioridadeordem;
         objprc_maxprioridadeordem = new prc_maxprioridadeordem();
         objprc_maxprioridadeordem.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_maxprioridadeordem.AV9Ordem = 0 ;
         objprc_maxprioridadeordem.context.SetSubmitInitialConfig(context);
         objprc_maxprioridadeordem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_maxprioridadeordem);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_Ordem=this.AV9Ordem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_maxprioridadeordem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00X43 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A40000GXC1 = P00X43_A40000GXC1[0];
            n40000GXC1 = P00X43_n40000GXC1[0];
            A40000GXC1 = P00X43_A40000GXC1[0];
            n40000GXC1 = P00X43_n40000GXC1[0];
            AV9Ordem = A40000GXC1;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X43_A160ContratoServicos_Codigo = new int[1] ;
         P00X43_A40000GXC1 = new short[1] ;
         P00X43_n40000GXC1 = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_maxprioridadeordem__default(),
            new Object[][] {
                new Object[] {
               P00X43_A160ContratoServicos_Codigo, P00X43_A40000GXC1, P00X43_n40000GXC1
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Ordem ;
      private short A40000GXC1 ;
      private int A160ContratoServicos_Codigo ;
      private String scmdbuf ;
      private bool n40000GXC1 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00X43_A160ContratoServicos_Codigo ;
      private short[] P00X43_A40000GXC1 ;
      private bool[] P00X43_n40000GXC1 ;
      private short aP1_Ordem ;
   }

   public class prc_maxprioridadeordem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X43 ;
          prmP00X43 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X43", "SELECT T1.[ContratoServicos_Codigo], COALESCE( T2.[GXC1], 0) AS GXC1 FROM ([ContratoServicos] T1 WITH (NOLOCK) LEFT JOIN (SELECT MAX([ContratoServicosPrioridade_Ordem]) AS GXC1, [ContratoServicosPrioridade_CntSrvCod] FROM [ContratoServicosPrioridade] WITH (NOLOCK) GROUP BY [ContratoServicosPrioridade_CntSrvCod] ) T2 ON T2.[ContratoServicosPrioridade_CntSrvCod] = T1.[ContratoServicos_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X43,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
