/*
               File: GAMExampleSetPassword
        Description: Set new password
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:31:57.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexamplesetpassword : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public gamexamplesetpassword( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexamplesetpassword( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_UserId )
      {
         this.AV8UserId = aP0_UserId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV8UserId = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UserId", AV8UserId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8UserId, ""))));
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA252( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavUsername_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Enabled), 5, 0)));
               WS252( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE252( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Set new password ") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117315775");
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexamplesetpassword.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV8UserId))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.RTrim( AV8UserId));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm252( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleSetPassword" ;
      }

      public override String GetPgmdesc( )
      {
         return "Set new password " ;
      }

      protected void WB250( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_252( true) ;
         }
         else
         {
            wb_table1_2_252( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_252e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_7_252( true) ;
         }
         else
         {
            wb_table2_7_252( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_252e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START252( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Set new password ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP250( ) ;
      }

      protected void WS252( )
      {
         START252( ) ;
         EVT252( ) ;
      }

      protected void EVT252( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11252 */
                           E11252 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E12252 */
                                 E12252 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13252 */
                           E13252 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE252( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm252( ) ;
            }
         }
      }

      protected void PA252( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavUsername_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF252( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsername_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Enabled), 5, 0)));
      }

      protected void RF252( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13252 */
            E13252 ();
            WB250( ) ;
         }
      }

      protected void STRUP250( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavUsername_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11252 */
         E11252 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV9UserName = cgiGet( edtavUsername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9UserName", AV9UserName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9UserName, ""))));
            AV10UserPasswordNew = cgiGet( edtavUserpasswordnew_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10UserPasswordNew", AV10UserPasswordNew);
            AV11UserPasswordNewConf = cgiGet( edtavUserpasswordnewconf_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11UserPasswordNewConf", AV11UserPasswordNewConf);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11252 */
         E11252 ();
         if (returnInSub) return;
      }

      protected void E11252( )
      {
         /* Start Routine */
         AV7User.load( AV8UserId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UserId", AV8UserId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8UserId, ""))));
         if ( ! AV7User.success() )
         {
            GX_msglist.addItem("User not found.");
            tblTblmain_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblmain_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblmain_Visible), 5, 0)));
         }
         else
         {
            tblTblmain_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblmain_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblmain_Visible), 5, 0)));
            AV9UserName = AV7User.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9UserName", AV9UserName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9UserName, ""))));
            AV10UserPasswordNew = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10UserPasswordNew", AV10UserPasswordNew);
            AV11UserPasswordNewConf = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11UserPasswordNewConf", AV11UserPasswordNewConf);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12252 */
         E12252 ();
         if (returnInSub) return;
      }

      protected void E12252( )
      {
         /* Enter Routine */
         if ( StringUtil.StrCmp(AV10UserPasswordNew, AV11UserPasswordNewConf) == 0 )
         {
            if ( AV7User.success() )
            {
               AV13IsChanged = AV7User.setpassword(AV10UserPasswordNew, out  AV6Errors);
               if ( AV13IsChanged )
               {
                  context.CommitDataStores( "GAMExampleSetPassword");
                  edtavUserpasswordnew_Enabled = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUserpasswordnew_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUserpasswordnew_Enabled), 5, 0)));
                  edtavUserpasswordnewconf_Enabled = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUserpasswordnewconf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUserpasswordnewconf_Enabled), 5, 0)));
                  bttBtnconfirm_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
                  GX_msglist.addItem("Password was changed successfully.");
               }
               else
               {
                  /* Execute user subroutine: 'DISPLAYMESSAGES' */
                  S112 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               AV6Errors = AV7User.geterrors();
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S112 ();
               if (returnInSub) return;
            }
         }
         else
         {
            GX_msglist.addItem("The new password and confirmation do not match.");
         }
      }

      protected void S112( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV16GXV1 = 1;
         while ( AV16GXV1 <= AV6Errors.Count )
         {
            AV5Error = ((SdtGAMError)AV6Errors.Item(AV16GXV1));
            GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV5Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            AV16GXV1 = (int)(AV16GXV1+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E13252( )
      {
         /* Load Routine */
      }

      protected void wb_table2_7_252( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblmain_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(450), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblmain_Internalname, tblTblmain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuserlogin_Internalname, "User login", "", "", lblTbuserlogin_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleSetPassword.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, AV9UserName, StringUtil.RTrim( context.localUtil.Format( AV9UserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsername_Jsonclick, 0, "Attribute", "", "", "", 1, edtavUsername_Enabled, 0, "text", "", 50, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleSetPassword.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnewpwd_Internalname, "New password", "", "", lblTbnewpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleSetPassword.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserpasswordnew_Internalname, StringUtil.RTrim( AV10UserPasswordNew), StringUtil.RTrim( context.localUtil.Format( AV10UserPasswordNew, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserpasswordnew_Jsonclick, 0, "Attribute", "", "", "", 1, edtavUserpasswordnew_Enabled, 1, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleSetPassword.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconfpwd_Internalname, "Confirm password", "", "", lblTbconfpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleSetPassword.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserpasswordnewconf_Internalname, StringUtil.RTrim( AV11UserPasswordNewConf), StringUtil.RTrim( context.localUtil.Format( AV11UserPasswordNewConf, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserpasswordnewconf_Jsonclick, 0, "Attribute", "", "", "", 1, edtavUserpasswordnewconf_Enabled, 1, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleSetPassword.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_252( true) ;
         }
         else
         {
            wb_table3_29_252( false) ;
         }
         return  ;
      }

      protected void wb_table3_29_252e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_252e( true) ;
         }
         else
         {
            wb_table2_7_252e( false) ;
         }
      }

      protected void wb_table3_29_252( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", "Confirmar", bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleSetPassword.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:2px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_252e( true) ;
         }
         else
         {
            wb_table3_29_252e( false) ;
         }
      }

      protected void wb_table1_2_252( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(450), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblmsg_Internalname, tblTblmsg_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:50px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_252e( true) ;
         }
         else
         {
            wb_table1_2_252e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8UserId = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8UserId", AV8UserId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8UserId, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA252( ) ;
         WS252( ) ;
         WE252( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311731582");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexamplesetpassword.js", "?2020311731582");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         tblTblmsg_Internalname = "TBLMSG";
         lblTbuserlogin_Internalname = "TBUSERLOGIN";
         edtavUsername_Internalname = "vUSERNAME";
         lblTbnewpwd_Internalname = "TBNEWPWD";
         edtavUserpasswordnew_Internalname = "vUSERPASSWORDNEW";
         lblTbconfpwd_Internalname = "TBCONFPWD";
         edtavUserpasswordnewconf_Internalname = "vUSERPASSWORDNEWCONF";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         tblTblmain_Internalname = "TBLMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         bttBtnconfirm_Visible = 1;
         edtavUserpasswordnewconf_Jsonclick = "";
         edtavUserpasswordnew_Jsonclick = "";
         edtavUsername_Jsonclick = "";
         edtavUsername_Enabled = 1;
         edtavUserpasswordnewconf_Enabled = 1;
         edtavUserpasswordnew_Enabled = 1;
         tblTblmain_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8UserId = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9UserName = "";
         AV10UserPasswordNew = "";
         AV11UserPasswordNewConf = "";
         AV7User = new SdtGAMUser(context);
         AV6Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV5Error = new SdtGAMError(context);
         sStyleString = "";
         lblTbuserlogin_Jsonclick = "";
         TempTags = "";
         lblTbnewpwd_Jsonclick = "";
         lblTbconfpwd_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtnconfirm_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexamplesetpassword__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsername_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavUsername_Enabled ;
      private int tblTblmain_Visible ;
      private int edtavUserpasswordnew_Enabled ;
      private int edtavUserpasswordnewconf_Enabled ;
      private int bttBtnconfirm_Visible ;
      private int AV16GXV1 ;
      private int idxLst ;
      private String AV8UserId ;
      private String wcpOAV8UserId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavUsername_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV10UserPasswordNew ;
      private String edtavUserpasswordnew_Internalname ;
      private String AV11UserPasswordNewConf ;
      private String edtavUserpasswordnewconf_Internalname ;
      private String tblTblmain_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String sStyleString ;
      private String lblTbuserlogin_Internalname ;
      private String lblTbuserlogin_Jsonclick ;
      private String TempTags ;
      private String edtavUsername_Jsonclick ;
      private String lblTbnewpwd_Internalname ;
      private String lblTbnewpwd_Jsonclick ;
      private String edtavUserpasswordnew_Jsonclick ;
      private String lblTbconfpwd_Internalname ;
      private String lblTbconfpwd_Jsonclick ;
      private String edtavUserpasswordnewconf_Jsonclick ;
      private String tblTblbuttons_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnconfirm_Jsonclick ;
      private String tblTblmsg_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV13IsChanged ;
      private String AV9UserName ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV6Errors ;
      private SdtGAMError AV5Error ;
      private SdtGAMUser AV7User ;
   }

   public class gamexamplesetpassword__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
