/*
               File: ContratadaContratoWC
        Description: Contratada Contrato WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:16:43.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratadacontratowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratadacontratowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratadacontratowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo )
      {
         this.AV7Contratada_Codigo = aP0_Contratada_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbContrato_CalculoDivergencia = new GXCombobox();
         chkContrato_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contratada_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_98 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_98_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_98_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV42Contrato_NumeroAta1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
                  AV18Contrato_Numero1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contrato_Numero1", AV18Contrato_Numero1);
                  AV19Contratada_PessoaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
                  AV20Contrato_Ano1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contrato_Ano1), 4, 0)));
                  AV21Contrato_Ano_To1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contrato_Ano_To1), 4, 0)));
                  AV23DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
                  AV44Contrato_NumeroAta2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44Contrato_NumeroAta2", AV44Contrato_NumeroAta2);
                  AV25Contrato_Numero2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Contrato_Numero2", AV25Contrato_Numero2);
                  AV26Contratada_PessoaNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Contratada_PessoaNom2", AV26Contratada_PessoaNom2);
                  AV27Contrato_Ano2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_Ano2), 4, 0)));
                  AV28Contrato_Ano_To2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contrato_Ano_To2), 4, 0)));
                  AV30DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
                  AV46Contrato_NumeroAta3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Contrato_NumeroAta3", AV46Contrato_NumeroAta3);
                  AV32Contrato_Numero3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Contrato_Numero3", AV32Contrato_Numero3);
                  AV33Contratada_PessoaNom3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Contratada_PessoaNom3", AV33Contratada_PessoaNom3);
                  AV34Contrato_Ano3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano3), 4, 0)));
                  AV35Contrato_Ano_To3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
                  AV22DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
                  AV29DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
                  AV7Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
                  AV115Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV37DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DynamicFiltersIgnoreFirst", AV37DynamicFiltersIgnoreFirst);
                  AV36DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
                  A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n74Contrato_Codigo = false;
                  A92Contrato_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  A1013Contrato_PrepostoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1013Contrato_PrepostoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1013Contrato_PrepostoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1013Contrato_PrepostoCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A1078ContratoGestor_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
                  A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA862( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV115Pgmname = "ContratadaContratoWC";
               context.Gx_err = 0;
               WS862( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratada Contrato WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117164370");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratadacontratowc.aspx") + "?" + UrlEncode("" +AV7Contratada_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_NUMEROATA1", StringUtil.RTrim( AV42Contrato_NumeroAta1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_NUMERO1", StringUtil.RTrim( AV18Contrato_Numero1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_PESSOANOM1", StringUtil.RTrim( AV19Contratada_PessoaNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_ANO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Contrato_Ano1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_ANO_TO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Contrato_Ano_To1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV23DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_NUMEROATA2", StringUtil.RTrim( AV44Contrato_NumeroAta2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_NUMERO2", StringUtil.RTrim( AV25Contrato_Numero2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_PESSOANOM2", StringUtil.RTrim( AV26Contratada_PessoaNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_ANO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Contrato_Ano2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_ANO_TO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28Contrato_Ano_To2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV30DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_NUMEROATA3", StringUtil.RTrim( AV46Contrato_NumeroAta3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_NUMERO3", StringUtil.RTrim( AV32Contrato_Numero3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_PESSOANOM3", StringUtil.RTrim( AV33Contratada_PessoaNom3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_ANO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Contrato_Ano3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATO_ANO_TO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Contrato_Ano_To3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV22DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV29DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_98", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_98), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV92GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contratada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV115Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV37DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV36DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_DATAVIGENCIATERMINO", context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_DATAFIMTA", context.localUtil.DToC( A843Contrato_DataFimTA, 0, "/"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Nobuttoncaption", StringUtil.RTrim( Confirmpanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttonposition", StringUtil.RTrim( Confirmpanel_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm862( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratadacontratowc.js", "?20203117164431");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratadaContratoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratada Contrato WC" ;
      }

      protected void WB860( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratadacontratowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_862( true) ;
         }
         else
         {
            wb_table1_2_862( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_862e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratada_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(128, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV29DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(129, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"");
         }
         wbLoad = true;
      }

      protected void START862( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratada Contrato WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP860( ) ;
            }
         }
      }

      protected void WS862( )
      {
         START862( ) ;
         EVT862( ) ;
      }

      protected void EVT862( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11862 */
                                    E11862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12862 */
                                    E12862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13862 */
                                    E13862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14862 */
                                    E14862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15862 */
                                    E15862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16862 */
                                    E16862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17862 */
                                    E17862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18862 */
                                    E18862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19862 */
                                    E19862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20862 */
                                    E20862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21862 */
                                    E21862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22862 */
                                    E22862 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 21), "VCLONARCONTRATO.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 21), "VCLONARCONTRATO.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP860( ) ;
                              }
                              nGXsfl_98_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
                              SubsflControlProps_982( ) ;
                              AV38Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV38Update)) ? AV107Update_GXI : context.convertURL( context.PathToRelativeUrl( AV38Update))));
                              AV39Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV39Delete)) ? AV108Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV39Delete))));
                              AV40Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV40Display)) ? AV109Display_GXI : context.convertURL( context.PathToRelativeUrl( AV40Display))));
                              A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                              n74Contrato_Codigo = false;
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
                              n78Contrato_NumeroAta = false;
                              A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
                              A85Contrato_DataAssinatura = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataAssinatura_Internalname), 0));
                              A1869Contrato_DataTermino = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataTermino_Internalname), 0));
                              cmbContrato_CalculoDivergencia.Name = cmbContrato_CalculoDivergencia_Internalname;
                              cmbContrato_CalculoDivergencia.CurrentValue = cgiGet( cmbContrato_CalculoDivergencia_Internalname);
                              A452Contrato_CalculoDivergencia = cgiGet( cmbContrato_CalculoDivergencia_Internalname);
                              A453Contrato_IndiceDivergencia = context.localUtil.CToN( cgiGet( edtContrato_IndiceDivergencia_Internalname), ",", ".");
                              A116Contrato_ValorUnidadeContratacao = context.localUtil.CToN( cgiGet( edtContrato_ValorUnidadeContratacao_Internalname), ",", ".");
                              A92Contrato_Ativo = StringUtil.StrToBool( cgiGet( chkContrato_Ativo_Internalname));
                              AV49AssociarGestor = cgiGet( edtavAssociargestor_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociargestor_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV49AssociarGestor)) ? AV110Associargestor_GXI : context.convertURL( context.PathToRelativeUrl( AV49AssociarGestor))));
                              AV93AssociarAuxiliar = cgiGet( edtavAssociarauxiliar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarauxiliar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV93AssociarAuxiliar)) ? AV111Associarauxiliar_GXI : context.convertURL( context.PathToRelativeUrl( AV93AssociarAuxiliar))));
                              AV50AssociarSistema = cgiGet( edtavAssociarsistema_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarsistema_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV50AssociarSistema)) ? AV112Associarsistema_GXI : context.convertURL( context.PathToRelativeUrl( AV50AssociarSistema))));
                              AV103ClonarContrato = cgiGet( edtavClonarcontrato_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonarcontrato_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV103ClonarContrato)) ? AV113Clonarcontrato_GXI : context.convertURL( context.PathToRelativeUrl( AV103ClonarContrato))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23862 */
                                          E23862 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24862 */
                                          E24862 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25862 */
                                          E25862 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VCLONARCONTRATO.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26862 */
                                          E26862 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_numeroata1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMEROATA1"), AV42Contrato_NumeroAta1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_numero1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMERO1"), AV18Contrato_Numero1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_pessoanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM1"), AV19Contratada_PessoaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_ano1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO1"), ",", ".") != Convert.ToDecimal( AV20Contrato_Ano1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_ano_to1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO_TO1"), ",", ".") != Convert.ToDecimal( AV21Contrato_Ano_To1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_numeroata2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMEROATA2"), AV44Contrato_NumeroAta2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_numero2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMERO2"), AV25Contrato_Numero2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_pessoanom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM2"), AV26Contratada_PessoaNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_ano2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO2"), ",", ".") != Convert.ToDecimal( AV27Contrato_Ano2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_ano_to2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO_TO2"), ",", ".") != Convert.ToDecimal( AV28Contrato_Ano_To2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV30DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_numeroata3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMEROATA3"), AV46Contrato_NumeroAta3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_numero3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMERO3"), AV32Contrato_Numero3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_pessoanom3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM3"), AV33Contratada_PessoaNom3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_ano3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO3"), ",", ".") != Convert.ToDecimal( AV34Contrato_Ano3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contrato_ano_to3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO_TO3"), ",", ".") != Convert.ToDecimal( AV35Contrato_Ano_To3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV29DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP860( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE862( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm862( ) ;
            }
         }
      }

      protected void PA862( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_NUMERO", "N�mero do Contrato", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_PESSOANOM", "Noma Contratada", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_NUMERO", "N�mero do Contrato", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_PESSOANOM", "Noma Contratada", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_NUMERO", "N�mero do Contrato", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATADA_PESSOANOM", "Noma Contratada", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV30DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV30DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
            }
            GXCCtl = "CONTRATO_CALCULODIVERGENCIA_" + sGXsfl_98_idx;
            cmbContrato_CalculoDivergencia.Name = GXCCtl;
            cmbContrato_CalculoDivergencia.WebTags = "";
            cmbContrato_CalculoDivergencia.addItem("B", "Sob PF Bruto", 0);
            cmbContrato_CalculoDivergencia.addItem("L", "Sob PF Liquido", 0);
            cmbContrato_CalculoDivergencia.addItem("A", "Maior diverg�ncia entre ambos", 0);
            if ( cmbContrato_CalculoDivergencia.ItemCount > 0 )
            {
               A452Contrato_CalculoDivergencia = cmbContrato_CalculoDivergencia.getValidValue(A452Contrato_CalculoDivergencia);
            }
            GXCCtl = "CONTRATO_ATIVO_" + sGXsfl_98_idx;
            chkContrato_Ativo.Name = GXCCtl;
            chkContrato_Ativo.WebTags = "";
            chkContrato_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContrato_Ativo_Internalname, "TitleCaption", chkContrato_Ativo.Caption);
            chkContrato_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_982( ) ;
         while ( nGXsfl_98_idx <= nRC_GXsfl_98 )
         {
            sendrow_982( ) ;
            nGXsfl_98_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_98_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_98_idx+1));
            sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
            SubsflControlProps_982( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV42Contrato_NumeroAta1 ,
                                       String AV18Contrato_Numero1 ,
                                       String AV19Contratada_PessoaNom1 ,
                                       short AV20Contrato_Ano1 ,
                                       short AV21Contrato_Ano_To1 ,
                                       String AV23DynamicFiltersSelector2 ,
                                       String AV44Contrato_NumeroAta2 ,
                                       String AV25Contrato_Numero2 ,
                                       String AV26Contratada_PessoaNom2 ,
                                       short AV27Contrato_Ano2 ,
                                       short AV28Contrato_Ano_To2 ,
                                       String AV30DynamicFiltersSelector3 ,
                                       String AV46Contrato_NumeroAta3 ,
                                       String AV32Contrato_Numero3 ,
                                       String AV33Contratada_PessoaNom3 ,
                                       short AV34Contrato_Ano3 ,
                                       short AV35Contrato_Ano_To3 ,
                                       bool AV22DynamicFiltersEnabled2 ,
                                       bool AV29DynamicFiltersEnabled3 ,
                                       int AV7Contratada_Codigo ,
                                       String AV115Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV37DynamicFiltersIgnoreFirst ,
                                       bool AV36DynamicFiltersRemoving ,
                                       int A74Contrato_Codigo ,
                                       bool A92Contrato_Ativo ,
                                       int A1013Contrato_PrepostoCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A1078ContratoGestor_ContratoCod ,
                                       int A1079ContratoGestor_UsuarioCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF862( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_NUMERO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_NUMEROATA", StringUtil.RTrim( A78Contrato_NumeroAta));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_ANO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_DATAASSINATURA", GetSecureSignedToken( sPrefix, A85Contrato_DataAssinatura));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_DATAASSINATURA", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_DATATERMINO", GetSecureSignedToken( sPrefix, A1869Contrato_DataTermino));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_DATATERMINO", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CALCULODIVERGENCIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A452Contrato_CalculoDivergencia, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_CALCULODIVERGENCIA", StringUtil.RTrim( A452Contrato_CalculoDivergencia));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_INDICEDIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A453Contrato_IndiceDivergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_INDICEDIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_VALORUNIDADECONTRATACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_VALORUNIDADECONTRATACAO", StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_ATIVO", GetSecureSignedToken( sPrefix, A92Contrato_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_ATIVO", StringUtil.BoolToStr( A92Contrato_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV30DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV30DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF862( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV115Pgmname = "ContratadaContratoWC";
         context.Gx_err = 0;
      }

      protected void RF862( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 98;
         /* Execute user event: E24862 */
         E24862 ();
         nGXsfl_98_idx = 1;
         sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
         SubsflControlProps_982( ) ;
         nGXsfl_98_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_982( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV42Contrato_NumeroAta1 ,
                                                 AV18Contrato_Numero1 ,
                                                 AV19Contratada_PessoaNom1 ,
                                                 AV20Contrato_Ano1 ,
                                                 AV21Contrato_Ano_To1 ,
                                                 AV22DynamicFiltersEnabled2 ,
                                                 AV23DynamicFiltersSelector2 ,
                                                 AV44Contrato_NumeroAta2 ,
                                                 AV25Contrato_Numero2 ,
                                                 AV26Contratada_PessoaNom2 ,
                                                 AV27Contrato_Ano2 ,
                                                 AV28Contrato_Ano_To2 ,
                                                 AV29DynamicFiltersEnabled3 ,
                                                 AV30DynamicFiltersSelector3 ,
                                                 AV46Contrato_NumeroAta3 ,
                                                 AV32Contrato_Numero3 ,
                                                 AV33Contratada_PessoaNom3 ,
                                                 AV34Contrato_Ano3 ,
                                                 AV35Contrato_Ano_To3 ,
                                                 A78Contrato_NumeroAta ,
                                                 A77Contrato_Numero ,
                                                 A41Contratada_PessoaNom ,
                                                 A79Contrato_Ano ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A39Contratada_Codigo ,
                                                 AV7Contratada_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV42Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV42Contrato_NumeroAta1), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
            lV18Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV18Contrato_Numero1), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contrato_Numero1", AV18Contrato_Numero1);
            lV19Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratada_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
            lV44Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta2), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44Contrato_NumeroAta2", AV44Contrato_NumeroAta2);
            lV25Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV25Contrato_Numero2), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Contrato_Numero2", AV25Contrato_Numero2);
            lV26Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV26Contratada_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Contratada_PessoaNom2", AV26Contratada_PessoaNom2);
            lV46Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV46Contrato_NumeroAta3), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Contrato_NumeroAta3", AV46Contrato_NumeroAta3);
            lV32Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV32Contrato_Numero3), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Contrato_Numero3", AV32Contrato_Numero3);
            lV33Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV33Contratada_PessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Contratada_PessoaNom3", AV33Contratada_PessoaNom3);
            /* Using cursor H00864 */
            pr_default.execute(0, new Object[] {AV7Contratada_Codigo, lV42Contrato_NumeroAta1, lV18Contrato_Numero1, lV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, lV44Contrato_NumeroAta2, lV25Contrato_Numero2, lV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, lV46Contrato_NumeroAta3, lV32Contrato_Numero3, lV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_98_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A40Contratada_PessoaCod = H00864_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = H00864_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00864_n74Contrato_Codigo[0];
               A41Contratada_PessoaNom = H00864_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00864_n41Contratada_PessoaNom[0];
               A1013Contrato_PrepostoCod = H00864_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00864_n1013Contrato_PrepostoCod[0];
               A39Contratada_Codigo = H00864_A39Contratada_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
               A92Contrato_Ativo = H00864_A92Contrato_Ativo[0];
               A116Contrato_ValorUnidadeContratacao = H00864_A116Contrato_ValorUnidadeContratacao[0];
               A453Contrato_IndiceDivergencia = H00864_A453Contrato_IndiceDivergencia[0];
               A452Contrato_CalculoDivergencia = H00864_A452Contrato_CalculoDivergencia[0];
               A85Contrato_DataAssinatura = H00864_A85Contrato_DataAssinatura[0];
               A79Contrato_Ano = H00864_A79Contrato_Ano[0];
               A78Contrato_NumeroAta = H00864_A78Contrato_NumeroAta[0];
               n78Contrato_NumeroAta = H00864_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H00864_A77Contrato_Numero[0];
               A843Contrato_DataFimTA = H00864_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00864_n843Contrato_DataFimTA[0];
               A83Contrato_DataVigenciaTermino = H00864_A83Contrato_DataVigenciaTermino[0];
               A843Contrato_DataFimTA = H00864_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H00864_n843Contrato_DataFimTA[0];
               A40Contratada_PessoaCod = H00864_A40Contratada_PessoaCod[0];
               A41Contratada_PessoaNom = H00864_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00864_n41Contratada_PessoaNom[0];
               A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
               /* Execute user event: E25862 */
               E25862 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 98;
            WB860( ) ;
         }
         nGXsfl_98_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV42Contrato_NumeroAta1 ,
                                              AV18Contrato_Numero1 ,
                                              AV19Contratada_PessoaNom1 ,
                                              AV20Contrato_Ano1 ,
                                              AV21Contrato_Ano_To1 ,
                                              AV22DynamicFiltersEnabled2 ,
                                              AV23DynamicFiltersSelector2 ,
                                              AV44Contrato_NumeroAta2 ,
                                              AV25Contrato_Numero2 ,
                                              AV26Contratada_PessoaNom2 ,
                                              AV27Contrato_Ano2 ,
                                              AV28Contrato_Ano_To2 ,
                                              AV29DynamicFiltersEnabled3 ,
                                              AV30DynamicFiltersSelector3 ,
                                              AV46Contrato_NumeroAta3 ,
                                              AV32Contrato_Numero3 ,
                                              AV33Contratada_PessoaNom3 ,
                                              AV34Contrato_Ano3 ,
                                              AV35Contrato_Ano_To3 ,
                                              A78Contrato_NumeroAta ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A39Contratada_Codigo ,
                                              AV7Contratada_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV42Contrato_NumeroAta1 = StringUtil.PadR( StringUtil.RTrim( AV42Contrato_NumeroAta1), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
         lV18Contrato_Numero1 = StringUtil.PadR( StringUtil.RTrim( AV18Contrato_Numero1), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contrato_Numero1", AV18Contrato_Numero1);
         lV19Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratada_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
         lV44Contrato_NumeroAta2 = StringUtil.PadR( StringUtil.RTrim( AV44Contrato_NumeroAta2), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44Contrato_NumeroAta2", AV44Contrato_NumeroAta2);
         lV25Contrato_Numero2 = StringUtil.PadR( StringUtil.RTrim( AV25Contrato_Numero2), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Contrato_Numero2", AV25Contrato_Numero2);
         lV26Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV26Contratada_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Contratada_PessoaNom2", AV26Contratada_PessoaNom2);
         lV46Contrato_NumeroAta3 = StringUtil.PadR( StringUtil.RTrim( AV46Contrato_NumeroAta3), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Contrato_NumeroAta3", AV46Contrato_NumeroAta3);
         lV32Contrato_Numero3 = StringUtil.PadR( StringUtil.RTrim( AV32Contrato_Numero3), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Contrato_Numero3", AV32Contrato_Numero3);
         lV33Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV33Contratada_PessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Contratada_PessoaNom3", AV33Contratada_PessoaNom3);
         /* Using cursor H00867 */
         pr_default.execute(1, new Object[] {AV7Contratada_Codigo, lV42Contrato_NumeroAta1, lV18Contrato_Numero1, lV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, lV44Contrato_NumeroAta2, lV25Contrato_Numero2, lV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, lV46Contrato_NumeroAta3, lV32Contrato_Numero3, lV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3});
         GRID_nRecordCount = H00867_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP860( )
      {
         /* Before Start, stand alone formulas. */
         AV115Pgmname = "ContratadaContratoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23862 */
         E23862 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV42Contrato_NumeroAta1 = cgiGet( edtavContrato_numeroata1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
            AV18Contrato_Numero1 = cgiGet( edtavContrato_numero1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contrato_Numero1", AV18Contrato_Numero1);
            AV19Contratada_PessoaNom1 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO1");
               GX_FocusControl = edtavContrato_ano1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20Contrato_Ano1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contrato_Ano1), 4, 0)));
            }
            else
            {
               AV20Contrato_Ano1 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contrato_Ano1), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO1");
               GX_FocusControl = edtavContrato_ano_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21Contrato_Ano_To1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contrato_Ano_To1), 4, 0)));
            }
            else
            {
               AV21Contrato_Ano_To1 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contrato_Ano_To1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV23DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            AV44Contrato_NumeroAta2 = cgiGet( edtavContrato_numeroata2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44Contrato_NumeroAta2", AV44Contrato_NumeroAta2);
            AV25Contrato_Numero2 = cgiGet( edtavContrato_numero2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Contrato_Numero2", AV25Contrato_Numero2);
            AV26Contratada_PessoaNom2 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Contratada_PessoaNom2", AV26Contratada_PessoaNom2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO2");
               GX_FocusControl = edtavContrato_ano2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27Contrato_Ano2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_Ano2), 4, 0)));
            }
            else
            {
               AV27Contrato_Ano2 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_Ano2), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO2");
               GX_FocusControl = edtavContrato_ano_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28Contrato_Ano_To2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contrato_Ano_To2), 4, 0)));
            }
            else
            {
               AV28Contrato_Ano_To2 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contrato_Ano_To2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV30DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
            AV46Contrato_NumeroAta3 = cgiGet( edtavContrato_numeroata3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Contrato_NumeroAta3", AV46Contrato_NumeroAta3);
            AV32Contrato_Numero3 = cgiGet( edtavContrato_numero3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Contrato_Numero3", AV32Contrato_Numero3);
            AV33Contratada_PessoaNom3 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Contratada_PessoaNom3", AV33Contratada_PessoaNom3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO3");
               GX_FocusControl = edtavContrato_ano3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Contrato_Ano3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano3), 4, 0)));
            }
            else
            {
               AV34Contrato_Ano3 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano3), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO3");
               GX_FocusControl = edtavContrato_ano_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35Contrato_Ano_To3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
            }
            else
            {
               AV35Contrato_Ano_To3 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
            }
            A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
            AV22DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
            AV29DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_98 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_98"), ",", "."));
            AV91GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV92GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contratada_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Nobuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Nobuttoncaption");
            Confirmpanel_Yesbuttonposition = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttonposition");
            Confirmpanel_Confirmtype = cgiGet( sPrefix+"CONFIRMPANEL_Confirmtype");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMEROATA1"), AV42Contrato_NumeroAta1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMERO1"), AV18Contrato_Numero1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM1"), AV19Contratada_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO1"), ",", ".") != Convert.ToDecimal( AV20Contrato_Ano1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO_TO1"), ",", ".") != Convert.ToDecimal( AV21Contrato_Ano_To1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMEROATA2"), AV44Contrato_NumeroAta2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMERO2"), AV25Contrato_Numero2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM2"), AV26Contratada_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO2"), ",", ".") != Convert.ToDecimal( AV27Contrato_Ano2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO_TO2"), ",", ".") != Convert.ToDecimal( AV28Contrato_Ano_To2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV30DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMEROATA3"), AV46Contrato_NumeroAta3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATO_NUMERO3"), AV32Contrato_Numero3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM3"), AV33Contratada_PessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO3"), ",", ".") != Convert.ToDecimal( AV34Contrato_Ano3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATO_ANO_TO3"), ",", ".") != Convert.ToDecimal( AV35Contrato_Ano_To3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV29DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23862 */
         E23862 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23862( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTRATO_NUMEROATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector2 = "CONTRATO_NUMEROATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersSelector3 = "CONTRATO_NUMEROATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtContratada_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "N�mero da Ata", 0);
         cmbavOrderedby.addItem("2", "N� de Contrato", 0);
         cmbavOrderedby.addItem("3", "Ano", 0);
         cmbavOrderedby.addItem("4", "Assinatura", 0);
         cmbavOrderedby.addItem("5", "Diverg�ncia", 0);
         cmbavOrderedby.addItem("6", "% Div.", 0);
         cmbavOrderedby.addItem("7", "Valor PF", 0);
         cmbavOrderedby.addItem("8", "Ativo", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E24862( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N� Contrato", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContrato_NumeroAta_Titleformat = 2;
         edtContrato_NumeroAta_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N� Ata", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_NumeroAta_Internalname, "Title", edtContrato_NumeroAta_Title);
         edtContrato_Ano_Titleformat = 2;
         edtContrato_Ano_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ano", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_Ano_Internalname, "Title", edtContrato_Ano_Title);
         edtContrato_DataAssinatura_Titleformat = 2;
         edtContrato_DataAssinatura_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Assinatura", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_DataAssinatura_Internalname, "Title", edtContrato_DataAssinatura_Title);
         cmbContrato_CalculoDivergencia_Titleformat = 2;
         cmbContrato_CalculoDivergencia.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Diverg�ncia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContrato_CalculoDivergencia_Internalname, "Title", cmbContrato_CalculoDivergencia.Title.Text);
         edtContrato_IndiceDivergencia_Titleformat = 2;
         edtContrato_IndiceDivergencia_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "% Div.", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_IndiceDivergencia_Internalname, "Title", edtContrato_IndiceDivergencia_Title);
         edtContrato_ValorUnidadeContratacao_Titleformat = 2;
         edtContrato_ValorUnidadeContratacao_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Valor PF", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_ValorUnidadeContratacao_Internalname, "Title", edtContrato_ValorUnidadeContratacao_Title);
         chkContrato_Ativo_Titleformat = 2;
         chkContrato_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ativo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContrato_Ativo_Internalname, "Title", chkContrato_Ativo.Title.Text);
         AV91GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV91GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91GridCurrentPage), 10, 0)));
         AV92GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV92GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV92GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         edtavAssociargestor_Title = "Gestores";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociargestor_Internalname, "Title", edtavAssociargestor_Title);
         edtavAssociarauxiliar_Title = "Auxiliares";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarauxiliar_Internalname, "Title", edtavAssociarauxiliar_Title);
         edtavAssociarsistema_Title = "Sistemas";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAssociarsistema_Internalname, "Title", edtavAssociarsistema_Title);
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         edtavClonarcontrato_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavClonarcontrato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClonarcontrato_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11862( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV90PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV90PageToGo) ;
         }
      }

      private void E25862( )
      {
         /* Grid_Load Routine */
         AV38Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV38Update);
         AV107Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +AV7Contratada_Codigo);
         AV39Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV39Delete);
         AV108Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +AV7Contratada_Codigo);
         AV40Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV40Display);
         AV109Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Cl�usulas, Servi�os, Garantias, Anexos, Dados Certame, Termos Aditivos, Ocorr�ncias, Obriga��es";
         edtavDisplay_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV49AssociarGestor = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociargestor_Internalname, AV49AssociarGestor);
         AV110Associargestor_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociargestor_Tooltiptext = "Clique aqui p/ associar os gestores deste contrato";
         AV93AssociarAuxiliar = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarauxiliar_Internalname, AV93AssociarAuxiliar);
         AV111Associarauxiliar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarauxiliar_Tooltiptext = "Clique aqui p/ associar os auxiliares deste contrato";
         AV50AssociarSistema = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAssociarsistema_Internalname, AV50AssociarSistema);
         AV112Associarsistema_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarsistema_Tooltiptext = "Associar os sistemas deste contrato";
         AV103ClonarContrato = context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavClonarcontrato_Internalname, AV103ClonarContrato);
         AV113Clonarcontrato_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "550f386a-e6ac-4748-b314-ee8e1f72ce4d", "", context.GetTheme( )));
         edtavClonarcontrato_Tooltiptext = "Clonar contrato";
         if ( A92Contrato_Ativo )
         {
            AV104Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV104Color = GXUtil.RGB( 255, 0, 0);
         }
         edtContrato_Numero_Forecolor = (int)(AV104Color);
         edtContrato_NumeroAta_Forecolor = (int)(AV104Color);
         edtContrato_Ano_Forecolor = (int)(AV104Color);
         edtContrato_DataAssinatura_Forecolor = (int)(AV104Color);
         edtContrato_DataTermino_Forecolor = (int)(AV104Color);
         cmbContrato_CalculoDivergencia.ForeColor = (int)(AV104Color);
         edtContrato_IndiceDivergencia_Forecolor = (int)(AV104Color);
         edtContrato_ValorUnidadeContratacao_Forecolor = (int)(AV104Color);
         edtContrato_Numero_Forecolor = (int)(AV104Color);
         edtContrato_Numero_Forecolor = (int)(AV104Color);
         if ( AV6WWPContext.gxTpr_Userehadministradorgam || ( A1013Contrato_PrepostoCod == AV6WWPContext.gxTpr_Userid ) )
         {
            edtavAssociargestor_Visible = 1;
            edtavAssociarauxiliar_Visible = 1;
         }
         else
         {
            AV114GXLvl156 = 0;
            /* Using cursor H00868 */
            pr_default.execute(2, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo, AV6WWPContext.gxTpr_Userid});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H00868_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H00868_A1079ContratoGestor_UsuarioCod[0];
               AV114GXLvl156 = 1;
               edtavAssociargestor_Visible = 1;
               edtavAssociarauxiliar_Visible = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            if ( AV114GXLvl156 == 0 )
            {
               edtavAssociargestor_Visible = 0;
               edtavAssociarauxiliar_Visible = 0;
            }
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 98;
         }
         sendrow_982( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_98_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(98, GridRow);
         }
      }

      protected void E12862( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E18862( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV22DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E13862( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV36DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV37DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DynamicFiltersIgnoreFirst", AV37DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV37DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37DynamicFiltersIgnoreFirst", AV37DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E19862( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20862( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV29DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E14862( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV36DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21862( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15862( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV36DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         AV29DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV36DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DynamicFiltersRemoving", AV36DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV42Contrato_NumeroAta1, AV18Contrato_Numero1, AV19Contratada_PessoaNom1, AV20Contrato_Ano1, AV21Contrato_Ano_To1, AV23DynamicFiltersSelector2, AV44Contrato_NumeroAta2, AV25Contrato_Numero2, AV26Contratada_PessoaNom2, AV27Contrato_Ano2, AV28Contrato_Ano_To2, AV30DynamicFiltersSelector3, AV46Contrato_NumeroAta3, AV32Contrato_Numero3, AV33Contratada_PessoaNom3, AV34Contrato_Ano3, AV35Contrato_Ano_To3, AV22DynamicFiltersEnabled2, AV29DynamicFiltersEnabled3, AV7Contratada_Codigo, AV115Pgmname, AV11GridState, AV37DynamicFiltersIgnoreFirst, AV36DynamicFiltersRemoving, A74Contrato_Codigo, A92Contrato_Ativo, A1013Contrato_PrepostoCod, AV6WWPContext, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22862( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16862( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E26862( )
      {
         /* Clonarcontrato_Click Routine */
         new prc_clonarcontrato(context ).execute(  A74Contrato_Codigo) ;
      }

      protected void E17862( )
      {
         /* 'DoInsert' Routine */
         if ( new prc_verificacontratadacontratanteusuario(context).executeUdp(  AV7Contratada_Codigo) )
         {
            context.wjLoc = formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7Contratada_Codigo);
            context.wjLocDisableFrm = 1;
         }
         else
         {
            Confirmpanel_Title = "Aten��o!";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Confirmpanel_Internalname, "Title", Confirmpanel_Title);
            Confirmpanel_Confirmationtext = "N�o � possivel cadastrar Contratos. Usu�rios n�o foram informados para a Contratada e/ou Contratante. Verifique!";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Confirmpanel_Internalname, "ConfirmationText", Confirmpanel_Confirmationtext);
            this.executeUsercontrolMethod(sPrefix, false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContrato_numeroata1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numeroata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata1_Visible), 5, 0)));
         edtavContrato_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero1_Visible), 5, 0)));
         edtavContratada_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numeroata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContrato_numeroata2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numeroata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata2_Visible), 5, 0)));
         edtavContrato_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero2_Visible), 5, 0)));
         edtavContratada_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numeroata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContrato_numeroata3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numeroata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata3_Visible), 5, 0)));
         edtavContrato_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero3_Visible), 5, 0)));
         edtavContratada_pessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numeroata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         AV23DynamicFiltersSelector2 = "CONTRATO_NUMEROATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         AV44Contrato_NumeroAta2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44Contrato_NumeroAta2", AV44Contrato_NumeroAta2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
         AV30DynamicFiltersSelector3 = "CONTRATO_NUMEROATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
         AV46Contrato_NumeroAta3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Contrato_NumeroAta3", AV46Contrato_NumeroAta3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16DynamicFiltersSelector1 = "CONTRATO_NUMEROATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV42Contrato_NumeroAta1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV41Session.Get(AV115Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV115Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV41Session.Get(AV115Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
            {
               AV42Contrato_NumeroAta1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42Contrato_NumeroAta1", AV42Contrato_NumeroAta1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
            {
               AV18Contrato_Numero1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contrato_Numero1", AV18Contrato_Numero1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV19Contratada_PessoaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
            {
               AV20Contrato_Ano1 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contrato_Ano1), 4, 0)));
               AV21Contrato_Ano_To1 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Contrato_Ano_To1), 4, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV22DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV23DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
               {
                  AV44Contrato_NumeroAta2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44Contrato_NumeroAta2", AV44Contrato_NumeroAta2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
               {
                  AV25Contrato_Numero2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25Contrato_Numero2", AV25Contrato_Numero2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV26Contratada_PessoaNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26Contratada_PessoaNom2", AV26Contratada_PessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
               {
                  AV27Contrato_Ano2 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Contrato_Ano2), 4, 0)));
                  AV28Contrato_Ano_To2 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Valueto, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Contrato_Ano_To2), 4, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV29DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled3", AV29DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV30DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector3", AV30DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
                  {
                     AV46Contrato_NumeroAta3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46Contrato_NumeroAta3", AV46Contrato_NumeroAta3);
                  }
                  else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
                  {
                     AV32Contrato_Numero3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32Contrato_Numero3", AV32Contrato_Numero3);
                  }
                  else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
                  {
                     AV33Contratada_PessoaNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33Contratada_PessoaNom3", AV33Contratada_PessoaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
                  {
                     AV34Contrato_Ano3 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contrato_Ano3), 4, 0)));
                     AV35Contrato_Ano_To3 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Valueto, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV36DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV41Session.Get(AV115Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV115Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV37DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV42Contrato_NumeroAta1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contrato_Numero1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Contrato_Numero1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Contratada_PessoaNom1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ! ( (0==AV20Contrato_Ano1) && (0==AV21Contrato_Ano_To1) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV20Contrato_Ano1), 4, 0);
               AV13GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV21Contrato_Ano_To1), 4, 0);
            }
            if ( AV36DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV44Contrato_NumeroAta2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contrato_Numero2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV25Contrato_Numero2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Contratada_PessoaNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV26Contratada_PessoaNom2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ! ( (0==AV27Contrato_Ano2) && (0==AV28Contrato_Ano_To2) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV27Contrato_Ano2), 4, 0);
               AV13GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV28Contrato_Ano_To2), 4, 0);
            }
            if ( AV36DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV29DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV30DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Contrato_NumeroAta3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV46Contrato_NumeroAta3;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Contrato_Numero3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV32Contrato_Numero3;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Contratada_PessoaNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV33Contratada_PessoaNom3;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ! ( (0==AV34Contrato_Ano3) && (0==AV35Contrato_Ano_To3) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV34Contrato_Ano3), 4, 0);
               AV13GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV35Contrato_Ano_To3), 4, 0);
            }
            if ( AV36DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV115Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Contrato";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contratada_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV41Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_862( true) ;
         }
         else
         {
            wb_table2_8_862( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_95_862( true) ;
         }
         else
         {
            wb_table3_95_862( false) ;
         }
         return  ;
      }

      protected void wb_table3_95_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_121_862( true) ;
         }
         else
         {
            wb_table4_121_862( false) ;
         }
         return  ;
      }

      protected void wb_table4_121_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_862e( true) ;
         }
         else
         {
            wb_table1_2_862e( false) ;
         }
      }

      protected void wb_table4_121_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(129), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(250), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblUsertable_uc_mensagem_Internalname, tblUsertable_uc_mensagem_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_121_862e( true) ;
         }
         else
         {
            wb_table4_121_862e( false) ;
         }
      }

      protected void wb_table3_95_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"98\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_NumeroAta_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_NumeroAta_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_NumeroAta_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Ano_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Ano_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Ano_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_DataAssinatura_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_DataAssinatura_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_DataAssinatura_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Vig�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContrato_CalculoDivergencia_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContrato_CalculoDivergencia.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContrato_CalculoDivergencia.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_IndiceDivergencia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_IndiceDivergencia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_IndiceDivergencia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_ValorUnidadeContratacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_ValorUnidadeContratacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_ValorUnidadeContratacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContrato_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContrato_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContrato_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAssociargestor_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavAssociargestor_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAssociarauxiliar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarauxiliar_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarsistema_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavClonarcontrato_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV38Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV39Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV40Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A78Contrato_NumeroAta));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_NumeroAta_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_NumeroAta_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_NumeroAta_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Ano_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Ano_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Ano_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_DataAssinatura_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_DataAssinatura_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_DataAssinatura_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_DataTermino_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A452Contrato_CalculoDivergencia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContrato_CalculoDivergencia.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContrato_CalculoDivergencia_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContrato_CalculoDivergencia.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_IndiceDivergencia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_IndiceDivergencia_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_IndiceDivergencia_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_ValorUnidadeContratacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_ValorUnidadeContratacao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_ValorUnidadeContratacao_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A92Contrato_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContrato_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContrato_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV49AssociarGestor));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociargestor_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociargestor_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociargestor_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV93AssociarAuxiliar));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarauxiliar_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarauxiliar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarauxiliar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV50AssociarSistema));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarsistema_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarsistema_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV103ClonarContrato));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavClonarcontrato_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavClonarcontrato_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 98 )
         {
            wbEnd = 0;
            nRC_GXsfl_98 = (short)(nGXsfl_98_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_95_862e( true) ;
         }
         else
         {
            wb_table3_95_862e( false) ;
         }
      }

      protected void wb_table2_8_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_11_862( true) ;
         }
         else
         {
            wb_table5_11_862( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ContratadaContratoWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_21_862( true) ;
         }
         else
         {
            wb_table6_21_862( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_862e( true) ;
         }
         else
         {
            wb_table2_8_862e( false) ;
         }
      }

      protected void wb_table6_21_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table7_26_862( true) ;
         }
         else
         {
            wb_table7_26_862( false) ;
         }
         return  ;
      }

      protected void wb_table7_26_862e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_862e( true) ;
         }
         else
         {
            wb_table6_21_862e( false) ;
         }
      }

      protected void wb_table7_26_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_ContratadaContratoWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata1_Internalname, StringUtil.RTrim( AV42Contrato_NumeroAta1), StringUtil.RTrim( context.localUtil.Format( AV42Contrato_NumeroAta1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero1_Internalname, StringUtil.RTrim( AV18Contrato_Numero1), StringUtil.RTrim( context.localUtil.Format( AV18Contrato_Numero1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom1_Internalname, StringUtil.RTrim( AV19Contratada_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19Contratada_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            wb_table8_38_862( true) ;
         }
         else
         {
            wb_table8_38_862( false) ;
         }
         return  ;
      }

      protected void wb_table8_38_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_ContratadaContratoWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata2_Internalname, StringUtil.RTrim( AV44Contrato_NumeroAta2), StringUtil.RTrim( context.localUtil.Format( AV44Contrato_NumeroAta2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero2_Internalname, StringUtil.RTrim( AV25Contrato_Numero2), StringUtil.RTrim( context.localUtil.Format( AV25Contrato_Numero2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom2_Internalname, StringUtil.RTrim( AV26Contratada_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV26Contratada_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            wb_table9_60_862( true) ;
         }
         else
         {
            wb_table9_60_862( false) ;
         }
         return  ;
      }

      protected void wb_table9_60_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV30DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", "", true, "HLP_ContratadaContratoWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata3_Internalname, StringUtil.RTrim( AV46Contrato_NumeroAta3), StringUtil.RTrim( context.localUtil.Format( AV46Contrato_NumeroAta3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero3_Internalname, StringUtil.RTrim( AV32Contrato_Numero3), StringUtil.RTrim( context.localUtil.Format( AV32Contrato_Numero3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom3_Internalname, StringUtil.RTrim( AV33Contratada_PessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV33Contratada_PessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratadaContratoWC.htm");
            wb_table10_82_862( true) ;
         }
         else
         {
            wb_table10_82_862( false) ;
         }
         return  ;
      }

      protected void wb_table10_82_862e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_26_862e( true) ;
         }
         else
         {
            wb_table7_26_862e( false) ;
         }
      }

      protected void wb_table10_82_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano3_Internalname, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Contrato_Ano3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34Contrato_Ano3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,85);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Contrato_Ano_To3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35Contrato_Ano_To3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_82_862e( true) ;
         }
         else
         {
            wb_table10_82_862e( false) ;
         }
      }

      protected void wb_table9_60_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano2_Internalname, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Contrato_Ano2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV27Contrato_Ano2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28Contrato_Ano_To2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV28Contrato_Ano_To2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_60_862e( true) ;
         }
         else
         {
            wb_table9_60_862e( false) ;
         }
      }

      protected void wb_table8_38_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano1_Internalname, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Contrato_Ano1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20Contrato_Ano1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_98_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Contrato_Ano_To1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21Contrato_Ano_To1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,45);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_38_862e( true) ;
         }
         else
         {
            wb_table8_38_862e( false) ;
         }
      }

      protected void wb_table5_11_862( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratadaContratoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_862e( true) ;
         }
         else
         {
            wb_table5_11_862e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contratada_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA862( ) ;
         WS862( ) ;
         WE862( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contratada_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA862( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratadacontratowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA862( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contratada_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
         }
         wcpOAV7Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contratada_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contratada_Codigo != wcpOAV7Contratada_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contratada_Codigo = AV7Contratada_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contratada_Codigo = cgiGet( sPrefix+"AV7Contratada_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contratada_Codigo) > 0 )
         {
            AV7Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contratada_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
         }
         else
         {
            AV7Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contratada_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA862( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS862( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS862( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contratada_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contratada_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contratada_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contratada_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Contratada_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE862( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117165093");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratadacontratowc.js", "?20203117165093");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_982( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_98_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_98_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_98_idx;
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO_"+sGXsfl_98_idx;
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO_"+sGXsfl_98_idx;
         edtContrato_NumeroAta_Internalname = sPrefix+"CONTRATO_NUMEROATA_"+sGXsfl_98_idx;
         edtContrato_Ano_Internalname = sPrefix+"CONTRATO_ANO_"+sGXsfl_98_idx;
         edtContrato_DataAssinatura_Internalname = sPrefix+"CONTRATO_DATAASSINATURA_"+sGXsfl_98_idx;
         edtContrato_DataTermino_Internalname = sPrefix+"CONTRATO_DATATERMINO_"+sGXsfl_98_idx;
         cmbContrato_CalculoDivergencia_Internalname = sPrefix+"CONTRATO_CALCULODIVERGENCIA_"+sGXsfl_98_idx;
         edtContrato_IndiceDivergencia_Internalname = sPrefix+"CONTRATO_INDICEDIVERGENCIA_"+sGXsfl_98_idx;
         edtContrato_ValorUnidadeContratacao_Internalname = sPrefix+"CONTRATO_VALORUNIDADECONTRATACAO_"+sGXsfl_98_idx;
         chkContrato_Ativo_Internalname = sPrefix+"CONTRATO_ATIVO_"+sGXsfl_98_idx;
         edtavAssociargestor_Internalname = sPrefix+"vASSOCIARGESTOR_"+sGXsfl_98_idx;
         edtavAssociarauxiliar_Internalname = sPrefix+"vASSOCIARAUXILIAR_"+sGXsfl_98_idx;
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA_"+sGXsfl_98_idx;
         edtavClonarcontrato_Internalname = sPrefix+"vCLONARCONTRATO_"+sGXsfl_98_idx;
      }

      protected void SubsflControlProps_fel_982( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_98_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_98_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_98_fel_idx;
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO_"+sGXsfl_98_fel_idx;
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO_"+sGXsfl_98_fel_idx;
         edtContrato_NumeroAta_Internalname = sPrefix+"CONTRATO_NUMEROATA_"+sGXsfl_98_fel_idx;
         edtContrato_Ano_Internalname = sPrefix+"CONTRATO_ANO_"+sGXsfl_98_fel_idx;
         edtContrato_DataAssinatura_Internalname = sPrefix+"CONTRATO_DATAASSINATURA_"+sGXsfl_98_fel_idx;
         edtContrato_DataTermino_Internalname = sPrefix+"CONTRATO_DATATERMINO_"+sGXsfl_98_fel_idx;
         cmbContrato_CalculoDivergencia_Internalname = sPrefix+"CONTRATO_CALCULODIVERGENCIA_"+sGXsfl_98_fel_idx;
         edtContrato_IndiceDivergencia_Internalname = sPrefix+"CONTRATO_INDICEDIVERGENCIA_"+sGXsfl_98_fel_idx;
         edtContrato_ValorUnidadeContratacao_Internalname = sPrefix+"CONTRATO_VALORUNIDADECONTRATACAO_"+sGXsfl_98_fel_idx;
         chkContrato_Ativo_Internalname = sPrefix+"CONTRATO_ATIVO_"+sGXsfl_98_fel_idx;
         edtavAssociargestor_Internalname = sPrefix+"vASSOCIARGESTOR_"+sGXsfl_98_fel_idx;
         edtavAssociarauxiliar_Internalname = sPrefix+"vASSOCIARAUXILIAR_"+sGXsfl_98_fel_idx;
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA_"+sGXsfl_98_fel_idx;
         edtavClonarcontrato_Internalname = sPrefix+"vCLONARCONTRATO_"+sGXsfl_98_fel_idx;
      }

      protected void sendrow_982( )
      {
         SubsflControlProps_982( ) ;
         WB860( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_98_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_98_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_98_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV38Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV38Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV107Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV38Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV38Update)) ? AV107Update_GXI : context.PathToRelativeUrl( AV38Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV38Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV39Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV39Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV108Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV39Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV39Delete)) ? AV108Delete_GXI : context.PathToRelativeUrl( AV39Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV39Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV40Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV40Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV109Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV40Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV40Display)) ? AV109Display_GXI : context.PathToRelativeUrl( AV40Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV40Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_Numero_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_NumeroAta_Internalname,StringUtil.RTrim( A78Contrato_NumeroAta),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_NumeroAta_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_NumeroAta_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroAta",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Ano_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Ano_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_Ano_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Ano",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_DataAssinatura_Internalname,context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"),context.localUtil.Format( A85Contrato_DataAssinatura, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_DataAssinatura_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_DataAssinatura_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_DataTermino_Internalname,context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"),context.localUtil.Format( A1869Contrato_DataTermino, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_DataTermino_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_DataTermino_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_98_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATO_CALCULODIVERGENCIA_" + sGXsfl_98_idx;
               cmbContrato_CalculoDivergencia.Name = GXCCtl;
               cmbContrato_CalculoDivergencia.WebTags = "";
               cmbContrato_CalculoDivergencia.addItem("B", "Sob PF Bruto", 0);
               cmbContrato_CalculoDivergencia.addItem("L", "Sob PF Liquido", 0);
               cmbContrato_CalculoDivergencia.addItem("A", "Maior diverg�ncia entre ambos", 0);
               if ( cmbContrato_CalculoDivergencia.ItemCount > 0 )
               {
                  A452Contrato_CalculoDivergencia = cmbContrato_CalculoDivergencia.getValidValue(A452Contrato_CalculoDivergencia);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContrato_CalculoDivergencia,(String)cmbContrato_CalculoDivergencia_Internalname,StringUtil.RTrim( A452Contrato_CalculoDivergencia),(short)1,(String)cmbContrato_CalculoDivergencia_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbContrato_CalculoDivergencia.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContrato_CalculoDivergencia.CurrentValue = StringUtil.RTrim( A452Contrato_CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContrato_CalculoDivergencia_Internalname, "Values", (String)(cmbContrato_CalculoDivergencia.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_IndiceDivergencia_Internalname,StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ",", "")),context.localUtil.Format( A453Contrato_IndiceDivergencia, "ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_IndiceDivergencia_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_IndiceDivergencia_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_ValorUnidadeContratacao_Internalname,StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")),context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_ValorUnidadeContratacao_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_ValorUnidadeContratacao_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)98,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContrato_Ativo_Internalname,StringUtil.BoolToStr( A92Contrato_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAssociargestor_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociargestor_Enabled!=0)&&(edtavAssociargestor_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 112,'"+sPrefix+"',false,'',98)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV49AssociarGestor_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV49AssociarGestor))&&String.IsNullOrEmpty(StringUtil.RTrim( AV110Associargestor_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV49AssociarGestor)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociargestor_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV49AssociarGestor)) ? AV110Associargestor_GXI : context.PathToRelativeUrl( AV49AssociarGestor)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAssociargestor_Visible,(short)1,(String)"",(String)edtavAssociargestor_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociargestor_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e27862_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV49AssociarGestor_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAssociarauxiliar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarauxiliar_Enabled!=0)&&(edtavAssociarauxiliar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 113,'"+sPrefix+"',false,'',98)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV93AssociarAuxiliar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV93AssociarAuxiliar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV111Associarauxiliar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV93AssociarAuxiliar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarauxiliar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV93AssociarAuxiliar)) ? AV111Associarauxiliar_GXI : context.PathToRelativeUrl( AV93AssociarAuxiliar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAssociarauxiliar_Visible,(short)1,(String)"",(String)edtavAssociarauxiliar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarauxiliar_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e28862_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV93AssociarAuxiliar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarsistema_Enabled!=0)&&(edtavAssociarsistema_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 114,'"+sPrefix+"',false,'',98)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV50AssociarSistema_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV50AssociarSistema))&&String.IsNullOrEmpty(StringUtil.RTrim( AV112Associarsistema_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV50AssociarSistema)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarsistema_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV50AssociarSistema)) ? AV112Associarsistema_GXI : context.PathToRelativeUrl( AV50AssociarSistema)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarsistema_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarsistema_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e29862_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV50AssociarSistema_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavClonarcontrato_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavClonarcontrato_Enabled!=0)&&(edtavClonarcontrato_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 115,'"+sPrefix+"',false,'',98)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV103ClonarContrato_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV103ClonarContrato))&&String.IsNullOrEmpty(StringUtil.RTrim( AV113Clonarcontrato_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV103ClonarContrato)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavClonarcontrato_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV103ClonarContrato)) ? AV113Clonarcontrato_GXI : context.PathToRelativeUrl( AV103ClonarContrato)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavClonarcontrato_Visible,(short)1,(String)"",(String)edtavClonarcontrato_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavClonarcontrato_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVCLONARCONTRATO.CLICK."+sGXsfl_98_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV103ClonarContrato_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_NUMERO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_NUMEROATA"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_ANO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_DATAASSINATURA"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, A85Contrato_DataAssinatura));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_DATATERMINO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, A1869Contrato_DataTermino));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_CALCULODIVERGENCIA"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, StringUtil.RTrim( context.localUtil.Format( A452Contrato_CalculoDivergencia, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_INDICEDIVERGENCIA"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, context.localUtil.Format( A453Contrato_IndiceDivergencia, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_VALORUNIDADECONTRATACAO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_ATIVO"+"_"+sGXsfl_98_idx, GetSecureSignedToken( sPrefix+sGXsfl_98_idx, A92Contrato_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_98_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_98_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_98_idx+1));
            sGXsfl_98_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_98_idx), 4, 0)), 4, "0");
            SubsflControlProps_982( ) ;
         }
         /* End function sendrow_982 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavContrato_numeroata1_Internalname = sPrefix+"vCONTRATO_NUMEROATA1";
         edtavContrato_numero1_Internalname = sPrefix+"vCONTRATO_NUMERO1";
         edtavContratada_pessoanom1_Internalname = sPrefix+"vCONTRATADA_PESSOANOM1";
         edtavContrato_ano1_Internalname = sPrefix+"vCONTRATO_ANO1";
         lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname = sPrefix+"DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT1";
         edtavContrato_ano_to1_Internalname = sPrefix+"vCONTRATO_ANO_TO1";
         tblTablemergeddynamicfilterscontrato_ano1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavContrato_numeroata2_Internalname = sPrefix+"vCONTRATO_NUMEROATA2";
         edtavContrato_numero2_Internalname = sPrefix+"vCONTRATO_NUMERO2";
         edtavContratada_pessoanom2_Internalname = sPrefix+"vCONTRATADA_PESSOANOM2";
         edtavContrato_ano2_Internalname = sPrefix+"vCONTRATO_ANO2";
         lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname = sPrefix+"DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT2";
         edtavContrato_ano_to2_Internalname = sPrefix+"vCONTRATO_ANO_TO2";
         tblTablemergeddynamicfilterscontrato_ano2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         edtavContrato_numeroata3_Internalname = sPrefix+"vCONTRATO_NUMEROATA3";
         edtavContrato_numero3_Internalname = sPrefix+"vCONTRATO_NUMERO3";
         edtavContratada_pessoanom3_Internalname = sPrefix+"vCONTRATADA_PESSOANOM3";
         edtavContrato_ano3_Internalname = sPrefix+"vCONTRATO_ANO3";
         lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname = sPrefix+"DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT3";
         edtavContrato_ano_to3_Internalname = sPrefix+"vCONTRATO_ANO_TO3";
         tblTablemergeddynamicfilterscontrato_ano3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO";
         edtContrato_NumeroAta_Internalname = sPrefix+"CONTRATO_NUMEROATA";
         edtContrato_Ano_Internalname = sPrefix+"CONTRATO_ANO";
         edtContrato_DataAssinatura_Internalname = sPrefix+"CONTRATO_DATAASSINATURA";
         edtContrato_DataTermino_Internalname = sPrefix+"CONTRATO_DATATERMINO";
         cmbContrato_CalculoDivergencia_Internalname = sPrefix+"CONTRATO_CALCULODIVERGENCIA";
         edtContrato_IndiceDivergencia_Internalname = sPrefix+"CONTRATO_INDICEDIVERGENCIA";
         edtContrato_ValorUnidadeContratacao_Internalname = sPrefix+"CONTRATO_VALORUNIDADECONTRATACAO";
         chkContrato_Ativo_Internalname = sPrefix+"CONTRATO_ATIVO";
         edtavAssociargestor_Internalname = sPrefix+"vASSOCIARGESTOR";
         edtavAssociarauxiliar_Internalname = sPrefix+"vASSOCIARAUXILIAR";
         edtavAssociarsistema_Internalname = sPrefix+"vASSOCIARSISTEMA";
         edtavClonarcontrato_Internalname = sPrefix+"vCLONARCONTRATO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         tblUsertable_uc_mensagem_Internalname = sPrefix+"USERTABLE_UC_MENSAGEM";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContratada_Codigo_Internalname = sPrefix+"CONTRATADA_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavClonarcontrato_Jsonclick = "";
         edtavClonarcontrato_Enabled = 1;
         edtavAssociarsistema_Jsonclick = "";
         edtavAssociarsistema_Visible = -1;
         edtavAssociarsistema_Enabled = 1;
         edtavAssociarauxiliar_Jsonclick = "";
         edtavAssociarauxiliar_Enabled = 1;
         edtavAssociargestor_Jsonclick = "";
         edtavAssociargestor_Enabled = 1;
         edtContrato_ValorUnidadeContratacao_Jsonclick = "";
         edtContrato_IndiceDivergencia_Jsonclick = "";
         cmbContrato_CalculoDivergencia_Jsonclick = "";
         edtContrato_DataTermino_Jsonclick = "";
         edtContrato_DataAssinatura_Jsonclick = "";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavContrato_ano_to1_Jsonclick = "";
         edtavContrato_ano1_Jsonclick = "";
         edtavContrato_ano_to2_Jsonclick = "";
         edtavContrato_ano2_Jsonclick = "";
         edtavContrato_ano_to3_Jsonclick = "";
         edtavContrato_ano3_Jsonclick = "";
         edtavContratada_pessoanom3_Jsonclick = "";
         edtavContrato_numero3_Jsonclick = "";
         edtavContrato_numeroata3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavContratada_pessoanom2_Jsonclick = "";
         edtavContrato_numero2_Jsonclick = "";
         edtavContrato_numeroata2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavContratada_pessoanom1_Jsonclick = "";
         edtavContrato_numero1_Jsonclick = "";
         edtavContrato_numeroata1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavClonarcontrato_Tooltiptext = "Clonar contrato";
         edtavAssociarsistema_Tooltiptext = "Associar os sistemas deste contrato";
         edtavAssociarauxiliar_Tooltiptext = "Clique aqui p/ associar os auxiliares deste contrato";
         edtavAssociargestor_Tooltiptext = "Clique aqui p/ associar os gestores deste contrato";
         edtContrato_ValorUnidadeContratacao_Forecolor = (int)(0xFFFFFF);
         edtContrato_IndiceDivergencia_Forecolor = (int)(0xFFFFFF);
         cmbContrato_CalculoDivergencia.ForeColor = (int)(0xFFFFFF);
         edtContrato_DataTermino_Forecolor = (int)(0xFFFFFF);
         edtContrato_DataAssinatura_Forecolor = (int)(0xFFFFFF);
         edtContrato_Ano_Forecolor = (int)(0xFFFFFF);
         edtContrato_NumeroAta_Forecolor = (int)(0xFFFFFF);
         edtContrato_Numero_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Cl�usulas, Servi�os, Garantias, Anexos, Dados Certame, Termos Aditivos, Ocorr�ncias, Obriga��es";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavAssociarauxiliar_Visible = -1;
         edtavAssociargestor_Visible = -1;
         chkContrato_Ativo_Titleformat = 0;
         edtContrato_ValorUnidadeContratacao_Titleformat = 0;
         edtContrato_IndiceDivergencia_Titleformat = 0;
         cmbContrato_CalculoDivergencia_Titleformat = 0;
         edtContrato_DataAssinatura_Titleformat = 0;
         edtContrato_Ano_Titleformat = 0;
         edtContrato_NumeroAta_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterscontrato_ano3_Visible = 1;
         edtavContratada_pessoanom3_Visible = 1;
         edtavContrato_numero3_Visible = 1;
         edtavContrato_numeroata3_Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano2_Visible = 1;
         edtavContratada_pessoanom2_Visible = 1;
         edtavContrato_numero2_Visible = 1;
         edtavContrato_numeroata2_Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano1_Visible = 1;
         edtavContratada_pessoanom1_Visible = 1;
         edtavContrato_numero1_Visible = 1;
         edtavContrato_numeroata1_Visible = 1;
         edtavClonarcontrato_Visible = -1;
         edtavAssociarsistema_Title = "";
         edtavAssociarauxiliar_Title = "";
         edtavAssociargestor_Title = "";
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         chkContrato_Ativo.Title.Text = "Ativo";
         edtContrato_ValorUnidadeContratacao_Title = "Valor PF";
         edtContrato_IndiceDivergencia_Title = "% Div.";
         cmbContrato_CalculoDivergencia.Title.Text = "Diverg�ncia";
         edtContrato_DataAssinatura_Title = "Assinatura";
         edtContrato_Ano_Title = "Ano";
         edtContrato_NumeroAta_Title = "N� Ata";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkContrato_Ativo.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContratada_Codigo_Jsonclick = "";
         edtContratada_Codigo_Visible = 1;
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Yesbuttonposition = "right";
         Confirmpanel_Nobuttoncaption = "N�o";
         Confirmpanel_Yesbuttoncaption = "OK";
         Confirmpanel_Confirmationtext = "";
         Confirmpanel_Title = "Aten��o";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContrato_NumeroAta_Titleformat',ctrl:'CONTRATO_NUMEROATA',prop:'Titleformat'},{av:'edtContrato_NumeroAta_Title',ctrl:'CONTRATO_NUMEROATA',prop:'Title'},{av:'edtContrato_Ano_Titleformat',ctrl:'CONTRATO_ANO',prop:'Titleformat'},{av:'edtContrato_Ano_Title',ctrl:'CONTRATO_ANO',prop:'Title'},{av:'edtContrato_DataAssinatura_Titleformat',ctrl:'CONTRATO_DATAASSINATURA',prop:'Titleformat'},{av:'edtContrato_DataAssinatura_Title',ctrl:'CONTRATO_DATAASSINATURA',prop:'Title'},{av:'cmbContrato_CalculoDivergencia'},{av:'edtContrato_IndiceDivergencia_Titleformat',ctrl:'CONTRATO_INDICEDIVERGENCIA',prop:'Titleformat'},{av:'edtContrato_IndiceDivergencia_Title',ctrl:'CONTRATO_INDICEDIVERGENCIA',prop:'Title'},{av:'edtContrato_ValorUnidadeContratacao_Titleformat',ctrl:'CONTRATO_VALORUNIDADECONTRATACAO',prop:'Titleformat'},{av:'edtContrato_ValorUnidadeContratacao_Title',ctrl:'CONTRATO_VALORUNIDADECONTRATACAO',prop:'Title'},{av:'chkContrato_Ativo_Titleformat',ctrl:'CONTRATO_ATIVO',prop:'Titleformat'},{av:'chkContrato_Ativo.Title.Text',ctrl:'CONTRATO_ATIVO',prop:'Title'},{av:'AV91GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV92GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'edtavAssociargestor_Title',ctrl:'vASSOCIARGESTOR',prop:'Title'},{av:'edtavAssociarauxiliar_Title',ctrl:'vASSOCIARAUXILIAR',prop:'Title'},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavClonarcontrato_Visible',ctrl:'vCLONARCONTRATO',prop:'Visible'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11862',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E25862',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV38Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV39Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV40Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV49AssociarGestor',fld:'vASSOCIARGESTOR',pic:'',nv:''},{av:'edtavAssociargestor_Tooltiptext',ctrl:'vASSOCIARGESTOR',prop:'Tooltiptext'},{av:'AV93AssociarAuxiliar',fld:'vASSOCIARAUXILIAR',pic:'',nv:''},{av:'edtavAssociarauxiliar_Tooltiptext',ctrl:'vASSOCIARAUXILIAR',prop:'Tooltiptext'},{av:'AV50AssociarSistema',fld:'vASSOCIARSISTEMA',pic:'',nv:''},{av:'edtavAssociarsistema_Tooltiptext',ctrl:'vASSOCIARSISTEMA',prop:'Tooltiptext'},{av:'AV103ClonarContrato',fld:'vCLONARCONTRATO',pic:'',nv:''},{av:'edtavClonarcontrato_Tooltiptext',ctrl:'vCLONARCONTRATO',prop:'Tooltiptext'},{av:'edtContrato_Numero_Forecolor',ctrl:'CONTRATO_NUMERO',prop:'Forecolor'},{av:'edtContrato_NumeroAta_Forecolor',ctrl:'CONTRATO_NUMEROATA',prop:'Forecolor'},{av:'edtContrato_Ano_Forecolor',ctrl:'CONTRATO_ANO',prop:'Forecolor'},{av:'edtContrato_DataAssinatura_Forecolor',ctrl:'CONTRATO_DATAASSINATURA',prop:'Forecolor'},{av:'edtContrato_DataTermino_Forecolor',ctrl:'CONTRATO_DATATERMINO',prop:'Forecolor'},{av:'cmbContrato_CalculoDivergencia'},{av:'edtContrato_IndiceDivergencia_Forecolor',ctrl:'CONTRATO_INDICEDIVERGENCIA',prop:'Forecolor'},{av:'edtContrato_ValorUnidadeContratacao_Forecolor',ctrl:'CONTRATO_VALORUNIDADECONTRATACAO',prop:'Forecolor'},{av:'edtavAssociargestor_Visible',ctrl:'vASSOCIARGESTOR',prop:'Visible'},{av:'edtavAssociarauxiliar_Visible',ctrl:'vASSOCIARAUXILIAR',prop:'Visible'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12862',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18862',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13862',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19862',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20862',iparms:[],oparms:[{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14862',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21862',iparms:[{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15862',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E22862',iparms:[{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16862',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV115Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV37DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV36DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV42Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV44Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV29DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV30DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV46Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV20Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV21Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV26Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV27Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV28Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV32Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV33Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV34Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV35Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'}]}");
         setEventMetadata("'DOASSOCIARGESTOR'","{handler:'E27862',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARAUXILIAR'","{handler:'E28862',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARSISTEMA'","{handler:'E29862',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VCLONARCONTRATO.CLICK","{handler:'E26862',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E17862',iparms:[{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Confirmpanel_Title',ctrl:'CONFIRMPANEL',prop:'Title'},{av:'Confirmpanel_Confirmationtext',ctrl:'CONFIRMPANEL',prop:'ConfirmationText'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV42Contrato_NumeroAta1 = "";
         AV18Contrato_Numero1 = "";
         AV19Contratada_PessoaNom1 = "";
         AV23DynamicFiltersSelector2 = "";
         AV44Contrato_NumeroAta2 = "";
         AV25Contrato_Numero2 = "";
         AV26Contratada_PessoaNom2 = "";
         AV30DynamicFiltersSelector3 = "";
         AV46Contrato_NumeroAta3 = "";
         AV32Contrato_Numero3 = "";
         AV33Contratada_PessoaNom3 = "";
         AV115Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A843Contrato_DataFimTA = DateTime.MinValue;
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV38Update = "";
         AV107Update_GXI = "";
         AV39Delete = "";
         AV108Delete_GXI = "";
         AV40Display = "";
         AV109Display_GXI = "";
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         A85Contrato_DataAssinatura = DateTime.MinValue;
         A1869Contrato_DataTermino = DateTime.MinValue;
         A452Contrato_CalculoDivergencia = "";
         AV49AssociarGestor = "";
         AV110Associargestor_GXI = "";
         AV93AssociarAuxiliar = "";
         AV111Associarauxiliar_GXI = "";
         AV50AssociarSistema = "";
         AV112Associarsistema_GXI = "";
         AV103ClonarContrato = "";
         AV113Clonarcontrato_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV42Contrato_NumeroAta1 = "";
         lV18Contrato_Numero1 = "";
         lV19Contratada_PessoaNom1 = "";
         lV44Contrato_NumeroAta2 = "";
         lV25Contrato_Numero2 = "";
         lV26Contratada_PessoaNom2 = "";
         lV46Contrato_NumeroAta3 = "";
         lV32Contrato_Numero3 = "";
         lV33Contratada_PessoaNom3 = "";
         A41Contratada_PessoaNom = "";
         H00864_A40Contratada_PessoaCod = new int[1] ;
         H00864_A74Contrato_Codigo = new int[1] ;
         H00864_n74Contrato_Codigo = new bool[] {false} ;
         H00864_A41Contratada_PessoaNom = new String[] {""} ;
         H00864_n41Contratada_PessoaNom = new bool[] {false} ;
         H00864_A1013Contrato_PrepostoCod = new int[1] ;
         H00864_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00864_A39Contratada_Codigo = new int[1] ;
         H00864_A92Contrato_Ativo = new bool[] {false} ;
         H00864_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H00864_A453Contrato_IndiceDivergencia = new decimal[1] ;
         H00864_A452Contrato_CalculoDivergencia = new String[] {""} ;
         H00864_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         H00864_A79Contrato_Ano = new short[1] ;
         H00864_A78Contrato_NumeroAta = new String[] {""} ;
         H00864_n78Contrato_NumeroAta = new bool[] {false} ;
         H00864_A77Contrato_Numero = new String[] {""} ;
         H00864_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00864_n843Contrato_DataFimTA = new bool[] {false} ;
         H00864_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00867_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         H00868_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00868_A1079ContratoGestor_UsuarioCod = new int[1] ;
         GridRow = new GXWebRow();
         AV41Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contratada_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratadacontratowc__default(),
            new Object[][] {
                new Object[] {
               H00864_A40Contratada_PessoaCod, H00864_A74Contrato_Codigo, H00864_A41Contratada_PessoaNom, H00864_n41Contratada_PessoaNom, H00864_A1013Contrato_PrepostoCod, H00864_n1013Contrato_PrepostoCod, H00864_A39Contratada_Codigo, H00864_A92Contrato_Ativo, H00864_A116Contrato_ValorUnidadeContratacao, H00864_A453Contrato_IndiceDivergencia,
               H00864_A452Contrato_CalculoDivergencia, H00864_A85Contrato_DataAssinatura, H00864_A79Contrato_Ano, H00864_A78Contrato_NumeroAta, H00864_n78Contrato_NumeroAta, H00864_A77Contrato_Numero, H00864_A843Contrato_DataFimTA, H00864_n843Contrato_DataFimTA, H00864_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00867_AGRID_nRecordCount
               }
               , new Object[] {
               H00868_A1078ContratoGestor_ContratoCod, H00868_A1079ContratoGestor_UsuarioCod
               }
            }
         );
         AV115Pgmname = "ContratadaContratoWC";
         /* GeneXus formulas. */
         AV115Pgmname = "ContratadaContratoWC";
         context.Gx_err = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_98 ;
      private short nGXsfl_98_idx=1 ;
      private short AV14OrderedBy ;
      private short AV20Contrato_Ano1 ;
      private short AV21Contrato_Ano_To1 ;
      private short AV27Contrato_Ano2 ;
      private short AV28Contrato_Ano_To2 ;
      private short AV34Contrato_Ano3 ;
      private short AV35Contrato_Ano_To3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A79Contrato_Ano ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_98_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContrato_NumeroAta_Titleformat ;
      private short edtContrato_Ano_Titleformat ;
      private short edtContrato_DataAssinatura_Titleformat ;
      private short cmbContrato_CalculoDivergencia_Titleformat ;
      private short edtContrato_IndiceDivergencia_Titleformat ;
      private short edtContrato_ValorUnidadeContratacao_Titleformat ;
      private short chkContrato_Ativo_Titleformat ;
      private short AV114GXLvl156 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Contratada_Codigo ;
      private int wcpOAV7Contratada_Codigo ;
      private int subGrid_Rows ;
      private int A74Contrato_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A39Contratada_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtContratada_Codigo_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A40Contratada_PessoaCod ;
      private int edtavOrdereddsc_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int imgInsert_Visible ;
      private int edtavClonarcontrato_Visible ;
      private int AV90PageToGo ;
      private int edtContrato_Numero_Forecolor ;
      private int edtContrato_NumeroAta_Forecolor ;
      private int edtContrato_Ano_Forecolor ;
      private int edtContrato_DataAssinatura_Forecolor ;
      private int edtContrato_DataTermino_Forecolor ;
      private int edtContrato_IndiceDivergencia_Forecolor ;
      private int edtContrato_ValorUnidadeContratacao_Forecolor ;
      private int edtavAssociargestor_Visible ;
      private int edtavAssociarauxiliar_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContrato_numeroata1_Visible ;
      private int edtavContrato_numero1_Visible ;
      private int edtavContratada_pessoanom1_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano1_Visible ;
      private int edtavContrato_numeroata2_Visible ;
      private int edtavContrato_numero2_Visible ;
      private int edtavContratada_pessoanom2_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano2_Visible ;
      private int edtavContrato_numeroata3_Visible ;
      private int edtavContrato_numero3_Visible ;
      private int edtavContratada_pessoanom3_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavAssociargestor_Enabled ;
      private int edtavAssociarauxiliar_Enabled ;
      private int edtavAssociarsistema_Enabled ;
      private int edtavAssociarsistema_Visible ;
      private int edtavClonarcontrato_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long AV91GridCurrentPage ;
      private long AV92GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV104Color ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_98_idx="0001" ;
      private String AV42Contrato_NumeroAta1 ;
      private String AV18Contrato_Numero1 ;
      private String AV19Contratada_PessoaNom1 ;
      private String AV44Contrato_NumeroAta2 ;
      private String AV25Contrato_Numero2 ;
      private String AV26Contratada_PessoaNom2 ;
      private String AV46Contrato_NumeroAta3 ;
      private String AV32Contrato_Numero3 ;
      private String AV33Contratada_PessoaNom3 ;
      private String AV115Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Nobuttoncaption ;
      private String Confirmpanel_Yesbuttonposition ;
      private String Confirmpanel_Confirmtype ;
      private String GX_FocusControl ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Internalname ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_DataAssinatura_Internalname ;
      private String edtContrato_DataTermino_Internalname ;
      private String cmbContrato_CalculoDivergencia_Internalname ;
      private String A452Contrato_CalculoDivergencia ;
      private String edtContrato_IndiceDivergencia_Internalname ;
      private String edtContrato_ValorUnidadeContratacao_Internalname ;
      private String chkContrato_Ativo_Internalname ;
      private String edtavAssociargestor_Internalname ;
      private String edtavAssociarauxiliar_Internalname ;
      private String edtavAssociarsistema_Internalname ;
      private String edtavClonarcontrato_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV42Contrato_NumeroAta1 ;
      private String lV18Contrato_Numero1 ;
      private String lV19Contratada_PessoaNom1 ;
      private String lV44Contrato_NumeroAta2 ;
      private String lV25Contrato_Numero2 ;
      private String lV26Contratada_PessoaNom2 ;
      private String lV46Contrato_NumeroAta3 ;
      private String lV32Contrato_Numero3 ;
      private String lV33Contratada_PessoaNom3 ;
      private String A41Contratada_PessoaNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContrato_numeroata1_Internalname ;
      private String edtavContrato_numero1_Internalname ;
      private String edtavContratada_pessoanom1_Internalname ;
      private String edtavContrato_ano1_Internalname ;
      private String edtavContrato_ano_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContrato_numeroata2_Internalname ;
      private String edtavContrato_numero2_Internalname ;
      private String edtavContratada_pessoanom2_Internalname ;
      private String edtavContrato_ano2_Internalname ;
      private String edtavContrato_ano_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContrato_numeroata3_Internalname ;
      private String edtavContrato_numero3_Internalname ;
      private String edtavContratada_pessoanom3_Internalname ;
      private String edtavContrato_ano3_Internalname ;
      private String edtavContrato_ano_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContrato_NumeroAta_Title ;
      private String edtContrato_Ano_Title ;
      private String edtContrato_DataAssinatura_Title ;
      private String edtContrato_IndiceDivergencia_Title ;
      private String edtContrato_ValorUnidadeContratacao_Title ;
      private String edtavAssociargestor_Title ;
      private String edtavAssociarauxiliar_Title ;
      private String edtavAssociarsistema_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavAssociargestor_Tooltiptext ;
      private String edtavAssociarauxiliar_Tooltiptext ;
      private String edtavAssociarsistema_Tooltiptext ;
      private String edtavClonarcontrato_Tooltiptext ;
      private String Confirmpanel_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_ano1_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_ano2_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_ano3_Internalname ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblUsertable_uc_mensagem_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContrato_numeroata1_Jsonclick ;
      private String edtavContrato_numero1_Jsonclick ;
      private String edtavContratada_pessoanom1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavContrato_numeroata2_Jsonclick ;
      private String edtavContrato_numero2_Jsonclick ;
      private String edtavContratada_pessoanom2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContrato_numeroata3_Jsonclick ;
      private String edtavContrato_numero3_Jsonclick ;
      private String edtavContratada_pessoanom3_Jsonclick ;
      private String edtavContrato_ano3_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick ;
      private String edtavContrato_ano_to3_Jsonclick ;
      private String edtavContrato_ano2_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick ;
      private String edtavContrato_ano_to2_Jsonclick ;
      private String edtavContrato_ano1_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick ;
      private String edtavContrato_ano_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Contratada_Codigo ;
      private String sGXsfl_98_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String edtContrato_Ano_Jsonclick ;
      private String edtContrato_DataAssinatura_Jsonclick ;
      private String edtContrato_DataTermino_Jsonclick ;
      private String cmbContrato_CalculoDivergencia_Jsonclick ;
      private String edtContrato_IndiceDivergencia_Jsonclick ;
      private String edtContrato_ValorUnidadeContratacao_Jsonclick ;
      private String edtavAssociargestor_Jsonclick ;
      private String edtavAssociarauxiliar_Jsonclick ;
      private String edtavAssociarsistema_Jsonclick ;
      private String edtavClonarcontrato_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A85Contrato_DataAssinatura ;
      private DateTime A1869Contrato_DataTermino ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV22DynamicFiltersEnabled2 ;
      private bool AV29DynamicFiltersEnabled3 ;
      private bool AV37DynamicFiltersIgnoreFirst ;
      private bool AV36DynamicFiltersRemoving ;
      private bool n74Contrato_Codigo ;
      private bool A92Contrato_Ativo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n843Contrato_DataFimTA ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV38Update_IsBlob ;
      private bool AV39Delete_IsBlob ;
      private bool AV40Display_IsBlob ;
      private bool AV49AssociarGestor_IsBlob ;
      private bool AV93AssociarAuxiliar_IsBlob ;
      private bool AV50AssociarSistema_IsBlob ;
      private bool AV103ClonarContrato_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV23DynamicFiltersSelector2 ;
      private String AV30DynamicFiltersSelector3 ;
      private String AV107Update_GXI ;
      private String AV108Delete_GXI ;
      private String AV109Display_GXI ;
      private String AV110Associargestor_GXI ;
      private String AV111Associarauxiliar_GXI ;
      private String AV112Associarsistema_GXI ;
      private String AV113Clonarcontrato_GXI ;
      private String AV38Update ;
      private String AV39Delete ;
      private String AV40Display ;
      private String AV49AssociarGestor ;
      private String AV93AssociarAuxiliar ;
      private String AV50AssociarSistema ;
      private String AV103ClonarContrato ;
      private IGxSession AV41Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbContrato_CalculoDivergencia ;
      private GXCheckbox chkContrato_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00864_A40Contratada_PessoaCod ;
      private int[] H00864_A74Contrato_Codigo ;
      private bool[] H00864_n74Contrato_Codigo ;
      private String[] H00864_A41Contratada_PessoaNom ;
      private bool[] H00864_n41Contratada_PessoaNom ;
      private int[] H00864_A1013Contrato_PrepostoCod ;
      private bool[] H00864_n1013Contrato_PrepostoCod ;
      private int[] H00864_A39Contratada_Codigo ;
      private bool[] H00864_A92Contrato_Ativo ;
      private decimal[] H00864_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] H00864_A453Contrato_IndiceDivergencia ;
      private String[] H00864_A452Contrato_CalculoDivergencia ;
      private DateTime[] H00864_A85Contrato_DataAssinatura ;
      private short[] H00864_A79Contrato_Ano ;
      private String[] H00864_A78Contrato_NumeroAta ;
      private bool[] H00864_n78Contrato_NumeroAta ;
      private String[] H00864_A77Contrato_Numero ;
      private DateTime[] H00864_A843Contrato_DataFimTA ;
      private bool[] H00864_n843Contrato_DataFimTA ;
      private DateTime[] H00864_A83Contrato_DataVigenciaTermino ;
      private long[] H00867_AGRID_nRecordCount ;
      private int[] H00868_A1078ContratoGestor_ContratoCod ;
      private int[] H00868_A1079ContratoGestor_UsuarioCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class contratadacontratowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00864( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV42Contrato_NumeroAta1 ,
                                             String AV18Contrato_Numero1 ,
                                             String AV19Contratada_PessoaNom1 ,
                                             short AV20Contrato_Ano1 ,
                                             short AV21Contrato_Ano_To1 ,
                                             bool AV22DynamicFiltersEnabled2 ,
                                             String AV23DynamicFiltersSelector2 ,
                                             String AV44Contrato_NumeroAta2 ,
                                             String AV25Contrato_Numero2 ,
                                             String AV26Contratada_PessoaNom2 ,
                                             short AV27Contrato_Ano2 ,
                                             short AV28Contrato_Ano_To2 ,
                                             bool AV29DynamicFiltersEnabled3 ,
                                             String AV30DynamicFiltersSelector3 ,
                                             String AV46Contrato_NumeroAta3 ,
                                             String AV32Contrato_Numero3 ,
                                             String AV33Contratada_PessoaNom3 ,
                                             short AV34Contrato_Ano3 ,
                                             short AV35Contrato_Ano_To3 ,
                                             String A78Contrato_NumeroAta ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A39Contratada_Codigo ,
                                             int AV7Contratada_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [21] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_PrepostoCod], T1.[Contratada_Codigo], T1.[Contrato_Ativo], T1.[Contrato_ValorUnidadeContratacao], T1.[Contrato_IndiceDivergencia], T1.[Contrato_CalculoDivergencia], T1.[Contrato_DataAssinatura], T1.[Contrato_Ano], T1.[Contrato_NumeroAta], T1.[Contrato_Numero], COALESCE( T2.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino]";
         sFromString = " FROM ((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T5.[ContratoTermoAditivo_DataFim], T5.[Contrato_Codigo], T5.[ContratoTermoAditivo_Codigo], T6.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T5 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) WHERE T5.[ContratoTermoAditivo_Codigo] = T6.[GXC4] ) T2 ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contratada_Codigo] = @AV7Contratada_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV42Contrato_NumeroAta1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contrato_Numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV18Contrato_Numero1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV19Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV20Contrato_Ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV20Contrato_Ano1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV21Contrato_Ano_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV21Contrato_Ano_To1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contrato_Numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV25Contrato_Numero2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV26Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV27Contrato_Ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV27Contrato_Ano2)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV28Contrato_Ano_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV28Contrato_Ano_To2)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Contrato_NumeroAta3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV46Contrato_NumeroAta3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Contrato_Numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV32Contrato_Numero3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Contratada_PessoaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV33Contratada_PessoaNom3)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV34Contrato_Ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV34Contrato_Ano3)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV35Contrato_Ano_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV35Contrato_Ano_To3)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_NumeroAta]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_NumeroAta] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_Numero]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_Numero] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_Ano]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_Ano] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_DataAssinatura]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_DataAssinatura] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_CalculoDivergencia]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_CalculoDivergencia] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_IndiceDivergencia]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_IndiceDivergencia] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_ValorUnidadeContratacao]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_ValorUnidadeContratacao] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo], T1.[Contrato_Ativo]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo] DESC, T1.[Contrato_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00867( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV42Contrato_NumeroAta1 ,
                                             String AV18Contrato_Numero1 ,
                                             String AV19Contratada_PessoaNom1 ,
                                             short AV20Contrato_Ano1 ,
                                             short AV21Contrato_Ano_To1 ,
                                             bool AV22DynamicFiltersEnabled2 ,
                                             String AV23DynamicFiltersSelector2 ,
                                             String AV44Contrato_NumeroAta2 ,
                                             String AV25Contrato_Numero2 ,
                                             String AV26Contratada_PessoaNom2 ,
                                             short AV27Contrato_Ano2 ,
                                             short AV28Contrato_Ano_To2 ,
                                             bool AV29DynamicFiltersEnabled3 ,
                                             String AV30DynamicFiltersSelector3 ,
                                             String AV46Contrato_NumeroAta3 ,
                                             String AV32Contrato_Numero3 ,
                                             String AV33Contratada_PessoaNom3 ,
                                             short AV34Contrato_Ano3 ,
                                             short AV35Contrato_Ano_To3 ,
                                             String A78Contrato_NumeroAta ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             short A79Contrato_Ano ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A39Contratada_Codigo ,
                                             int AV7Contratada_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([Contrato] T1 WITH (NOLOCK) LEFT JOIN (SELECT T5.[ContratoTermoAditivo_DataFim], T5.[Contrato_Codigo], T5.[ContratoTermoAditivo_Codigo], T6.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T5 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T6 ON T6.[Contrato_Codigo] = T5.[Contrato_Codigo]) WHERE T5.[ContratoTermoAditivo_Codigo] = T6.[GXC4] ) T4 ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_Codigo] = @AV7Contratada_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_NumeroAta1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV42Contrato_NumeroAta1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contrato_Numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV18Contrato_Numero1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV19Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV20Contrato_Ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV20Contrato_Ano1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV21Contrato_Ano_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV21Contrato_Ano_To1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Contrato_NumeroAta2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV44Contrato_NumeroAta2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Contrato_Numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV25Contrato_Numero2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV26Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV27Contrato_Ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV27Contrato_Ano2)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV22DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV28Contrato_Ano_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV28Contrato_Ano_To2)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Contrato_NumeroAta3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV46Contrato_NumeroAta3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32Contrato_Numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV32Contrato_Numero3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Contratada_PessoaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV33Contratada_PessoaNom3)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV34Contrato_Ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV34Contrato_Ano3)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV29DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV35Contrato_Ano_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV35Contrato_Ano_To3)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00864(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (short)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
               case 1 :
                     return conditional_H00867(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (short)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00868 ;
          prmH00868 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00864 ;
          prmH00864 = new Object[] {
          new Object[] {"@AV7Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV42Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV19Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV20Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV44Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV25Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV26Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV27Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV28Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV46Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV32Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV33Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV34Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35Contrato_Ano_To3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00867 ;
          prmH00867 = new Object[] {
          new Object[] {"@AV7Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV42Contrato_NumeroAta1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV18Contrato_Numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV19Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV20Contrato_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21Contrato_Ano_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV44Contrato_NumeroAta2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV25Contrato_Numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV26Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV27Contrato_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV28Contrato_Ano_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV46Contrato_NumeroAta3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV32Contrato_Numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV33Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV34Contrato_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35Contrato_Ano_To3",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00864", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00864,11,0,true,false )
             ,new CursorDef("H00867", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00867,1,0,true,false )
             ,new CursorDef("H00868", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @Contrato_Codigo and [ContratoGestor_UsuarioCod] = @AV6WWPContext__Userid ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00868,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 1) ;
                ((DateTime[]) buf[11])[0] = rslt.getGXDate(10) ;
                ((short[]) buf[12])[0] = rslt.getShort(11) ;
                ((String[]) buf[13])[0] = rslt.getString(12, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(12);
                ((String[]) buf[15])[0] = rslt.getString(13, 20) ;
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(15) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (short)parms[2]);
                return;
       }
    }

 }

}
