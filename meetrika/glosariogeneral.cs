/*
               File: GlosarioGeneral
        Description: Glosario General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:35.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class glosariogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public glosariogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public glosariogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Glosario_Codigo )
      {
         this.A1347Glosario_Codigo = aP0_Glosario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1347Glosario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1347Glosario_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAFA2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "GlosarioGeneral";
               context.Gx_err = 0;
               WSFA2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Glosario General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117233530");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("glosariogeneral.aspx") + "?" + UrlEncode("" +A1347Glosario_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1347Glosario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1347Glosario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GLOSARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1347Glosario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_GLOSARIO_TERMO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_GLOSARIO_DESCRICAO", GetSecureSignedToken( sPrefix, A859Glosario_Descricao));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormFA2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("glosariogeneral.js", "?20203117233532");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "GlosarioGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Glosario General" ;
      }

      protected void WBFA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "glosariogeneral.aspx");
            }
            wb_table1_2_FA2( true) ;
         }
         else
         {
            wb_table1_2_FA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FA2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTFA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Glosario General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPFA0( ) ;
            }
         }
      }

      protected void WSFA2( )
      {
         STARTFA2( ) ;
         EVTFA2( ) ;
      }

      protected void EVTFA2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11FA2 */
                                    E11FA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12FA2 */
                                    E12FA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13FA2 */
                                    E13FA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14FA2 */
                                    E14FA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15FA2 */
                                    E15FA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPFA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormFA2( ) ;
            }
         }
      }

      protected void PAFA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "GlosarioGeneral";
         context.Gx_err = 0;
      }

      protected void RFFA2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00FA2 */
            pr_default.execute(0, new Object[] {A1347Glosario_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1343Glosario_NomeArq = H00FA2_A1343Glosario_NomeArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1343Glosario_NomeArq", A1343Glosario_NomeArq);
               n1343Glosario_NomeArq = H00FA2_n1343Glosario_NomeArq[0];
               edtGlosario_Arquivo_Filename = A1343Glosario_NomeArq;
               A1344Glosario_TipoArq = H00FA2_A1344Glosario_TipoArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1344Glosario_TipoArq", A1344Glosario_TipoArq);
               n1344Glosario_TipoArq = H00FA2_n1344Glosario_TipoArq[0];
               edtGlosario_Arquivo_Filetype = A1344Glosario_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
               A859Glosario_Descricao = H00FA2_A859Glosario_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A859Glosario_Descricao", A859Glosario_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_GLOSARIO_DESCRICAO", GetSecureSignedToken( sPrefix, A859Glosario_Descricao));
               A858Glosario_Termo = H00FA2_A858Glosario_Termo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A858Glosario_Termo", A858Glosario_Termo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_GLOSARIO_TERMO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, ""))));
               A1342Glosario_Arquivo = H00FA2_A1342Glosario_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
               n1342Glosario_Arquivo = H00FA2_n1342Glosario_Arquivo[0];
               /* Execute user event: E12FA2 */
               E12FA2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBFA0( ) ;
         }
      }

      protected void STRUPFA0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "GlosarioGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11FA2 */
         E11FA2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A858Glosario_Termo = cgiGet( edtGlosario_Termo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A858Glosario_Termo", A858Glosario_Termo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_GLOSARIO_TERMO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, ""))));
            A859Glosario_Descricao = cgiGet( edtGlosario_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A859Glosario_Descricao", A859Glosario_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_GLOSARIO_DESCRICAO", GetSecureSignedToken( sPrefix, A859Glosario_Descricao));
            A1342Glosario_Arquivo = cgiGet( edtGlosario_Arquivo_Internalname);
            n1342Glosario_Arquivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
            /* Read saved values. */
            wcpOA1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1347Glosario_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11FA2 */
         E11FA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11FA2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtGlosario_Arquivo_Display = 1;
         edtGlosario_Arquivo_Tooltiptext = "Arquivo anexo";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "Tooltiptext", edtGlosario_Arquivo_Tooltiptext);
         edtGlosario_Arquivo_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "Linktarget", edtGlosario_Arquivo_Linktarget);
      }

      protected void nextLoad( )
      {
      }

      protected void E12FA2( )
      {
         /* Load Routine */
      }

      protected void E13FA2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("glosario.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1347Glosario_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14FA2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("glosario.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1347Glosario_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15FA2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwglosario.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Glosario";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Glosario_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV12Glosario_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_FA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_FA2( true) ;
         }
         else
         {
            wb_table2_8_FA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_FA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_39_FA2( true) ;
         }
         else
         {
            wb_table3_39_FA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_39_FA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FA2e( true) ;
         }
         else
         {
            wb_table1_2_FA2e( false) ;
         }
      }

      protected void wb_table3_39_FA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_FA2e( true) ;
         }
         else
         {
            wb_table3_39_FA2e( false) ;
         }
      }

      protected void wb_table2_8_FA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_termo_Internalname, "Termo", "", "", lblTextblockglosario_termo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGlosario_Termo_Internalname, A858Glosario_Termo, StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGlosario_Termo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_descricao_Internalname, "Descri��o", "", "", lblTextblockglosario_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtGlosario_Descricao_Internalname, A859Glosario_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_arquivo_Internalname, "Arquivo", "", "", lblTextblockglosario_arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            ClassString = "ReadonlyBootstrapAttribute";
            StyleString = "";
            edtGlosario_Arquivo_Filename = A1343Glosario_NomeArq;
            edtGlosario_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            edtGlosario_Arquivo_Filetype = A1344Glosario_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) )
            {
               gxblobfileaux.Source = A1342Glosario_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtGlosario_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtGlosario_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A1342Glosario_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n1342Glosario_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
                  edtGlosario_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtGlosario_Arquivo_Internalname, StringUtil.RTrim( A1342Glosario_Arquivo), context.PathToRelativeUrl( A1342Glosario_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtGlosario_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtGlosario_Arquivo_Filetype)) ? A1342Glosario_Arquivo : edtGlosario_Arquivo_Filetype)) : edtGlosario_Arquivo_Contenttype), true, edtGlosario_Arquivo_Linktarget, edtGlosario_Arquivo_Parameters, edtGlosario_Arquivo_Display, 0, 1, "", edtGlosario_Arquivo_Tooltiptext, 0, -1, 250, "px", 60, "px", 0, 0, 0, edtGlosario_Arquivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+"", "", "", "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_nomearq_Internalname, "Nome de Arquivo", "", "", lblTextblockglosario_nomearq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGlosario_NomeArq_Internalname, StringUtil.RTrim( A1343Glosario_NomeArq), StringUtil.RTrim( context.localUtil.Format( A1343Glosario_NomeArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGlosario_NomeArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_tipoarq_Internalname, "Tipo de Arquivo", "", "", lblTextblockglosario_tipoarq_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGlosario_TipoArq_Internalname, StringUtil.RTrim( A1344Glosario_TipoArq), StringUtil.RTrim( context.localUtil.Format( A1344Glosario_TipoArq, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGlosario_TipoArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_GlosarioGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_FA2e( true) ;
         }
         else
         {
            wb_table2_8_FA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1347Glosario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFA2( ) ;
         WSFA2( ) ;
         WEFA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1347Glosario_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAFA2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "glosariogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAFA2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1347Glosario_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
         }
         wcpOA1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1347Glosario_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1347Glosario_Codigo != wcpOA1347Glosario_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1347Glosario_Codigo = A1347Glosario_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1347Glosario_Codigo = cgiGet( sPrefix+"A1347Glosario_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1347Glosario_Codigo) > 0 )
         {
            A1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1347Glosario_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
         }
         else
         {
            A1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1347Glosario_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAFA2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSFA2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSFA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1347Glosario_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1347Glosario_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1347Glosario_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1347Glosario_Codigo_CTRL", StringUtil.RTrim( sCtrlA1347Glosario_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEFA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117233566");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("glosariogeneral.js", "?20203117233567");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockglosario_termo_Internalname = sPrefix+"TEXTBLOCKGLOSARIO_TERMO";
         edtGlosario_Termo_Internalname = sPrefix+"GLOSARIO_TERMO";
         lblTextblockglosario_descricao_Internalname = sPrefix+"TEXTBLOCKGLOSARIO_DESCRICAO";
         edtGlosario_Descricao_Internalname = sPrefix+"GLOSARIO_DESCRICAO";
         lblTextblockglosario_arquivo_Internalname = sPrefix+"TEXTBLOCKGLOSARIO_ARQUIVO";
         edtGlosario_Arquivo_Internalname = sPrefix+"GLOSARIO_ARQUIVO";
         lblTextblockglosario_nomearq_Internalname = sPrefix+"TEXTBLOCKGLOSARIO_NOMEARQ";
         edtGlosario_NomeArq_Internalname = sPrefix+"GLOSARIO_NOMEARQ";
         lblTextblockglosario_tipoarq_Internalname = sPrefix+"TEXTBLOCKGLOSARIO_TIPOARQ";
         edtGlosario_TipoArq_Internalname = sPrefix+"GLOSARIO_TIPOARQ";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtGlosario_TipoArq_Jsonclick = "";
         edtGlosario_NomeArq_Jsonclick = "";
         edtGlosario_Arquivo_Jsonclick = "";
         edtGlosario_Arquivo_Parameters = "";
         edtGlosario_Arquivo_Contenttype = "";
         edtGlosario_Arquivo_Display = 0;
         edtGlosario_Termo_Jsonclick = "";
         edtGlosario_Arquivo_Linktarget = "";
         edtGlosario_Arquivo_Tooltiptext = "";
         edtGlosario_Arquivo_Filetype = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13FA2',iparms:[{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14FA2',iparms:[{av:'A1347Glosario_Codigo',fld:'GLOSARIO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15FA2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A858Glosario_Termo = "";
         A859Glosario_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00FA2_A1347Glosario_Codigo = new int[1] ;
         H00FA2_A1343Glosario_NomeArq = new String[] {""} ;
         H00FA2_n1343Glosario_NomeArq = new bool[] {false} ;
         H00FA2_A1344Glosario_TipoArq = new String[] {""} ;
         H00FA2_n1344Glosario_TipoArq = new bool[] {false} ;
         H00FA2_A859Glosario_Descricao = new String[] {""} ;
         H00FA2_A858Glosario_Termo = new String[] {""} ;
         H00FA2_A1342Glosario_Arquivo = new String[] {""} ;
         H00FA2_n1342Glosario_Arquivo = new bool[] {false} ;
         A1343Glosario_NomeArq = "";
         edtGlosario_Arquivo_Filename = "";
         A1344Glosario_TipoArq = "";
         A1342Glosario_Arquivo = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockglosario_termo_Jsonclick = "";
         lblTextblockglosario_descricao_Jsonclick = "";
         lblTextblockglosario_arquivo_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblockglosario_nomearq_Jsonclick = "";
         lblTextblockglosario_tipoarq_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1347Glosario_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.glosariogeneral__default(),
            new Object[][] {
                new Object[] {
               H00FA2_A1347Glosario_Codigo, H00FA2_A1343Glosario_NomeArq, H00FA2_n1343Glosario_NomeArq, H00FA2_A1344Glosario_TipoArq, H00FA2_n1344Glosario_TipoArq, H00FA2_A859Glosario_Descricao, H00FA2_A858Glosario_Termo, H00FA2_A1342Glosario_Arquivo, H00FA2_n1342Glosario_Arquivo
               }
            }
         );
         AV15Pgmname = "GlosarioGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "GlosarioGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short edtGlosario_Arquivo_Display ;
      private short nGXWrapped ;
      private int A1347Glosario_Codigo ;
      private int wcpOA1347Glosario_Codigo ;
      private int AV12Glosario_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A1343Glosario_NomeArq ;
      private String edtGlosario_Arquivo_Filename ;
      private String A1344Glosario_TipoArq ;
      private String edtGlosario_Arquivo_Filetype ;
      private String edtGlosario_Arquivo_Internalname ;
      private String edtGlosario_Termo_Internalname ;
      private String edtGlosario_Descricao_Internalname ;
      private String edtGlosario_Arquivo_Tooltiptext ;
      private String edtGlosario_Arquivo_Linktarget ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockglosario_termo_Internalname ;
      private String lblTextblockglosario_termo_Jsonclick ;
      private String edtGlosario_Termo_Jsonclick ;
      private String lblTextblockglosario_descricao_Internalname ;
      private String lblTextblockglosario_descricao_Jsonclick ;
      private String lblTextblockglosario_arquivo_Internalname ;
      private String lblTextblockglosario_arquivo_Jsonclick ;
      private String edtGlosario_Arquivo_Contenttype ;
      private String edtGlosario_Arquivo_Parameters ;
      private String edtGlosario_Arquivo_Jsonclick ;
      private String lblTextblockglosario_nomearq_Internalname ;
      private String lblTextblockglosario_nomearq_Jsonclick ;
      private String edtGlosario_NomeArq_Internalname ;
      private String edtGlosario_NomeArq_Jsonclick ;
      private String lblTextblockglosario_tipoarq_Internalname ;
      private String lblTextblockglosario_tipoarq_Jsonclick ;
      private String edtGlosario_TipoArq_Internalname ;
      private String edtGlosario_TipoArq_Jsonclick ;
      private String sCtrlA1347Glosario_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1343Glosario_NomeArq ;
      private bool n1344Glosario_TipoArq ;
      private bool n1342Glosario_Arquivo ;
      private bool returnInSub ;
      private String A859Glosario_Descricao ;
      private String A858Glosario_Termo ;
      private String A1342Glosario_Arquivo ;
      private GxFile gxblobfileaux ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00FA2_A1347Glosario_Codigo ;
      private String[] H00FA2_A1343Glosario_NomeArq ;
      private bool[] H00FA2_n1343Glosario_NomeArq ;
      private String[] H00FA2_A1344Glosario_TipoArq ;
      private bool[] H00FA2_n1344Glosario_TipoArq ;
      private String[] H00FA2_A859Glosario_Descricao ;
      private String[] H00FA2_A858Glosario_Termo ;
      private String[] H00FA2_A1342Glosario_Arquivo ;
      private bool[] H00FA2_n1342Glosario_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class glosariogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FA2 ;
          prmH00FA2 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FA2", "SELECT [Glosario_Codigo], [Glosario_NomeArq], [Glosario_TipoArq], [Glosario_Descricao], [Glosario_Termo], [Glosario_Arquivo] FROM [Glosario] WITH (NOLOCK) WHERE [Glosario_Codigo] = @Glosario_Codigo ORDER BY [Glosario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FA2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[7])[0] = rslt.getBLOBFile(6, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
