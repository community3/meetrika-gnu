/*
               File: PRC_FuncaoesTRNRepetidasDLT
        Description: Apagar Funcaoes TRN Repetidas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:39.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaoestrnrepetidasdlt : GXProcedure
   {
      public prc_funcaoestrnrepetidasdlt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaoestrnrepetidasdlt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref bool aP0_EmCascata )
      {
         this.AV13EmCascata = aP0_EmCascata;
         initialize();
         executePrivate();
         aP0_EmCascata=this.AV13EmCascata;
      }

      public bool executeUdp( )
      {
         this.AV13EmCascata = aP0_EmCascata;
         initialize();
         executePrivate();
         aP0_EmCascata=this.AV13EmCascata;
         return AV13EmCascata ;
      }

      public void executeSubmit( ref bool aP0_EmCascata )
      {
         prc_funcaoestrnrepetidasdlt objprc_funcaoestrnrepetidasdlt;
         objprc_funcaoestrnrepetidasdlt = new prc_funcaoestrnrepetidasdlt();
         objprc_funcaoestrnrepetidasdlt.AV13EmCascata = aP0_EmCascata;
         objprc_funcaoestrnrepetidasdlt.context.SetSubmitInitialConfig(context);
         objprc_funcaoestrnrepetidasdlt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaoestrnrepetidasdlt);
         aP0_EmCascata=this.AV13EmCascata;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaoestrnrepetidasdlt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Codigos.FromXml(AV9WebSession.Get("Codigos"), "Collection");
         if ( AV13EmCascata )
         {
            AV16GXV1 = 1;
            while ( AV16GXV1 <= AV8Codigos.Count )
            {
               AV11FuncaoAPF_Codigo = (int)(AV8Codigos.GetNumeric(AV16GXV1));
               /* Using cursor P006Z2 */
               pr_default.execute(0, new Object[] {AV11FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A358FuncaoAPF_FunAPFPaiCod = P006Z2_A358FuncaoAPF_FunAPFPaiCod[0];
                  n358FuncaoAPF_FunAPFPaiCod = P006Z2_n358FuncaoAPF_FunAPFPaiCod[0];
                  A165FuncaoAPF_Codigo = P006Z2_A165FuncaoAPF_Codigo[0];
                  n165FuncaoAPF_Codigo = P006Z2_n165FuncaoAPF_Codigo[0];
                  /* Optimized DELETE. */
                  /* Using cursor P006Z3 */
                  pr_default.execute(1, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                  pr_default.close(1);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
                  /* End optimized DELETE. */
                  /* Optimized DELETE. */
                  /* Using cursor P006Z4 */
                  pr_default.execute(2, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                  pr_default.close(2);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemItem") ;
                  /* End optimized DELETE. */
                  while ( (pr_default.getStatus(0) != 101) && ( P006Z2_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
                  {
                     A358FuncaoAPF_FunAPFPaiCod = P006Z2_A358FuncaoAPF_FunAPFPaiCod[0];
                     n358FuncaoAPF_FunAPFPaiCod = P006Z2_n358FuncaoAPF_FunAPFPaiCod[0];
                     if ( A358FuncaoAPF_FunAPFPaiCod == AV11FuncaoAPF_Codigo )
                     {
                        /* Using cursor P006Z5 */
                        pr_default.execute(3, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                        pr_default.close(3);
                        dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  /* Optimized DELETE. */
                  /* Using cursor P006Z6 */
                  pr_default.execute(4, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                  pr_default.close(4);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
                  /* End optimized DELETE. */
                  /* Optimized DELETE. */
                  /* Using cursor P006Z7 */
                  pr_default.execute(5, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                  pr_default.close(5);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesUsuarioFuncoesAPF") ;
                  /* End optimized DELETE. */
                  /* Optimized DELETE. */
                  /* Using cursor P006Z8 */
                  pr_default.execute(6, new Object[] {AV11FuncaoAPF_Codigo});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("ProjetoMelhoria") ;
                  /* End optimized DELETE. */
                  /* Optimized DELETE. */
                  /* Using cursor P006Z9 */
                  pr_default.execute(7, new Object[] {AV11FuncaoAPF_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("SolicitacaoMudancaItem") ;
                  /* End optimized DELETE. */
                  /* Using cursor P006Z10 */
                  pr_default.execute(8, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(0);
               AV16GXV1 = (int)(AV16GXV1+1);
            }
         }
         else
         {
            AV25GXV2 = 1;
            while ( AV25GXV2 <= AV8Codigos.Count )
            {
               AV11FuncaoAPF_Codigo = (int)(AV8Codigos.GetNumeric(AV25GXV2));
               /* Using cursor P006Z11 */
               pr_default.execute(9, new Object[] {AV11FuncaoAPF_Codigo});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A358FuncaoAPF_FunAPFPaiCod = P006Z11_A358FuncaoAPF_FunAPFPaiCod[0];
                  n358FuncaoAPF_FunAPFPaiCod = P006Z11_n358FuncaoAPF_FunAPFPaiCod[0];
                  A165FuncaoAPF_Codigo = P006Z11_A165FuncaoAPF_Codigo[0];
                  n165FuncaoAPF_Codigo = P006Z11_n165FuncaoAPF_Codigo[0];
                  AV10TemReferencias = false;
                  AV27GXLvl45 = 0;
                  /* Using cursor P006Z12 */
                  pr_default.execute(10, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                  while ( (pr_default.getStatus(10) != 101) )
                  {
                     A406FuncaoAPFEvidencia_Codigo = P006Z12_A406FuncaoAPFEvidencia_Codigo[0];
                     AV27GXLvl45 = 1;
                     AV10TemReferencias = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(10);
                  }
                  pr_default.close(10);
                  if ( AV27GXLvl45 == 0 )
                  {
                     AV28GXLvl49 = 0;
                     /* Using cursor P006Z13 */
                     pr_default.execute(11, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                     while ( (pr_default.getStatus(11) != 101) )
                     {
                        A224ContagemItem_Lancamento = P006Z13_A224ContagemItem_Lancamento[0];
                        AV28GXLvl49 = 1;
                        AV10TemReferencias = true;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        pr_default.readNext(11);
                     }
                     pr_default.close(11);
                     if ( AV28GXLvl49 == 0 )
                     {
                        AV29GXLvl53 = 0;
                        while ( (pr_default.getStatus(9) != 101) && ( P006Z11_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) )
                        {
                           A358FuncaoAPF_FunAPFPaiCod = P006Z11_A358FuncaoAPF_FunAPFPaiCod[0];
                           n358FuncaoAPF_FunAPFPaiCod = P006Z11_n358FuncaoAPF_FunAPFPaiCod[0];
                           if ( A358FuncaoAPF_FunAPFPaiCod == AV11FuncaoAPF_Codigo )
                           {
                              AV29GXLvl53 = 1;
                              AV10TemReferencias = true;
                              /* Exit For each command. Update data (if necessary), close cursors & exit. */
                              if (true) break;
                           }
                           /* Exiting from a For First loop. */
                           if (true) break;
                        }
                        if ( AV29GXLvl53 == 0 )
                        {
                           AV30GXLvl58 = 0;
                           /* Using cursor P006Z14 */
                           pr_default.execute(12, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                           while ( (pr_default.getStatus(12) != 101) )
                           {
                              A364FuncaoAPFAtributos_AtributosCod = P006Z14_A364FuncaoAPFAtributos_AtributosCod[0];
                              AV30GXLvl58 = 1;
                              AV10TemReferencias = true;
                              /* Exit For each command. Update data (if necessary), close cursors & exit. */
                              if (true) break;
                              pr_default.readNext(12);
                           }
                           pr_default.close(12);
                           if ( AV30GXLvl58 == 0 )
                           {
                              AV31GXLvl62 = 0;
                              /* Using cursor P006Z15 */
                              pr_default.execute(13, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                              while ( (pr_default.getStatus(13) != 101) )
                              {
                                 A161FuncaoUsuario_Codigo = P006Z15_A161FuncaoUsuario_Codigo[0];
                                 AV31GXLvl62 = 1;
                                 AV10TemReferencias = true;
                                 /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                 if (true) break;
                                 pr_default.readNext(13);
                              }
                              pr_default.close(13);
                              if ( AV31GXLvl62 == 0 )
                              {
                                 AV32GXLvl66 = 0;
                                 /* Using cursor P006Z16 */
                                 pr_default.execute(14, new Object[] {AV11FuncaoAPF_Codigo});
                                 while ( (pr_default.getStatus(14) != 101) )
                                 {
                                    A698ProjetoMelhoria_FnAPFCod = P006Z16_A698ProjetoMelhoria_FnAPFCod[0];
                                    n698ProjetoMelhoria_FnAPFCod = P006Z16_n698ProjetoMelhoria_FnAPFCod[0];
                                    A736ProjetoMelhoria_Codigo = P006Z16_A736ProjetoMelhoria_Codigo[0];
                                    AV32GXLvl66 = 1;
                                    AV10TemReferencias = true;
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                    pr_default.readNext(14);
                                 }
                                 pr_default.close(14);
                                 if ( AV32GXLvl66 == 0 )
                                 {
                                    /* Using cursor P006Z17 */
                                    pr_default.execute(15, new Object[] {AV11FuncaoAPF_Codigo});
                                    while ( (pr_default.getStatus(15) != 101) )
                                    {
                                       A995SolicitacaoMudancaItem_FuncaoAPF = P006Z17_A995SolicitacaoMudancaItem_FuncaoAPF[0];
                                       A996SolicitacaoMudanca_Codigo = P006Z17_A996SolicitacaoMudanca_Codigo[0];
                                       AV10TemReferencias = true;
                                       /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                       if (true) break;
                                       pr_default.readNext(15);
                                    }
                                    pr_default.close(15);
                                 }
                              }
                           }
                        }
                     }
                  }
                  if ( ! AV10TemReferencias )
                  {
                     /* Using cursor P006Z18 */
                     pr_default.execute(16, new Object[] {n165FuncaoAPF_Codigo, A165FuncaoAPF_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
                  }
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(9);
               AV25GXV2 = (int)(AV25GXV2+1);
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_FuncaoesTRNRepetidasDLT");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Codigos = new GxSimpleCollection();
         AV9WebSession = context.GetSession();
         scmdbuf = "";
         P006Z2_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         P006Z2_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         P006Z2_A165FuncaoAPF_Codigo = new int[1] ;
         P006Z2_n165FuncaoAPF_Codigo = new bool[] {false} ;
         P006Z11_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         P006Z11_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         P006Z11_A165FuncaoAPF_Codigo = new int[1] ;
         P006Z11_n165FuncaoAPF_Codigo = new bool[] {false} ;
         P006Z12_A165FuncaoAPF_Codigo = new int[1] ;
         P006Z12_n165FuncaoAPF_Codigo = new bool[] {false} ;
         P006Z12_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         P006Z13_A165FuncaoAPF_Codigo = new int[1] ;
         P006Z13_n165FuncaoAPF_Codigo = new bool[] {false} ;
         P006Z13_A224ContagemItem_Lancamento = new int[1] ;
         P006Z14_A165FuncaoAPF_Codigo = new int[1] ;
         P006Z14_n165FuncaoAPF_Codigo = new bool[] {false} ;
         P006Z14_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         P006Z15_A165FuncaoAPF_Codigo = new int[1] ;
         P006Z15_n165FuncaoAPF_Codigo = new bool[] {false} ;
         P006Z15_A161FuncaoUsuario_Codigo = new int[1] ;
         P006Z16_A698ProjetoMelhoria_FnAPFCod = new int[1] ;
         P006Z16_n698ProjetoMelhoria_FnAPFCod = new bool[] {false} ;
         P006Z16_A736ProjetoMelhoria_Codigo = new int[1] ;
         P006Z17_A995SolicitacaoMudancaItem_FuncaoAPF = new int[1] ;
         P006Z17_A996SolicitacaoMudanca_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaoestrnrepetidasdlt__default(),
            new Object[][] {
                new Object[] {
               P006Z2_A358FuncaoAPF_FunAPFPaiCod, P006Z2_n358FuncaoAPF_FunAPFPaiCod, P006Z2_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P006Z11_A358FuncaoAPF_FunAPFPaiCod, P006Z11_n358FuncaoAPF_FunAPFPaiCod, P006Z11_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P006Z12_A165FuncaoAPF_Codigo, P006Z12_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               P006Z13_A165FuncaoAPF_Codigo, P006Z13_n165FuncaoAPF_Codigo, P006Z13_A224ContagemItem_Lancamento
               }
               , new Object[] {
               P006Z14_A165FuncaoAPF_Codigo, P006Z14_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               P006Z15_A165FuncaoAPF_Codigo, P006Z15_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               P006Z16_A698ProjetoMelhoria_FnAPFCod, P006Z16_n698ProjetoMelhoria_FnAPFCod, P006Z16_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               P006Z17_A995SolicitacaoMudancaItem_FuncaoAPF, P006Z17_A996SolicitacaoMudanca_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV27GXLvl45 ;
      private short AV28GXLvl49 ;
      private short AV29GXLvl53 ;
      private short AV30GXLvl58 ;
      private short AV31GXLvl62 ;
      private short AV32GXLvl66 ;
      private int AV16GXV1 ;
      private int AV11FuncaoAPF_Codigo ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int A165FuncaoAPF_Codigo ;
      private int AV25GXV2 ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private int A224ContagemItem_Lancamento ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A161FuncaoUsuario_Codigo ;
      private int A698ProjetoMelhoria_FnAPFCod ;
      private int A736ProjetoMelhoria_Codigo ;
      private int A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int A996SolicitacaoMudanca_Codigo ;
      private String scmdbuf ;
      private bool AV13EmCascata ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n165FuncaoAPF_Codigo ;
      private bool AV10TemReferencias ;
      private bool n698ProjetoMelhoria_FnAPFCod ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private bool aP0_EmCascata ;
      private IDataStoreProvider pr_default ;
      private int[] P006Z2_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] P006Z2_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] P006Z2_A165FuncaoAPF_Codigo ;
      private bool[] P006Z2_n165FuncaoAPF_Codigo ;
      private int[] P006Z11_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] P006Z11_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] P006Z11_A165FuncaoAPF_Codigo ;
      private bool[] P006Z11_n165FuncaoAPF_Codigo ;
      private int[] P006Z12_A165FuncaoAPF_Codigo ;
      private bool[] P006Z12_n165FuncaoAPF_Codigo ;
      private int[] P006Z12_A406FuncaoAPFEvidencia_Codigo ;
      private int[] P006Z13_A165FuncaoAPF_Codigo ;
      private bool[] P006Z13_n165FuncaoAPF_Codigo ;
      private int[] P006Z13_A224ContagemItem_Lancamento ;
      private int[] P006Z14_A165FuncaoAPF_Codigo ;
      private bool[] P006Z14_n165FuncaoAPF_Codigo ;
      private int[] P006Z14_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] P006Z15_A165FuncaoAPF_Codigo ;
      private bool[] P006Z15_n165FuncaoAPF_Codigo ;
      private int[] P006Z15_A161FuncaoUsuario_Codigo ;
      private int[] P006Z16_A698ProjetoMelhoria_FnAPFCod ;
      private bool[] P006Z16_n698ProjetoMelhoria_FnAPFCod ;
      private int[] P006Z16_A736ProjetoMelhoria_Codigo ;
      private int[] P006Z17_A995SolicitacaoMudancaItem_FuncaoAPF ;
      private int[] P006Z17_A996SolicitacaoMudanca_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Codigos ;
   }

   public class prc_funcaoestrnrepetidasdlt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006Z2 ;
          prmP006Z2 = new Object[] {
          new Object[] {"@AV11FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z3 ;
          prmP006Z3 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z4 ;
          prmP006Z4 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z5 ;
          prmP006Z5 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z6 ;
          prmP006Z6 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z7 ;
          prmP006Z7 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z8 ;
          prmP006Z8 = new Object[] {
          new Object[] {"@AV11FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z9 ;
          prmP006Z9 = new Object[] {
          new Object[] {"@AV11FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z10 ;
          prmP006Z10 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z11 ;
          prmP006Z11 = new Object[] {
          new Object[] {"@AV11FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z12 ;
          prmP006Z12 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z13 ;
          prmP006Z13 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z14 ;
          prmP006Z14 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z15 ;
          prmP006Z15 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z16 ;
          prmP006Z16 = new Object[] {
          new Object[] {"@AV11FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z17 ;
          prmP006Z17 = new Object[] {
          new Object[] {"@AV11FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006Z18 ;
          prmP006Z18 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006Z2", "SELECT [FuncaoAPF_FunAPFPaiCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (UPDLOCK) WHERE [FuncaoAPF_Codigo] = @AV11FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z2,1,0,true,true )
             ,new CursorDef("P006Z3", "DELETE FROM [FuncaoAPFEvidencia]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z3)
             ,new CursorDef("P006Z4", "DELETE FROM [ContagemItem]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z4)
             ,new CursorDef("P006Z5", "DELETE FROM [FuncoesAPF]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z5)
             ,new CursorDef("P006Z6", "DELETE FROM [FuncoesAPFAtributos]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z6)
             ,new CursorDef("P006Z7", "DELETE FROM [FuncoesUsuarioFuncoesAPF]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z7)
             ,new CursorDef("P006Z8", "DELETE FROM [ProjetoMelhoria]  WHERE [ProjetoMelhoria_FnAPFCod] = @AV11FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z8)
             ,new CursorDef("P006Z9", "DELETE FROM [SolicitacaoMudancaItem]  WHERE [SolicitacaoMudancaItem_FuncaoAPF] = @AV11FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z9)
             ,new CursorDef("P006Z10", "DELETE FROM [FuncoesAPF]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z10)
             ,new CursorDef("P006Z11", "SELECT [FuncaoAPF_FunAPFPaiCod], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (UPDLOCK) WHERE [FuncaoAPF_Codigo] = @AV11FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z11,1,0,true,true )
             ,new CursorDef("P006Z12", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z12,1,0,false,true )
             ,new CursorDef("P006Z13", "SELECT TOP 1 [FuncaoAPF_Codigo], [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z13,1,0,false,true )
             ,new CursorDef("P006Z14", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z14,1,0,false,true )
             ,new CursorDef("P006Z15", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoUsuario_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z15,1,0,false,true )
             ,new CursorDef("P006Z16", "SELECT TOP 1 [ProjetoMelhoria_FnAPFCod], [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_FnAPFCod] = @AV11FuncaoAPF_Codigo ORDER BY [ProjetoMelhoria_FnAPFCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z16,1,0,false,true )
             ,new CursorDef("P006Z17", "SELECT TOP 1 [SolicitacaoMudancaItem_FuncaoAPF], [SolicitacaoMudanca_Codigo] FROM [SolicitacaoMudancaItem] WITH (NOLOCK) WHERE [SolicitacaoMudancaItem_FuncaoAPF] = @AV11FuncaoAPF_Codigo ORDER BY [SolicitacaoMudancaItem_FuncaoAPF] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006Z17,1,0,false,true )
             ,new CursorDef("P006Z18", "DELETE FROM [FuncoesAPF]  WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006Z18)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
