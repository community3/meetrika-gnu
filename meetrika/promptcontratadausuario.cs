/*
               File: PromptContratadaUsuario
        Description: Selecione Contratada Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:38:13.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratadausuario : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratadausuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratadausuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratadaUsuario_ContratadaCod ,
                           ref int aP1_InOutContratadaUsuario_UsuarioCod )
      {
         this.AV7InOutContratadaUsuario_ContratadaCod = aP0_InOutContratadaUsuario_ContratadaCod;
         this.AV8InOutContratadaUsuario_UsuarioCod = aP1_InOutContratadaUsuario_UsuarioCod;
         executePrivate();
         aP0_InOutContratadaUsuario_ContratadaCod=this.AV7InOutContratadaUsuario_ContratadaCod;
         aP1_InOutContratadaUsuario_UsuarioCod=this.AV8InOutContratadaUsuario_UsuarioCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Usuario_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
               AV18ContratadaUsuario_UsuarioPessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
               AV19ContratadaUsuario_ContratadaPessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratadaUsuario_ContratadaPessoaNom1", AV19ContratadaUsuario_ContratadaPessoaNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23Usuario_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
               AV24ContratadaUsuario_UsuarioPessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratadaUsuario_UsuarioPessoaNom2", AV24ContratadaUsuario_UsuarioPessoaNom2);
               AV25ContratadaUsuario_ContratadaPessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratadaUsuario_ContratadaPessoaNom2", AV25ContratadaUsuario_ContratadaPessoaNom2);
               AV27DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
               AV29Usuario_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
               AV30ContratadaUsuario_UsuarioPessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratadaUsuario_UsuarioPessoaNom3", AV30ContratadaUsuario_UsuarioPessoaNom3);
               AV31ContratadaUsuario_ContratadaPessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaUsuario_ContratadaPessoaNom3", AV31ContratadaUsuario_ContratadaPessoaNom3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
               AV37TFUsuario_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFUsuario_Nome", AV37TFUsuario_Nome);
               AV38TFUsuario_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome_Sel", AV38TFUsuario_Nome_Sel);
               AV41TFContratadaUsuario_UsuarioPessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratadaUsuario_UsuarioPessoaNom", AV41TFContratadaUsuario_UsuarioPessoaNom);
               AV42TFContratadaUsuario_UsuarioPessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratadaUsuario_UsuarioPessoaNom_Sel", AV42TFContratadaUsuario_UsuarioPessoaNom_Sel);
               AV45TFContratadaUsuario_ContratadaPessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratadaUsuario_ContratadaPessoaNom", AV45TFContratadaUsuario_ContratadaPessoaNom);
               AV46TFContratadaUsuario_ContratadaPessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratadaUsuario_ContratadaPessoaNom_Sel", AV46TFContratadaUsuario_ContratadaPessoaNom_Sel);
               AV39ddo_Usuario_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Usuario_NomeTitleControlIdToReplace", AV39ddo_Usuario_NomeTitleControlIdToReplace);
               AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace);
               AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace", AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace);
               AV55Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptContratadaUsuario";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptcontratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_ContratadaCod:"+context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratadaUsuario_ContratadaCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratadaUsuario_UsuarioCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA8V2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV55Pgmname = "PromptContratadaUsuario";
               context.Gx_err = 0;
               WS8V2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE8V2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117381392");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratadausuario.aspx") + "?" + UrlEncode("" +AV7InOutContratadaUsuario_ContratadaCod) + "," + UrlEncode("" +AV8InOutContratadaUsuario_UsuarioCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_NOME1", StringUtil.RTrim( AV17Usuario_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM1", StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1", StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_NOME2", StringUtil.RTrim( AV23Usuario_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM2", StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2", StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_NOME3", StringUtil.RTrim( AV29Usuario_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM3", StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3", StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME", StringUtil.RTrim( AV37TFUsuario_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME_SEL", StringUtil.RTrim( AV38TFUsuario_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM", StringUtil.RTrim( AV41TFContratadaUsuario_UsuarioPessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL", StringUtil.RTrim( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM", StringUtil.RTrim( AV45TFContratadaUsuario_ContratadaPessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL", StringUtil.RTrim( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_NOMETITLEFILTERDATA", AV36Usuario_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_NOMETITLEFILTERDATA", AV36Usuario_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA", AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA", AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLEFILTERDATA", AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLEFILTERDATA", AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV55Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Caption", StringUtil.RTrim( Ddo_usuario_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Tooltip", StringUtil.RTrim( Ddo_usuario_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cls", StringUtil.RTrim( Ddo_usuario_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_usuario_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_usuario_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filtertype", StringUtil.RTrim( Ddo_usuario_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_usuario_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_usuario_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortasc", StringUtil.RTrim( Ddo_usuario_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_usuario_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_usuario_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_usuario_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_usuario_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Caption", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cls", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratadausuario_usuariopessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Caption", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Cls", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratadausuario_contratadapessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratadausuario_contratadapessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratadausuario_contratadapessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratadausuario_contratadapessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratadausuario_contratadapessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratadausuario_contratadapessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_usuario_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratadausuario_contratadapessoanom_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptContratadaUsuario";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptcontratadausuario:[SendSecurityCheck value for]"+"ContratadaUsuario_ContratadaCod:"+context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm8V2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratadaUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contratada Usuario" ;
      }

      protected void WB8V0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_8V2( true) ;
         }
         else
         {
            wb_table1_2_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratadaUsuario_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratadaUsuario_ContratadaCod_Jsonclick, 0, "Attribute", "", "", "", edtContratadaUsuario_ContratadaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratadaUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_Internalname, StringUtil.RTrim( AV37TFUsuario_Nome), StringUtil.RTrim( context.localUtil.Format( AV37TFUsuario_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_sel_Internalname, StringUtil.RTrim( AV38TFUsuario_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV38TFUsuario_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_usuariopessoanom_Internalname, StringUtil.RTrim( AV41TFContratadaUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV41TFContratadaUsuario_UsuarioPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_usuariopessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratadausuario_usuariopessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_usuariopessoanom_sel_Internalname, StringUtil.RTrim( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_usuariopessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratadausuario_usuariopessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_contratadapessoanom_Internalname, StringUtil.RTrim( AV45TFContratadaUsuario_ContratadaPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV45TFContratadaUsuario_ContratadaPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_contratadapessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratadausuario_contratadapessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratadausuario_contratadapessoanom_sel_Internalname, StringUtil.RTrim( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratadausuario_contratadapessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratadausuario_contratadapessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, AV39ddo_Usuario_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_usuario_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratadaUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADAUSUARIO_USUARIOPESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratadaUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Internalname, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratadaUsuario.htm");
         }
         wbLoad = true;
      }

      protected void START8V2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contratada Usuario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8V0( ) ;
      }

      protected void WS8V2( )
      {
         START8V2( ) ;
         EVT8V2( ) ;
      }

      protected void EVT8V2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E118V2 */
                           E118V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E128V2 */
                           E128V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E138V2 */
                           E138V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E148V2 */
                           E148V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E158V2 */
                           E158V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E168V2 */
                           E168V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E178V2 */
                           E178V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E188V2 */
                           E188V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E198V2 */
                           E198V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E208V2 */
                           E208V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E218V2 */
                           E218V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E228V2 */
                           E228V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E238V2 */
                           E238V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E248V2 */
                           E248V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           AV34Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV54Select_GXI : context.convertURL( context.PathToRelativeUrl( AV34Select))));
                           A67ContratadaUsuario_ContratadaPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_ContratadaPessoaCod_Internalname), ",", "."));
                           n67ContratadaUsuario_ContratadaPessoaCod = false;
                           A70ContratadaUsuario_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioPessoaCod_Internalname), ",", "."));
                           n70ContratadaUsuario_UsuarioPessoaCod = false;
                           A69ContratadaUsuario_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_UsuarioCod_Internalname), ",", "."));
                           A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
                           n2Usuario_Nome = false;
                           A71ContratadaUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContratadaUsuario_UsuarioPessoaNom_Internalname));
                           n71ContratadaUsuario_UsuarioPessoaNom = false;
                           A68ContratadaUsuario_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtContratadaUsuario_ContratadaPessoaNom_Internalname));
                           n68ContratadaUsuario_ContratadaPessoaNom = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E258V2 */
                                 E258V2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E268V2 */
                                 E268V2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E278V2 */
                                 E278V2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME1"), AV17Usuario_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratadausuario_usuariopessoanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM1"), AV18ContratadaUsuario_UsuarioPessoaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratadausuario_contratadapessoanom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1"), AV19ContratadaUsuario_ContratadaPessoaNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME2"), AV23Usuario_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratadausuario_usuariopessoanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM2"), AV24ContratadaUsuario_UsuarioPessoaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratadausuario_contratadapessoanom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2"), AV25ContratadaUsuario_ContratadaPessoaNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Usuario_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME3"), AV29Usuario_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratadausuario_usuariopessoanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM3"), AV30ContratadaUsuario_UsuarioPessoaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratadausuario_contratadapessoanom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3"), AV31ContratadaUsuario_ContratadaPessoaNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfusuario_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV37TFUsuario_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfusuario_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV38TFUsuario_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratadausuario_usuariopessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM"), AV41TFContratadaUsuario_UsuarioPessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratadausuario_usuariopessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL"), AV42TFContratadaUsuario_UsuarioPessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratadausuario_contratadapessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM"), AV45TFContratadaUsuario_ContratadaPessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratadausuario_contratadapessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL"), AV46TFContratadaUsuario_ContratadaPessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E288V2 */
                                       E288V2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE8V2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm8V2( ) ;
            }
         }
      }

      protected void PA8V2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("USUARIO_NOME", "Usu�rio", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADAUSUARIO_USUARIOPESSOANOM", "Pessoa", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADAUSUARIO_CONTRATADAPESSOANOM", "Contratada", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("USUARIO_NOME", "Usu�rio", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADAUSUARIO_USUARIOPESSOANOM", "Pessoa", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADAUSUARIO_CONTRATADAPESSOANOM", "Contratada", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("USUARIO_NOME", "Usu�rio", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATADAUSUARIO_USUARIOPESSOANOM", "Pessoa", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATADAUSUARIO_CONTRATADAPESSOANOM", "Contratada", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Usuario_Nome1 ,
                                       String AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                       String AV19ContratadaUsuario_ContratadaPessoaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23Usuario_Nome2 ,
                                       String AV24ContratadaUsuario_UsuarioPessoaNom2 ,
                                       String AV25ContratadaUsuario_ContratadaPessoaNom2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       short AV28DynamicFiltersOperator3 ,
                                       String AV29Usuario_Nome3 ,
                                       String AV30ContratadaUsuario_UsuarioPessoaNom3 ,
                                       String AV31ContratadaUsuario_ContratadaPessoaNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 ,
                                       String AV37TFUsuario_Nome ,
                                       String AV38TFUsuario_Nome_Sel ,
                                       String AV41TFContratadaUsuario_UsuarioPessoaNom ,
                                       String AV42TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                       String AV45TFContratadaUsuario_ContratadaPessoaNom ,
                                       String AV46TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                       String AV39ddo_Usuario_NomeTitleControlIdToReplace ,
                                       String AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace ,
                                       String AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace ,
                                       String AV55Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A69ContratadaUsuario_UsuarioCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF8V2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADAUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8V2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV55Pgmname = "PromptContratadaUsuario";
         context.Gx_err = 0;
      }

      protected void RF8V2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E268V2 */
         E268V2 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17Usuario_Nome1 ,
                                                 AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                                 AV19ContratadaUsuario_ContratadaPessoaNom1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23Usuario_Nome2 ,
                                                 AV24ContratadaUsuario_UsuarioPessoaNom2 ,
                                                 AV25ContratadaUsuario_ContratadaPessoaNom2 ,
                                                 AV26DynamicFiltersEnabled3 ,
                                                 AV27DynamicFiltersSelector3 ,
                                                 AV28DynamicFiltersOperator3 ,
                                                 AV29Usuario_Nome3 ,
                                                 AV30ContratadaUsuario_UsuarioPessoaNom3 ,
                                                 AV31ContratadaUsuario_ContratadaPessoaNom3 ,
                                                 AV38TFUsuario_Nome_Sel ,
                                                 AV37TFUsuario_Nome ,
                                                 AV42TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                                 AV41TFContratadaUsuario_UsuarioPessoaNom ,
                                                 AV46TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                                 AV45TFContratadaUsuario_ContratadaPessoaNom ,
                                                 A2Usuario_Nome ,
                                                 A71ContratadaUsuario_UsuarioPessoaNom ,
                                                 A68ContratadaUsuario_ContratadaPessoaNom ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN
                                                 }
            });
            lV17Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Usuario_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
            lV17Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Usuario_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
            lV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
            lV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
            lV19ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratadaUsuario_ContratadaPessoaNom1", AV19ContratadaUsuario_ContratadaPessoaNom1);
            lV19ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratadaUsuario_ContratadaPessoaNom1", AV19ContratadaUsuario_ContratadaPessoaNom1);
            lV23Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Usuario_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
            lV23Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Usuario_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
            lV24ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratadaUsuario_UsuarioPessoaNom2", AV24ContratadaUsuario_UsuarioPessoaNom2);
            lV24ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratadaUsuario_UsuarioPessoaNom2", AV24ContratadaUsuario_UsuarioPessoaNom2);
            lV25ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratadaUsuario_ContratadaPessoaNom2", AV25ContratadaUsuario_ContratadaPessoaNom2);
            lV25ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratadaUsuario_ContratadaPessoaNom2", AV25ContratadaUsuario_ContratadaPessoaNom2);
            lV29Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29Usuario_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
            lV29Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29Usuario_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
            lV30ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratadaUsuario_UsuarioPessoaNom3", AV30ContratadaUsuario_UsuarioPessoaNom3);
            lV30ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratadaUsuario_UsuarioPessoaNom3", AV30ContratadaUsuario_UsuarioPessoaNom3);
            lV31ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaUsuario_ContratadaPessoaNom3", AV31ContratadaUsuario_ContratadaPessoaNom3);
            lV31ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaUsuario_ContratadaPessoaNom3", AV31ContratadaUsuario_ContratadaPessoaNom3);
            lV37TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV37TFUsuario_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFUsuario_Nome", AV37TFUsuario_Nome);
            lV41TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV41TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratadaUsuario_UsuarioPessoaNom", AV41TFContratadaUsuario_UsuarioPessoaNom);
            lV45TFContratadaUsuario_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV45TFContratadaUsuario_ContratadaPessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratadaUsuario_ContratadaPessoaNom", AV45TFContratadaUsuario_ContratadaPessoaNom);
            /* Using cursor H008V2 */
            pr_default.execute(0, new Object[] {lV17Usuario_Nome1, lV17Usuario_Nome1, lV18ContratadaUsuario_UsuarioPessoaNom1, lV18ContratadaUsuario_UsuarioPessoaNom1, lV19ContratadaUsuario_ContratadaPessoaNom1, lV19ContratadaUsuario_ContratadaPessoaNom1, lV23Usuario_Nome2, lV23Usuario_Nome2, lV24ContratadaUsuario_UsuarioPessoaNom2, lV24ContratadaUsuario_UsuarioPessoaNom2, lV25ContratadaUsuario_ContratadaPessoaNom2, lV25ContratadaUsuario_ContratadaPessoaNom2, lV29Usuario_Nome3, lV29Usuario_Nome3, lV30ContratadaUsuario_UsuarioPessoaNom3, lV30ContratadaUsuario_UsuarioPessoaNom3, lV31ContratadaUsuario_ContratadaPessoaNom3, lV31ContratadaUsuario_ContratadaPessoaNom3, lV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, lV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, lV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A66ContratadaUsuario_ContratadaCod = H008V2_A66ContratadaUsuario_ContratadaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
               A68ContratadaUsuario_ContratadaPessoaNom = H008V2_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H008V2_n68ContratadaUsuario_ContratadaPessoaNom[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H008V2_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H008V2_n71ContratadaUsuario_UsuarioPessoaNom[0];
               A2Usuario_Nome = H008V2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008V2_n2Usuario_Nome[0];
               A69ContratadaUsuario_UsuarioCod = H008V2_A69ContratadaUsuario_UsuarioCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H008V2_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H008V2_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H008V2_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H008V2_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H008V2_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H008V2_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A68ContratadaUsuario_ContratadaPessoaNom = H008V2_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H008V2_n68ContratadaUsuario_ContratadaPessoaNom[0];
               A2Usuario_Nome = H008V2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008V2_n2Usuario_Nome[0];
               A70ContratadaUsuario_UsuarioPessoaCod = H008V2_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = H008V2_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A71ContratadaUsuario_UsuarioPessoaNom = H008V2_A71ContratadaUsuario_UsuarioPessoaNom[0];
               n71ContratadaUsuario_UsuarioPessoaNom = H008V2_n71ContratadaUsuario_UsuarioPessoaNom[0];
               /* Execute user event: E278V2 */
               E278V2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WB8V0( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17Usuario_Nome1 ,
                                              AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                              AV19ContratadaUsuario_ContratadaPessoaNom1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23Usuario_Nome2 ,
                                              AV24ContratadaUsuario_UsuarioPessoaNom2 ,
                                              AV25ContratadaUsuario_ContratadaPessoaNom2 ,
                                              AV26DynamicFiltersEnabled3 ,
                                              AV27DynamicFiltersSelector3 ,
                                              AV28DynamicFiltersOperator3 ,
                                              AV29Usuario_Nome3 ,
                                              AV30ContratadaUsuario_UsuarioPessoaNom3 ,
                                              AV31ContratadaUsuario_ContratadaPessoaNom3 ,
                                              AV38TFUsuario_Nome_Sel ,
                                              AV37TFUsuario_Nome ,
                                              AV42TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                              AV41TFContratadaUsuario_UsuarioPessoaNom ,
                                              AV46TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                              AV45TFContratadaUsuario_ContratadaPessoaNom ,
                                              A2Usuario_Nome ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A68ContratadaUsuario_ContratadaPessoaNom ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV17Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Usuario_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
         lV17Usuario_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Usuario_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
         lV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
         lV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
         lV19ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratadaUsuario_ContratadaPessoaNom1", AV19ContratadaUsuario_ContratadaPessoaNom1);
         lV19ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratadaUsuario_ContratadaPessoaNom1", AV19ContratadaUsuario_ContratadaPessoaNom1);
         lV23Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Usuario_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
         lV23Usuario_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23Usuario_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
         lV24ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratadaUsuario_UsuarioPessoaNom2", AV24ContratadaUsuario_UsuarioPessoaNom2);
         lV24ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratadaUsuario_UsuarioPessoaNom2", AV24ContratadaUsuario_UsuarioPessoaNom2);
         lV25ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratadaUsuario_ContratadaPessoaNom2", AV25ContratadaUsuario_ContratadaPessoaNom2);
         lV25ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratadaUsuario_ContratadaPessoaNom2", AV25ContratadaUsuario_ContratadaPessoaNom2);
         lV29Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29Usuario_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
         lV29Usuario_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29Usuario_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
         lV30ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratadaUsuario_UsuarioPessoaNom3", AV30ContratadaUsuario_UsuarioPessoaNom3);
         lV30ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratadaUsuario_UsuarioPessoaNom3", AV30ContratadaUsuario_UsuarioPessoaNom3);
         lV31ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaUsuario_ContratadaPessoaNom3", AV31ContratadaUsuario_ContratadaPessoaNom3);
         lV31ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaUsuario_ContratadaPessoaNom3", AV31ContratadaUsuario_ContratadaPessoaNom3);
         lV37TFUsuario_Nome = StringUtil.PadR( StringUtil.RTrim( AV37TFUsuario_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFUsuario_Nome", AV37TFUsuario_Nome);
         lV41TFContratadaUsuario_UsuarioPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV41TFContratadaUsuario_UsuarioPessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratadaUsuario_UsuarioPessoaNom", AV41TFContratadaUsuario_UsuarioPessoaNom);
         lV45TFContratadaUsuario_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV45TFContratadaUsuario_ContratadaPessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratadaUsuario_ContratadaPessoaNom", AV45TFContratadaUsuario_ContratadaPessoaNom);
         /* Using cursor H008V3 */
         pr_default.execute(1, new Object[] {lV17Usuario_Nome1, lV17Usuario_Nome1, lV18ContratadaUsuario_UsuarioPessoaNom1, lV18ContratadaUsuario_UsuarioPessoaNom1, lV19ContratadaUsuario_ContratadaPessoaNom1, lV19ContratadaUsuario_ContratadaPessoaNom1, lV23Usuario_Nome2, lV23Usuario_Nome2, lV24ContratadaUsuario_UsuarioPessoaNom2, lV24ContratadaUsuario_UsuarioPessoaNom2, lV25ContratadaUsuario_ContratadaPessoaNom2, lV25ContratadaUsuario_ContratadaPessoaNom2, lV29Usuario_Nome3, lV29Usuario_Nome3, lV30ContratadaUsuario_UsuarioPessoaNom3, lV30ContratadaUsuario_UsuarioPessoaNom3, lV31ContratadaUsuario_ContratadaPessoaNom3, lV31ContratadaUsuario_ContratadaPessoaNom3, lV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, lV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, lV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel});
         GRID_nRecordCount = H008V3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUP8V0( )
      {
         /* Before Start, stand alone formulas. */
         AV55Pgmname = "PromptContratadaUsuario";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E258V2 */
         E258V2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV48DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_NOMETITLEFILTERDATA"), AV36Usuario_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA"), AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLEFILTERDATA"), AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Usuario_Nome1 = StringUtil.Upper( cgiGet( edtavUsuario_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
            AV18ContratadaUsuario_UsuarioPessoaNom1 = StringUtil.Upper( cgiGet( edtavContratadausuario_usuariopessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
            AV19ContratadaUsuario_ContratadaPessoaNom1 = StringUtil.Upper( cgiGet( edtavContratadausuario_contratadapessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratadaUsuario_ContratadaPessoaNom1", AV19ContratadaUsuario_ContratadaPessoaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23Usuario_Nome2 = StringUtil.Upper( cgiGet( edtavUsuario_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
            AV24ContratadaUsuario_UsuarioPessoaNom2 = StringUtil.Upper( cgiGet( edtavContratadausuario_usuariopessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratadaUsuario_UsuarioPessoaNom2", AV24ContratadaUsuario_UsuarioPessoaNom2);
            AV25ContratadaUsuario_ContratadaPessoaNom2 = StringUtil.Upper( cgiGet( edtavContratadausuario_contratadapessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratadaUsuario_ContratadaPessoaNom2", AV25ContratadaUsuario_ContratadaPessoaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV28DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
            AV29Usuario_Nome3 = StringUtil.Upper( cgiGet( edtavUsuario_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
            AV30ContratadaUsuario_UsuarioPessoaNom3 = StringUtil.Upper( cgiGet( edtavContratadausuario_usuariopessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratadaUsuario_UsuarioPessoaNom3", AV30ContratadaUsuario_UsuarioPessoaNom3);
            AV31ContratadaUsuario_ContratadaPessoaNom3 = StringUtil.Upper( cgiGet( edtavContratadausuario_contratadapessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaUsuario_ContratadaPessoaNom3", AV31ContratadaUsuario_ContratadaPessoaNom3);
            A66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_ContratadaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            AV37TFUsuario_Nome = StringUtil.Upper( cgiGet( edtavTfusuario_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFUsuario_Nome", AV37TFUsuario_Nome);
            AV38TFUsuario_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome_Sel", AV38TFUsuario_Nome_Sel);
            AV41TFContratadaUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratadausuario_usuariopessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratadaUsuario_UsuarioPessoaNom", AV41TFContratadaUsuario_UsuarioPessoaNom);
            AV42TFContratadaUsuario_UsuarioPessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratadausuario_usuariopessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratadaUsuario_UsuarioPessoaNom_Sel", AV42TFContratadaUsuario_UsuarioPessoaNom_Sel);
            AV45TFContratadaUsuario_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratadausuario_contratadapessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratadaUsuario_ContratadaPessoaNom", AV45TFContratadaUsuario_ContratadaPessoaNom);
            AV46TFContratadaUsuario_ContratadaPessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratadausuario_contratadapessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratadaUsuario_ContratadaPessoaNom_Sel", AV46TFContratadaUsuario_ContratadaPessoaNom_Sel);
            AV39ddo_Usuario_NomeTitleControlIdToReplace = cgiGet( edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Usuario_NomeTitleControlIdToReplace", AV39ddo_Usuario_NomeTitleControlIdToReplace);
            AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace);
            AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace", AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV50GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV51GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_usuario_nome_Caption = cgiGet( "DDO_USUARIO_NOME_Caption");
            Ddo_usuario_nome_Tooltip = cgiGet( "DDO_USUARIO_NOME_Tooltip");
            Ddo_usuario_nome_Cls = cgiGet( "DDO_USUARIO_NOME_Cls");
            Ddo_usuario_nome_Filteredtext_set = cgiGet( "DDO_USUARIO_NOME_Filteredtext_set");
            Ddo_usuario_nome_Selectedvalue_set = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_set");
            Ddo_usuario_nome_Dropdownoptionstype = cgiGet( "DDO_USUARIO_NOME_Dropdownoptionstype");
            Ddo_usuario_nome_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_NOME_Titlecontrolidtoreplace");
            Ddo_usuario_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortasc"));
            Ddo_usuario_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortdsc"));
            Ddo_usuario_nome_Sortedstatus = cgiGet( "DDO_USUARIO_NOME_Sortedstatus");
            Ddo_usuario_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includefilter"));
            Ddo_usuario_nome_Filtertype = cgiGet( "DDO_USUARIO_NOME_Filtertype");
            Ddo_usuario_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Filterisrange"));
            Ddo_usuario_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includedatalist"));
            Ddo_usuario_nome_Datalisttype = cgiGet( "DDO_USUARIO_NOME_Datalisttype");
            Ddo_usuario_nome_Datalistproc = cgiGet( "DDO_USUARIO_NOME_Datalistproc");
            Ddo_usuario_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_nome_Sortasc = cgiGet( "DDO_USUARIO_NOME_Sortasc");
            Ddo_usuario_nome_Sortdsc = cgiGet( "DDO_USUARIO_NOME_Sortdsc");
            Ddo_usuario_nome_Loadingdata = cgiGet( "DDO_USUARIO_NOME_Loadingdata");
            Ddo_usuario_nome_Cleanfilter = cgiGet( "DDO_USUARIO_NOME_Cleanfilter");
            Ddo_usuario_nome_Noresultsfound = cgiGet( "DDO_USUARIO_NOME_Noresultsfound");
            Ddo_usuario_nome_Searchbuttontext = cgiGet( "DDO_USUARIO_NOME_Searchbuttontext");
            Ddo_contratadausuario_usuariopessoanom_Caption = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Caption");
            Ddo_contratadausuario_usuariopessoanom_Tooltip = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Tooltip");
            Ddo_contratadausuario_usuariopessoanom_Cls = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cls");
            Ddo_contratadausuario_usuariopessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_set");
            Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_set");
            Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Dropdownoptionstype");
            Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratadausuario_usuariopessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortasc"));
            Ddo_contratadausuario_usuariopessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includesortdsc"));
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortedstatus");
            Ddo_contratadausuario_usuariopessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includefilter"));
            Ddo_contratadausuario_usuariopessoanom_Filtertype = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filtertype");
            Ddo_contratadausuario_usuariopessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filterisrange"));
            Ddo_contratadausuario_usuariopessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Includedatalist"));
            Ddo_contratadausuario_usuariopessoanom_Datalisttype = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalisttype");
            Ddo_contratadausuario_usuariopessoanom_Datalistproc = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistproc");
            Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratadausuario_usuariopessoanom_Sortasc = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortasc");
            Ddo_contratadausuario_usuariopessoanom_Sortdsc = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Sortdsc");
            Ddo_contratadausuario_usuariopessoanom_Loadingdata = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Loadingdata");
            Ddo_contratadausuario_usuariopessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Cleanfilter");
            Ddo_contratadausuario_usuariopessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Noresultsfound");
            Ddo_contratadausuario_usuariopessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Searchbuttontext");
            Ddo_contratadausuario_contratadapessoanom_Caption = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Caption");
            Ddo_contratadausuario_contratadapessoanom_Tooltip = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Tooltip");
            Ddo_contratadausuario_contratadapessoanom_Cls = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Cls");
            Ddo_contratadausuario_contratadapessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filteredtext_set");
            Ddo_contratadausuario_contratadapessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Selectedvalue_set");
            Ddo_contratadausuario_contratadapessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Dropdownoptionstype");
            Ddo_contratadausuario_contratadapessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratadausuario_contratadapessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includesortasc"));
            Ddo_contratadausuario_contratadapessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includesortdsc"));
            Ddo_contratadausuario_contratadapessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Sortedstatus");
            Ddo_contratadausuario_contratadapessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includefilter"));
            Ddo_contratadausuario_contratadapessoanom_Filtertype = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filtertype");
            Ddo_contratadausuario_contratadapessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filterisrange"));
            Ddo_contratadausuario_contratadapessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Includedatalist"));
            Ddo_contratadausuario_contratadapessoanom_Datalisttype = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Datalisttype");
            Ddo_contratadausuario_contratadapessoanom_Datalistproc = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Datalistproc");
            Ddo_contratadausuario_contratadapessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratadausuario_contratadapessoanom_Sortasc = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Sortasc");
            Ddo_contratadausuario_contratadapessoanom_Sortdsc = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Sortdsc");
            Ddo_contratadausuario_contratadapessoanom_Loadingdata = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Loadingdata");
            Ddo_contratadausuario_contratadapessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Cleanfilter");
            Ddo_contratadausuario_contratadapessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Noresultsfound");
            Ddo_contratadausuario_contratadapessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_usuario_nome_Activeeventkey = cgiGet( "DDO_USUARIO_NOME_Activeeventkey");
            Ddo_usuario_nome_Filteredtext_get = cgiGet( "DDO_USUARIO_NOME_Filteredtext_get");
            Ddo_usuario_nome_Selectedvalue_get = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_get");
            Ddo_contratadausuario_usuariopessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Activeeventkey");
            Ddo_contratadausuario_usuariopessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Filteredtext_get");
            Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM_Selectedvalue_get");
            Ddo_contratadausuario_contratadapessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Activeeventkey");
            Ddo_contratadausuario_contratadapessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Filteredtext_get");
            Ddo_contratadausuario_contratadapessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptContratadaUsuario";
            A66ContratadaUsuario_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContratadaUsuario_ContratadaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptcontratadausuario:[SecurityCheckFailed value for]"+"ContratadaUsuario_ContratadaCod:"+context.localUtil.Format( (decimal)(A66ContratadaUsuario_ContratadaCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME1"), AV17Usuario_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM1"), AV18ContratadaUsuario_UsuarioPessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1"), AV19ContratadaUsuario_ContratadaPessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME2"), AV23Usuario_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM2"), AV24ContratadaUsuario_UsuarioPessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2"), AV25ContratadaUsuario_ContratadaPessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_NOME3"), AV29Usuario_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_USUARIOPESSOANOM3"), AV30ContratadaUsuario_UsuarioPessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3"), AV31ContratadaUsuario_ContratadaPessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV37TFUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV38TFUsuario_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM"), AV41TFContratadaUsuario_UsuarioPessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL"), AV42TFContratadaUsuario_UsuarioPessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM"), AV45TFContratadaUsuario_ContratadaPessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL"), AV46TFContratadaUsuario_ContratadaPessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E258V2 */
         E258V2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E258V2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfusuario_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_Visible), 5, 0)));
         edtavTfusuario_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_sel_Visible), 5, 0)));
         edtavTfcontratadausuario_usuariopessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratadausuario_usuariopessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_usuariopessoanom_Visible), 5, 0)));
         edtavTfcontratadausuario_usuariopessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratadausuario_usuariopessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_usuariopessoanom_sel_Visible), 5, 0)));
         edtavTfcontratadausuario_contratadapessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratadausuario_contratadapessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_contratadapessoanom_Visible), 5, 0)));
         edtavTfcontratadausuario_contratadapessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratadausuario_contratadapessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratadausuario_contratadapessoanom_sel_Visible), 5, 0)));
         Ddo_usuario_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "TitleControlIdToReplace", Ddo_usuario_nome_Titlecontrolidtoreplace);
         AV39ddo_Usuario_NomeTitleControlIdToReplace = Ddo_usuario_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Usuario_NomeTitleControlIdToReplace", AV39ddo_Usuario_NomeTitleControlIdToReplace);
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratadaUsuario_UsuarioPessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_usuariopessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace);
         AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace", AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace);
         edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratadausuario_contratadapessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratadaUsuario_ContratadaPessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_contratadapessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratadausuario_contratadapessoanom_Titlecontrolidtoreplace);
         AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace = Ddo_contratadausuario_contratadapessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace", AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace);
         edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contratada Usuario";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtContratadaUsuario_ContratadaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_ContratadaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratadaUsuario_ContratadaCod_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Usu�rio", 0);
         cmbavOrderedby.addItem("2", "Pessoa", 0);
         cmbavOrderedby.addItem("3", "Contratada", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV48DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV48DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E268V2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV36Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV26DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuario_Nome_Titleformat = 2;
         edtUsuario_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV39ddo_Usuario_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Title", edtUsuario_Nome_Title);
         edtContratadaUsuario_UsuarioPessoaNom_Titleformat = 2;
         edtContratadaUsuario_UsuarioPessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pessoa", AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_UsuarioPessoaNom_Internalname, "Title", edtContratadaUsuario_UsuarioPessoaNom_Title);
         edtContratadaUsuario_ContratadaPessoaNom_Titleformat = 2;
         edtContratadaUsuario_ContratadaPessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratadaUsuario_ContratadaPessoaNom_Internalname, "Title", edtContratadaUsuario_ContratadaPessoaNom_Title);
         AV50GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50GridCurrentPage), 10, 0)));
         AV51GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36Usuario_NomeTitleFilterData", AV36Usuario_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData", AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData", AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E118V2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV49PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV49PageToGo) ;
         }
      }

      protected void E128V2( )
      {
         /* Ddo_usuario_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFUsuario_Nome = Ddo_usuario_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFUsuario_Nome", AV37TFUsuario_Nome);
            AV38TFUsuario_Nome_Sel = Ddo_usuario_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome_Sel", AV38TFUsuario_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E138V2( )
      {
         /* Ddo_contratadausuario_usuariopessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_usuariopessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFContratadaUsuario_UsuarioPessoaNom = Ddo_contratadausuario_usuariopessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratadaUsuario_UsuarioPessoaNom", AV41TFContratadaUsuario_UsuarioPessoaNom);
            AV42TFContratadaUsuario_UsuarioPessoaNom_Sel = Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratadaUsuario_UsuarioPessoaNom_Sel", AV42TFContratadaUsuario_UsuarioPessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E148V2( )
      {
         /* Ddo_contratadausuario_contratadapessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratadausuario_contratadapessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratadausuario_contratadapessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_contratadapessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_contratadapessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_contratadapessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratadausuario_contratadapessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_contratadapessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_contratadapessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratadausuario_contratadapessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFContratadaUsuario_ContratadaPessoaNom = Ddo_contratadausuario_contratadapessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratadaUsuario_ContratadaPessoaNom", AV45TFContratadaUsuario_ContratadaPessoaNom);
            AV46TFContratadaUsuario_ContratadaPessoaNom_Sel = Ddo_contratadausuario_contratadapessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratadaUsuario_ContratadaPessoaNom_Sel", AV46TFContratadaUsuario_ContratadaPessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E278V2( )
      {
         /* Grid_Load Routine */
         AV34Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV34Select);
         AV54Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         edtUsuario_Nome_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A69ContratadaUsuario_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E288V2 */
         E288V2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E288V2( )
      {
         /* Enter Routine */
         AV7InOutContratadaUsuario_ContratadaCod = A66ContratadaUsuario_ContratadaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratadaUsuario_ContratadaCod), 6, 0)));
         AV8InOutContratadaUsuario_UsuarioCod = A69ContratadaUsuario_UsuarioCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratadaUsuario_UsuarioCod), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratadaUsuario_ContratadaCod,(int)AV8InOutContratadaUsuario_UsuarioCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E158V2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E208V2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
      }

      protected void E168V2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E218V2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E228V2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
      }

      protected void E178V2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E238V2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E188V2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Usuario_Nome1, AV18ContratadaUsuario_UsuarioPessoaNom1, AV19ContratadaUsuario_ContratadaPessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Usuario_Nome2, AV24ContratadaUsuario_UsuarioPessoaNom2, AV25ContratadaUsuario_ContratadaPessoaNom2, AV27DynamicFiltersSelector3, AV28DynamicFiltersOperator3, AV29Usuario_Nome3, AV30ContratadaUsuario_UsuarioPessoaNom3, AV31ContratadaUsuario_ContratadaPessoaNom3, AV20DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV37TFUsuario_Nome, AV38TFUsuario_Nome_Sel, AV41TFContratadaUsuario_UsuarioPessoaNom, AV42TFContratadaUsuario_UsuarioPessoaNom_Sel, AV45TFContratadaUsuario_ContratadaPessoaNom, AV46TFContratadaUsuario_ContratadaPessoaNom_Sel, AV39ddo_Usuario_NomeTitleControlIdToReplace, AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace, AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace, AV55Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A69ContratadaUsuario_UsuarioCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E248V2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E198V2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_usuario_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
         Ddo_contratadausuario_contratadapessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_contratadapessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_contratadapessoanom_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_usuario_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratadausuario_usuariopessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_usuariopessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratadausuario_contratadapessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_contratadapessoanom_Internalname, "SortedStatus", Ddo_contratadausuario_contratadapessoanom_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUsuario_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome1_Visible), 5, 0)));
         edtavContratadausuario_usuariopessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom1_Visible), 5, 0)));
         edtavContratadausuario_contratadapessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_contratadapessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_contratadapessoanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
         {
            edtavUsuario_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            edtavContratadausuario_usuariopessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_usuariopessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
         {
            edtavContratadausuario_contratadapessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_contratadapessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_contratadapessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUsuario_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome2_Visible), 5, 0)));
         edtavContratadausuario_usuariopessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_usuariopessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom2_Visible), 5, 0)));
         edtavContratadausuario_contratadapessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_contratadapessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_contratadapessoanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
         {
            edtavUsuario_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            edtavContratadausuario_usuariopessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_usuariopessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
         {
            edtavContratadausuario_contratadapessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_contratadapessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_contratadapessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUsuario_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome3_Visible), 5, 0)));
         edtavContratadausuario_usuariopessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_usuariopessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom3_Visible), 5, 0)));
         edtavContratadausuario_contratadapessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_contratadapessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_contratadapessoanom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 )
         {
            edtavUsuario_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            edtavContratadausuario_usuariopessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_usuariopessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_usuariopessoanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
         {
            edtavContratadausuario_contratadapessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratadausuario_contratadapessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadausuario_contratadapessoanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23Usuario_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV28DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
         AV29Usuario_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37TFUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFUsuario_Nome", AV37TFUsuario_Nome);
         Ddo_usuario_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
         AV38TFUsuario_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFUsuario_Nome_Sel", AV38TFUsuario_Nome_Sel);
         Ddo_usuario_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
         AV41TFContratadaUsuario_UsuarioPessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratadaUsuario_UsuarioPessoaNom", AV41TFContratadaUsuario_UsuarioPessoaNom);
         Ddo_contratadausuario_usuariopessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_usuariopessoanom_Internalname, "FilteredText_set", Ddo_contratadausuario_usuariopessoanom_Filteredtext_set);
         AV42TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFContratadaUsuario_UsuarioPessoaNom_Sel", AV42TFContratadaUsuario_UsuarioPessoaNom_Sel);
         Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_usuariopessoanom_Internalname, "SelectedValue_set", Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set);
         AV45TFContratadaUsuario_ContratadaPessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratadaUsuario_ContratadaPessoaNom", AV45TFContratadaUsuario_ContratadaPessoaNom);
         Ddo_contratadausuario_contratadapessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_contratadapessoanom_Internalname, "FilteredText_set", Ddo_contratadausuario_contratadapessoanom_Filteredtext_set);
         AV46TFContratadaUsuario_ContratadaPessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContratadaUsuario_ContratadaPessoaNom_Sel", AV46TFContratadaUsuario_ContratadaPessoaNom_Sel);
         Ddo_contratadausuario_contratadapessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratadausuario_contratadapessoanom_Internalname, "SelectedValue_set", Ddo_contratadausuario_contratadapessoanom_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "USUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Usuario_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Usuario_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Usuario_Nome1", AV17Usuario_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18ContratadaUsuario_UsuarioPessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratadaUsuario_UsuarioPessoaNom1", AV18ContratadaUsuario_UsuarioPessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19ContratadaUsuario_ContratadaPessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratadaUsuario_ContratadaPessoaNom1", AV19ContratadaUsuario_ContratadaPessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Usuario_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Usuario_Nome2", AV23Usuario_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24ContratadaUsuario_UsuarioPessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratadaUsuario_UsuarioPessoaNom2", AV24ContratadaUsuario_UsuarioPessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25ContratadaUsuario_ContratadaPessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratadaUsuario_ContratadaPessoaNom2", AV25ContratadaUsuario_ContratadaPessoaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV29Usuario_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Usuario_Nome3", AV29Usuario_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV30ContratadaUsuario_UsuarioPessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContratadaUsuario_UsuarioPessoaNom3", AV30ContratadaUsuario_UsuarioPessoaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
                  {
                     AV28DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)));
                     AV31ContratadaUsuario_ContratadaPessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContratadaUsuario_ContratadaPessoaNom3", AV31ContratadaUsuario_ContratadaPessoaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFUsuario_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV37TFUsuario_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFUsuario_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratadaUsuario_UsuarioPessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_USUARIOPESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFContratadaUsuario_UsuarioPessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFContratadaUsuario_UsuarioPessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratadaUsuario_ContratadaPessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_CONTRATADAPESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFContratadaUsuario_ContratadaPessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFContratadaUsuario_ContratadaPessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV55Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Usuario_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Usuario_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18ContratadaUsuario_UsuarioPessoaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19ContratadaUsuario_ContratadaPessoaNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Usuario_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Usuario_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24ContratadaUsuario_UsuarioPessoaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContratadaUsuario_ContratadaPessoaNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV29Usuario_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV30ContratadaUsuario_UsuarioPessoaNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31ContratadaUsuario_ContratadaPessoaNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator3;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_8V2( true) ;
         }
         else
         {
            wb_table2_5_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_8V2( true) ;
         }
         else
         {
            wb_table3_80_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8V2e( true) ;
         }
         else
         {
            wb_table1_2_8V2e( false) ;
         }
      }

      protected void wb_table3_80_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_8V2( true) ;
         }
         else
         {
            wb_table4_83_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_8V2e( true) ;
         }
         else
         {
            wb_table3_80_8V2e( false) ;
         }
      }

      protected void wb_table4_83_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Pessoa Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Pessoa Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Usuario_Usuario Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratadaUsuario_UsuarioPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratadaUsuario_UsuarioPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratadaUsuario_UsuarioPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratadaUsuario_ContratadaPessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratadaUsuario_ContratadaPessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratadaUsuario_ContratadaPessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2Usuario_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUsuario_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratadaUsuario_UsuarioPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratadaUsuario_UsuarioPessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratadaUsuario_ContratadaPessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratadaUsuario_ContratadaPessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_8V2e( true) ;
         }
         else
         {
            wb_table4_83_8V2e( false) ;
         }
      }

      protected void wb_table2_5_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratadaUsuario.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_8V2( true) ;
         }
         else
         {
            wb_table5_14_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_8V2e( true) ;
         }
         else
         {
            wb_table2_5_8V2e( false) ;
         }
      }

      protected void wb_table5_14_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_8V2( true) ;
         }
         else
         {
            wb_table6_19_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_8V2e( true) ;
         }
         else
         {
            wb_table5_14_8V2e( false) ;
         }
      }

      protected void wb_table6_19_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratadaUsuario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_8V2( true) ;
         }
         else
         {
            wb_table7_28_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratadaUsuario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptContratadaUsuario.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_8V2( true) ;
         }
         else
         {
            wb_table8_47_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratadaUsuario.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptContratadaUsuario.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_8V2( true) ;
         }
         else
         {
            wb_table9_66_8V2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_8V2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_8V2e( true) ;
         }
         else
         {
            wb_table6_19_8V2e( false) ;
         }
      }

      protected void wb_table9_66_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_PromptContratadaUsuario.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_nome3_Internalname, StringUtil.RTrim( AV29Usuario_Nome3), StringUtil.RTrim( context.localUtil.Format( AV29Usuario_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_usuariopessoanom3_Internalname, StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV30ContratadaUsuario_UsuarioPessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_usuariopessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_usuariopessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_contratadapessoanom3_Internalname, StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV31ContratadaUsuario_ContratadaPessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_contratadapessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_contratadapessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_8V2e( true) ;
         }
         else
         {
            wb_table9_66_8V2e( false) ;
         }
      }

      protected void wb_table8_47_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_PromptContratadaUsuario.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_nome2_Internalname, StringUtil.RTrim( AV23Usuario_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Usuario_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_usuariopessoanom2_Internalname, StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV24ContratadaUsuario_UsuarioPessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_usuariopessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_usuariopessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_contratadapessoanom2_Internalname, StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV25ContratadaUsuario_ContratadaPessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_contratadapessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_contratadapessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_8V2e( true) ;
         }
         else
         {
            wb_table8_47_8V2e( false) ;
         }
      }

      protected void wb_table7_28_8V2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratadaUsuario.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_nome1_Internalname, StringUtil.RTrim( AV17Usuario_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Usuario_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_usuariopessoanom1_Internalname, StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV18ContratadaUsuario_UsuarioPessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_usuariopessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_usuariopessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratadausuario_contratadapessoanom1_Internalname, StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19ContratadaUsuario_ContratadaPessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratadausuario_contratadapessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratadausuario_contratadapessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratadaUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_8V2e( true) ;
         }
         else
         {
            wb_table7_28_8V2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratadaUsuario_ContratadaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratadaUsuario_ContratadaCod), 6, 0)));
         AV8InOutContratadaUsuario_UsuarioCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratadaUsuario_UsuarioCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8V2( ) ;
         WS8V2( ) ;
         WE8V2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117382047");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratadausuario.js", "?20203117382048");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtContratadaUsuario_ContratadaPessoaCod_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOACOD_"+sGXsfl_86_idx;
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOACOD_"+sGXsfl_86_idx;
         edtContratadaUsuario_UsuarioCod_Internalname = "CONTRATADAUSUARIO_USUARIOCOD_"+sGXsfl_86_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_86_idx;
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOANOM_"+sGXsfl_86_idx;
         edtContratadaUsuario_ContratadaPessoaNom_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOANOM_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtContratadaUsuario_ContratadaPessoaCod_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOACOD_"+sGXsfl_86_fel_idx;
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOACOD_"+sGXsfl_86_fel_idx;
         edtContratadaUsuario_UsuarioCod_Internalname = "CONTRATADAUSUARIO_USUARIOCOD_"+sGXsfl_86_fel_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_86_fel_idx;
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOANOM_"+sGXsfl_86_fel_idx;
         edtContratadaUsuario_ContratadaPessoaNom_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOANOM_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WB8V0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV34Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV54Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Select)) ? AV54Select_GXI : context.PathToRelativeUrl( AV34Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV34Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_ContratadaPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_ContratadaPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_UsuarioPessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A70ContratadaUsuario_UsuarioPessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_UsuarioPessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Nome_Internalname,StringUtil.RTrim( A2Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtUsuario_Nome_Link,(String)"",(String)"",(String)"",(String)edtUsuario_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_UsuarioPessoaNom_Internalname,StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom),StringUtil.RTrim( context.localUtil.Format( A71ContratadaUsuario_UsuarioPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_UsuarioPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratadaUsuario_ContratadaPessoaNom_Internalname,StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom),StringUtil.RTrim( context.localUtil.Format( A68ContratadaUsuario_ContratadaPessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratadaUsuario_ContratadaPessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADAUSUARIO_USUARIOCOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A69ContratadaUsuario_UsuarioCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavUsuario_nome1_Internalname = "vUSUARIO_NOME1";
         edtavContratadausuario_usuariopessoanom1_Internalname = "vCONTRATADAUSUARIO_USUARIOPESSOANOM1";
         edtavContratadausuario_contratadapessoanom1_Internalname = "vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavUsuario_nome2_Internalname = "vUSUARIO_NOME2";
         edtavContratadausuario_usuariopessoanom2_Internalname = "vCONTRATADAUSUARIO_USUARIOPESSOANOM2";
         edtavContratadausuario_contratadapessoanom2_Internalname = "vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavUsuario_nome3_Internalname = "vUSUARIO_NOME3";
         edtavContratadausuario_usuariopessoanom3_Internalname = "vCONTRATADAUSUARIO_USUARIOPESSOANOM3";
         edtavContratadausuario_contratadapessoanom3_Internalname = "vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratadaUsuario_ContratadaPessoaCod_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOACOD";
         edtContratadaUsuario_UsuarioPessoaCod_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOACOD";
         edtContratadaUsuario_UsuarioCod_Internalname = "CONTRATADAUSUARIO_USUARIOCOD";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         edtContratadaUsuario_UsuarioPessoaNom_Internalname = "CONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtContratadaUsuario_ContratadaPessoaNom_Internalname = "CONTRATADAUSUARIO_CONTRATADAPESSOANOM";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratadaUsuario_ContratadaCod_Internalname = "CONTRATADAUSUARIO_CONTRATADACOD";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfusuario_nome_Internalname = "vTFUSUARIO_NOME";
         edtavTfusuario_nome_sel_Internalname = "vTFUSUARIO_NOME_SEL";
         edtavTfcontratadausuario_usuariopessoanom_Internalname = "vTFCONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtavTfcontratadausuario_usuariopessoanom_sel_Internalname = "vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL";
         edtavTfcontratadausuario_contratadapessoanom_Internalname = "vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM";
         edtavTfcontratadausuario_contratadapessoanom_sel_Internalname = "vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL";
         Ddo_usuario_nome_Internalname = "DDO_USUARIO_NOME";
         edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_contratadausuario_usuariopessoanom_Internalname = "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM";
         edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratadausuario_contratadapessoanom_Internalname = "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM";
         edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratadaUsuario_ContratadaPessoaNom_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaNom_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         edtContratadaUsuario_UsuarioCod_Jsonclick = "";
         edtContratadaUsuario_UsuarioPessoaCod_Jsonclick = "";
         edtContratadaUsuario_ContratadaPessoaCod_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratadausuario_contratadapessoanom1_Jsonclick = "";
         edtavContratadausuario_usuariopessoanom1_Jsonclick = "";
         edtavUsuario_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratadausuario_contratadapessoanom2_Jsonclick = "";
         edtavContratadausuario_usuariopessoanom2_Jsonclick = "";
         edtavUsuario_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratadausuario_contratadapessoanom3_Jsonclick = "";
         edtavContratadausuario_usuariopessoanom3_Jsonclick = "";
         edtavUsuario_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtUsuario_Nome_Link = "";
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratadaUsuario_ContratadaPessoaNom_Titleformat = 0;
         edtContratadaUsuario_UsuarioPessoaNom_Titleformat = 0;
         edtUsuario_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratadausuario_contratadapessoanom3_Visible = 1;
         edtavContratadausuario_usuariopessoanom3_Visible = 1;
         edtavUsuario_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratadausuario_contratadapessoanom2_Visible = 1;
         edtavContratadausuario_usuariopessoanom2_Visible = 1;
         edtavUsuario_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratadausuario_contratadapessoanom1_Visible = 1;
         edtavContratadausuario_usuariopessoanom1_Visible = 1;
         edtavUsuario_nome1_Visible = 1;
         edtContratadaUsuario_ContratadaPessoaNom_Title = "Contratada";
         edtContratadaUsuario_UsuarioPessoaNom_Title = "Pessoa";
         edtUsuario_Nome_Title = "Usu�rio";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratadausuario_contratadapessoanom_sel_Jsonclick = "";
         edtavTfcontratadausuario_contratadapessoanom_sel_Visible = 1;
         edtavTfcontratadausuario_contratadapessoanom_Jsonclick = "";
         edtavTfcontratadausuario_contratadapessoanom_Visible = 1;
         edtavTfcontratadausuario_usuariopessoanom_sel_Jsonclick = "";
         edtavTfcontratadausuario_usuariopessoanom_sel_Visible = 1;
         edtavTfcontratadausuario_usuariopessoanom_Jsonclick = "";
         edtavTfcontratadausuario_usuariopessoanom_Visible = 1;
         edtavTfusuario_nome_sel_Jsonclick = "";
         edtavTfusuario_nome_sel_Visible = 1;
         edtavTfusuario_nome_Jsonclick = "";
         edtavTfusuario_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContratadaUsuario_ContratadaCod_Jsonclick = "";
         edtContratadaUsuario_ContratadaCod_Visible = 1;
         Ddo_contratadausuario_contratadapessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratadausuario_contratadapessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratadausuario_contratadapessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratadausuario_contratadapessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratadausuario_contratadapessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratadausuario_contratadapessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratadausuario_contratadapessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratadausuario_contratadapessoanom_Datalistproc = "GetPromptContratadaUsuarioFilterData";
         Ddo_contratadausuario_contratadapessoanom_Datalisttype = "Dynamic";
         Ddo_contratadausuario_contratadapessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratadausuario_contratadapessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratadausuario_contratadapessoanom_Filtertype = "Character";
         Ddo_contratadausuario_contratadapessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratadausuario_contratadapessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_contratadapessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_contratadapessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratadausuario_contratadapessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratadausuario_contratadapessoanom_Cls = "ColumnSettings";
         Ddo_contratadausuario_contratadapessoanom_Tooltip = "Op��es";
         Ddo_contratadausuario_contratadapessoanom_Caption = "";
         Ddo_contratadausuario_usuariopessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratadausuario_usuariopessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratadausuario_usuariopessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratadausuario_usuariopessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratadausuario_usuariopessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratadausuario_usuariopessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratadausuario_usuariopessoanom_Datalistproc = "GetPromptContratadaUsuarioFilterData";
         Ddo_contratadausuario_usuariopessoanom_Datalisttype = "Dynamic";
         Ddo_contratadausuario_usuariopessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratadausuario_usuariopessoanom_Filtertype = "Character";
         Ddo_contratadausuario_usuariopessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratadausuario_usuariopessoanom_Cls = "ColumnSettings";
         Ddo_contratadausuario_usuariopessoanom_Tooltip = "Op��es";
         Ddo_contratadausuario_usuariopessoanom_Caption = "";
         Ddo_usuario_nome_Searchbuttontext = "Pesquisar";
         Ddo_usuario_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_nome_Loadingdata = "Carregando dados...";
         Ddo_usuario_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_nome_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_nome_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_nome_Datalistproc = "GetPromptContratadaUsuarioFilterData";
         Ddo_usuario_nome_Datalisttype = "Dynamic";
         Ddo_usuario_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_nome_Filtertype = "Character";
         Ddo_usuario_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Titlecontrolidtoreplace = "";
         Ddo_usuario_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_nome_Cls = "ColumnSettings";
         Ddo_usuario_nome_Tooltip = "Op��es";
         Ddo_usuario_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contratada Usuario";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''}],oparms:[{av:'AV36Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Titleformat',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Titleformat'},{av:'edtContratadaUsuario_UsuarioPessoaNom_Title',ctrl:'CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'Title'},{av:'edtContratadaUsuario_ContratadaPessoaNom_Titleformat',ctrl:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'Titleformat'},{av:'edtContratadaUsuario_ContratadaPessoaNom_Title',ctrl:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'Title'},{av:'AV50GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV51GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E118V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_USUARIO_NOME.ONOPTIONCLICKED","{handler:'E128V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuario_nome_Activeeventkey',ctrl:'DDO_USUARIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_usuario_nome_Filteredtext_get',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_get'},{av:'Ddo_usuario_nome_Selectedvalue_get',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratadausuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratadausuario_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM.ONOPTIONCLICKED","{handler:'E138V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratadausuario_usuariopessoanom_Activeeventkey',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratadausuario_usuariopessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratadausuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_contratadausuario_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM.ONOPTIONCLICKED","{handler:'E148V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratadausuario_contratadapessoanom_Activeeventkey',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratadausuario_contratadapessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratadausuario_contratadapessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratadausuario_contratadapessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'SortedStatus'},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_contratadausuario_usuariopessoanom_Sortedstatus',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E278V2',iparms:[{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV34Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'},{av:'edtUsuario_Nome_Link',ctrl:'USUARIO_NOME',prop:'Link'}]}");
         setEventMetadata("ENTER","{handler:'E288V2',iparms:[{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutContratadaUsuario_ContratadaCod',fld:'vINOUTCONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratadaUsuario_UsuarioCod',fld:'vINOUTCONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E158V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E208V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E168V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuario_nome3_Visible',ctrl:'vUSUARIO_NOME3',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E218V2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E228V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E178V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuario_nome3_Visible',ctrl:'vUSUARIO_NOME3',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E238V2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E188V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuario_nome3_Visible',ctrl:'vUSUARIO_NOME3',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E248V2',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavUsuario_nome3_Visible',ctrl:'vUSUARIO_NOME3',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E198V2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV39ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_USUARIOPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV37TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Filteredtext_set',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_set'},{av:'AV38TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Selectedvalue_set',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_set'},{av:'AV41TFContratadaUsuario_UsuarioPessoaNom',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratadausuario_usuariopessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'FilteredText_set'},{av:'AV42TFContratadaUsuario_UsuarioPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM',prop:'SelectedValue_set'},{av:'AV45TFContratadaUsuario_ContratadaPessoaNom',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratadausuario_contratadapessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'FilteredText_set'},{av:'AV46TFContratadaUsuario_ContratadaPessoaNom_Sel',fld:'vTFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratadausuario_contratadapessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Usuario_Nome1',fld:'vUSUARIO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavUsuario_nome1_Visible',ctrl:'vUSUARIO_NOME1',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom1_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Usuario_Nome2',fld:'vUSUARIO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Usuario_Nome3',fld:'vUSUARIO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18ContratadaUsuario_UsuarioPessoaNom1',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM1',pic:'@!',nv:''},{av:'AV19ContratadaUsuario_ContratadaPessoaNom1',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24ContratadaUsuario_UsuarioPessoaNom2',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',pic:'@!',nv:''},{av:'AV25ContratadaUsuario_ContratadaPessoaNom2',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',pic:'@!',nv:''},{av:'AV30ContratadaUsuario_UsuarioPessoaNom3',fld:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',pic:'@!',nv:''},{av:'AV31ContratadaUsuario_ContratadaPessoaNom3',fld:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',pic:'@!',nv:''},{av:'edtavUsuario_nome2_Visible',ctrl:'vUSUARIO_NOME2',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM2',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom2_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuario_nome3_Visible',ctrl:'vUSUARIO_NOME3',prop:'Visible'},{av:'edtavContratadausuario_usuariopessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_USUARIOPESSOANOM3',prop:'Visible'},{av:'edtavContratadausuario_contratadapessoanom3_Visible',ctrl:'vCONTRATADAUSUARIO_CONTRATADAPESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_usuario_nome_Activeeventkey = "";
         Ddo_usuario_nome_Filteredtext_get = "";
         Ddo_usuario_nome_Selectedvalue_get = "";
         Ddo_contratadausuario_usuariopessoanom_Activeeventkey = "";
         Ddo_contratadausuario_usuariopessoanom_Filteredtext_get = "";
         Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get = "";
         Ddo_contratadausuario_contratadapessoanom_Activeeventkey = "";
         Ddo_contratadausuario_contratadapessoanom_Filteredtext_get = "";
         Ddo_contratadausuario_contratadapessoanom_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Usuario_Nome1 = "";
         AV18ContratadaUsuario_UsuarioPessoaNom1 = "";
         AV19ContratadaUsuario_ContratadaPessoaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Usuario_Nome2 = "";
         AV24ContratadaUsuario_UsuarioPessoaNom2 = "";
         AV25ContratadaUsuario_ContratadaPessoaNom2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV29Usuario_Nome3 = "";
         AV30ContratadaUsuario_UsuarioPessoaNom3 = "";
         AV31ContratadaUsuario_ContratadaPessoaNom3 = "";
         AV37TFUsuario_Nome = "";
         AV38TFUsuario_Nome_Sel = "";
         AV41TFContratadaUsuario_UsuarioPessoaNom = "";
         AV42TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         AV45TFContratadaUsuario_ContratadaPessoaNom = "";
         AV46TFContratadaUsuario_ContratadaPessoaNom_Sel = "";
         AV39ddo_Usuario_NomeTitleControlIdToReplace = "";
         AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace = "";
         AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace = "";
         AV55Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV48DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_usuario_nome_Filteredtext_set = "";
         Ddo_usuario_nome_Selectedvalue_set = "";
         Ddo_usuario_nome_Sortedstatus = "";
         Ddo_contratadausuario_usuariopessoanom_Filteredtext_set = "";
         Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set = "";
         Ddo_contratadausuario_usuariopessoanom_Sortedstatus = "";
         Ddo_contratadausuario_contratadapessoanom_Filteredtext_set = "";
         Ddo_contratadausuario_contratadapessoanom_Selectedvalue_set = "";
         Ddo_contratadausuario_contratadapessoanom_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Select = "";
         AV54Select_GXI = "";
         A2Usuario_Nome = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17Usuario_Nome1 = "";
         lV18ContratadaUsuario_UsuarioPessoaNom1 = "";
         lV19ContratadaUsuario_ContratadaPessoaNom1 = "";
         lV23Usuario_Nome2 = "";
         lV24ContratadaUsuario_UsuarioPessoaNom2 = "";
         lV25ContratadaUsuario_ContratadaPessoaNom2 = "";
         lV29Usuario_Nome3 = "";
         lV30ContratadaUsuario_UsuarioPessoaNom3 = "";
         lV31ContratadaUsuario_ContratadaPessoaNom3 = "";
         lV37TFUsuario_Nome = "";
         lV41TFContratadaUsuario_UsuarioPessoaNom = "";
         lV45TFContratadaUsuario_ContratadaPessoaNom = "";
         H008V2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H008V2_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H008V2_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         H008V2_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H008V2_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H008V2_A2Usuario_Nome = new String[] {""} ;
         H008V2_n2Usuario_Nome = new bool[] {false} ;
         H008V2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H008V2_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H008V2_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008V2_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H008V2_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H008V3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratadausuario__default(),
            new Object[][] {
                new Object[] {
               H008V2_A66ContratadaUsuario_ContratadaCod, H008V2_A68ContratadaUsuario_ContratadaPessoaNom, H008V2_n68ContratadaUsuario_ContratadaPessoaNom, H008V2_A71ContratadaUsuario_UsuarioPessoaNom, H008V2_n71ContratadaUsuario_UsuarioPessoaNom, H008V2_A2Usuario_Nome, H008V2_n2Usuario_Nome, H008V2_A69ContratadaUsuario_UsuarioCod, H008V2_A70ContratadaUsuario_UsuarioPessoaCod, H008V2_n70ContratadaUsuario_UsuarioPessoaCod,
               H008V2_A67ContratadaUsuario_ContratadaPessoaCod, H008V2_n67ContratadaUsuario_ContratadaPessoaCod
               }
               , new Object[] {
               H008V3_AGRID_nRecordCount
               }
            }
         );
         AV55Pgmname = "PromptContratadaUsuario";
         /* GeneXus formulas. */
         AV55Pgmname = "PromptContratadaUsuario";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV28DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUsuario_Nome_Titleformat ;
      private short edtContratadaUsuario_UsuarioPessoaNom_Titleformat ;
      private short edtContratadaUsuario_ContratadaPessoaNom_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratadaUsuario_ContratadaCod ;
      private int AV8InOutContratadaUsuario_UsuarioCod ;
      private int wcpOAV7InOutContratadaUsuario_ContratadaCod ;
      private int wcpOAV8InOutContratadaUsuario_UsuarioCod ;
      private int subGrid_Rows ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_usuario_nome_Datalistupdateminimumcharacters ;
      private int Ddo_contratadausuario_usuariopessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratadausuario_contratadapessoanom_Datalistupdateminimumcharacters ;
      private int edtContratadaUsuario_ContratadaCod_Visible ;
      private int edtavTfusuario_nome_Visible ;
      private int edtavTfusuario_nome_sel_Visible ;
      private int edtavTfcontratadausuario_usuariopessoanom_Visible ;
      private int edtavTfcontratadausuario_usuariopessoanom_sel_Visible ;
      private int edtavTfcontratadausuario_contratadapessoanom_Visible ;
      private int edtavTfcontratadausuario_contratadapessoanom_sel_Visible ;
      private int edtavDdo_usuario_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Visible ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV49PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavUsuario_nome1_Visible ;
      private int edtavContratadausuario_usuariopessoanom1_Visible ;
      private int edtavContratadausuario_contratadapessoanom1_Visible ;
      private int edtavUsuario_nome2_Visible ;
      private int edtavContratadausuario_usuariopessoanom2_Visible ;
      private int edtavContratadausuario_contratadapessoanom2_Visible ;
      private int edtavUsuario_nome3_Visible ;
      private int edtavContratadausuario_usuariopessoanom3_Visible ;
      private int edtavContratadausuario_contratadapessoanom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV50GridCurrentPage ;
      private long AV51GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_usuario_nome_Activeeventkey ;
      private String Ddo_usuario_nome_Filteredtext_get ;
      private String Ddo_usuario_nome_Selectedvalue_get ;
      private String Ddo_contratadausuario_usuariopessoanom_Activeeventkey ;
      private String Ddo_contratadausuario_usuariopessoanom_Filteredtext_get ;
      private String Ddo_contratadausuario_usuariopessoanom_Selectedvalue_get ;
      private String Ddo_contratadausuario_contratadapessoanom_Activeeventkey ;
      private String Ddo_contratadausuario_contratadapessoanom_Filteredtext_get ;
      private String Ddo_contratadausuario_contratadapessoanom_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String AV17Usuario_Nome1 ;
      private String AV18ContratadaUsuario_UsuarioPessoaNom1 ;
      private String AV19ContratadaUsuario_ContratadaPessoaNom1 ;
      private String AV23Usuario_Nome2 ;
      private String AV24ContratadaUsuario_UsuarioPessoaNom2 ;
      private String AV25ContratadaUsuario_ContratadaPessoaNom2 ;
      private String AV29Usuario_Nome3 ;
      private String AV30ContratadaUsuario_UsuarioPessoaNom3 ;
      private String AV31ContratadaUsuario_ContratadaPessoaNom3 ;
      private String AV37TFUsuario_Nome ;
      private String AV38TFUsuario_Nome_Sel ;
      private String AV41TFContratadaUsuario_UsuarioPessoaNom ;
      private String AV42TFContratadaUsuario_UsuarioPessoaNom_Sel ;
      private String AV45TFContratadaUsuario_ContratadaPessoaNom ;
      private String AV46TFContratadaUsuario_ContratadaPessoaNom_Sel ;
      private String AV55Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_usuario_nome_Caption ;
      private String Ddo_usuario_nome_Tooltip ;
      private String Ddo_usuario_nome_Cls ;
      private String Ddo_usuario_nome_Filteredtext_set ;
      private String Ddo_usuario_nome_Selectedvalue_set ;
      private String Ddo_usuario_nome_Dropdownoptionstype ;
      private String Ddo_usuario_nome_Titlecontrolidtoreplace ;
      private String Ddo_usuario_nome_Sortedstatus ;
      private String Ddo_usuario_nome_Filtertype ;
      private String Ddo_usuario_nome_Datalisttype ;
      private String Ddo_usuario_nome_Datalistproc ;
      private String Ddo_usuario_nome_Sortasc ;
      private String Ddo_usuario_nome_Sortdsc ;
      private String Ddo_usuario_nome_Loadingdata ;
      private String Ddo_usuario_nome_Cleanfilter ;
      private String Ddo_usuario_nome_Noresultsfound ;
      private String Ddo_usuario_nome_Searchbuttontext ;
      private String Ddo_contratadausuario_usuariopessoanom_Caption ;
      private String Ddo_contratadausuario_usuariopessoanom_Tooltip ;
      private String Ddo_contratadausuario_usuariopessoanom_Cls ;
      private String Ddo_contratadausuario_usuariopessoanom_Filteredtext_set ;
      private String Ddo_contratadausuario_usuariopessoanom_Selectedvalue_set ;
      private String Ddo_contratadausuario_usuariopessoanom_Dropdownoptionstype ;
      private String Ddo_contratadausuario_usuariopessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratadausuario_usuariopessoanom_Sortedstatus ;
      private String Ddo_contratadausuario_usuariopessoanom_Filtertype ;
      private String Ddo_contratadausuario_usuariopessoanom_Datalisttype ;
      private String Ddo_contratadausuario_usuariopessoanom_Datalistproc ;
      private String Ddo_contratadausuario_usuariopessoanom_Sortasc ;
      private String Ddo_contratadausuario_usuariopessoanom_Sortdsc ;
      private String Ddo_contratadausuario_usuariopessoanom_Loadingdata ;
      private String Ddo_contratadausuario_usuariopessoanom_Cleanfilter ;
      private String Ddo_contratadausuario_usuariopessoanom_Noresultsfound ;
      private String Ddo_contratadausuario_usuariopessoanom_Searchbuttontext ;
      private String Ddo_contratadausuario_contratadapessoanom_Caption ;
      private String Ddo_contratadausuario_contratadapessoanom_Tooltip ;
      private String Ddo_contratadausuario_contratadapessoanom_Cls ;
      private String Ddo_contratadausuario_contratadapessoanom_Filteredtext_set ;
      private String Ddo_contratadausuario_contratadapessoanom_Selectedvalue_set ;
      private String Ddo_contratadausuario_contratadapessoanom_Dropdownoptionstype ;
      private String Ddo_contratadausuario_contratadapessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratadausuario_contratadapessoanom_Sortedstatus ;
      private String Ddo_contratadausuario_contratadapessoanom_Filtertype ;
      private String Ddo_contratadausuario_contratadapessoanom_Datalisttype ;
      private String Ddo_contratadausuario_contratadapessoanom_Datalistproc ;
      private String Ddo_contratadausuario_contratadapessoanom_Sortasc ;
      private String Ddo_contratadausuario_contratadapessoanom_Sortdsc ;
      private String Ddo_contratadausuario_contratadapessoanom_Loadingdata ;
      private String Ddo_contratadausuario_contratadapessoanom_Cleanfilter ;
      private String Ddo_contratadausuario_contratadapessoanom_Noresultsfound ;
      private String Ddo_contratadausuario_contratadapessoanom_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtContratadaUsuario_ContratadaCod_Internalname ;
      private String edtContratadaUsuario_ContratadaCod_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfusuario_nome_Internalname ;
      private String edtavTfusuario_nome_Jsonclick ;
      private String edtavTfusuario_nome_sel_Internalname ;
      private String edtavTfusuario_nome_sel_Jsonclick ;
      private String edtavTfcontratadausuario_usuariopessoanom_Internalname ;
      private String edtavTfcontratadausuario_usuariopessoanom_Jsonclick ;
      private String edtavTfcontratadausuario_usuariopessoanom_sel_Internalname ;
      private String edtavTfcontratadausuario_usuariopessoanom_sel_Jsonclick ;
      private String edtavTfcontratadausuario_contratadapessoanom_Internalname ;
      private String edtavTfcontratadausuario_contratadapessoanom_Jsonclick ;
      private String edtavTfcontratadausuario_contratadapessoanom_sel_Internalname ;
      private String edtavTfcontratadausuario_contratadapessoanom_sel_Jsonclick ;
      private String edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratadausuario_usuariopessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratadausuario_contratadapessoanomtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratadaUsuario_ContratadaPessoaCod_Internalname ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Internalname ;
      private String edtContratadaUsuario_UsuarioCod_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Internalname ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Internalname ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17Usuario_Nome1 ;
      private String lV18ContratadaUsuario_UsuarioPessoaNom1 ;
      private String lV19ContratadaUsuario_ContratadaPessoaNom1 ;
      private String lV23Usuario_Nome2 ;
      private String lV24ContratadaUsuario_UsuarioPessoaNom2 ;
      private String lV25ContratadaUsuario_ContratadaPessoaNom2 ;
      private String lV29Usuario_Nome3 ;
      private String lV30ContratadaUsuario_UsuarioPessoaNom3 ;
      private String lV31ContratadaUsuario_ContratadaPessoaNom3 ;
      private String lV37TFUsuario_Nome ;
      private String lV41TFContratadaUsuario_UsuarioPessoaNom ;
      private String lV45TFContratadaUsuario_ContratadaPessoaNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavUsuario_nome1_Internalname ;
      private String edtavContratadausuario_usuariopessoanom1_Internalname ;
      private String edtavContratadausuario_contratadapessoanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavUsuario_nome2_Internalname ;
      private String edtavContratadausuario_usuariopessoanom2_Internalname ;
      private String edtavContratadausuario_contratadapessoanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavUsuario_nome3_Internalname ;
      private String edtavContratadausuario_usuariopessoanom3_Internalname ;
      private String edtavContratadausuario_contratadapessoanom3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_usuario_nome_Internalname ;
      private String Ddo_contratadausuario_usuariopessoanom_Internalname ;
      private String Ddo_contratadausuario_contratadapessoanom_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUsuario_Nome_Title ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Title ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Title ;
      private String edtavSelect_Tooltiptext ;
      private String edtUsuario_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavUsuario_nome3_Jsonclick ;
      private String edtavContratadausuario_usuariopessoanom3_Jsonclick ;
      private String edtavContratadausuario_contratadapessoanom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavUsuario_nome2_Jsonclick ;
      private String edtavContratadausuario_usuariopessoanom2_Jsonclick ;
      private String edtavContratadausuario_contratadapessoanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavUsuario_nome1_Jsonclick ;
      private String edtavContratadausuario_usuariopessoanom1_Jsonclick ;
      private String edtavContratadausuario_contratadapessoanom1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratadaUsuario_ContratadaPessoaCod_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaCod_Jsonclick ;
      private String edtContratadaUsuario_UsuarioCod_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String edtContratadaUsuario_UsuarioPessoaNom_Jsonclick ;
      private String edtContratadaUsuario_ContratadaPessoaNom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_usuario_nome_Includesortasc ;
      private bool Ddo_usuario_nome_Includesortdsc ;
      private bool Ddo_usuario_nome_Includefilter ;
      private bool Ddo_usuario_nome_Filterisrange ;
      private bool Ddo_usuario_nome_Includedatalist ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includesortasc ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includesortdsc ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includefilter ;
      private bool Ddo_contratadausuario_usuariopessoanom_Filterisrange ;
      private bool Ddo_contratadausuario_usuariopessoanom_Includedatalist ;
      private bool Ddo_contratadausuario_contratadapessoanom_Includesortasc ;
      private bool Ddo_contratadausuario_contratadapessoanom_Includesortdsc ;
      private bool Ddo_contratadausuario_contratadapessoanom_Includefilter ;
      private bool Ddo_contratadausuario_contratadapessoanom_Filterisrange ;
      private bool Ddo_contratadausuario_contratadapessoanom_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n2Usuario_Nome ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV39ddo_Usuario_NomeTitleControlIdToReplace ;
      private String AV43ddo_ContratadaUsuario_UsuarioPessoaNomTitleControlIdToReplace ;
      private String AV47ddo_ContratadaUsuario_ContratadaPessoaNomTitleControlIdToReplace ;
      private String AV54Select_GXI ;
      private String AV34Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratadaUsuario_ContratadaCod ;
      private int aP1_InOutContratadaUsuario_UsuarioCod ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H008V2_A66ContratadaUsuario_ContratadaCod ;
      private String[] H008V2_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H008V2_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] H008V2_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H008V2_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] H008V2_A2Usuario_Nome ;
      private bool[] H008V2_n2Usuario_Nome ;
      private int[] H008V2_A69ContratadaUsuario_UsuarioCod ;
      private int[] H008V2_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H008V2_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H008V2_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H008V2_n67ContratadaUsuario_ContratadaPessoaCod ;
      private long[] H008V3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36Usuario_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40ContratadaUsuario_UsuarioPessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44ContratadaUsuario_ContratadaPessoaNomTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV48DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratadausuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H008V2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Usuario_Nome1 ,
                                             String AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV19ContratadaUsuario_ContratadaPessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23Usuario_Nome2 ,
                                             String AV24ContratadaUsuario_UsuarioPessoaNom2 ,
                                             String AV25ContratadaUsuario_ContratadaPessoaNom2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             String AV29Usuario_Nome3 ,
                                             String AV30ContratadaUsuario_UsuarioPessoaNom3 ,
                                             String AV31ContratadaUsuario_ContratadaPessoaNom3 ,
                                             String AV38TFUsuario_Nome_Sel ,
                                             String AV37TFUsuario_Nome ,
                                             String AV42TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV41TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV46TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                             String AV45TFContratadaUsuario_ContratadaPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [29] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T4.[Usuario_Nome], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod";
         sFromString = " FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV17Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV17Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV17Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV17Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV23Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV23Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV23Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV23Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV29Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV29Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV29Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV29Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV37TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV37TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV38TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV38TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV41TFContratadaUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV41TFContratadaUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratadaUsuario_ContratadaPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV45TFContratadaUsuario_ContratadaPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV45TFContratadaUsuario_ContratadaPessoaNom)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Usuario_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Usuario_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H008V3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Usuario_Nome1 ,
                                             String AV18ContratadaUsuario_UsuarioPessoaNom1 ,
                                             String AV19ContratadaUsuario_ContratadaPessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23Usuario_Nome2 ,
                                             String AV24ContratadaUsuario_UsuarioPessoaNom2 ,
                                             String AV25ContratadaUsuario_ContratadaPessoaNom2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             short AV28DynamicFiltersOperator3 ,
                                             String AV29Usuario_Nome3 ,
                                             String AV30ContratadaUsuario_UsuarioPessoaNom3 ,
                                             String AV31ContratadaUsuario_ContratadaPessoaNom3 ,
                                             String AV38TFUsuario_Nome_Sel ,
                                             String AV37TFUsuario_Nome ,
                                             String AV42TFContratadaUsuario_UsuarioPessoaNom_Sel ,
                                             String AV41TFContratadaUsuario_UsuarioPessoaNom ,
                                             String AV46TFContratadaUsuario_ContratadaPessoaNom_Sel ,
                                             String AV45TFContratadaUsuario_ContratadaPessoaNom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [24] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV17Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV17Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Usuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV17Usuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV17Usuario_Nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ContratadaUsuario_UsuarioPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV18ContratadaUsuario_UsuarioPessoaNom1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratadaUsuario_ContratadaPessoaNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV19ContratadaUsuario_ContratadaPessoaNom1)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV23Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV23Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Usuario_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV23Usuario_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV23Usuario_Nome2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ContratadaUsuario_UsuarioPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV24ContratadaUsuario_UsuarioPessoaNom2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratadaUsuario_ContratadaPessoaNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV25ContratadaUsuario_ContratadaPessoaNom2)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV29Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV29Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "USUARIO_NOME") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Usuario_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV29Usuario_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV29Usuario_Nome3)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30ContratadaUsuario_UsuarioPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV30ContratadaUsuario_UsuarioPessoaNom3)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 ) && ( AV28DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContratadaUsuario_ContratadaPessoaNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV31ContratadaUsuario_ContratadaPessoaNom3)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV37TFUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV37TFUsuario_Nome)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV38TFUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV38TFUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContratadaUsuario_UsuarioPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV41TFContratadaUsuario_UsuarioPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV41TFContratadaUsuario_UsuarioPessoaNom)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV42TFContratadaUsuario_UsuarioPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratadaUsuario_ContratadaPessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV45TFContratadaUsuario_ContratadaPessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV45TFContratadaUsuario_ContratadaPessoaNom)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV46TFContratadaUsuario_ContratadaPessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H008V2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 1 :
                     return conditional_H008V3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008V2 ;
          prmH008V2 = new Object[] {
          new Object[] {"@lV17Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV30ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV37TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV42TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45TFContratadaUsuario_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV46TFContratadaUsuario_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH008V3 ;
          prmH008V3 = new Object[] {
          new Object[] {"@lV17Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17Usuario_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18ContratadaUsuario_UsuarioPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19ContratadaUsuario_ContratadaPessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23Usuario_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24ContratadaUsuario_UsuarioPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV25ContratadaUsuario_ContratadaPessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29Usuario_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV30ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV30ContratadaUsuario_UsuarioPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV31ContratadaUsuario_ContratadaPessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV37TFUsuario_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38TFUsuario_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV41TFContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV42TFContratadaUsuario_UsuarioPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV45TFContratadaUsuario_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV46TFContratadaUsuario_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008V2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008V2,11,0,true,false )
             ,new CursorDef("H008V3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008V3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

}
