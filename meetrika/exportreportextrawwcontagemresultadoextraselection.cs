/*
               File: ExportReportExtraWWContagemResultadoExtraSelection
        Description: Stub for ExportReportExtraWWContagemResultadoExtraSelection
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:58:0.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportreportextrawwcontagemresultadoextraselection : GXProcedure
   {
      public exportreportextrawwcontagemresultadoextraselection( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public exportreportextrawwcontagemresultadoextraselection( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           String aP2_TFContagemResultado_Agrupador ,
                           String aP3_TFContagemResultado_Agrupador_Sel ,
                           String aP4_TFContagemResultado_DemandaFM ,
                           String aP5_TFContagemResultado_DemandaFM_Sel ,
                           String aP6_TFContagemResultado_Demanda ,
                           String aP7_TFContagemResultado_Demanda_Sel ,
                           String aP8_TFContagemResultado_Descricao ,
                           String aP9_TFContagemResultado_Descricao_Sel ,
                           DateTime aP10_TFContagemResultado_DataPrevista ,
                           DateTime aP11_TFContagemResultado_DataPrevista_To ,
                           DateTime aP12_TFContagemResultado_DataDmn ,
                           DateTime aP13_TFContagemResultado_DataDmn_To ,
                           DateTime aP14_TFContagemResultado_DataUltCnt ,
                           DateTime aP15_TFContagemResultado_DataUltCnt_To ,
                           String aP16_TFContagemResultado_ContratadaSigla ,
                           String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP18_TFContagemResultado_SistemaCoord ,
                           String aP19_TFContagemResultado_SistemaCoord_Sel ,
                           String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                           short aP21_TFContagemResultado_Baseline_Sel ,
                           String aP22_TFContagemResultado_ServicoSigla ,
                           String aP23_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP24_TFContagemResultado_PFFinal ,
                           decimal aP25_TFContagemResultado_PFFinal_To ,
                           decimal aP26_TFContagemResultado_ValorPF ,
                           decimal aP27_TFContagemResultado_ValorPF_To ,
                           short aP28_OrderedBy ,
                           bool aP29_OrderedDsc ,
                           String aP30_GridStateXML )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV4TFContagemResultado_Agrupador = aP2_TFContagemResultado_Agrupador;
         this.AV5TFContagemResultado_Agrupador_Sel = aP3_TFContagemResultado_Agrupador_Sel;
         this.AV6TFContagemResultado_DemandaFM = aP4_TFContagemResultado_DemandaFM;
         this.AV7TFContagemResultado_DemandaFM_Sel = aP5_TFContagemResultado_DemandaFM_Sel;
         this.AV8TFContagemResultado_Demanda = aP6_TFContagemResultado_Demanda;
         this.AV9TFContagemResultado_Demanda_Sel = aP7_TFContagemResultado_Demanda_Sel;
         this.AV10TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV11TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV12TFContagemResultado_DataPrevista = aP10_TFContagemResultado_DataPrevista;
         this.AV13TFContagemResultado_DataPrevista_To = aP11_TFContagemResultado_DataPrevista_To;
         this.AV14TFContagemResultado_DataDmn = aP12_TFContagemResultado_DataDmn;
         this.AV15TFContagemResultado_DataDmn_To = aP13_TFContagemResultado_DataDmn_To;
         this.AV16TFContagemResultado_DataUltCnt = aP14_TFContagemResultado_DataUltCnt;
         this.AV17TFContagemResultado_DataUltCnt_To = aP15_TFContagemResultado_DataUltCnt_To;
         this.AV18TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         this.AV19TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         this.AV20TFContagemResultado_SistemaCoord = aP18_TFContagemResultado_SistemaCoord;
         this.AV21TFContagemResultado_SistemaCoord_Sel = aP19_TFContagemResultado_SistemaCoord_Sel;
         this.AV22TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         this.AV23TFContagemResultado_Baseline_Sel = aP21_TFContagemResultado_Baseline_Sel;
         this.AV24TFContagemResultado_ServicoSigla = aP22_TFContagemResultado_ServicoSigla;
         this.AV25TFContagemResultado_ServicoSigla_Sel = aP23_TFContagemResultado_ServicoSigla_Sel;
         this.AV26TFContagemResultado_PFFinal = aP24_TFContagemResultado_PFFinal;
         this.AV27TFContagemResultado_PFFinal_To = aP25_TFContagemResultado_PFFinal_To;
         this.AV28TFContagemResultado_ValorPF = aP26_TFContagemResultado_ValorPF;
         this.AV29TFContagemResultado_ValorPF_To = aP27_TFContagemResultado_ValorPF_To;
         this.AV30OrderedBy = aP28_OrderedBy;
         this.AV31OrderedDsc = aP29_OrderedDsc;
         this.AV32GridStateXML = aP30_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 String aP2_TFContagemResultado_Agrupador ,
                                 String aP3_TFContagemResultado_Agrupador_Sel ,
                                 String aP4_TFContagemResultado_DemandaFM ,
                                 String aP5_TFContagemResultado_DemandaFM_Sel ,
                                 String aP6_TFContagemResultado_Demanda ,
                                 String aP7_TFContagemResultado_Demanda_Sel ,
                                 String aP8_TFContagemResultado_Descricao ,
                                 String aP9_TFContagemResultado_Descricao_Sel ,
                                 DateTime aP10_TFContagemResultado_DataPrevista ,
                                 DateTime aP11_TFContagemResultado_DataPrevista_To ,
                                 DateTime aP12_TFContagemResultado_DataDmn ,
                                 DateTime aP13_TFContagemResultado_DataDmn_To ,
                                 DateTime aP14_TFContagemResultado_DataUltCnt ,
                                 DateTime aP15_TFContagemResultado_DataUltCnt_To ,
                                 String aP16_TFContagemResultado_ContratadaSigla ,
                                 String aP17_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP18_TFContagemResultado_SistemaCoord ,
                                 String aP19_TFContagemResultado_SistemaCoord_Sel ,
                                 String aP20_TFContagemResultado_StatusDmn_SelsJson ,
                                 short aP21_TFContagemResultado_Baseline_Sel ,
                                 String aP22_TFContagemResultado_ServicoSigla ,
                                 String aP23_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP24_TFContagemResultado_PFFinal ,
                                 decimal aP25_TFContagemResultado_PFFinal_To ,
                                 decimal aP26_TFContagemResultado_ValorPF ,
                                 decimal aP27_TFContagemResultado_ValorPF_To ,
                                 short aP28_OrderedBy ,
                                 bool aP29_OrderedDsc ,
                                 String aP30_GridStateXML )
      {
         exportreportextrawwcontagemresultadoextraselection objexportreportextrawwcontagemresultadoextraselection;
         objexportreportextrawwcontagemresultadoextraselection = new exportreportextrawwcontagemresultadoextraselection();
         objexportreportextrawwcontagemresultadoextraselection.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objexportreportextrawwcontagemresultadoextraselection.AV3ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objexportreportextrawwcontagemresultadoextraselection.AV4TFContagemResultado_Agrupador = aP2_TFContagemResultado_Agrupador;
         objexportreportextrawwcontagemresultadoextraselection.AV5TFContagemResultado_Agrupador_Sel = aP3_TFContagemResultado_Agrupador_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV6TFContagemResultado_DemandaFM = aP4_TFContagemResultado_DemandaFM;
         objexportreportextrawwcontagemresultadoextraselection.AV7TFContagemResultado_DemandaFM_Sel = aP5_TFContagemResultado_DemandaFM_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV8TFContagemResultado_Demanda = aP6_TFContagemResultado_Demanda;
         objexportreportextrawwcontagemresultadoextraselection.AV9TFContagemResultado_Demanda_Sel = aP7_TFContagemResultado_Demanda_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV10TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         objexportreportextrawwcontagemresultadoextraselection.AV11TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV12TFContagemResultado_DataPrevista = aP10_TFContagemResultado_DataPrevista;
         objexportreportextrawwcontagemresultadoextraselection.AV13TFContagemResultado_DataPrevista_To = aP11_TFContagemResultado_DataPrevista_To;
         objexportreportextrawwcontagemresultadoextraselection.AV14TFContagemResultado_DataDmn = aP12_TFContagemResultado_DataDmn;
         objexportreportextrawwcontagemresultadoextraselection.AV15TFContagemResultado_DataDmn_To = aP13_TFContagemResultado_DataDmn_To;
         objexportreportextrawwcontagemresultadoextraselection.AV16TFContagemResultado_DataUltCnt = aP14_TFContagemResultado_DataUltCnt;
         objexportreportextrawwcontagemresultadoextraselection.AV17TFContagemResultado_DataUltCnt_To = aP15_TFContagemResultado_DataUltCnt_To;
         objexportreportextrawwcontagemresultadoextraselection.AV18TFContagemResultado_ContratadaSigla = aP16_TFContagemResultado_ContratadaSigla;
         objexportreportextrawwcontagemresultadoextraselection.AV19TFContagemResultado_ContratadaSigla_Sel = aP17_TFContagemResultado_ContratadaSigla_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV20TFContagemResultado_SistemaCoord = aP18_TFContagemResultado_SistemaCoord;
         objexportreportextrawwcontagemresultadoextraselection.AV21TFContagemResultado_SistemaCoord_Sel = aP19_TFContagemResultado_SistemaCoord_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV22TFContagemResultado_StatusDmn_SelsJson = aP20_TFContagemResultado_StatusDmn_SelsJson;
         objexportreportextrawwcontagemresultadoextraselection.AV23TFContagemResultado_Baseline_Sel = aP21_TFContagemResultado_Baseline_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV24TFContagemResultado_ServicoSigla = aP22_TFContagemResultado_ServicoSigla;
         objexportreportextrawwcontagemresultadoextraselection.AV25TFContagemResultado_ServicoSigla_Sel = aP23_TFContagemResultado_ServicoSigla_Sel;
         objexportreportextrawwcontagemresultadoextraselection.AV26TFContagemResultado_PFFinal = aP24_TFContagemResultado_PFFinal;
         objexportreportextrawwcontagemresultadoextraselection.AV27TFContagemResultado_PFFinal_To = aP25_TFContagemResultado_PFFinal_To;
         objexportreportextrawwcontagemresultadoextraselection.AV28TFContagemResultado_ValorPF = aP26_TFContagemResultado_ValorPF;
         objexportreportextrawwcontagemresultadoextraselection.AV29TFContagemResultado_ValorPF_To = aP27_TFContagemResultado_ValorPF_To;
         objexportreportextrawwcontagemresultadoextraselection.AV30OrderedBy = aP28_OrderedBy;
         objexportreportextrawwcontagemresultadoextraselection.AV31OrderedDsc = aP29_OrderedDsc;
         objexportreportextrawwcontagemresultadoextraselection.AV32GridStateXML = aP30_GridStateXML;
         objexportreportextrawwcontagemresultadoextraselection.context.SetSubmitInitialConfig(context);
         objexportreportextrawwcontagemresultadoextraselection.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportreportextrawwcontagemresultadoextraselection);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportreportextrawwcontagemresultadoextraselection)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_AreaTrabalhoCod,(short)AV3ContagemResultado_StatusCnt,(String)AV4TFContagemResultado_Agrupador,(String)AV5TFContagemResultado_Agrupador_Sel,(String)AV6TFContagemResultado_DemandaFM,(String)AV7TFContagemResultado_DemandaFM_Sel,(String)AV8TFContagemResultado_Demanda,(String)AV9TFContagemResultado_Demanda_Sel,(String)AV10TFContagemResultado_Descricao,(String)AV11TFContagemResultado_Descricao_Sel,(DateTime)AV12TFContagemResultado_DataPrevista,(DateTime)AV13TFContagemResultado_DataPrevista_To,(DateTime)AV14TFContagemResultado_DataDmn,(DateTime)AV15TFContagemResultado_DataDmn_To,(DateTime)AV16TFContagemResultado_DataUltCnt,(DateTime)AV17TFContagemResultado_DataUltCnt_To,(String)AV18TFContagemResultado_ContratadaSigla,(String)AV19TFContagemResultado_ContratadaSigla_Sel,(String)AV20TFContagemResultado_SistemaCoord,(String)AV21TFContagemResultado_SistemaCoord_Sel,(String)AV22TFContagemResultado_StatusDmn_SelsJson,(short)AV23TFContagemResultado_Baseline_Sel,(String)AV24TFContagemResultado_ServicoSigla,(String)AV25TFContagemResultado_ServicoSigla_Sel,(decimal)AV26TFContagemResultado_PFFinal,(decimal)AV27TFContagemResultado_PFFinal_To,(decimal)AV28TFContagemResultado_ValorPF,(decimal)AV29TFContagemResultado_ValorPF_To,(short)AV30OrderedBy,(bool)AV31OrderedDsc,(String)AV32GridStateXML} ;
         ClassLoader.Execute("aexportreportextrawwcontagemresultadoextraselection","GeneXus.Programs.aexportreportextrawwcontagemresultadoextraselection", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 31 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV3ContagemResultado_StatusCnt ;
      private short AV23TFContagemResultado_Baseline_Sel ;
      private short AV30OrderedBy ;
      private int AV2Contratada_AreaTrabalhoCod ;
      private decimal AV26TFContagemResultado_PFFinal ;
      private decimal AV27TFContagemResultado_PFFinal_To ;
      private decimal AV28TFContagemResultado_ValorPF ;
      private decimal AV29TFContagemResultado_ValorPF_To ;
      private String AV4TFContagemResultado_Agrupador ;
      private String AV5TFContagemResultado_Agrupador_Sel ;
      private String AV18TFContagemResultado_ContratadaSigla ;
      private String AV19TFContagemResultado_ContratadaSigla_Sel ;
      private String AV24TFContagemResultado_ServicoSigla ;
      private String AV25TFContagemResultado_ServicoSigla_Sel ;
      private DateTime AV12TFContagemResultado_DataPrevista ;
      private DateTime AV13TFContagemResultado_DataPrevista_To ;
      private DateTime AV14TFContagemResultado_DataDmn ;
      private DateTime AV15TFContagemResultado_DataDmn_To ;
      private DateTime AV16TFContagemResultado_DataUltCnt ;
      private DateTime AV17TFContagemResultado_DataUltCnt_To ;
      private bool AV31OrderedDsc ;
      private String AV22TFContagemResultado_StatusDmn_SelsJson ;
      private String AV32GridStateXML ;
      private String AV6TFContagemResultado_DemandaFM ;
      private String AV7TFContagemResultado_DemandaFM_Sel ;
      private String AV8TFContagemResultado_Demanda ;
      private String AV9TFContagemResultado_Demanda_Sel ;
      private String AV10TFContagemResultado_Descricao ;
      private String AV11TFContagemResultado_Descricao_Sel ;
      private String AV20TFContagemResultado_SistemaCoord ;
      private String AV21TFContagemResultado_SistemaCoord_Sel ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
