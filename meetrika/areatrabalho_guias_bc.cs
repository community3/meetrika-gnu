/*
               File: AreaTrabalho_Guias_BC
        Description: Area de Trabalho x Guias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:16.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class areatrabalho_guias_bc : GXHttpHandler, IGxSilentTrn
   {
      public areatrabalho_guias_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public areatrabalho_guias_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0J20( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0J20( ) ;
         standaloneModal( ) ;
         AddRow0J20( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
               Z93Guia_Codigo = A93Guia_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0J0( )
      {
         BeforeValidate0J20( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0J20( ) ;
            }
            else
            {
               CheckExtendedTable0J20( ) ;
               if ( AnyError == 0 )
               {
                  ZM0J20( 2) ;
                  ZM0J20( 3) ;
               }
               CloseExtendedTableCursors0J20( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM0J20( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z94Guia_Nome = A94Guia_Nome;
            Z95Guia_Versao = A95Guia_Versao;
         }
         if ( GX_JID == -1 )
         {
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z93Guia_Codigo = A93Guia_Codigo;
            Z6AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
            Z94Guia_Nome = A94Guia_Nome;
            Z95Guia_Versao = A95Guia_Versao;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load0J20( )
      {
         /* Using cursor BC000J6 */
         pr_default.execute(4, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound20 = 1;
            A6AreaTrabalho_Descricao = BC000J6_A6AreaTrabalho_Descricao[0];
            A94Guia_Nome = BC000J6_A94Guia_Nome[0];
            A95Guia_Versao = BC000J6_A95Guia_Versao[0];
            ZM0J20( -1) ;
         }
         pr_default.close(4);
         OnLoadActions0J20( ) ;
      }

      protected void OnLoadActions0J20( )
      {
      }

      protected void CheckExtendedTable0J20( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000J4 */
         pr_default.execute(2, new Object[] {A5AreaTrabalho_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
            AnyError = 1;
         }
         A6AreaTrabalho_Descricao = BC000J4_A6AreaTrabalho_Descricao[0];
         pr_default.close(2);
         /* Using cursor BC000J5 */
         pr_default.execute(3, new Object[] {A93Guia_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
            AnyError = 1;
         }
         A94Guia_Nome = BC000J5_A94Guia_Nome[0];
         A95Guia_Versao = BC000J5_A95Guia_Versao[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors0J20( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0J20( )
      {
         /* Using cursor BC000J7 */
         pr_default.execute(5, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound20 = 1;
         }
         else
         {
            RcdFound20 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000J3 */
         pr_default.execute(1, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0J20( 1) ;
            RcdFound20 = 1;
            A5AreaTrabalho_Codigo = BC000J3_A5AreaTrabalho_Codigo[0];
            A93Guia_Codigo = BC000J3_A93Guia_Codigo[0];
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z93Guia_Codigo = A93Guia_Codigo;
            sMode20 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0J20( ) ;
            if ( AnyError == 1 )
            {
               RcdFound20 = 0;
               InitializeNonKey0J20( ) ;
            }
            Gx_mode = sMode20;
         }
         else
         {
            RcdFound20 = 0;
            InitializeNonKey0J20( ) ;
            sMode20 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode20;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0J0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0J20( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000J2 */
            pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AreaTrabalho_Guias"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AreaTrabalho_Guias"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0J20( )
      {
         BeforeValidate0J20( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0J20( 0) ;
            CheckOptimisticConcurrency0J20( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0J20( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0J20( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000J8 */
                     pr_default.execute(6, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho_Guias") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0J20( ) ;
            }
            EndLevel0J20( ) ;
         }
         CloseExtendedTableCursors0J20( ) ;
      }

      protected void Update0J20( )
      {
         BeforeValidate0J20( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0J20( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0J20( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0J20( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [AreaTrabalho_Guias] */
                     DeferredUpdate0J20( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0J20( ) ;
         }
         CloseExtendedTableCursors0J20( ) ;
      }

      protected void DeferredUpdate0J20( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0J20( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0J20( ) ;
            AfterConfirm0J20( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0J20( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000J9 */
                  pr_default.execute(7, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("AreaTrabalho_Guias") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode20 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0J20( ) ;
         Gx_mode = sMode20;
      }

      protected void OnDeleteControls0J20( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000J10 */
            pr_default.execute(8, new Object[] {A5AreaTrabalho_Codigo});
            A6AreaTrabalho_Descricao = BC000J10_A6AreaTrabalho_Descricao[0];
            pr_default.close(8);
            /* Using cursor BC000J11 */
            pr_default.execute(9, new Object[] {A93Guia_Codigo});
            A94Guia_Nome = BC000J11_A94Guia_Nome[0];
            A95Guia_Versao = BC000J11_A95Guia_Versao[0];
            pr_default.close(9);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000J12 */
            pr_default.execute(10, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Guia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
         }
      }

      protected void EndLevel0J20( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0J20( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0J20( )
      {
         /* Using cursor BC000J13 */
         pr_default.execute(11, new Object[] {A5AreaTrabalho_Codigo, A93Guia_Codigo});
         RcdFound20 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound20 = 1;
            A6AreaTrabalho_Descricao = BC000J13_A6AreaTrabalho_Descricao[0];
            A94Guia_Nome = BC000J13_A94Guia_Nome[0];
            A95Guia_Versao = BC000J13_A95Guia_Versao[0];
            A5AreaTrabalho_Codigo = BC000J13_A5AreaTrabalho_Codigo[0];
            A93Guia_Codigo = BC000J13_A93Guia_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0J20( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound20 = 0;
         ScanKeyLoad0J20( ) ;
      }

      protected void ScanKeyLoad0J20( )
      {
         sMode20 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound20 = 1;
            A6AreaTrabalho_Descricao = BC000J13_A6AreaTrabalho_Descricao[0];
            A94Guia_Nome = BC000J13_A94Guia_Nome[0];
            A95Guia_Versao = BC000J13_A95Guia_Versao[0];
            A5AreaTrabalho_Codigo = BC000J13_A5AreaTrabalho_Codigo[0];
            A93Guia_Codigo = BC000J13_A93Guia_Codigo[0];
         }
         Gx_mode = sMode20;
      }

      protected void ScanKeyEnd0J20( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm0J20( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0J20( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0J20( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0J20( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0J20( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0J20( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0J20( )
      {
      }

      protected void AddRow0J20( )
      {
         VarsToRow20( bcAreaTrabalho_Guias) ;
      }

      protected void ReadRow0J20( )
      {
         RowToVars20( bcAreaTrabalho_Guias, 1) ;
      }

      protected void InitializeNonKey0J20( )
      {
         A6AreaTrabalho_Descricao = "";
         A94Guia_Nome = "";
         A95Guia_Versao = "";
      }

      protected void InitAll0J20( )
      {
         A5AreaTrabalho_Codigo = 0;
         A93Guia_Codigo = 0;
         InitializeNonKey0J20( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow20( SdtAreaTrabalho_Guias obj20 )
      {
         obj20.gxTpr_Mode = Gx_mode;
         obj20.gxTpr_Areatrabalho_descricao = A6AreaTrabalho_Descricao;
         obj20.gxTpr_Guia_nome = A94Guia_Nome;
         obj20.gxTpr_Guia_versao = A95Guia_Versao;
         obj20.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
         obj20.gxTpr_Guia_codigo = A93Guia_Codigo;
         obj20.gxTpr_Areatrabalho_codigo_Z = Z5AreaTrabalho_Codigo;
         obj20.gxTpr_Areatrabalho_descricao_Z = Z6AreaTrabalho_Descricao;
         obj20.gxTpr_Guia_codigo_Z = Z93Guia_Codigo;
         obj20.gxTpr_Guia_nome_Z = Z94Guia_Nome;
         obj20.gxTpr_Guia_versao_Z = Z95Guia_Versao;
         obj20.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow20( SdtAreaTrabalho_Guias obj20 )
      {
         obj20.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
         obj20.gxTpr_Guia_codigo = A93Guia_Codigo;
         return  ;
      }

      public void RowToVars20( SdtAreaTrabalho_Guias obj20 ,
                               int forceLoad )
      {
         Gx_mode = obj20.gxTpr_Mode;
         A6AreaTrabalho_Descricao = obj20.gxTpr_Areatrabalho_descricao;
         A94Guia_Nome = obj20.gxTpr_Guia_nome;
         A95Guia_Versao = obj20.gxTpr_Guia_versao;
         A5AreaTrabalho_Codigo = obj20.gxTpr_Areatrabalho_codigo;
         A93Guia_Codigo = obj20.gxTpr_Guia_codigo;
         Z5AreaTrabalho_Codigo = obj20.gxTpr_Areatrabalho_codigo_Z;
         Z6AreaTrabalho_Descricao = obj20.gxTpr_Areatrabalho_descricao_Z;
         Z93Guia_Codigo = obj20.gxTpr_Guia_codigo_Z;
         Z94Guia_Nome = obj20.gxTpr_Guia_nome_Z;
         Z95Guia_Versao = obj20.gxTpr_Guia_versao_Z;
         Gx_mode = obj20.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A5AreaTrabalho_Codigo = (int)getParm(obj,0);
         A93Guia_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0J20( ) ;
         ScanKeyStart0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000J10 */
            pr_default.execute(8, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
               AnyError = 1;
            }
            A6AreaTrabalho_Descricao = BC000J10_A6AreaTrabalho_Descricao[0];
            pr_default.close(8);
            /* Using cursor BC000J11 */
            pr_default.execute(9, new Object[] {A93Guia_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
               AnyError = 1;
            }
            A94Guia_Nome = BC000J11_A94Guia_Nome[0];
            A95Guia_Versao = BC000J11_A95Guia_Versao[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z93Guia_Codigo = A93Guia_Codigo;
         }
         ZM0J20( -1) ;
         OnLoadActions0J20( ) ;
         AddRow0J20( ) ;
         ScanKeyEnd0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars20( bcAreaTrabalho_Guias, 0) ;
         ScanKeyStart0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000J10 */
            pr_default.execute(8, new Object[] {A5AreaTrabalho_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "AREATRABALHO_CODIGO");
               AnyError = 1;
            }
            A6AreaTrabalho_Descricao = BC000J10_A6AreaTrabalho_Descricao[0];
            pr_default.close(8);
            /* Using cursor BC000J11 */
            pr_default.execute(9, new Object[] {A93Guia_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Guia'.", "ForeignKeyNotFound", 1, "GUIA_CODIGO");
               AnyError = 1;
            }
            A94Guia_Nome = BC000J11_A94Guia_Nome[0];
            A95Guia_Versao = BC000J11_A95Guia_Versao[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z5AreaTrabalho_Codigo = A5AreaTrabalho_Codigo;
            Z93Guia_Codigo = A93Guia_Codigo;
         }
         ZM0J20( -1) ;
         OnLoadActions0J20( ) ;
         AddRow0J20( ) ;
         ScanKeyEnd0J20( ) ;
         if ( RcdFound20 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars20( bcAreaTrabalho_Guias, 0) ;
         nKeyPressed = 1;
         GetKey0J20( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0J20( ) ;
         }
         else
         {
            if ( RcdFound20 == 1 )
            {
               if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A93Guia_Codigo != Z93Guia_Codigo ) )
               {
                  A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
                  A93Guia_Codigo = Z93Guia_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0J20( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A93Guia_Codigo != Z93Guia_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0J20( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0J20( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow20( bcAreaTrabalho_Guias) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars20( bcAreaTrabalho_Guias, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0J20( ) ;
         if ( RcdFound20 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A93Guia_Codigo != Z93Guia_Codigo ) )
            {
               A5AreaTrabalho_Codigo = Z5AreaTrabalho_Codigo;
               A93Guia_Codigo = Z93Guia_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A5AreaTrabalho_Codigo != Z5AreaTrabalho_Codigo ) || ( A93Guia_Codigo != Z93Guia_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
         context.RollbackDataStores( "AreaTrabalho_Guias_BC");
         VarsToRow20( bcAreaTrabalho_Guias) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcAreaTrabalho_Guias.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcAreaTrabalho_Guias.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcAreaTrabalho_Guias )
         {
            bcAreaTrabalho_Guias = (SdtAreaTrabalho_Guias)(sdt);
            if ( StringUtil.StrCmp(bcAreaTrabalho_Guias.gxTpr_Mode, "") == 0 )
            {
               bcAreaTrabalho_Guias.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow20( bcAreaTrabalho_Guias) ;
            }
            else
            {
               RowToVars20( bcAreaTrabalho_Guias, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcAreaTrabalho_Guias.gxTpr_Mode, "") == 0 )
            {
               bcAreaTrabalho_Guias.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars20( bcAreaTrabalho_Guias, 1) ;
         return  ;
      }

      public SdtAreaTrabalho_Guias AreaTrabalho_Guias_BC
      {
         get {
            return bcAreaTrabalho_Guias ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z6AreaTrabalho_Descricao = "";
         A6AreaTrabalho_Descricao = "";
         Z94Guia_Nome = "";
         A94Guia_Nome = "";
         Z95Guia_Versao = "";
         A95Guia_Versao = "";
         BC000J6_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC000J6_A94Guia_Nome = new String[] {""} ;
         BC000J6_A95Guia_Versao = new String[] {""} ;
         BC000J6_A5AreaTrabalho_Codigo = new int[1] ;
         BC000J6_A93Guia_Codigo = new int[1] ;
         BC000J4_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC000J5_A94Guia_Nome = new String[] {""} ;
         BC000J5_A95Guia_Versao = new String[] {""} ;
         BC000J7_A5AreaTrabalho_Codigo = new int[1] ;
         BC000J7_A93Guia_Codigo = new int[1] ;
         BC000J3_A5AreaTrabalho_Codigo = new int[1] ;
         BC000J3_A93Guia_Codigo = new int[1] ;
         sMode20 = "";
         BC000J2_A5AreaTrabalho_Codigo = new int[1] ;
         BC000J2_A93Guia_Codigo = new int[1] ;
         BC000J10_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC000J11_A94Guia_Nome = new String[] {""} ;
         BC000J11_A95Guia_Versao = new String[] {""} ;
         BC000J12_A93Guia_Codigo = new int[1] ;
         BC000J13_A6AreaTrabalho_Descricao = new String[] {""} ;
         BC000J13_A94Guia_Nome = new String[] {""} ;
         BC000J13_A95Guia_Versao = new String[] {""} ;
         BC000J13_A5AreaTrabalho_Codigo = new int[1] ;
         BC000J13_A93Guia_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.areatrabalho_guias_bc__default(),
            new Object[][] {
                new Object[] {
               BC000J2_A5AreaTrabalho_Codigo, BC000J2_A93Guia_Codigo
               }
               , new Object[] {
               BC000J3_A5AreaTrabalho_Codigo, BC000J3_A93Guia_Codigo
               }
               , new Object[] {
               BC000J4_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               BC000J5_A94Guia_Nome, BC000J5_A95Guia_Versao
               }
               , new Object[] {
               BC000J6_A6AreaTrabalho_Descricao, BC000J6_A94Guia_Nome, BC000J6_A95Guia_Versao, BC000J6_A5AreaTrabalho_Codigo, BC000J6_A93Guia_Codigo
               }
               , new Object[] {
               BC000J7_A5AreaTrabalho_Codigo, BC000J7_A93Guia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000J10_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               BC000J11_A94Guia_Nome, BC000J11_A95Guia_Versao
               }
               , new Object[] {
               BC000J12_A93Guia_Codigo
               }
               , new Object[] {
               BC000J13_A6AreaTrabalho_Descricao, BC000J13_A94Guia_Nome, BC000J13_A95Guia_Versao, BC000J13_A5AreaTrabalho_Codigo, BC000J13_A93Guia_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound20 ;
      private int trnEnded ;
      private int Z5AreaTrabalho_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int Z93Guia_Codigo ;
      private int A93Guia_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z94Guia_Nome ;
      private String A94Guia_Nome ;
      private String Z95Guia_Versao ;
      private String A95Guia_Versao ;
      private String sMode20 ;
      private String Z6AreaTrabalho_Descricao ;
      private String A6AreaTrabalho_Descricao ;
      private SdtAreaTrabalho_Guias bcAreaTrabalho_Guias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC000J6_A6AreaTrabalho_Descricao ;
      private String[] BC000J6_A94Guia_Nome ;
      private String[] BC000J6_A95Guia_Versao ;
      private int[] BC000J6_A5AreaTrabalho_Codigo ;
      private int[] BC000J6_A93Guia_Codigo ;
      private String[] BC000J4_A6AreaTrabalho_Descricao ;
      private String[] BC000J5_A94Guia_Nome ;
      private String[] BC000J5_A95Guia_Versao ;
      private int[] BC000J7_A5AreaTrabalho_Codigo ;
      private int[] BC000J7_A93Guia_Codigo ;
      private int[] BC000J3_A5AreaTrabalho_Codigo ;
      private int[] BC000J3_A93Guia_Codigo ;
      private int[] BC000J2_A5AreaTrabalho_Codigo ;
      private int[] BC000J2_A93Guia_Codigo ;
      private String[] BC000J10_A6AreaTrabalho_Descricao ;
      private String[] BC000J11_A94Guia_Nome ;
      private String[] BC000J11_A95Guia_Versao ;
      private int[] BC000J12_A93Guia_Codigo ;
      private String[] BC000J13_A6AreaTrabalho_Descricao ;
      private String[] BC000J13_A94Guia_Nome ;
      private String[] BC000J13_A95Guia_Versao ;
      private int[] BC000J13_A5AreaTrabalho_Codigo ;
      private int[] BC000J13_A93Guia_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class areatrabalho_guias_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000J6 ;
          prmBC000J6 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J4 ;
          prmBC000J4 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J5 ;
          prmBC000J5 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J7 ;
          prmBC000J7 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J3 ;
          prmBC000J3 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J2 ;
          prmBC000J2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J8 ;
          prmBC000J8 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J9 ;
          prmBC000J9 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J12 ;
          prmBC000J12 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J13 ;
          prmBC000J13 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J10 ;
          prmBC000J10 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000J11 ;
          prmBC000J11 = new Object[] {
          new Object[] {"@Guia_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000J2", "SELECT [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (UPDLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J2,1,0,true,false )
             ,new CursorDef("BC000J3", "SELECT [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J3,1,0,true,false )
             ,new CursorDef("BC000J4", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J4,1,0,true,false )
             ,new CursorDef("BC000J5", "SELECT [Guia_Nome], [Guia_Versao] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J5,1,0,true,false )
             ,new CursorDef("BC000J6", "SELECT T2.[AreaTrabalho_Descricao], T3.[Guia_Nome], T3.[Guia_Versao], TM1.[AreaTrabalho_Codigo], TM1.[Guia_Codigo] FROM (([AreaTrabalho_Guias] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[AreaTrabalho_Codigo]) INNER JOIN [Guia] T3 WITH (NOLOCK) ON T3.[Guia_Codigo] = TM1.[Guia_Codigo]) WHERE TM1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and TM1.[Guia_Codigo] = @Guia_Codigo ORDER BY TM1.[AreaTrabalho_Codigo], TM1.[Guia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J6,100,0,true,false )
             ,new CursorDef("BC000J7", "SELECT [AreaTrabalho_Codigo], [Guia_Codigo] FROM [AreaTrabalho_Guias] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J7,1,0,true,false )
             ,new CursorDef("BC000J8", "INSERT INTO [AreaTrabalho_Guias]([AreaTrabalho_Codigo], [Guia_Codigo]) VALUES(@AreaTrabalho_Codigo, @Guia_Codigo)", GxErrorMask.GX_NOMASK,prmBC000J8)
             ,new CursorDef("BC000J9", "DELETE FROM [AreaTrabalho_Guias]  WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo", GxErrorMask.GX_NOMASK,prmBC000J9)
             ,new CursorDef("BC000J10", "SELECT [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J10,1,0,true,false )
             ,new CursorDef("BC000J11", "SELECT [Guia_Nome], [Guia_Versao] FROM [Guia] WITH (NOLOCK) WHERE [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J11,1,0,true,false )
             ,new CursorDef("BC000J12", "SELECT TOP 1 [Guia_Codigo] FROM [Guia] WITH (NOLOCK) WHERE [Guia_AreaTrabalhoCod] = @AreaTrabalho_Codigo AND [Guia_Codigo] = @Guia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J12,1,0,true,true )
             ,new CursorDef("BC000J13", "SELECT T2.[AreaTrabalho_Descricao], T3.[Guia_Nome], T3.[Guia_Versao], TM1.[AreaTrabalho_Codigo], TM1.[Guia_Codigo] FROM (([AreaTrabalho_Guias] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[AreaTrabalho_Codigo]) INNER JOIN [Guia] T3 WITH (NOLOCK) ON T3.[Guia_Codigo] = TM1.[Guia_Codigo]) WHERE TM1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo and TM1.[Guia_Codigo] = @Guia_Codigo ORDER BY TM1.[AreaTrabalho_Codigo], TM1.[Guia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000J13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
