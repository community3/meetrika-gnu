/*
               File: HelpSistema_BC
        Description: Cadastro de Help do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:12.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class helpsistema_bc : GXHttpHandler, IGxSilentTrn
   {
      public helpsistema_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public helpsistema_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow42181( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey42181( ) ;
         standaloneModal( ) ;
         AddRow42181( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1638HelpSistema_Codigo = A1638HelpSistema_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_420( )
      {
         BeforeValidate42181( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls42181( ) ;
            }
            else
            {
               CheckExtendedTable42181( ) ;
               if ( AnyError == 0 )
               {
               }
               CloseExtendedTableCursors42181( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM42181( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1639HelpSistema_Objeto = A1639HelpSistema_Objeto;
            Z1641HelpSistema_Video = A1641HelpSistema_Video;
         }
         if ( GX_JID == -1 )
         {
            Z1638HelpSistema_Codigo = A1638HelpSistema_Codigo;
            Z1639HelpSistema_Objeto = A1639HelpSistema_Objeto;
            Z1640HelpSistema_Descricao = A1640HelpSistema_Descricao;
            Z1641HelpSistema_Video = A1641HelpSistema_Video;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load42181( )
      {
         /* Using cursor BC00424 */
         pr_default.execute(2, new Object[] {A1638HelpSistema_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound181 = 1;
            A1639HelpSistema_Objeto = BC00424_A1639HelpSistema_Objeto[0];
            A1640HelpSistema_Descricao = BC00424_A1640HelpSistema_Descricao[0];
            A1641HelpSistema_Video = BC00424_A1641HelpSistema_Video[0];
            ZM42181( -1) ;
         }
         pr_default.close(2);
         OnLoadActions42181( ) ;
      }

      protected void OnLoadActions42181( )
      {
      }

      protected void CheckExtendedTable42181( )
      {
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors42181( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey42181( )
      {
         /* Using cursor BC00425 */
         pr_default.execute(3, new Object[] {A1638HelpSistema_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound181 = 1;
         }
         else
         {
            RcdFound181 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00423 */
         pr_default.execute(1, new Object[] {A1638HelpSistema_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM42181( 1) ;
            RcdFound181 = 1;
            A1638HelpSistema_Codigo = BC00423_A1638HelpSistema_Codigo[0];
            A1639HelpSistema_Objeto = BC00423_A1639HelpSistema_Objeto[0];
            A1640HelpSistema_Descricao = BC00423_A1640HelpSistema_Descricao[0];
            A1641HelpSistema_Video = BC00423_A1641HelpSistema_Video[0];
            Z1638HelpSistema_Codigo = A1638HelpSistema_Codigo;
            sMode181 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load42181( ) ;
            if ( AnyError == 1 )
            {
               RcdFound181 = 0;
               InitializeNonKey42181( ) ;
            }
            Gx_mode = sMode181;
         }
         else
         {
            RcdFound181 = 0;
            InitializeNonKey42181( ) ;
            sMode181 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode181;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey42181( ) ;
         if ( RcdFound181 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_420( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency42181( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00422 */
            pr_default.execute(0, new Object[] {A1638HelpSistema_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"HelpSistema"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1639HelpSistema_Objeto, BC00422_A1639HelpSistema_Objeto[0]) != 0 ) || ( StringUtil.StrCmp(Z1641HelpSistema_Video, BC00422_A1641HelpSistema_Video[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"HelpSistema"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert42181( )
      {
         BeforeValidate42181( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable42181( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM42181( 0) ;
            CheckOptimisticConcurrency42181( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm42181( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert42181( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00426 */
                     pr_default.execute(4, new Object[] {A1639HelpSistema_Objeto, A1640HelpSistema_Descricao, A1641HelpSistema_Video});
                     A1638HelpSistema_Codigo = BC00426_A1638HelpSistema_Codigo[0];
                     pr_default.close(4);
                     dsDefault.SmartCacheProvider.SetUpdated("HelpSistema") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load42181( ) ;
            }
            EndLevel42181( ) ;
         }
         CloseExtendedTableCursors42181( ) ;
      }

      protected void Update42181( )
      {
         BeforeValidate42181( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable42181( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency42181( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm42181( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate42181( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00427 */
                     pr_default.execute(5, new Object[] {A1639HelpSistema_Objeto, A1640HelpSistema_Descricao, A1641HelpSistema_Video, A1638HelpSistema_Codigo});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("HelpSistema") ;
                     if ( (pr_default.getStatus(5) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"HelpSistema"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate42181( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel42181( ) ;
         }
         CloseExtendedTableCursors42181( ) ;
      }

      protected void DeferredUpdate42181( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate42181( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency42181( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls42181( ) ;
            AfterConfirm42181( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete42181( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00428 */
                  pr_default.execute(6, new Object[] {A1638HelpSistema_Codigo});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("HelpSistema") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode181 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel42181( ) ;
         Gx_mode = sMode181;
      }

      protected void OnDeleteControls42181( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel42181( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete42181( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart42181( )
      {
         /* Using cursor BC00429 */
         pr_default.execute(7, new Object[] {A1638HelpSistema_Codigo});
         RcdFound181 = 0;
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound181 = 1;
            A1638HelpSistema_Codigo = BC00429_A1638HelpSistema_Codigo[0];
            A1639HelpSistema_Objeto = BC00429_A1639HelpSistema_Objeto[0];
            A1640HelpSistema_Descricao = BC00429_A1640HelpSistema_Descricao[0];
            A1641HelpSistema_Video = BC00429_A1641HelpSistema_Video[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext42181( )
      {
         /* Scan next routine */
         pr_default.readNext(7);
         RcdFound181 = 0;
         ScanKeyLoad42181( ) ;
      }

      protected void ScanKeyLoad42181( )
      {
         sMode181 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound181 = 1;
            A1638HelpSistema_Codigo = BC00429_A1638HelpSistema_Codigo[0];
            A1639HelpSistema_Objeto = BC00429_A1639HelpSistema_Objeto[0];
            A1640HelpSistema_Descricao = BC00429_A1640HelpSistema_Descricao[0];
            A1641HelpSistema_Video = BC00429_A1641HelpSistema_Video[0];
         }
         Gx_mode = sMode181;
      }

      protected void ScanKeyEnd42181( )
      {
         pr_default.close(7);
      }

      protected void AfterConfirm42181( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert42181( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate42181( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete42181( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete42181( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate42181( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes42181( )
      {
      }

      protected void AddRow42181( )
      {
         VarsToRow181( bcHelpSistema) ;
      }

      protected void ReadRow42181( )
      {
         RowToVars181( bcHelpSistema, 1) ;
      }

      protected void InitializeNonKey42181( )
      {
         A1639HelpSistema_Objeto = "";
         A1640HelpSistema_Descricao = "";
         A1641HelpSistema_Video = "";
         Z1639HelpSistema_Objeto = "";
         Z1641HelpSistema_Video = "";
      }

      protected void InitAll42181( )
      {
         A1638HelpSistema_Codigo = 0;
         InitializeNonKey42181( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow181( SdtHelpSistema obj181 )
      {
         obj181.gxTpr_Mode = Gx_mode;
         obj181.gxTpr_Helpsistema_objeto = A1639HelpSistema_Objeto;
         obj181.gxTpr_Helpsistema_descricao = A1640HelpSistema_Descricao;
         obj181.gxTpr_Helpsistema_video = A1641HelpSistema_Video;
         obj181.gxTpr_Helpsistema_codigo = A1638HelpSistema_Codigo;
         obj181.gxTpr_Helpsistema_codigo_Z = Z1638HelpSistema_Codigo;
         obj181.gxTpr_Helpsistema_objeto_Z = Z1639HelpSistema_Objeto;
         obj181.gxTpr_Helpsistema_video_Z = Z1641HelpSistema_Video;
         obj181.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow181( SdtHelpSistema obj181 )
      {
         obj181.gxTpr_Helpsistema_codigo = A1638HelpSistema_Codigo;
         return  ;
      }

      public void RowToVars181( SdtHelpSistema obj181 ,
                                int forceLoad )
      {
         Gx_mode = obj181.gxTpr_Mode;
         A1639HelpSistema_Objeto = obj181.gxTpr_Helpsistema_objeto;
         A1640HelpSistema_Descricao = obj181.gxTpr_Helpsistema_descricao;
         A1641HelpSistema_Video = obj181.gxTpr_Helpsistema_video;
         A1638HelpSistema_Codigo = obj181.gxTpr_Helpsistema_codigo;
         Z1638HelpSistema_Codigo = obj181.gxTpr_Helpsistema_codigo_Z;
         Z1639HelpSistema_Objeto = obj181.gxTpr_Helpsistema_objeto_Z;
         Z1641HelpSistema_Video = obj181.gxTpr_Helpsistema_video_Z;
         Gx_mode = obj181.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1638HelpSistema_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey42181( ) ;
         ScanKeyStart42181( ) ;
         if ( RcdFound181 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1638HelpSistema_Codigo = A1638HelpSistema_Codigo;
         }
         ZM42181( -1) ;
         OnLoadActions42181( ) ;
         AddRow42181( ) ;
         ScanKeyEnd42181( ) ;
         if ( RcdFound181 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars181( bcHelpSistema, 0) ;
         ScanKeyStart42181( ) ;
         if ( RcdFound181 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1638HelpSistema_Codigo = A1638HelpSistema_Codigo;
         }
         ZM42181( -1) ;
         OnLoadActions42181( ) ;
         AddRow42181( ) ;
         ScanKeyEnd42181( ) ;
         if ( RcdFound181 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars181( bcHelpSistema, 0) ;
         nKeyPressed = 1;
         GetKey42181( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert42181( ) ;
         }
         else
         {
            if ( RcdFound181 == 1 )
            {
               if ( A1638HelpSistema_Codigo != Z1638HelpSistema_Codigo )
               {
                  A1638HelpSistema_Codigo = Z1638HelpSistema_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update42181( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1638HelpSistema_Codigo != Z1638HelpSistema_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert42181( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert42181( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow181( bcHelpSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars181( bcHelpSistema, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey42181( ) ;
         if ( RcdFound181 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1638HelpSistema_Codigo != Z1638HelpSistema_Codigo )
            {
               A1638HelpSistema_Codigo = Z1638HelpSistema_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1638HelpSistema_Codigo != Z1638HelpSistema_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "HelpSistema_BC");
         VarsToRow181( bcHelpSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcHelpSistema.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcHelpSistema.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcHelpSistema )
         {
            bcHelpSistema = (SdtHelpSistema)(sdt);
            if ( StringUtil.StrCmp(bcHelpSistema.gxTpr_Mode, "") == 0 )
            {
               bcHelpSistema.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow181( bcHelpSistema) ;
            }
            else
            {
               RowToVars181( bcHelpSistema, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcHelpSistema.gxTpr_Mode, "") == 0 )
            {
               bcHelpSistema.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars181( bcHelpSistema, 1) ;
         return  ;
      }

      public SdtHelpSistema HelpSistema_BC
      {
         get {
            return bcHelpSistema ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1639HelpSistema_Objeto = "";
         A1639HelpSistema_Objeto = "";
         Z1641HelpSistema_Video = "";
         A1641HelpSistema_Video = "";
         Z1640HelpSistema_Descricao = "";
         A1640HelpSistema_Descricao = "";
         BC00424_A1638HelpSistema_Codigo = new int[1] ;
         BC00424_A1639HelpSistema_Objeto = new String[] {""} ;
         BC00424_A1640HelpSistema_Descricao = new String[] {""} ;
         BC00424_A1641HelpSistema_Video = new String[] {""} ;
         BC00425_A1638HelpSistema_Codigo = new int[1] ;
         BC00423_A1638HelpSistema_Codigo = new int[1] ;
         BC00423_A1639HelpSistema_Objeto = new String[] {""} ;
         BC00423_A1640HelpSistema_Descricao = new String[] {""} ;
         BC00423_A1641HelpSistema_Video = new String[] {""} ;
         sMode181 = "";
         BC00422_A1638HelpSistema_Codigo = new int[1] ;
         BC00422_A1639HelpSistema_Objeto = new String[] {""} ;
         BC00422_A1640HelpSistema_Descricao = new String[] {""} ;
         BC00422_A1641HelpSistema_Video = new String[] {""} ;
         BC00426_A1638HelpSistema_Codigo = new int[1] ;
         BC00429_A1638HelpSistema_Codigo = new int[1] ;
         BC00429_A1639HelpSistema_Objeto = new String[] {""} ;
         BC00429_A1640HelpSistema_Descricao = new String[] {""} ;
         BC00429_A1641HelpSistema_Video = new String[] {""} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.helpsistema_bc__default(),
            new Object[][] {
                new Object[] {
               BC00422_A1638HelpSistema_Codigo, BC00422_A1639HelpSistema_Objeto, BC00422_A1640HelpSistema_Descricao, BC00422_A1641HelpSistema_Video
               }
               , new Object[] {
               BC00423_A1638HelpSistema_Codigo, BC00423_A1639HelpSistema_Objeto, BC00423_A1640HelpSistema_Descricao, BC00423_A1641HelpSistema_Video
               }
               , new Object[] {
               BC00424_A1638HelpSistema_Codigo, BC00424_A1639HelpSistema_Objeto, BC00424_A1640HelpSistema_Descricao, BC00424_A1641HelpSistema_Video
               }
               , new Object[] {
               BC00425_A1638HelpSistema_Codigo
               }
               , new Object[] {
               BC00426_A1638HelpSistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC00429_A1638HelpSistema_Codigo, BC00429_A1639HelpSistema_Objeto, BC00429_A1640HelpSistema_Descricao, BC00429_A1641HelpSistema_Video
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound181 ;
      private int trnEnded ;
      private int Z1638HelpSistema_Codigo ;
      private int A1638HelpSistema_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode181 ;
      private String Z1640HelpSistema_Descricao ;
      private String A1640HelpSistema_Descricao ;
      private String Z1639HelpSistema_Objeto ;
      private String A1639HelpSistema_Objeto ;
      private String Z1641HelpSistema_Video ;
      private String A1641HelpSistema_Video ;
      private SdtHelpSistema bcHelpSistema ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00424_A1638HelpSistema_Codigo ;
      private String[] BC00424_A1639HelpSistema_Objeto ;
      private String[] BC00424_A1640HelpSistema_Descricao ;
      private String[] BC00424_A1641HelpSistema_Video ;
      private int[] BC00425_A1638HelpSistema_Codigo ;
      private int[] BC00423_A1638HelpSistema_Codigo ;
      private String[] BC00423_A1639HelpSistema_Objeto ;
      private String[] BC00423_A1640HelpSistema_Descricao ;
      private String[] BC00423_A1641HelpSistema_Video ;
      private int[] BC00422_A1638HelpSistema_Codigo ;
      private String[] BC00422_A1639HelpSistema_Objeto ;
      private String[] BC00422_A1640HelpSistema_Descricao ;
      private String[] BC00422_A1641HelpSistema_Video ;
      private int[] BC00426_A1638HelpSistema_Codigo ;
      private int[] BC00429_A1638HelpSistema_Codigo ;
      private String[] BC00429_A1639HelpSistema_Objeto ;
      private String[] BC00429_A1640HelpSistema_Descricao ;
      private String[] BC00429_A1641HelpSistema_Video ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class helpsistema_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00424 ;
          prmBC00424 = new Object[] {
          new Object[] {"@HelpSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00425 ;
          prmBC00425 = new Object[] {
          new Object[] {"@HelpSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00423 ;
          prmBC00423 = new Object[] {
          new Object[] {"@HelpSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00422 ;
          prmBC00422 = new Object[] {
          new Object[] {"@HelpSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00426 ;
          prmBC00426 = new Object[] {
          new Object[] {"@HelpSistema_Objeto",SqlDbType.VarChar,80,0} ,
          new Object[] {"@HelpSistema_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@HelpSistema_Video",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmBC00427 ;
          prmBC00427 = new Object[] {
          new Object[] {"@HelpSistema_Objeto",SqlDbType.VarChar,80,0} ,
          new Object[] {"@HelpSistema_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@HelpSistema_Video",SqlDbType.VarChar,200,0} ,
          new Object[] {"@HelpSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00428 ;
          prmBC00428 = new Object[] {
          new Object[] {"@HelpSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00429 ;
          prmBC00429 = new Object[] {
          new Object[] {"@HelpSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00422", "SELECT [HelpSistema_Codigo], [HelpSistema_Objeto], [HelpSistema_Descricao], [HelpSistema_Video] FROM [HelpSistema] WITH (UPDLOCK) WHERE [HelpSistema_Codigo] = @HelpSistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00422,1,0,true,false )
             ,new CursorDef("BC00423", "SELECT [HelpSistema_Codigo], [HelpSistema_Objeto], [HelpSistema_Descricao], [HelpSistema_Video] FROM [HelpSistema] WITH (NOLOCK) WHERE [HelpSistema_Codigo] = @HelpSistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00423,1,0,true,false )
             ,new CursorDef("BC00424", "SELECT TM1.[HelpSistema_Codigo], TM1.[HelpSistema_Objeto], TM1.[HelpSistema_Descricao], TM1.[HelpSistema_Video] FROM [HelpSistema] TM1 WITH (NOLOCK) WHERE TM1.[HelpSistema_Codigo] = @HelpSistema_Codigo ORDER BY TM1.[HelpSistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00424,100,0,true,false )
             ,new CursorDef("BC00425", "SELECT [HelpSistema_Codigo] FROM [HelpSistema] WITH (NOLOCK) WHERE [HelpSistema_Codigo] = @HelpSistema_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00425,1,0,true,false )
             ,new CursorDef("BC00426", "INSERT INTO [HelpSistema]([HelpSistema_Objeto], [HelpSistema_Descricao], [HelpSistema_Video]) VALUES(@HelpSistema_Objeto, @HelpSistema_Descricao, @HelpSistema_Video); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC00426)
             ,new CursorDef("BC00427", "UPDATE [HelpSistema] SET [HelpSistema_Objeto]=@HelpSistema_Objeto, [HelpSistema_Descricao]=@HelpSistema_Descricao, [HelpSistema_Video]=@HelpSistema_Video  WHERE [HelpSistema_Codigo] = @HelpSistema_Codigo", GxErrorMask.GX_NOMASK,prmBC00427)
             ,new CursorDef("BC00428", "DELETE FROM [HelpSistema]  WHERE [HelpSistema_Codigo] = @HelpSistema_Codigo", GxErrorMask.GX_NOMASK,prmBC00428)
             ,new CursorDef("BC00429", "SELECT TM1.[HelpSistema_Codigo], TM1.[HelpSistema_Objeto], TM1.[HelpSistema_Descricao], TM1.[HelpSistema_Video] FROM [HelpSistema] TM1 WITH (NOLOCK) WHERE TM1.[HelpSistema_Codigo] = @HelpSistema_Codigo ORDER BY TM1.[HelpSistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00429,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
