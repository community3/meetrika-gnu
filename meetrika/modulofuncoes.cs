/*
               File: ModuloFuncoes
        Description: Modulo Funcoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:53.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class modulofuncoes : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"MODULO_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAMODULO_CODIGO1441( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A146Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A146Modulo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A161FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A161FuncaoUsuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynModulo_Codigo.Name = "MODULO_CODIGO";
         dynModulo_Codigo.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Modulo Funcoes", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynModulo_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public modulofuncoes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public modulofuncoes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynModulo_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynModulo_Codigo.ItemCount > 0 )
         {
            A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0))), "."));
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1441( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1441e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1441( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1441( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1441e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Modulo Funcoes", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ModuloFuncoes.htm");
            wb_table3_28_1441( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1441e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1441e( true) ;
         }
         else
         {
            wb_table1_2_1441e( false) ;
         }
      }

      protected void wb_table3_28_1441( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1441( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1441e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ModuloFuncoes.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ModuloFuncoes.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1441e( true) ;
         }
         else
         {
            wb_table3_28_1441e( false) ;
         }
      }

      protected void wb_table4_34_1441( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_codigo_Internalname, "M�dulo", "", "", lblTextblockmodulo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynModulo_Codigo, dynModulo_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)), 1, dynModulo_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynModulo_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_ModuloFuncoes.htm");
            dynModulo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynModulo_Codigo_Internalname, "Values", (String)(dynModulo_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_descricao_Internalname, "Descri��o", "", "", lblTextblockmodulo_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtModulo_Descricao_Internalname, A144Modulo_Descricao, "", "", 0, 1, edtModulo_Descricao_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaousuario_codigo_Internalname, "Fun��o de Usu�rio", "", "", lblTextblockfuncaousuario_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")), ((edtFuncaoUsuario_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoUsuario_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoUsuario_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaousuario_nome_Internalname, "Fun��o de Usu�rio", "", "", lblTextblockfuncaousuario_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoUsuario_Nome_Internalname, A162FuncaoUsuario_Nome, StringUtil.RTrim( context.localUtil.Format( A162FuncaoUsuario_Nome, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoUsuario_Nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoUsuario_Nome_Enabled, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ModuloFuncoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1441e( true) ;
         }
         else
         {
            wb_table4_34_1441e( false) ;
         }
      }

      protected void wb_table2_5_1441( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ModuloFuncoes.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1441e( true) ;
         }
         else
         {
            wb_table2_5_1441e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynModulo_Codigo.CurrentValue = cgiGet( dynModulo_Codigo_Internalname);
               A146Modulo_Codigo = (int)(NumberUtil.Val( cgiGet( dynModulo_Codigo_Internalname), "."));
               n146Modulo_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               A144Modulo_Descricao = cgiGet( edtModulo_Descricao_Internalname);
               n144Modulo_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOUSUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A161FuncaoUsuario_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               }
               else
               {
                  A161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               }
               A162FuncaoUsuario_Nome = cgiGet( edtFuncaoUsuario_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
               /* Read saved values. */
               Z146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z146Modulo_Codigo"), ",", "."));
               Z161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z161FuncaoUsuario_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A146Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n146Modulo_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
                  A161FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1441( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1441( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption140( )
      {
      }

      protected void ZM1441( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -2 )
         {
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            Z144Modulo_Descricao = A144Modulo_Descricao;
            Z162FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAMODULO_CODIGO_html1441( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00144 */
            pr_default.execute(2, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            A144Modulo_Descricao = T00144_A144Modulo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
            n144Modulo_Descricao = T00144_n144Modulo_Descricao[0];
            pr_default.close(2);
         }
      }

      protected void Load1441( )
      {
         /* Using cursor T00146 */
         pr_default.execute(4, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound41 = 1;
            A144Modulo_Descricao = T00146_A144Modulo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
            n144Modulo_Descricao = T00146_n144Modulo_Descricao[0];
            A162FuncaoUsuario_Nome = T00146_A162FuncaoUsuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
            ZM1441( -2) ;
         }
         pr_default.close(4);
         OnLoadActions1441( ) ;
      }

      protected void OnLoadActions1441( )
      {
      }

      protected void CheckExtendedTable1441( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00144 */
         pr_default.execute(2, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A144Modulo_Descricao = T00144_A144Modulo_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
         n144Modulo_Descricao = T00144_n144Modulo_Descricao[0];
         pr_default.close(2);
         /* Using cursor T00145 */
         pr_default.execute(3, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A162FuncaoUsuario_Nome = T00145_A162FuncaoUsuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1441( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A146Modulo_Codigo )
      {
         /* Using cursor T00147 */
         pr_default.execute(5, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A144Modulo_Descricao = T00147_A144Modulo_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
         n144Modulo_Descricao = T00147_n144Modulo_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A144Modulo_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_4( int A161FuncaoUsuario_Codigo )
      {
         /* Using cursor T00148 */
         pr_default.execute(6, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A162FuncaoUsuario_Nome = T00148_A162FuncaoUsuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A162FuncaoUsuario_Nome)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey1441( )
      {
         /* Using cursor T00149 */
         pr_default.execute(7, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound41 = 1;
         }
         else
         {
            RcdFound41 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00143 */
         pr_default.execute(1, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1441( 2) ;
            RcdFound41 = 1;
            A146Modulo_Codigo = T00143_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T00143_n146Modulo_Codigo[0];
            A161FuncaoUsuario_Codigo = T00143_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            sMode41 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1441( ) ;
            if ( AnyError == 1 )
            {
               RcdFound41 = 0;
               InitializeNonKey1441( ) ;
            }
            Gx_mode = sMode41;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound41 = 0;
            InitializeNonKey1441( ) ;
            sMode41 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode41;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1441( ) ;
         if ( RcdFound41 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound41 = 0;
         /* Using cursor T001410 */
         pr_default.execute(8, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001410_A146Modulo_Codigo[0] < A146Modulo_Codigo ) || ( T001410_A146Modulo_Codigo[0] == A146Modulo_Codigo ) && ( T001410_A161FuncaoUsuario_Codigo[0] < A161FuncaoUsuario_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001410_A146Modulo_Codigo[0] > A146Modulo_Codigo ) || ( T001410_A146Modulo_Codigo[0] == A146Modulo_Codigo ) && ( T001410_A161FuncaoUsuario_Codigo[0] > A161FuncaoUsuario_Codigo ) ) )
            {
               A146Modulo_Codigo = T001410_A146Modulo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               n146Modulo_Codigo = T001410_n146Modulo_Codigo[0];
               A161FuncaoUsuario_Codigo = T001410_A161FuncaoUsuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               RcdFound41 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound41 = 0;
         /* Using cursor T001411 */
         pr_default.execute(9, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001411_A146Modulo_Codigo[0] > A146Modulo_Codigo ) || ( T001411_A146Modulo_Codigo[0] == A146Modulo_Codigo ) && ( T001411_A161FuncaoUsuario_Codigo[0] > A161FuncaoUsuario_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001411_A146Modulo_Codigo[0] < A146Modulo_Codigo ) || ( T001411_A146Modulo_Codigo[0] == A146Modulo_Codigo ) && ( T001411_A161FuncaoUsuario_Codigo[0] < A161FuncaoUsuario_Codigo ) ) )
            {
               A146Modulo_Codigo = T001411_A146Modulo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               n146Modulo_Codigo = T001411_n146Modulo_Codigo[0];
               A161FuncaoUsuario_Codigo = T001411_A161FuncaoUsuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               RcdFound41 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1441( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1441( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound41 == 1 )
            {
               if ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) || ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) )
               {
                  A146Modulo_Codigo = Z146Modulo_Codigo;
                  n146Modulo_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
                  A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "MODULO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = dynModulo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynModulo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1441( ) ;
                  GX_FocusControl = dynModulo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) || ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = dynModulo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1441( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "MODULO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = dynModulo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = dynModulo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1441( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A146Modulo_Codigo != Z146Modulo_Codigo ) || ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) )
         {
            A146Modulo_Codigo = Z146Modulo_Codigo;
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "MODULO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound41 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "MODULO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1441( ) ;
         if ( RcdFound41 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1441( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound41 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound41 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1441( ) ;
         if ( RcdFound41 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound41 != 0 )
            {
               ScanNext1441( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1441( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1441( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00142 */
            pr_default.execute(0, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ModuloFuncoes1"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ModuloFuncoes1"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1441( )
      {
         BeforeValidate1441( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1441( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1441( 0) ;
            CheckOptimisticConcurrency1441( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1441( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1441( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001412 */
                     pr_default.execute(10, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ModuloFuncoes1") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption140( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1441( ) ;
            }
            EndLevel1441( ) ;
         }
         CloseExtendedTableCursors1441( ) ;
      }

      protected void Update1441( )
      {
         BeforeValidate1441( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1441( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1441( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1441( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1441( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ModuloFuncoes1] */
                     DeferredUpdate1441( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption140( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1441( ) ;
         }
         CloseExtendedTableCursors1441( ) ;
      }

      protected void DeferredUpdate1441( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1441( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1441( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1441( ) ;
            AfterConfirm1441( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1441( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001413 */
                  pr_default.execute(11, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo, A161FuncaoUsuario_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ModuloFuncoes1") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound41 == 0 )
                        {
                           InitAll1441( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption140( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode41 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1441( ) ;
         Gx_mode = sMode41;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1441( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001414 */
            pr_default.execute(12, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
            A144Modulo_Descricao = T001414_A144Modulo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
            n144Modulo_Descricao = T001414_n144Modulo_Descricao[0];
            pr_default.close(12);
            /* Using cursor T001415 */
            pr_default.execute(13, new Object[] {A161FuncaoUsuario_Codigo});
            A162FuncaoUsuario_Nome = T001415_A162FuncaoUsuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
            pr_default.close(13);
         }
      }

      protected void EndLevel1441( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1441( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.CommitDataStores( "ModuloFuncoes");
            if ( AnyError == 0 )
            {
               ConfirmValues140( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.RollbackDataStores( "ModuloFuncoes");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1441( )
      {
         /* Using cursor T001416 */
         pr_default.execute(14);
         RcdFound41 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound41 = 1;
            A146Modulo_Codigo = T001416_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T001416_n146Modulo_Codigo[0];
            A161FuncaoUsuario_Codigo = T001416_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1441( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound41 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound41 = 1;
            A146Modulo_Codigo = T001416_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T001416_n146Modulo_Codigo[0];
            A161FuncaoUsuario_Codigo = T001416_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1441( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm1441( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1441( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1441( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1441( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1441( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1441( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1441( )
      {
         dynModulo_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynModulo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynModulo_Codigo.Enabled), 5, 0)));
         edtModulo_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtModulo_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtModulo_Descricao_Enabled), 5, 0)));
         edtFuncaoUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Codigo_Enabled), 5, 0)));
         edtFuncaoUsuario_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Nome_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues140( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117185416");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("modulofuncoes.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z146Modulo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z146Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("modulofuncoes.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ModuloFuncoes" ;
      }

      public override String GetPgmdesc( )
      {
         return "Modulo Funcoes" ;
      }

      protected void InitializeNonKey1441( )
      {
         A144Modulo_Descricao = "";
         n144Modulo_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
         A162FuncaoUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
      }

      protected void InitAll1441( )
      {
         A146Modulo_Codigo = 0;
         n146Modulo_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
         A161FuncaoUsuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         InitializeNonKey1441( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117185419");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("modulofuncoes.js", "?20203117185419");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockmodulo_codigo_Internalname = "TEXTBLOCKMODULO_CODIGO";
         dynModulo_Codigo_Internalname = "MODULO_CODIGO";
         lblTextblockmodulo_descricao_Internalname = "TEXTBLOCKMODULO_DESCRICAO";
         edtModulo_Descricao_Internalname = "MODULO_DESCRICAO";
         lblTextblockfuncaousuario_codigo_Internalname = "TEXTBLOCKFUNCAOUSUARIO_CODIGO";
         edtFuncaoUsuario_Codigo_Internalname = "FUNCAOUSUARIO_CODIGO";
         lblTextblockfuncaousuario_nome_Internalname = "TEXTBLOCKFUNCAOUSUARIO_NOME";
         edtFuncaoUsuario_Nome_Internalname = "FUNCAOUSUARIO_NOME";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Modulo Funcoes";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtFuncaoUsuario_Nome_Jsonclick = "";
         edtFuncaoUsuario_Nome_Enabled = 0;
         edtFuncaoUsuario_Codigo_Jsonclick = "";
         edtFuncaoUsuario_Codigo_Enabled = 1;
         edtModulo_Descricao_Enabled = 0;
         dynModulo_Codigo_Jsonclick = "";
         dynModulo_Codigo.Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAMODULO_CODIGO1441( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAMODULO_CODIGO_data1441( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAMODULO_CODIGO_html1441( )
      {
         int gxdynajaxvalue ;
         GXDLAMODULO_CODIGO_data1441( ) ;
         gxdynajaxindex = 1;
         dynModulo_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynModulo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAMODULO_CODIGO_data1441( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T001417 */
         pr_default.execute(15);
         while ( (pr_default.getStatus(15) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001417_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001417_A143Modulo_Nome[0]));
            pr_default.readNext(15);
         }
         pr_default.close(15);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001418 */
         pr_default.execute(16, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynModulo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A144Modulo_Descricao = T001418_A144Modulo_Descricao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A144Modulo_Descricao", A144Modulo_Descricao);
         n144Modulo_Descricao = T001418_n144Modulo_Descricao[0];
         pr_default.close(16);
         /* Using cursor T001419 */
         pr_default.execute(17, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A162FuncaoUsuario_Nome = T001419_A162FuncaoUsuario_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
         pr_default.close(17);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Modulo_codigo( GXCombobox dynGX_Parm1 ,
                                       String GX_Parm2 )
      {
         dynModulo_Codigo = dynGX_Parm1;
         A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.CurrentValue, "."));
         n146Modulo_Codigo = false;
         A144Modulo_Descricao = GX_Parm2;
         n144Modulo_Descricao = false;
         /* Using cursor T001420 */
         pr_default.execute(18, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynModulo_Codigo_Internalname;
         }
         A144Modulo_Descricao = T001420_A144Modulo_Descricao[0];
         n144Modulo_Descricao = T001420_n144Modulo_Descricao[0];
         pr_default.close(18);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A144Modulo_Descricao = "";
            n144Modulo_Descricao = false;
         }
         isValidOutput.Add(A144Modulo_Descricao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaousuario_codigo( GXCombobox dynGX_Parm1 ,
                                              int GX_Parm2 ,
                                              String GX_Parm3 )
      {
         dynModulo_Codigo = dynGX_Parm1;
         A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.CurrentValue, "."));
         n146Modulo_Codigo = false;
         A161FuncaoUsuario_Codigo = GX_Parm2;
         A162FuncaoUsuario_Nome = GX_Parm3;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T001421 */
         pr_default.execute(19, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
         }
         A162FuncaoUsuario_Nome = T001421_A162FuncaoUsuario_Nome[0];
         pr_default.close(19);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A144Modulo_Descricao = "";
            n144Modulo_Descricao = false;
            A162FuncaoUsuario_Nome = "";
         }
         isValidOutput.Add(A144Modulo_Descricao);
         isValidOutput.Add(A162FuncaoUsuario_Nome);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z146Modulo_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z144Modulo_Descricao);
         isValidOutput.Add(Z162FuncaoUsuario_Nome);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(18);
         pr_default.close(16);
         pr_default.close(12);
         pr_default.close(19);
         pr_default.close(17);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockmodulo_codigo_Jsonclick = "";
         lblTextblockmodulo_descricao_Jsonclick = "";
         A144Modulo_Descricao = "";
         lblTextblockfuncaousuario_codigo_Jsonclick = "";
         lblTextblockfuncaousuario_nome_Jsonclick = "";
         A162FuncaoUsuario_Nome = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z144Modulo_Descricao = "";
         Z162FuncaoUsuario_Nome = "";
         T00144_A144Modulo_Descricao = new String[] {""} ;
         T00144_n144Modulo_Descricao = new bool[] {false} ;
         T00146_A144Modulo_Descricao = new String[] {""} ;
         T00146_n144Modulo_Descricao = new bool[] {false} ;
         T00146_A162FuncaoUsuario_Nome = new String[] {""} ;
         T00146_A146Modulo_Codigo = new int[1] ;
         T00146_n146Modulo_Codigo = new bool[] {false} ;
         T00146_A161FuncaoUsuario_Codigo = new int[1] ;
         T00145_A162FuncaoUsuario_Nome = new String[] {""} ;
         T00147_A144Modulo_Descricao = new String[] {""} ;
         T00147_n144Modulo_Descricao = new bool[] {false} ;
         T00148_A162FuncaoUsuario_Nome = new String[] {""} ;
         T00149_A146Modulo_Codigo = new int[1] ;
         T00149_n146Modulo_Codigo = new bool[] {false} ;
         T00149_A161FuncaoUsuario_Codigo = new int[1] ;
         T00143_A146Modulo_Codigo = new int[1] ;
         T00143_n146Modulo_Codigo = new bool[] {false} ;
         T00143_A161FuncaoUsuario_Codigo = new int[1] ;
         sMode41 = "";
         T001410_A146Modulo_Codigo = new int[1] ;
         T001410_n146Modulo_Codigo = new bool[] {false} ;
         T001410_A161FuncaoUsuario_Codigo = new int[1] ;
         T001411_A146Modulo_Codigo = new int[1] ;
         T001411_n146Modulo_Codigo = new bool[] {false} ;
         T001411_A161FuncaoUsuario_Codigo = new int[1] ;
         T00142_A146Modulo_Codigo = new int[1] ;
         T00142_n146Modulo_Codigo = new bool[] {false} ;
         T00142_A161FuncaoUsuario_Codigo = new int[1] ;
         T001414_A144Modulo_Descricao = new String[] {""} ;
         T001414_n144Modulo_Descricao = new bool[] {false} ;
         T001415_A162FuncaoUsuario_Nome = new String[] {""} ;
         T001416_A146Modulo_Codigo = new int[1] ;
         T001416_n146Modulo_Codigo = new bool[] {false} ;
         T001416_A161FuncaoUsuario_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001417_A456ContagemResultado_Codigo = new int[1] ;
         T001417_A146Modulo_Codigo = new int[1] ;
         T001417_n146Modulo_Codigo = new bool[] {false} ;
         T001417_A143Modulo_Nome = new String[] {""} ;
         T001417_A127Sistema_Codigo = new int[1] ;
         T001417_A489ContagemResultado_SistemaCod = new int[1] ;
         T001417_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         T001418_A144Modulo_Descricao = new String[] {""} ;
         T001418_n144Modulo_Descricao = new bool[] {false} ;
         T001419_A162FuncaoUsuario_Nome = new String[] {""} ;
         T001420_A144Modulo_Descricao = new String[] {""} ;
         T001420_n144Modulo_Descricao = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T001421_A162FuncaoUsuario_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.modulofuncoes__default(),
            new Object[][] {
                new Object[] {
               T00142_A146Modulo_Codigo, T00142_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T00143_A146Modulo_Codigo, T00143_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T00144_A144Modulo_Descricao, T00144_n144Modulo_Descricao
               }
               , new Object[] {
               T00145_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               T00146_A144Modulo_Descricao, T00146_n144Modulo_Descricao, T00146_A162FuncaoUsuario_Nome, T00146_A146Modulo_Codigo, T00146_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T00147_A144Modulo_Descricao, T00147_n144Modulo_Descricao
               }
               , new Object[] {
               T00148_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               T00149_A146Modulo_Codigo, T00149_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T001410_A146Modulo_Codigo, T001410_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T001411_A146Modulo_Codigo, T001411_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001414_A144Modulo_Descricao, T001414_n144Modulo_Descricao
               }
               , new Object[] {
               T001415_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               T001416_A146Modulo_Codigo, T001416_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T001417_A456ContagemResultado_Codigo, T001417_A146Modulo_Codigo, T001417_n146Modulo_Codigo, T001417_A143Modulo_Nome, T001417_A127Sistema_Codigo, T001417_A489ContagemResultado_SistemaCod, T001417_n489ContagemResultado_SistemaCod
               }
               , new Object[] {
               T001418_A144Modulo_Descricao, T001418_n144Modulo_Descricao
               }
               , new Object[] {
               T001419_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               T001420_A144Modulo_Descricao, T001420_n144Modulo_Descricao
               }
               , new Object[] {
               T001421_A162FuncaoUsuario_Nome
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound41 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z146Modulo_Codigo ;
      private int Z161FuncaoUsuario_Codigo ;
      private int A146Modulo_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtModulo_Descricao_Enabled ;
      private int edtFuncaoUsuario_Codigo_Enabled ;
      private int edtFuncaoUsuario_Nome_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynModulo_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockmodulo_codigo_Internalname ;
      private String lblTextblockmodulo_codigo_Jsonclick ;
      private String dynModulo_Codigo_Jsonclick ;
      private String lblTextblockmodulo_descricao_Internalname ;
      private String lblTextblockmodulo_descricao_Jsonclick ;
      private String edtModulo_Descricao_Internalname ;
      private String lblTextblockfuncaousuario_codigo_Internalname ;
      private String lblTextblockfuncaousuario_codigo_Jsonclick ;
      private String edtFuncaoUsuario_Codigo_Internalname ;
      private String edtFuncaoUsuario_Codigo_Jsonclick ;
      private String lblTextblockfuncaousuario_nome_Internalname ;
      private String lblTextblockfuncaousuario_nome_Jsonclick ;
      private String edtFuncaoUsuario_Nome_Internalname ;
      private String edtFuncaoUsuario_Nome_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode41 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool n146Modulo_Codigo ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n144Modulo_Descricao ;
      private String A144Modulo_Descricao ;
      private String Z144Modulo_Descricao ;
      private String A162FuncaoUsuario_Nome ;
      private String Z162FuncaoUsuario_Nome ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynModulo_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T00144_A144Modulo_Descricao ;
      private bool[] T00144_n144Modulo_Descricao ;
      private String[] T00146_A144Modulo_Descricao ;
      private bool[] T00146_n144Modulo_Descricao ;
      private String[] T00146_A162FuncaoUsuario_Nome ;
      private int[] T00146_A146Modulo_Codigo ;
      private bool[] T00146_n146Modulo_Codigo ;
      private int[] T00146_A161FuncaoUsuario_Codigo ;
      private String[] T00145_A162FuncaoUsuario_Nome ;
      private String[] T00147_A144Modulo_Descricao ;
      private bool[] T00147_n144Modulo_Descricao ;
      private String[] T00148_A162FuncaoUsuario_Nome ;
      private int[] T00149_A146Modulo_Codigo ;
      private bool[] T00149_n146Modulo_Codigo ;
      private int[] T00149_A161FuncaoUsuario_Codigo ;
      private int[] T00143_A146Modulo_Codigo ;
      private bool[] T00143_n146Modulo_Codigo ;
      private int[] T00143_A161FuncaoUsuario_Codigo ;
      private int[] T001410_A146Modulo_Codigo ;
      private bool[] T001410_n146Modulo_Codigo ;
      private int[] T001410_A161FuncaoUsuario_Codigo ;
      private int[] T001411_A146Modulo_Codigo ;
      private bool[] T001411_n146Modulo_Codigo ;
      private int[] T001411_A161FuncaoUsuario_Codigo ;
      private int[] T00142_A146Modulo_Codigo ;
      private bool[] T00142_n146Modulo_Codigo ;
      private int[] T00142_A161FuncaoUsuario_Codigo ;
      private String[] T001414_A144Modulo_Descricao ;
      private bool[] T001414_n144Modulo_Descricao ;
      private String[] T001415_A162FuncaoUsuario_Nome ;
      private int[] T001416_A146Modulo_Codigo ;
      private bool[] T001416_n146Modulo_Codigo ;
      private int[] T001416_A161FuncaoUsuario_Codigo ;
      private int[] T001417_A456ContagemResultado_Codigo ;
      private int[] T001417_A146Modulo_Codigo ;
      private bool[] T001417_n146Modulo_Codigo ;
      private String[] T001417_A143Modulo_Nome ;
      private int[] T001417_A127Sistema_Codigo ;
      private int[] T001417_A489ContagemResultado_SistemaCod ;
      private bool[] T001417_n489ContagemResultado_SistemaCod ;
      private String[] T001418_A144Modulo_Descricao ;
      private bool[] T001418_n144Modulo_Descricao ;
      private String[] T001419_A162FuncaoUsuario_Nome ;
      private String[] T001420_A144Modulo_Descricao ;
      private bool[] T001420_n144Modulo_Descricao ;
      private String[] T001421_A162FuncaoUsuario_Nome ;
      private GXWebForm Form ;
   }

   public class modulofuncoes__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00146 ;
          prmT00146 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00144 ;
          prmT00144 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00145 ;
          prmT00145 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00147 ;
          prmT00147 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00148 ;
          prmT00148 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00149 ;
          prmT00149 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00143 ;
          prmT00143 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001410 ;
          prmT001410 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001411 ;
          prmT001411 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00142 ;
          prmT00142 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001412 ;
          prmT001412 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001413 ;
          prmT001413 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001414 ;
          prmT001414 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001415 ;
          prmT001415 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001416 ;
          prmT001416 = new Object[] {
          } ;
          Object[] prmT001417 ;
          prmT001417 = new Object[] {
          } ;
          Object[] prmT001418 ;
          prmT001418 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001419 ;
          prmT001419 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001420 ;
          prmT001420 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001421 ;
          prmT001421 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00142", "SELECT [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (UPDLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00142,1,0,true,false )
             ,new CursorDef("T00143", "SELECT [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00143,1,0,true,false )
             ,new CursorDef("T00144", "SELECT [Modulo_Descricao] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00144,1,0,true,false )
             ,new CursorDef("T00145", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00145,1,0,true,false )
             ,new CursorDef("T00146", "SELECT T2.[Modulo_Descricao], T3.[FuncaoUsuario_Nome], TM1.[Modulo_Codigo], TM1.[FuncaoUsuario_Codigo] FROM (([ModuloFuncoes1] TM1 WITH (NOLOCK) INNER JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = TM1.[Modulo_Codigo]) INNER JOIN [ModuloFuncoes] T3 WITH (NOLOCK) ON T3.[FuncaoUsuario_Codigo] = TM1.[FuncaoUsuario_Codigo]) WHERE TM1.[Modulo_Codigo] = @Modulo_Codigo and TM1.[FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ORDER BY TM1.[Modulo_Codigo], TM1.[FuncaoUsuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00146,100,0,true,false )
             ,new CursorDef("T00147", "SELECT [Modulo_Descricao] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00147,1,0,true,false )
             ,new CursorDef("T00148", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00148,1,0,true,false )
             ,new CursorDef("T00149", "SELECT [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00149,1,0,true,false )
             ,new CursorDef("T001410", "SELECT TOP 1 [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE ( [Modulo_Codigo] > @Modulo_Codigo or [Modulo_Codigo] = @Modulo_Codigo and [FuncaoUsuario_Codigo] > @FuncaoUsuario_Codigo) ORDER BY [Modulo_Codigo], [FuncaoUsuario_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001410,1,0,true,true )
             ,new CursorDef("T001411", "SELECT TOP 1 [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE ( [Modulo_Codigo] < @Modulo_Codigo or [Modulo_Codigo] = @Modulo_Codigo and [FuncaoUsuario_Codigo] < @FuncaoUsuario_Codigo) ORDER BY [Modulo_Codigo] DESC, [FuncaoUsuario_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001411,1,0,true,true )
             ,new CursorDef("T001412", "INSERT INTO [ModuloFuncoes1]([Modulo_Codigo], [FuncaoUsuario_Codigo]) VALUES(@Modulo_Codigo, @FuncaoUsuario_Codigo)", GxErrorMask.GX_NOMASK,prmT001412)
             ,new CursorDef("T001413", "DELETE FROM [ModuloFuncoes1]  WHERE [Modulo_Codigo] = @Modulo_Codigo AND [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo", GxErrorMask.GX_NOMASK,prmT001413)
             ,new CursorDef("T001414", "SELECT [Modulo_Descricao] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001414,1,0,true,false )
             ,new CursorDef("T001415", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001415,1,0,true,false )
             ,new CursorDef("T001416", "SELECT [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) ORDER BY [Modulo_Codigo], [FuncaoUsuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001416,100,0,true,false )
             ,new CursorDef("T001417", "SELECT T1.[ContagemResultado_Codigo], T1.[Modulo_Codigo], T2.[Modulo_Nome], T2.[Sistema_Codigo], T1.[ContagemResultado_SistemaCod] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) WHERE T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod] ORDER BY T2.[Modulo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001417,0,0,true,false )
             ,new CursorDef("T001418", "SELECT [Modulo_Descricao] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001418,1,0,true,false )
             ,new CursorDef("T001419", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001419,1,0,true,false )
             ,new CursorDef("T001420", "SELECT [Modulo_Descricao] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001420,1,0,true,false )
             ,new CursorDef("T001421", "SELECT [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001421,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
