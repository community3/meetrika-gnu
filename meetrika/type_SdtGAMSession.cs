/*
               File: type_SdtGAMSession
        Description: GAMSession
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:12.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMSession : GxUserType, IGxExternalObject
   {
      public SdtGAMSession( )
      {
         initialize();
      }

      public SdtGAMSession( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public SdtGAMSession get( out IGxCollection gxTp_Errors )
      {
         SdtGAMSession returnget ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMSession_externalReference == null )
         {
            GAMSession_externalReference = new Artech.Security.GAMSession(context);
         }
         returnget = new SdtGAMSession(context);
         Artech.Security.GAMSession externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = GAMSession_externalReference.Get(out externalParm1);
         returnget.ExternalInstance = externalParm0;
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returnget ;
      }

      public bool isvalid( out SdtGAMSession gxTp_Session ,
                           out IGxCollection gxTp_Errors )
      {
         bool returnisvalid ;
         gxTp_Session = new SdtGAMSession(context);
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMSession_externalReference == null )
         {
            GAMSession_externalReference = new Artech.Security.GAMSession(context);
         }
         returnisvalid = false;
         Artech.Security.GAMSession externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         returnisvalid = (bool)(GAMSession_externalReference.IsValid(out externalParm0, out externalParm1));
         gxTp_Session.ExternalInstance = externalParm0;
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returnisvalid ;
      }

      public bool isanonymoususer( out IGxCollection gxTp_Errors )
      {
         bool returnisanonymoususer ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMSession_externalReference == null )
         {
            GAMSession_externalReference = new Artech.Security.GAMSession(context);
         }
         returnisanonymoususer = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returnisanonymoususer = (bool)(GAMSession_externalReference.IsAnonymousUser(out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returnisanonymoususer ;
      }

      public IGxCollection getroles( out IGxCollection gxTp_Errors )
      {
         IGxCollection returngetroles ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMSession_externalReference == null )
         {
            GAMSession_externalReference = new Artech.Security.GAMSession(context);
         }
         returngetroles = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMRole> externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = GAMSession_externalReference.GetRoles(out externalParm1);
         returngetroles.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMRole>), externalParm0);
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returngetroles ;
      }

      public bool setapplicationdata( String gxTp_ApplicationData ,
                                      out IGxCollection gxTp_Errors )
      {
         bool returnsetapplicationdata ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMSession_externalReference == null )
         {
            GAMSession_externalReference = new Artech.Security.GAMSession(context);
         }
         returnsetapplicationdata = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returnsetapplicationdata = (bool)(GAMSession_externalReference.SetApplicationData((string)(gxTp_ApplicationData), out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returnsetapplicationdata ;
      }

      public String getapplicationdata( )
      {
         String returngetapplicationdata ;
         if ( GAMSession_externalReference == null )
         {
            GAMSession_externalReference = new Artech.Security.GAMSession(context);
         }
         returngetapplicationdata = "";
         returngetapplicationdata = (String)(GAMSession_externalReference.GetApplicationData());
         return returngetapplicationdata ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMSession_externalReference == null )
         {
            GAMSession_externalReference = new Artech.Security.GAMSession(context);
         }
         returntostring = "";
         returntostring = (String)(GAMSession_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Token
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.Token ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.Token = value;
         }

      }

      public SdtGAMUser gxTpr_User
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            SdtGAMUser intValue ;
            intValue = new SdtGAMUser(context);
            Artech.Security.GAMUser externalParm0 ;
            externalParm0 = GAMSession_externalReference.User;
            intValue.ExternalInstance = externalParm0;
            return intValue ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            SdtGAMUser intValue ;
            Artech.Security.GAMUser externalParm1 ;
            intValue = value;
            externalParm1 = (Artech.Security.GAMUser)(intValue.ExternalInstance);
            GAMSession_externalReference.User = externalParm1;
         }

      }

      public DateTime gxTpr_Date
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.Date ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.Date = value;
         }

      }

      public String gxTpr_Status
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.Status ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.Status = value;
         }

      }

      public short gxTpr_Type
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.Type ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.Type = value;
         }

      }

      public String gxTpr_Initialurl
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.InitialURL ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.InitialURL = value;
         }

      }

      public String gxTpr_Initialipaddress
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.InitialIPAddress ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.InitialIPAddress = value;
         }

      }

      public String gxTpr_Initialdeviceid
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.InitialDeviceId ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.InitialDeviceId = value;
         }

      }

      public short gxTpr_Browserid
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.BrowserId ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.BrowserId = value;
         }

      }

      public short gxTpr_Operatingsystemid
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.OperatingSystemId ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.OperatingSystemId = value;
         }

      }

      public DateTime gxTpr_Lastaccess
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.LastAccess ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.LastAccess = value;
         }

      }

      public String gxTpr_Lasturl
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.LastURL ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.LastURL = value;
         }

      }

      public SdtGAMSecurityPolicy gxTpr_Securitypolicy
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            SdtGAMSecurityPolicy intValue ;
            intValue = new SdtGAMSecurityPolicy(context);
            Artech.Security.GAMSecurityPolicy externalParm2 ;
            externalParm2 = GAMSession_externalReference.SecurityPolicy;
            intValue.ExternalInstance = externalParm2;
            return intValue ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            SdtGAMSecurityPolicy intValue ;
            Artech.Security.GAMSecurityPolicy externalParm3 ;
            intValue = value;
            externalParm3 = (Artech.Security.GAMSecurityPolicy)(intValue.ExternalInstance);
            GAMSession_externalReference.SecurityPolicy = externalParm3;
         }

      }

      public DateTime gxTpr_Logindate
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.LoginDate ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.LoginDate = value;
         }

      }

      public String gxTpr_Authenticationtypename
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.AuthenticationTypeName ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.AuthenticationTypeName = value;
         }

      }

      public int gxTpr_Loginretrycount
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.LoginRetryCount ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.LoginRetryCount = value;
         }

      }

      public bool gxTpr_Isanonymous
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.IsAnonymous ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.IsAnonymous = value;
         }

      }

      public String gxTpr_Externaltoken
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.ExternalToken ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.ExternalToken = value;
         }

      }

      public bool gxTpr_Isended
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.IsEnded ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.IsEnded = value;
         }

      }

      public DateTime gxTpr_Ended
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.Ended ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.Ended = value;
         }

      }

      public long gxTpr_Applicationid
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.ApplicationId ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.ApplicationId = value;
         }

      }

      public DateTime gxTpr_Expires
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.Expires ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.Expires = value;
         }

      }

      public String gxTpr_Refreshtoken
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.RefreshToken ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.RefreshToken = value;
         }

      }

      public String gxTpr_Permissiontoken
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.PermissionToken ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.PermissionToken = value;
         }

      }

      public IGxCollection gxTpr_Loginretries
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMSessionLoginRetry", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMSessionLoginRetry> externalParm4 ;
            externalParm4 = GAMSession_externalReference.LoginRetries;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMSessionLoginRetry>), externalParm4);
            return intValue ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMSessionLoginRetry> externalParm5 ;
            intValue = value;
            externalParm5 = (System.Collections.Generic.List<Artech.Security.GAMSessionLoginRetry>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMSessionLoginRetry>), intValue.ExternalInstance);
            GAMSession_externalReference.LoginRetries = externalParm5;
         }

      }

      public DateTime gxTpr_Lastaccessdb
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.LastAccessDB ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.LastAccessDB = value;
         }

      }

      public bool gxTpr_Sessionindb
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.SessionInDB ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.SessionInDB = value;
         }

      }

      public IGxCollection gxTpr_Roles
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMSessionRole", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMSessionRole> externalParm6 ;
            externalParm6 = GAMSession_externalReference.Roles;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMSessionRole>), externalParm6);
            return intValue ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMSessionRole> externalParm7 ;
            intValue = value;
            externalParm7 = (System.Collections.Generic.List<Artech.Security.GAMSessionRole>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMSessionRole>), intValue.ExternalInstance);
            GAMSession_externalReference.Roles = externalParm7;
         }

      }

      public String gxTpr_Scope
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference.Scope ;
         }

         set {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            GAMSession_externalReference.Scope = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMSession_externalReference == null )
            {
               GAMSession_externalReference = new Artech.Security.GAMSession(context);
            }
            return GAMSession_externalReference ;
         }

         set {
            GAMSession_externalReference = (Artech.Security.GAMSession)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMSession GAMSession_externalReference=null ;
   }

}
