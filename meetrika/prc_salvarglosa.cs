/*
               File: PRC_SalvarGlosa
        Description: Salvar Glosa
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:6:17.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_salvarglosa : GXProcedure
   {
      public prc_salvarglosa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_salvarglosa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_Indicador ,
                           DateTime aP2_Solicitada ,
                           short aP3_Prazo ,
                           DateTime aP4_Entregue ,
                           short aP5_Atraso ,
                           decimal aP6_Reduz ,
                           decimal aP7_Valor )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV12Indicador = aP1_Indicador;
         this.AV13Solicitada = aP2_Solicitada;
         this.AV14Prazo = aP3_Prazo;
         this.AV15Entregue = aP4_Entregue;
         this.AV16Atraso = aP5_Atraso;
         this.AV17Reduz = aP6_Reduz;
         this.AV11Valor = aP7_Valor;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_Indicador ,
                                 DateTime aP2_Solicitada ,
                                 short aP3_Prazo ,
                                 DateTime aP4_Entregue ,
                                 short aP5_Atraso ,
                                 decimal aP6_Reduz ,
                                 decimal aP7_Valor )
      {
         prc_salvarglosa objprc_salvarglosa;
         objprc_salvarglosa = new prc_salvarglosa();
         objprc_salvarglosa.AV8Codigo = aP0_Codigo;
         objprc_salvarglosa.AV12Indicador = aP1_Indicador;
         objprc_salvarglosa.AV13Solicitada = aP2_Solicitada;
         objprc_salvarglosa.AV14Prazo = aP3_Prazo;
         objprc_salvarglosa.AV15Entregue = aP4_Entregue;
         objprc_salvarglosa.AV16Atraso = aP5_Atraso;
         objprc_salvarglosa.AV17Reduz = aP6_Reduz;
         objprc_salvarglosa.AV11Valor = aP7_Valor;
         objprc_salvarglosa.context.SetSubmitInitialConfig(context);
         objprc_salvarglosa.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_salvarglosa);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_salvarglosa)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P008Q2 */
         pr_default.execute(0, new Object[] {AV8Codigo, AV12Indicador});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1315ContagemResultadoIndicadores_IndicadorCod = P008Q2_A1315ContagemResultadoIndicadores_IndicadorCod[0];
            A1314ContagemResultadoIndicadores_DemandaCod = P008Q2_A1314ContagemResultadoIndicadores_DemandaCod[0];
            A484ContagemResultado_StatusDmn = P008Q2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008Q2_n484ContagemResultado_StatusDmn[0];
            A1317ContagemResultadoIndicadores_Data = P008Q2_A1317ContagemResultadoIndicadores_Data[0];
            A1318ContagemResultadoIndicadores_Solicitada = P008Q2_A1318ContagemResultadoIndicadores_Solicitada[0];
            A1319ContagemResultadoIndicadores_Prazo = P008Q2_A1319ContagemResultadoIndicadores_Prazo[0];
            A1324ContagemResultadoIndicadores_Entregue = P008Q2_A1324ContagemResultadoIndicadores_Entregue[0];
            A1321ContagemResultadoIndicadores_Atraso = P008Q2_A1321ContagemResultadoIndicadores_Atraso[0];
            A1322ContagemResultadoIndicadores_Reduz = P008Q2_A1322ContagemResultadoIndicadores_Reduz[0];
            A1323ContagemResultadoIndicadores_Valor = P008Q2_A1323ContagemResultadoIndicadores_Valor[0];
            A484ContagemResultado_StatusDmn = P008Q2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P008Q2_n484ContagemResultado_StatusDmn[0];
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H") == 0 )
            {
               A1317ContagemResultadoIndicadores_Data = DateTimeUtil.ServerDate( context, "DEFAULT");
               A1318ContagemResultadoIndicadores_Solicitada = AV13Solicitada;
               A1319ContagemResultadoIndicadores_Prazo = AV14Prazo;
               A1324ContagemResultadoIndicadores_Entregue = AV15Entregue;
               A1321ContagemResultadoIndicadores_Atraso = AV16Atraso;
               A1322ContagemResultadoIndicadores_Reduz = AV17Reduz;
               A1323ContagemResultadoIndicadores_Valor = AV11Valor;
            }
            AV19Achado = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P008Q3 */
            pr_default.execute(1, new Object[] {A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1324ContagemResultadoIndicadores_Entregue, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
            if (true) break;
            /* Using cursor P008Q4 */
            pr_default.execute(2, new Object[] {A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1324ContagemResultadoIndicadores_Entregue, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ! AV19Achado && ( AV12Indicador > 0 ) )
         {
            /*
               INSERT RECORD ON TABLE ContagemResultadoIndicadores

            */
            A1314ContagemResultadoIndicadores_DemandaCod = AV8Codigo;
            A1315ContagemResultadoIndicadores_IndicadorCod = AV12Indicador;
            A1317ContagemResultadoIndicadores_Data = DateTimeUtil.ServerDate( context, "DEFAULT");
            A1318ContagemResultadoIndicadores_Solicitada = AV13Solicitada;
            A1319ContagemResultadoIndicadores_Prazo = AV14Prazo;
            A1324ContagemResultadoIndicadores_Entregue = AV15Entregue;
            A1321ContagemResultadoIndicadores_Atraso = AV16Atraso;
            A1322ContagemResultadoIndicadores_Reduz = AV17Reduz;
            A1323ContagemResultadoIndicadores_Valor = AV11Valor;
            A1316ContagemResultadoIndicadores_LoteCod = 0;
            n1316ContagemResultadoIndicadores_LoteCod = false;
            n1316ContagemResultadoIndicadores_LoteCod = true;
            /* Using cursor P008Q5 */
            pr_default.execute(3, new Object[] {A1314ContagemResultadoIndicadores_DemandaCod, A1315ContagemResultadoIndicadores_IndicadorCod, A1317ContagemResultadoIndicadores_Data, A1318ContagemResultadoIndicadores_Solicitada, A1319ContagemResultadoIndicadores_Prazo, A1321ContagemResultadoIndicadores_Atraso, A1322ContagemResultadoIndicadores_Reduz, A1323ContagemResultadoIndicadores_Valor, n1316ContagemResultadoIndicadores_LoteCod, A1316ContagemResultadoIndicadores_LoteCod, A1324ContagemResultadoIndicadores_Entregue});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoIndicadores") ;
            if ( (pr_default.getStatus(3) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008Q2_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         P008Q2_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         P008Q2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P008Q2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P008Q2_A1317ContagemResultadoIndicadores_Data = new DateTime[] {DateTime.MinValue} ;
         P008Q2_A1318ContagemResultadoIndicadores_Solicitada = new DateTime[] {DateTime.MinValue} ;
         P008Q2_A1319ContagemResultadoIndicadores_Prazo = new short[1] ;
         P008Q2_A1324ContagemResultadoIndicadores_Entregue = new DateTime[] {DateTime.MinValue} ;
         P008Q2_A1321ContagemResultadoIndicadores_Atraso = new short[1] ;
         P008Q2_A1322ContagemResultadoIndicadores_Reduz = new decimal[1] ;
         P008Q2_A1323ContagemResultadoIndicadores_Valor = new decimal[1] ;
         A484ContagemResultado_StatusDmn = "";
         A1317ContagemResultadoIndicadores_Data = DateTime.MinValue;
         A1318ContagemResultadoIndicadores_Solicitada = DateTime.MinValue;
         A1324ContagemResultadoIndicadores_Entregue = DateTime.MinValue;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_salvarglosa__default(),
            new Object[][] {
                new Object[] {
               P008Q2_A1315ContagemResultadoIndicadores_IndicadorCod, P008Q2_A1314ContagemResultadoIndicadores_DemandaCod, P008Q2_A484ContagemResultado_StatusDmn, P008Q2_n484ContagemResultado_StatusDmn, P008Q2_A1317ContagemResultadoIndicadores_Data, P008Q2_A1318ContagemResultadoIndicadores_Solicitada, P008Q2_A1319ContagemResultadoIndicadores_Prazo, P008Q2_A1324ContagemResultadoIndicadores_Entregue, P008Q2_A1321ContagemResultadoIndicadores_Atraso, P008Q2_A1322ContagemResultadoIndicadores_Reduz,
               P008Q2_A1323ContagemResultadoIndicadores_Valor
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14Prazo ;
      private short AV16Atraso ;
      private short A1319ContagemResultadoIndicadores_Prazo ;
      private short A1321ContagemResultadoIndicadores_Atraso ;
      private int AV8Codigo ;
      private int AV12Indicador ;
      private int A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int A1314ContagemResultadoIndicadores_DemandaCod ;
      private int GX_INS160 ;
      private int A1316ContagemResultadoIndicadores_LoteCod ;
      private decimal AV17Reduz ;
      private decimal AV11Valor ;
      private decimal A1322ContagemResultadoIndicadores_Reduz ;
      private decimal A1323ContagemResultadoIndicadores_Valor ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String Gx_emsg ;
      private DateTime AV13Solicitada ;
      private DateTime AV15Entregue ;
      private DateTime A1317ContagemResultadoIndicadores_Data ;
      private DateTime A1318ContagemResultadoIndicadores_Solicitada ;
      private DateTime A1324ContagemResultadoIndicadores_Entregue ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool AV19Achado ;
      private bool n1316ContagemResultadoIndicadores_LoteCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P008Q2_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] P008Q2_A1314ContagemResultadoIndicadores_DemandaCod ;
      private String[] P008Q2_A484ContagemResultado_StatusDmn ;
      private bool[] P008Q2_n484ContagemResultado_StatusDmn ;
      private DateTime[] P008Q2_A1317ContagemResultadoIndicadores_Data ;
      private DateTime[] P008Q2_A1318ContagemResultadoIndicadores_Solicitada ;
      private short[] P008Q2_A1319ContagemResultadoIndicadores_Prazo ;
      private DateTime[] P008Q2_A1324ContagemResultadoIndicadores_Entregue ;
      private short[] P008Q2_A1321ContagemResultadoIndicadores_Atraso ;
      private decimal[] P008Q2_A1322ContagemResultadoIndicadores_Reduz ;
      private decimal[] P008Q2_A1323ContagemResultadoIndicadores_Valor ;
   }

   public class prc_salvarglosa__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008Q2 ;
          prmP008Q2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12Indicador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008Q3 ;
          prmP008Q3 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008Q4 ;
          prmP008Q4 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008Q5 ;
          prmP008Q5 = new Object[] {
          new Object[] {"@ContagemResultadoIndicadores_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_IndicadorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Solicitada",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Atraso",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultadoIndicadores_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultadoIndicadores_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoIndicadores_Entregue",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008Q2", "SELECT TOP 1 T1.[ContagemResultadoIndicadores_IndicadorCod], T1.[ContagemResultadoIndicadores_DemandaCod] AS ContagemResultadoIndicadores_DemandaCod, T2.[ContagemResultado_StatusDmn], T1.[ContagemResultadoIndicadores_Data], T1.[ContagemResultadoIndicadores_Solicitada], T1.[ContagemResultadoIndicadores_Prazo], T1.[ContagemResultadoIndicadores_Entregue], T1.[ContagemResultadoIndicadores_Atraso], T1.[ContagemResultadoIndicadores_Reduz], T1.[ContagemResultadoIndicadores_Valor] FROM ([ContagemResultadoIndicadores] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoIndicadores_DemandaCod]) WHERE T1.[ContagemResultadoIndicadores_DemandaCod] = @AV8Codigo and T1.[ContagemResultadoIndicadores_IndicadorCod] = @AV12Indicador ORDER BY T1.[ContagemResultadoIndicadores_DemandaCod], T1.[ContagemResultadoIndicadores_IndicadorCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008Q2,1,0,true,true )
             ,new CursorDef("P008Q3", "UPDATE [ContagemResultadoIndicadores] SET [ContagemResultadoIndicadores_Data]=@ContagemResultadoIndicadores_Data, [ContagemResultadoIndicadores_Solicitada]=@ContagemResultadoIndicadores_Solicitada, [ContagemResultadoIndicadores_Prazo]=@ContagemResultadoIndicadores_Prazo, [ContagemResultadoIndicadores_Entregue]=@ContagemResultadoIndicadores_Entregue, [ContagemResultadoIndicadores_Atraso]=@ContagemResultadoIndicadores_Atraso, [ContagemResultadoIndicadores_Reduz]=@ContagemResultadoIndicadores_Reduz, [ContagemResultadoIndicadores_Valor]=@ContagemResultadoIndicadores_Valor  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008Q3)
             ,new CursorDef("P008Q4", "UPDATE [ContagemResultadoIndicadores] SET [ContagemResultadoIndicadores_Data]=@ContagemResultadoIndicadores_Data, [ContagemResultadoIndicadores_Solicitada]=@ContagemResultadoIndicadores_Solicitada, [ContagemResultadoIndicadores_Prazo]=@ContagemResultadoIndicadores_Prazo, [ContagemResultadoIndicadores_Entregue]=@ContagemResultadoIndicadores_Entregue, [ContagemResultadoIndicadores_Atraso]=@ContagemResultadoIndicadores_Atraso, [ContagemResultadoIndicadores_Reduz]=@ContagemResultadoIndicadores_Reduz, [ContagemResultadoIndicadores_Valor]=@ContagemResultadoIndicadores_Valor  WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultadoIndicadores_DemandaCod AND [ContagemResultadoIndicadores_IndicadorCod] = @ContagemResultadoIndicadores_IndicadorCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP008Q4)
             ,new CursorDef("P008Q5", "INSERT INTO [ContagemResultadoIndicadores]([ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod], [ContagemResultadoIndicadores_Data], [ContagemResultadoIndicadores_Solicitada], [ContagemResultadoIndicadores_Prazo], [ContagemResultadoIndicadores_Atraso], [ContagemResultadoIndicadores_Reduz], [ContagemResultadoIndicadores_Valor], [ContagemResultadoIndicadores_LoteCod], [ContagemResultadoIndicadores_Entregue]) VALUES(@ContagemResultadoIndicadores_DemandaCod, @ContagemResultadoIndicadores_IndicadorCod, @ContagemResultadoIndicadores_Data, @ContagemResultadoIndicadores_Solicitada, @ContagemResultadoIndicadores_Prazo, @ContagemResultadoIndicadores_Atraso, @ContagemResultadoIndicadores_Reduz, @ContagemResultadoIndicadores_Valor, @ContagemResultadoIndicadores_LoteCod, @ContagemResultadoIndicadores_Entregue)", GxErrorMask.GX_NOMASK,prmP008Q5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((short[]) buf[8])[0] = rslt.getShort(8) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[10])[0] = rslt.getDecimal(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                return;
             case 2 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (int)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (decimal)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[9]);
                }
                stmt.SetParameter(10, (DateTime)parms[10]);
                return;
       }
    }

 }

}
