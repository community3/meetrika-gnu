/*
               File: GetWWContratoTermoAditivoFilterData
        Description: Get WWContrato Termo Aditivo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:50.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratotermoaditivofilterdata : GXProcedure
   {
      public getwwcontratotermoaditivofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratotermoaditivofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratotermoaditivofilterdata objgetwwcontratotermoaditivofilterdata;
         objgetwwcontratotermoaditivofilterdata = new getwwcontratotermoaditivofilterdata();
         objgetwwcontratotermoaditivofilterdata.AV20DDOName = aP0_DDOName;
         objgetwwcontratotermoaditivofilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwcontratotermoaditivofilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratotermoaditivofilterdata.AV24OptionsJson = "" ;
         objgetwwcontratotermoaditivofilterdata.AV27OptionsDescJson = "" ;
         objgetwwcontratotermoaditivofilterdata.AV29OptionIndexesJson = "" ;
         objgetwwcontratotermoaditivofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratotermoaditivofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratotermoaditivofilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratotermoaditivofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWContratoTermoAditivoGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoTermoAditivoGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWContratoTermoAditivoGridState"), "");
         }
         AV61GXV1 = 1;
         while ( AV61GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV61GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOTERMOADITIVO_DATAINICIO") == 0 )
            {
               AV12TFContratoTermoAditivo_DataInicio = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContratoTermoAditivo_DataInicio_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOTERMOADITIVO_DATAFIM") == 0 )
            {
               AV14TFContratoTermoAditivo_DataFim = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContratoTermoAditivo_DataFim_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
            {
               AV16TFContratoTermoAditivo_DataAssinatura = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContratoTermoAditivo_DataAssinatura_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV61GXV1 = (int)(AV61GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
            {
               AV37ContratoTermoAditivo_DataInicio1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
               AV38ContratoTermoAditivo_DataInicio_To1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
            {
               AV39ContratoTermoAditivo_DataFim1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
               AV40ContratoTermoAditivo_DataFim_To1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
            {
               AV41ContratoTermoAditivo_DataAssinatura1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
               AV42ContratoTermoAditivo_DataAssinatura_To1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
               {
                  AV45ContratoTermoAditivo_DataInicio2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                  AV46ContratoTermoAditivo_DataInicio_To2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
               {
                  AV47ContratoTermoAditivo_DataFim2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                  AV48ContratoTermoAditivo_DataFim_To2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
               {
                  AV49ContratoTermoAditivo_DataAssinatura2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                  AV50ContratoTermoAditivo_DataAssinatura_To2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV51DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV52DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
                  {
                     AV53ContratoTermoAditivo_DataInicio3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                     AV54ContratoTermoAditivo_DataInicio_To3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
                  {
                     AV55ContratoTermoAditivo_DataFim3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                     AV56ContratoTermoAditivo_DataFim_To3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
                  {
                     AV57ContratoTermoAditivo_DataAssinatura3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                     AV58ContratoTermoAditivo_DataAssinatura_To3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV18SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1 = AV37ContratoTermoAditivo_DataInicio1;
         AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1 = AV38ContratoTermoAditivo_DataInicio_To1;
         AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1 = AV39ContratoTermoAditivo_DataFim1;
         AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1 = AV40ContratoTermoAditivo_DataFim_To1;
         AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1 = AV41ContratoTermoAditivo_DataAssinatura1;
         AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1 = AV42ContratoTermoAditivo_DataAssinatura_To1;
         AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2 = AV45ContratoTermoAditivo_DataInicio2;
         AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2 = AV46ContratoTermoAditivo_DataInicio_To2;
         AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2 = AV47ContratoTermoAditivo_DataFim2;
         AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2 = AV48ContratoTermoAditivo_DataFim_To2;
         AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2 = AV49ContratoTermoAditivo_DataAssinatura2;
         AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2 = AV50ContratoTermoAditivo_DataAssinatura_To2;
         AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 = AV51DynamicFiltersEnabled3;
         AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3 = AV52DynamicFiltersSelector3;
         AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3 = AV53ContratoTermoAditivo_DataInicio3;
         AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3 = AV54ContratoTermoAditivo_DataInicio_To3;
         AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3 = AV55ContratoTermoAditivo_DataFim3;
         AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3 = AV56ContratoTermoAditivo_DataFim_To3;
         AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3 = AV57ContratoTermoAditivo_DataAssinatura3;
         AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3 = AV58ContratoTermoAditivo_DataAssinatura_To3;
         AV86WWContratoTermoAditivoDS_24_Tfcontrato_numero = AV10TFContrato_Numero;
         AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio = AV12TFContratoTermoAditivo_DataInicio;
         AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to = AV13TFContratoTermoAditivo_DataInicio_To;
         AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim = AV14TFContratoTermoAditivo_DataFim;
         AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to = AV15TFContratoTermoAditivo_DataFim_To;
         AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura = AV16TFContratoTermoAditivo_DataAssinatura;
         AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to = AV17TFContratoTermoAditivo_DataAssinatura_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1 ,
                                              AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1 ,
                                              AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1 ,
                                              AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1 ,
                                              AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1 ,
                                              AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1 ,
                                              AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1 ,
                                              AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 ,
                                              AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2 ,
                                              AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2 ,
                                              AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2 ,
                                              AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2 ,
                                              AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2 ,
                                              AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2 ,
                                              AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2 ,
                                              AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 ,
                                              AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3 ,
                                              AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3 ,
                                              AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3 ,
                                              AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3 ,
                                              AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3 ,
                                              AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3 ,
                                              AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3 ,
                                              AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel ,
                                              AV86WWContratoTermoAditivoDS_24_Tfcontrato_numero ,
                                              AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio ,
                                              AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to ,
                                              AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim ,
                                              AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to ,
                                              AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura ,
                                              AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to ,
                                              A316ContratoTermoAditivo_DataInicio ,
                                              A317ContratoTermoAditivo_DataFim ,
                                              A318ContratoTermoAditivo_DataAssinatura ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV86WWContratoTermoAditivoDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV86WWContratoTermoAditivoDS_24_Tfcontrato_numero), 20, "%");
         /* Using cursor P00KH2 */
         pr_default.execute(0, new Object[] {AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1, AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1, AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1, AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1, AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1, AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1, AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2, AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2, AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2, AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2, AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2, AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2, AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3, AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3, AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3, AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3, AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3, AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3, lV86WWContratoTermoAditivoDS_24_Tfcontrato_numero, AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel, AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio, AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to, AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim, AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to, AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura, AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKH2 = false;
            A74Contrato_Codigo = P00KH2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KH2_A77Contrato_Numero[0];
            A318ContratoTermoAditivo_DataAssinatura = P00KH2_A318ContratoTermoAditivo_DataAssinatura[0];
            n318ContratoTermoAditivo_DataAssinatura = P00KH2_n318ContratoTermoAditivo_DataAssinatura[0];
            A317ContratoTermoAditivo_DataFim = P00KH2_A317ContratoTermoAditivo_DataFim[0];
            n317ContratoTermoAditivo_DataFim = P00KH2_n317ContratoTermoAditivo_DataFim[0];
            A316ContratoTermoAditivo_DataInicio = P00KH2_A316ContratoTermoAditivo_DataInicio[0];
            n316ContratoTermoAditivo_DataInicio = P00KH2_n316ContratoTermoAditivo_DataInicio[0];
            A315ContratoTermoAditivo_Codigo = P00KH2_A315ContratoTermoAditivo_Codigo[0];
            A77Contrato_Numero = P00KH2_A77Contrato_Numero[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KH2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKKH2 = false;
               A74Contrato_Codigo = P00KH2_A74Contrato_Codigo[0];
               A315ContratoTermoAditivo_Codigo = P00KH2_A315ContratoTermoAditivo_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKKH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV22Option = A77Contrato_Numero;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKH2 )
            {
               BRKKH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoTermoAditivo_DataInicio = DateTime.MinValue;
         AV13TFContratoTermoAditivo_DataInicio_To = DateTime.MinValue;
         AV14TFContratoTermoAditivo_DataFim = DateTime.MinValue;
         AV15TFContratoTermoAditivo_DataFim_To = DateTime.MinValue;
         AV16TFContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         AV17TFContratoTermoAditivo_DataAssinatura_To = DateTime.MinValue;
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37ContratoTermoAditivo_DataInicio1 = DateTime.MinValue;
         AV38ContratoTermoAditivo_DataInicio_To1 = DateTime.MinValue;
         AV39ContratoTermoAditivo_DataFim1 = DateTime.MinValue;
         AV40ContratoTermoAditivo_DataFim_To1 = DateTime.MinValue;
         AV41ContratoTermoAditivo_DataAssinatura1 = DateTime.MinValue;
         AV42ContratoTermoAditivo_DataAssinatura_To1 = DateTime.MinValue;
         AV44DynamicFiltersSelector2 = "";
         AV45ContratoTermoAditivo_DataInicio2 = DateTime.MinValue;
         AV46ContratoTermoAditivo_DataInicio_To2 = DateTime.MinValue;
         AV47ContratoTermoAditivo_DataFim2 = DateTime.MinValue;
         AV48ContratoTermoAditivo_DataFim_To2 = DateTime.MinValue;
         AV49ContratoTermoAditivo_DataAssinatura2 = DateTime.MinValue;
         AV50ContratoTermoAditivo_DataAssinatura_To2 = DateTime.MinValue;
         AV52DynamicFiltersSelector3 = "";
         AV53ContratoTermoAditivo_DataInicio3 = DateTime.MinValue;
         AV54ContratoTermoAditivo_DataInicio_To3 = DateTime.MinValue;
         AV55ContratoTermoAditivo_DataFim3 = DateTime.MinValue;
         AV56ContratoTermoAditivo_DataFim_To3 = DateTime.MinValue;
         AV57ContratoTermoAditivo_DataAssinatura3 = DateTime.MinValue;
         AV58ContratoTermoAditivo_DataAssinatura_To3 = DateTime.MinValue;
         AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1 = "";
         AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1 = DateTime.MinValue;
         AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1 = DateTime.MinValue;
         AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1 = DateTime.MinValue;
         AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1 = DateTime.MinValue;
         AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1 = DateTime.MinValue;
         AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1 = DateTime.MinValue;
         AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2 = "";
         AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2 = DateTime.MinValue;
         AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2 = DateTime.MinValue;
         AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2 = DateTime.MinValue;
         AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2 = DateTime.MinValue;
         AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2 = DateTime.MinValue;
         AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2 = DateTime.MinValue;
         AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3 = "";
         AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3 = DateTime.MinValue;
         AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3 = DateTime.MinValue;
         AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3 = DateTime.MinValue;
         AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3 = DateTime.MinValue;
         AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3 = DateTime.MinValue;
         AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3 = DateTime.MinValue;
         AV86WWContratoTermoAditivoDS_24_Tfcontrato_numero = "";
         AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel = "";
         AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio = DateTime.MinValue;
         AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to = DateTime.MinValue;
         AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim = DateTime.MinValue;
         AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to = DateTime.MinValue;
         AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura = DateTime.MinValue;
         AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to = DateTime.MinValue;
         scmdbuf = "";
         lV86WWContratoTermoAditivoDS_24_Tfcontrato_numero = "";
         A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         A318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         A77Contrato_Numero = "";
         P00KH2_A74Contrato_Codigo = new int[1] ;
         P00KH2_A77Contrato_Numero = new String[] {""} ;
         P00KH2_A318ContratoTermoAditivo_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         P00KH2_n318ContratoTermoAditivo_DataAssinatura = new bool[] {false} ;
         P00KH2_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00KH2_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         P00KH2_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         P00KH2_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         P00KH2_A315ContratoTermoAditivo_Codigo = new int[1] ;
         AV22Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratotermoaditivofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KH2_A74Contrato_Codigo, P00KH2_A77Contrato_Numero, P00KH2_A318ContratoTermoAditivo_DataAssinatura, P00KH2_n318ContratoTermoAditivo_DataAssinatura, P00KH2_A317ContratoTermoAditivo_DataFim, P00KH2_n317ContratoTermoAditivo_DataFim, P00KH2_A316ContratoTermoAditivo_DataInicio, P00KH2_n316ContratoTermoAditivo_DataInicio, P00KH2_A315ContratoTermoAditivo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV61GXV1 ;
      private int A74Contrato_Codigo ;
      private int A315ContratoTermoAditivo_Codigo ;
      private long AV30count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV86WWContratoTermoAditivoDS_24_Tfcontrato_numero ;
      private String AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel ;
      private String scmdbuf ;
      private String lV86WWContratoTermoAditivoDS_24_Tfcontrato_numero ;
      private String A77Contrato_Numero ;
      private DateTime AV12TFContratoTermoAditivo_DataInicio ;
      private DateTime AV13TFContratoTermoAditivo_DataInicio_To ;
      private DateTime AV14TFContratoTermoAditivo_DataFim ;
      private DateTime AV15TFContratoTermoAditivo_DataFim_To ;
      private DateTime AV16TFContratoTermoAditivo_DataAssinatura ;
      private DateTime AV17TFContratoTermoAditivo_DataAssinatura_To ;
      private DateTime AV37ContratoTermoAditivo_DataInicio1 ;
      private DateTime AV38ContratoTermoAditivo_DataInicio_To1 ;
      private DateTime AV39ContratoTermoAditivo_DataFim1 ;
      private DateTime AV40ContratoTermoAditivo_DataFim_To1 ;
      private DateTime AV41ContratoTermoAditivo_DataAssinatura1 ;
      private DateTime AV42ContratoTermoAditivo_DataAssinatura_To1 ;
      private DateTime AV45ContratoTermoAditivo_DataInicio2 ;
      private DateTime AV46ContratoTermoAditivo_DataInicio_To2 ;
      private DateTime AV47ContratoTermoAditivo_DataFim2 ;
      private DateTime AV48ContratoTermoAditivo_DataFim_To2 ;
      private DateTime AV49ContratoTermoAditivo_DataAssinatura2 ;
      private DateTime AV50ContratoTermoAditivo_DataAssinatura_To2 ;
      private DateTime AV53ContratoTermoAditivo_DataInicio3 ;
      private DateTime AV54ContratoTermoAditivo_DataInicio_To3 ;
      private DateTime AV55ContratoTermoAditivo_DataFim3 ;
      private DateTime AV56ContratoTermoAditivo_DataFim_To3 ;
      private DateTime AV57ContratoTermoAditivo_DataAssinatura3 ;
      private DateTime AV58ContratoTermoAditivo_DataAssinatura_To3 ;
      private DateTime AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1 ;
      private DateTime AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1 ;
      private DateTime AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1 ;
      private DateTime AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1 ;
      private DateTime AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1 ;
      private DateTime AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1 ;
      private DateTime AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2 ;
      private DateTime AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2 ;
      private DateTime AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2 ;
      private DateTime AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2 ;
      private DateTime AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2 ;
      private DateTime AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2 ;
      private DateTime AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3 ;
      private DateTime AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3 ;
      private DateTime AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3 ;
      private DateTime AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3 ;
      private DateTime AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3 ;
      private DateTime AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3 ;
      private DateTime AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio ;
      private DateTime AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to ;
      private DateTime AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim ;
      private DateTime AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to ;
      private DateTime AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura ;
      private DateTime AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to ;
      private DateTime A316ContratoTermoAditivo_DataInicio ;
      private DateTime A317ContratoTermoAditivo_DataFim ;
      private DateTime A318ContratoTermoAditivo_DataAssinatura ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV51DynamicFiltersEnabled3 ;
      private bool AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 ;
      private bool AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 ;
      private bool BRKKH2 ;
      private bool n318ContratoTermoAditivo_DataAssinatura ;
      private bool n317ContratoTermoAditivo_DataFim ;
      private bool n316ContratoTermoAditivo_DataInicio ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV52DynamicFiltersSelector3 ;
      private String AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1 ;
      private String AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2 ;
      private String AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3 ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KH2_A74Contrato_Codigo ;
      private String[] P00KH2_A77Contrato_Numero ;
      private DateTime[] P00KH2_A318ContratoTermoAditivo_DataAssinatura ;
      private bool[] P00KH2_n318ContratoTermoAditivo_DataAssinatura ;
      private DateTime[] P00KH2_A317ContratoTermoAditivo_DataFim ;
      private bool[] P00KH2_n317ContratoTermoAditivo_DataFim ;
      private DateTime[] P00KH2_A316ContratoTermoAditivo_DataInicio ;
      private bool[] P00KH2_n316ContratoTermoAditivo_DataInicio ;
      private int[] P00KH2_A315ContratoTermoAditivo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwcontratotermoaditivofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KH2( IGxContext context ,
                                             String AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1 ,
                                             DateTime AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1 ,
                                             DateTime AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1 ,
                                             DateTime AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1 ,
                                             DateTime AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1 ,
                                             DateTime AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1 ,
                                             bool AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 ,
                                             String AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2 ,
                                             DateTime AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2 ,
                                             DateTime AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2 ,
                                             DateTime AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2 ,
                                             DateTime AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2 ,
                                             DateTime AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2 ,
                                             DateTime AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2 ,
                                             bool AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 ,
                                             String AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3 ,
                                             DateTime AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3 ,
                                             DateTime AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3 ,
                                             DateTime AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3 ,
                                             DateTime AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3 ,
                                             DateTime AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3 ,
                                             DateTime AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3 ,
                                             String AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel ,
                                             String AV86WWContratoTermoAditivoDS_24_Tfcontrato_numero ,
                                             DateTime AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio ,
                                             DateTime AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to ,
                                             DateTime AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim ,
                                             DateTime AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to ,
                                             DateTime AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura ,
                                             DateTime AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to ,
                                             DateTime A316ContratoTermoAditivo_DataInicio ,
                                             DateTime A317ContratoTermoAditivo_DataFim ,
                                             DateTime A318ContratoTermoAditivo_DataAssinatura ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [26] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoTermoAditivo_DataAssinatura], T1.[ContratoTermoAditivo_DataFim], T1.[ContratoTermoAditivo_DataInicio], T1.[ContratoTermoAditivo_Codigo] FROM ([ContratoTermoAditivo] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV63WWContratoTermoAditivoDS_1_Dynamicfiltersselector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV70WWContratoTermoAditivoDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWContratoTermoAditivoDS_9_Dynamicfiltersselector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV78WWContratoTermoAditivoDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWContratoTermoAditivoDS_17_Dynamicfiltersselector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWContratoTermoAditivoDS_24_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV86WWContratoTermoAditivoDS_24_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV86WWContratoTermoAditivoDS_24_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KH2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KH2 ;
          prmP00KH2 = new Object[] {
          new Object[] {"@AV64WWContratoTermoAditivoDS_2_Contratotermoaditivo_datainicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV65WWContratoTermoAditivoDS_3_Contratotermoaditivo_datainicio_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV66WWContratoTermoAditivoDS_4_Contratotermoaditivo_datafim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67WWContratoTermoAditivoDS_5_Contratotermoaditivo_datafim_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68WWContratoTermoAditivoDS_6_Contratotermoaditivo_dataassinatura1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV69WWContratoTermoAditivoDS_7_Contratotermoaditivo_dataassinatura_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWContratoTermoAditivoDS_10_Contratotermoaditivo_datainicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWContratoTermoAditivoDS_11_Contratotermoaditivo_datainicio_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74WWContratoTermoAditivoDS_12_Contratotermoaditivo_datafim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75WWContratoTermoAditivoDS_13_Contratotermoaditivo_datafim_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV76WWContratoTermoAditivoDS_14_Contratotermoaditivo_dataassinatura2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV77WWContratoTermoAditivoDS_15_Contratotermoaditivo_dataassinatura_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WWContratoTermoAditivoDS_18_Contratotermoaditivo_datainicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WWContratoTermoAditivoDS_19_Contratotermoaditivo_datainicio_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV82WWContratoTermoAditivoDS_20_Contratotermoaditivo_datafim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV83WWContratoTermoAditivoDS_21_Contratotermoaditivo_datafim_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84WWContratoTermoAditivoDS_22_Contratotermoaditivo_dataassinatura3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWContratoTermoAditivoDS_23_Contratotermoaditivo_dataassinatura_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV86WWContratoTermoAditivoDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV87WWContratoTermoAditivoDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV88WWContratoTermoAditivoDS_26_Tfcontratotermoaditivo_datainicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWContratoTermoAditivoDS_27_Tfcontratotermoaditivo_datainicio_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV90WWContratoTermoAditivoDS_28_Tfcontratotermoaditivo_datafim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV91WWContratoTermoAditivoDS_29_Tfcontratotermoaditivo_datafim_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV92WWContratoTermoAditivoDS_30_Tfcontratotermoaditivo_dataassinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWContratoTermoAditivoDS_31_Tfcontratotermoaditivo_dataassinatura_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KH2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratotermoaditivofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratotermoaditivofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratotermoaditivofilterdata") )
          {
             return  ;
          }
          getwwcontratotermoaditivofilterdata worker = new getwwcontratotermoaditivofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
