/*
               File: PRC_AtualizaValorPF
        Description: Atualiza Valor PF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:29.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_atualizavalorpf : GXProcedure
   {
      public prc_atualizavalorpf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_atualizavalorpf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_AreaTrabalhoCod ,
                           decimal aP1_ValorPF ,
                           int aP2_Servico_Codigo )
      {
         this.A52Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV8ValorPF = aP1_ValorPF;
         this.AV9Servico_Codigo = aP2_Servico_Codigo;
         initialize();
         executePrivate();
         aP0_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
      }

      public void executeSubmit( ref int aP0_Contratada_AreaTrabalhoCod ,
                                 decimal aP1_ValorPF ,
                                 int aP2_Servico_Codigo )
      {
         prc_atualizavalorpf objprc_atualizavalorpf;
         objprc_atualizavalorpf = new prc_atualizavalorpf();
         objprc_atualizavalorpf.A52Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objprc_atualizavalorpf.AV8ValorPF = aP1_ValorPF;
         objprc_atualizavalorpf.AV9Servico_Codigo = aP2_Servico_Codigo;
         objprc_atualizavalorpf.context.SetSubmitInitialConfig(context);
         objprc_atualizavalorpf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_atualizavalorpf);
         aP0_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_atualizavalorpf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00642 */
         pr_default.execute(0, new Object[] {A52Contratada_AreaTrabalhoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A39Contratada_Codigo = P00642_A39Contratada_Codigo[0];
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV9Servico_Codigo ,
                                                 A601ContagemResultado_Servico ,
                                                 A512ContagemResultado_ValorPF ,
                                                 A39Contratada_Codigo ,
                                                 A490ContagemResultado_ContratadaCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00643 */
            pr_default.execute(1, new Object[] {A39Contratada_Codigo, AV9Servico_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00643_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00643_n1553ContagemResultado_CntSrvCod[0];
               A490ContagemResultado_ContratadaCod = P00643_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00643_n490ContagemResultado_ContratadaCod[0];
               A512ContagemResultado_ValorPF = P00643_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P00643_n512ContagemResultado_ValorPF[0];
               A601ContagemResultado_Servico = P00643_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00643_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = P00643_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = P00643_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00643_n601ContagemResultado_Servico[0];
               A512ContagemResultado_ValorPF = AV8ValorPF;
               n512ContagemResultado_ValorPF = false;
               BatchSize = 100;
               pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp00644");
               /* Using cursor P00644 */
               pr_default.addRecord(2, new Object[] {n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, A456ContagemResultado_Codigo});
               if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
               {
                  Executebatchp00644( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(1);
            }
            if ( pr_default.getBatchSize(2) > 0 )
            {
               Executebatchp00644( ) ;
            }
            pr_default.close(1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void Executebatchp00644( )
      {
         /* Using cursor P00644 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AtualizaValorPF");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00642_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00642_A39Contratada_Codigo = new int[1] ;
         P00643_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00643_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00643_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00643_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00643_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00643_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00643_A601ContagemResultado_Servico = new int[1] ;
         P00643_n601ContagemResultado_Servico = new bool[] {false} ;
         P00643_A456ContagemResultado_Codigo = new int[1] ;
         P00644_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00644_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00644_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_atualizavalorpf__default(),
            new Object[][] {
                new Object[] {
               P00642_A52Contratada_AreaTrabalhoCod, P00642_A39Contratada_Codigo
               }
               , new Object[] {
               P00643_A1553ContagemResultado_CntSrvCod, P00643_n1553ContagemResultado_CntSrvCod, P00643_A490ContagemResultado_ContratadaCod, P00643_n490ContagemResultado_ContratadaCod, P00643_A512ContagemResultado_ValorPF, P00643_n512ContagemResultado_ValorPF, P00643_A601ContagemResultado_Servico, P00643_n601ContagemResultado_Servico, P00643_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A52Contratada_AreaTrabalhoCod ;
      private int AV9Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A601ContagemResultado_Servico ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int BatchSize ;
      private decimal AV8ValorPF ;
      private decimal A512ContagemResultado_ValorPF ;
      private String scmdbuf ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n601ContagemResultado_Servico ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00642_A52Contratada_AreaTrabalhoCod ;
      private int[] P00642_A39Contratada_Codigo ;
      private int[] P00643_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00643_n1553ContagemResultado_CntSrvCod ;
      private int[] P00643_A490ContagemResultado_ContratadaCod ;
      private bool[] P00643_n490ContagemResultado_ContratadaCod ;
      private decimal[] P00643_A512ContagemResultado_ValorPF ;
      private bool[] P00643_n512ContagemResultado_ValorPF ;
      private int[] P00643_A601ContagemResultado_Servico ;
      private bool[] P00643_n601ContagemResultado_Servico ;
      private int[] P00643_A456ContagemResultado_Codigo ;
      private decimal[] P00644_A512ContagemResultado_ValorPF ;
      private bool[] P00644_n512ContagemResultado_ValorPF ;
      private int[] P00644_A456ContagemResultado_Codigo ;
   }

   public class prc_atualizavalorpf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00643( IGxContext context ,
                                             int AV9Servico_Codigo ,
                                             int A601ContagemResultado_Servico ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             int A39Contratada_Codigo ,
                                             int A490ContagemResultado_ContratadaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [2] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_ValorPF], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_ContratadaCod] = @Contratada_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ValorPF] = 0 or T1.[ContagemResultado_ValorPF] IS NULL)";
         if ( ! (0==AV9Servico_Codigo) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV9Servico_Codigo)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00643(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (decimal)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00642 ;
          prmP00642 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00644 ;
          prmP00644 = new Object[] {
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00643 ;
          prmP00643 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00642", "SELECT [Contratada_AreaTrabalhoCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @Contratada_AreaTrabalhoCod ORDER BY [Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00642,100,0,true,false )
             ,new CursorDef("P00643", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00643,1,0,true,false )
             ,new CursorDef("P00644", "UPDATE [ContagemResultado] SET [ContagemResultado_ValorPF]=@ContagemResultado_ValorPF  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00644)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
