/*
               File: PRC_AreasDoUsuario
        Description: Areas Do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:45.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_areasdousuario : GXProcedure
   {
      public prc_areasdousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_areasdousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           out IGxCollection aP1_Areas_Codigos )
      {
         this.AV9Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV8Areas_Codigos = new GxSimpleCollection() ;
         initialize();
         executePrivate();
         aP1_Areas_Codigos=this.AV8Areas_Codigos;
      }

      public IGxCollection executeUdp( int aP0_Usuario_Codigo )
      {
         this.AV9Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV8Areas_Codigos = new GxSimpleCollection() ;
         initialize();
         executePrivate();
         aP1_Areas_Codigos=this.AV8Areas_Codigos;
         return AV8Areas_Codigos ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 out IGxCollection aP1_Areas_Codigos )
      {
         prc_areasdousuario objprc_areasdousuario;
         objprc_areasdousuario = new prc_areasdousuario();
         objprc_areasdousuario.AV9Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_areasdousuario.AV8Areas_Codigos = new GxSimpleCollection() ;
         objprc_areasdousuario.context.SetSubmitInitialConfig(context);
         objprc_areasdousuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_areasdousuario);
         aP1_Areas_Codigos=this.AV8Areas_Codigos;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_areasdousuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00XE2 */
         pr_default.execute(0, new Object[] {AV9Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A66ContratadaUsuario_ContratadaCod = P00XE2_A66ContratadaUsuario_ContratadaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00XE2_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P00XE2_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P00XE2_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P00XE2_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P00XE2_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            AV8Areas_Codigos.Add(A1228ContratadaUsuario_AreaTrabalhoCod, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor P00XE4 */
         pr_default.execute(1, new Object[] {AV9Usuario_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = P00XE4_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = P00XE4_A60ContratanteUsuario_UsuarioCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00XE4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00XE4_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00XE4_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00XE4_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            AV8Areas_Codigos.Add(A1020ContratanteUsuario_AreaTrabalhoCod, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XE2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00XE2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00XE2_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P00XE2_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00XE4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00XE4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00XE4_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         P00XE4_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_areasdousuario__default(),
            new Object[][] {
                new Object[] {
               P00XE2_A66ContratadaUsuario_ContratadaCod, P00XE2_A69ContratadaUsuario_UsuarioCod, P00XE2_A1228ContratadaUsuario_AreaTrabalhoCod, P00XE2_n1228ContratadaUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               P00XE4_A63ContratanteUsuario_ContratanteCod, P00XE4_A60ContratanteUsuario_UsuarioCod, P00XE4_A1020ContratanteUsuario_AreaTrabalhoCod, P00XE4_n1020ContratanteUsuario_AreaTrabalhoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Usuario_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private String scmdbuf ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XE2_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00XE2_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00XE2_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P00XE2_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] P00XE4_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00XE4_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00XE4_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] P00XE4_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private IGxCollection aP1_Areas_Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Areas_Codigos ;
   }

   public class prc_areasdousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XE2 ;
          prmP00XE2 = new Object[] {
          new Object[] {"@AV9Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XE4 ;
          prmP00XE4 = new Object[] {
          new Object[] {"@AV9Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XE2", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE T1.[ContratadaUsuario_UsuarioCod] = @AV9Usuario_Codigo ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XE2,100,0,false,false )
             ,new CursorDef("P00XE4", "SELECT T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE T1.[ContratanteUsuario_UsuarioCod] = @AV9Usuario_Codigo ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XE4,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
