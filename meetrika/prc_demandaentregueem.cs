/*
               File: PRC_DemandaEntregueEm
        Description: Demanda Entregue Em
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:6:17.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_demandaentregueem : GXProcedure
   {
      public prc_demandaentregueem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_demandaentregueem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_Contratada ,
                           short aP2_Prazo ,
                           ref DateTime aP3_Previsto ,
                           ref DateTime aP4_Entregue ,
                           ref short aP5_Atraso )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV12Contratada = aP1_Contratada;
         this.AV13Prazo = aP2_Prazo;
         this.AV9Previsto = aP3_Previsto;
         this.AV10Entregue = aP4_Entregue;
         this.AV14Atraso = aP5_Atraso;
         initialize();
         executePrivate();
         aP3_Previsto=this.AV9Previsto;
         aP4_Entregue=this.AV10Entregue;
         aP5_Atraso=this.AV14Atraso;
      }

      public short executeUdp( int aP0_Codigo ,
                               int aP1_Contratada ,
                               short aP2_Prazo ,
                               ref DateTime aP3_Previsto ,
                               ref DateTime aP4_Entregue )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV12Contratada = aP1_Contratada;
         this.AV13Prazo = aP2_Prazo;
         this.AV9Previsto = aP3_Previsto;
         this.AV10Entregue = aP4_Entregue;
         this.AV14Atraso = aP5_Atraso;
         initialize();
         executePrivate();
         aP3_Previsto=this.AV9Previsto;
         aP4_Entregue=this.AV10Entregue;
         aP5_Atraso=this.AV14Atraso;
         return AV14Atraso ;
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_Contratada ,
                                 short aP2_Prazo ,
                                 ref DateTime aP3_Previsto ,
                                 ref DateTime aP4_Entregue ,
                                 ref short aP5_Atraso )
      {
         prc_demandaentregueem objprc_demandaentregueem;
         objprc_demandaentregueem = new prc_demandaentregueem();
         objprc_demandaentregueem.AV8Codigo = aP0_Codigo;
         objprc_demandaentregueem.AV12Contratada = aP1_Contratada;
         objprc_demandaentregueem.AV13Prazo = aP2_Prazo;
         objprc_demandaentregueem.AV9Previsto = aP3_Previsto;
         objprc_demandaentregueem.AV10Entregue = aP4_Entregue;
         objprc_demandaentregueem.AV14Atraso = aP5_Atraso;
         objprc_demandaentregueem.context.SetSubmitInitialConfig(context);
         objprc_demandaentregueem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_demandaentregueem);
         aP3_Previsto=this.AV9Previsto;
         aP4_Entregue=this.AV10Entregue;
         aP5_Atraso=this.AV14Atraso;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_demandaentregueem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10Entregue = DateTime.MinValue;
         AV19GXLvl4 = 0;
         /* Using cursor P008P2 */
         pr_default.execute(0, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P008P2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P008P2_n1553ContagemResultado_CntSrvCod[0];
            A1349ContagemResultado_DataExecucao = P008P2_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P008P2_n1349ContagemResultado_DataExecucao[0];
            A1351ContagemResultado_DataPrevista = P008P2_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P008P2_n1351ContagemResultado_DataPrevista[0];
            A456ContagemResultado_Codigo = P008P2_A456ContagemResultado_Codigo[0];
            A1611ContagemResultado_PrzTpDias = P008P2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008P2_n1611ContagemResultado_PrzTpDias[0];
            A1611ContagemResultado_PrzTpDias = P008P2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P008P2_n1611ContagemResultado_PrzTpDias[0];
            AV19GXLvl4 = 1;
            AV9Previsto = DateTimeUtil.ResetTime(A1351ContagemResultado_DataPrevista);
            AV10Entregue = DateTimeUtil.ResetTime(A1349ContagemResultado_DataExecucao);
            AV16TipoDias = A1611ContagemResultado_PrzTpDias;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( AV19GXLvl4 == 0 )
         {
            AV20GXLvl13 = 0;
            /* Using cursor P008P3 */
            pr_default.execute(1, new Object[] {AV8Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P008P3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008P3_n1553ContagemResultado_CntSrvCod[0];
               A894LogResponsavel_Acao = P008P3_A894LogResponsavel_Acao[0];
               A892LogResponsavel_DemandaCod = P008P3_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = P008P3_n892LogResponsavel_DemandaCod[0];
               A1177LogResponsavel_Prazo = P008P3_A1177LogResponsavel_Prazo[0];
               n1177LogResponsavel_Prazo = P008P3_n1177LogResponsavel_Prazo[0];
               A1611ContagemResultado_PrzTpDias = P008P3_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008P3_n1611ContagemResultado_PrzTpDias[0];
               A893LogResponsavel_DataHora = P008P3_A893LogResponsavel_DataHora[0];
               A1797LogResponsavel_Codigo = P008P3_A1797LogResponsavel_Codigo[0];
               A1553ContagemResultado_CntSrvCod = P008P3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P008P3_n1553ContagemResultado_CntSrvCod[0];
               A1611ContagemResultado_PrzTpDias = P008P3_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P008P3_n1611ContagemResultado_PrzTpDias[0];
               AV20GXLvl13 = 1;
               AV9Previsto = DateTimeUtil.ResetTime(A1177LogResponsavel_Prazo);
               AV16TipoDias = A1611ContagemResultado_PrzTpDias;
               if ( (DateTime.MinValue==AV10Entregue) || ( A893LogResponsavel_DataHora < AV10Entregue ) )
               {
                  AV10Entregue = DateTimeUtil.ResetTime(A893LogResponsavel_DataHora);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( AV20GXLvl13 == 0 )
            {
               /* Using cursor P008P4 */
               pr_default.execute(2, new Object[] {AV8Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1553ContagemResultado_CntSrvCod = P008P4_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P008P4_n1553ContagemResultado_CntSrvCod[0];
                  A483ContagemResultado_StatusCnt = P008P4_A483ContagemResultado_StatusCnt[0];
                  A456ContagemResultado_Codigo = P008P4_A456ContagemResultado_Codigo[0];
                  A1611ContagemResultado_PrzTpDias = P008P4_A1611ContagemResultado_PrzTpDias[0];
                  n1611ContagemResultado_PrzTpDias = P008P4_n1611ContagemResultado_PrzTpDias[0];
                  A471ContagemResultado_DataDmn = P008P4_A471ContagemResultado_DataDmn[0];
                  A473ContagemResultado_DataCnt = P008P4_A473ContagemResultado_DataCnt[0];
                  A511ContagemResultado_HoraCnt = P008P4_A511ContagemResultado_HoraCnt[0];
                  A1553ContagemResultado_CntSrvCod = P008P4_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P008P4_n1553ContagemResultado_CntSrvCod[0];
                  A471ContagemResultado_DataDmn = P008P4_A471ContagemResultado_DataDmn[0];
                  A1611ContagemResultado_PrzTpDias = P008P4_A1611ContagemResultado_PrzTpDias[0];
                  n1611ContagemResultado_PrzTpDias = P008P4_n1611ContagemResultado_PrzTpDias[0];
                  AV16TipoDias = A1611ContagemResultado_PrzTpDias;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV9Previsto ) ;
                  GXt_dtime2 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
                  new prc_adddiasuteis(context ).execute(  GXt_dtime2,  AV13Prazo,  AV16TipoDias, out  GXt_dtime1) ;
                  AV9Previsto = DateTimeUtil.ResetTime(GXt_dtime1);
                  if ( (DateTime.MinValue==AV10Entregue) || ( A473ContagemResultado_DataCnt < AV10Entregue ) )
                  {
                     AV10Entregue = A473ContagemResultado_DataCnt;
                  }
                  pr_default.readNext(2);
               }
               pr_default.close(2);
            }
         }
         if ( ! ( (DateTime.MinValue==AV9Previsto) || (DateTime.MinValue==AV10Entregue) ) )
         {
            AV14Atraso = 0;
            if ( AV10Entregue > AV9Previsto )
            {
               GXt_int3 = AV14Atraso;
               new prc_diasuteisentre(context ).execute(  AV9Previsto,  AV10Entregue,  AV16TipoDias, out  GXt_int3) ;
               AV14Atraso = GXt_int3;
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P008P2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008P2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008P2_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P008P2_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P008P2_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P008P2_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P008P2_A456ContagemResultado_Codigo = new int[1] ;
         P008P2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008P2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         AV16TipoDias = "";
         P008P3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008P3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008P3_A894LogResponsavel_Acao = new String[] {""} ;
         P008P3_A892LogResponsavel_DemandaCod = new int[1] ;
         P008P3_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P008P3_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P008P3_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P008P3_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008P3_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008P3_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P008P3_A1797LogResponsavel_Codigo = new long[1] ;
         A894LogResponsavel_Acao = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         P008P4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P008P4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P008P4_A483ContagemResultado_StatusCnt = new short[1] ;
         P008P4_A456ContagemResultado_Codigo = new int[1] ;
         P008P4_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P008P4_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P008P4_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P008P4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P008P4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_demandaentregueem__default(),
            new Object[][] {
                new Object[] {
               P008P2_A1553ContagemResultado_CntSrvCod, P008P2_n1553ContagemResultado_CntSrvCod, P008P2_A1349ContagemResultado_DataExecucao, P008P2_n1349ContagemResultado_DataExecucao, P008P2_A1351ContagemResultado_DataPrevista, P008P2_n1351ContagemResultado_DataPrevista, P008P2_A456ContagemResultado_Codigo, P008P2_A1611ContagemResultado_PrzTpDias, P008P2_n1611ContagemResultado_PrzTpDias
               }
               , new Object[] {
               P008P3_A1553ContagemResultado_CntSrvCod, P008P3_n1553ContagemResultado_CntSrvCod, P008P3_A894LogResponsavel_Acao, P008P3_A892LogResponsavel_DemandaCod, P008P3_n892LogResponsavel_DemandaCod, P008P3_A1177LogResponsavel_Prazo, P008P3_n1177LogResponsavel_Prazo, P008P3_A1611ContagemResultado_PrzTpDias, P008P3_n1611ContagemResultado_PrzTpDias, P008P3_A893LogResponsavel_DataHora,
               P008P3_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P008P4_A1553ContagemResultado_CntSrvCod, P008P4_n1553ContagemResultado_CntSrvCod, P008P4_A483ContagemResultado_StatusCnt, P008P4_A456ContagemResultado_Codigo, P008P4_A1611ContagemResultado_PrzTpDias, P008P4_n1611ContagemResultado_PrzTpDias, P008P4_A471ContagemResultado_DataDmn, P008P4_A473ContagemResultado_DataCnt, P008P4_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV13Prazo ;
      private short AV14Atraso ;
      private short AV19GXLvl4 ;
      private short AV20GXLvl13 ;
      private short A483ContagemResultado_StatusCnt ;
      private short GXt_int3 ;
      private int AV8Codigo ;
      private int AV12Contratada ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A892LogResponsavel_DemandaCod ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV16TipoDias ;
      private String A894LogResponsavel_Acao ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime GXt_dtime1 ;
      private DateTime GXt_dtime2 ;
      private DateTime AV9Previsto ;
      private DateTime AV10Entregue ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1177LogResponsavel_Prazo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private DateTime aP3_Previsto ;
      private DateTime aP4_Entregue ;
      private short aP5_Atraso ;
      private IDataStoreProvider pr_default ;
      private int[] P008P2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008P2_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P008P2_A1349ContagemResultado_DataExecucao ;
      private bool[] P008P2_n1349ContagemResultado_DataExecucao ;
      private DateTime[] P008P2_A1351ContagemResultado_DataPrevista ;
      private bool[] P008P2_n1351ContagemResultado_DataPrevista ;
      private int[] P008P2_A456ContagemResultado_Codigo ;
      private String[] P008P2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008P2_n1611ContagemResultado_PrzTpDias ;
      private int[] P008P3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008P3_n1553ContagemResultado_CntSrvCod ;
      private String[] P008P3_A894LogResponsavel_Acao ;
      private int[] P008P3_A892LogResponsavel_DemandaCod ;
      private bool[] P008P3_n892LogResponsavel_DemandaCod ;
      private DateTime[] P008P3_A1177LogResponsavel_Prazo ;
      private bool[] P008P3_n1177LogResponsavel_Prazo ;
      private String[] P008P3_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008P3_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P008P3_A893LogResponsavel_DataHora ;
      private long[] P008P3_A1797LogResponsavel_Codigo ;
      private int[] P008P4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P008P4_n1553ContagemResultado_CntSrvCod ;
      private short[] P008P4_A483ContagemResultado_StatusCnt ;
      private int[] P008P4_A456ContagemResultado_Codigo ;
      private String[] P008P4_A1611ContagemResultado_PrzTpDias ;
      private bool[] P008P4_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P008P4_A471ContagemResultado_DataDmn ;
      private DateTime[] P008P4_A473ContagemResultado_DataCnt ;
      private String[] P008P4_A511ContagemResultado_HoraCnt ;
   }

   public class prc_demandaentregueem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP008P2 ;
          prmP008P2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008P3 ;
          prmP008P3 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP008P4 ;
          prmP008P4 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P008P2", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_Codigo], T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV8Codigo) AND (Not (T1.[ContagemResultado_DataPrevista] = convert( DATETIME, '17530101', 112 ))) AND (Not (T1.[ContagemResultado_DataExecucao] = convert( DATETIME, '17530101', 112 ))) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008P2,1,0,false,true )
             ,new CursorDef("P008P3", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[LogResponsavel_Acao], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Prazo], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[LogResponsavel_DataHora], T1.[LogResponsavel_Codigo] FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV8Codigo) AND (T1.[LogResponsavel_Acao] = 'D' or T1.[LogResponsavel_Acao] = 'V') ORDER BY T1.[LogResponsavel_DemandaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008P3,100,0,false,false )
             ,new CursorDef("P008P4", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Codigo], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T2.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Codigo] = @AV8Codigo) AND (T1.[ContagemResultado_StatusCnt] = 7 or T1.[ContagemResultado_StatusCnt] = 5) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP008P4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
