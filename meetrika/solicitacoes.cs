/*
               File: Solicitacoes
        Description: Solicitacoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:21:3.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacoes : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATADA_CODIGO") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV7WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATADA_CODIGO1S67( AV7WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASERVICO_CODIGO1S67( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_28") == 0 )
         {
            A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_28( A39Contratada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_26") == 0 )
         {
            A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_26( A127Sistema_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_27") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_27( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_29") == 0 )
         {
            A442Solicitacoes_Usuario = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_29( A442Solicitacoes_Usuario) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_30") == 0 )
         {
            A443Solicitacoes_Usuario_Ult = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_30( A443Solicitacoes_Usuario_Ult) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV17Solicitacoes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Solicitacoes_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITACOES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17Solicitacoes_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContratada_Codigo.Name = "CONTRATADA_CODIGO";
         dynContratada_Codigo.WebTags = "";
         dynServico_Codigo.Name = "SERVICO_CODIGO";
         dynServico_Codigo.WebTags = "";
         radSolicitacoes_Novo_Projeto.Name = "SOLICITACOES_NOVO_PROJETO";
         radSolicitacoes_Novo_Projeto.WebTags = "";
         radSolicitacoes_Novo_Projeto.addItem("1", "Sim", 0);
         radSolicitacoes_Novo_Projeto.addItem("0", "N�o", 0);
         cmbSolicitacoes_Status.Name = "SOLICITACOES_STATUS";
         cmbSolicitacoes_Status.WebTags = "";
         cmbSolicitacoes_Status.addItem("A", "Ativo", 0);
         cmbSolicitacoes_Status.addItem("E", "Excluido", 0);
         cmbSolicitacoes_Status.addItem("R", "Rascunho", 0);
         if ( cmbSolicitacoes_Status.ItemCount > 0 )
         {
            A446Solicitacoes_Status = cmbSolicitacoes_Status.getValidValue(A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Solicitacoes", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynContratada_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public solicitacoes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacoes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Solicitacoes_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV17Solicitacoes_Codigo = aP1_Solicitacoes_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContratada_Codigo = new GXCombobox();
         dynServico_Codigo = new GXCombobox();
         radSolicitacoes_Novo_Projeto = new GXRadio();
         cmbSolicitacoes_Status = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContratada_Codigo.ItemCount > 0 )
         {
            A39Contratada_Codigo = (int)(NumberUtil.Val( dynContratada_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
         if ( dynServico_Codigo.ItemCount > 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( dynServico_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         if ( cmbSolicitacoes_Status.ItemCount > 0 )
         {
            A446Solicitacoes_Status = cmbSolicitacoes_Status.getValidValue(A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1S67( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1S67e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1S67( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1S67( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1S67e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_71_1S67( true) ;
         }
         return  ;
      }

      protected void wb_table3_71_1S67e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1S67e( true) ;
         }
         else
         {
            wb_table1_2_1S67e( false) ;
         }
      }

      protected void wb_table3_71_1S67( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_71_1S67e( true) ;
         }
         else
         {
            wb_table3_71_1S67e( false) ;
         }
      }

      protected void wb_table2_5_1S67( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1S67( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1S67e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1S67e( true) ;
         }
         else
         {
            wb_table2_5_1S67e( false) ;
         }
      }

      protected void wb_table4_13_1S67( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_codigo_Internalname, "Solicitacoes_Codigo", "", "", lblTextblocksolicitacoes_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ",", "")), ((edtSolicitacoes_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicitacoes_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_codigo_Internalname, "Contratada", "", "", lblTextblockcontratada_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratada_Codigo, dynContratada_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)), 1, dynContratada_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratada_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_Solicitacoes.htm");
            dynContratada_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_Codigo_Internalname, "Values", (String)(dynContratada_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_codigo_Internalname, "Sistema", "", "", lblTextblocksistema_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistema_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_codigo_Internalname, "Servi�o", "", "", lblTextblockservico_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_Codigo, dynServico_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)), 1, dynServico_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServico_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_Solicitacoes.htm");
            dynServico_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Codigo_Internalname, "Values", (String)(dynServico_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_novo_projeto_Internalname, "Projeto", "", "", lblTextblocksolicitacoes_novo_projeto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_radio_ctrl( context, radSolicitacoes_Novo_Projeto, radSolicitacoes_Novo_Projeto_Internalname, StringUtil.RTrim( A440Solicitacoes_Novo_Projeto), "", 1, radSolicitacoes_Novo_Projeto.Enabled, 0, 0, StyleString, ClassString, "", 0, radSolicitacoes_Novo_Projeto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_objetivo_Internalname, "Objetivo", "", "", lblTextblocksolicitacoes_objetivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSolicitacoes_Objetivo_Internalname, A441Solicitacoes_Objetivo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", 0, 1, edtSolicitacoes_Objetivo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_usuario_Internalname, "da Solicita��es", "", "", lblTextblocksolicitacoes_usuario_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Usuario_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A442Solicitacoes_Usuario), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Usuario_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicitacoes_Usuario_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_data_Internalname, "de inclus�o", "", "", lblTextblocksolicitacoes_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtSolicitacoes_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Data_Internalname, context.localUtil.TToC( A444Solicitacoes_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtSolicitacoes_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Solicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtSolicitacoes_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtSolicitacoes_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Solicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_usuario_ult_Internalname, "Ultima Modifica��o", "", "", lblTextblocksolicitacoes_usuario_ult_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Usuario_Ult_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Usuario_Ult_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSolicitacoes_Usuario_Ult_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_data_ult_Internalname, "Ultima Modifica��o", "", "", lblTextblocksolicitacoes_data_ult_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtSolicitacoes_Data_Ult_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSolicitacoes_Data_Ult_Internalname, context.localUtil.TToC( A445Solicitacoes_Data_Ult, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSolicitacoes_Data_Ult_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtSolicitacoes_Data_Ult_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Solicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtSolicitacoes_Data_Ult_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtSolicitacoes_Data_Ult_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Solicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksolicitacoes_status_Internalname, "Solicitacoes_Status", "", "", lblTextblocksolicitacoes_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Solicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSolicitacoes_Status, cmbSolicitacoes_Status_Internalname, StringUtil.RTrim( A446Solicitacoes_Status), 1, cmbSolicitacoes_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbSolicitacoes_Status.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_Solicitacoes.htm");
            cmbSolicitacoes_Status.CurrentValue = StringUtil.RTrim( A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicitacoes_Status_Internalname, "Values", (String)(cmbSolicitacoes_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1S67e( true) ;
         }
         else
         {
            wb_table4_13_1S67e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111S2 */
         E111S2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
               dynContratada_Codigo.CurrentValue = cgiGet( dynContratada_Codigo_Internalname);
               A39Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynContratada_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A127Sistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               }
               else
               {
                  A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               }
               dynServico_Codigo.CurrentValue = cgiGet( dynServico_Codigo_Internalname);
               A155Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynServico_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A440Solicitacoes_Novo_Projeto = cgiGet( radSolicitacoes_Novo_Projeto_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A440Solicitacoes_Novo_Projeto", A440Solicitacoes_Novo_Projeto);
               A441Solicitacoes_Objetivo = cgiGet( edtSolicitacoes_Objetivo_Internalname);
               n441Solicitacoes_Objetivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A441Solicitacoes_Objetivo", A441Solicitacoes_Objetivo);
               n441Solicitacoes_Objetivo = (String.IsNullOrEmpty(StringUtil.RTrim( A441Solicitacoes_Objetivo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICITACOES_USUARIO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoes_Usuario_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A442Solicitacoes_Usuario = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
               }
               else
               {
                  A442Solicitacoes_Usuario = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtSolicitacoes_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data de inclus�o"}), 1, "SOLICITACOES_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoes_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A444Solicitacoes_Data", context.localUtil.TToC( A444Solicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A444Solicitacoes_Data = context.localUtil.CToT( cgiGet( edtSolicitacoes_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A444Solicitacoes_Data", context.localUtil.TToC( A444Solicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Ult_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Ult_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SOLICITACOES_USUARIO_ULT");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoes_Usuario_Ult_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A443Solicitacoes_Usuario_Ult = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
               }
               else
               {
                  A443Solicitacoes_Usuario_Ult = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Ult_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtSolicitacoes_Data_Ult_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data da Ultima Modifica��o"}), 1, "SOLICITACOES_DATA_ULT");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoes_Data_Ult_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A445Solicitacoes_Data_Ult", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A445Solicitacoes_Data_Ult = context.localUtil.CToT( cgiGet( edtSolicitacoes_Data_Ult_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A445Solicitacoes_Data_Ult", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
               }
               cmbSolicitacoes_Status.CurrentValue = cgiGet( cmbSolicitacoes_Status_Internalname);
               A446Solicitacoes_Status = cgiGet( cmbSolicitacoes_Status_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
               /* Read saved values. */
               Z439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z439Solicitacoes_Codigo"), ",", "."));
               Z440Solicitacoes_Novo_Projeto = cgiGet( "Z440Solicitacoes_Novo_Projeto");
               Z444Solicitacoes_Data = context.localUtil.CToT( cgiGet( "Z444Solicitacoes_Data"), 0);
               Z445Solicitacoes_Data_Ult = context.localUtil.CToT( cgiGet( "Z445Solicitacoes_Data_Ult"), 0);
               Z446Solicitacoes_Status = cgiGet( "Z446Solicitacoes_Status");
               Z127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z127Sistema_Codigo"), ",", "."));
               Z155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z155Servico_Codigo"), ",", "."));
               Z39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z39Contratada_Codigo"), ",", "."));
               Z442Solicitacoes_Usuario = (int)(context.localUtil.CToN( cgiGet( "Z442Solicitacoes_Usuario"), ",", "."));
               Z443Solicitacoes_Usuario_Ult = (int)(context.localUtil.CToN( cgiGet( "Z443Solicitacoes_Usuario_Ult"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "N39Contratada_Codigo"), ",", "."));
               N127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "N127Sistema_Codigo"), ",", "."));
               N155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "N155Servico_Codigo"), ",", "."));
               N442Solicitacoes_Usuario = (int)(context.localUtil.CToN( cgiGet( "N442Solicitacoes_Usuario"), ",", "."));
               N443Solicitacoes_Usuario_Ult = (int)(context.localUtil.CToN( cgiGet( "N443Solicitacoes_Usuario_Ult"), ",", "."));
               AV17Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSOLICITACOES_CODIGO"), ",", "."));
               AV10Insert_Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATADA_CODIGO"), ",", "."));
               AV16Insert_Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMA_CODIGO"), ",", "."));
               AV11Insert_Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICO_CODIGO"), ",", "."));
               AV13Insert_Solicitacoes_Usuario = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SOLICITACOES_USUARIO"), ",", "."));
               AV14Insert_Solicitacoes_Usuario_Ult = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SOLICITACOES_USUARIO_ULT"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV7WWPContext);
               AV18Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Solicitacoes";
               A439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("solicitacoes:[SecurityCheckFailed value for]"+"Solicitacoes_Codigo:"+context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("solicitacoes:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A439Solicitacoes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode67 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode67;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound67 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1S0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SOLICITACOES_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111S2 */
                           E111S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121S2 */
                           E121S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121S2 */
            E121S2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1S67( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1S67( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1S0( )
      {
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1S67( ) ;
            }
            else
            {
               CheckExtendedTable1S67( ) ;
               CloseExtendedTableCursors1S67( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1S0( )
      {
      }

      protected void E111S2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV8TrnContext.FromXml(AV9WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Transactionname, AV18Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV19GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            while ( AV19GXV1 <= AV8TrnContext.gxTpr_Attributes.Count )
            {
               AV15TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV8TrnContext.gxTpr_Attributes.Item(AV19GXV1));
               if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Contratada_Codigo") == 0 )
               {
                  AV10Insert_Contratada_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Insert_Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Insert_Contratada_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Sistema_Codigo") == 0 )
               {
                  AV16Insert_Sistema_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_Sistema_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Servico_Codigo") == 0 )
               {
                  AV11Insert_Servico_Codigo = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Servico_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Solicitacoes_Usuario") == 0 )
               {
                  AV13Insert_Solicitacoes_Usuario = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Solicitacoes_Usuario), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV15TrnContextAtt.gxTpr_Attributename, "Solicitacoes_Usuario_Ult") == 0 )
               {
                  AV14Insert_Solicitacoes_Usuario_Ult = (int)(NumberUtil.Val( AV15TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_Solicitacoes_Usuario_Ult), 6, 0)));
               }
               AV19GXV1 = (int)(AV19GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            }
         }
      }

      protected void E121S2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV8TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwsolicitacoes.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1S67( short GX_JID )
      {
         if ( ( GX_JID == 25 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z440Solicitacoes_Novo_Projeto = T001S3_A440Solicitacoes_Novo_Projeto[0];
               Z444Solicitacoes_Data = T001S3_A444Solicitacoes_Data[0];
               Z445Solicitacoes_Data_Ult = T001S3_A445Solicitacoes_Data_Ult[0];
               Z446Solicitacoes_Status = T001S3_A446Solicitacoes_Status[0];
               Z127Sistema_Codigo = T001S3_A127Sistema_Codigo[0];
               Z155Servico_Codigo = T001S3_A155Servico_Codigo[0];
               Z39Contratada_Codigo = T001S3_A39Contratada_Codigo[0];
               Z442Solicitacoes_Usuario = T001S3_A442Solicitacoes_Usuario[0];
               Z443Solicitacoes_Usuario_Ult = T001S3_A443Solicitacoes_Usuario_Ult[0];
            }
            else
            {
               Z440Solicitacoes_Novo_Projeto = A440Solicitacoes_Novo_Projeto;
               Z444Solicitacoes_Data = A444Solicitacoes_Data;
               Z445Solicitacoes_Data_Ult = A445Solicitacoes_Data_Ult;
               Z446Solicitacoes_Status = A446Solicitacoes_Status;
               Z127Sistema_Codigo = A127Sistema_Codigo;
               Z155Servico_Codigo = A155Servico_Codigo;
               Z39Contratada_Codigo = A39Contratada_Codigo;
               Z442Solicitacoes_Usuario = A442Solicitacoes_Usuario;
               Z443Solicitacoes_Usuario_Ult = A443Solicitacoes_Usuario_Ult;
            }
         }
         if ( GX_JID == -25 )
         {
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
            Z440Solicitacoes_Novo_Projeto = A440Solicitacoes_Novo_Projeto;
            Z441Solicitacoes_Objetivo = A441Solicitacoes_Objetivo;
            Z444Solicitacoes_Data = A444Solicitacoes_Data;
            Z445Solicitacoes_Data_Ult = A445Solicitacoes_Data_Ult;
            Z446Solicitacoes_Status = A446Solicitacoes_Status;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z155Servico_Codigo = A155Servico_Codigo;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z442Solicitacoes_Usuario = A442Solicitacoes_Usuario;
            Z443Solicitacoes_Usuario_Ult = A443Solicitacoes_Usuario_Ult;
         }
      }

      protected void standaloneNotModal( )
      {
         GXASERVICO_CODIGO_html1S67( ) ;
         edtSolicitacoes_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Codigo_Enabled), 5, 0)));
         AV18Pgmname = "Solicitacoes";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Pgmname", AV18Pgmname);
         edtSolicitacoes_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV17Solicitacoes_Codigo) )
         {
            A439Solicitacoes_Codigo = AV17Solicitacoes_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
         }
         GXACONTRATADA_CODIGO_html1S67( AV7WWPContext) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV10Insert_Contratada_Codigo) )
         {
            dynContratada_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratada_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynContratada_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratada_Codigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_Sistema_Codigo) )
         {
            edtSistema_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtSistema_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Servico_Codigo) )
         {
            dynServico_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynServico_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Codigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Solicitacoes_Usuario) )
         {
            edtSolicitacoes_Usuario_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Usuario_Enabled), 5, 0)));
         }
         else
         {
            edtSolicitacoes_Usuario_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Usuario_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_Solicitacoes_Usuario_Ult) )
         {
            edtSolicitacoes_Usuario_Ult_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Ult_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Usuario_Ult_Enabled), 5, 0)));
         }
         else
         {
            edtSolicitacoes_Usuario_Ult_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Ult_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Usuario_Ult_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_Solicitacoes_Usuario_Ult) )
         {
            A443Solicitacoes_Usuario_Ult = AV14Insert_Solicitacoes_Usuario_Ult;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Solicitacoes_Usuario) )
         {
            A442Solicitacoes_Usuario = AV13Insert_Solicitacoes_Usuario;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Servico_Codigo) )
         {
            A155Servico_Codigo = AV11Insert_Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_Sistema_Codigo) )
         {
            A127Sistema_Codigo = AV16Insert_Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV10Insert_Contratada_Codigo) )
         {
            A39Contratada_Codigo = AV10Insert_Contratada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load1S67( )
      {
         /* Using cursor T001S9 */
         pr_default.execute(7, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound67 = 1;
            A440Solicitacoes_Novo_Projeto = T001S9_A440Solicitacoes_Novo_Projeto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A440Solicitacoes_Novo_Projeto", A440Solicitacoes_Novo_Projeto);
            A441Solicitacoes_Objetivo = T001S9_A441Solicitacoes_Objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A441Solicitacoes_Objetivo", A441Solicitacoes_Objetivo);
            n441Solicitacoes_Objetivo = T001S9_n441Solicitacoes_Objetivo[0];
            A444Solicitacoes_Data = T001S9_A444Solicitacoes_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A444Solicitacoes_Data", context.localUtil.TToC( A444Solicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
            A445Solicitacoes_Data_Ult = T001S9_A445Solicitacoes_Data_Ult[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A445Solicitacoes_Data_Ult", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
            A446Solicitacoes_Status = T001S9_A446Solicitacoes_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
            A127Sistema_Codigo = T001S9_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A155Servico_Codigo = T001S9_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A39Contratada_Codigo = T001S9_A39Contratada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            A442Solicitacoes_Usuario = T001S9_A442Solicitacoes_Usuario[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
            A443Solicitacoes_Usuario_Ult = T001S9_A443Solicitacoes_Usuario_Ult[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
            ZM1S67( -25) ;
         }
         pr_default.close(7);
         OnLoadActions1S67( ) ;
      }

      protected void OnLoadActions1S67( )
      {
      }

      protected void CheckExtendedTable1S67( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T001S6 */
         pr_default.execute(4, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynContratada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(4);
         /* Using cursor T001S4 */
         pr_default.execute(2, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T001S5 */
         pr_default.execute(3, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         /* Using cursor T001S7 */
         pr_default.execute(5, new Object[] {A442Solicitacoes_Usuario});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Solicitacoes_Usuario'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Usuario_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(5);
         if ( ! ( (DateTime.MinValue==A444Solicitacoes_Data) || ( A444Solicitacoes_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data de inclus�o fora do intervalo", "OutOfRange", 1, "SOLICITACOES_DATA");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001S8 */
         pr_default.execute(6, new Object[] {A443Solicitacoes_Usuario_Ult});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes_Usuario Ult'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO_ULT");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Usuario_Ult_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(6);
         if ( ! ( (DateTime.MinValue==A445Solicitacoes_Data_Ult) || ( A445Solicitacoes_Data_Ult >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data da Ultima Modifica��o fora do intervalo", "OutOfRange", 1, "SOLICITACOES_DATA_ULT");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Data_Ult_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A446Solicitacoes_Status, "A") == 0 ) || ( StringUtil.StrCmp(A446Solicitacoes_Status, "E") == 0 ) || ( StringUtil.StrCmp(A446Solicitacoes_Status, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Solicitacoes_Status fora do intervalo", "OutOfRange", 1, "SOLICITACOES_STATUS");
            AnyError = 1;
            GX_FocusControl = cmbSolicitacoes_Status_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors1S67( )
      {
         pr_default.close(4);
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_28( int A39Contratada_Codigo )
      {
         /* Using cursor T001S10 */
         pr_default.execute(8, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynContratada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_26( int A127Sistema_Codigo )
      {
         /* Using cursor T001S11 */
         pr_default.execute(9, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_27( int A155Servico_Codigo )
      {
         /* Using cursor T001S12 */
         pr_default.execute(10, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_29( int A442Solicitacoes_Usuario )
      {
         /* Using cursor T001S13 */
         pr_default.execute(11, new Object[] {A442Solicitacoes_Usuario});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Solicitacoes_Usuario'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Usuario_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_30( int A443Solicitacoes_Usuario_Ult )
      {
         /* Using cursor T001S14 */
         pr_default.execute(12, new Object[] {A443Solicitacoes_Usuario_Ult});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes_Usuario Ult'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO_ULT");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Usuario_Ult_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey1S67( )
      {
         /* Using cursor T001S15 */
         pr_default.execute(13, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound67 = 1;
         }
         else
         {
            RcdFound67 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001S3 */
         pr_default.execute(1, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1S67( 25) ;
            RcdFound67 = 1;
            A439Solicitacoes_Codigo = T001S3_A439Solicitacoes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
            A440Solicitacoes_Novo_Projeto = T001S3_A440Solicitacoes_Novo_Projeto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A440Solicitacoes_Novo_Projeto", A440Solicitacoes_Novo_Projeto);
            A441Solicitacoes_Objetivo = T001S3_A441Solicitacoes_Objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A441Solicitacoes_Objetivo", A441Solicitacoes_Objetivo);
            n441Solicitacoes_Objetivo = T001S3_n441Solicitacoes_Objetivo[0];
            A444Solicitacoes_Data = T001S3_A444Solicitacoes_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A444Solicitacoes_Data", context.localUtil.TToC( A444Solicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
            A445Solicitacoes_Data_Ult = T001S3_A445Solicitacoes_Data_Ult[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A445Solicitacoes_Data_Ult", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
            A446Solicitacoes_Status = T001S3_A446Solicitacoes_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
            A127Sistema_Codigo = T001S3_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A155Servico_Codigo = T001S3_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A39Contratada_Codigo = T001S3_A39Contratada_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            A442Solicitacoes_Usuario = T001S3_A442Solicitacoes_Usuario[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
            A443Solicitacoes_Usuario_Ult = T001S3_A443Solicitacoes_Usuario_Ult[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
            sMode67 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1S67( ) ;
            if ( AnyError == 1 )
            {
               RcdFound67 = 0;
               InitializeNonKey1S67( ) ;
            }
            Gx_mode = sMode67;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound67 = 0;
            InitializeNonKey1S67( ) ;
            sMode67 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode67;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1S67( ) ;
         if ( RcdFound67 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound67 = 0;
         /* Using cursor T001S16 */
         pr_default.execute(14, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T001S16_A439Solicitacoes_Codigo[0] < A439Solicitacoes_Codigo ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T001S16_A439Solicitacoes_Codigo[0] > A439Solicitacoes_Codigo ) ) )
            {
               A439Solicitacoes_Codigo = T001S16_A439Solicitacoes_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
               RcdFound67 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound67 = 0;
         /* Using cursor T001S17 */
         pr_default.execute(15, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T001S17_A439Solicitacoes_Codigo[0] > A439Solicitacoes_Codigo ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T001S17_A439Solicitacoes_Codigo[0] < A439Solicitacoes_Codigo ) ) )
            {
               A439Solicitacoes_Codigo = T001S17_A439Solicitacoes_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
               RcdFound67 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1S67( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynContratada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1S67( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound67 == 1 )
            {
               if ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo )
               {
                  A439Solicitacoes_Codigo = Z439Solicitacoes_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SOLICITACOES_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynContratada_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1S67( ) ;
                  GX_FocusControl = dynContratada_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynContratada_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1S67( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SOLICITACOES_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynContratada_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1S67( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A439Solicitacoes_Codigo != Z439Solicitacoes_Codigo )
         {
            A439Solicitacoes_Codigo = Z439Solicitacoes_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SOLICITACOES_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynContratada_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1S67( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001S2 */
            pr_default.execute(0, new Object[] {A439Solicitacoes_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Solicitacoes"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z440Solicitacoes_Novo_Projeto, T001S2_A440Solicitacoes_Novo_Projeto[0]) != 0 ) || ( Z444Solicitacoes_Data != T001S2_A444Solicitacoes_Data[0] ) || ( Z445Solicitacoes_Data_Ult != T001S2_A445Solicitacoes_Data_Ult[0] ) || ( StringUtil.StrCmp(Z446Solicitacoes_Status, T001S2_A446Solicitacoes_Status[0]) != 0 ) || ( Z127Sistema_Codigo != T001S2_A127Sistema_Codigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z155Servico_Codigo != T001S2_A155Servico_Codigo[0] ) || ( Z39Contratada_Codigo != T001S2_A39Contratada_Codigo[0] ) || ( Z442Solicitacoes_Usuario != T001S2_A442Solicitacoes_Usuario[0] ) || ( Z443Solicitacoes_Usuario_Ult != T001S2_A443Solicitacoes_Usuario_Ult[0] ) )
            {
               if ( StringUtil.StrCmp(Z440Solicitacoes_Novo_Projeto, T001S2_A440Solicitacoes_Novo_Projeto[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Solicitacoes_Novo_Projeto");
                  GXUtil.WriteLogRaw("Old: ",Z440Solicitacoes_Novo_Projeto);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A440Solicitacoes_Novo_Projeto[0]);
               }
               if ( Z444Solicitacoes_Data != T001S2_A444Solicitacoes_Data[0] )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Solicitacoes_Data");
                  GXUtil.WriteLogRaw("Old: ",Z444Solicitacoes_Data);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A444Solicitacoes_Data[0]);
               }
               if ( Z445Solicitacoes_Data_Ult != T001S2_A445Solicitacoes_Data_Ult[0] )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Solicitacoes_Data_Ult");
                  GXUtil.WriteLogRaw("Old: ",Z445Solicitacoes_Data_Ult);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A445Solicitacoes_Data_Ult[0]);
               }
               if ( StringUtil.StrCmp(Z446Solicitacoes_Status, T001S2_A446Solicitacoes_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Solicitacoes_Status");
                  GXUtil.WriteLogRaw("Old: ",Z446Solicitacoes_Status);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A446Solicitacoes_Status[0]);
               }
               if ( Z127Sistema_Codigo != T001S2_A127Sistema_Codigo[0] )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Sistema_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z127Sistema_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A127Sistema_Codigo[0]);
               }
               if ( Z155Servico_Codigo != T001S2_A155Servico_Codigo[0] )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Servico_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z155Servico_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A155Servico_Codigo[0]);
               }
               if ( Z39Contratada_Codigo != T001S2_A39Contratada_Codigo[0] )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Contratada_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z39Contratada_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A39Contratada_Codigo[0]);
               }
               if ( Z442Solicitacoes_Usuario != T001S2_A442Solicitacoes_Usuario[0] )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Solicitacoes_Usuario");
                  GXUtil.WriteLogRaw("Old: ",Z442Solicitacoes_Usuario);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A442Solicitacoes_Usuario[0]);
               }
               if ( Z443Solicitacoes_Usuario_Ult != T001S2_A443Solicitacoes_Usuario_Ult[0] )
               {
                  GXUtil.WriteLog("solicitacoes:[seudo value changed for attri]"+"Solicitacoes_Usuario_Ult");
                  GXUtil.WriteLogRaw("Old: ",Z443Solicitacoes_Usuario_Ult);
                  GXUtil.WriteLogRaw("Current: ",T001S2_A443Solicitacoes_Usuario_Ult[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Solicitacoes"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1S67( )
      {
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1S67( 0) ;
            CheckOptimisticConcurrency1S67( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1S67( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1S67( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001S18 */
                     pr_default.execute(16, new Object[] {A440Solicitacoes_Novo_Projeto, n441Solicitacoes_Objetivo, A441Solicitacoes_Objetivo, A444Solicitacoes_Data, A445Solicitacoes_Data_Ult, A446Solicitacoes_Status, A127Sistema_Codigo, A155Servico_Codigo, A39Contratada_Codigo, A442Solicitacoes_Usuario, A443Solicitacoes_Usuario_Ult});
                     A439Solicitacoes_Codigo = T001S18_A439Solicitacoes_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Solicitacoes") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1S0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1S67( ) ;
            }
            EndLevel1S67( ) ;
         }
         CloseExtendedTableCursors1S67( ) ;
      }

      protected void Update1S67( )
      {
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1S67( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1S67( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1S67( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001S19 */
                     pr_default.execute(17, new Object[] {A440Solicitacoes_Novo_Projeto, n441Solicitacoes_Objetivo, A441Solicitacoes_Objetivo, A444Solicitacoes_Data, A445Solicitacoes_Data_Ult, A446Solicitacoes_Status, A127Sistema_Codigo, A155Servico_Codigo, A39Contratada_Codigo, A442Solicitacoes_Usuario, A443Solicitacoes_Usuario_Ult, A439Solicitacoes_Codigo});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("Solicitacoes") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Solicitacoes"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1S67( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1S67( ) ;
         }
         CloseExtendedTableCursors1S67( ) ;
      }

      protected void DeferredUpdate1S67( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1S67( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1S67( ) ;
            AfterConfirm1S67( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1S67( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001S20 */
                  pr_default.execute(18, new Object[] {A439Solicitacoes_Codigo});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("Solicitacoes") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode67 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1S67( ) ;
         Gx_mode = sMode67;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1S67( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T001S21 */
            pr_default.execute(19, new Object[] {A439Solicitacoes_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes Itens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
         }
      }

      protected void EndLevel1S67( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1S67( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Solicitacoes");
            if ( AnyError == 0 )
            {
               ConfirmValues1S0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Solicitacoes");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1S67( )
      {
         /* Scan By routine */
         /* Using cursor T001S22 */
         pr_default.execute(20);
         RcdFound67 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound67 = 1;
            A439Solicitacoes_Codigo = T001S22_A439Solicitacoes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1S67( )
      {
         /* Scan next routine */
         pr_default.readNext(20);
         RcdFound67 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound67 = 1;
            A439Solicitacoes_Codigo = T001S22_A439Solicitacoes_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1S67( )
      {
         pr_default.close(20);
      }

      protected void AfterConfirm1S67( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1S67( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1S67( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1S67( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1S67( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1S67( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1S67( )
      {
         edtSolicitacoes_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Codigo_Enabled), 5, 0)));
         dynContratada_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratada_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratada_Codigo.Enabled), 5, 0)));
         edtSistema_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Enabled), 5, 0)));
         dynServico_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Codigo.Enabled), 5, 0)));
         radSolicitacoes_Novo_Projeto.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radSolicitacoes_Novo_Projeto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(radSolicitacoes_Novo_Projeto.Enabled), 5, 0)));
         edtSolicitacoes_Objetivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Objetivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Objetivo_Enabled), 5, 0)));
         edtSolicitacoes_Usuario_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Usuario_Enabled), 5, 0)));
         edtSolicitacoes_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Data_Enabled), 5, 0)));
         edtSolicitacoes_Usuario_Ult_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Ult_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Usuario_Ult_Enabled), 5, 0)));
         edtSolicitacoes_Data_Ult_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Data_Ult_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSolicitacoes_Data_Ult_Enabled), 5, 0)));
         cmbSolicitacoes_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicitacoes_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbSolicitacoes_Status.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1S0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311721633");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacoes.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV17Solicitacoes_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z439Solicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z440Solicitacoes_Novo_Projeto", StringUtil.RTrim( Z440Solicitacoes_Novo_Projeto));
         GxWebStd.gx_hidden_field( context, "Z444Solicitacoes_Data", context.localUtil.TToC( Z444Solicitacoes_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z445Solicitacoes_Data_Ult", context.localUtil.TToC( Z445Solicitacoes_Data_Ult, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z446Solicitacoes_Status", StringUtil.RTrim( Z446Solicitacoes_Status));
         GxWebStd.gx_hidden_field( context, "Z127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z39Contratada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z442Solicitacoes_Usuario), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z443Solicitacoes_Usuario_Ult), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N39Contratada_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.NToC( (decimal)(A442Solicitacoes_Usuario), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.NToC( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV8TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV8TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSOLICITACOES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Solicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Insert_Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Insert_Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SOLICITACOES_USUARIO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Solicitacoes_Usuario), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SOLICITACOES_USUARIO_ULT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_Solicitacoes_Usuario_Ult), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV7WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV7WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV18Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSOLICITACOES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV17Solicitacoes_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Solicitacoes";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("solicitacoes:[SendSecurityCheck value for]"+"Solicitacoes_Codigo:"+context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("solicitacoes:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("solicitacoes.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV17Solicitacoes_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Solicitacoes" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacoes" ;
      }

      protected void InitializeNonKey1S67( )
      {
         A39Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         A127Sistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         A155Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A442Solicitacoes_Usuario = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A442Solicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(A442Solicitacoes_Usuario), 6, 0)));
         A443Solicitacoes_Usuario_Ult = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A443Solicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0)));
         A440Solicitacoes_Novo_Projeto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A440Solicitacoes_Novo_Projeto", A440Solicitacoes_Novo_Projeto);
         A441Solicitacoes_Objetivo = "";
         n441Solicitacoes_Objetivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A441Solicitacoes_Objetivo", A441Solicitacoes_Objetivo);
         n441Solicitacoes_Objetivo = (String.IsNullOrEmpty(StringUtil.RTrim( A441Solicitacoes_Objetivo)) ? true : false);
         A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A444Solicitacoes_Data", context.localUtil.TToC( A444Solicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
         A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A445Solicitacoes_Data_Ult", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
         A446Solicitacoes_Status = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A446Solicitacoes_Status", A446Solicitacoes_Status);
         Z440Solicitacoes_Novo_Projeto = "";
         Z444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         Z445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         Z446Solicitacoes_Status = "";
         Z127Sistema_Codigo = 0;
         Z155Servico_Codigo = 0;
         Z39Contratada_Codigo = 0;
         Z442Solicitacoes_Usuario = 0;
         Z443Solicitacoes_Usuario_Ult = 0;
      }

      protected void InitAll1S67( )
      {
         A439Solicitacoes_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A439Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A439Solicitacoes_Codigo), 6, 0)));
         InitializeNonKey1S67( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311721670");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("solicitacoes.js", "?2020311721671");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksolicitacoes_codigo_Internalname = "TEXTBLOCKSOLICITACOES_CODIGO";
         edtSolicitacoes_Codigo_Internalname = "SOLICITACOES_CODIGO";
         lblTextblockcontratada_codigo_Internalname = "TEXTBLOCKCONTRATADA_CODIGO";
         dynContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         lblTextblocksistema_codigo_Internalname = "TEXTBLOCKSISTEMA_CODIGO";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         lblTextblockservico_codigo_Internalname = "TEXTBLOCKSERVICO_CODIGO";
         dynServico_Codigo_Internalname = "SERVICO_CODIGO";
         lblTextblocksolicitacoes_novo_projeto_Internalname = "TEXTBLOCKSOLICITACOES_NOVO_PROJETO";
         radSolicitacoes_Novo_Projeto_Internalname = "SOLICITACOES_NOVO_PROJETO";
         lblTextblocksolicitacoes_objetivo_Internalname = "TEXTBLOCKSOLICITACOES_OBJETIVO";
         edtSolicitacoes_Objetivo_Internalname = "SOLICITACOES_OBJETIVO";
         lblTextblocksolicitacoes_usuario_Internalname = "TEXTBLOCKSOLICITACOES_USUARIO";
         edtSolicitacoes_Usuario_Internalname = "SOLICITACOES_USUARIO";
         lblTextblocksolicitacoes_data_Internalname = "TEXTBLOCKSOLICITACOES_DATA";
         edtSolicitacoes_Data_Internalname = "SOLICITACOES_DATA";
         lblTextblocksolicitacoes_usuario_ult_Internalname = "TEXTBLOCKSOLICITACOES_USUARIO_ULT";
         edtSolicitacoes_Usuario_Ult_Internalname = "SOLICITACOES_USUARIO_ULT";
         lblTextblocksolicitacoes_data_ult_Internalname = "TEXTBLOCKSOLICITACOES_DATA_ULT";
         edtSolicitacoes_Data_Ult_Internalname = "SOLICITACOES_DATA_ULT";
         lblTextblocksolicitacoes_status_Internalname = "TEXTBLOCKSOLICITACOES_STATUS";
         cmbSolicitacoes_Status_Internalname = "SOLICITACOES_STATUS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Solicitacoes";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Solicitacoes";
         cmbSolicitacoes_Status_Jsonclick = "";
         cmbSolicitacoes_Status.Enabled = 1;
         edtSolicitacoes_Data_Ult_Jsonclick = "";
         edtSolicitacoes_Data_Ult_Enabled = 1;
         edtSolicitacoes_Usuario_Ult_Jsonclick = "";
         edtSolicitacoes_Usuario_Ult_Enabled = 1;
         edtSolicitacoes_Data_Jsonclick = "";
         edtSolicitacoes_Data_Enabled = 1;
         edtSolicitacoes_Usuario_Jsonclick = "";
         edtSolicitacoes_Usuario_Enabled = 1;
         edtSolicitacoes_Objetivo_Enabled = 1;
         radSolicitacoes_Novo_Projeto_Jsonclick = "";
         radSolicitacoes_Novo_Projeto.Enabled = 1;
         dynServico_Codigo_Jsonclick = "";
         dynServico_Codigo.Enabled = 1;
         edtSistema_Codigo_Jsonclick = "";
         edtSistema_Codigo_Enabled = 1;
         dynContratada_Codigo_Jsonclick = "";
         dynContratada_Codigo.Enabled = 1;
         edtSolicitacoes_Codigo_Jsonclick = "";
         edtSolicitacoes_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATADA_CODIGO1S67( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATADA_CODIGO_data1S67( AV7WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATADA_CODIGO_html1S67( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATADA_CODIGO_data1S67( AV7WWPContext) ;
         gxdynajaxindex = 1;
         dynContratada_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratada_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATADA_CODIGO_data1S67( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T001S23 */
         pr_default.execute(21, new Object[] {AV7WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(21) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001S23_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001S23_A41Contratada_PessoaNom[0]));
            pr_default.readNext(21);
         }
         pr_default.close(21);
      }

      protected void GXDLASERVICO_CODIGO1S67( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_CODIGO_data1S67( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_CODIGO_html1S67( )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_CODIGO_data1S67( ) ;
         gxdynajaxindex = 1;
         dynServico_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICO_CODIGO_data1S67( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T001S24 */
         pr_default.execute(22);
         while ( (pr_default.getStatus(22) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001S24_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001S24_A608Servico_Nome[0]));
            pr_default.readNext(22);
         }
         pr_default.close(22);
      }

      public void Valid_Contratada_codigo( GXCombobox dynGX_Parm1 )
      {
         dynContratada_Codigo = dynGX_Parm1;
         A39Contratada_Codigo = (int)(NumberUtil.Val( dynContratada_Codigo.CurrentValue, "."));
         /* Using cursor T001S25 */
         pr_default.execute(23, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "CONTRATADA_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynContratada_Codigo_Internalname;
         }
         pr_default.close(23);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Sistema_codigo( int GX_Parm1 )
      {
         A127Sistema_Codigo = GX_Parm1;
         /* Using cursor T001S26 */
         pr_default.execute(24, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(24) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
         }
         pr_default.close(24);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servico_codigo( GXCombobox dynGX_Parm1 )
      {
         dynServico_Codigo = dynGX_Parm1;
         A155Servico_Codigo = (int)(NumberUtil.Val( dynServico_Codigo.CurrentValue, "."));
         /* Using cursor T001S27 */
         pr_default.execute(25, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(25) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServico_Codigo_Internalname;
         }
         pr_default.close(25);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Solicitacoes_usuario( int GX_Parm1 )
      {
         A442Solicitacoes_Usuario = GX_Parm1;
         /* Using cursor T001S28 */
         pr_default.execute(26, new Object[] {A442Solicitacoes_Usuario});
         if ( (pr_default.getStatus(26) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Solicitacoes_Usuario'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Usuario_Internalname;
         }
         pr_default.close(26);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Solicitacoes_usuario_ult( int GX_Parm1 )
      {
         A443Solicitacoes_Usuario_Ult = GX_Parm1;
         /* Using cursor T001S29 */
         pr_default.execute(27, new Object[] {A443Solicitacoes_Usuario_Ult});
         if ( (pr_default.getStatus(27) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes_Usuario Ult'.", "ForeignKeyNotFound", 1, "SOLICITACOES_USUARIO_ULT");
            AnyError = 1;
            GX_FocusControl = edtSolicitacoes_Usuario_Ult_Internalname;
         }
         pr_default.close(27);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV17Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121S2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(23);
         pr_default.close(26);
         pr_default.close(27);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z440Solicitacoes_Novo_Projeto = "";
         Z444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         Z445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         Z446Solicitacoes_Status = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         A446Solicitacoes_Status = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocksolicitacoes_codigo_Jsonclick = "";
         lblTextblockcontratada_codigo_Jsonclick = "";
         lblTextblocksistema_codigo_Jsonclick = "";
         lblTextblockservico_codigo_Jsonclick = "";
         lblTextblocksolicitacoes_novo_projeto_Jsonclick = "";
         A440Solicitacoes_Novo_Projeto = "";
         lblTextblocksolicitacoes_objetivo_Jsonclick = "";
         A441Solicitacoes_Objetivo = "";
         lblTextblocksolicitacoes_usuario_Jsonclick = "";
         lblTextblocksolicitacoes_data_Jsonclick = "";
         A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         lblTextblocksolicitacoes_usuario_ult_Jsonclick = "";
         lblTextblocksolicitacoes_data_ult_Jsonclick = "";
         A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         lblTextblocksolicitacoes_status_Jsonclick = "";
         AV18Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode67 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9WebSession = context.GetSession();
         AV15TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z441Solicitacoes_Objetivo = "";
         T001S9_A439Solicitacoes_Codigo = new int[1] ;
         T001S9_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         T001S9_A441Solicitacoes_Objetivo = new String[] {""} ;
         T001S9_n441Solicitacoes_Objetivo = new bool[] {false} ;
         T001S9_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         T001S9_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         T001S9_A446Solicitacoes_Status = new String[] {""} ;
         T001S9_A127Sistema_Codigo = new int[1] ;
         T001S9_A155Servico_Codigo = new int[1] ;
         T001S9_A39Contratada_Codigo = new int[1] ;
         T001S9_A442Solicitacoes_Usuario = new int[1] ;
         T001S9_A443Solicitacoes_Usuario_Ult = new int[1] ;
         T001S6_A39Contratada_Codigo = new int[1] ;
         T001S4_A127Sistema_Codigo = new int[1] ;
         T001S5_A155Servico_Codigo = new int[1] ;
         T001S7_A442Solicitacoes_Usuario = new int[1] ;
         T001S8_A443Solicitacoes_Usuario_Ult = new int[1] ;
         T001S10_A39Contratada_Codigo = new int[1] ;
         T001S11_A127Sistema_Codigo = new int[1] ;
         T001S12_A155Servico_Codigo = new int[1] ;
         T001S13_A442Solicitacoes_Usuario = new int[1] ;
         T001S14_A443Solicitacoes_Usuario_Ult = new int[1] ;
         T001S15_A439Solicitacoes_Codigo = new int[1] ;
         T001S3_A439Solicitacoes_Codigo = new int[1] ;
         T001S3_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         T001S3_A441Solicitacoes_Objetivo = new String[] {""} ;
         T001S3_n441Solicitacoes_Objetivo = new bool[] {false} ;
         T001S3_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         T001S3_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         T001S3_A446Solicitacoes_Status = new String[] {""} ;
         T001S3_A127Sistema_Codigo = new int[1] ;
         T001S3_A155Servico_Codigo = new int[1] ;
         T001S3_A39Contratada_Codigo = new int[1] ;
         T001S3_A442Solicitacoes_Usuario = new int[1] ;
         T001S3_A443Solicitacoes_Usuario_Ult = new int[1] ;
         T001S16_A439Solicitacoes_Codigo = new int[1] ;
         T001S17_A439Solicitacoes_Codigo = new int[1] ;
         T001S2_A439Solicitacoes_Codigo = new int[1] ;
         T001S2_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         T001S2_A441Solicitacoes_Objetivo = new String[] {""} ;
         T001S2_n441Solicitacoes_Objetivo = new bool[] {false} ;
         T001S2_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         T001S2_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         T001S2_A446Solicitacoes_Status = new String[] {""} ;
         T001S2_A127Sistema_Codigo = new int[1] ;
         T001S2_A155Servico_Codigo = new int[1] ;
         T001S2_A39Contratada_Codigo = new int[1] ;
         T001S2_A442Solicitacoes_Usuario = new int[1] ;
         T001S2_A443Solicitacoes_Usuario_Ult = new int[1] ;
         T001S18_A439Solicitacoes_Codigo = new int[1] ;
         T001S21_A447SolicitacoesItens_Codigo = new int[1] ;
         T001S22_A439Solicitacoes_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001S23_A40Contratada_PessoaCod = new int[1] ;
         T001S23_A39Contratada_Codigo = new int[1] ;
         T001S23_A41Contratada_PessoaNom = new String[] {""} ;
         T001S23_n41Contratada_PessoaNom = new bool[] {false} ;
         T001S23_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T001S24_A155Servico_Codigo = new int[1] ;
         T001S24_A608Servico_Nome = new String[] {""} ;
         T001S24_A631Servico_Vinculado = new int[1] ;
         T001S24_n631Servico_Vinculado = new bool[] {false} ;
         T001S24_A632Servico_Ativo = new bool[] {false} ;
         T001S25_A39Contratada_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T001S26_A127Sistema_Codigo = new int[1] ;
         T001S27_A155Servico_Codigo = new int[1] ;
         T001S28_A442Solicitacoes_Usuario = new int[1] ;
         T001S29_A443Solicitacoes_Usuario_Ult = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacoes__default(),
            new Object[][] {
                new Object[] {
               T001S2_A439Solicitacoes_Codigo, T001S2_A440Solicitacoes_Novo_Projeto, T001S2_A441Solicitacoes_Objetivo, T001S2_n441Solicitacoes_Objetivo, T001S2_A444Solicitacoes_Data, T001S2_A445Solicitacoes_Data_Ult, T001S2_A446Solicitacoes_Status, T001S2_A127Sistema_Codigo, T001S2_A155Servico_Codigo, T001S2_A39Contratada_Codigo,
               T001S2_A442Solicitacoes_Usuario, T001S2_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               T001S3_A439Solicitacoes_Codigo, T001S3_A440Solicitacoes_Novo_Projeto, T001S3_A441Solicitacoes_Objetivo, T001S3_n441Solicitacoes_Objetivo, T001S3_A444Solicitacoes_Data, T001S3_A445Solicitacoes_Data_Ult, T001S3_A446Solicitacoes_Status, T001S3_A127Sistema_Codigo, T001S3_A155Servico_Codigo, T001S3_A39Contratada_Codigo,
               T001S3_A442Solicitacoes_Usuario, T001S3_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               T001S4_A127Sistema_Codigo
               }
               , new Object[] {
               T001S5_A155Servico_Codigo
               }
               , new Object[] {
               T001S6_A39Contratada_Codigo
               }
               , new Object[] {
               T001S7_A442Solicitacoes_Usuario
               }
               , new Object[] {
               T001S8_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               T001S9_A439Solicitacoes_Codigo, T001S9_A440Solicitacoes_Novo_Projeto, T001S9_A441Solicitacoes_Objetivo, T001S9_n441Solicitacoes_Objetivo, T001S9_A444Solicitacoes_Data, T001S9_A445Solicitacoes_Data_Ult, T001S9_A446Solicitacoes_Status, T001S9_A127Sistema_Codigo, T001S9_A155Servico_Codigo, T001S9_A39Contratada_Codigo,
               T001S9_A442Solicitacoes_Usuario, T001S9_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               T001S10_A39Contratada_Codigo
               }
               , new Object[] {
               T001S11_A127Sistema_Codigo
               }
               , new Object[] {
               T001S12_A155Servico_Codigo
               }
               , new Object[] {
               T001S13_A442Solicitacoes_Usuario
               }
               , new Object[] {
               T001S14_A443Solicitacoes_Usuario_Ult
               }
               , new Object[] {
               T001S15_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T001S16_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T001S17_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T001S18_A439Solicitacoes_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001S21_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               T001S22_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T001S23_A40Contratada_PessoaCod, T001S23_A39Contratada_Codigo, T001S23_A41Contratada_PessoaNom, T001S23_n41Contratada_PessoaNom, T001S23_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T001S24_A155Servico_Codigo, T001S24_A608Servico_Nome, T001S24_A631Servico_Vinculado, T001S24_n631Servico_Vinculado, T001S24_A632Servico_Ativo
               }
               , new Object[] {
               T001S25_A39Contratada_Codigo
               }
               , new Object[] {
               T001S26_A127Sistema_Codigo
               }
               , new Object[] {
               T001S27_A155Servico_Codigo
               }
               , new Object[] {
               T001S28_A442Solicitacoes_Usuario
               }
               , new Object[] {
               T001S29_A443Solicitacoes_Usuario_Ult
               }
            }
         );
         AV18Pgmname = "Solicitacoes";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound67 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV17Solicitacoes_Codigo ;
      private int Z439Solicitacoes_Codigo ;
      private int Z127Sistema_Codigo ;
      private int Z155Servico_Codigo ;
      private int Z39Contratada_Codigo ;
      private int Z442Solicitacoes_Usuario ;
      private int Z443Solicitacoes_Usuario_Ult ;
      private int N39Contratada_Codigo ;
      private int N127Sistema_Codigo ;
      private int N155Servico_Codigo ;
      private int N442Solicitacoes_Usuario ;
      private int N443Solicitacoes_Usuario_Ult ;
      private int A39Contratada_Codigo ;
      private int A127Sistema_Codigo ;
      private int A155Servico_Codigo ;
      private int A442Solicitacoes_Usuario ;
      private int A443Solicitacoes_Usuario_Ult ;
      private int AV17Solicitacoes_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A439Solicitacoes_Codigo ;
      private int edtSolicitacoes_Codigo_Enabled ;
      private int edtSistema_Codigo_Enabled ;
      private int edtSolicitacoes_Objetivo_Enabled ;
      private int edtSolicitacoes_Usuario_Enabled ;
      private int edtSolicitacoes_Data_Enabled ;
      private int edtSolicitacoes_Usuario_Ult_Enabled ;
      private int edtSolicitacoes_Data_Ult_Enabled ;
      private int AV10Insert_Contratada_Codigo ;
      private int AV16Insert_Sistema_Codigo ;
      private int AV11Insert_Servico_Codigo ;
      private int AV13Insert_Solicitacoes_Usuario ;
      private int AV14Insert_Solicitacoes_Usuario_Ult ;
      private int AV19GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z440Solicitacoes_Novo_Projeto ;
      private String Z446Solicitacoes_Status ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A446Solicitacoes_Status ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynContratada_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksolicitacoes_codigo_Internalname ;
      private String lblTextblocksolicitacoes_codigo_Jsonclick ;
      private String edtSolicitacoes_Codigo_Internalname ;
      private String edtSolicitacoes_Codigo_Jsonclick ;
      private String lblTextblockcontratada_codigo_Internalname ;
      private String lblTextblockcontratada_codigo_Jsonclick ;
      private String dynContratada_Codigo_Jsonclick ;
      private String lblTextblocksistema_codigo_Internalname ;
      private String lblTextblocksistema_codigo_Jsonclick ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Codigo_Jsonclick ;
      private String lblTextblockservico_codigo_Internalname ;
      private String lblTextblockservico_codigo_Jsonclick ;
      private String dynServico_Codigo_Internalname ;
      private String dynServico_Codigo_Jsonclick ;
      private String lblTextblocksolicitacoes_novo_projeto_Internalname ;
      private String lblTextblocksolicitacoes_novo_projeto_Jsonclick ;
      private String radSolicitacoes_Novo_Projeto_Internalname ;
      private String A440Solicitacoes_Novo_Projeto ;
      private String radSolicitacoes_Novo_Projeto_Jsonclick ;
      private String lblTextblocksolicitacoes_objetivo_Internalname ;
      private String lblTextblocksolicitacoes_objetivo_Jsonclick ;
      private String edtSolicitacoes_Objetivo_Internalname ;
      private String lblTextblocksolicitacoes_usuario_Internalname ;
      private String lblTextblocksolicitacoes_usuario_Jsonclick ;
      private String edtSolicitacoes_Usuario_Internalname ;
      private String edtSolicitacoes_Usuario_Jsonclick ;
      private String lblTextblocksolicitacoes_data_Internalname ;
      private String lblTextblocksolicitacoes_data_Jsonclick ;
      private String edtSolicitacoes_Data_Internalname ;
      private String edtSolicitacoes_Data_Jsonclick ;
      private String lblTextblocksolicitacoes_usuario_ult_Internalname ;
      private String lblTextblocksolicitacoes_usuario_ult_Jsonclick ;
      private String edtSolicitacoes_Usuario_Ult_Internalname ;
      private String edtSolicitacoes_Usuario_Ult_Jsonclick ;
      private String lblTextblocksolicitacoes_data_ult_Internalname ;
      private String lblTextblocksolicitacoes_data_ult_Jsonclick ;
      private String edtSolicitacoes_Data_Ult_Internalname ;
      private String edtSolicitacoes_Data_Ult_Jsonclick ;
      private String lblTextblocksolicitacoes_status_Internalname ;
      private String lblTextblocksolicitacoes_status_Jsonclick ;
      private String cmbSolicitacoes_Status_Internalname ;
      private String cmbSolicitacoes_Status_Jsonclick ;
      private String AV18Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode67 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z444Solicitacoes_Data ;
      private DateTime Z445Solicitacoes_Data_Ult ;
      private DateTime A444Solicitacoes_Data ;
      private DateTime A445Solicitacoes_Data_Ult ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n441Solicitacoes_Objetivo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A441Solicitacoes_Objetivo ;
      private String Z441Solicitacoes_Objetivo ;
      private IGxSession AV9WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContratada_Codigo ;
      private GXCombobox dynServico_Codigo ;
      private GXRadio radSolicitacoes_Novo_Projeto ;
      private GXCombobox cmbSolicitacoes_Status ;
      private IDataStoreProvider pr_default ;
      private int[] T001S9_A439Solicitacoes_Codigo ;
      private String[] T001S9_A440Solicitacoes_Novo_Projeto ;
      private String[] T001S9_A441Solicitacoes_Objetivo ;
      private bool[] T001S9_n441Solicitacoes_Objetivo ;
      private DateTime[] T001S9_A444Solicitacoes_Data ;
      private DateTime[] T001S9_A445Solicitacoes_Data_Ult ;
      private String[] T001S9_A446Solicitacoes_Status ;
      private int[] T001S9_A127Sistema_Codigo ;
      private int[] T001S9_A155Servico_Codigo ;
      private int[] T001S9_A39Contratada_Codigo ;
      private int[] T001S9_A442Solicitacoes_Usuario ;
      private int[] T001S9_A443Solicitacoes_Usuario_Ult ;
      private int[] T001S6_A39Contratada_Codigo ;
      private int[] T001S4_A127Sistema_Codigo ;
      private int[] T001S5_A155Servico_Codigo ;
      private int[] T001S7_A442Solicitacoes_Usuario ;
      private int[] T001S8_A443Solicitacoes_Usuario_Ult ;
      private int[] T001S10_A39Contratada_Codigo ;
      private int[] T001S11_A127Sistema_Codigo ;
      private int[] T001S12_A155Servico_Codigo ;
      private int[] T001S13_A442Solicitacoes_Usuario ;
      private int[] T001S14_A443Solicitacoes_Usuario_Ult ;
      private int[] T001S15_A439Solicitacoes_Codigo ;
      private int[] T001S3_A439Solicitacoes_Codigo ;
      private String[] T001S3_A440Solicitacoes_Novo_Projeto ;
      private String[] T001S3_A441Solicitacoes_Objetivo ;
      private bool[] T001S3_n441Solicitacoes_Objetivo ;
      private DateTime[] T001S3_A444Solicitacoes_Data ;
      private DateTime[] T001S3_A445Solicitacoes_Data_Ult ;
      private String[] T001S3_A446Solicitacoes_Status ;
      private int[] T001S3_A127Sistema_Codigo ;
      private int[] T001S3_A155Servico_Codigo ;
      private int[] T001S3_A39Contratada_Codigo ;
      private int[] T001S3_A442Solicitacoes_Usuario ;
      private int[] T001S3_A443Solicitacoes_Usuario_Ult ;
      private int[] T001S16_A439Solicitacoes_Codigo ;
      private int[] T001S17_A439Solicitacoes_Codigo ;
      private int[] T001S2_A439Solicitacoes_Codigo ;
      private String[] T001S2_A440Solicitacoes_Novo_Projeto ;
      private String[] T001S2_A441Solicitacoes_Objetivo ;
      private bool[] T001S2_n441Solicitacoes_Objetivo ;
      private DateTime[] T001S2_A444Solicitacoes_Data ;
      private DateTime[] T001S2_A445Solicitacoes_Data_Ult ;
      private String[] T001S2_A446Solicitacoes_Status ;
      private int[] T001S2_A127Sistema_Codigo ;
      private int[] T001S2_A155Servico_Codigo ;
      private int[] T001S2_A39Contratada_Codigo ;
      private int[] T001S2_A442Solicitacoes_Usuario ;
      private int[] T001S2_A443Solicitacoes_Usuario_Ult ;
      private int[] T001S18_A439Solicitacoes_Codigo ;
      private int[] T001S21_A447SolicitacoesItens_Codigo ;
      private int[] T001S22_A439Solicitacoes_Codigo ;
      private int[] T001S23_A40Contratada_PessoaCod ;
      private int[] T001S23_A39Contratada_Codigo ;
      private String[] T001S23_A41Contratada_PessoaNom ;
      private bool[] T001S23_n41Contratada_PessoaNom ;
      private int[] T001S23_A52Contratada_AreaTrabalhoCod ;
      private int[] T001S24_A155Servico_Codigo ;
      private String[] T001S24_A608Servico_Nome ;
      private int[] T001S24_A631Servico_Vinculado ;
      private bool[] T001S24_n631Servico_Vinculado ;
      private bool[] T001S24_A632Servico_Ativo ;
      private int[] T001S25_A39Contratada_Codigo ;
      private int[] T001S26_A127Sistema_Codigo ;
      private int[] T001S27_A155Servico_Codigo ;
      private int[] T001S28_A442Solicitacoes_Usuario ;
      private int[] T001S29_A443Solicitacoes_Usuario_Ult ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV15TrnContextAtt ;
   }

   public class solicitacoes__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001S9 ;
          prmT001S9 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S6 ;
          prmT001S6 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S4 ;
          prmT001S4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S5 ;
          prmT001S5 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S7 ;
          prmT001S7 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S8 ;
          prmT001S8 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S10 ;
          prmT001S10 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S11 ;
          prmT001S11 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S12 ;
          prmT001S12 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S13 ;
          prmT001S13 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S14 ;
          prmT001S14 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S15 ;
          prmT001S15 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S3 ;
          prmT001S3 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S16 ;
          prmT001S16 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S17 ;
          prmT001S17 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S2 ;
          prmT001S2 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S18 ;
          prmT001S18 = new Object[] {
          new Object[] {"@Solicitacoes_Novo_Projeto",SqlDbType.Char,1,0} ,
          new Object[] {"@Solicitacoes_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Solicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S19 ;
          prmT001S19 = new Object[] {
          new Object[] {"@Solicitacoes_Novo_Projeto",SqlDbType.Char,1,0} ,
          new Object[] {"@Solicitacoes_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Solicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Solicitacoes_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0} ,
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S20 ;
          prmT001S20 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S21 ;
          prmT001S21 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S22 ;
          prmT001S22 = new Object[] {
          } ;
          Object[] prmT001S23 ;
          prmT001S23 = new Object[] {
          new Object[] {"@AV7WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S24 ;
          prmT001S24 = new Object[] {
          } ;
          Object[] prmT001S25 ;
          prmT001S25 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S26 ;
          prmT001S26 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S27 ;
          prmT001S27 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S28 ;
          prmT001S28 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001S29 ;
          prmT001S29 = new Object[] {
          new Object[] {"@Solicitacoes_Usuario_Ult",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001S2", "SELECT [Solicitacoes_Codigo], [Solicitacoes_Novo_Projeto], [Solicitacoes_Objetivo], [Solicitacoes_Data], [Solicitacoes_Data_Ult], [Solicitacoes_Status], [Sistema_Codigo], [Servico_Codigo], [Contratada_Codigo], [Solicitacoes_Usuario] AS Solicitacoes_Usuario, [Solicitacoes_Usuario_Ult] AS Solicitacoes_Usuario_Ult FROM [Solicitacoes] WITH (UPDLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S2,1,0,true,false )
             ,new CursorDef("T001S3", "SELECT [Solicitacoes_Codigo], [Solicitacoes_Novo_Projeto], [Solicitacoes_Objetivo], [Solicitacoes_Data], [Solicitacoes_Data_Ult], [Solicitacoes_Status], [Sistema_Codigo], [Servico_Codigo], [Contratada_Codigo], [Solicitacoes_Usuario] AS Solicitacoes_Usuario, [Solicitacoes_Usuario_Ult] AS Solicitacoes_Usuario_Ult FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S3,1,0,true,false )
             ,new CursorDef("T001S4", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S4,1,0,true,false )
             ,new CursorDef("T001S5", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S5,1,0,true,false )
             ,new CursorDef("T001S6", "SELECT [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S6,1,0,true,false )
             ,new CursorDef("T001S7", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S7,1,0,true,false )
             ,new CursorDef("T001S8", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario_Ult FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario_Ult ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S8,1,0,true,false )
             ,new CursorDef("T001S9", "SELECT TM1.[Solicitacoes_Codigo], TM1.[Solicitacoes_Novo_Projeto], TM1.[Solicitacoes_Objetivo], TM1.[Solicitacoes_Data], TM1.[Solicitacoes_Data_Ult], TM1.[Solicitacoes_Status], TM1.[Sistema_Codigo], TM1.[Servico_Codigo], TM1.[Contratada_Codigo], TM1.[Solicitacoes_Usuario] AS Solicitacoes_Usuario, TM1.[Solicitacoes_Usuario_Ult] AS Solicitacoes_Usuario_Ult FROM [Solicitacoes] TM1 WITH (NOLOCK) WHERE TM1.[Solicitacoes_Codigo] = @Solicitacoes_Codigo ORDER BY TM1.[Solicitacoes_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001S9,100,0,true,false )
             ,new CursorDef("T001S10", "SELECT [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S10,1,0,true,false )
             ,new CursorDef("T001S11", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S11,1,0,true,false )
             ,new CursorDef("T001S12", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S12,1,0,true,false )
             ,new CursorDef("T001S13", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S13,1,0,true,false )
             ,new CursorDef("T001S14", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario_Ult FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario_Ult ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S14,1,0,true,false )
             ,new CursorDef("T001S15", "SELECT [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001S15,1,0,true,false )
             ,new CursorDef("T001S16", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE ( [Solicitacoes_Codigo] > @Solicitacoes_Codigo) ORDER BY [Solicitacoes_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001S16,1,0,true,true )
             ,new CursorDef("T001S17", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE ( [Solicitacoes_Codigo] < @Solicitacoes_Codigo) ORDER BY [Solicitacoes_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001S17,1,0,true,true )
             ,new CursorDef("T001S18", "INSERT INTO [Solicitacoes]([Solicitacoes_Novo_Projeto], [Solicitacoes_Objetivo], [Solicitacoes_Data], [Solicitacoes_Data_Ult], [Solicitacoes_Status], [Sistema_Codigo], [Servico_Codigo], [Contratada_Codigo], [Solicitacoes_Usuario], [Solicitacoes_Usuario_Ult]) VALUES(@Solicitacoes_Novo_Projeto, @Solicitacoes_Objetivo, @Solicitacoes_Data, @Solicitacoes_Data_Ult, @Solicitacoes_Status, @Sistema_Codigo, @Servico_Codigo, @Contratada_Codigo, @Solicitacoes_Usuario, @Solicitacoes_Usuario_Ult); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001S18)
             ,new CursorDef("T001S19", "UPDATE [Solicitacoes] SET [Solicitacoes_Novo_Projeto]=@Solicitacoes_Novo_Projeto, [Solicitacoes_Objetivo]=@Solicitacoes_Objetivo, [Solicitacoes_Data]=@Solicitacoes_Data, [Solicitacoes_Data_Ult]=@Solicitacoes_Data_Ult, [Solicitacoes_Status]=@Solicitacoes_Status, [Sistema_Codigo]=@Sistema_Codigo, [Servico_Codigo]=@Servico_Codigo, [Contratada_Codigo]=@Contratada_Codigo, [Solicitacoes_Usuario]=@Solicitacoes_Usuario, [Solicitacoes_Usuario_Ult]=@Solicitacoes_Usuario_Ult  WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo", GxErrorMask.GX_NOMASK,prmT001S19)
             ,new CursorDef("T001S20", "DELETE FROM [Solicitacoes]  WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo", GxErrorMask.GX_NOMASK,prmT001S20)
             ,new CursorDef("T001S21", "SELECT TOP 1 [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S21,1,0,true,true )
             ,new CursorDef("T001S22", "SELECT [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) ORDER BY [Solicitacoes_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001S22,100,0,true,false )
             ,new CursorDef("T001S23", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV7WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S23,0,0,true,false )
             ,new CursorDef("T001S24", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Vinculado], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE (([Servico_Vinculado] = convert(int, 0))) AND ([Servico_Ativo] = 1) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S24,0,0,true,false )
             ,new CursorDef("T001S25", "SELECT [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S25,1,0,true,false )
             ,new CursorDef("T001S26", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S26,1,0,true,false )
             ,new CursorDef("T001S27", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S27,1,0,true,false )
             ,new CursorDef("T001S28", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S28,1,0,true,false )
             ,new CursorDef("T001S29", "SELECT [Usuario_Codigo] AS Solicitacoes_Usuario_Ult FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Solicitacoes_Usuario_Ult ",true, GxErrorMask.GX_NOMASK, false, this,prmT001S29,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (int)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                return;
             case 17 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                stmt.SetParameterDatetime(4, (DateTime)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                stmt.SetParameter(8, (int)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                stmt.SetParameter(11, (int)parms[11]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
