/*
               File: PRC_GeraRelatorioGerencialContagem
        Description: Gera as informa��es do Relat�rio Gerencial de Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:18:28.18
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_gerarelatoriogerencialcontagem : GXProcedure
   {
      public aprc_gerarelatoriogerencialcontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_gerarelatoriogerencialcontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_AreaTrabalhoCod ,
                           DateTime aP1_ContagemResultado_DataDmnIn ,
                           DateTime aP2_ContagemResultado_DataDmnFim ,
                           int aP3_ContagemResultado_ContratadaOrigemCod ,
                           int aP4_ContagemResultado_CntSrvCod ,
                           String aP5_ContagemResultado_StatusDmn ,
                           out IGxCollection aP6_SDT_RelatorioGerencial )
      {
         this.AV30Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         this.AV34ContagemResultado_DataDmnIn = aP1_ContagemResultado_DataDmnIn;
         this.AV33ContagemResultado_DataDmnFim = aP2_ContagemResultado_DataDmnFim;
         this.AV9ContagemResultado_ContratadaOrigemCod = aP3_ContagemResultado_ContratadaOrigemCod;
         this.AV8ContagemResultado_CntSrvCod = aP4_ContagemResultado_CntSrvCod;
         this.AV16ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         this.AV24SDT_RelatorioGerencial = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_Meetrika", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP6_SDT_RelatorioGerencial=this.AV24SDT_RelatorioGerencial;
      }

      public IGxCollection executeUdp( int aP0_Contrato_AreaTrabalhoCod ,
                                       DateTime aP1_ContagemResultado_DataDmnIn ,
                                       DateTime aP2_ContagemResultado_DataDmnFim ,
                                       int aP3_ContagemResultado_ContratadaOrigemCod ,
                                       int aP4_ContagemResultado_CntSrvCod ,
                                       String aP5_ContagemResultado_StatusDmn )
      {
         this.AV30Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         this.AV34ContagemResultado_DataDmnIn = aP1_ContagemResultado_DataDmnIn;
         this.AV33ContagemResultado_DataDmnFim = aP2_ContagemResultado_DataDmnFim;
         this.AV9ContagemResultado_ContratadaOrigemCod = aP3_ContagemResultado_ContratadaOrigemCod;
         this.AV8ContagemResultado_CntSrvCod = aP4_ContagemResultado_CntSrvCod;
         this.AV16ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         this.AV24SDT_RelatorioGerencial = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_Meetrika", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP6_SDT_RelatorioGerencial=this.AV24SDT_RelatorioGerencial;
         return AV24SDT_RelatorioGerencial ;
      }

      public void executeSubmit( int aP0_Contrato_AreaTrabalhoCod ,
                                 DateTime aP1_ContagemResultado_DataDmnIn ,
                                 DateTime aP2_ContagemResultado_DataDmnFim ,
                                 int aP3_ContagemResultado_ContratadaOrigemCod ,
                                 int aP4_ContagemResultado_CntSrvCod ,
                                 String aP5_ContagemResultado_StatusDmn ,
                                 out IGxCollection aP6_SDT_RelatorioGerencial )
      {
         aprc_gerarelatoriogerencialcontagem objaprc_gerarelatoriogerencialcontagem;
         objaprc_gerarelatoriogerencialcontagem = new aprc_gerarelatoriogerencialcontagem();
         objaprc_gerarelatoriogerencialcontagem.AV30Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         objaprc_gerarelatoriogerencialcontagem.AV34ContagemResultado_DataDmnIn = aP1_ContagemResultado_DataDmnIn;
         objaprc_gerarelatoriogerencialcontagem.AV33ContagemResultado_DataDmnFim = aP2_ContagemResultado_DataDmnFim;
         objaprc_gerarelatoriogerencialcontagem.AV9ContagemResultado_ContratadaOrigemCod = aP3_ContagemResultado_ContratadaOrigemCod;
         objaprc_gerarelatoriogerencialcontagem.AV8ContagemResultado_CntSrvCod = aP4_ContagemResultado_CntSrvCod;
         objaprc_gerarelatoriogerencialcontagem.AV16ContagemResultado_StatusDmn = aP5_ContagemResultado_StatusDmn;
         objaprc_gerarelatoriogerencialcontagem.AV24SDT_RelatorioGerencial = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_Meetrika", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs") ;
         objaprc_gerarelatoriogerencialcontagem.context.SetSubmitInitialConfig(context);
         objaprc_gerarelatoriogerencialcontagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_gerarelatoriogerencialcontagem);
         aP6_SDT_RelatorioGerencial=this.AV24SDT_RelatorioGerencial;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_gerarelatoriogerencialcontagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV24SDT_RelatorioGerencial.Clear();
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34ContagemResultado_DataDmnIn ,
                                              AV33ContagemResultado_DataDmnFim ,
                                              AV16ContagemResultado_StatusDmn ,
                                              AV8ContagemResultado_CntSrvCod ,
                                              AV9ContagemResultado_ContratadaOrigemCod ,
                                              A471ContagemResultado_DataDmn ,
                                              A484ContagemResultado_StatusDmn ,
                                              A1553ContagemResultado_CntSrvCod ,
                                              A805ContagemResultado_ContratadaOrigemCod ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              AV30Contrato_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor P00XV2 */
         pr_default.execute(0, new Object[] {AV30Contrato_AreaTrabalhoCod, AV34ContagemResultado_DataDmnIn, AV33ContagemResultado_DataDmnFim, AV16ContagemResultado_StatusDmn, AV8ContagemResultado_CntSrvCod, AV9ContagemResultado_ContratadaOrigemCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKXV2 = false;
            A1603ContagemResultado_CntCod = P00XV2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00XV2_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P00XV2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00XV2_n601ContagemResultado_Servico[0];
            A75Contrato_AreaTrabalhoCod = P00XV2_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = P00XV2_n75Contrato_AreaTrabalhoCod[0];
            A808ContagemResultado_ContratadaOrigemPesNom = P00XV2_A808ContagemResultado_ContratadaOrigemPesNom[0];
            n808ContagemResultado_ContratadaOrigemPesNom = P00XV2_n808ContagemResultado_ContratadaOrigemPesNom[0];
            A494ContagemResultado_Descricao = P00XV2_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00XV2_n494ContagemResultado_Descricao[0];
            A457ContagemResultado_Demanda = P00XV2_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00XV2_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00XV2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00XV2_n493ContagemResultado_DemandaFM[0];
            A484ContagemResultado_StatusDmn = P00XV2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00XV2_n484ContagemResultado_StatusDmn[0];
            A1553ContagemResultado_CntSrvCod = P00XV2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XV2_n1553ContagemResultado_CntSrvCod[0];
            A801ContagemResultado_ServicoSigla = P00XV2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00XV2_n801ContagemResultado_ServicoSigla[0];
            A602ContagemResultado_OSVinculada = P00XV2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00XV2_n602ContagemResultado_OSVinculada[0];
            A603ContagemResultado_DmnVinculada = P00XV2_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00XV2_n603ContagemResultado_DmnVinculada[0];
            A471ContagemResultado_DataDmn = P00XV2_A471ContagemResultado_DataDmn[0];
            A805ContagemResultado_ContratadaOrigemCod = P00XV2_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P00XV2_n805ContagemResultado_ContratadaOrigemCod[0];
            A807ContagemResultado_ContratadaOrigemPesCod = P00XV2_A807ContagemResultado_ContratadaOrigemPesCod[0];
            n807ContagemResultado_ContratadaOrigemPesCod = P00XV2_n807ContagemResultado_ContratadaOrigemPesCod[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00XV2_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00XV2_n866ContagemResultado_ContratadaOrigemSigla[0];
            A456ContagemResultado_Codigo = P00XV2_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = P00XV2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00XV2_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P00XV2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00XV2_n601ContagemResultado_Servico[0];
            A75Contrato_AreaTrabalhoCod = P00XV2_A75Contrato_AreaTrabalhoCod[0];
            n75Contrato_AreaTrabalhoCod = P00XV2_n75Contrato_AreaTrabalhoCod[0];
            A801ContagemResultado_ServicoSigla = P00XV2_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00XV2_n801ContagemResultado_ServicoSigla[0];
            A603ContagemResultado_DmnVinculada = P00XV2_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00XV2_n603ContagemResultado_DmnVinculada[0];
            A807ContagemResultado_ContratadaOrigemPesCod = P00XV2_A807ContagemResultado_ContratadaOrigemPesCod[0];
            n807ContagemResultado_ContratadaOrigemPesCod = P00XV2_n807ContagemResultado_ContratadaOrigemPesCod[0];
            A866ContagemResultado_ContratadaOrigemSigla = P00XV2_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P00XV2_n866ContagemResultado_ContratadaOrigemSigla[0];
            A808ContagemResultado_ContratadaOrigemPesNom = P00XV2_A808ContagemResultado_ContratadaOrigemPesNom[0];
            n808ContagemResultado_ContratadaOrigemPesNom = P00XV2_n808ContagemResultado_ContratadaOrigemPesNom[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            AV25SDT_RelatorioGerencialContagem = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem(context);
            AV25SDT_RelatorioGerencialContagem.gxTpr_Contagemresultado_contratadaorigemcod = A805ContagemResultado_ContratadaOrigemCod;
            AV25SDT_RelatorioGerencialContagem.gxTpr_Contagemresultado_contratadaorigempescod = A807ContagemResultado_ContratadaOrigemPesCod;
            AV25SDT_RelatorioGerencialContagem.gxTpr_Contagemresultado_contratadaorigempesnom = (String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( A808ContagemResultado_ContratadaOrigemPesNom))) ? "N�o Informada" : StringUtil.Trim( A808ContagemResultado_ContratadaOrigemPesNom));
            AV25SDT_RelatorioGerencialContagem.gxTpr_Contagemresultado_contratadaorigemsigla = A866ContagemResultado_ContratadaOrigemSigla;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00XV2_A808ContagemResultado_ContratadaOrigemPesNom[0], A808ContagemResultado_ContratadaOrigemPesNom) == 0 ) )
            {
               BRKXV2 = false;
               A601ContagemResultado_Servico = P00XV2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00XV2_n601ContagemResultado_Servico[0];
               A494ContagemResultado_Descricao = P00XV2_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = P00XV2_n494ContagemResultado_Descricao[0];
               A457ContagemResultado_Demanda = P00XV2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P00XV2_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = P00XV2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = P00XV2_n493ContagemResultado_DemandaFM[0];
               A484ContagemResultado_StatusDmn = P00XV2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P00XV2_n484ContagemResultado_StatusDmn[0];
               A1553ContagemResultado_CntSrvCod = P00XV2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00XV2_n1553ContagemResultado_CntSrvCod[0];
               A801ContagemResultado_ServicoSigla = P00XV2_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00XV2_n801ContagemResultado_ServicoSigla[0];
               A602ContagemResultado_OSVinculada = P00XV2_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = P00XV2_n602ContagemResultado_OSVinculada[0];
               A603ContagemResultado_DmnVinculada = P00XV2_A603ContagemResultado_DmnVinculada[0];
               n603ContagemResultado_DmnVinculada = P00XV2_n603ContagemResultado_DmnVinculada[0];
               A471ContagemResultado_DataDmn = P00XV2_A471ContagemResultado_DataDmn[0];
               A805ContagemResultado_ContratadaOrigemCod = P00XV2_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = P00XV2_n805ContagemResultado_ContratadaOrigemCod[0];
               A807ContagemResultado_ContratadaOrigemPesCod = P00XV2_A807ContagemResultado_ContratadaOrigemPesCod[0];
               n807ContagemResultado_ContratadaOrigemPesCod = P00XV2_n807ContagemResultado_ContratadaOrigemPesCod[0];
               A456ContagemResultado_Codigo = P00XV2_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = P00XV2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00XV2_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = P00XV2_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00XV2_n801ContagemResultado_ServicoSigla[0];
               A603ContagemResultado_DmnVinculada = P00XV2_A603ContagemResultado_DmnVinculada[0];
               n603ContagemResultado_DmnVinculada = P00XV2_n603ContagemResultado_DmnVinculada[0];
               A807ContagemResultado_ContratadaOrigemPesCod = P00XV2_A807ContagemResultado_ContratadaOrigemPesCod[0];
               n807ContagemResultado_ContratadaOrigemPesCod = P00XV2_n807ContagemResultado_ContratadaOrigemPesCod[0];
               if ( ! (AV28ContagemResultado_CodigoColl.IndexOf(A456ContagemResultado_Codigo)>0) )
               {
                  GXt_decimal1 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                  A574ContagemResultado_PFFinal = GXt_decimal1;
                  AV39IsPossuiDivergencia = false;
                  AV25SDT_RelatorioGerencialContagem.gxTpr_Totalos = (short)(AV25SDT_RelatorioGerencialContagem.gxTpr_Totalos+1);
                  AV26SDT_RelatorioGerencialContagemOS = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS(context);
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_descricao = A494ContagemResultado_Descricao;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_nnref = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_demanda = A457ContagemResultado_Demanda;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_demandafm = A493ContagemResultado_DemandaFM;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_statusdmn = A484ContagemResultado_StatusDmn;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_statusdescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_cntsrvcod = A1553ContagemResultado_CntSrvCod;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_servicosigla = A801ContagemResultado_ServicoSigla;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_osvinculada = A602ContagemResultado_OSVinculada;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_dmnvinculada = A603ContagemResultado_DmnVinculada;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf = A574ContagemResultado_PFFinal;
                  AV12ContagemResultado_DataCntInicial = DateTime.MinValue;
                  AV15ContagemResultado_PFLFS_Inicial = 0;
                  /* Using cursor P00XV3 */
                  pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(1) != 101) )
                  {
                     A483ContagemResultado_StatusCnt = P00XV3_A483ContagemResultado_StatusCnt[0];
                     A459ContagemResultado_PFLFS = P00XV3_A459ContagemResultado_PFLFS[0];
                     n459ContagemResultado_PFLFS = P00XV3_n459ContagemResultado_PFLFS[0];
                     A511ContagemResultado_HoraCnt = P00XV3_A511ContagemResultado_HoraCnt[0];
                     A473ContagemResultado_DataCnt = P00XV3_A473ContagemResultado_DataCnt[0];
                     AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_datacontageminicial = A473ContagemResultado_DataCnt;
                     AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial = A459ContagemResultado_PFLFS;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(1);
                  }
                  pr_default.close(1);
                  AV11ContagemResultado_DataCntFinal = DateTime.MinValue;
                  /* Using cursor P00XV4 */
                  pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A483ContagemResultado_StatusCnt = P00XV4_A483ContagemResultado_StatusCnt[0];
                     A459ContagemResultado_PFLFS = P00XV4_A459ContagemResultado_PFLFS[0];
                     n459ContagemResultado_PFLFS = P00XV4_n459ContagemResultado_PFLFS[0];
                     A511ContagemResultado_HoraCnt = P00XV4_A511ContagemResultado_HoraCnt[0];
                     A473ContagemResultado_DataCnt = P00XV4_A473ContagemResultado_DataCnt[0];
                     AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_datacontagemfinal = A473ContagemResultado_DataCnt;
                     AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final = A459ContagemResultado_PFLFS;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
                  if ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final > AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial )
                  {
                     AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca = (decimal)(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final-AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial);
                  }
                  else
                  {
                     AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca = (decimal)(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial-AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final);
                  }
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total = AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total = AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total = AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca;
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total = AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf;
                  /* Execute user subroutine: 'CARREGA.VINVULOS' */
                  S111 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     this.cleanup();
                     if (true) return;
                  }
                  if ( ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca > Convert.ToDecimal( 0 )) && ! AV39IsPossuiDivergencia )
                  {
                     AV25SDT_RelatorioGerencialContagem.gxTpr_Totalosdivergentes = (short)(AV25SDT_RelatorioGerencialContagem.gxTpr_Totalosdivergentes+1);
                     AV25SDT_RelatorioGerencialContagem.gxTpr_Totalpfdivergentes = (decimal)(AV25SDT_RelatorioGerencialContagem.gxTpr_Totalpfdivergentes+(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total));
                  }
                  AV26SDT_RelatorioGerencialContagemOS.gxTpr_Osvinculada.Sort("ContagemResultado_DataDmn") ;
                  AV25SDT_RelatorioGerencialContagem.gxTpr_Os.Add(AV26SDT_RelatorioGerencialContagemOS, 0);
                  AV28ContagemResultado_CodigoColl.Add(A456ContagemResultado_Codigo, 0);
               }
               BRKXV2 = true;
               pr_default.readNext(0);
            }
            AV25SDT_RelatorioGerencialContagem.gxTpr_Os.Sort("ContagemResultado_DataDmn") ;
            AV24SDT_RelatorioGerencial.Add(AV25SDT_RelatorioGerencialContagem, 0);
            if ( ! BRKXV2 )
            {
               BRKXV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
         AV24SDT_RelatorioGerencial.Sort("ContagemResultado_ContratadaOrigemPesNom");
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CARREGA.VINVULOS' Routine */
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV28ContagemResultado_CodigoColl ,
                                              A602ContagemResultado_OSVinculada ,
                                              AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor P00XV5 */
         pr_default.execute(3, new Object[] {AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A601ContagemResultado_Servico = P00XV5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00XV5_n601ContagemResultado_Servico[0];
            A602ContagemResultado_OSVinculada = P00XV5_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00XV5_n602ContagemResultado_OSVinculada[0];
            A494ContagemResultado_Descricao = P00XV5_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00XV5_n494ContagemResultado_Descricao[0];
            A457ContagemResultado_Demanda = P00XV5_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00XV5_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00XV5_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00XV5_n493ContagemResultado_DemandaFM[0];
            A484ContagemResultado_StatusDmn = P00XV5_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00XV5_n484ContagemResultado_StatusDmn[0];
            A1553ContagemResultado_CntSrvCod = P00XV5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XV5_n1553ContagemResultado_CntSrvCod[0];
            A801ContagemResultado_ServicoSigla = P00XV5_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00XV5_n801ContagemResultado_ServicoSigla[0];
            A603ContagemResultado_DmnVinculada = P00XV5_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00XV5_n603ContagemResultado_DmnVinculada[0];
            A471ContagemResultado_DataDmn = P00XV5_A471ContagemResultado_DataDmn[0];
            A456ContagemResultado_Codigo = P00XV5_A456ContagemResultado_Codigo[0];
            A603ContagemResultado_DmnVinculada = P00XV5_A603ContagemResultado_DmnVinculada[0];
            n603ContagemResultado_DmnVinculada = P00XV5_n603ContagemResultado_DmnVinculada[0];
            A601ContagemResultado_Servico = P00XV5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00XV5_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00XV5_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00XV5_n801ContagemResultado_ServicoSigla[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            AV29SDT_RelatorioGerencialContagemOSVinculada = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada(context);
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_codigo = A456ContagemResultado_Codigo;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_descricao = A494ContagemResultado_Descricao;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_nnref = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_demanda = A457ContagemResultado_Demanda;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_demandafm = A493ContagemResultado_DemandaFM;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_statusdmn = A484ContagemResultado_StatusDmn;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_statusdescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_cntsrvcod = A1553ContagemResultado_CntSrvCod;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_servicosigla = A801ContagemResultado_ServicoSigla;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_osvinculada = A602ContagemResultado_OSVinculada;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_dmnvinculada = A603ContagemResultado_DmnVinculada;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_nnrefvinculada = AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_nnref;
            AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pf = A574ContagemResultado_PFFinal;
            AV12ContagemResultado_DataCntInicial = DateTime.MinValue;
            AV15ContagemResultado_PFLFS_Inicial = 0;
            /* Using cursor P00XV6 */
            pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A483ContagemResultado_StatusCnt = P00XV6_A483ContagemResultado_StatusCnt[0];
               A459ContagemResultado_PFLFS = P00XV6_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P00XV6_n459ContagemResultado_PFLFS[0];
               A511ContagemResultado_HoraCnt = P00XV6_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = P00XV6_A473ContagemResultado_DataCnt[0];
               AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_datacontageminicial = A473ContagemResultado_DataCnt;
               AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial = A459ContagemResultado_PFLFS;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
            AV11ContagemResultado_DataCntFinal = DateTime.MinValue;
            /* Using cursor P00XV7 */
            pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A483ContagemResultado_StatusCnt = P00XV7_A483ContagemResultado_StatusCnt[0];
               A459ContagemResultado_PFLFS = P00XV7_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P00XV7_n459ContagemResultado_PFLFS[0];
               A511ContagemResultado_HoraCnt = P00XV7_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = P00XV7_A473ContagemResultado_DataCnt[0];
               AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_datacontagemfinal = A473ContagemResultado_DataCnt;
               AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final = A459ContagemResultado_PFLFS;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(5);
            }
            pr_default.close(5);
            if ( AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final > AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial )
            {
               AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_diferenca = (decimal)(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final-AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial);
            }
            else
            {
               AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_diferenca = (decimal)(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial-AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final);
            }
            if ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total > AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial )
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total = (decimal)(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total-(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial));
            }
            else
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total = (decimal)(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial-AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total);
            }
            if ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total > AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final )
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total = (decimal)(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total-(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final));
            }
            else
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total = (decimal)(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final-AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total);
            }
            if ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total > AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_diferenca )
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total = (decimal)(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total-(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_diferenca));
            }
            else
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total = (decimal)(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_diferenca-AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total);
            }
            if ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total > AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pf )
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total = (decimal)(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total-(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pf));
            }
            else
            {
               AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total = (decimal)(AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pf-AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total);
            }
            if ( ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total > Convert.ToDecimal( 0 )) || ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total > Convert.ToDecimal( 0 )) || ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total > Convert.ToDecimal( 0 )) || ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total > Convert.ToDecimal( 0 )) )
            {
               AV25SDT_RelatorioGerencialContagem.gxTpr_Totalosdivergentes = (short)(AV25SDT_RelatorioGerencialContagem.gxTpr_Totalosdivergentes+1);
               AV25SDT_RelatorioGerencialContagem.gxTpr_Totalpfdivergentes = (decimal)(AV25SDT_RelatorioGerencialContagem.gxTpr_Totalpfdivergentes+(AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total));
               AV39IsPossuiDivergencia = true;
            }
            if ( ( AV26SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca > Convert.ToDecimal( 0 )) && ! AV39IsPossuiDivergencia )
            {
            }
            AV26SDT_RelatorioGerencialContagemOS.gxTpr_Osvinculada.Add(AV29SDT_RelatorioGerencialContagemOSVinculada, 0);
            AV28ContagemResultado_CodigoColl.Add(A456ContagemResultado_Codigo, 0);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S121( )
      {
         /* 'BUSCA.DADOS.CONTAGEM' Routine */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         P00XV2_A1603ContagemResultado_CntCod = new int[1] ;
         P00XV2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00XV2_A601ContagemResultado_Servico = new int[1] ;
         P00XV2_n601ContagemResultado_Servico = new bool[] {false} ;
         P00XV2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00XV2_n75Contrato_AreaTrabalhoCod = new bool[] {false} ;
         P00XV2_A808ContagemResultado_ContratadaOrigemPesNom = new String[] {""} ;
         P00XV2_n808ContagemResultado_ContratadaOrigemPesNom = new bool[] {false} ;
         P00XV2_A494ContagemResultado_Descricao = new String[] {""} ;
         P00XV2_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00XV2_A457ContagemResultado_Demanda = new String[] {""} ;
         P00XV2_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00XV2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00XV2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00XV2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00XV2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00XV2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XV2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XV2_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00XV2_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00XV2_A602ContagemResultado_OSVinculada = new int[1] ;
         P00XV2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00XV2_A603ContagemResultado_DmnVinculada = new String[] {""} ;
         P00XV2_n603ContagemResultado_DmnVinculada = new bool[] {false} ;
         P00XV2_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00XV2_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P00XV2_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P00XV2_A807ContagemResultado_ContratadaOrigemPesCod = new int[1] ;
         P00XV2_n807ContagemResultado_ContratadaOrigemPesCod = new bool[] {false} ;
         P00XV2_A866ContagemResultado_ContratadaOrigemSigla = new String[] {""} ;
         P00XV2_n866ContagemResultado_ContratadaOrigemSigla = new bool[] {false} ;
         P00XV2_A456ContagemResultado_Codigo = new int[1] ;
         A808ContagemResultado_ContratadaOrigemPesNom = "";
         A494ContagemResultado_Descricao = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A801ContagemResultado_ServicoSigla = "";
         A603ContagemResultado_DmnVinculada = "";
         A866ContagemResultado_ContratadaOrigemSigla = "";
         AV25SDT_RelatorioGerencialContagem = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem(context);
         AV28ContagemResultado_CodigoColl = new GxSimpleCollection();
         AV26SDT_RelatorioGerencialContagemOS = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS(context);
         AV12ContagemResultado_DataCntInicial = DateTime.MinValue;
         P00XV3_A456ContagemResultado_Codigo = new int[1] ;
         P00XV3_A483ContagemResultado_StatusCnt = new short[1] ;
         P00XV3_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00XV3_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00XV3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00XV3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         AV11ContagemResultado_DataCntFinal = DateTime.MinValue;
         P00XV4_A456ContagemResultado_Codigo = new int[1] ;
         P00XV4_A483ContagemResultado_StatusCnt = new short[1] ;
         P00XV4_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00XV4_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00XV4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00XV4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00XV5_A601ContagemResultado_Servico = new int[1] ;
         P00XV5_n601ContagemResultado_Servico = new bool[] {false} ;
         P00XV5_A602ContagemResultado_OSVinculada = new int[1] ;
         P00XV5_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00XV5_A494ContagemResultado_Descricao = new String[] {""} ;
         P00XV5_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00XV5_A457ContagemResultado_Demanda = new String[] {""} ;
         P00XV5_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00XV5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00XV5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00XV5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00XV5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00XV5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XV5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XV5_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00XV5_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00XV5_A603ContagemResultado_DmnVinculada = new String[] {""} ;
         P00XV5_n603ContagemResultado_DmnVinculada = new bool[] {false} ;
         P00XV5_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00XV5_A456ContagemResultado_Codigo = new int[1] ;
         AV29SDT_RelatorioGerencialContagemOSVinculada = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada(context);
         P00XV6_A456ContagemResultado_Codigo = new int[1] ;
         P00XV6_A483ContagemResultado_StatusCnt = new short[1] ;
         P00XV6_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00XV6_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00XV6_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00XV6_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00XV7_A456ContagemResultado_Codigo = new int[1] ;
         P00XV7_A483ContagemResultado_StatusCnt = new short[1] ;
         P00XV7_A459ContagemResultado_PFLFS = new decimal[1] ;
         P00XV7_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P00XV7_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00XV7_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_gerarelatoriogerencialcontagem__default(),
            new Object[][] {
                new Object[] {
               P00XV2_A1603ContagemResultado_CntCod, P00XV2_n1603ContagemResultado_CntCod, P00XV2_A601ContagemResultado_Servico, P00XV2_n601ContagemResultado_Servico, P00XV2_A75Contrato_AreaTrabalhoCod, P00XV2_n75Contrato_AreaTrabalhoCod, P00XV2_A808ContagemResultado_ContratadaOrigemPesNom, P00XV2_n808ContagemResultado_ContratadaOrigemPesNom, P00XV2_A494ContagemResultado_Descricao, P00XV2_n494ContagemResultado_Descricao,
               P00XV2_A457ContagemResultado_Demanda, P00XV2_n457ContagemResultado_Demanda, P00XV2_A493ContagemResultado_DemandaFM, P00XV2_n493ContagemResultado_DemandaFM, P00XV2_A484ContagemResultado_StatusDmn, P00XV2_n484ContagemResultado_StatusDmn, P00XV2_A1553ContagemResultado_CntSrvCod, P00XV2_n1553ContagemResultado_CntSrvCod, P00XV2_A801ContagemResultado_ServicoSigla, P00XV2_n801ContagemResultado_ServicoSigla,
               P00XV2_A602ContagemResultado_OSVinculada, P00XV2_n602ContagemResultado_OSVinculada, P00XV2_A603ContagemResultado_DmnVinculada, P00XV2_n603ContagemResultado_DmnVinculada, P00XV2_A471ContagemResultado_DataDmn, P00XV2_A805ContagemResultado_ContratadaOrigemCod, P00XV2_n805ContagemResultado_ContratadaOrigemCod, P00XV2_A807ContagemResultado_ContratadaOrigemPesCod, P00XV2_n807ContagemResultado_ContratadaOrigemPesCod, P00XV2_A866ContagemResultado_ContratadaOrigemSigla,
               P00XV2_n866ContagemResultado_ContratadaOrigemSigla, P00XV2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00XV3_A456ContagemResultado_Codigo, P00XV3_A483ContagemResultado_StatusCnt, P00XV3_A459ContagemResultado_PFLFS, P00XV3_n459ContagemResultado_PFLFS, P00XV3_A511ContagemResultado_HoraCnt, P00XV3_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               P00XV4_A456ContagemResultado_Codigo, P00XV4_A483ContagemResultado_StatusCnt, P00XV4_A459ContagemResultado_PFLFS, P00XV4_n459ContagemResultado_PFLFS, P00XV4_A511ContagemResultado_HoraCnt, P00XV4_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               P00XV5_A601ContagemResultado_Servico, P00XV5_n601ContagemResultado_Servico, P00XV5_A602ContagemResultado_OSVinculada, P00XV5_n602ContagemResultado_OSVinculada, P00XV5_A494ContagemResultado_Descricao, P00XV5_n494ContagemResultado_Descricao, P00XV5_A457ContagemResultado_Demanda, P00XV5_n457ContagemResultado_Demanda, P00XV5_A493ContagemResultado_DemandaFM, P00XV5_n493ContagemResultado_DemandaFM,
               P00XV5_A484ContagemResultado_StatusDmn, P00XV5_n484ContagemResultado_StatusDmn, P00XV5_A1553ContagemResultado_CntSrvCod, P00XV5_n1553ContagemResultado_CntSrvCod, P00XV5_A801ContagemResultado_ServicoSigla, P00XV5_n801ContagemResultado_ServicoSigla, P00XV5_A603ContagemResultado_DmnVinculada, P00XV5_n603ContagemResultado_DmnVinculada, P00XV5_A471ContagemResultado_DataDmn, P00XV5_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00XV6_A456ContagemResultado_Codigo, P00XV6_A483ContagemResultado_StatusCnt, P00XV6_A459ContagemResultado_PFLFS, P00XV6_n459ContagemResultado_PFLFS, P00XV6_A511ContagemResultado_HoraCnt, P00XV6_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               P00XV7_A456ContagemResultado_Codigo, P00XV7_A483ContagemResultado_StatusCnt, P00XV7_A459ContagemResultado_PFLFS, P00XV7_n459ContagemResultado_PFLFS, P00XV7_A511ContagemResultado_HoraCnt, P00XV7_A473ContagemResultado_DataCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A483ContagemResultado_StatusCnt ;
      private int AV30Contrato_AreaTrabalhoCod ;
      private int AV9ContagemResultado_ContratadaOrigemCod ;
      private int AV8ContagemResultado_CntSrvCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A601ContagemResultado_Servico ;
      private int A602ContagemResultado_OSVinculada ;
      private int A807ContagemResultado_ContratadaOrigemPesCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV26SDT_RelatorioGerencialContagemOS_gxTpr_Contagemresultado_codigo ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV15ContagemResultado_PFLFS_Inicial ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal GXt_decimal1 ;
      private String AV16ContagemResultado_StatusDmn ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A808ContagemResultado_ContratadaOrigemPesNom ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A866ContagemResultado_ContratadaOrigemSigla ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime AV34ContagemResultado_DataDmnIn ;
      private DateTime AV33ContagemResultado_DataDmnFim ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV12ContagemResultado_DataCntInicial ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV11ContagemResultado_DataCntFinal ;
      private bool BRKXV2 ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n75Contrato_AreaTrabalhoCod ;
      private bool n808ContagemResultado_ContratadaOrigemPesNom ;
      private bool n494ContagemResultado_Descricao ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n603ContagemResultado_DmnVinculada ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n807ContagemResultado_ContratadaOrigemPesCod ;
      private bool n866ContagemResultado_ContratadaOrigemSigla ;
      private bool AV39IsPossuiDivergencia ;
      private bool n459ContagemResultado_PFLFS ;
      private bool returnInSub ;
      private String A494ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A603ContagemResultado_DmnVinculada ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV28ContagemResultado_CodigoColl ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XV2_A1603ContagemResultado_CntCod ;
      private bool[] P00XV2_n1603ContagemResultado_CntCod ;
      private int[] P00XV2_A601ContagemResultado_Servico ;
      private bool[] P00XV2_n601ContagemResultado_Servico ;
      private int[] P00XV2_A75Contrato_AreaTrabalhoCod ;
      private bool[] P00XV2_n75Contrato_AreaTrabalhoCod ;
      private String[] P00XV2_A808ContagemResultado_ContratadaOrigemPesNom ;
      private bool[] P00XV2_n808ContagemResultado_ContratadaOrigemPesNom ;
      private String[] P00XV2_A494ContagemResultado_Descricao ;
      private bool[] P00XV2_n494ContagemResultado_Descricao ;
      private String[] P00XV2_A457ContagemResultado_Demanda ;
      private bool[] P00XV2_n457ContagemResultado_Demanda ;
      private String[] P00XV2_A493ContagemResultado_DemandaFM ;
      private bool[] P00XV2_n493ContagemResultado_DemandaFM ;
      private String[] P00XV2_A484ContagemResultado_StatusDmn ;
      private bool[] P00XV2_n484ContagemResultado_StatusDmn ;
      private int[] P00XV2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XV2_n1553ContagemResultado_CntSrvCod ;
      private String[] P00XV2_A801ContagemResultado_ServicoSigla ;
      private bool[] P00XV2_n801ContagemResultado_ServicoSigla ;
      private int[] P00XV2_A602ContagemResultado_OSVinculada ;
      private bool[] P00XV2_n602ContagemResultado_OSVinculada ;
      private String[] P00XV2_A603ContagemResultado_DmnVinculada ;
      private bool[] P00XV2_n603ContagemResultado_DmnVinculada ;
      private DateTime[] P00XV2_A471ContagemResultado_DataDmn ;
      private int[] P00XV2_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P00XV2_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P00XV2_A807ContagemResultado_ContratadaOrigemPesCod ;
      private bool[] P00XV2_n807ContagemResultado_ContratadaOrigemPesCod ;
      private String[] P00XV2_A866ContagemResultado_ContratadaOrigemSigla ;
      private bool[] P00XV2_n866ContagemResultado_ContratadaOrigemSigla ;
      private int[] P00XV2_A456ContagemResultado_Codigo ;
      private int[] P00XV3_A456ContagemResultado_Codigo ;
      private short[] P00XV3_A483ContagemResultado_StatusCnt ;
      private decimal[] P00XV3_A459ContagemResultado_PFLFS ;
      private bool[] P00XV3_n459ContagemResultado_PFLFS ;
      private String[] P00XV3_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00XV3_A473ContagemResultado_DataCnt ;
      private int[] P00XV4_A456ContagemResultado_Codigo ;
      private short[] P00XV4_A483ContagemResultado_StatusCnt ;
      private decimal[] P00XV4_A459ContagemResultado_PFLFS ;
      private bool[] P00XV4_n459ContagemResultado_PFLFS ;
      private String[] P00XV4_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00XV4_A473ContagemResultado_DataCnt ;
      private int[] P00XV5_A601ContagemResultado_Servico ;
      private bool[] P00XV5_n601ContagemResultado_Servico ;
      private int[] P00XV5_A602ContagemResultado_OSVinculada ;
      private bool[] P00XV5_n602ContagemResultado_OSVinculada ;
      private String[] P00XV5_A494ContagemResultado_Descricao ;
      private bool[] P00XV5_n494ContagemResultado_Descricao ;
      private String[] P00XV5_A457ContagemResultado_Demanda ;
      private bool[] P00XV5_n457ContagemResultado_Demanda ;
      private String[] P00XV5_A493ContagemResultado_DemandaFM ;
      private bool[] P00XV5_n493ContagemResultado_DemandaFM ;
      private String[] P00XV5_A484ContagemResultado_StatusDmn ;
      private bool[] P00XV5_n484ContagemResultado_StatusDmn ;
      private int[] P00XV5_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XV5_n1553ContagemResultado_CntSrvCod ;
      private String[] P00XV5_A801ContagemResultado_ServicoSigla ;
      private bool[] P00XV5_n801ContagemResultado_ServicoSigla ;
      private String[] P00XV5_A603ContagemResultado_DmnVinculada ;
      private bool[] P00XV5_n603ContagemResultado_DmnVinculada ;
      private DateTime[] P00XV5_A471ContagemResultado_DataDmn ;
      private int[] P00XV5_A456ContagemResultado_Codigo ;
      private int[] P00XV6_A456ContagemResultado_Codigo ;
      private short[] P00XV6_A483ContagemResultado_StatusCnt ;
      private decimal[] P00XV6_A459ContagemResultado_PFLFS ;
      private bool[] P00XV6_n459ContagemResultado_PFLFS ;
      private String[] P00XV6_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00XV6_A473ContagemResultado_DataCnt ;
      private int[] P00XV7_A456ContagemResultado_Codigo ;
      private short[] P00XV7_A483ContagemResultado_StatusCnt ;
      private decimal[] P00XV7_A459ContagemResultado_PFLFS ;
      private bool[] P00XV7_n459ContagemResultado_PFLFS ;
      private String[] P00XV7_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00XV7_A473ContagemResultado_DataCnt ;
      private IGxCollection aP6_SDT_RelatorioGerencial ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem ))]
      private IGxCollection AV24SDT_RelatorioGerencial ;
      private SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem AV25SDT_RelatorioGerencialContagem ;
      private SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS AV26SDT_RelatorioGerencialContagemOS ;
      private SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada AV29SDT_RelatorioGerencialContagemOSVinculada ;
   }

   public class aprc_gerarelatoriogerencialcontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00XV2( IGxContext context ,
                                             DateTime AV34ContagemResultado_DataDmnIn ,
                                             DateTime AV33ContagemResultado_DataDmnFim ,
                                             String AV16ContagemResultado_StatusDmn ,
                                             int AV8ContagemResultado_CntSrvCod ,
                                             int AV9ContagemResultado_ContratadaOrigemCod ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int A1553ContagemResultado_CntSrvCod ,
                                             int A805ContagemResultado_ContratadaOrigemCod ,
                                             int A75Contrato_AreaTrabalhoCod ,
                                             int AV30Contrato_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [6] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[Contrato_AreaTrabalhoCod], T7.[Pessoa_Nome] AS ContagemResultado_ContratadaOrigemPesNom, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T5.[ContagemResultado_Demanda] AS ContagemResultado_DmnVinculada, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOrigemCod, T6.[Contratada_PessoaCod] AS ContagemResultado_ContratadaOrigemPesCod, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaOrigemSigla, T1.[ContagemResultado_Codigo] FROM (((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T5 WITH (NOLOCK) ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (T3.[Contrato_AreaTrabalhoCod] = @AV30Contrato_AreaTrabalhoCod)";
         if ( ! (DateTime.MinValue==AV34ContagemResultado_DataDmnIn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV34ContagemResultado_DataDmnIn)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV33ContagemResultado_DataDmnFim) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV33ContagemResultado_DataDmnFim)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ContagemResultado_StatusDmn)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV16ContagemResultado_StatusDmn)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV8ContagemResultado_CntSrvCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_CntSrvCod] = @AV8ContagemResultado_CntSrvCod)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV9ContagemResultado_ContratadaOrigemCod) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaOrigemCod] = @AV9ContagemResultado_ContratadaOrigemCod)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T7.[Pessoa_Nome], T1.[ContagemResultado_DataDmn]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00XV5( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV28ContagemResultado_CodigoColl ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int AV26SDT_RelatorioGerencialContagemOS_gxTpr_Contagemresultado_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [1] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[ContagemResultado_Demanda] AS ContagemResultado_DmnVinculada, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Codigo] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV28ContagemResultado_CodigoColl, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_OSVinculada] = @AV26SDT__1Contagemresultado_c)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataDmn]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00XV2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
               case 3 :
                     return conditional_P00XV5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XV3 ;
          prmP00XV3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XV4 ;
          prmP00XV4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XV6 ;
          prmP00XV6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XV7 ;
          prmP00XV7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XV2 ;
          prmP00XV2 = new Object[] {
          new Object[] {"@AV30Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV34ContagemResultado_DataDmnIn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33ContagemResultado_DataDmnFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@AV8ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XV5 ;
          prmP00XV5 = new Object[] {
          new Object[] {"@AV26SDT__1Contagemresultado_c",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XV2,100,0,true,false )
             ,new CursorDef("P00XV3", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_PFLFS], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_StatusCnt] = 5) ORDER BY [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XV3,1,0,false,true )
             ,new CursorDef("P00XV4", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_PFLFS], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_StatusCnt] = 5) ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XV4,1,0,false,true )
             ,new CursorDef("P00XV5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XV5,100,0,true,false )
             ,new CursorDef("P00XV6", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_PFLFS], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_StatusCnt] = 5) AND ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XV6,1,0,false,true )
             ,new CursorDef("P00XV7", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_StatusCnt], [ContagemResultado_PFLFS], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_StatusCnt] = 5) ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XV7,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(13) ;
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((int[]) buf[27])[0] = rslt.getInt(15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((String[]) buf[29])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(10) ;
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
