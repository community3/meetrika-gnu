/*
               File: PRC_NewOSNaoCnf
        Description: PRC_New OSNao Cnf
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:22.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newosnaocnf : GXProcedure
   {
      public prc_newosnaocnf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newosnaocnf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoNaoCnf_OSCod ,
                           ref int aP1_ContagemResultadoNaoCnf_NaoCnfCod ,
                           ref bool aP2_ContagemResultadoNaoCnf_OSEntregue ,
                           long aP3_LogResponsavel_Codigo )
      {
         this.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         this.A2026ContagemResultadoNaoCnf_NaoCnfCod = aP1_ContagemResultadoNaoCnf_NaoCnfCod;
         this.A2053ContagemResultadoNaoCnf_OSEntregue = aP2_ContagemResultadoNaoCnf_OSEntregue;
         this.AV8LogResponsavel_Codigo = aP3_LogResponsavel_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
         aP1_ContagemResultadoNaoCnf_NaoCnfCod=this.A2026ContagemResultadoNaoCnf_NaoCnfCod;
         aP2_ContagemResultadoNaoCnf_OSEntregue=this.A2053ContagemResultadoNaoCnf_OSEntregue;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoNaoCnf_OSCod ,
                                 ref int aP1_ContagemResultadoNaoCnf_NaoCnfCod ,
                                 ref bool aP2_ContagemResultadoNaoCnf_OSEntregue ,
                                 long aP3_LogResponsavel_Codigo )
      {
         prc_newosnaocnf objprc_newosnaocnf;
         objprc_newosnaocnf = new prc_newosnaocnf();
         objprc_newosnaocnf.A2020ContagemResultadoNaoCnf_OSCod = aP0_ContagemResultadoNaoCnf_OSCod;
         objprc_newosnaocnf.A2026ContagemResultadoNaoCnf_NaoCnfCod = aP1_ContagemResultadoNaoCnf_NaoCnfCod;
         objprc_newosnaocnf.A2053ContagemResultadoNaoCnf_OSEntregue = aP2_ContagemResultadoNaoCnf_OSEntregue;
         objprc_newosnaocnf.AV8LogResponsavel_Codigo = aP3_LogResponsavel_Codigo;
         objprc_newosnaocnf.context.SetSubmitInitialConfig(context);
         objprc_newosnaocnf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newosnaocnf);
         aP0_ContagemResultadoNaoCnf_OSCod=this.A2020ContagemResultadoNaoCnf_OSCod;
         aP1_ContagemResultadoNaoCnf_NaoCnfCod=this.A2026ContagemResultadoNaoCnf_NaoCnfCod;
         aP2_ContagemResultadoNaoCnf_OSEntregue=this.A2053ContagemResultadoNaoCnf_OSEntregue;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newosnaocnf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /*
            INSERT RECORD ON TABLE ContagemResultadoNaoCnf

         */
         A2023ContagemResultadoNaoCnf_LogRspCod = AV8LogResponsavel_Codigo;
         A2060ContagemResultadoNaoCnf_IndCod = 0;
         n2060ContagemResultadoNaoCnf_IndCod = false;
         n2060ContagemResultadoNaoCnf_IndCod = true;
         /* Using cursor P00WB2 */
         pr_default.execute(0, new Object[] {A2020ContagemResultadoNaoCnf_OSCod, A2023ContagemResultadoNaoCnf_LogRspCod, A2026ContagemResultadoNaoCnf_NaoCnfCod, n2053ContagemResultadoNaoCnf_OSEntregue, A2053ContagemResultadoNaoCnf_OSEntregue, n2060ContagemResultadoNaoCnf_IndCod, A2060ContagemResultadoNaoCnf_IndCod});
         A2024ContagemResultadoNaoCnf_Codigo = P00WB2_A2024ContagemResultadoNaoCnf_Codigo[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNaoCnf") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         P00WB2_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newosnaocnf__default(),
            new Object[][] {
                new Object[] {
               P00WB2_A2024ContagemResultadoNaoCnf_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A2020ContagemResultadoNaoCnf_OSCod ;
      private int A2026ContagemResultadoNaoCnf_NaoCnfCod ;
      private int GX_INS224 ;
      private int A2060ContagemResultadoNaoCnf_IndCod ;
      private int A2024ContagemResultadoNaoCnf_Codigo ;
      private long AV8LogResponsavel_Codigo ;
      private long A2023ContagemResultadoNaoCnf_LogRspCod ;
      private String Gx_emsg ;
      private bool A2053ContagemResultadoNaoCnf_OSEntregue ;
      private bool n2060ContagemResultadoNaoCnf_IndCod ;
      private bool n2053ContagemResultadoNaoCnf_OSEntregue ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoNaoCnf_OSCod ;
      private int aP1_ContagemResultadoNaoCnf_NaoCnfCod ;
      private bool aP2_ContagemResultadoNaoCnf_OSEntregue ;
      private IDataStoreProvider pr_default ;
      private int[] P00WB2_A2024ContagemResultadoNaoCnf_Codigo ;
   }

   public class prc_newosnaocnf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WB2 ;
          prmP00WB2 = new Object[] {
          new Object[] {"@ContagemResultadoNaoCnf_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_LogRspCod",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_NaoCnfCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_OSEntregue",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNaoCnf_IndCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WB2", "INSERT INTO [ContagemResultadoNaoCnf]([ContagemResultadoNaoCnf_OSCod], [ContagemResultadoNaoCnf_LogRspCod], [ContagemResultadoNaoCnf_NaoCnfCod], [ContagemResultadoNaoCnf_OSEntregue], [ContagemResultadoNaoCnf_IndCod], [ContagemResultadoNaoCnf_Contestada]) VALUES(@ContagemResultadoNaoCnf_OSCod, @ContagemResultadoNaoCnf_LogRspCod, @ContagemResultadoNaoCnf_NaoCnfCod, @ContagemResultadoNaoCnf_OSEntregue, @ContagemResultadoNaoCnf_IndCod, convert(bit, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00WB2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (long)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[6]);
                }
                return;
       }
    }

 }

}
