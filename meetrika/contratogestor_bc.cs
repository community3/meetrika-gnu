/*
               File: ContratoGestor_BC
        Description: Gestor do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:27:17.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratogestor_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratogestor_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratogestor_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow35129( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey35129( ) ;
         standaloneModal( ) ;
         AddRow35129( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1078ContratoGestor_ContratoCod = A1078ContratoGestor_ContratoCod;
               Z1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_350( )
      {
         BeforeValidate35129( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls35129( ) ;
            }
            else
            {
               CheckExtendedTable35129( ) ;
               if ( AnyError == 0 )
               {
                  ZM35129( 4) ;
                  ZM35129( 5) ;
                  ZM35129( 6) ;
                  ZM35129( 7) ;
                  ZM35129( 8) ;
               }
               CloseExtendedTableCursors35129( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM35129( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z2009ContratoGestor_Tipo = A2009ContratoGestor_Tipo;
            Z1135ContratoGestor_UsuarioEhContratante = A1135ContratoGestor_UsuarioEhContratante;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z1136ContratoGestor_ContratadaCod = A1136ContratoGestor_ContratadaCod;
            Z1446ContratoGestor_ContratadaAreaCod = A1446ContratoGestor_ContratadaAreaCod;
            Z1135ContratoGestor_UsuarioEhContratante = A1135ContratoGestor_UsuarioEhContratante;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z2033ContratoGestor_UsuarioAtv = A2033ContratoGestor_UsuarioAtv;
            Z1080ContratoGestor_UsuarioPesCod = A1080ContratoGestor_UsuarioPesCod;
            Z1135ContratoGestor_UsuarioEhContratante = A1135ContratoGestor_UsuarioEhContratante;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z1223ContratoGestor_ContratadaSigla = A1223ContratoGestor_ContratadaSigla;
            Z1137ContratoGestor_ContratadaTipo = A1137ContratoGestor_ContratadaTipo;
            Z1135ContratoGestor_UsuarioEhContratante = A1135ContratoGestor_UsuarioEhContratante;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z1447ContratoGestor_ContratadaAreaDes = A1447ContratoGestor_ContratadaAreaDes;
            Z1135ContratoGestor_UsuarioEhContratante = A1135ContratoGestor_UsuarioEhContratante;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z1081ContratoGestor_UsuarioPesNom = A1081ContratoGestor_UsuarioPesNom;
            Z1135ContratoGestor_UsuarioEhContratante = A1135ContratoGestor_UsuarioEhContratante;
         }
         if ( GX_JID == -3 )
         {
            Z2009ContratoGestor_Tipo = A2009ContratoGestor_Tipo;
            Z1078ContratoGestor_ContratoCod = A1078ContratoGestor_ContratoCod;
            Z1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
            Z1136ContratoGestor_ContratadaCod = A1136ContratoGestor_ContratadaCod;
            Z1446ContratoGestor_ContratadaAreaCod = A1446ContratoGestor_ContratadaAreaCod;
            Z1223ContratoGestor_ContratadaSigla = A1223ContratoGestor_ContratadaSigla;
            Z1137ContratoGestor_ContratadaTipo = A1137ContratoGestor_ContratadaTipo;
            Z1447ContratoGestor_ContratadaAreaDes = A1447ContratoGestor_ContratadaAreaDes;
            Z2033ContratoGestor_UsuarioAtv = A2033ContratoGestor_UsuarioAtv;
            Z1080ContratoGestor_UsuarioPesCod = A1080ContratoGestor_UsuarioPesCod;
            Z1081ContratoGestor_UsuarioPesNom = A1081ContratoGestor_UsuarioPesNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load35129( )
      {
         /* Using cursor BC00359 */
         pr_default.execute(7, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound129 = 1;
            A1223ContratoGestor_ContratadaSigla = BC00359_A1223ContratoGestor_ContratadaSigla[0];
            n1223ContratoGestor_ContratadaSigla = BC00359_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = BC00359_A1137ContratoGestor_ContratadaTipo[0];
            n1137ContratoGestor_ContratadaTipo = BC00359_n1137ContratoGestor_ContratadaTipo[0];
            A1447ContratoGestor_ContratadaAreaDes = BC00359_A1447ContratoGestor_ContratadaAreaDes[0];
            n1447ContratoGestor_ContratadaAreaDes = BC00359_n1447ContratoGestor_ContratadaAreaDes[0];
            A1081ContratoGestor_UsuarioPesNom = BC00359_A1081ContratoGestor_UsuarioPesNom[0];
            n1081ContratoGestor_UsuarioPesNom = BC00359_n1081ContratoGestor_UsuarioPesNom[0];
            A2033ContratoGestor_UsuarioAtv = BC00359_A2033ContratoGestor_UsuarioAtv[0];
            n2033ContratoGestor_UsuarioAtv = BC00359_n2033ContratoGestor_UsuarioAtv[0];
            A2009ContratoGestor_Tipo = BC00359_A2009ContratoGestor_Tipo[0];
            n2009ContratoGestor_Tipo = BC00359_n2009ContratoGestor_Tipo[0];
            A1136ContratoGestor_ContratadaCod = BC00359_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = BC00359_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = BC00359_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = BC00359_n1446ContratoGestor_ContratadaAreaCod[0];
            A1080ContratoGestor_UsuarioPesCod = BC00359_A1080ContratoGestor_UsuarioPesCod[0];
            n1080ContratoGestor_UsuarioPesCod = BC00359_n1080ContratoGestor_UsuarioPesCod[0];
            ZM35129( -3) ;
         }
         pr_default.close(7);
         OnLoadActions35129( ) ;
      }

      protected void OnLoadActions35129( )
      {
         GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
         new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
         A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
      }

      protected void CheckExtendedTable35129( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00354 */
         pr_default.execute(2, new Object[] {A1078ContratoGestor_ContratoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
            AnyError = 1;
         }
         A1136ContratoGestor_ContratadaCod = BC00354_A1136ContratoGestor_ContratadaCod[0];
         n1136ContratoGestor_ContratadaCod = BC00354_n1136ContratoGestor_ContratadaCod[0];
         A1446ContratoGestor_ContratadaAreaCod = BC00354_A1446ContratoGestor_ContratadaAreaCod[0];
         n1446ContratoGestor_ContratadaAreaCod = BC00354_n1446ContratoGestor_ContratadaAreaCod[0];
         pr_default.close(2);
         /* Using cursor BC00356 */
         pr_default.execute(4, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1223ContratoGestor_ContratadaSigla = BC00356_A1223ContratoGestor_ContratadaSigla[0];
         n1223ContratoGestor_ContratadaSigla = BC00356_n1223ContratoGestor_ContratadaSigla[0];
         A1137ContratoGestor_ContratadaTipo = BC00356_A1137ContratoGestor_ContratadaTipo[0];
         n1137ContratoGestor_ContratadaTipo = BC00356_n1137ContratoGestor_ContratadaTipo[0];
         pr_default.close(4);
         /* Using cursor BC00357 */
         pr_default.execute(5, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1447ContratoGestor_ContratadaAreaDes = BC00357_A1447ContratoGestor_ContratadaAreaDes[0];
         n1447ContratoGestor_ContratadaAreaDes = BC00357_n1447ContratoGestor_ContratadaAreaDes[0];
         pr_default.close(5);
         /* Using cursor BC00355 */
         pr_default.execute(3, new Object[] {A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_USUARIOCOD");
            AnyError = 1;
         }
         A2033ContratoGestor_UsuarioAtv = BC00355_A2033ContratoGestor_UsuarioAtv[0];
         n2033ContratoGestor_UsuarioAtv = BC00355_n2033ContratoGestor_UsuarioAtv[0];
         A1080ContratoGestor_UsuarioPesCod = BC00355_A1080ContratoGestor_UsuarioPesCod[0];
         n1080ContratoGestor_UsuarioPesCod = BC00355_n1080ContratoGestor_UsuarioPesCod[0];
         pr_default.close(3);
         /* Using cursor BC00358 */
         pr_default.execute(6, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1081ContratoGestor_UsuarioPesNom = BC00358_A1081ContratoGestor_UsuarioPesNom[0];
         n1081ContratoGestor_UsuarioPesNom = BC00358_n1081ContratoGestor_UsuarioPesNom[0];
         pr_default.close(6);
         GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
         new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
         A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
         if ( ! ( ( A2009ContratoGestor_Tipo == 1 ) || ( A2009ContratoGestor_Tipo == 2 ) || (0==A2009ContratoGestor_Tipo) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors35129( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(3);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey35129( )
      {
         /* Using cursor BC003510 */
         pr_default.execute(8, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound129 = 1;
         }
         else
         {
            RcdFound129 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00353 */
         pr_default.execute(1, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM35129( 3) ;
            RcdFound129 = 1;
            A2009ContratoGestor_Tipo = BC00353_A2009ContratoGestor_Tipo[0];
            n2009ContratoGestor_Tipo = BC00353_n2009ContratoGestor_Tipo[0];
            A1078ContratoGestor_ContratoCod = BC00353_A1078ContratoGestor_ContratoCod[0];
            A1079ContratoGestor_UsuarioCod = BC00353_A1079ContratoGestor_UsuarioCod[0];
            Z1078ContratoGestor_ContratoCod = A1078ContratoGestor_ContratoCod;
            Z1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
            sMode129 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load35129( ) ;
            if ( AnyError == 1 )
            {
               RcdFound129 = 0;
               InitializeNonKey35129( ) ;
            }
            Gx_mode = sMode129;
         }
         else
         {
            RcdFound129 = 0;
            InitializeNonKey35129( ) ;
            sMode129 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode129;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey35129( ) ;
         if ( RcdFound129 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_350( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency35129( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00352 */
            pr_default.execute(0, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoGestor"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2009ContratoGestor_Tipo != BC00352_A2009ContratoGestor_Tipo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoGestor"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert35129( )
      {
         BeforeValidate35129( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable35129( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM35129( 0) ;
            CheckOptimisticConcurrency35129( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm35129( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert35129( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003511 */
                     pr_default.execute(9, new Object[] {n2009ContratoGestor_Tipo, A2009ContratoGestor_Tipo, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
                     if ( (pr_default.getStatus(9) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load35129( ) ;
            }
            EndLevel35129( ) ;
         }
         CloseExtendedTableCursors35129( ) ;
      }

      protected void Update35129( )
      {
         BeforeValidate35129( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable35129( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency35129( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm35129( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate35129( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003512 */
                     pr_default.execute(10, new Object[] {n2009ContratoGestor_Tipo, A2009ContratoGestor_Tipo, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoGestor"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate35129( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel35129( ) ;
         }
         CloseExtendedTableCursors35129( ) ;
      }

      protected void DeferredUpdate35129( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate35129( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency35129( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls35129( ) ;
            AfterConfirm35129( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete35129( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003513 */
                  pr_default.execute(11, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode129 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel35129( ) ;
         Gx_mode = sMode129;
      }

      protected void OnDeleteControls35129( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003514 */
            pr_default.execute(12, new Object[] {A1078ContratoGestor_ContratoCod});
            A1136ContratoGestor_ContratadaCod = BC003514_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = BC003514_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = BC003514_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = BC003514_n1446ContratoGestor_ContratadaAreaCod[0];
            pr_default.close(12);
            /* Using cursor BC003515 */
            pr_default.execute(13, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
            A1223ContratoGestor_ContratadaSigla = BC003515_A1223ContratoGestor_ContratadaSigla[0];
            n1223ContratoGestor_ContratadaSigla = BC003515_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = BC003515_A1137ContratoGestor_ContratadaTipo[0];
            n1137ContratoGestor_ContratadaTipo = BC003515_n1137ContratoGestor_ContratadaTipo[0];
            pr_default.close(13);
            /* Using cursor BC003516 */
            pr_default.execute(14, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
            A1447ContratoGestor_ContratadaAreaDes = BC003516_A1447ContratoGestor_ContratadaAreaDes[0];
            n1447ContratoGestor_ContratadaAreaDes = BC003516_n1447ContratoGestor_ContratadaAreaDes[0];
            pr_default.close(14);
            /* Using cursor BC003517 */
            pr_default.execute(15, new Object[] {A1079ContratoGestor_UsuarioCod});
            A2033ContratoGestor_UsuarioAtv = BC003517_A2033ContratoGestor_UsuarioAtv[0];
            n2033ContratoGestor_UsuarioAtv = BC003517_n2033ContratoGestor_UsuarioAtv[0];
            A1080ContratoGestor_UsuarioPesCod = BC003517_A1080ContratoGestor_UsuarioPesCod[0];
            n1080ContratoGestor_UsuarioPesCod = BC003517_n1080ContratoGestor_UsuarioPesCod[0];
            pr_default.close(15);
            /* Using cursor BC003518 */
            pr_default.execute(16, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
            A1081ContratoGestor_UsuarioPesNom = BC003518_A1081ContratoGestor_UsuarioPesNom[0];
            n1081ContratoGestor_UsuarioPesNom = BC003518_n1081ContratoGestor_UsuarioPesNom[0];
            pr_default.close(16);
            GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
         }
      }

      protected void EndLevel35129( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete35129( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart35129( )
      {
         /* Using cursor BC003519 */
         pr_default.execute(17, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         RcdFound129 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound129 = 1;
            A1223ContratoGestor_ContratadaSigla = BC003519_A1223ContratoGestor_ContratadaSigla[0];
            n1223ContratoGestor_ContratadaSigla = BC003519_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = BC003519_A1137ContratoGestor_ContratadaTipo[0];
            n1137ContratoGestor_ContratadaTipo = BC003519_n1137ContratoGestor_ContratadaTipo[0];
            A1447ContratoGestor_ContratadaAreaDes = BC003519_A1447ContratoGestor_ContratadaAreaDes[0];
            n1447ContratoGestor_ContratadaAreaDes = BC003519_n1447ContratoGestor_ContratadaAreaDes[0];
            A1081ContratoGestor_UsuarioPesNom = BC003519_A1081ContratoGestor_UsuarioPesNom[0];
            n1081ContratoGestor_UsuarioPesNom = BC003519_n1081ContratoGestor_UsuarioPesNom[0];
            A2033ContratoGestor_UsuarioAtv = BC003519_A2033ContratoGestor_UsuarioAtv[0];
            n2033ContratoGestor_UsuarioAtv = BC003519_n2033ContratoGestor_UsuarioAtv[0];
            A2009ContratoGestor_Tipo = BC003519_A2009ContratoGestor_Tipo[0];
            n2009ContratoGestor_Tipo = BC003519_n2009ContratoGestor_Tipo[0];
            A1078ContratoGestor_ContratoCod = BC003519_A1078ContratoGestor_ContratoCod[0];
            A1079ContratoGestor_UsuarioCod = BC003519_A1079ContratoGestor_UsuarioCod[0];
            A1136ContratoGestor_ContratadaCod = BC003519_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = BC003519_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = BC003519_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = BC003519_n1446ContratoGestor_ContratadaAreaCod[0];
            A1080ContratoGestor_UsuarioPesCod = BC003519_A1080ContratoGestor_UsuarioPesCod[0];
            n1080ContratoGestor_UsuarioPesCod = BC003519_n1080ContratoGestor_UsuarioPesCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext35129( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound129 = 0;
         ScanKeyLoad35129( ) ;
      }

      protected void ScanKeyLoad35129( )
      {
         sMode129 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound129 = 1;
            A1223ContratoGestor_ContratadaSigla = BC003519_A1223ContratoGestor_ContratadaSigla[0];
            n1223ContratoGestor_ContratadaSigla = BC003519_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = BC003519_A1137ContratoGestor_ContratadaTipo[0];
            n1137ContratoGestor_ContratadaTipo = BC003519_n1137ContratoGestor_ContratadaTipo[0];
            A1447ContratoGestor_ContratadaAreaDes = BC003519_A1447ContratoGestor_ContratadaAreaDes[0];
            n1447ContratoGestor_ContratadaAreaDes = BC003519_n1447ContratoGestor_ContratadaAreaDes[0];
            A1081ContratoGestor_UsuarioPesNom = BC003519_A1081ContratoGestor_UsuarioPesNom[0];
            n1081ContratoGestor_UsuarioPesNom = BC003519_n1081ContratoGestor_UsuarioPesNom[0];
            A2033ContratoGestor_UsuarioAtv = BC003519_A2033ContratoGestor_UsuarioAtv[0];
            n2033ContratoGestor_UsuarioAtv = BC003519_n2033ContratoGestor_UsuarioAtv[0];
            A2009ContratoGestor_Tipo = BC003519_A2009ContratoGestor_Tipo[0];
            n2009ContratoGestor_Tipo = BC003519_n2009ContratoGestor_Tipo[0];
            A1078ContratoGestor_ContratoCod = BC003519_A1078ContratoGestor_ContratoCod[0];
            A1079ContratoGestor_UsuarioCod = BC003519_A1079ContratoGestor_UsuarioCod[0];
            A1136ContratoGestor_ContratadaCod = BC003519_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = BC003519_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = BC003519_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = BC003519_n1446ContratoGestor_ContratadaAreaCod[0];
            A1080ContratoGestor_UsuarioPesCod = BC003519_A1080ContratoGestor_UsuarioPesCod[0];
            n1080ContratoGestor_UsuarioPesCod = BC003519_n1080ContratoGestor_UsuarioPesCod[0];
         }
         Gx_mode = sMode129;
      }

      protected void ScanKeyEnd35129( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm35129( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert35129( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate35129( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete35129( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete35129( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate35129( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes35129( )
      {
      }

      protected void AddRow35129( )
      {
         VarsToRow129( bcContratoGestor) ;
      }

      protected void ReadRow35129( )
      {
         RowToVars129( bcContratoGestor, 1) ;
      }

      protected void InitializeNonKey35129( )
      {
         A1135ContratoGestor_UsuarioEhContratante = false;
         A1136ContratoGestor_ContratadaCod = 0;
         n1136ContratoGestor_ContratadaCod = false;
         A1223ContratoGestor_ContratadaSigla = "";
         n1223ContratoGestor_ContratadaSigla = false;
         A1137ContratoGestor_ContratadaTipo = "";
         n1137ContratoGestor_ContratadaTipo = false;
         A1446ContratoGestor_ContratadaAreaCod = 0;
         n1446ContratoGestor_ContratadaAreaCod = false;
         A1447ContratoGestor_ContratadaAreaDes = "";
         n1447ContratoGestor_ContratadaAreaDes = false;
         A1080ContratoGestor_UsuarioPesCod = 0;
         n1080ContratoGestor_UsuarioPesCod = false;
         A1081ContratoGestor_UsuarioPesNom = "";
         n1081ContratoGestor_UsuarioPesNom = false;
         A2033ContratoGestor_UsuarioAtv = false;
         n2033ContratoGestor_UsuarioAtv = false;
         A2009ContratoGestor_Tipo = 0;
         n2009ContratoGestor_Tipo = false;
         Z2009ContratoGestor_Tipo = 0;
      }

      protected void InitAll35129( )
      {
         A1078ContratoGestor_ContratoCod = 0;
         A1079ContratoGestor_UsuarioCod = 0;
         InitializeNonKey35129( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow129( SdtContratoGestor obj129 )
      {
         obj129.gxTpr_Mode = Gx_mode;
         obj129.gxTpr_Contratogestor_usuarioehcontratante = A1135ContratoGestor_UsuarioEhContratante;
         obj129.gxTpr_Contratogestor_contratadacod = A1136ContratoGestor_ContratadaCod;
         obj129.gxTpr_Contratogestor_contratadasigla = A1223ContratoGestor_ContratadaSigla;
         obj129.gxTpr_Contratogestor_contratadatipo = A1137ContratoGestor_ContratadaTipo;
         obj129.gxTpr_Contratogestor_contratadaareacod = A1446ContratoGestor_ContratadaAreaCod;
         obj129.gxTpr_Contratogestor_contratadaareades = A1447ContratoGestor_ContratadaAreaDes;
         obj129.gxTpr_Contratogestor_usuariopescod = A1080ContratoGestor_UsuarioPesCod;
         obj129.gxTpr_Contratogestor_usuariopesnom = A1081ContratoGestor_UsuarioPesNom;
         obj129.gxTpr_Contratogestor_usuarioatv = A2033ContratoGestor_UsuarioAtv;
         obj129.gxTpr_Contratogestor_tipo = A2009ContratoGestor_Tipo;
         obj129.gxTpr_Contratogestor_contratocod = A1078ContratoGestor_ContratoCod;
         obj129.gxTpr_Contratogestor_usuariocod = A1079ContratoGestor_UsuarioCod;
         obj129.gxTpr_Contratogestor_contratocod_Z = Z1078ContratoGestor_ContratoCod;
         obj129.gxTpr_Contratogestor_contratadacod_Z = Z1136ContratoGestor_ContratadaCod;
         obj129.gxTpr_Contratogestor_contratadasigla_Z = Z1223ContratoGestor_ContratadaSigla;
         obj129.gxTpr_Contratogestor_contratadatipo_Z = Z1137ContratoGestor_ContratadaTipo;
         obj129.gxTpr_Contratogestor_contratadaareacod_Z = Z1446ContratoGestor_ContratadaAreaCod;
         obj129.gxTpr_Contratogestor_contratadaareades_Z = Z1447ContratoGestor_ContratadaAreaDes;
         obj129.gxTpr_Contratogestor_usuariocod_Z = Z1079ContratoGestor_UsuarioCod;
         obj129.gxTpr_Contratogestor_usuariopescod_Z = Z1080ContratoGestor_UsuarioPesCod;
         obj129.gxTpr_Contratogestor_usuariopesnom_Z = Z1081ContratoGestor_UsuarioPesNom;
         obj129.gxTpr_Contratogestor_usuarioehcontratante_Z = Z1135ContratoGestor_UsuarioEhContratante;
         obj129.gxTpr_Contratogestor_usuarioatv_Z = Z2033ContratoGestor_UsuarioAtv;
         obj129.gxTpr_Contratogestor_tipo_Z = Z2009ContratoGestor_Tipo;
         obj129.gxTpr_Contratogestor_contratadacod_N = (short)(Convert.ToInt16(n1136ContratoGestor_ContratadaCod));
         obj129.gxTpr_Contratogestor_contratadasigla_N = (short)(Convert.ToInt16(n1223ContratoGestor_ContratadaSigla));
         obj129.gxTpr_Contratogestor_contratadatipo_N = (short)(Convert.ToInt16(n1137ContratoGestor_ContratadaTipo));
         obj129.gxTpr_Contratogestor_contratadaareacod_N = (short)(Convert.ToInt16(n1446ContratoGestor_ContratadaAreaCod));
         obj129.gxTpr_Contratogestor_contratadaareades_N = (short)(Convert.ToInt16(n1447ContratoGestor_ContratadaAreaDes));
         obj129.gxTpr_Contratogestor_usuariopescod_N = (short)(Convert.ToInt16(n1080ContratoGestor_UsuarioPesCod));
         obj129.gxTpr_Contratogestor_usuariopesnom_N = (short)(Convert.ToInt16(n1081ContratoGestor_UsuarioPesNom));
         obj129.gxTpr_Contratogestor_usuarioatv_N = (short)(Convert.ToInt16(n2033ContratoGestor_UsuarioAtv));
         obj129.gxTpr_Contratogestor_tipo_N = (short)(Convert.ToInt16(n2009ContratoGestor_Tipo));
         obj129.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow129( SdtContratoGestor obj129 )
      {
         obj129.gxTpr_Contratogestor_contratocod = A1078ContratoGestor_ContratoCod;
         obj129.gxTpr_Contratogestor_usuariocod = A1079ContratoGestor_UsuarioCod;
         return  ;
      }

      public void RowToVars129( SdtContratoGestor obj129 ,
                                int forceLoad )
      {
         Gx_mode = obj129.gxTpr_Mode;
         A1135ContratoGestor_UsuarioEhContratante = obj129.gxTpr_Contratogestor_usuarioehcontratante;
         A1136ContratoGestor_ContratadaCod = obj129.gxTpr_Contratogestor_contratadacod;
         n1136ContratoGestor_ContratadaCod = false;
         A1223ContratoGestor_ContratadaSigla = obj129.gxTpr_Contratogestor_contratadasigla;
         n1223ContratoGestor_ContratadaSigla = false;
         A1137ContratoGestor_ContratadaTipo = obj129.gxTpr_Contratogestor_contratadatipo;
         n1137ContratoGestor_ContratadaTipo = false;
         A1446ContratoGestor_ContratadaAreaCod = obj129.gxTpr_Contratogestor_contratadaareacod;
         n1446ContratoGestor_ContratadaAreaCod = false;
         A1447ContratoGestor_ContratadaAreaDes = obj129.gxTpr_Contratogestor_contratadaareades;
         n1447ContratoGestor_ContratadaAreaDes = false;
         A1080ContratoGestor_UsuarioPesCod = obj129.gxTpr_Contratogestor_usuariopescod;
         n1080ContratoGestor_UsuarioPesCod = false;
         A1081ContratoGestor_UsuarioPesNom = obj129.gxTpr_Contratogestor_usuariopesnom;
         n1081ContratoGestor_UsuarioPesNom = false;
         A2033ContratoGestor_UsuarioAtv = obj129.gxTpr_Contratogestor_usuarioatv;
         n2033ContratoGestor_UsuarioAtv = false;
         A2009ContratoGestor_Tipo = obj129.gxTpr_Contratogestor_tipo;
         n2009ContratoGestor_Tipo = false;
         A1078ContratoGestor_ContratoCod = obj129.gxTpr_Contratogestor_contratocod;
         A1079ContratoGestor_UsuarioCod = obj129.gxTpr_Contratogestor_usuariocod;
         Z1078ContratoGestor_ContratoCod = obj129.gxTpr_Contratogestor_contratocod_Z;
         Z1136ContratoGestor_ContratadaCod = obj129.gxTpr_Contratogestor_contratadacod_Z;
         Z1223ContratoGestor_ContratadaSigla = obj129.gxTpr_Contratogestor_contratadasigla_Z;
         Z1137ContratoGestor_ContratadaTipo = obj129.gxTpr_Contratogestor_contratadatipo_Z;
         Z1446ContratoGestor_ContratadaAreaCod = obj129.gxTpr_Contratogestor_contratadaareacod_Z;
         Z1447ContratoGestor_ContratadaAreaDes = obj129.gxTpr_Contratogestor_contratadaareades_Z;
         Z1079ContratoGestor_UsuarioCod = obj129.gxTpr_Contratogestor_usuariocod_Z;
         Z1080ContratoGestor_UsuarioPesCod = obj129.gxTpr_Contratogestor_usuariopescod_Z;
         Z1081ContratoGestor_UsuarioPesNom = obj129.gxTpr_Contratogestor_usuariopesnom_Z;
         Z1135ContratoGestor_UsuarioEhContratante = obj129.gxTpr_Contratogestor_usuarioehcontratante_Z;
         Z2033ContratoGestor_UsuarioAtv = obj129.gxTpr_Contratogestor_usuarioatv_Z;
         Z2009ContratoGestor_Tipo = obj129.gxTpr_Contratogestor_tipo_Z;
         n1136ContratoGestor_ContratadaCod = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_contratadacod_N));
         n1223ContratoGestor_ContratadaSigla = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_contratadasigla_N));
         n1137ContratoGestor_ContratadaTipo = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_contratadatipo_N));
         n1446ContratoGestor_ContratadaAreaCod = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_contratadaareacod_N));
         n1447ContratoGestor_ContratadaAreaDes = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_contratadaareades_N));
         n1080ContratoGestor_UsuarioPesCod = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_usuariopescod_N));
         n1081ContratoGestor_UsuarioPesNom = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_usuariopesnom_N));
         n2033ContratoGestor_UsuarioAtv = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_usuarioatv_N));
         n2009ContratoGestor_Tipo = (bool)(Convert.ToBoolean(obj129.gxTpr_Contratogestor_tipo_N));
         Gx_mode = obj129.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1078ContratoGestor_ContratoCod = (int)getParm(obj,0);
         A1079ContratoGestor_UsuarioCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey35129( ) ;
         ScanKeyStart35129( ) ;
         if ( RcdFound129 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003514 */
            pr_default.execute(12, new Object[] {A1078ContratoGestor_ContratoCod});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
               AnyError = 1;
            }
            A1136ContratoGestor_ContratadaCod = BC003514_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = BC003514_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = BC003514_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = BC003514_n1446ContratoGestor_ContratadaAreaCod[0];
            pr_default.close(12);
            /* Using cursor BC003515 */
            pr_default.execute(13, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1223ContratoGestor_ContratadaSigla = BC003515_A1223ContratoGestor_ContratadaSigla[0];
            n1223ContratoGestor_ContratadaSigla = BC003515_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = BC003515_A1137ContratoGestor_ContratadaTipo[0];
            n1137ContratoGestor_ContratadaTipo = BC003515_n1137ContratoGestor_ContratadaTipo[0];
            pr_default.close(13);
            /* Using cursor BC003516 */
            pr_default.execute(14, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1447ContratoGestor_ContratadaAreaDes = BC003516_A1447ContratoGestor_ContratadaAreaDes[0];
            n1447ContratoGestor_ContratadaAreaDes = BC003516_n1447ContratoGestor_ContratadaAreaDes[0];
            pr_default.close(14);
            /* Using cursor BC003517 */
            pr_default.execute(15, new Object[] {A1079ContratoGestor_UsuarioCod});
            if ( (pr_default.getStatus(15) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Gestor_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_USUARIOCOD");
               AnyError = 1;
            }
            A2033ContratoGestor_UsuarioAtv = BC003517_A2033ContratoGestor_UsuarioAtv[0];
            n2033ContratoGestor_UsuarioAtv = BC003517_n2033ContratoGestor_UsuarioAtv[0];
            A1080ContratoGestor_UsuarioPesCod = BC003517_A1080ContratoGestor_UsuarioPesCod[0];
            n1080ContratoGestor_UsuarioPesCod = BC003517_n1080ContratoGestor_UsuarioPesCod[0];
            pr_default.close(15);
            /* Using cursor BC003518 */
            pr_default.execute(16, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1081ContratoGestor_UsuarioPesNom = BC003518_A1081ContratoGestor_UsuarioPesNom[0];
            n1081ContratoGestor_UsuarioPesNom = BC003518_n1081ContratoGestor_UsuarioPesNom[0];
            pr_default.close(16);
         }
         else
         {
            Gx_mode = "UPD";
            Z1078ContratoGestor_ContratoCod = A1078ContratoGestor_ContratoCod;
            Z1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
         }
         ZM35129( -3) ;
         OnLoadActions35129( ) ;
         AddRow35129( ) ;
         ScanKeyEnd35129( ) ;
         if ( RcdFound129 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars129( bcContratoGestor, 0) ;
         ScanKeyStart35129( ) ;
         if ( RcdFound129 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003514 */
            pr_default.execute(12, new Object[] {A1078ContratoGestor_ContratoCod});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
               AnyError = 1;
            }
            A1136ContratoGestor_ContratadaCod = BC003514_A1136ContratoGestor_ContratadaCod[0];
            n1136ContratoGestor_ContratadaCod = BC003514_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = BC003514_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = BC003514_n1446ContratoGestor_ContratadaAreaCod[0];
            pr_default.close(12);
            /* Using cursor BC003515 */
            pr_default.execute(13, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
            if ( (pr_default.getStatus(13) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1223ContratoGestor_ContratadaSigla = BC003515_A1223ContratoGestor_ContratadaSigla[0];
            n1223ContratoGestor_ContratadaSigla = BC003515_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = BC003515_A1137ContratoGestor_ContratadaTipo[0];
            n1137ContratoGestor_ContratadaTipo = BC003515_n1137ContratoGestor_ContratadaTipo[0];
            pr_default.close(13);
            /* Using cursor BC003516 */
            pr_default.execute(14, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
            if ( (pr_default.getStatus(14) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1447ContratoGestor_ContratadaAreaDes = BC003516_A1447ContratoGestor_ContratadaAreaDes[0];
            n1447ContratoGestor_ContratadaAreaDes = BC003516_n1447ContratoGestor_ContratadaAreaDes[0];
            pr_default.close(14);
            /* Using cursor BC003517 */
            pr_default.execute(15, new Object[] {A1079ContratoGestor_UsuarioCod});
            if ( (pr_default.getStatus(15) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Gestor_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_USUARIOCOD");
               AnyError = 1;
            }
            A2033ContratoGestor_UsuarioAtv = BC003517_A2033ContratoGestor_UsuarioAtv[0];
            n2033ContratoGestor_UsuarioAtv = BC003517_n2033ContratoGestor_UsuarioAtv[0];
            A1080ContratoGestor_UsuarioPesCod = BC003517_A1080ContratoGestor_UsuarioPesCod[0];
            n1080ContratoGestor_UsuarioPesCod = BC003517_n1080ContratoGestor_UsuarioPesCod[0];
            pr_default.close(15);
            /* Using cursor BC003518 */
            pr_default.execute(16, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
            if ( (pr_default.getStatus(16) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A1081ContratoGestor_UsuarioPesNom = BC003518_A1081ContratoGestor_UsuarioPesNom[0];
            n1081ContratoGestor_UsuarioPesNom = BC003518_n1081ContratoGestor_UsuarioPesNom[0];
            pr_default.close(16);
         }
         else
         {
            Gx_mode = "UPD";
            Z1078ContratoGestor_ContratoCod = A1078ContratoGestor_ContratoCod;
            Z1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
         }
         ZM35129( -3) ;
         OnLoadActions35129( ) ;
         AddRow35129( ) ;
         ScanKeyEnd35129( ) ;
         if ( RcdFound129 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars129( bcContratoGestor, 0) ;
         nKeyPressed = 1;
         GetKey35129( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert35129( ) ;
         }
         else
         {
            if ( RcdFound129 == 1 )
            {
               if ( ( A1078ContratoGestor_ContratoCod != Z1078ContratoGestor_ContratoCod ) || ( A1079ContratoGestor_UsuarioCod != Z1079ContratoGestor_UsuarioCod ) )
               {
                  A1078ContratoGestor_ContratoCod = Z1078ContratoGestor_ContratoCod;
                  A1079ContratoGestor_UsuarioCod = Z1079ContratoGestor_UsuarioCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update35129( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A1078ContratoGestor_ContratoCod != Z1078ContratoGestor_ContratoCod ) || ( A1079ContratoGestor_UsuarioCod != Z1079ContratoGestor_UsuarioCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert35129( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert35129( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow129( bcContratoGestor) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars129( bcContratoGestor, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey35129( ) ;
         if ( RcdFound129 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A1078ContratoGestor_ContratoCod != Z1078ContratoGestor_ContratoCod ) || ( A1079ContratoGestor_UsuarioCod != Z1079ContratoGestor_UsuarioCod ) )
            {
               A1078ContratoGestor_ContratoCod = Z1078ContratoGestor_ContratoCod;
               A1079ContratoGestor_UsuarioCod = Z1079ContratoGestor_UsuarioCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A1078ContratoGestor_ContratoCod != Z1078ContratoGestor_ContratoCod ) || ( A1079ContratoGestor_UsuarioCod != Z1079ContratoGestor_UsuarioCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(15);
         pr_default.close(13);
         pr_default.close(14);
         pr_default.close(16);
         context.RollbackDataStores( "ContratoGestor_BC");
         VarsToRow129( bcContratoGestor) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoGestor.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoGestor.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoGestor )
         {
            bcContratoGestor = (SdtContratoGestor)(sdt);
            if ( StringUtil.StrCmp(bcContratoGestor.gxTpr_Mode, "") == 0 )
            {
               bcContratoGestor.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow129( bcContratoGestor) ;
            }
            else
            {
               RowToVars129( bcContratoGestor, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoGestor.gxTpr_Mode, "") == 0 )
            {
               bcContratoGestor.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars129( bcContratoGestor, 1) ;
         return  ;
      }

      public SdtContratoGestor ContratoGestor_BC
      {
         get {
            return bcContratoGestor ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(15);
         pr_default.close(13);
         pr_default.close(14);
         pr_default.close(16);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1223ContratoGestor_ContratadaSigla = "";
         A1223ContratoGestor_ContratadaSigla = "";
         Z1137ContratoGestor_ContratadaTipo = "";
         A1137ContratoGestor_ContratadaTipo = "";
         Z1447ContratoGestor_ContratadaAreaDes = "";
         A1447ContratoGestor_ContratadaAreaDes = "";
         Z1081ContratoGestor_UsuarioPesNom = "";
         A1081ContratoGestor_UsuarioPesNom = "";
         BC00359_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         BC00359_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         BC00359_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         BC00359_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         BC00359_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         BC00359_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         BC00359_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         BC00359_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         BC00359_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC00359_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC00359_A2009ContratoGestor_Tipo = new short[1] ;
         BC00359_n2009ContratoGestor_Tipo = new bool[] {false} ;
         BC00359_A1078ContratoGestor_ContratoCod = new int[1] ;
         BC00359_A1079ContratoGestor_UsuarioCod = new int[1] ;
         BC00359_A1136ContratoGestor_ContratadaCod = new int[1] ;
         BC00359_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         BC00359_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         BC00359_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         BC00359_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         BC00359_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         BC00354_A1136ContratoGestor_ContratadaCod = new int[1] ;
         BC00354_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         BC00354_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         BC00354_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         BC00356_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         BC00356_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         BC00356_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         BC00356_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         BC00357_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         BC00357_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         BC00355_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC00355_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC00355_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         BC00355_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         BC00358_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         BC00358_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         BC003510_A1078ContratoGestor_ContratoCod = new int[1] ;
         BC003510_A1079ContratoGestor_UsuarioCod = new int[1] ;
         BC00353_A2009ContratoGestor_Tipo = new short[1] ;
         BC00353_n2009ContratoGestor_Tipo = new bool[] {false} ;
         BC00353_A1078ContratoGestor_ContratoCod = new int[1] ;
         BC00353_A1079ContratoGestor_UsuarioCod = new int[1] ;
         sMode129 = "";
         BC00352_A2009ContratoGestor_Tipo = new short[1] ;
         BC00352_n2009ContratoGestor_Tipo = new bool[] {false} ;
         BC00352_A1078ContratoGestor_ContratoCod = new int[1] ;
         BC00352_A1079ContratoGestor_UsuarioCod = new int[1] ;
         BC003514_A1136ContratoGestor_ContratadaCod = new int[1] ;
         BC003514_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         BC003514_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         BC003514_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         BC003515_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         BC003515_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         BC003515_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         BC003515_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         BC003516_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         BC003516_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         BC003517_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC003517_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC003517_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         BC003517_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         BC003518_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         BC003518_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         BC003519_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         BC003519_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         BC003519_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         BC003519_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         BC003519_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         BC003519_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         BC003519_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         BC003519_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         BC003519_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC003519_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         BC003519_A2009ContratoGestor_Tipo = new short[1] ;
         BC003519_n2009ContratoGestor_Tipo = new bool[] {false} ;
         BC003519_A1078ContratoGestor_ContratoCod = new int[1] ;
         BC003519_A1079ContratoGestor_UsuarioCod = new int[1] ;
         BC003519_A1136ContratoGestor_ContratadaCod = new int[1] ;
         BC003519_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         BC003519_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         BC003519_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         BC003519_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         BC003519_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratogestor_bc__default(),
            new Object[][] {
                new Object[] {
               BC00352_A2009ContratoGestor_Tipo, BC00352_n2009ContratoGestor_Tipo, BC00352_A1078ContratoGestor_ContratoCod, BC00352_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               BC00353_A2009ContratoGestor_Tipo, BC00353_n2009ContratoGestor_Tipo, BC00353_A1078ContratoGestor_ContratoCod, BC00353_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               BC00354_A1136ContratoGestor_ContratadaCod, BC00354_n1136ContratoGestor_ContratadaCod, BC00354_A1446ContratoGestor_ContratadaAreaCod, BC00354_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               BC00355_A2033ContratoGestor_UsuarioAtv, BC00355_n2033ContratoGestor_UsuarioAtv, BC00355_A1080ContratoGestor_UsuarioPesCod, BC00355_n1080ContratoGestor_UsuarioPesCod
               }
               , new Object[] {
               BC00356_A1223ContratoGestor_ContratadaSigla, BC00356_n1223ContratoGestor_ContratadaSigla, BC00356_A1137ContratoGestor_ContratadaTipo, BC00356_n1137ContratoGestor_ContratadaTipo
               }
               , new Object[] {
               BC00357_A1447ContratoGestor_ContratadaAreaDes, BC00357_n1447ContratoGestor_ContratadaAreaDes
               }
               , new Object[] {
               BC00358_A1081ContratoGestor_UsuarioPesNom, BC00358_n1081ContratoGestor_UsuarioPesNom
               }
               , new Object[] {
               BC00359_A1223ContratoGestor_ContratadaSigla, BC00359_n1223ContratoGestor_ContratadaSigla, BC00359_A1137ContratoGestor_ContratadaTipo, BC00359_n1137ContratoGestor_ContratadaTipo, BC00359_A1447ContratoGestor_ContratadaAreaDes, BC00359_n1447ContratoGestor_ContratadaAreaDes, BC00359_A1081ContratoGestor_UsuarioPesNom, BC00359_n1081ContratoGestor_UsuarioPesNom, BC00359_A2033ContratoGestor_UsuarioAtv, BC00359_n2033ContratoGestor_UsuarioAtv,
               BC00359_A2009ContratoGestor_Tipo, BC00359_n2009ContratoGestor_Tipo, BC00359_A1078ContratoGestor_ContratoCod, BC00359_A1079ContratoGestor_UsuarioCod, BC00359_A1136ContratoGestor_ContratadaCod, BC00359_n1136ContratoGestor_ContratadaCod, BC00359_A1446ContratoGestor_ContratadaAreaCod, BC00359_n1446ContratoGestor_ContratadaAreaCod, BC00359_A1080ContratoGestor_UsuarioPesCod, BC00359_n1080ContratoGestor_UsuarioPesCod
               }
               , new Object[] {
               BC003510_A1078ContratoGestor_ContratoCod, BC003510_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003514_A1136ContratoGestor_ContratadaCod, BC003514_n1136ContratoGestor_ContratadaCod, BC003514_A1446ContratoGestor_ContratadaAreaCod, BC003514_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               BC003515_A1223ContratoGestor_ContratadaSigla, BC003515_n1223ContratoGestor_ContratadaSigla, BC003515_A1137ContratoGestor_ContratadaTipo, BC003515_n1137ContratoGestor_ContratadaTipo
               }
               , new Object[] {
               BC003516_A1447ContratoGestor_ContratadaAreaDes, BC003516_n1447ContratoGestor_ContratadaAreaDes
               }
               , new Object[] {
               BC003517_A2033ContratoGestor_UsuarioAtv, BC003517_n2033ContratoGestor_UsuarioAtv, BC003517_A1080ContratoGestor_UsuarioPesCod, BC003517_n1080ContratoGestor_UsuarioPesCod
               }
               , new Object[] {
               BC003518_A1081ContratoGestor_UsuarioPesNom, BC003518_n1081ContratoGestor_UsuarioPesNom
               }
               , new Object[] {
               BC003519_A1223ContratoGestor_ContratadaSigla, BC003519_n1223ContratoGestor_ContratadaSigla, BC003519_A1137ContratoGestor_ContratadaTipo, BC003519_n1137ContratoGestor_ContratadaTipo, BC003519_A1447ContratoGestor_ContratadaAreaDes, BC003519_n1447ContratoGestor_ContratadaAreaDes, BC003519_A1081ContratoGestor_UsuarioPesNom, BC003519_n1081ContratoGestor_UsuarioPesNom, BC003519_A2033ContratoGestor_UsuarioAtv, BC003519_n2033ContratoGestor_UsuarioAtv,
               BC003519_A2009ContratoGestor_Tipo, BC003519_n2009ContratoGestor_Tipo, BC003519_A1078ContratoGestor_ContratoCod, BC003519_A1079ContratoGestor_UsuarioCod, BC003519_A1136ContratoGestor_ContratadaCod, BC003519_n1136ContratoGestor_ContratadaCod, BC003519_A1446ContratoGestor_ContratadaAreaCod, BC003519_n1446ContratoGestor_ContratadaAreaCod, BC003519_A1080ContratoGestor_UsuarioPesCod, BC003519_n1080ContratoGestor_UsuarioPesCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z2009ContratoGestor_Tipo ;
      private short A2009ContratoGestor_Tipo ;
      private short RcdFound129 ;
      private int trnEnded ;
      private int Z1078ContratoGestor_ContratoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int Z1079ContratoGestor_UsuarioCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int Z1136ContratoGestor_ContratadaCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int Z1446ContratoGestor_ContratadaAreaCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int Z1080ContratoGestor_UsuarioPesCod ;
      private int A1080ContratoGestor_UsuarioPesCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1223ContratoGestor_ContratadaSigla ;
      private String A1223ContratoGestor_ContratadaSigla ;
      private String Z1137ContratoGestor_ContratadaTipo ;
      private String A1137ContratoGestor_ContratadaTipo ;
      private String Z1081ContratoGestor_UsuarioPesNom ;
      private String A1081ContratoGestor_UsuarioPesNom ;
      private String sMode129 ;
      private bool Z1135ContratoGestor_UsuarioEhContratante ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool Z2033ContratoGestor_UsuarioAtv ;
      private bool A2033ContratoGestor_UsuarioAtv ;
      private bool n1223ContratoGestor_ContratadaSigla ;
      private bool n1137ContratoGestor_ContratadaTipo ;
      private bool n1447ContratoGestor_ContratadaAreaDes ;
      private bool n1081ContratoGestor_UsuarioPesNom ;
      private bool n2033ContratoGestor_UsuarioAtv ;
      private bool n2009ContratoGestor_Tipo ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool n1080ContratoGestor_UsuarioPesCod ;
      private bool GXt_boolean1 ;
      private String Z1447ContratoGestor_ContratadaAreaDes ;
      private String A1447ContratoGestor_ContratadaAreaDes ;
      private SdtContratoGestor bcContratoGestor ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00359_A1223ContratoGestor_ContratadaSigla ;
      private bool[] BC00359_n1223ContratoGestor_ContratadaSigla ;
      private String[] BC00359_A1137ContratoGestor_ContratadaTipo ;
      private bool[] BC00359_n1137ContratoGestor_ContratadaTipo ;
      private String[] BC00359_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] BC00359_n1447ContratoGestor_ContratadaAreaDes ;
      private String[] BC00359_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] BC00359_n1081ContratoGestor_UsuarioPesNom ;
      private bool[] BC00359_A2033ContratoGestor_UsuarioAtv ;
      private bool[] BC00359_n2033ContratoGestor_UsuarioAtv ;
      private short[] BC00359_A2009ContratoGestor_Tipo ;
      private bool[] BC00359_n2009ContratoGestor_Tipo ;
      private int[] BC00359_A1078ContratoGestor_ContratoCod ;
      private int[] BC00359_A1079ContratoGestor_UsuarioCod ;
      private int[] BC00359_A1136ContratoGestor_ContratadaCod ;
      private bool[] BC00359_n1136ContratoGestor_ContratadaCod ;
      private int[] BC00359_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] BC00359_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] BC00359_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] BC00359_n1080ContratoGestor_UsuarioPesCod ;
      private int[] BC00354_A1136ContratoGestor_ContratadaCod ;
      private bool[] BC00354_n1136ContratoGestor_ContratadaCod ;
      private int[] BC00354_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] BC00354_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] BC00356_A1223ContratoGestor_ContratadaSigla ;
      private bool[] BC00356_n1223ContratoGestor_ContratadaSigla ;
      private String[] BC00356_A1137ContratoGestor_ContratadaTipo ;
      private bool[] BC00356_n1137ContratoGestor_ContratadaTipo ;
      private String[] BC00357_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] BC00357_n1447ContratoGestor_ContratadaAreaDes ;
      private bool[] BC00355_A2033ContratoGestor_UsuarioAtv ;
      private bool[] BC00355_n2033ContratoGestor_UsuarioAtv ;
      private int[] BC00355_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] BC00355_n1080ContratoGestor_UsuarioPesCod ;
      private String[] BC00358_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] BC00358_n1081ContratoGestor_UsuarioPesNom ;
      private int[] BC003510_A1078ContratoGestor_ContratoCod ;
      private int[] BC003510_A1079ContratoGestor_UsuarioCod ;
      private short[] BC00353_A2009ContratoGestor_Tipo ;
      private bool[] BC00353_n2009ContratoGestor_Tipo ;
      private int[] BC00353_A1078ContratoGestor_ContratoCod ;
      private int[] BC00353_A1079ContratoGestor_UsuarioCod ;
      private short[] BC00352_A2009ContratoGestor_Tipo ;
      private bool[] BC00352_n2009ContratoGestor_Tipo ;
      private int[] BC00352_A1078ContratoGestor_ContratoCod ;
      private int[] BC00352_A1079ContratoGestor_UsuarioCod ;
      private int[] BC003514_A1136ContratoGestor_ContratadaCod ;
      private bool[] BC003514_n1136ContratoGestor_ContratadaCod ;
      private int[] BC003514_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] BC003514_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] BC003515_A1223ContratoGestor_ContratadaSigla ;
      private bool[] BC003515_n1223ContratoGestor_ContratadaSigla ;
      private String[] BC003515_A1137ContratoGestor_ContratadaTipo ;
      private bool[] BC003515_n1137ContratoGestor_ContratadaTipo ;
      private String[] BC003516_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] BC003516_n1447ContratoGestor_ContratadaAreaDes ;
      private bool[] BC003517_A2033ContratoGestor_UsuarioAtv ;
      private bool[] BC003517_n2033ContratoGestor_UsuarioAtv ;
      private int[] BC003517_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] BC003517_n1080ContratoGestor_UsuarioPesCod ;
      private String[] BC003518_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] BC003518_n1081ContratoGestor_UsuarioPesNom ;
      private String[] BC003519_A1223ContratoGestor_ContratadaSigla ;
      private bool[] BC003519_n1223ContratoGestor_ContratadaSigla ;
      private String[] BC003519_A1137ContratoGestor_ContratadaTipo ;
      private bool[] BC003519_n1137ContratoGestor_ContratadaTipo ;
      private String[] BC003519_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] BC003519_n1447ContratoGestor_ContratadaAreaDes ;
      private String[] BC003519_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] BC003519_n1081ContratoGestor_UsuarioPesNom ;
      private bool[] BC003519_A2033ContratoGestor_UsuarioAtv ;
      private bool[] BC003519_n2033ContratoGestor_UsuarioAtv ;
      private short[] BC003519_A2009ContratoGestor_Tipo ;
      private bool[] BC003519_n2009ContratoGestor_Tipo ;
      private int[] BC003519_A1078ContratoGestor_ContratoCod ;
      private int[] BC003519_A1079ContratoGestor_UsuarioCod ;
      private int[] BC003519_A1136ContratoGestor_ContratadaCod ;
      private bool[] BC003519_n1136ContratoGestor_ContratadaCod ;
      private int[] BC003519_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] BC003519_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] BC003519_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] BC003519_n1080ContratoGestor_UsuarioPesCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class contratogestor_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00359 ;
          prmBC00359 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00354 ;
          prmBC00354 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00356 ;
          prmBC00356 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00357 ;
          prmBC00357 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaAreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00355 ;
          prmBC00355 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00358 ;
          prmBC00358 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003510 ;
          prmBC003510 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00353 ;
          prmBC00353 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00352 ;
          prmBC00352 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003511 ;
          prmBC003511 = new Object[] {
          new Object[] {"@ContratoGestor_Tipo",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003512 ;
          prmBC003512 = new Object[] {
          new Object[] {"@ContratoGestor_Tipo",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003513 ;
          prmBC003513 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003519 ;
          prmBC003519 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003514 ;
          prmBC003514 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003515 ;
          prmBC003515 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003516 ;
          prmBC003516 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaAreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003517 ;
          prmBC003517 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003518 ;
          prmBC003518 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioPesCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00352", "SELECT [ContratoGestor_Tipo], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (UPDLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00352,1,0,true,false )
             ,new CursorDef("BC00353", "SELECT [ContratoGestor_Tipo], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00353,1,0,true,false )
             ,new CursorDef("BC00354", "SELECT [Contratada_Codigo] AS ContratoGestor_ContratadaCod, [Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoGestor_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00354,1,0,true,false )
             ,new CursorDef("BC00355", "SELECT [Usuario_Ativo] AS ContratoGestor_UsuarioAtv, [Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00355,1,0,true,false )
             ,new CursorDef("BC00356", "SELECT [Contratada_Sigla] AS ContratoGestor_ContratadaSigla, [Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoGestor_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00356,1,0,true,false )
             ,new CursorDef("BC00357", "SELECT [AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratoGestor_ContratadaAreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00357,1,0,true,false )
             ,new CursorDef("BC00358", "SELECT [Pessoa_Nome] AS ContratoGestor_UsuarioPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoGestor_UsuarioPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00358,1,0,true,false )
             ,new CursorDef("BC00359", "SELECT T3.[Contratada_Sigla] AS ContratoGestor_ContratadaSigla, T3.[Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo, T4.[AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes, T6.[Pessoa_Nome] AS ContratoGestor_UsuarioPesNom, T5.[Usuario_Ativo] AS ContratoGestor_UsuarioAtv, TM1.[ContratoGestor_Tipo], TM1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, TM1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T5.[Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM ((((([ContratoGestor] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T2.[Contrato_AreaTrabalhoCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContratoGestor_UsuarioCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod and TM1.[ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod ORDER BY TM1.[ContratoGestor_ContratoCod], TM1.[ContratoGestor_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00359,100,0,true,false )
             ,new CursorDef("BC003510", "SELECT [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003510,1,0,true,false )
             ,new CursorDef("BC003511", "INSERT INTO [ContratoGestor]([ContratoGestor_Tipo], [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod]) VALUES(@ContratoGestor_Tipo, @ContratoGestor_ContratoCod, @ContratoGestor_UsuarioCod)", GxErrorMask.GX_NOMASK,prmBC003511)
             ,new CursorDef("BC003512", "UPDATE [ContratoGestor] SET [ContratoGestor_Tipo]=@ContratoGestor_Tipo  WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod", GxErrorMask.GX_NOMASK,prmBC003512)
             ,new CursorDef("BC003513", "DELETE FROM [ContratoGestor]  WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod", GxErrorMask.GX_NOMASK,prmBC003513)
             ,new CursorDef("BC003514", "SELECT [Contratada_Codigo] AS ContratoGestor_ContratadaCod, [Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoGestor_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003514,1,0,true,false )
             ,new CursorDef("BC003515", "SELECT [Contratada_Sigla] AS ContratoGestor_ContratadaSigla, [Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoGestor_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003515,1,0,true,false )
             ,new CursorDef("BC003516", "SELECT [AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratoGestor_ContratadaAreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003516,1,0,true,false )
             ,new CursorDef("BC003517", "SELECT [Usuario_Ativo] AS ContratoGestor_UsuarioAtv, [Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003517,1,0,true,false )
             ,new CursorDef("BC003518", "SELECT [Pessoa_Nome] AS ContratoGestor_UsuarioPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoGestor_UsuarioPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003518,1,0,true,false )
             ,new CursorDef("BC003519", "SELECT T3.[Contratada_Sigla] AS ContratoGestor_ContratadaSigla, T3.[Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo, T4.[AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes, T6.[Pessoa_Nome] AS ContratoGestor_UsuarioPesNom, T5.[Usuario_Ativo] AS ContratoGestor_UsuarioAtv, TM1.[ContratoGestor_Tipo], TM1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, TM1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T5.[Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM ((((([ContratoGestor] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T2.[Contrato_AreaTrabalhoCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContratoGestor_UsuarioCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod and TM1.[ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod ORDER BY TM1.[ContratoGestor_ContratoCod], TM1.[ContratoGestor_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003519,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
