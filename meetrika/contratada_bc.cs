/*
               File: Contratada_BC
        Description: Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:16:10.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratada_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratada_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratada_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0C13( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0C13( ) ;
         standaloneModal( ) ;
         AddRow0C13( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110C2 */
            E110C2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z39Contratada_Codigo = A39Contratada_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0C0( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0C13( ) ;
            }
            else
            {
               CheckExtendedTable0C13( ) ;
               if ( AnyError == 0 )
               {
                  ZM0C13( 13) ;
                  ZM0C13( 14) ;
                  ZM0C13( 15) ;
               }
               CloseExtendedTableCursors0C13( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E120C2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV39Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV40GXV1 = 1;
            while ( AV40GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV40GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_AreaTrabalhoCod") == 0 )
               {
                  AV14Insert_Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_PessoaCod") == 0 )
               {
                  AV11Insert_Contratada_PessoaCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contratada_MunicipioCod") == 0 )
               {
                  AV26Insert_Contratada_MunicipioCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV40GXV1 = (int)(AV40GXV1+1);
            }
         }
         AV23Contratada_PessoaCod = 0;
      }

      protected void E110C2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV35AuditingObject,  AV39Pgmname) ;
         new wwpbaseobjects.audittransaction(context ).execute(  AV35AuditingObject,  AV39Pgmname) ;
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
      }

      protected void E130C2( )
      {
         /* Contratada_cnpj_Isvalid Routine */
      }

      protected void E140C2( )
      {
         /* Contratada_TipoFabrica_Click Routine */
      }

      protected void ZM0C13( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z438Contratada_Sigla = A438Contratada_Sigla;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z342Contratada_BancoNome = A342Contratada_BancoNome;
            Z343Contratada_BancoNro = A343Contratada_BancoNro;
            Z344Contratada_AgenciaNome = A344Contratada_AgenciaNome;
            Z345Contratada_AgenciaNro = A345Contratada_AgenciaNro;
            Z51Contratada_ContaCorrente = A51Contratada_ContaCorrente;
            Z524Contratada_OS = A524Contratada_OS;
            Z1451Contratada_SS = A1451Contratada_SS;
            Z530Contratada_Lote = A530Contratada_Lote;
            Z1481Contratada_UsaOSistema = A1481Contratada_UsaOSistema;
            Z1867Contratada_OSPreferencial = A1867Contratada_OSPreferencial;
            Z1953Contratada_CntPadrao = A1953Contratada_CntPadrao;
            Z43Contratada_Ativo = A43Contratada_Ativo;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z349Contratada_MunicipioCod = A349Contratada_MunicipioCod;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z518Pessoa_IE = A518Pessoa_IE;
            Z519Pessoa_Endereco = A519Pessoa_Endereco;
            Z521Pessoa_CEP = A521Pessoa_CEP;
            Z522Pessoa_Telefone = A522Pessoa_Telefone;
            Z523Pessoa_Fax = A523Pessoa_Fax;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
         }
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            Z53Contratada_AreaTrabalhoDes = A53Contratada_AreaTrabalhoDes;
            Z1592Contratada_AreaTrbClcPFnl = A1592Contratada_AreaTrbClcPFnl;
            Z1595Contratada_AreaTrbSrvPdr = A1595Contratada_AreaTrbSrvPdr;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
         }
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            Z350Contratada_UF = A350Contratada_UF;
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
         }
         if ( GX_JID == -12 )
         {
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z438Contratada_Sigla = A438Contratada_Sigla;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z342Contratada_BancoNome = A342Contratada_BancoNome;
            Z343Contratada_BancoNro = A343Contratada_BancoNro;
            Z344Contratada_AgenciaNome = A344Contratada_AgenciaNome;
            Z345Contratada_AgenciaNro = A345Contratada_AgenciaNro;
            Z51Contratada_ContaCorrente = A51Contratada_ContaCorrente;
            Z524Contratada_OS = A524Contratada_OS;
            Z1451Contratada_SS = A1451Contratada_SS;
            Z530Contratada_Lote = A530Contratada_Lote;
            Z1127Contratada_LogoArquivo = A1127Contratada_LogoArquivo;
            Z1481Contratada_UsaOSistema = A1481Contratada_UsaOSistema;
            Z1867Contratada_OSPreferencial = A1867Contratada_OSPreferencial;
            Z1953Contratada_CntPadrao = A1953Contratada_CntPadrao;
            Z43Contratada_Ativo = A43Contratada_Ativo;
            Z1664Contratada_Logo = A1664Contratada_Logo;
            Z40000Contratada_Logo_GXI = A40000Contratada_Logo_GXI;
            Z1129Contratada_LogoTipoArq = A1129Contratada_LogoTipoArq;
            Z1128Contratada_LogoNomeArq = A1128Contratada_LogoNomeArq;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z349Contratada_MunicipioCod = A349Contratada_MunicipioCod;
            Z53Contratada_AreaTrabalhoDes = A53Contratada_AreaTrabalhoDes;
            Z1592Contratada_AreaTrbClcPFnl = A1592Contratada_AreaTrbClcPFnl;
            Z1595Contratada_AreaTrbSrvPdr = A1595Contratada_AreaTrbSrvPdr;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z518Pessoa_IE = A518Pessoa_IE;
            Z519Pessoa_Endereco = A519Pessoa_Endereco;
            Z521Pessoa_CEP = A521Pessoa_CEP;
            Z522Pessoa_Telefone = A522Pessoa_Telefone;
            Z523Pessoa_Fax = A523Pessoa_Fax;
            Z350Contratada_UF = A350Contratada_UF;
         }
      }

      protected void standaloneNotModal( )
      {
         AV39Pgmname = "Contratada_BC";
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A43Contratada_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A43Contratada_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A52Contratada_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
         {
            A52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A516Contratada_TipoFabrica)) && ( Gx_BScreen == 0 ) )
         {
            A516Contratada_TipoFabrica = "";
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor BC000C5 */
            pr_default.execute(3, new Object[] {A52Contratada_AreaTrabalhoCod});
            A53Contratada_AreaTrabalhoDes = BC000C5_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = BC000C5_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = BC000C5_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = BC000C5_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = BC000C5_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = BC000C5_n1595Contratada_AreaTrbSrvPdr[0];
            pr_default.close(3);
         }
      }

      protected void Load0C13( )
      {
         /* Using cursor BC000C7 */
         pr_default.execute(5, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound13 = 1;
            A53Contratada_AreaTrabalhoDes = BC000C7_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = BC000C7_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = BC000C7_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = BC000C7_n1592Contratada_AreaTrbClcPFnl[0];
            A41Contratada_PessoaNom = BC000C7_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000C7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000C7_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000C7_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = BC000C7_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000C7_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000C7_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000C7_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000C7_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000C7_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000C7_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000C7_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000C7_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000C7_n523Pessoa_Fax[0];
            A438Contratada_Sigla = BC000C7_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000C7_A516Contratada_TipoFabrica[0];
            A342Contratada_BancoNome = BC000C7_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = BC000C7_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = BC000C7_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = BC000C7_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = BC000C7_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = BC000C7_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = BC000C7_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = BC000C7_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = BC000C7_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = BC000C7_n51Contratada_ContaCorrente[0];
            A524Contratada_OS = BC000C7_A524Contratada_OS[0];
            n524Contratada_OS = BC000C7_n524Contratada_OS[0];
            A1451Contratada_SS = BC000C7_A1451Contratada_SS[0];
            n1451Contratada_SS = BC000C7_n1451Contratada_SS[0];
            A530Contratada_Lote = BC000C7_A530Contratada_Lote[0];
            n530Contratada_Lote = BC000C7_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = BC000C7_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = BC000C7_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = BC000C7_A1867Contratada_OSPreferencial[0];
            n1867Contratada_OSPreferencial = BC000C7_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = BC000C7_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = BC000C7_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = BC000C7_A43Contratada_Ativo[0];
            A40000Contratada_Logo_GXI = BC000C7_A40000Contratada_Logo_GXI[0];
            n40000Contratada_Logo_GXI = BC000C7_n40000Contratada_Logo_GXI[0];
            A1129Contratada_LogoTipoArq = BC000C7_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = BC000C7_n1129Contratada_LogoTipoArq[0];
            A1127Contratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
            A1128Contratada_LogoNomeArq = BC000C7_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = BC000C7_n1128Contratada_LogoNomeArq[0];
            A1127Contratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
            A40Contratada_PessoaCod = BC000C7_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = BC000C7_A52Contratada_AreaTrabalhoCod[0];
            A349Contratada_MunicipioCod = BC000C7_A349Contratada_MunicipioCod[0];
            n349Contratada_MunicipioCod = BC000C7_n349Contratada_MunicipioCod[0];
            A1595Contratada_AreaTrbSrvPdr = BC000C7_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = BC000C7_n1595Contratada_AreaTrbSrvPdr[0];
            A350Contratada_UF = BC000C7_A350Contratada_UF[0];
            n350Contratada_UF = BC000C7_n350Contratada_UF[0];
            A1127Contratada_LogoArquivo = BC000C7_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = BC000C7_n1127Contratada_LogoArquivo[0];
            A1664Contratada_Logo = BC000C7_A1664Contratada_Logo[0];
            n1664Contratada_Logo = BC000C7_n1664Contratada_Logo[0];
            ZM0C13( -12) ;
         }
         pr_default.close(5);
         OnLoadActions0C13( ) ;
      }

      protected void OnLoadActions0C13( )
      {
      }

      protected void CheckExtendedTable0C13( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000C5 */
         pr_default.execute(3, new Object[] {A52Contratada_AreaTrabalhoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTRATADA_AREATRABALHOCOD");
            AnyError = 1;
         }
         A53Contratada_AreaTrabalhoDes = BC000C5_A53Contratada_AreaTrabalhoDes[0];
         n53Contratada_AreaTrabalhoDes = BC000C5_n53Contratada_AreaTrabalhoDes[0];
         A1592Contratada_AreaTrbClcPFnl = BC000C5_A1592Contratada_AreaTrbClcPFnl[0];
         n1592Contratada_AreaTrbClcPFnl = BC000C5_n1592Contratada_AreaTrbClcPFnl[0];
         A1595Contratada_AreaTrbSrvPdr = BC000C5_A1595Contratada_AreaTrbSrvPdr[0];
         n1595Contratada_AreaTrbSrvPdr = BC000C5_n1595Contratada_AreaTrbSrvPdr[0];
         pr_default.close(3);
         /* Using cursor BC000C4 */
         pr_default.execute(2, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "CONTRATADA_PESSOACOD");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = BC000C4_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = BC000C4_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = BC000C4_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = BC000C4_n42Contratada_PessoaCNPJ[0];
         A518Pessoa_IE = BC000C4_A518Pessoa_IE[0];
         n518Pessoa_IE = BC000C4_n518Pessoa_IE[0];
         A519Pessoa_Endereco = BC000C4_A519Pessoa_Endereco[0];
         n519Pessoa_Endereco = BC000C4_n519Pessoa_Endereco[0];
         A521Pessoa_CEP = BC000C4_A521Pessoa_CEP[0];
         n521Pessoa_CEP = BC000C4_n521Pessoa_CEP[0];
         A522Pessoa_Telefone = BC000C4_A522Pessoa_Telefone[0];
         n522Pessoa_Telefone = BC000C4_n522Pessoa_Telefone[0];
         A523Pessoa_Fax = BC000C4_A523Pessoa_Fax[0];
         n523Pessoa_Fax = BC000C4_n523Pessoa_Fax[0];
         pr_default.close(2);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A438Contratada_Sigla)) )
         {
            GX_msglist.addItem("Sigla � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "S") == 0 ) || ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "M") == 0 ) || ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 ) || ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "O") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC000C6 */
         pr_default.execute(4, new Object[] {n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A349Contratada_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contratada_Municipio'.", "ForeignKeyNotFound", 1, "CONTRATADA_MUNICIPIOCOD");
               AnyError = 1;
            }
         }
         A350Contratada_UF = BC000C6_A350Contratada_UF[0];
         n350Contratada_UF = BC000C6_n350Contratada_UF[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors0C13( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0C13( )
      {
         /* Using cursor BC000C8 */
         pr_default.execute(6, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound13 = 1;
         }
         else
         {
            RcdFound13 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000C3 */
         pr_default.execute(1, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0C13( 12) ;
            RcdFound13 = 1;
            A39Contratada_Codigo = BC000C3_A39Contratada_Codigo[0];
            A438Contratada_Sigla = BC000C3_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000C3_A516Contratada_TipoFabrica[0];
            A342Contratada_BancoNome = BC000C3_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = BC000C3_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = BC000C3_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = BC000C3_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = BC000C3_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = BC000C3_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = BC000C3_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = BC000C3_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = BC000C3_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = BC000C3_n51Contratada_ContaCorrente[0];
            A524Contratada_OS = BC000C3_A524Contratada_OS[0];
            n524Contratada_OS = BC000C3_n524Contratada_OS[0];
            A1451Contratada_SS = BC000C3_A1451Contratada_SS[0];
            n1451Contratada_SS = BC000C3_n1451Contratada_SS[0];
            A530Contratada_Lote = BC000C3_A530Contratada_Lote[0];
            n530Contratada_Lote = BC000C3_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = BC000C3_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = BC000C3_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = BC000C3_A1867Contratada_OSPreferencial[0];
            n1867Contratada_OSPreferencial = BC000C3_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = BC000C3_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = BC000C3_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = BC000C3_A43Contratada_Ativo[0];
            A40000Contratada_Logo_GXI = BC000C3_A40000Contratada_Logo_GXI[0];
            n40000Contratada_Logo_GXI = BC000C3_n40000Contratada_Logo_GXI[0];
            A1129Contratada_LogoTipoArq = BC000C3_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = BC000C3_n1129Contratada_LogoTipoArq[0];
            A1127Contratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
            A1128Contratada_LogoNomeArq = BC000C3_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = BC000C3_n1128Contratada_LogoNomeArq[0];
            A1127Contratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
            A40Contratada_PessoaCod = BC000C3_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = BC000C3_A52Contratada_AreaTrabalhoCod[0];
            A349Contratada_MunicipioCod = BC000C3_A349Contratada_MunicipioCod[0];
            n349Contratada_MunicipioCod = BC000C3_n349Contratada_MunicipioCod[0];
            A1127Contratada_LogoArquivo = BC000C3_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = BC000C3_n1127Contratada_LogoArquivo[0];
            A1664Contratada_Logo = BC000C3_A1664Contratada_Logo[0];
            n1664Contratada_Logo = BC000C3_n1664Contratada_Logo[0];
            Z39Contratada_Codigo = A39Contratada_Codigo;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0C13( ) ;
            if ( AnyError == 1 )
            {
               RcdFound13 = 0;
               InitializeNonKey0C13( ) ;
            }
            Gx_mode = sMode13;
         }
         else
         {
            RcdFound13 = 0;
            InitializeNonKey0C13( ) ;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode13;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0C13( ) ;
         if ( RcdFound13 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0C0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000C2 */
            pr_default.execute(0, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratada"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z438Contratada_Sigla, BC000C2_A438Contratada_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z516Contratada_TipoFabrica, BC000C2_A516Contratada_TipoFabrica[0]) != 0 ) || ( StringUtil.StrCmp(Z342Contratada_BancoNome, BC000C2_A342Contratada_BancoNome[0]) != 0 ) || ( StringUtil.StrCmp(Z343Contratada_BancoNro, BC000C2_A343Contratada_BancoNro[0]) != 0 ) || ( StringUtil.StrCmp(Z344Contratada_AgenciaNome, BC000C2_A344Contratada_AgenciaNome[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z345Contratada_AgenciaNro, BC000C2_A345Contratada_AgenciaNro[0]) != 0 ) || ( StringUtil.StrCmp(Z51Contratada_ContaCorrente, BC000C2_A51Contratada_ContaCorrente[0]) != 0 ) || ( Z524Contratada_OS != BC000C2_A524Contratada_OS[0] ) || ( Z1451Contratada_SS != BC000C2_A1451Contratada_SS[0] ) || ( Z530Contratada_Lote != BC000C2_A530Contratada_Lote[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1481Contratada_UsaOSistema != BC000C2_A1481Contratada_UsaOSistema[0] ) || ( Z1867Contratada_OSPreferencial != BC000C2_A1867Contratada_OSPreferencial[0] ) || ( Z1953Contratada_CntPadrao != BC000C2_A1953Contratada_CntPadrao[0] ) || ( Z43Contratada_Ativo != BC000C2_A43Contratada_Ativo[0] ) || ( Z40Contratada_PessoaCod != BC000C2_A40Contratada_PessoaCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z52Contratada_AreaTrabalhoCod != BC000C2_A52Contratada_AreaTrabalhoCod[0] ) || ( Z349Contratada_MunicipioCod != BC000C2_A349Contratada_MunicipioCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Contratada"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0C13( 0) ;
            CheckOptimisticConcurrency0C13( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C13( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000C9 */
                     pr_default.execute(7, new Object[] {A438Contratada_Sigla, A516Contratada_TipoFabrica, n342Contratada_BancoNome, A342Contratada_BancoNome, n343Contratada_BancoNro, A343Contratada_BancoNro, n344Contratada_AgenciaNome, A344Contratada_AgenciaNome, n345Contratada_AgenciaNro, A345Contratada_AgenciaNro, n51Contratada_ContaCorrente, A51Contratada_ContaCorrente, n524Contratada_OS, A524Contratada_OS, n1451Contratada_SS, A1451Contratada_SS, n530Contratada_Lote, A530Contratada_Lote, n1127Contratada_LogoArquivo, A1127Contratada_LogoArquivo, n1481Contratada_UsaOSistema, A1481Contratada_UsaOSistema, n1867Contratada_OSPreferencial, A1867Contratada_OSPreferencial, n1953Contratada_CntPadrao, A1953Contratada_CntPadrao, A43Contratada_Ativo, n1664Contratada_Logo, A1664Contratada_Logo, n40000Contratada_Logo_GXI, A40000Contratada_Logo_GXI, n1129Contratada_LogoTipoArq, A1129Contratada_LogoTipoArq, n1128Contratada_LogoNomeArq, A1128Contratada_LogoNomeArq, A40Contratada_PessoaCod, A52Contratada_AreaTrabalhoCod, n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
                     A39Contratada_Codigo = BC000C9_A39Contratada_Codigo[0];
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0C13( ) ;
            }
            EndLevel0C13( ) ;
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void Update0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C13( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C13( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000C10 */
                     pr_default.execute(8, new Object[] {A438Contratada_Sigla, A516Contratada_TipoFabrica, n342Contratada_BancoNome, A342Contratada_BancoNome, n343Contratada_BancoNro, A343Contratada_BancoNro, n344Contratada_AgenciaNome, A344Contratada_AgenciaNome, n345Contratada_AgenciaNro, A345Contratada_AgenciaNro, n51Contratada_ContaCorrente, A51Contratada_ContaCorrente, n524Contratada_OS, A524Contratada_OS, n1451Contratada_SS, A1451Contratada_SS, n530Contratada_Lote, A530Contratada_Lote, n1481Contratada_UsaOSistema, A1481Contratada_UsaOSistema, n1867Contratada_OSPreferencial, A1867Contratada_OSPreferencial, n1953Contratada_CntPadrao, A1953Contratada_CntPadrao, A43Contratada_Ativo, n1129Contratada_LogoTipoArq, A1129Contratada_LogoTipoArq, n1128Contratada_LogoNomeArq, A1128Contratada_LogoNomeArq, A40Contratada_PessoaCod, A52Contratada_AreaTrabalhoCod, n349Contratada_MunicipioCod, A349Contratada_MunicipioCod, A39Contratada_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contratada"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0C13( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0C13( ) ;
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void DeferredUpdate0C13( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC000C11 */
            pr_default.execute(9, new Object[] {n1127Contratada_LogoArquivo, A1127Contratada_LogoArquivo, A39Contratada_Codigo});
            pr_default.close(9);
            dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000C12 */
            pr_default.execute(10, new Object[] {n1664Contratada_Logo, A1664Contratada_Logo, n40000Contratada_Logo_GXI, A40000Contratada_Logo_GXI, A39Contratada_Codigo});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0C13( ) ;
            AfterConfirm0C13( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0C13( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000C13 */
                  pr_default.execute(11, new Object[] {A39Contratada_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode13 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0C13( ) ;
         Gx_mode = sMode13;
      }

      protected void OnDeleteControls0C13( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000C14 */
            pr_default.execute(12, new Object[] {A52Contratada_AreaTrabalhoCod});
            A53Contratada_AreaTrabalhoDes = BC000C14_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = BC000C14_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = BC000C14_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = BC000C14_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = BC000C14_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = BC000C14_n1595Contratada_AreaTrbSrvPdr[0];
            pr_default.close(12);
            /* Using cursor BC000C15 */
            pr_default.execute(13, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = BC000C15_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000C15_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000C15_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000C15_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = BC000C15_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000C15_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000C15_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000C15_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000C15_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000C15_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000C15_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000C15_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000C15_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000C15_n523Pessoa_Fax[0];
            pr_default.close(13);
            /* Using cursor BC000C16 */
            pr_default.execute(14, new Object[] {n349Contratada_MunicipioCod, A349Contratada_MunicipioCod});
            A350Contratada_UF = BC000C16_A350Contratada_UF[0];
            n350Contratada_UF = BC000C16_n350Contratada_UF[0];
            pr_default.close(14);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000C17 */
            pr_default.execute(15, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tributo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor BC000C18 */
            pr_default.execute(16, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Gest�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor BC000C19 */
            pr_default.execute(17, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resutlado_Contratada Origem"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor BC000C20 */
            pr_default.execute(18, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"+" ("+"Contagem Resultado_Contratada"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor BC000C21 */
            pr_default.execute(19, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
            /* Using cursor BC000C22 */
            pr_default.execute(20, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor BC000C23 */
            pr_default.execute(21, new Object[] {A39Contratada_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
         }
      }

      protected void EndLevel0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0C13( )
      {
         /* Scan By routine */
         /* Using cursor BC000C24 */
         pr_default.execute(22, new Object[] {A39Contratada_Codigo});
         RcdFound13 = 0;
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound13 = 1;
            A39Contratada_Codigo = BC000C24_A39Contratada_Codigo[0];
            A53Contratada_AreaTrabalhoDes = BC000C24_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = BC000C24_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = BC000C24_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = BC000C24_n1592Contratada_AreaTrbClcPFnl[0];
            A41Contratada_PessoaNom = BC000C24_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000C24_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000C24_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000C24_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = BC000C24_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000C24_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000C24_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000C24_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000C24_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000C24_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000C24_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000C24_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000C24_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000C24_n523Pessoa_Fax[0];
            A438Contratada_Sigla = BC000C24_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000C24_A516Contratada_TipoFabrica[0];
            A342Contratada_BancoNome = BC000C24_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = BC000C24_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = BC000C24_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = BC000C24_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = BC000C24_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = BC000C24_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = BC000C24_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = BC000C24_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = BC000C24_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = BC000C24_n51Contratada_ContaCorrente[0];
            A524Contratada_OS = BC000C24_A524Contratada_OS[0];
            n524Contratada_OS = BC000C24_n524Contratada_OS[0];
            A1451Contratada_SS = BC000C24_A1451Contratada_SS[0];
            n1451Contratada_SS = BC000C24_n1451Contratada_SS[0];
            A530Contratada_Lote = BC000C24_A530Contratada_Lote[0];
            n530Contratada_Lote = BC000C24_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = BC000C24_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = BC000C24_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = BC000C24_A1867Contratada_OSPreferencial[0];
            n1867Contratada_OSPreferencial = BC000C24_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = BC000C24_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = BC000C24_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = BC000C24_A43Contratada_Ativo[0];
            A40000Contratada_Logo_GXI = BC000C24_A40000Contratada_Logo_GXI[0];
            n40000Contratada_Logo_GXI = BC000C24_n40000Contratada_Logo_GXI[0];
            A1129Contratada_LogoTipoArq = BC000C24_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = BC000C24_n1129Contratada_LogoTipoArq[0];
            A1127Contratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
            A1128Contratada_LogoNomeArq = BC000C24_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = BC000C24_n1128Contratada_LogoNomeArq[0];
            A1127Contratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
            A40Contratada_PessoaCod = BC000C24_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = BC000C24_A52Contratada_AreaTrabalhoCod[0];
            A349Contratada_MunicipioCod = BC000C24_A349Contratada_MunicipioCod[0];
            n349Contratada_MunicipioCod = BC000C24_n349Contratada_MunicipioCod[0];
            A1595Contratada_AreaTrbSrvPdr = BC000C24_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = BC000C24_n1595Contratada_AreaTrbSrvPdr[0];
            A350Contratada_UF = BC000C24_A350Contratada_UF[0];
            n350Contratada_UF = BC000C24_n350Contratada_UF[0];
            A1127Contratada_LogoArquivo = BC000C24_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = BC000C24_n1127Contratada_LogoArquivo[0];
            A1664Contratada_Logo = BC000C24_A1664Contratada_Logo[0];
            n1664Contratada_Logo = BC000C24_n1664Contratada_Logo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0C13( )
      {
         /* Scan next routine */
         pr_default.readNext(22);
         RcdFound13 = 0;
         ScanKeyLoad0C13( ) ;
      }

      protected void ScanKeyLoad0C13( )
      {
         sMode13 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound13 = 1;
            A39Contratada_Codigo = BC000C24_A39Contratada_Codigo[0];
            A53Contratada_AreaTrabalhoDes = BC000C24_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = BC000C24_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = BC000C24_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = BC000C24_n1592Contratada_AreaTrbClcPFnl[0];
            A41Contratada_PessoaNom = BC000C24_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000C24_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000C24_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000C24_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = BC000C24_A518Pessoa_IE[0];
            n518Pessoa_IE = BC000C24_n518Pessoa_IE[0];
            A519Pessoa_Endereco = BC000C24_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = BC000C24_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = BC000C24_A521Pessoa_CEP[0];
            n521Pessoa_CEP = BC000C24_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = BC000C24_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = BC000C24_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = BC000C24_A523Pessoa_Fax[0];
            n523Pessoa_Fax = BC000C24_n523Pessoa_Fax[0];
            A438Contratada_Sigla = BC000C24_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = BC000C24_A516Contratada_TipoFabrica[0];
            A342Contratada_BancoNome = BC000C24_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = BC000C24_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = BC000C24_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = BC000C24_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = BC000C24_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = BC000C24_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = BC000C24_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = BC000C24_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = BC000C24_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = BC000C24_n51Contratada_ContaCorrente[0];
            A524Contratada_OS = BC000C24_A524Contratada_OS[0];
            n524Contratada_OS = BC000C24_n524Contratada_OS[0];
            A1451Contratada_SS = BC000C24_A1451Contratada_SS[0];
            n1451Contratada_SS = BC000C24_n1451Contratada_SS[0];
            A530Contratada_Lote = BC000C24_A530Contratada_Lote[0];
            n530Contratada_Lote = BC000C24_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = BC000C24_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = BC000C24_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = BC000C24_A1867Contratada_OSPreferencial[0];
            n1867Contratada_OSPreferencial = BC000C24_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = BC000C24_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = BC000C24_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = BC000C24_A43Contratada_Ativo[0];
            A40000Contratada_Logo_GXI = BC000C24_A40000Contratada_Logo_GXI[0];
            n40000Contratada_Logo_GXI = BC000C24_n40000Contratada_Logo_GXI[0];
            A1129Contratada_LogoTipoArq = BC000C24_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = BC000C24_n1129Contratada_LogoTipoArq[0];
            A1127Contratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
            A1128Contratada_LogoNomeArq = BC000C24_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = BC000C24_n1128Contratada_LogoNomeArq[0];
            A1127Contratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
            A40Contratada_PessoaCod = BC000C24_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = BC000C24_A52Contratada_AreaTrabalhoCod[0];
            A349Contratada_MunicipioCod = BC000C24_A349Contratada_MunicipioCod[0];
            n349Contratada_MunicipioCod = BC000C24_n349Contratada_MunicipioCod[0];
            A1595Contratada_AreaTrbSrvPdr = BC000C24_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = BC000C24_n1595Contratada_AreaTrbSrvPdr[0];
            A350Contratada_UF = BC000C24_A350Contratada_UF[0];
            n350Contratada_UF = BC000C24_n350Contratada_UF[0];
            A1127Contratada_LogoArquivo = BC000C24_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = BC000C24_n1127Contratada_LogoArquivo[0];
            A1664Contratada_Logo = BC000C24_A1664Contratada_Logo[0];
            n1664Contratada_Logo = BC000C24_n1664Contratada_Logo[0];
         }
         Gx_mode = sMode13;
      }

      protected void ScanKeyEnd0C13( )
      {
         pr_default.close(22);
      }

      protected void AfterConfirm0C13( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0C13( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0C13( )
      {
         /* Before Update Rules */
         new loadauditcontratada(context ).execute(  "Y", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
      }

      protected void BeforeDelete0C13( )
      {
         /* Before Delete Rules */
         new loadauditcontratada(context ).execute(  "Y", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete0C13( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratada(context ).execute(  "N", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratada(context ).execute(  "N", ref  AV35AuditingObject,  A39Contratada_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate0C13( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0C13( )
      {
      }

      protected void AddRow0C13( )
      {
         VarsToRow13( bcContratada) ;
      }

      protected void ReadRow0C13( )
      {
         RowToVars13( bcContratada, 1) ;
      }

      protected void InitializeNonKey0C13( )
      {
         AV35AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A53Contratada_AreaTrabalhoDes = "";
         n53Contratada_AreaTrabalhoDes = false;
         A1592Contratada_AreaTrbClcPFnl = "";
         n1592Contratada_AreaTrbClcPFnl = false;
         A1595Contratada_AreaTrbSrvPdr = 0;
         n1595Contratada_AreaTrbSrvPdr = false;
         A40Contratada_PessoaCod = 0;
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         A518Pessoa_IE = "";
         n518Pessoa_IE = false;
         A519Pessoa_Endereco = "";
         n519Pessoa_Endereco = false;
         A521Pessoa_CEP = "";
         n521Pessoa_CEP = false;
         A522Pessoa_Telefone = "";
         n522Pessoa_Telefone = false;
         A523Pessoa_Fax = "";
         n523Pessoa_Fax = false;
         A438Contratada_Sigla = "";
         A342Contratada_BancoNome = "";
         n342Contratada_BancoNome = false;
         A343Contratada_BancoNro = "";
         n343Contratada_BancoNro = false;
         A344Contratada_AgenciaNome = "";
         n344Contratada_AgenciaNome = false;
         A345Contratada_AgenciaNro = "";
         n345Contratada_AgenciaNro = false;
         A51Contratada_ContaCorrente = "";
         n51Contratada_ContaCorrente = false;
         A349Contratada_MunicipioCod = 0;
         n349Contratada_MunicipioCod = false;
         A350Contratada_UF = "";
         n350Contratada_UF = false;
         A524Contratada_OS = 0;
         n524Contratada_OS = false;
         A1451Contratada_SS = 0;
         n1451Contratada_SS = false;
         A530Contratada_Lote = 0;
         n530Contratada_Lote = false;
         A1127Contratada_LogoArquivo = "";
         n1127Contratada_LogoArquivo = false;
         A1481Contratada_UsaOSistema = false;
         n1481Contratada_UsaOSistema = false;
         A1867Contratada_OSPreferencial = false;
         n1867Contratada_OSPreferencial = false;
         A1953Contratada_CntPadrao = 0;
         n1953Contratada_CntPadrao = false;
         A1664Contratada_Logo = "";
         n1664Contratada_Logo = false;
         A40000Contratada_Logo_GXI = "";
         n40000Contratada_Logo_GXI = false;
         A1129Contratada_LogoTipoArq = "";
         n1129Contratada_LogoTipoArq = false;
         A1128Contratada_LogoNomeArq = "";
         n1128Contratada_LogoNomeArq = false;
         A52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A516Contratada_TipoFabrica = "";
         A43Contratada_Ativo = true;
         Z438Contratada_Sigla = "";
         Z516Contratada_TipoFabrica = "";
         Z342Contratada_BancoNome = "";
         Z343Contratada_BancoNro = "";
         Z344Contratada_AgenciaNome = "";
         Z345Contratada_AgenciaNro = "";
         Z51Contratada_ContaCorrente = "";
         Z524Contratada_OS = 0;
         Z1451Contratada_SS = 0;
         Z530Contratada_Lote = 0;
         Z1481Contratada_UsaOSistema = false;
         Z1867Contratada_OSPreferencial = false;
         Z1953Contratada_CntPadrao = 0;
         Z43Contratada_Ativo = false;
         Z40Contratada_PessoaCod = 0;
         Z52Contratada_AreaTrabalhoCod = 0;
         Z349Contratada_MunicipioCod = 0;
      }

      protected void InitAll0C13( )
      {
         A39Contratada_Codigo = 0;
         InitializeNonKey0C13( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A43Contratada_Ativo = i43Contratada_Ativo;
         A52Contratada_AreaTrabalhoCod = i52Contratada_AreaTrabalhoCod;
         A516Contratada_TipoFabrica = i516Contratada_TipoFabrica;
      }

      public void VarsToRow13( SdtContratada obj13 )
      {
         obj13.gxTpr_Mode = Gx_mode;
         obj13.gxTpr_Contratada_areatrabalhodes = A53Contratada_AreaTrabalhoDes;
         obj13.gxTpr_Contratada_areatrbclcpfnl = A1592Contratada_AreaTrbClcPFnl;
         obj13.gxTpr_Contratada_areatrbsrvpdr = A1595Contratada_AreaTrbSrvPdr;
         obj13.gxTpr_Contratada_pessoacod = A40Contratada_PessoaCod;
         obj13.gxTpr_Contratada_pessoanom = A41Contratada_PessoaNom;
         obj13.gxTpr_Contratada_pessoacnpj = A42Contratada_PessoaCNPJ;
         obj13.gxTpr_Pessoa_ie = A518Pessoa_IE;
         obj13.gxTpr_Pessoa_endereco = A519Pessoa_Endereco;
         obj13.gxTpr_Pessoa_cep = A521Pessoa_CEP;
         obj13.gxTpr_Pessoa_telefone = A522Pessoa_Telefone;
         obj13.gxTpr_Pessoa_fax = A523Pessoa_Fax;
         obj13.gxTpr_Contratada_sigla = A438Contratada_Sigla;
         obj13.gxTpr_Contratada_banconome = A342Contratada_BancoNome;
         obj13.gxTpr_Contratada_banconro = A343Contratada_BancoNro;
         obj13.gxTpr_Contratada_agencianome = A344Contratada_AgenciaNome;
         obj13.gxTpr_Contratada_agencianro = A345Contratada_AgenciaNro;
         obj13.gxTpr_Contratada_contacorrente = A51Contratada_ContaCorrente;
         obj13.gxTpr_Contratada_municipiocod = A349Contratada_MunicipioCod;
         obj13.gxTpr_Contratada_uf = A350Contratada_UF;
         obj13.gxTpr_Contratada_os = A524Contratada_OS;
         obj13.gxTpr_Contratada_ss = A1451Contratada_SS;
         obj13.gxTpr_Contratada_lote = A530Contratada_Lote;
         obj13.gxTpr_Contratada_logoarquivo = A1127Contratada_LogoArquivo;
         obj13.gxTpr_Contratada_usaosistema = A1481Contratada_UsaOSistema;
         obj13.gxTpr_Contratada_ospreferencial = A1867Contratada_OSPreferencial;
         obj13.gxTpr_Contratada_cntpadrao = A1953Contratada_CntPadrao;
         obj13.gxTpr_Contratada_logo = A1664Contratada_Logo;
         obj13.gxTpr_Contratada_logo_gxi = A40000Contratada_Logo_GXI;
         obj13.gxTpr_Contratada_logotipoarq = A1129Contratada_LogoTipoArq;
         obj13.gxTpr_Contratada_logonomearq = A1128Contratada_LogoNomeArq;
         obj13.gxTpr_Contratada_areatrabalhocod = A52Contratada_AreaTrabalhoCod;
         obj13.gxTpr_Contratada_tipofabrica = A516Contratada_TipoFabrica;
         obj13.gxTpr_Contratada_ativo = A43Contratada_Ativo;
         obj13.gxTpr_Contratada_codigo = A39Contratada_Codigo;
         obj13.gxTpr_Pessoa_ie_Z = Z518Pessoa_IE;
         obj13.gxTpr_Pessoa_endereco_Z = Z519Pessoa_Endereco;
         obj13.gxTpr_Pessoa_cep_Z = Z521Pessoa_CEP;
         obj13.gxTpr_Pessoa_telefone_Z = Z522Pessoa_Telefone;
         obj13.gxTpr_Pessoa_fax_Z = Z523Pessoa_Fax;
         obj13.gxTpr_Contratada_codigo_Z = Z39Contratada_Codigo;
         obj13.gxTpr_Contratada_areatrabalhocod_Z = Z52Contratada_AreaTrabalhoCod;
         obj13.gxTpr_Contratada_areatrabalhodes_Z = Z53Contratada_AreaTrabalhoDes;
         obj13.gxTpr_Contratada_areatrbclcpfnl_Z = Z1592Contratada_AreaTrbClcPFnl;
         obj13.gxTpr_Contratada_areatrbsrvpdr_Z = Z1595Contratada_AreaTrbSrvPdr;
         obj13.gxTpr_Contratada_pessoacod_Z = Z40Contratada_PessoaCod;
         obj13.gxTpr_Contratada_pessoanom_Z = Z41Contratada_PessoaNom;
         obj13.gxTpr_Contratada_pessoacnpj_Z = Z42Contratada_PessoaCNPJ;
         obj13.gxTpr_Pessoa_ie_Z = Z518Pessoa_IE;
         obj13.gxTpr_Pessoa_endereco_Z = Z519Pessoa_Endereco;
         obj13.gxTpr_Pessoa_cep_Z = Z521Pessoa_CEP;
         obj13.gxTpr_Pessoa_telefone_Z = Z522Pessoa_Telefone;
         obj13.gxTpr_Pessoa_fax_Z = Z523Pessoa_Fax;
         obj13.gxTpr_Contratada_sigla_Z = Z438Contratada_Sigla;
         obj13.gxTpr_Contratada_tipofabrica_Z = Z516Contratada_TipoFabrica;
         obj13.gxTpr_Contratada_banconome_Z = Z342Contratada_BancoNome;
         obj13.gxTpr_Contratada_banconro_Z = Z343Contratada_BancoNro;
         obj13.gxTpr_Contratada_agencianome_Z = Z344Contratada_AgenciaNome;
         obj13.gxTpr_Contratada_agencianro_Z = Z345Contratada_AgenciaNro;
         obj13.gxTpr_Contratada_contacorrente_Z = Z51Contratada_ContaCorrente;
         obj13.gxTpr_Contratada_municipiocod_Z = Z349Contratada_MunicipioCod;
         obj13.gxTpr_Contratada_uf_Z = Z350Contratada_UF;
         obj13.gxTpr_Contratada_os_Z = Z524Contratada_OS;
         obj13.gxTpr_Contratada_ss_Z = Z1451Contratada_SS;
         obj13.gxTpr_Contratada_lote_Z = Z530Contratada_Lote;
         obj13.gxTpr_Contratada_logonomearq_Z = Z1128Contratada_LogoNomeArq;
         obj13.gxTpr_Contratada_logotipoarq_Z = Z1129Contratada_LogoTipoArq;
         obj13.gxTpr_Contratada_usaosistema_Z = Z1481Contratada_UsaOSistema;
         obj13.gxTpr_Contratada_ospreferencial_Z = Z1867Contratada_OSPreferencial;
         obj13.gxTpr_Contratada_cntpadrao_Z = Z1953Contratada_CntPadrao;
         obj13.gxTpr_Contratada_ativo_Z = Z43Contratada_Ativo;
         obj13.gxTpr_Contratada_logo_gxi_Z = Z40000Contratada_Logo_GXI;
         obj13.gxTpr_Contratada_areatrabalhodes_N = (short)(Convert.ToInt16(n53Contratada_AreaTrabalhoDes));
         obj13.gxTpr_Contratada_areatrbclcpfnl_N = (short)(Convert.ToInt16(n1592Contratada_AreaTrbClcPFnl));
         obj13.gxTpr_Contratada_areatrbsrvpdr_N = (short)(Convert.ToInt16(n1595Contratada_AreaTrbSrvPdr));
         obj13.gxTpr_Contratada_pessoanom_N = (short)(Convert.ToInt16(n41Contratada_PessoaNom));
         obj13.gxTpr_Contratada_pessoacnpj_N = (short)(Convert.ToInt16(n42Contratada_PessoaCNPJ));
         obj13.gxTpr_Pessoa_ie_N = (short)(Convert.ToInt16(n518Pessoa_IE));
         obj13.gxTpr_Pessoa_endereco_N = (short)(Convert.ToInt16(n519Pessoa_Endereco));
         obj13.gxTpr_Pessoa_cep_N = (short)(Convert.ToInt16(n521Pessoa_CEP));
         obj13.gxTpr_Pessoa_telefone_N = (short)(Convert.ToInt16(n522Pessoa_Telefone));
         obj13.gxTpr_Pessoa_fax_N = (short)(Convert.ToInt16(n523Pessoa_Fax));
         obj13.gxTpr_Contratada_banconome_N = (short)(Convert.ToInt16(n342Contratada_BancoNome));
         obj13.gxTpr_Contratada_banconro_N = (short)(Convert.ToInt16(n343Contratada_BancoNro));
         obj13.gxTpr_Contratada_agencianome_N = (short)(Convert.ToInt16(n344Contratada_AgenciaNome));
         obj13.gxTpr_Contratada_agencianro_N = (short)(Convert.ToInt16(n345Contratada_AgenciaNro));
         obj13.gxTpr_Contratada_contacorrente_N = (short)(Convert.ToInt16(n51Contratada_ContaCorrente));
         obj13.gxTpr_Contratada_municipiocod_N = (short)(Convert.ToInt16(n349Contratada_MunicipioCod));
         obj13.gxTpr_Contratada_uf_N = (short)(Convert.ToInt16(n350Contratada_UF));
         obj13.gxTpr_Contratada_os_N = (short)(Convert.ToInt16(n524Contratada_OS));
         obj13.gxTpr_Contratada_ss_N = (short)(Convert.ToInt16(n1451Contratada_SS));
         obj13.gxTpr_Contratada_lote_N = (short)(Convert.ToInt16(n530Contratada_Lote));
         obj13.gxTpr_Contratada_logoarquivo_N = (short)(Convert.ToInt16(n1127Contratada_LogoArquivo));
         obj13.gxTpr_Contratada_logonomearq_N = (short)(Convert.ToInt16(n1128Contratada_LogoNomeArq));
         obj13.gxTpr_Contratada_logotipoarq_N = (short)(Convert.ToInt16(n1129Contratada_LogoTipoArq));
         obj13.gxTpr_Contratada_usaosistema_N = (short)(Convert.ToInt16(n1481Contratada_UsaOSistema));
         obj13.gxTpr_Contratada_ospreferencial_N = (short)(Convert.ToInt16(n1867Contratada_OSPreferencial));
         obj13.gxTpr_Contratada_cntpadrao_N = (short)(Convert.ToInt16(n1953Contratada_CntPadrao));
         obj13.gxTpr_Contratada_logo_N = (short)(Convert.ToInt16(n1664Contratada_Logo));
         obj13.gxTpr_Contratada_logo_gxi_N = (short)(Convert.ToInt16(n40000Contratada_Logo_GXI));
         obj13.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow13( SdtContratada obj13 )
      {
         obj13.gxTpr_Contratada_codigo = A39Contratada_Codigo;
         return  ;
      }

      public void RowToVars13( SdtContratada obj13 ,
                               int forceLoad )
      {
         Gx_mode = obj13.gxTpr_Mode;
         A53Contratada_AreaTrabalhoDes = obj13.gxTpr_Contratada_areatrabalhodes;
         n53Contratada_AreaTrabalhoDes = false;
         A1592Contratada_AreaTrbClcPFnl = obj13.gxTpr_Contratada_areatrbclcpfnl;
         n1592Contratada_AreaTrbClcPFnl = false;
         A1595Contratada_AreaTrbSrvPdr = obj13.gxTpr_Contratada_areatrbsrvpdr;
         n1595Contratada_AreaTrbSrvPdr = false;
         A40Contratada_PessoaCod = obj13.gxTpr_Contratada_pessoacod;
         A41Contratada_PessoaNom = obj13.gxTpr_Contratada_pessoanom;
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = obj13.gxTpr_Contratada_pessoacnpj;
         n42Contratada_PessoaCNPJ = false;
         A518Pessoa_IE = obj13.gxTpr_Pessoa_ie;
         n518Pessoa_IE = false;
         A519Pessoa_Endereco = obj13.gxTpr_Pessoa_endereco;
         n519Pessoa_Endereco = false;
         A521Pessoa_CEP = obj13.gxTpr_Pessoa_cep;
         n521Pessoa_CEP = false;
         A522Pessoa_Telefone = obj13.gxTpr_Pessoa_telefone;
         n522Pessoa_Telefone = false;
         A523Pessoa_Fax = obj13.gxTpr_Pessoa_fax;
         n523Pessoa_Fax = false;
         A438Contratada_Sigla = obj13.gxTpr_Contratada_sigla;
         A342Contratada_BancoNome = obj13.gxTpr_Contratada_banconome;
         n342Contratada_BancoNome = false;
         A343Contratada_BancoNro = obj13.gxTpr_Contratada_banconro;
         n343Contratada_BancoNro = false;
         A344Contratada_AgenciaNome = obj13.gxTpr_Contratada_agencianome;
         n344Contratada_AgenciaNome = false;
         A345Contratada_AgenciaNro = obj13.gxTpr_Contratada_agencianro;
         n345Contratada_AgenciaNro = false;
         A51Contratada_ContaCorrente = obj13.gxTpr_Contratada_contacorrente;
         n51Contratada_ContaCorrente = false;
         A349Contratada_MunicipioCod = obj13.gxTpr_Contratada_municipiocod;
         n349Contratada_MunicipioCod = false;
         A350Contratada_UF = obj13.gxTpr_Contratada_uf;
         n350Contratada_UF = false;
         A524Contratada_OS = obj13.gxTpr_Contratada_os;
         n524Contratada_OS = false;
         A1451Contratada_SS = obj13.gxTpr_Contratada_ss;
         n1451Contratada_SS = false;
         A530Contratada_Lote = obj13.gxTpr_Contratada_lote;
         n530Contratada_Lote = false;
         A1127Contratada_LogoArquivo = obj13.gxTpr_Contratada_logoarquivo;
         n1127Contratada_LogoArquivo = false;
         A1481Contratada_UsaOSistema = obj13.gxTpr_Contratada_usaosistema;
         n1481Contratada_UsaOSistema = false;
         A1867Contratada_OSPreferencial = obj13.gxTpr_Contratada_ospreferencial;
         n1867Contratada_OSPreferencial = false;
         A1953Contratada_CntPadrao = obj13.gxTpr_Contratada_cntpadrao;
         n1953Contratada_CntPadrao = false;
         A1664Contratada_Logo = obj13.gxTpr_Contratada_logo;
         n1664Contratada_Logo = false;
         A40000Contratada_Logo_GXI = obj13.gxTpr_Contratada_logo_gxi;
         n40000Contratada_Logo_GXI = false;
         A1129Contratada_LogoTipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj13.gxTpr_Contratada_logotipoarq)) ? FileUtil.GetFileType( A1127Contratada_LogoArquivo) : obj13.gxTpr_Contratada_logotipoarq);
         n1129Contratada_LogoTipoArq = false;
         A1128Contratada_LogoNomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( obj13.gxTpr_Contratada_logonomearq)) ? FileUtil.GetFileName( A1127Contratada_LogoArquivo) : obj13.gxTpr_Contratada_logonomearq);
         n1128Contratada_LogoNomeArq = false;
         A52Contratada_AreaTrabalhoCod = obj13.gxTpr_Contratada_areatrabalhocod;
         A516Contratada_TipoFabrica = obj13.gxTpr_Contratada_tipofabrica;
         A43Contratada_Ativo = obj13.gxTpr_Contratada_ativo;
         A39Contratada_Codigo = obj13.gxTpr_Contratada_codigo;
         Z518Pessoa_IE = obj13.gxTpr_Pessoa_ie_Z;
         Z519Pessoa_Endereco = obj13.gxTpr_Pessoa_endereco_Z;
         Z521Pessoa_CEP = obj13.gxTpr_Pessoa_cep_Z;
         Z522Pessoa_Telefone = obj13.gxTpr_Pessoa_telefone_Z;
         Z523Pessoa_Fax = obj13.gxTpr_Pessoa_fax_Z;
         Z39Contratada_Codigo = obj13.gxTpr_Contratada_codigo_Z;
         Z52Contratada_AreaTrabalhoCod = obj13.gxTpr_Contratada_areatrabalhocod_Z;
         Z53Contratada_AreaTrabalhoDes = obj13.gxTpr_Contratada_areatrabalhodes_Z;
         Z1592Contratada_AreaTrbClcPFnl = obj13.gxTpr_Contratada_areatrbclcpfnl_Z;
         Z1595Contratada_AreaTrbSrvPdr = obj13.gxTpr_Contratada_areatrbsrvpdr_Z;
         Z40Contratada_PessoaCod = obj13.gxTpr_Contratada_pessoacod_Z;
         Z41Contratada_PessoaNom = obj13.gxTpr_Contratada_pessoanom_Z;
         Z42Contratada_PessoaCNPJ = obj13.gxTpr_Contratada_pessoacnpj_Z;
         Z518Pessoa_IE = obj13.gxTpr_Pessoa_ie_Z;
         Z519Pessoa_Endereco = obj13.gxTpr_Pessoa_endereco_Z;
         Z521Pessoa_CEP = obj13.gxTpr_Pessoa_cep_Z;
         Z522Pessoa_Telefone = obj13.gxTpr_Pessoa_telefone_Z;
         Z523Pessoa_Fax = obj13.gxTpr_Pessoa_fax_Z;
         Z438Contratada_Sigla = obj13.gxTpr_Contratada_sigla_Z;
         Z516Contratada_TipoFabrica = obj13.gxTpr_Contratada_tipofabrica_Z;
         Z342Contratada_BancoNome = obj13.gxTpr_Contratada_banconome_Z;
         Z343Contratada_BancoNro = obj13.gxTpr_Contratada_banconro_Z;
         Z344Contratada_AgenciaNome = obj13.gxTpr_Contratada_agencianome_Z;
         Z345Contratada_AgenciaNro = obj13.gxTpr_Contratada_agencianro_Z;
         Z51Contratada_ContaCorrente = obj13.gxTpr_Contratada_contacorrente_Z;
         Z349Contratada_MunicipioCod = obj13.gxTpr_Contratada_municipiocod_Z;
         Z350Contratada_UF = obj13.gxTpr_Contratada_uf_Z;
         Z524Contratada_OS = obj13.gxTpr_Contratada_os_Z;
         Z1451Contratada_SS = obj13.gxTpr_Contratada_ss_Z;
         Z530Contratada_Lote = obj13.gxTpr_Contratada_lote_Z;
         Z1128Contratada_LogoNomeArq = obj13.gxTpr_Contratada_logonomearq_Z;
         Z1129Contratada_LogoTipoArq = obj13.gxTpr_Contratada_logotipoarq_Z;
         Z1481Contratada_UsaOSistema = obj13.gxTpr_Contratada_usaosistema_Z;
         Z1867Contratada_OSPreferencial = obj13.gxTpr_Contratada_ospreferencial_Z;
         Z1953Contratada_CntPadrao = obj13.gxTpr_Contratada_cntpadrao_Z;
         Z43Contratada_Ativo = obj13.gxTpr_Contratada_ativo_Z;
         Z40000Contratada_Logo_GXI = obj13.gxTpr_Contratada_logo_gxi_Z;
         n53Contratada_AreaTrabalhoDes = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_areatrabalhodes_N));
         n1592Contratada_AreaTrbClcPFnl = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_areatrbclcpfnl_N));
         n1595Contratada_AreaTrbSrvPdr = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_areatrbsrvpdr_N));
         n41Contratada_PessoaNom = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_pessoanom_N));
         n42Contratada_PessoaCNPJ = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_pessoacnpj_N));
         n518Pessoa_IE = (bool)(Convert.ToBoolean(obj13.gxTpr_Pessoa_ie_N));
         n519Pessoa_Endereco = (bool)(Convert.ToBoolean(obj13.gxTpr_Pessoa_endereco_N));
         n521Pessoa_CEP = (bool)(Convert.ToBoolean(obj13.gxTpr_Pessoa_cep_N));
         n522Pessoa_Telefone = (bool)(Convert.ToBoolean(obj13.gxTpr_Pessoa_telefone_N));
         n523Pessoa_Fax = (bool)(Convert.ToBoolean(obj13.gxTpr_Pessoa_fax_N));
         n342Contratada_BancoNome = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_banconome_N));
         n343Contratada_BancoNro = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_banconro_N));
         n344Contratada_AgenciaNome = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_agencianome_N));
         n345Contratada_AgenciaNro = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_agencianro_N));
         n51Contratada_ContaCorrente = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_contacorrente_N));
         n349Contratada_MunicipioCod = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_municipiocod_N));
         n350Contratada_UF = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_uf_N));
         n524Contratada_OS = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_os_N));
         n1451Contratada_SS = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_ss_N));
         n530Contratada_Lote = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_lote_N));
         n1127Contratada_LogoArquivo = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_logoarquivo_N));
         n1128Contratada_LogoNomeArq = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_logonomearq_N));
         n1129Contratada_LogoTipoArq = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_logotipoarq_N));
         n1481Contratada_UsaOSistema = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_usaosistema_N));
         n1867Contratada_OSPreferencial = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_ospreferencial_N));
         n1953Contratada_CntPadrao = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_cntpadrao_N));
         n1664Contratada_Logo = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_logo_N));
         n40000Contratada_Logo_GXI = (bool)(Convert.ToBoolean(obj13.gxTpr_Contratada_logo_gxi_N));
         Gx_mode = obj13.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A39Contratada_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0C13( ) ;
         ScanKeyStart0C13( ) ;
         if ( RcdFound13 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z39Contratada_Codigo = A39Contratada_Codigo;
         }
         ZM0C13( -12) ;
         OnLoadActions0C13( ) ;
         AddRow0C13( ) ;
         ScanKeyEnd0C13( ) ;
         if ( RcdFound13 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars13( bcContratada, 0) ;
         ScanKeyStart0C13( ) ;
         if ( RcdFound13 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z39Contratada_Codigo = A39Contratada_Codigo;
         }
         ZM0C13( -12) ;
         OnLoadActions0C13( ) ;
         AddRow0C13( ) ;
         ScanKeyEnd0C13( ) ;
         if ( RcdFound13 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars13( bcContratada, 0) ;
         nKeyPressed = 1;
         GetKey0C13( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0C13( ) ;
         }
         else
         {
            if ( RcdFound13 == 1 )
            {
               if ( A39Contratada_Codigo != Z39Contratada_Codigo )
               {
                  A39Contratada_Codigo = Z39Contratada_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0C13( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A39Contratada_Codigo != Z39Contratada_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0C13( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0C13( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow13( bcContratada) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars13( bcContratada, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0C13( ) ;
         if ( RcdFound13 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A39Contratada_Codigo != Z39Contratada_Codigo )
            {
               A39Contratada_Codigo = Z39Contratada_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A39Contratada_Codigo != Z39Contratada_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(12);
         pr_default.close(14);
         context.RollbackDataStores( "Contratada_BC");
         VarsToRow13( bcContratada) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratada.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratada.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratada )
         {
            bcContratada = (SdtContratada)(sdt);
            if ( StringUtil.StrCmp(bcContratada.gxTpr_Mode, "") == 0 )
            {
               bcContratada.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow13( bcContratada) ;
            }
            else
            {
               RowToVars13( bcContratada, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratada.gxTpr_Mode, "") == 0 )
            {
               bcContratada.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars13( bcContratada, 1) ;
         return  ;
      }

      public SdtContratada Contratada_BC
      {
         get {
            return bcContratada ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(12);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV39Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV35AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         Z438Contratada_Sigla = "";
         A438Contratada_Sigla = "";
         Z516Contratada_TipoFabrica = "";
         A516Contratada_TipoFabrica = "";
         Z342Contratada_BancoNome = "";
         A342Contratada_BancoNome = "";
         Z343Contratada_BancoNro = "";
         A343Contratada_BancoNro = "";
         Z344Contratada_AgenciaNome = "";
         A344Contratada_AgenciaNome = "";
         Z345Contratada_AgenciaNro = "";
         A345Contratada_AgenciaNro = "";
         Z51Contratada_ContaCorrente = "";
         A51Contratada_ContaCorrente = "";
         Z41Contratada_PessoaNom = "";
         A41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         A42Contratada_PessoaCNPJ = "";
         Z518Pessoa_IE = "";
         A518Pessoa_IE = "";
         Z519Pessoa_Endereco = "";
         A519Pessoa_Endereco = "";
         Z521Pessoa_CEP = "";
         A521Pessoa_CEP = "";
         Z522Pessoa_Telefone = "";
         A522Pessoa_Telefone = "";
         Z523Pessoa_Fax = "";
         A523Pessoa_Fax = "";
         Z53Contratada_AreaTrabalhoDes = "";
         A53Contratada_AreaTrabalhoDes = "";
         Z1592Contratada_AreaTrbClcPFnl = "";
         A1592Contratada_AreaTrbClcPFnl = "";
         Z350Contratada_UF = "";
         A350Contratada_UF = "";
         Z1127Contratada_LogoArquivo = "";
         A1127Contratada_LogoArquivo = "";
         Z1664Contratada_Logo = "";
         A1664Contratada_Logo = "";
         Z40000Contratada_Logo_GXI = "";
         A40000Contratada_Logo_GXI = "";
         Z1129Contratada_LogoTipoArq = "";
         A1129Contratada_LogoTipoArq = "";
         Z1128Contratada_LogoNomeArq = "";
         A1128Contratada_LogoNomeArq = "";
         BC000C5_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         BC000C5_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         BC000C5_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         BC000C5_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         BC000C5_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         BC000C5_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         BC000C7_A39Contratada_Codigo = new int[1] ;
         BC000C7_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         BC000C7_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         BC000C7_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         BC000C7_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         BC000C7_A41Contratada_PessoaNom = new String[] {""} ;
         BC000C7_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000C7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000C7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000C7_A518Pessoa_IE = new String[] {""} ;
         BC000C7_n518Pessoa_IE = new bool[] {false} ;
         BC000C7_A519Pessoa_Endereco = new String[] {""} ;
         BC000C7_n519Pessoa_Endereco = new bool[] {false} ;
         BC000C7_A521Pessoa_CEP = new String[] {""} ;
         BC000C7_n521Pessoa_CEP = new bool[] {false} ;
         BC000C7_A522Pessoa_Telefone = new String[] {""} ;
         BC000C7_n522Pessoa_Telefone = new bool[] {false} ;
         BC000C7_A523Pessoa_Fax = new String[] {""} ;
         BC000C7_n523Pessoa_Fax = new bool[] {false} ;
         BC000C7_A438Contratada_Sigla = new String[] {""} ;
         BC000C7_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000C7_A342Contratada_BancoNome = new String[] {""} ;
         BC000C7_n342Contratada_BancoNome = new bool[] {false} ;
         BC000C7_A343Contratada_BancoNro = new String[] {""} ;
         BC000C7_n343Contratada_BancoNro = new bool[] {false} ;
         BC000C7_A344Contratada_AgenciaNome = new String[] {""} ;
         BC000C7_n344Contratada_AgenciaNome = new bool[] {false} ;
         BC000C7_A345Contratada_AgenciaNro = new String[] {""} ;
         BC000C7_n345Contratada_AgenciaNro = new bool[] {false} ;
         BC000C7_A51Contratada_ContaCorrente = new String[] {""} ;
         BC000C7_n51Contratada_ContaCorrente = new bool[] {false} ;
         BC000C7_A524Contratada_OS = new int[1] ;
         BC000C7_n524Contratada_OS = new bool[] {false} ;
         BC000C7_A1451Contratada_SS = new int[1] ;
         BC000C7_n1451Contratada_SS = new bool[] {false} ;
         BC000C7_A530Contratada_Lote = new short[1] ;
         BC000C7_n530Contratada_Lote = new bool[] {false} ;
         BC000C7_A1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C7_n1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C7_A1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C7_n1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C7_A1953Contratada_CntPadrao = new int[1] ;
         BC000C7_n1953Contratada_CntPadrao = new bool[] {false} ;
         BC000C7_A43Contratada_Ativo = new bool[] {false} ;
         BC000C7_A40000Contratada_Logo_GXI = new String[] {""} ;
         BC000C7_n40000Contratada_Logo_GXI = new bool[] {false} ;
         BC000C7_A1129Contratada_LogoTipoArq = new String[] {""} ;
         BC000C7_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         BC000C7_A1128Contratada_LogoNomeArq = new String[] {""} ;
         BC000C7_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         BC000C7_A40Contratada_PessoaCod = new int[1] ;
         BC000C7_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC000C7_A349Contratada_MunicipioCod = new int[1] ;
         BC000C7_n349Contratada_MunicipioCod = new bool[] {false} ;
         BC000C7_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         BC000C7_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         BC000C7_A350Contratada_UF = new String[] {""} ;
         BC000C7_n350Contratada_UF = new bool[] {false} ;
         BC000C7_A1127Contratada_LogoArquivo = new String[] {""} ;
         BC000C7_n1127Contratada_LogoArquivo = new bool[] {false} ;
         BC000C7_A1664Contratada_Logo = new String[] {""} ;
         BC000C7_n1664Contratada_Logo = new bool[] {false} ;
         A1127Contratada_LogoArquivo_Filetype = "";
         A1127Contratada_LogoArquivo_Filename = "";
         BC000C4_A41Contratada_PessoaNom = new String[] {""} ;
         BC000C4_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000C4_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000C4_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000C4_A518Pessoa_IE = new String[] {""} ;
         BC000C4_n518Pessoa_IE = new bool[] {false} ;
         BC000C4_A519Pessoa_Endereco = new String[] {""} ;
         BC000C4_n519Pessoa_Endereco = new bool[] {false} ;
         BC000C4_A521Pessoa_CEP = new String[] {""} ;
         BC000C4_n521Pessoa_CEP = new bool[] {false} ;
         BC000C4_A522Pessoa_Telefone = new String[] {""} ;
         BC000C4_n522Pessoa_Telefone = new bool[] {false} ;
         BC000C4_A523Pessoa_Fax = new String[] {""} ;
         BC000C4_n523Pessoa_Fax = new bool[] {false} ;
         BC000C6_A350Contratada_UF = new String[] {""} ;
         BC000C6_n350Contratada_UF = new bool[] {false} ;
         BC000C8_A39Contratada_Codigo = new int[1] ;
         BC000C3_A39Contratada_Codigo = new int[1] ;
         BC000C3_A438Contratada_Sigla = new String[] {""} ;
         BC000C3_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000C3_A342Contratada_BancoNome = new String[] {""} ;
         BC000C3_n342Contratada_BancoNome = new bool[] {false} ;
         BC000C3_A343Contratada_BancoNro = new String[] {""} ;
         BC000C3_n343Contratada_BancoNro = new bool[] {false} ;
         BC000C3_A344Contratada_AgenciaNome = new String[] {""} ;
         BC000C3_n344Contratada_AgenciaNome = new bool[] {false} ;
         BC000C3_A345Contratada_AgenciaNro = new String[] {""} ;
         BC000C3_n345Contratada_AgenciaNro = new bool[] {false} ;
         BC000C3_A51Contratada_ContaCorrente = new String[] {""} ;
         BC000C3_n51Contratada_ContaCorrente = new bool[] {false} ;
         BC000C3_A524Contratada_OS = new int[1] ;
         BC000C3_n524Contratada_OS = new bool[] {false} ;
         BC000C3_A1451Contratada_SS = new int[1] ;
         BC000C3_n1451Contratada_SS = new bool[] {false} ;
         BC000C3_A530Contratada_Lote = new short[1] ;
         BC000C3_n530Contratada_Lote = new bool[] {false} ;
         BC000C3_A1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C3_n1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C3_A1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C3_n1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C3_A1953Contratada_CntPadrao = new int[1] ;
         BC000C3_n1953Contratada_CntPadrao = new bool[] {false} ;
         BC000C3_A43Contratada_Ativo = new bool[] {false} ;
         BC000C3_A40000Contratada_Logo_GXI = new String[] {""} ;
         BC000C3_n40000Contratada_Logo_GXI = new bool[] {false} ;
         BC000C3_A1129Contratada_LogoTipoArq = new String[] {""} ;
         BC000C3_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         BC000C3_A1128Contratada_LogoNomeArq = new String[] {""} ;
         BC000C3_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         BC000C3_A40Contratada_PessoaCod = new int[1] ;
         BC000C3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC000C3_A349Contratada_MunicipioCod = new int[1] ;
         BC000C3_n349Contratada_MunicipioCod = new bool[] {false} ;
         BC000C3_A1127Contratada_LogoArquivo = new String[] {""} ;
         BC000C3_n1127Contratada_LogoArquivo = new bool[] {false} ;
         BC000C3_A1664Contratada_Logo = new String[] {""} ;
         BC000C3_n1664Contratada_Logo = new bool[] {false} ;
         sMode13 = "";
         BC000C2_A39Contratada_Codigo = new int[1] ;
         BC000C2_A438Contratada_Sigla = new String[] {""} ;
         BC000C2_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000C2_A342Contratada_BancoNome = new String[] {""} ;
         BC000C2_n342Contratada_BancoNome = new bool[] {false} ;
         BC000C2_A343Contratada_BancoNro = new String[] {""} ;
         BC000C2_n343Contratada_BancoNro = new bool[] {false} ;
         BC000C2_A344Contratada_AgenciaNome = new String[] {""} ;
         BC000C2_n344Contratada_AgenciaNome = new bool[] {false} ;
         BC000C2_A345Contratada_AgenciaNro = new String[] {""} ;
         BC000C2_n345Contratada_AgenciaNro = new bool[] {false} ;
         BC000C2_A51Contratada_ContaCorrente = new String[] {""} ;
         BC000C2_n51Contratada_ContaCorrente = new bool[] {false} ;
         BC000C2_A524Contratada_OS = new int[1] ;
         BC000C2_n524Contratada_OS = new bool[] {false} ;
         BC000C2_A1451Contratada_SS = new int[1] ;
         BC000C2_n1451Contratada_SS = new bool[] {false} ;
         BC000C2_A530Contratada_Lote = new short[1] ;
         BC000C2_n530Contratada_Lote = new bool[] {false} ;
         BC000C2_A1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C2_n1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C2_A1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C2_n1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C2_A1953Contratada_CntPadrao = new int[1] ;
         BC000C2_n1953Contratada_CntPadrao = new bool[] {false} ;
         BC000C2_A43Contratada_Ativo = new bool[] {false} ;
         BC000C2_A40000Contratada_Logo_GXI = new String[] {""} ;
         BC000C2_n40000Contratada_Logo_GXI = new bool[] {false} ;
         BC000C2_A1129Contratada_LogoTipoArq = new String[] {""} ;
         BC000C2_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         BC000C2_A1128Contratada_LogoNomeArq = new String[] {""} ;
         BC000C2_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         BC000C2_A40Contratada_PessoaCod = new int[1] ;
         BC000C2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC000C2_A349Contratada_MunicipioCod = new int[1] ;
         BC000C2_n349Contratada_MunicipioCod = new bool[] {false} ;
         BC000C2_A1127Contratada_LogoArquivo = new String[] {""} ;
         BC000C2_n1127Contratada_LogoArquivo = new bool[] {false} ;
         BC000C2_A1664Contratada_Logo = new String[] {""} ;
         BC000C2_n1664Contratada_Logo = new bool[] {false} ;
         BC000C9_A39Contratada_Codigo = new int[1] ;
         BC000C14_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         BC000C14_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         BC000C14_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         BC000C14_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         BC000C14_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         BC000C14_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         BC000C15_A41Contratada_PessoaNom = new String[] {""} ;
         BC000C15_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000C15_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000C15_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000C15_A518Pessoa_IE = new String[] {""} ;
         BC000C15_n518Pessoa_IE = new bool[] {false} ;
         BC000C15_A519Pessoa_Endereco = new String[] {""} ;
         BC000C15_n519Pessoa_Endereco = new bool[] {false} ;
         BC000C15_A521Pessoa_CEP = new String[] {""} ;
         BC000C15_n521Pessoa_CEP = new bool[] {false} ;
         BC000C15_A522Pessoa_Telefone = new String[] {""} ;
         BC000C15_n522Pessoa_Telefone = new bool[] {false} ;
         BC000C15_A523Pessoa_Fax = new String[] {""} ;
         BC000C15_n523Pessoa_Fax = new bool[] {false} ;
         BC000C16_A350Contratada_UF = new String[] {""} ;
         BC000C16_n350Contratada_UF = new bool[] {false} ;
         BC000C17_A1653Tributo_Codigo = new int[1] ;
         BC000C18_A1482Gestao_Codigo = new int[1] ;
         BC000C19_A456ContagemResultado_Codigo = new int[1] ;
         BC000C20_A456ContagemResultado_Codigo = new int[1] ;
         BC000C21_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         BC000C21_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         BC000C22_A439Solicitacoes_Codigo = new int[1] ;
         BC000C23_A74Contrato_Codigo = new int[1] ;
         BC000C24_A39Contratada_Codigo = new int[1] ;
         BC000C24_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         BC000C24_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         BC000C24_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         BC000C24_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         BC000C24_A41Contratada_PessoaNom = new String[] {""} ;
         BC000C24_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000C24_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000C24_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000C24_A518Pessoa_IE = new String[] {""} ;
         BC000C24_n518Pessoa_IE = new bool[] {false} ;
         BC000C24_A519Pessoa_Endereco = new String[] {""} ;
         BC000C24_n519Pessoa_Endereco = new bool[] {false} ;
         BC000C24_A521Pessoa_CEP = new String[] {""} ;
         BC000C24_n521Pessoa_CEP = new bool[] {false} ;
         BC000C24_A522Pessoa_Telefone = new String[] {""} ;
         BC000C24_n522Pessoa_Telefone = new bool[] {false} ;
         BC000C24_A523Pessoa_Fax = new String[] {""} ;
         BC000C24_n523Pessoa_Fax = new bool[] {false} ;
         BC000C24_A438Contratada_Sigla = new String[] {""} ;
         BC000C24_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000C24_A342Contratada_BancoNome = new String[] {""} ;
         BC000C24_n342Contratada_BancoNome = new bool[] {false} ;
         BC000C24_A343Contratada_BancoNro = new String[] {""} ;
         BC000C24_n343Contratada_BancoNro = new bool[] {false} ;
         BC000C24_A344Contratada_AgenciaNome = new String[] {""} ;
         BC000C24_n344Contratada_AgenciaNome = new bool[] {false} ;
         BC000C24_A345Contratada_AgenciaNro = new String[] {""} ;
         BC000C24_n345Contratada_AgenciaNro = new bool[] {false} ;
         BC000C24_A51Contratada_ContaCorrente = new String[] {""} ;
         BC000C24_n51Contratada_ContaCorrente = new bool[] {false} ;
         BC000C24_A524Contratada_OS = new int[1] ;
         BC000C24_n524Contratada_OS = new bool[] {false} ;
         BC000C24_A1451Contratada_SS = new int[1] ;
         BC000C24_n1451Contratada_SS = new bool[] {false} ;
         BC000C24_A530Contratada_Lote = new short[1] ;
         BC000C24_n530Contratada_Lote = new bool[] {false} ;
         BC000C24_A1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C24_n1481Contratada_UsaOSistema = new bool[] {false} ;
         BC000C24_A1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C24_n1867Contratada_OSPreferencial = new bool[] {false} ;
         BC000C24_A1953Contratada_CntPadrao = new int[1] ;
         BC000C24_n1953Contratada_CntPadrao = new bool[] {false} ;
         BC000C24_A43Contratada_Ativo = new bool[] {false} ;
         BC000C24_A40000Contratada_Logo_GXI = new String[] {""} ;
         BC000C24_n40000Contratada_Logo_GXI = new bool[] {false} ;
         BC000C24_A1129Contratada_LogoTipoArq = new String[] {""} ;
         BC000C24_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         BC000C24_A1128Contratada_LogoNomeArq = new String[] {""} ;
         BC000C24_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         BC000C24_A40Contratada_PessoaCod = new int[1] ;
         BC000C24_A52Contratada_AreaTrabalhoCod = new int[1] ;
         BC000C24_A349Contratada_MunicipioCod = new int[1] ;
         BC000C24_n349Contratada_MunicipioCod = new bool[] {false} ;
         BC000C24_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         BC000C24_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         BC000C24_A350Contratada_UF = new String[] {""} ;
         BC000C24_n350Contratada_UF = new bool[] {false} ;
         BC000C24_A1127Contratada_LogoArquivo = new String[] {""} ;
         BC000C24_n1127Contratada_LogoArquivo = new bool[] {false} ;
         BC000C24_A1664Contratada_Logo = new String[] {""} ;
         BC000C24_n1664Contratada_Logo = new bool[] {false} ;
         i516Contratada_TipoFabrica = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratada_bc__default(),
            new Object[][] {
                new Object[] {
               BC000C2_A39Contratada_Codigo, BC000C2_A438Contratada_Sigla, BC000C2_A516Contratada_TipoFabrica, BC000C2_A342Contratada_BancoNome, BC000C2_n342Contratada_BancoNome, BC000C2_A343Contratada_BancoNro, BC000C2_n343Contratada_BancoNro, BC000C2_A344Contratada_AgenciaNome, BC000C2_n344Contratada_AgenciaNome, BC000C2_A345Contratada_AgenciaNro,
               BC000C2_n345Contratada_AgenciaNro, BC000C2_A51Contratada_ContaCorrente, BC000C2_n51Contratada_ContaCorrente, BC000C2_A524Contratada_OS, BC000C2_n524Contratada_OS, BC000C2_A1451Contratada_SS, BC000C2_n1451Contratada_SS, BC000C2_A530Contratada_Lote, BC000C2_n530Contratada_Lote, BC000C2_A1481Contratada_UsaOSistema,
               BC000C2_n1481Contratada_UsaOSistema, BC000C2_A1867Contratada_OSPreferencial, BC000C2_n1867Contratada_OSPreferencial, BC000C2_A1953Contratada_CntPadrao, BC000C2_n1953Contratada_CntPadrao, BC000C2_A43Contratada_Ativo, BC000C2_A40000Contratada_Logo_GXI, BC000C2_n40000Contratada_Logo_GXI, BC000C2_A1129Contratada_LogoTipoArq, BC000C2_n1129Contratada_LogoTipoArq,
               BC000C2_A1128Contratada_LogoNomeArq, BC000C2_n1128Contratada_LogoNomeArq, BC000C2_A40Contratada_PessoaCod, BC000C2_A52Contratada_AreaTrabalhoCod, BC000C2_A349Contratada_MunicipioCod, BC000C2_n349Contratada_MunicipioCod, BC000C2_A1127Contratada_LogoArquivo, BC000C2_n1127Contratada_LogoArquivo, BC000C2_A1664Contratada_Logo, BC000C2_n1664Contratada_Logo
               }
               , new Object[] {
               BC000C3_A39Contratada_Codigo, BC000C3_A438Contratada_Sigla, BC000C3_A516Contratada_TipoFabrica, BC000C3_A342Contratada_BancoNome, BC000C3_n342Contratada_BancoNome, BC000C3_A343Contratada_BancoNro, BC000C3_n343Contratada_BancoNro, BC000C3_A344Contratada_AgenciaNome, BC000C3_n344Contratada_AgenciaNome, BC000C3_A345Contratada_AgenciaNro,
               BC000C3_n345Contratada_AgenciaNro, BC000C3_A51Contratada_ContaCorrente, BC000C3_n51Contratada_ContaCorrente, BC000C3_A524Contratada_OS, BC000C3_n524Contratada_OS, BC000C3_A1451Contratada_SS, BC000C3_n1451Contratada_SS, BC000C3_A530Contratada_Lote, BC000C3_n530Contratada_Lote, BC000C3_A1481Contratada_UsaOSistema,
               BC000C3_n1481Contratada_UsaOSistema, BC000C3_A1867Contratada_OSPreferencial, BC000C3_n1867Contratada_OSPreferencial, BC000C3_A1953Contratada_CntPadrao, BC000C3_n1953Contratada_CntPadrao, BC000C3_A43Contratada_Ativo, BC000C3_A40000Contratada_Logo_GXI, BC000C3_n40000Contratada_Logo_GXI, BC000C3_A1129Contratada_LogoTipoArq, BC000C3_n1129Contratada_LogoTipoArq,
               BC000C3_A1128Contratada_LogoNomeArq, BC000C3_n1128Contratada_LogoNomeArq, BC000C3_A40Contratada_PessoaCod, BC000C3_A52Contratada_AreaTrabalhoCod, BC000C3_A349Contratada_MunicipioCod, BC000C3_n349Contratada_MunicipioCod, BC000C3_A1127Contratada_LogoArquivo, BC000C3_n1127Contratada_LogoArquivo, BC000C3_A1664Contratada_Logo, BC000C3_n1664Contratada_Logo
               }
               , new Object[] {
               BC000C4_A41Contratada_PessoaNom, BC000C4_n41Contratada_PessoaNom, BC000C4_A42Contratada_PessoaCNPJ, BC000C4_n42Contratada_PessoaCNPJ, BC000C4_A518Pessoa_IE, BC000C4_n518Pessoa_IE, BC000C4_A519Pessoa_Endereco, BC000C4_n519Pessoa_Endereco, BC000C4_A521Pessoa_CEP, BC000C4_n521Pessoa_CEP,
               BC000C4_A522Pessoa_Telefone, BC000C4_n522Pessoa_Telefone, BC000C4_A523Pessoa_Fax, BC000C4_n523Pessoa_Fax
               }
               , new Object[] {
               BC000C5_A53Contratada_AreaTrabalhoDes, BC000C5_n53Contratada_AreaTrabalhoDes, BC000C5_A1592Contratada_AreaTrbClcPFnl, BC000C5_n1592Contratada_AreaTrbClcPFnl, BC000C5_A1595Contratada_AreaTrbSrvPdr, BC000C5_n1595Contratada_AreaTrbSrvPdr
               }
               , new Object[] {
               BC000C6_A350Contratada_UF, BC000C6_n350Contratada_UF
               }
               , new Object[] {
               BC000C7_A39Contratada_Codigo, BC000C7_A53Contratada_AreaTrabalhoDes, BC000C7_n53Contratada_AreaTrabalhoDes, BC000C7_A1592Contratada_AreaTrbClcPFnl, BC000C7_n1592Contratada_AreaTrbClcPFnl, BC000C7_A41Contratada_PessoaNom, BC000C7_n41Contratada_PessoaNom, BC000C7_A42Contratada_PessoaCNPJ, BC000C7_n42Contratada_PessoaCNPJ, BC000C7_A518Pessoa_IE,
               BC000C7_n518Pessoa_IE, BC000C7_A519Pessoa_Endereco, BC000C7_n519Pessoa_Endereco, BC000C7_A521Pessoa_CEP, BC000C7_n521Pessoa_CEP, BC000C7_A522Pessoa_Telefone, BC000C7_n522Pessoa_Telefone, BC000C7_A523Pessoa_Fax, BC000C7_n523Pessoa_Fax, BC000C7_A438Contratada_Sigla,
               BC000C7_A516Contratada_TipoFabrica, BC000C7_A342Contratada_BancoNome, BC000C7_n342Contratada_BancoNome, BC000C7_A343Contratada_BancoNro, BC000C7_n343Contratada_BancoNro, BC000C7_A344Contratada_AgenciaNome, BC000C7_n344Contratada_AgenciaNome, BC000C7_A345Contratada_AgenciaNro, BC000C7_n345Contratada_AgenciaNro, BC000C7_A51Contratada_ContaCorrente,
               BC000C7_n51Contratada_ContaCorrente, BC000C7_A524Contratada_OS, BC000C7_n524Contratada_OS, BC000C7_A1451Contratada_SS, BC000C7_n1451Contratada_SS, BC000C7_A530Contratada_Lote, BC000C7_n530Contratada_Lote, BC000C7_A1481Contratada_UsaOSistema, BC000C7_n1481Contratada_UsaOSistema, BC000C7_A1867Contratada_OSPreferencial,
               BC000C7_n1867Contratada_OSPreferencial, BC000C7_A1953Contratada_CntPadrao, BC000C7_n1953Contratada_CntPadrao, BC000C7_A43Contratada_Ativo, BC000C7_A40000Contratada_Logo_GXI, BC000C7_n40000Contratada_Logo_GXI, BC000C7_A1129Contratada_LogoTipoArq, BC000C7_n1129Contratada_LogoTipoArq, BC000C7_A1128Contratada_LogoNomeArq, BC000C7_n1128Contratada_LogoNomeArq,
               BC000C7_A40Contratada_PessoaCod, BC000C7_A52Contratada_AreaTrabalhoCod, BC000C7_A349Contratada_MunicipioCod, BC000C7_n349Contratada_MunicipioCod, BC000C7_A1595Contratada_AreaTrbSrvPdr, BC000C7_n1595Contratada_AreaTrbSrvPdr, BC000C7_A350Contratada_UF, BC000C7_n350Contratada_UF, BC000C7_A1127Contratada_LogoArquivo, BC000C7_n1127Contratada_LogoArquivo,
               BC000C7_A1664Contratada_Logo, BC000C7_n1664Contratada_Logo
               }
               , new Object[] {
               BC000C8_A39Contratada_Codigo
               }
               , new Object[] {
               BC000C9_A39Contratada_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000C14_A53Contratada_AreaTrabalhoDes, BC000C14_n53Contratada_AreaTrabalhoDes, BC000C14_A1592Contratada_AreaTrbClcPFnl, BC000C14_n1592Contratada_AreaTrbClcPFnl, BC000C14_A1595Contratada_AreaTrbSrvPdr, BC000C14_n1595Contratada_AreaTrbSrvPdr
               }
               , new Object[] {
               BC000C15_A41Contratada_PessoaNom, BC000C15_n41Contratada_PessoaNom, BC000C15_A42Contratada_PessoaCNPJ, BC000C15_n42Contratada_PessoaCNPJ, BC000C15_A518Pessoa_IE, BC000C15_n518Pessoa_IE, BC000C15_A519Pessoa_Endereco, BC000C15_n519Pessoa_Endereco, BC000C15_A521Pessoa_CEP, BC000C15_n521Pessoa_CEP,
               BC000C15_A522Pessoa_Telefone, BC000C15_n522Pessoa_Telefone, BC000C15_A523Pessoa_Fax, BC000C15_n523Pessoa_Fax
               }
               , new Object[] {
               BC000C16_A350Contratada_UF, BC000C16_n350Contratada_UF
               }
               , new Object[] {
               BC000C17_A1653Tributo_Codigo
               }
               , new Object[] {
               BC000C18_A1482Gestao_Codigo
               }
               , new Object[] {
               BC000C19_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000C20_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000C21_A66ContratadaUsuario_ContratadaCod, BC000C21_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               BC000C22_A439Solicitacoes_Codigo
               }
               , new Object[] {
               BC000C23_A74Contrato_Codigo
               }
               , new Object[] {
               BC000C24_A39Contratada_Codigo, BC000C24_A53Contratada_AreaTrabalhoDes, BC000C24_n53Contratada_AreaTrabalhoDes, BC000C24_A1592Contratada_AreaTrbClcPFnl, BC000C24_n1592Contratada_AreaTrbClcPFnl, BC000C24_A41Contratada_PessoaNom, BC000C24_n41Contratada_PessoaNom, BC000C24_A42Contratada_PessoaCNPJ, BC000C24_n42Contratada_PessoaCNPJ, BC000C24_A518Pessoa_IE,
               BC000C24_n518Pessoa_IE, BC000C24_A519Pessoa_Endereco, BC000C24_n519Pessoa_Endereco, BC000C24_A521Pessoa_CEP, BC000C24_n521Pessoa_CEP, BC000C24_A522Pessoa_Telefone, BC000C24_n522Pessoa_Telefone, BC000C24_A523Pessoa_Fax, BC000C24_n523Pessoa_Fax, BC000C24_A438Contratada_Sigla,
               BC000C24_A516Contratada_TipoFabrica, BC000C24_A342Contratada_BancoNome, BC000C24_n342Contratada_BancoNome, BC000C24_A343Contratada_BancoNro, BC000C24_n343Contratada_BancoNro, BC000C24_A344Contratada_AgenciaNome, BC000C24_n344Contratada_AgenciaNome, BC000C24_A345Contratada_AgenciaNro, BC000C24_n345Contratada_AgenciaNro, BC000C24_A51Contratada_ContaCorrente,
               BC000C24_n51Contratada_ContaCorrente, BC000C24_A524Contratada_OS, BC000C24_n524Contratada_OS, BC000C24_A1451Contratada_SS, BC000C24_n1451Contratada_SS, BC000C24_A530Contratada_Lote, BC000C24_n530Contratada_Lote, BC000C24_A1481Contratada_UsaOSistema, BC000C24_n1481Contratada_UsaOSistema, BC000C24_A1867Contratada_OSPreferencial,
               BC000C24_n1867Contratada_OSPreferencial, BC000C24_A1953Contratada_CntPadrao, BC000C24_n1953Contratada_CntPadrao, BC000C24_A43Contratada_Ativo, BC000C24_A40000Contratada_Logo_GXI, BC000C24_n40000Contratada_Logo_GXI, BC000C24_A1129Contratada_LogoTipoArq, BC000C24_n1129Contratada_LogoTipoArq, BC000C24_A1128Contratada_LogoNomeArq, BC000C24_n1128Contratada_LogoNomeArq,
               BC000C24_A40Contratada_PessoaCod, BC000C24_A52Contratada_AreaTrabalhoCod, BC000C24_A349Contratada_MunicipioCod, BC000C24_n349Contratada_MunicipioCod, BC000C24_A1595Contratada_AreaTrbSrvPdr, BC000C24_n1595Contratada_AreaTrbSrvPdr, BC000C24_A350Contratada_UF, BC000C24_n350Contratada_UF, BC000C24_A1127Contratada_LogoArquivo, BC000C24_n1127Contratada_LogoArquivo,
               BC000C24_A1664Contratada_Logo, BC000C24_n1664Contratada_Logo
               }
            }
         );
         Z43Contratada_Ativo = true;
         A43Contratada_Ativo = true;
         i43Contratada_Ativo = true;
         AV39Pgmname = "Contratada_BC";
         Z516Contratada_TipoFabrica = "";
         A516Contratada_TipoFabrica = "";
         i516Contratada_TipoFabrica = "";
         Z52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i52Contratada_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120C2 */
         E120C2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z530Contratada_Lote ;
      private short A530Contratada_Lote ;
      private short Gx_BScreen ;
      private short RcdFound13 ;
      private int trnEnded ;
      private int Z39Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int AV40GXV1 ;
      private int AV14Insert_Contratada_AreaTrabalhoCod ;
      private int AV11Insert_Contratada_PessoaCod ;
      private int AV26Insert_Contratada_MunicipioCod ;
      private int AV23Contratada_PessoaCod ;
      private int Z524Contratada_OS ;
      private int A524Contratada_OS ;
      private int Z1451Contratada_SS ;
      private int A1451Contratada_SS ;
      private int Z1953Contratada_CntPadrao ;
      private int A1953Contratada_CntPadrao ;
      private int Z40Contratada_PessoaCod ;
      private int A40Contratada_PessoaCod ;
      private int Z52Contratada_AreaTrabalhoCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int Z349Contratada_MunicipioCod ;
      private int A349Contratada_MunicipioCod ;
      private int Z34Pessoa_Codigo ;
      private int A34Pessoa_Codigo ;
      private int Z1595Contratada_AreaTrbSrvPdr ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int i52Contratada_AreaTrabalhoCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV39Pgmname ;
      private String Z438Contratada_Sigla ;
      private String A438Contratada_Sigla ;
      private String Z516Contratada_TipoFabrica ;
      private String A516Contratada_TipoFabrica ;
      private String Z342Contratada_BancoNome ;
      private String A342Contratada_BancoNome ;
      private String Z343Contratada_BancoNro ;
      private String A343Contratada_BancoNro ;
      private String Z344Contratada_AgenciaNome ;
      private String A344Contratada_AgenciaNome ;
      private String Z345Contratada_AgenciaNro ;
      private String A345Contratada_AgenciaNro ;
      private String Z51Contratada_ContaCorrente ;
      private String A51Contratada_ContaCorrente ;
      private String Z41Contratada_PessoaNom ;
      private String A41Contratada_PessoaNom ;
      private String Z518Pessoa_IE ;
      private String A518Pessoa_IE ;
      private String Z521Pessoa_CEP ;
      private String A521Pessoa_CEP ;
      private String Z522Pessoa_Telefone ;
      private String A522Pessoa_Telefone ;
      private String Z523Pessoa_Fax ;
      private String A523Pessoa_Fax ;
      private String Z1592Contratada_AreaTrbClcPFnl ;
      private String A1592Contratada_AreaTrbClcPFnl ;
      private String Z350Contratada_UF ;
      private String A350Contratada_UF ;
      private String Z1129Contratada_LogoTipoArq ;
      private String A1129Contratada_LogoTipoArq ;
      private String Z1128Contratada_LogoNomeArq ;
      private String A1128Contratada_LogoNomeArq ;
      private String A1127Contratada_LogoArquivo_Filetype ;
      private String A1127Contratada_LogoArquivo_Filename ;
      private String sMode13 ;
      private String i516Contratada_TipoFabrica ;
      private bool returnInSub ;
      private bool Z1481Contratada_UsaOSistema ;
      private bool A1481Contratada_UsaOSistema ;
      private bool Z1867Contratada_OSPreferencial ;
      private bool A1867Contratada_OSPreferencial ;
      private bool Z43Contratada_Ativo ;
      private bool A43Contratada_Ativo ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n1592Contratada_AreaTrbClcPFnl ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n342Contratada_BancoNome ;
      private bool n343Contratada_BancoNro ;
      private bool n344Contratada_AgenciaNome ;
      private bool n345Contratada_AgenciaNro ;
      private bool n51Contratada_ContaCorrente ;
      private bool n524Contratada_OS ;
      private bool n1451Contratada_SS ;
      private bool n530Contratada_Lote ;
      private bool n1481Contratada_UsaOSistema ;
      private bool n1867Contratada_OSPreferencial ;
      private bool n1953Contratada_CntPadrao ;
      private bool n40000Contratada_Logo_GXI ;
      private bool n1129Contratada_LogoTipoArq ;
      private bool n1128Contratada_LogoNomeArq ;
      private bool n349Contratada_MunicipioCod ;
      private bool n350Contratada_UF ;
      private bool n1127Contratada_LogoArquivo ;
      private bool n1664Contratada_Logo ;
      private bool Gx_longc ;
      private bool i43Contratada_Ativo ;
      private String Z42Contratada_PessoaCNPJ ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z519Pessoa_Endereco ;
      private String A519Pessoa_Endereco ;
      private String Z53Contratada_AreaTrabalhoDes ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String Z40000Contratada_Logo_GXI ;
      private String A40000Contratada_Logo_GXI ;
      private String Z1664Contratada_Logo ;
      private String A1664Contratada_Logo ;
      private String Z1127Contratada_LogoArquivo ;
      private String A1127Contratada_LogoArquivo ;
      private IGxSession AV10WebSession ;
      private SdtContratada bcContratada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC000C5_A53Contratada_AreaTrabalhoDes ;
      private bool[] BC000C5_n53Contratada_AreaTrabalhoDes ;
      private String[] BC000C5_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] BC000C5_n1592Contratada_AreaTrbClcPFnl ;
      private int[] BC000C5_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] BC000C5_n1595Contratada_AreaTrbSrvPdr ;
      private int[] BC000C7_A39Contratada_Codigo ;
      private String[] BC000C7_A53Contratada_AreaTrabalhoDes ;
      private bool[] BC000C7_n53Contratada_AreaTrabalhoDes ;
      private String[] BC000C7_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] BC000C7_n1592Contratada_AreaTrbClcPFnl ;
      private String[] BC000C7_A41Contratada_PessoaNom ;
      private bool[] BC000C7_n41Contratada_PessoaNom ;
      private String[] BC000C7_A42Contratada_PessoaCNPJ ;
      private bool[] BC000C7_n42Contratada_PessoaCNPJ ;
      private String[] BC000C7_A518Pessoa_IE ;
      private bool[] BC000C7_n518Pessoa_IE ;
      private String[] BC000C7_A519Pessoa_Endereco ;
      private bool[] BC000C7_n519Pessoa_Endereco ;
      private String[] BC000C7_A521Pessoa_CEP ;
      private bool[] BC000C7_n521Pessoa_CEP ;
      private String[] BC000C7_A522Pessoa_Telefone ;
      private bool[] BC000C7_n522Pessoa_Telefone ;
      private String[] BC000C7_A523Pessoa_Fax ;
      private bool[] BC000C7_n523Pessoa_Fax ;
      private String[] BC000C7_A438Contratada_Sigla ;
      private String[] BC000C7_A516Contratada_TipoFabrica ;
      private String[] BC000C7_A342Contratada_BancoNome ;
      private bool[] BC000C7_n342Contratada_BancoNome ;
      private String[] BC000C7_A343Contratada_BancoNro ;
      private bool[] BC000C7_n343Contratada_BancoNro ;
      private String[] BC000C7_A344Contratada_AgenciaNome ;
      private bool[] BC000C7_n344Contratada_AgenciaNome ;
      private String[] BC000C7_A345Contratada_AgenciaNro ;
      private bool[] BC000C7_n345Contratada_AgenciaNro ;
      private String[] BC000C7_A51Contratada_ContaCorrente ;
      private bool[] BC000C7_n51Contratada_ContaCorrente ;
      private int[] BC000C7_A524Contratada_OS ;
      private bool[] BC000C7_n524Contratada_OS ;
      private int[] BC000C7_A1451Contratada_SS ;
      private bool[] BC000C7_n1451Contratada_SS ;
      private short[] BC000C7_A530Contratada_Lote ;
      private bool[] BC000C7_n530Contratada_Lote ;
      private bool[] BC000C7_A1481Contratada_UsaOSistema ;
      private bool[] BC000C7_n1481Contratada_UsaOSistema ;
      private bool[] BC000C7_A1867Contratada_OSPreferencial ;
      private bool[] BC000C7_n1867Contratada_OSPreferencial ;
      private int[] BC000C7_A1953Contratada_CntPadrao ;
      private bool[] BC000C7_n1953Contratada_CntPadrao ;
      private bool[] BC000C7_A43Contratada_Ativo ;
      private String[] BC000C7_A40000Contratada_Logo_GXI ;
      private bool[] BC000C7_n40000Contratada_Logo_GXI ;
      private String[] BC000C7_A1129Contratada_LogoTipoArq ;
      private bool[] BC000C7_n1129Contratada_LogoTipoArq ;
      private String[] BC000C7_A1128Contratada_LogoNomeArq ;
      private bool[] BC000C7_n1128Contratada_LogoNomeArq ;
      private int[] BC000C7_A40Contratada_PessoaCod ;
      private int[] BC000C7_A52Contratada_AreaTrabalhoCod ;
      private int[] BC000C7_A349Contratada_MunicipioCod ;
      private bool[] BC000C7_n349Contratada_MunicipioCod ;
      private int[] BC000C7_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] BC000C7_n1595Contratada_AreaTrbSrvPdr ;
      private String[] BC000C7_A350Contratada_UF ;
      private bool[] BC000C7_n350Contratada_UF ;
      private String[] BC000C7_A1127Contratada_LogoArquivo ;
      private bool[] BC000C7_n1127Contratada_LogoArquivo ;
      private String[] BC000C7_A1664Contratada_Logo ;
      private bool[] BC000C7_n1664Contratada_Logo ;
      private String[] BC000C4_A41Contratada_PessoaNom ;
      private bool[] BC000C4_n41Contratada_PessoaNom ;
      private String[] BC000C4_A42Contratada_PessoaCNPJ ;
      private bool[] BC000C4_n42Contratada_PessoaCNPJ ;
      private String[] BC000C4_A518Pessoa_IE ;
      private bool[] BC000C4_n518Pessoa_IE ;
      private String[] BC000C4_A519Pessoa_Endereco ;
      private bool[] BC000C4_n519Pessoa_Endereco ;
      private String[] BC000C4_A521Pessoa_CEP ;
      private bool[] BC000C4_n521Pessoa_CEP ;
      private String[] BC000C4_A522Pessoa_Telefone ;
      private bool[] BC000C4_n522Pessoa_Telefone ;
      private String[] BC000C4_A523Pessoa_Fax ;
      private bool[] BC000C4_n523Pessoa_Fax ;
      private String[] BC000C6_A350Contratada_UF ;
      private bool[] BC000C6_n350Contratada_UF ;
      private int[] BC000C8_A39Contratada_Codigo ;
      private int[] BC000C3_A39Contratada_Codigo ;
      private String[] BC000C3_A438Contratada_Sigla ;
      private String[] BC000C3_A516Contratada_TipoFabrica ;
      private String[] BC000C3_A342Contratada_BancoNome ;
      private bool[] BC000C3_n342Contratada_BancoNome ;
      private String[] BC000C3_A343Contratada_BancoNro ;
      private bool[] BC000C3_n343Contratada_BancoNro ;
      private String[] BC000C3_A344Contratada_AgenciaNome ;
      private bool[] BC000C3_n344Contratada_AgenciaNome ;
      private String[] BC000C3_A345Contratada_AgenciaNro ;
      private bool[] BC000C3_n345Contratada_AgenciaNro ;
      private String[] BC000C3_A51Contratada_ContaCorrente ;
      private bool[] BC000C3_n51Contratada_ContaCorrente ;
      private int[] BC000C3_A524Contratada_OS ;
      private bool[] BC000C3_n524Contratada_OS ;
      private int[] BC000C3_A1451Contratada_SS ;
      private bool[] BC000C3_n1451Contratada_SS ;
      private short[] BC000C3_A530Contratada_Lote ;
      private bool[] BC000C3_n530Contratada_Lote ;
      private bool[] BC000C3_A1481Contratada_UsaOSistema ;
      private bool[] BC000C3_n1481Contratada_UsaOSistema ;
      private bool[] BC000C3_A1867Contratada_OSPreferencial ;
      private bool[] BC000C3_n1867Contratada_OSPreferencial ;
      private int[] BC000C3_A1953Contratada_CntPadrao ;
      private bool[] BC000C3_n1953Contratada_CntPadrao ;
      private bool[] BC000C3_A43Contratada_Ativo ;
      private String[] BC000C3_A40000Contratada_Logo_GXI ;
      private bool[] BC000C3_n40000Contratada_Logo_GXI ;
      private String[] BC000C3_A1129Contratada_LogoTipoArq ;
      private bool[] BC000C3_n1129Contratada_LogoTipoArq ;
      private String[] BC000C3_A1128Contratada_LogoNomeArq ;
      private bool[] BC000C3_n1128Contratada_LogoNomeArq ;
      private int[] BC000C3_A40Contratada_PessoaCod ;
      private int[] BC000C3_A52Contratada_AreaTrabalhoCod ;
      private int[] BC000C3_A349Contratada_MunicipioCod ;
      private bool[] BC000C3_n349Contratada_MunicipioCod ;
      private String[] BC000C3_A1127Contratada_LogoArquivo ;
      private bool[] BC000C3_n1127Contratada_LogoArquivo ;
      private String[] BC000C3_A1664Contratada_Logo ;
      private bool[] BC000C3_n1664Contratada_Logo ;
      private int[] BC000C2_A39Contratada_Codigo ;
      private String[] BC000C2_A438Contratada_Sigla ;
      private String[] BC000C2_A516Contratada_TipoFabrica ;
      private String[] BC000C2_A342Contratada_BancoNome ;
      private bool[] BC000C2_n342Contratada_BancoNome ;
      private String[] BC000C2_A343Contratada_BancoNro ;
      private bool[] BC000C2_n343Contratada_BancoNro ;
      private String[] BC000C2_A344Contratada_AgenciaNome ;
      private bool[] BC000C2_n344Contratada_AgenciaNome ;
      private String[] BC000C2_A345Contratada_AgenciaNro ;
      private bool[] BC000C2_n345Contratada_AgenciaNro ;
      private String[] BC000C2_A51Contratada_ContaCorrente ;
      private bool[] BC000C2_n51Contratada_ContaCorrente ;
      private int[] BC000C2_A524Contratada_OS ;
      private bool[] BC000C2_n524Contratada_OS ;
      private int[] BC000C2_A1451Contratada_SS ;
      private bool[] BC000C2_n1451Contratada_SS ;
      private short[] BC000C2_A530Contratada_Lote ;
      private bool[] BC000C2_n530Contratada_Lote ;
      private bool[] BC000C2_A1481Contratada_UsaOSistema ;
      private bool[] BC000C2_n1481Contratada_UsaOSistema ;
      private bool[] BC000C2_A1867Contratada_OSPreferencial ;
      private bool[] BC000C2_n1867Contratada_OSPreferencial ;
      private int[] BC000C2_A1953Contratada_CntPadrao ;
      private bool[] BC000C2_n1953Contratada_CntPadrao ;
      private bool[] BC000C2_A43Contratada_Ativo ;
      private String[] BC000C2_A40000Contratada_Logo_GXI ;
      private bool[] BC000C2_n40000Contratada_Logo_GXI ;
      private String[] BC000C2_A1129Contratada_LogoTipoArq ;
      private bool[] BC000C2_n1129Contratada_LogoTipoArq ;
      private String[] BC000C2_A1128Contratada_LogoNomeArq ;
      private bool[] BC000C2_n1128Contratada_LogoNomeArq ;
      private int[] BC000C2_A40Contratada_PessoaCod ;
      private int[] BC000C2_A52Contratada_AreaTrabalhoCod ;
      private int[] BC000C2_A349Contratada_MunicipioCod ;
      private bool[] BC000C2_n349Contratada_MunicipioCod ;
      private String[] BC000C2_A1127Contratada_LogoArquivo ;
      private bool[] BC000C2_n1127Contratada_LogoArquivo ;
      private String[] BC000C2_A1664Contratada_Logo ;
      private bool[] BC000C2_n1664Contratada_Logo ;
      private int[] BC000C9_A39Contratada_Codigo ;
      private String[] BC000C14_A53Contratada_AreaTrabalhoDes ;
      private bool[] BC000C14_n53Contratada_AreaTrabalhoDes ;
      private String[] BC000C14_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] BC000C14_n1592Contratada_AreaTrbClcPFnl ;
      private int[] BC000C14_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] BC000C14_n1595Contratada_AreaTrbSrvPdr ;
      private String[] BC000C15_A41Contratada_PessoaNom ;
      private bool[] BC000C15_n41Contratada_PessoaNom ;
      private String[] BC000C15_A42Contratada_PessoaCNPJ ;
      private bool[] BC000C15_n42Contratada_PessoaCNPJ ;
      private String[] BC000C15_A518Pessoa_IE ;
      private bool[] BC000C15_n518Pessoa_IE ;
      private String[] BC000C15_A519Pessoa_Endereco ;
      private bool[] BC000C15_n519Pessoa_Endereco ;
      private String[] BC000C15_A521Pessoa_CEP ;
      private bool[] BC000C15_n521Pessoa_CEP ;
      private String[] BC000C15_A522Pessoa_Telefone ;
      private bool[] BC000C15_n522Pessoa_Telefone ;
      private String[] BC000C15_A523Pessoa_Fax ;
      private bool[] BC000C15_n523Pessoa_Fax ;
      private String[] BC000C16_A350Contratada_UF ;
      private bool[] BC000C16_n350Contratada_UF ;
      private int[] BC000C17_A1653Tributo_Codigo ;
      private int[] BC000C18_A1482Gestao_Codigo ;
      private int[] BC000C19_A456ContagemResultado_Codigo ;
      private int[] BC000C20_A456ContagemResultado_Codigo ;
      private int[] BC000C21_A66ContratadaUsuario_ContratadaCod ;
      private int[] BC000C21_A69ContratadaUsuario_UsuarioCod ;
      private int[] BC000C22_A439Solicitacoes_Codigo ;
      private int[] BC000C23_A74Contrato_Codigo ;
      private int[] BC000C24_A39Contratada_Codigo ;
      private String[] BC000C24_A53Contratada_AreaTrabalhoDes ;
      private bool[] BC000C24_n53Contratada_AreaTrabalhoDes ;
      private String[] BC000C24_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] BC000C24_n1592Contratada_AreaTrbClcPFnl ;
      private String[] BC000C24_A41Contratada_PessoaNom ;
      private bool[] BC000C24_n41Contratada_PessoaNom ;
      private String[] BC000C24_A42Contratada_PessoaCNPJ ;
      private bool[] BC000C24_n42Contratada_PessoaCNPJ ;
      private String[] BC000C24_A518Pessoa_IE ;
      private bool[] BC000C24_n518Pessoa_IE ;
      private String[] BC000C24_A519Pessoa_Endereco ;
      private bool[] BC000C24_n519Pessoa_Endereco ;
      private String[] BC000C24_A521Pessoa_CEP ;
      private bool[] BC000C24_n521Pessoa_CEP ;
      private String[] BC000C24_A522Pessoa_Telefone ;
      private bool[] BC000C24_n522Pessoa_Telefone ;
      private String[] BC000C24_A523Pessoa_Fax ;
      private bool[] BC000C24_n523Pessoa_Fax ;
      private String[] BC000C24_A438Contratada_Sigla ;
      private String[] BC000C24_A516Contratada_TipoFabrica ;
      private String[] BC000C24_A342Contratada_BancoNome ;
      private bool[] BC000C24_n342Contratada_BancoNome ;
      private String[] BC000C24_A343Contratada_BancoNro ;
      private bool[] BC000C24_n343Contratada_BancoNro ;
      private String[] BC000C24_A344Contratada_AgenciaNome ;
      private bool[] BC000C24_n344Contratada_AgenciaNome ;
      private String[] BC000C24_A345Contratada_AgenciaNro ;
      private bool[] BC000C24_n345Contratada_AgenciaNro ;
      private String[] BC000C24_A51Contratada_ContaCorrente ;
      private bool[] BC000C24_n51Contratada_ContaCorrente ;
      private int[] BC000C24_A524Contratada_OS ;
      private bool[] BC000C24_n524Contratada_OS ;
      private int[] BC000C24_A1451Contratada_SS ;
      private bool[] BC000C24_n1451Contratada_SS ;
      private short[] BC000C24_A530Contratada_Lote ;
      private bool[] BC000C24_n530Contratada_Lote ;
      private bool[] BC000C24_A1481Contratada_UsaOSistema ;
      private bool[] BC000C24_n1481Contratada_UsaOSistema ;
      private bool[] BC000C24_A1867Contratada_OSPreferencial ;
      private bool[] BC000C24_n1867Contratada_OSPreferencial ;
      private int[] BC000C24_A1953Contratada_CntPadrao ;
      private bool[] BC000C24_n1953Contratada_CntPadrao ;
      private bool[] BC000C24_A43Contratada_Ativo ;
      private String[] BC000C24_A40000Contratada_Logo_GXI ;
      private bool[] BC000C24_n40000Contratada_Logo_GXI ;
      private String[] BC000C24_A1129Contratada_LogoTipoArq ;
      private bool[] BC000C24_n1129Contratada_LogoTipoArq ;
      private String[] BC000C24_A1128Contratada_LogoNomeArq ;
      private bool[] BC000C24_n1128Contratada_LogoNomeArq ;
      private int[] BC000C24_A40Contratada_PessoaCod ;
      private int[] BC000C24_A52Contratada_AreaTrabalhoCod ;
      private int[] BC000C24_A349Contratada_MunicipioCod ;
      private bool[] BC000C24_n349Contratada_MunicipioCod ;
      private int[] BC000C24_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] BC000C24_n1595Contratada_AreaTrbSrvPdr ;
      private String[] BC000C24_A350Contratada_UF ;
      private bool[] BC000C24_n350Contratada_UF ;
      private String[] BC000C24_A1127Contratada_LogoArquivo ;
      private bool[] BC000C24_n1127Contratada_LogoArquivo ;
      private String[] BC000C24_A1664Contratada_Logo ;
      private bool[] BC000C24_n1664Contratada_Logo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtAuditingObject AV35AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratada_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000C7 ;
          prmBC000C7 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C5 ;
          prmBC000C5 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C4 ;
          prmBC000C4 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C6 ;
          prmBC000C6 = new Object[] {
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C8 ;
          prmBC000C8 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C3 ;
          prmBC000C3 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C2 ;
          prmBC000C2 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C9 ;
          prmBC000C9 = new Object[] {
          new Object[] {"@Contratada_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratada_TipoFabrica",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratada_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratada_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_OSPreferencial",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_CntPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_Logo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_Logo_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Contratada_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C10 ;
          prmBC000C10 = new Object[] {
          new Object[] {"@Contratada_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Contratada_TipoFabrica",SqlDbType.Char,1,0} ,
          new Object[] {"@Contratada_BancoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_BancoNro",SqlDbType.Char,6,0} ,
          new Object[] {"@Contratada_AgenciaNome",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_AgenciaNro",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_ContaCorrente",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_UsaOSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_OSPreferencial",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_CntPadrao",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contratada_LogoTipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Contratada_LogoNomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C11 ;
          prmBC000C11 = new Object[] {
          new Object[] {"@Contratada_LogoArquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C12 ;
          prmBC000C12 = new Object[] {
          new Object[] {"@Contratada_Logo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Contratada_Logo_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C13 ;
          prmBC000C13 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C14 ;
          prmBC000C14 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C15 ;
          prmBC000C15 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C16 ;
          prmBC000C16 = new Object[] {
          new Object[] {"@Contratada_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C17 ;
          prmBC000C17 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C18 ;
          prmBC000C18 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C19 ;
          prmBC000C19 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C20 ;
          prmBC000C20 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C21 ;
          prmBC000C21 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C22 ;
          prmBC000C22 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C23 ;
          prmBC000C23 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000C24 ;
          prmBC000C24 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000C2", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_BancoNome], [Contratada_BancoNro], [Contratada_AgenciaNome], [Contratada_AgenciaNro], [Contratada_ContaCorrente], [Contratada_OS], [Contratada_SS], [Contratada_Lote], [Contratada_UsaOSistema], [Contratada_OSPreferencial], [Contratada_CntPadrao], [Contratada_Ativo], [Contratada_Logo_GXI], [Contratada_LogoTipoArq], [Contratada_LogoNomeArq], [Contratada_PessoaCod] AS Contratada_PessoaCod, [Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, [Contratada_MunicipioCod] AS Contratada_MunicipioCod, [Contratada_LogoArquivo], [Contratada_Logo] FROM [Contratada] WITH (UPDLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C2,1,0,true,false )
             ,new CursorDef("BC000C3", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_BancoNome], [Contratada_BancoNro], [Contratada_AgenciaNome], [Contratada_AgenciaNro], [Contratada_ContaCorrente], [Contratada_OS], [Contratada_SS], [Contratada_Lote], [Contratada_UsaOSistema], [Contratada_OSPreferencial], [Contratada_CntPadrao], [Contratada_Ativo], [Contratada_Logo_GXI], [Contratada_LogoTipoArq], [Contratada_LogoNomeArq], [Contratada_PessoaCod] AS Contratada_PessoaCod, [Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, [Contratada_MunicipioCod] AS Contratada_MunicipioCod, [Contratada_LogoArquivo], [Contratada_Logo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C3,1,0,true,false )
             ,new CursorDef("BC000C4", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C4,1,0,true,false )
             ,new CursorDef("BC000C5", "SELECT [AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, [AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, [AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C5,1,0,true,false )
             ,new CursorDef("BC000C6", "SELECT [Estado_UF] AS Contratada_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Contratada_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C6,1,0,true,false )
             ,new CursorDef("BC000C7", "SELECT TM1.[Contratada_Codigo], T2.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T2.[AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, T3.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Pessoa_IE], T3.[Pessoa_Endereco], T3.[Pessoa_CEP], T3.[Pessoa_Telefone], T3.[Pessoa_Fax], TM1.[Contratada_Sigla], TM1.[Contratada_TipoFabrica], TM1.[Contratada_BancoNome], TM1.[Contratada_BancoNro], TM1.[Contratada_AgenciaNome], TM1.[Contratada_AgenciaNro], TM1.[Contratada_ContaCorrente], TM1.[Contratada_OS], TM1.[Contratada_SS], TM1.[Contratada_Lote], TM1.[Contratada_UsaOSistema], TM1.[Contratada_OSPreferencial], TM1.[Contratada_CntPadrao], TM1.[Contratada_Ativo], TM1.[Contratada_Logo_GXI], TM1.[Contratada_LogoTipoArq], TM1.[Contratada_LogoNomeArq], TM1.[Contratada_PessoaCod] AS Contratada_PessoaCod, TM1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, TM1.[Contratada_MunicipioCod] AS Contratada_MunicipioCod, T2.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T4.[Estado_UF] AS Contratada_UF, TM1.[Contratada_LogoArquivo], TM1.[Contratada_Logo] FROM ((([Contratada] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Contratada_AreaTrabalhoCod]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = TM1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T4 WITH (NOLOCK) ON T4.[Municipio_Codigo] = TM1.[Contratada_MunicipioCod]) WHERE TM1.[Contratada_Codigo] = @Contratada_Codigo ORDER BY TM1.[Contratada_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C7,100,0,true,false )
             ,new CursorDef("BC000C8", "SELECT [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C8,1,0,true,false )
             ,new CursorDef("BC000C9", "INSERT INTO [Contratada]([Contratada_Sigla], [Contratada_TipoFabrica], [Contratada_BancoNome], [Contratada_BancoNro], [Contratada_AgenciaNome], [Contratada_AgenciaNro], [Contratada_ContaCorrente], [Contratada_OS], [Contratada_SS], [Contratada_Lote], [Contratada_LogoArquivo], [Contratada_UsaOSistema], [Contratada_OSPreferencial], [Contratada_CntPadrao], [Contratada_Ativo], [Contratada_Logo], [Contratada_Logo_GXI], [Contratada_LogoTipoArq], [Contratada_LogoNomeArq], [Contratada_PessoaCod], [Contratada_AreaTrabalhoCod], [Contratada_MunicipioCod]) VALUES(@Contratada_Sigla, @Contratada_TipoFabrica, @Contratada_BancoNome, @Contratada_BancoNro, @Contratada_AgenciaNome, @Contratada_AgenciaNro, @Contratada_ContaCorrente, @Contratada_OS, @Contratada_SS, @Contratada_Lote, @Contratada_LogoArquivo, @Contratada_UsaOSistema, @Contratada_OSPreferencial, @Contratada_CntPadrao, @Contratada_Ativo, @Contratada_Logo, @Contratada_Logo_GXI, @Contratada_LogoTipoArq, @Contratada_LogoNomeArq, @Contratada_PessoaCod, @Contratada_AreaTrabalhoCod, @Contratada_MunicipioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000C9)
             ,new CursorDef("BC000C10", "UPDATE [Contratada] SET [Contratada_Sigla]=@Contratada_Sigla, [Contratada_TipoFabrica]=@Contratada_TipoFabrica, [Contratada_BancoNome]=@Contratada_BancoNome, [Contratada_BancoNro]=@Contratada_BancoNro, [Contratada_AgenciaNome]=@Contratada_AgenciaNome, [Contratada_AgenciaNro]=@Contratada_AgenciaNro, [Contratada_ContaCorrente]=@Contratada_ContaCorrente, [Contratada_OS]=@Contratada_OS, [Contratada_SS]=@Contratada_SS, [Contratada_Lote]=@Contratada_Lote, [Contratada_UsaOSistema]=@Contratada_UsaOSistema, [Contratada_OSPreferencial]=@Contratada_OSPreferencial, [Contratada_CntPadrao]=@Contratada_CntPadrao, [Contratada_Ativo]=@Contratada_Ativo, [Contratada_LogoTipoArq]=@Contratada_LogoTipoArq, [Contratada_LogoNomeArq]=@Contratada_LogoNomeArq, [Contratada_PessoaCod]=@Contratada_PessoaCod, [Contratada_AreaTrabalhoCod]=@Contratada_AreaTrabalhoCod, [Contratada_MunicipioCod]=@Contratada_MunicipioCod  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmBC000C10)
             ,new CursorDef("BC000C11", "UPDATE [Contratada] SET [Contratada_LogoArquivo]=@Contratada_LogoArquivo  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmBC000C11)
             ,new CursorDef("BC000C12", "UPDATE [Contratada] SET [Contratada_Logo]=@Contratada_Logo, [Contratada_Logo_GXI]=@Contratada_Logo_GXI  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmBC000C12)
             ,new CursorDef("BC000C13", "DELETE FROM [Contratada]  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK,prmBC000C13)
             ,new CursorDef("BC000C14", "SELECT [AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, [AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, [AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contratada_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C14,1,0,true,false )
             ,new CursorDef("BC000C15", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C15,1,0,true,false )
             ,new CursorDef("BC000C16", "SELECT [Estado_UF] AS Contratada_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Contratada_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C16,1,0,true,false )
             ,new CursorDef("BC000C17", "SELECT TOP 1 [Tributo_Codigo] FROM [Tributo] WITH (NOLOCK) WHERE [Tributo_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C17,1,0,true,true )
             ,new CursorDef("BC000C18", "SELECT TOP 1 [Gestao_Codigo] FROM [Gestao] WITH (NOLOCK) WHERE [Gestao_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C18,1,0,true,true )
             ,new CursorDef("BC000C19", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ContratadaOrigemCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C19,1,0,true,true )
             ,new CursorDef("BC000C20", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C20,1,0,true,true )
             ,new CursorDef("BC000C21", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C21,1,0,true,true )
             ,new CursorDef("BC000C22", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C22,1,0,true,true )
             ,new CursorDef("BC000C23", "SELECT TOP 1 [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C23,1,0,true,true )
             ,new CursorDef("BC000C24", "SELECT TM1.[Contratada_Codigo], T2.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T2.[AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, T3.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Pessoa_IE], T3.[Pessoa_Endereco], T3.[Pessoa_CEP], T3.[Pessoa_Telefone], T3.[Pessoa_Fax], TM1.[Contratada_Sigla], TM1.[Contratada_TipoFabrica], TM1.[Contratada_BancoNome], TM1.[Contratada_BancoNro], TM1.[Contratada_AgenciaNome], TM1.[Contratada_AgenciaNro], TM1.[Contratada_ContaCorrente], TM1.[Contratada_OS], TM1.[Contratada_SS], TM1.[Contratada_Lote], TM1.[Contratada_UsaOSistema], TM1.[Contratada_OSPreferencial], TM1.[Contratada_CntPadrao], TM1.[Contratada_Ativo], TM1.[Contratada_Logo_GXI], TM1.[Contratada_LogoTipoArq], TM1.[Contratada_LogoNomeArq], TM1.[Contratada_PessoaCod] AS Contratada_PessoaCod, TM1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, TM1.[Contratada_MunicipioCod] AS Contratada_MunicipioCod, T2.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T4.[Estado_UF] AS Contratada_UF, TM1.[Contratada_LogoArquivo], TM1.[Contratada_Logo] FROM ((([Contratada] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Contratada_AreaTrabalhoCod]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = TM1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T4 WITH (NOLOCK) ON T4.[Municipio_Codigo] = TM1.[Contratada_MunicipioCod]) WHERE TM1.[Contratada_Codigo] = @Contratada_Codigo ORDER BY TM1.[Contratada_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C24,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((bool[]) buf[21])[0] = rslt.getBool(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                ((String[]) buf[26])[0] = rslt.getMultimediaUri(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getBLOBFile(22, rslt.getString(17, 10), rslt.getString(18, 50)) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((String[]) buf[38])[0] = rslt.getMultimediaFile(23, rslt.getVarchar(16)) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((short[]) buf[17])[0] = rslt.getShort(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((bool[]) buf[19])[0] = rslt.getBool(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((bool[]) buf[21])[0] = rslt.getBool(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                ((String[]) buf[26])[0] = rslt.getMultimediaUri(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((int[]) buf[34])[0] = rslt.getInt(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getBLOBFile(22, rslt.getString(17, 10), rslt.getString(18, 50)) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((String[]) buf[38])[0] = rslt.getMultimediaFile(23, rslt.getVarchar(16)) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[21])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 6) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 10) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((int[]) buf[33])[0] = rslt.getInt(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((short[]) buf[35])[0] = rslt.getShort(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((bool[]) buf[37])[0] = rslt.getBool(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((bool[]) buf[39])[0] = rslt.getBool(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((int[]) buf[41])[0] = rslt.getInt(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((bool[]) buf[43])[0] = rslt.getBool(24) ;
                ((String[]) buf[44])[0] = rslt.getMultimediaUri(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((String[]) buf[46])[0] = rslt.getString(26, 10) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((String[]) buf[48])[0] = rslt.getString(27, 50) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((int[]) buf[51])[0] = rslt.getInt(29) ;
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((int[]) buf[54])[0] = rslt.getInt(31) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(31);
                ((String[]) buf[56])[0] = rslt.getString(32, 2) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                ((String[]) buf[58])[0] = rslt.getBLOBFile(33, rslt.getString(26, 10), rslt.getString(27, 50)) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(33);
                ((String[]) buf[60])[0] = rslt.getMultimediaFile(34, rslt.getVarchar(25)) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(34);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[21])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 6) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 50) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 10) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getString(17, 10) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((int[]) buf[33])[0] = rslt.getInt(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((short[]) buf[35])[0] = rslt.getShort(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((bool[]) buf[37])[0] = rslt.getBool(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((bool[]) buf[39])[0] = rslt.getBool(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((int[]) buf[41])[0] = rslt.getInt(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((bool[]) buf[43])[0] = rslt.getBool(24) ;
                ((String[]) buf[44])[0] = rslt.getMultimediaUri(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((String[]) buf[46])[0] = rslt.getString(26, 10) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((String[]) buf[48])[0] = rslt.getString(27, 50) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((int[]) buf[51])[0] = rslt.getInt(29) ;
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((int[]) buf[54])[0] = rslt.getInt(31) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(31);
                ((String[]) buf[56])[0] = rslt.getString(32, 2) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                ((String[]) buf[58])[0] = rslt.getBLOBFile(33, rslt.getString(26, 10), rslt.getString(27, 50)) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(33);
                ((String[]) buf[60])[0] = rslt.getMultimediaFile(34, rslt.getVarchar(25)) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(34);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[25]);
                }
                stmt.SetParameter(15, (bool)parms[26]);
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(17, (String)parms[30], (String)parms[28]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[34]);
                }
                stmt.SetParameter(20, (int)parms[35]);
                stmt.SetParameter(21, (int)parms[36]);
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[38]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(11, (bool)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[23]);
                }
                stmt.SetParameter(14, (bool)parms[24]);
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[28]);
                }
                stmt.SetParameter(17, (int)parms[29]);
                stmt.SetParameter(18, (int)parms[30]);
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[32]);
                }
                stmt.SetParameter(20, (int)parms[33]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
