/*
               File: WC_SelArquivo
        Description: Anexar arquivo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:26.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_selarquivo : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_selarquivo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_selarquivo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynavTipodocumento_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODOCUMENTO_CODIGO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvTIPODOCUMENTO_CODIGOJY2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAJY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               GXVvTIPODOCUMENTO_CODIGO_htmlJY2( ) ;
               WSJY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Anexar arquivo") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311762640");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.WriteHtmlText( Form.Headerrawhtml) ;
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_selarquivo.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_ARQUIVOEVIDENCIA", AV10Sdt_ArquivoEvidencia);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_ARQUIVOEVIDENCIA", AV10Sdt_ArquivoEvidencia);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV9Sdt_ContagemResultadoEvidencias);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV9Sdt_ContagemResultadoEvidencias);
         }
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtlgxBlob, AV5Blob);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormJY2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_selarquivo.js", "?2020311762641");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_SelArquivo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Anexar arquivo" ;
      }

      protected void WBJY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_selarquivo.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_JY2( true) ;
         }
         else
         {
            wb_table1_2_JY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JY2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTJY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Anexar arquivo", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPJY0( ) ;
            }
         }
      }

      protected void WSJY2( )
      {
         STARTJY2( ) ;
         EVTJY2( ) ;
      }

      protected void EVTJY2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11JY2 */
                                    E11JY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VBLOB.ISVALID") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12JY2 */
                                    E12JY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VTIPODOCUMENTO_CODIGO.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13JY2 */
                                    E13JY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14JY2 */
                                    E14JY2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJY0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavBlob_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJY2( ) ;
            }
         }
      }

      protected void PAJY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynavTipodocumento_codigo.Name = "vTIPODOCUMENTO_CODIGO";
            dynavTipodocumento_codigo.WebTags = "";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavBlob_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGOJY2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODOCUMENTO_CODIGO_dataJY2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODOCUMENTO_CODIGO_htmlJY2( )
      {
         int gxdynajaxvalue ;
         GXDLVvTIPODOCUMENTO_CODIGO_dataJY2( ) ;
         gxdynajaxindex = 1;
         dynavTipodocumento_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTipodocumento_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV7TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGO_dataJY2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00JY2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JY2_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JY2_A646TipoDocumento_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV7TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFJY2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14JY2 */
            E14JY2 ();
            WBJY0( ) ;
         }
      }

      protected void STRUPJY0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvTIPODOCUMENTO_CODIGO_htmlJY2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11JY2 */
         E11JY2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5Blob = cgiGet( edtavBlob_Internalname);
            dynavTipodocumento_codigo.CurrentValue = cgiGet( dynavTipodocumento_codigo_Internalname);
            AV7TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynavTipodocumento_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0)));
            AV6FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6FileName", AV6FileName);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV5Blob = cgiGet( sPrefix+GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvTIPODOCUMENTO_CODIGO_htmlJY2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11JY2 */
         E11JY2 ();
         if (returnInSub) return;
      }

      protected void E11JY2( )
      {
         /* Start Routine */
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         AV9Sdt_ContagemResultadoEvidencias.FromXml(AV8Websession.Get("ArquivosEvd"), "");
      }

      protected void E12JY2( )
      {
         /* Blob_Isvalid Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
         {
            AV14Arquivo = AV5Blob;
            AV6FileName = StringUtil.Substring( AV6FileName, StringUtil.StringSearchRev( AV6FileName, "\\", -1)+1, 255);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6FileName", AV6FileName);
            AV13Ext = StringUtil.Trim( StringUtil.Substring( AV6FileName, StringUtil.StringSearchRev( AV6FileName, ".", -1)+1, 4));
            AV6FileName = StringUtil.Substring( AV6FileName, 1, StringUtil.StringSearchRev( AV6FileName, ".", -1)-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6FileName", AV6FileName);
            AV10Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo = AV14Arquivo;
            AV10Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = AV6FileName;
            AV10Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq = AV13Ext;
            if ( (0==AV9Sdt_ContagemResultadoEvidencias.Count) )
            {
               AV9Sdt_ContagemResultadoEvidencias.Add(AV10Sdt_ArquivoEvidencia, 0);
            }
            AV8Websession.Set("ArquivosEvd", AV9Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV10Sdt_ArquivoEvidencia", AV10Sdt_ArquivoEvidencia);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV9Sdt_ContagemResultadoEvidencias", AV9Sdt_ContagemResultadoEvidencias);
      }

      protected void E13JY2( )
      {
         /* Tipodocumento_codigo_Click Routine */
         AV10Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo = AV7TipoDocumento_Codigo;
         AV10Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_nome = StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0);
         AV9Sdt_ContagemResultadoEvidencias.Add(AV10Sdt_ArquivoEvidencia, 0);
         if ( (0==AV9Sdt_ContagemResultadoEvidencias.Count) )
         {
            AV8Websession.Set("ArquivosEvd", AV9Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
         }
         AV8Websession.Set("ArquivosEvd", AV9Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV10Sdt_ArquivoEvidencia", AV10Sdt_ArquivoEvidencia);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV9Sdt_ContagemResultadoEvidencias", AV9Sdt_ContagemResultadoEvidencias);
      }

      protected void nextLoad( )
      {
      }

      protected void E14JY2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_JY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 5,'" + sPrefix + "',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               gxblobfileaux.Source = AV5Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV5Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV5Blob), context.PathToRelativeUrl( AV5Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV5Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 250, "px", 18, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,5);\"", "", "", "HLP_WC_SelArquivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Tipo Documento:", "", "", lblTextblock1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WC_SelArquivo.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'" + sPrefix + "',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodocumento_codigo, dynavTipodocumento_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0)), 1, dynavTipodocumento_codigo_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVTIPODOCUMENTO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,9);\"", "", true, "HLP_WC_SelArquivo.htm");
            dynavTipodocumento_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV7TipoDocumento_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavTipodocumento_codigo_Internalname, "Values", (String)(dynavTipodocumento_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "<p>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, AV6FileName, StringUtil.RTrim( context.localUtil.Format( AV6FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:1.0pt; font-weight:normal; font-style:normal; color:#00FFFFFF; background-color:#00FFFFFF;", "", "", 1, edtavFilename_Enabled, 0, "text", "", 615, "px", 1, "px", 255, 0, 0, 0, 1, 0, -1, true, "", "left", true, "HLP_WC_SelArquivo.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JY2e( true) ;
         }
         else
         {
            wb_table1_2_JY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJY2( ) ;
         WSJY2( ) ;
         WEJY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAJY2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_selarquivo");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAJY2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
         }
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAJY2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSJY2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSJY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEJY2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311762661");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_selarquivo.js", "?2020311762661");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         edtavBlob_Internalname = sPrefix+"vBLOB";
         lblTextblock1_Internalname = sPrefix+"TEXTBLOCK1";
         dynavTipodocumento_codigo_Internalname = sPrefix+"vTIPODOCUMENTO_CODIGO";
         edtavFilename_Internalname = sPrefix+"vFILENAME";
         tblTable1_Internalname = sPrefix+"TABLE1";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavFilename_Jsonclick = "";
         edtavFilename_Backstyle = -1;
         edtavFilename_Backcolor = (int)(0x00FFFFFF);
         edtavFilename_Enabled = 1;
         dynavTipodocumento_codigo_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         Form.Headerrawhtml = "";
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("VBLOB.ISVALID","{handler:'E12JY2',iparms:[{av:'AV5Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV6FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV10Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV9Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}],oparms:[{av:'AV6FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV10Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV9Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}]}");
         setEventMetadata("VTIPODOCUMENTO_CODIGO.CLICK","{handler:'E13JY2',iparms:[{av:'AV7TipoDocumento_Codigo',fld:'vTIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV9Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}],oparms:[{av:'AV10Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV9Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV10Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV9Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         GXCCtlgxBlob = "";
         AV5Blob = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00JY2_A645TipoDocumento_Codigo = new int[1] ;
         H00JY2_A646TipoDocumento_Nome = new String[] {""} ;
         H00JY2_A647TipoDocumento_Ativo = new bool[] {false} ;
         AV6FileName = "";
         AV8Websession = context.GetSession();
         AV14Arquivo = "";
         AV13Ext = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_selarquivo__default(),
            new Object[][] {
                new Object[] {
               H00JY2_A645TipoDocumento_Codigo, H00JY2_A646TipoDocumento_Nome, H00JY2_A647TipoDocumento_Ativo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short edtavFilename_Backstyle ;
      private int gxdynajaxindex ;
      private int AV7TipoDocumento_Codigo ;
      private int edtavFilename_Enabled ;
      private int idxLst ;
      private int edtavFilename_Backcolor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBlob_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavTipodocumento_codigo_Internalname ;
      private String edtavFilename_Internalname ;
      private String AV14Arquivo ;
      private String AV13Ext ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String dynavTipodocumento_codigo_Jsonclick ;
      private String edtavFilename_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV6FileName ;
      private String AV5Blob ;
      private IGxSession AV8Websession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavTipodocumento_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00JY2_A645TipoDocumento_Codigo ;
      private String[] H00JY2_A646TipoDocumento_Nome ;
      private bool[] H00JY2_A647TipoDocumento_Ativo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV9Sdt_ContagemResultadoEvidencias ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV10Sdt_ArquivoEvidencia ;
   }

   public class wc_selarquivo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JY2 ;
          prmH00JY2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JY2", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Ativo] = 1 ORDER BY [TipoDocumento_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JY2,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
