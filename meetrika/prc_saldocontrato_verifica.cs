/*
               File: Prc_SaldoContrato_Verifica
        Description: Prc_Saldo Contrato_Verifica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:3.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saldocontrato_verifica : GXProcedure
   {
      public prc_saldocontrato_verifica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saldocontrato_verifica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo ,
                           ref DateTime aP1_ContratoTermoAditivo_DataInicio ,
                           ref bool aP2_isOK )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV12ContratoTermoAditivo_DataInicio = aP1_ContratoTermoAditivo_DataInicio;
         this.AV13isOK = aP2_isOK;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.AV8Contrato_Codigo;
         aP1_ContratoTermoAditivo_DataInicio=this.AV12ContratoTermoAditivo_DataInicio;
         aP2_isOK=this.AV13isOK;
      }

      public bool executeUdp( ref int aP0_Contrato_Codigo ,
                              ref DateTime aP1_ContratoTermoAditivo_DataInicio )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV12ContratoTermoAditivo_DataInicio = aP1_ContratoTermoAditivo_DataInicio;
         this.AV13isOK = aP2_isOK;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.AV8Contrato_Codigo;
         aP1_ContratoTermoAditivo_DataInicio=this.AV12ContratoTermoAditivo_DataInicio;
         aP2_isOK=this.AV13isOK;
         return AV13isOK ;
      }

      public void executeSubmit( ref int aP0_Contrato_Codigo ,
                                 ref DateTime aP1_ContratoTermoAditivo_DataInicio ,
                                 ref bool aP2_isOK )
      {
         prc_saldocontrato_verifica objprc_saldocontrato_verifica;
         objprc_saldocontrato_verifica = new prc_saldocontrato_verifica();
         objprc_saldocontrato_verifica.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_saldocontrato_verifica.AV12ContratoTermoAditivo_DataInicio = aP1_ContratoTermoAditivo_DataInicio;
         objprc_saldocontrato_verifica.AV13isOK = aP2_isOK;
         objprc_saldocontrato_verifica.context.SetSubmitInitialConfig(context);
         objprc_saldocontrato_verifica.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saldocontrato_verifica);
         aP0_Contrato_Codigo=this.AV8Contrato_Codigo;
         aP1_ContratoTermoAditivo_DataInicio=this.AV12ContratoTermoAditivo_DataInicio;
         aP2_isOK=this.AV13isOK;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saldocontrato_verifica)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CZ2 */
         pr_default.execute(0, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1781SaldoContrato_Ativo = P00CZ2_A1781SaldoContrato_Ativo[0];
            A74Contrato_Codigo = P00CZ2_A74Contrato_Codigo[0];
            A1561SaldoContrato_Codigo = P00CZ2_A1561SaldoContrato_Codigo[0];
            A1571SaldoContrato_VigenciaInicio = P00CZ2_A1571SaldoContrato_VigenciaInicio[0];
            A1572SaldoContrato_VigenciaFim = P00CZ2_A1572SaldoContrato_VigenciaFim[0];
            AV9SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            AV10SaldoContrato_VigenciaInicio = A1571SaldoContrato_VigenciaInicio;
            AV11SaldoContrato_VigenciaFim = A1572SaldoContrato_VigenciaFim;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV13isOK = false;
         if ( AV12ContratoTermoAditivo_DataInicio > AV11SaldoContrato_VigenciaFim )
         {
            AV13isOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CZ2_A1781SaldoContrato_Ativo = new bool[] {false} ;
         P00CZ2_A74Contrato_Codigo = new int[1] ;
         P00CZ2_A1561SaldoContrato_Codigo = new int[1] ;
         P00CZ2_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00CZ2_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         AV10SaldoContrato_VigenciaInicio = DateTime.MinValue;
         AV11SaldoContrato_VigenciaFim = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saldocontrato_verifica__default(),
            new Object[][] {
                new Object[] {
               P00CZ2_A1781SaldoContrato_Ativo, P00CZ2_A74Contrato_Codigo, P00CZ2_A1561SaldoContrato_Codigo, P00CZ2_A1571SaldoContrato_VigenciaInicio, P00CZ2_A1572SaldoContrato_VigenciaFim
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int AV9SaldoContrato_Codigo ;
      private String scmdbuf ;
      private DateTime AV12ContratoTermoAditivo_DataInicio ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private DateTime AV10SaldoContrato_VigenciaInicio ;
      private DateTime AV11SaldoContrato_VigenciaFim ;
      private bool AV13isOK ;
      private bool A1781SaldoContrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private DateTime aP1_ContratoTermoAditivo_DataInicio ;
      private bool aP2_isOK ;
      private IDataStoreProvider pr_default ;
      private bool[] P00CZ2_A1781SaldoContrato_Ativo ;
      private int[] P00CZ2_A74Contrato_Codigo ;
      private int[] P00CZ2_A1561SaldoContrato_Codigo ;
      private DateTime[] P00CZ2_A1571SaldoContrato_VigenciaInicio ;
      private DateTime[] P00CZ2_A1572SaldoContrato_VigenciaFim ;
   }

   public class prc_saldocontrato_verifica__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CZ2 ;
          prmP00CZ2 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CZ2", "SELECT [SaldoContrato_Ativo], [Contrato_Codigo], [SaldoContrato_Codigo], [SaldoContrato_VigenciaInicio], [SaldoContrato_VigenciaFim] FROM [SaldoContrato] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV8Contrato_Codigo) AND ([SaldoContrato_Ativo] = 1) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CZ2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
