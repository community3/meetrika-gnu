/*
               File: PRC_NovaOSVinculada
        Description: Nova OS Vinculada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:5:29.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_novaosvinculada : GXProcedure
   {
      public prc_novaosvinculada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_novaosvinculada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.AV15ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.AV15ContagemResultado_Codigo;
      }

      public int executeUdp( )
      {
         this.AV15ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.AV15ContagemResultado_Codigo;
         return AV15ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo )
      {
         prc_novaosvinculada objprc_novaosvinculada;
         objprc_novaosvinculada = new prc_novaosvinculada();
         objprc_novaosvinculada.AV15ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_novaosvinculada.context.SetSubmitInitialConfig(context);
         objprc_novaosvinculada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_novaosvinculada);
         aP0_ContagemResultado_Codigo=this.AV15ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_novaosvinculada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV30Sdt_Demandas.FromXml(AV27Websession.Get("SDT_Demandas"), "");
         AV27Websession.Remove("SDT_Demandas");
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV30Sdt_Demandas.Count )
         {
            AV31Sdt_Demanda = ((SdtSDT_Demandas_Demanda)AV30Sdt_Demandas.Item(AV42GXV1));
            AV23UserId = AV31Sdt_Demanda.gxTpr_Contadorfmcod;
            AV22ContagemResultado_ContratadaCod = AV31Sdt_Demanda.gxTpr_Contratada_codigo;
            AV11ContagemResultado_DataCnt = AV31Sdt_Demanda.gxTpr_Datacnt;
            AV8NewContagemResultado_Demanda = AV31Sdt_Demanda.gxTpr_Demanda;
            AV18ContagemResultado_PFBFM = AV31Sdt_Demanda.gxTpr_Pfbfm;
            AV32ContagemResultado_Observacao = AV31Sdt_Demanda.gxTpr_Observacao;
            AV25ContagemResultado_Servico = AV31Sdt_Demanda.gxTpr_Servico_codigo;
            AV34ContagemResultado_NaoCnfDmnCod = AV31Sdt_Demanda.gxTpr_Naoconformidadedmn;
            AV33ContagemResultado_NaoCnfCntCod = AV31Sdt_Demanda.gxTpr_Naoconformidadecnt;
            AV42GXV1 = (int)(AV42GXV1+1);
         }
         /* Using cursor P004J3 */
         pr_default.execute(0, new Object[] {AV15ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P004J3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004J3_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P004J3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004J3_n601ContagemResultado_Servico[0];
            A456ContagemResultado_Codigo = P004J3_A456ContagemResultado_Codigo[0];
            A805ContagemResultado_ContratadaOrigemCod = P004J3_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P004J3_n805ContagemResultado_ContratadaOrigemCod[0];
            A454ContagemResultado_ContadorFSCod = P004J3_A454ContagemResultado_ContadorFSCod[0];
            n454ContagemResultado_ContadorFSCod = P004J3_n454ContagemResultado_ContadorFSCod[0];
            A489ContagemResultado_SistemaCod = P004J3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P004J3_n489ContagemResultado_SistemaCod[0];
            A146Modulo_Codigo = P004J3_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P004J3_n146Modulo_Codigo[0];
            A457ContagemResultado_Demanda = P004J3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P004J3_n457ContagemResultado_Demanda[0];
            A1598ContagemResultado_ServicoNome = P004J3_A1598ContagemResultado_ServicoNome[0];
            n1598ContagemResultado_ServicoNome = P004J3_n1598ContagemResultado_ServicoNome[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P004J3_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P004J3_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1596ContagemResultado_CntSrvPrc = P004J3_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P004J3_n1596ContagemResultado_CntSrvPrc[0];
            A1603ContagemResultado_CntCod = P004J3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P004J3_n1603ContagemResultado_CntCod[0];
            A684ContagemResultado_PFBFSUltima = P004J3_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = P004J3_A685ContagemResultado_PFLFSUltima[0];
            A601ContagemResultado_Servico = P004J3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004J3_n601ContagemResultado_Servico[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P004J3_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P004J3_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1596ContagemResultado_CntSrvPrc = P004J3_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P004J3_n1596ContagemResultado_CntSrvPrc[0];
            A1603ContagemResultado_CntCod = P004J3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P004J3_n1603ContagemResultado_CntCod[0];
            A1598ContagemResultado_ServicoNome = P004J3_A1598ContagemResultado_ServicoNome[0];
            n1598ContagemResultado_ServicoNome = P004J3_n1598ContagemResultado_ServicoNome[0];
            A684ContagemResultado_PFBFSUltima = P004J3_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = P004J3_A685ContagemResultado_PFLFSUltima[0];
            AV37ContratadaOrigem_Codigo = A805ContagemResultado_ContratadaOrigemCod;
            AV36ContagemResultado_ContadorFSCod = A454ContagemResultado_ContadorFSCod;
            AV9ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
            AV12Modulo_Codigo = A146Modulo_Codigo;
            AV29ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            AV16ContagemResultado_PFBFS = A684ContagemResultado_PFBFSUltima;
            AV17ContagemResultado_PFLFS = A685ContagemResultado_PFLFSUltima;
            AV13ContagemResultado_Descricao = StringUtil.Trim( A1598ContagemResultado_ServicoNome) + " (OS " + StringUtil.Trim( AV29ContagemResultado_Demanda) + ")";
            AV26ContagemResultado_ValorPF = A1597ContagemResultado_CntSrvVlrUndCnt;
            AV28Servico_Percentual = A1596ContagemResultado_CntSrvPrc;
            AV38Contrato_Codigo = A1603ContagemResultado_CntCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P004J4 */
         pr_default.execute(1, new Object[] {AV38Contrato_Codigo, AV25ContagemResultado_Servico});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A155Servico_Codigo = P004J4_A155Servico_Codigo[0];
            A74Contrato_Codigo = P004J4_A74Contrato_Codigo[0];
            A160ContratoServicos_Codigo = P004J4_A160ContratoServicos_Codigo[0];
            AV39ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         /*
            INSERT RECORD ON TABLE ContagemResultado

         */
         A457ContagemResultado_Demanda = AV8NewContagemResultado_Demanda;
         n457ContagemResultado_Demanda = false;
         A489ContagemResultado_SistemaCod = AV9ContagemResultado_SistemaCod;
         n489ContagemResultado_SistemaCod = false;
         A490ContagemResultado_ContratadaCod = AV22ContagemResultado_ContratadaCod;
         n490ContagemResultado_ContratadaCod = false;
         A471ContagemResultado_DataDmn = AV11ContagemResultado_DataCnt;
         if ( (0==AV12Modulo_Codigo) )
         {
            A146Modulo_Codigo = 0;
            n146Modulo_Codigo = false;
            n146Modulo_Codigo = true;
         }
         else
         {
            A146Modulo_Codigo = AV12Modulo_Codigo;
            n146Modulo_Codigo = false;
         }
         A494ContagemResultado_Descricao = AV13ContagemResultado_Descricao;
         n494ContagemResultado_Descricao = false;
         A465ContagemResultado_Link = AV14ContagemResultado_Link;
         n465ContagemResultado_Link = false;
         if ( (0==AV37ContratadaOrigem_Codigo) )
         {
            A805ContagemResultado_ContratadaOrigemCod = 0;
            n805ContagemResultado_ContratadaOrigemCod = false;
            n805ContagemResultado_ContratadaOrigemCod = true;
         }
         else
         {
            A805ContagemResultado_ContratadaOrigemCod = AV35ContraradaOrigem_Codigo;
            n805ContagemResultado_ContratadaOrigemCod = false;
         }
         if ( (0==AV36ContagemResultado_ContadorFSCod) )
         {
            A454ContagemResultado_ContadorFSCod = 0;
            n454ContagemResultado_ContadorFSCod = false;
            n454ContagemResultado_ContadorFSCod = true;
         }
         else
         {
            A454ContagemResultado_ContadorFSCod = AV36ContagemResultado_ContadorFSCod;
            n454ContagemResultado_ContadorFSCod = false;
         }
         A484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         A512ContagemResultado_ValorPF = AV26ContagemResultado_ValorPF;
         n512ContagemResultado_ValorPF = false;
         A602ContagemResultado_OSVinculada = AV15ContagemResultado_Codigo;
         n602ContagemResultado_OSVinculada = false;
         A508ContagemResultado_Owner = AV23UserId;
         A514ContagemResultado_Observacao = AV32ContagemResultado_Observacao;
         n514ContagemResultado_Observacao = false;
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         A1553ContagemResultado_CntSrvCod = AV39ContratoServicos_Codigo;
         n1553ContagemResultado_CntSrvCod = false;
         A468ContagemResultado_NaoCnfDmnCod = 0;
         n468ContagemResultado_NaoCnfDmnCod = false;
         n468ContagemResultado_NaoCnfDmnCod = true;
         A597ContagemResultado_LoteAceiteCod = 0;
         n597ContagemResultado_LoteAceiteCod = false;
         n597ContagemResultado_LoteAceiteCod = true;
         A890ContagemResultado_Responsavel = 0;
         n890ContagemResultado_Responsavel = false;
         n890ContagemResultado_Responsavel = true;
         A1043ContagemResultado_LiqLogCod = 0;
         n1043ContagemResultado_LiqLogCod = false;
         n1043ContagemResultado_LiqLogCod = true;
         A1583ContagemResultado_TipoRegistro = 1;
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         A1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         /* Using cursor P004J5 */
         pr_default.execute(2, new Object[] {A471ContagemResultado_DataDmn, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n465ContagemResultado_Link, A465ContagemResultado_Link, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1583ContagemResultado_TipoRegistro});
         A456ContagemResultado_Codigo = P004J5_A456ContagemResultado_Codigo[0];
         pr_default.close(2);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         if ( (pr_default.getStatus(2) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV15ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         AV27Websession.Set("InserindoContagem", "Vinculada");
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NovaOSVinculada");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV30Sdt_Demandas = new GxObjectCollection( context, "SDT_Demandas.Demanda", "GxEv3Up14_Meetrika", "SdtSDT_Demandas_Demanda", "GeneXus.Programs");
         AV27Websession = context.GetSession();
         AV31Sdt_Demanda = new SdtSDT_Demandas_Demanda(context);
         AV11ContagemResultado_DataCnt = DateTime.MinValue;
         AV8NewContagemResultado_Demanda = "";
         AV32ContagemResultado_Observacao = "";
         scmdbuf = "";
         P004J3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004J3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004J3_A601ContagemResultado_Servico = new int[1] ;
         P004J3_n601ContagemResultado_Servico = new bool[] {false} ;
         P004J3_A456ContagemResultado_Codigo = new int[1] ;
         P004J3_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P004J3_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P004J3_A454ContagemResultado_ContadorFSCod = new int[1] ;
         P004J3_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         P004J3_A489ContagemResultado_SistemaCod = new int[1] ;
         P004J3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P004J3_A146Modulo_Codigo = new int[1] ;
         P004J3_n146Modulo_Codigo = new bool[] {false} ;
         P004J3_A457ContagemResultado_Demanda = new String[] {""} ;
         P004J3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P004J3_A1598ContagemResultado_ServicoNome = new String[] {""} ;
         P004J3_n1598ContagemResultado_ServicoNome = new bool[] {false} ;
         P004J3_A1597ContagemResultado_CntSrvVlrUndCnt = new decimal[1] ;
         P004J3_n1597ContagemResultado_CntSrvVlrUndCnt = new bool[] {false} ;
         P004J3_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P004J3_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P004J3_A1603ContagemResultado_CntCod = new int[1] ;
         P004J3_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P004J3_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P004J3_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         A457ContagemResultado_Demanda = "";
         A1598ContagemResultado_ServicoNome = "";
         AV29ContagemResultado_Demanda = "";
         AV13ContagemResultado_Descricao = "";
         P004J4_A155Servico_Codigo = new int[1] ;
         P004J4_A74Contrato_Codigo = new int[1] ;
         P004J4_A160ContratoServicos_Codigo = new int[1] ;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A494ContagemResultado_Descricao = "";
         A465ContagemResultado_Link = "";
         AV14ContagemResultado_Link = "";
         A484ContagemResultado_StatusDmn = "";
         A514ContagemResultado_Observacao = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         P004J5_A456ContagemResultado_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_novaosvinculada__default(),
            new Object[][] {
                new Object[] {
               P004J3_A1553ContagemResultado_CntSrvCod, P004J3_n1553ContagemResultado_CntSrvCod, P004J3_A601ContagemResultado_Servico, P004J3_n601ContagemResultado_Servico, P004J3_A456ContagemResultado_Codigo, P004J3_A805ContagemResultado_ContratadaOrigemCod, P004J3_n805ContagemResultado_ContratadaOrigemCod, P004J3_A454ContagemResultado_ContadorFSCod, P004J3_n454ContagemResultado_ContadorFSCod, P004J3_A489ContagemResultado_SistemaCod,
               P004J3_n489ContagemResultado_SistemaCod, P004J3_A146Modulo_Codigo, P004J3_n146Modulo_Codigo, P004J3_A457ContagemResultado_Demanda, P004J3_n457ContagemResultado_Demanda, P004J3_A1598ContagemResultado_ServicoNome, P004J3_n1598ContagemResultado_ServicoNome, P004J3_A1597ContagemResultado_CntSrvVlrUndCnt, P004J3_n1597ContagemResultado_CntSrvVlrUndCnt, P004J3_A1596ContagemResultado_CntSrvPrc,
               P004J3_n1596ContagemResultado_CntSrvPrc, P004J3_A1603ContagemResultado_CntCod, P004J3_n1603ContagemResultado_CntCod, P004J3_A684ContagemResultado_PFBFSUltima, P004J3_A685ContagemResultado_PFLFSUltima
               }
               , new Object[] {
               P004J4_A155Servico_Codigo, P004J4_A74Contrato_Codigo, P004J4_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P004J5_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1583ContagemResultado_TipoRegistro ;
      private short A1515ContagemResultado_Evento ;
      private int AV15ContagemResultado_Codigo ;
      private int AV42GXV1 ;
      private int AV23UserId ;
      private int AV22ContagemResultado_ContratadaCod ;
      private int AV25ContagemResultado_Servico ;
      private int AV34ContagemResultado_NaoCnfDmnCod ;
      private int AV33ContagemResultado_NaoCnfCntCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A146Modulo_Codigo ;
      private int A1603ContagemResultado_CntCod ;
      private int AV37ContratadaOrigem_Codigo ;
      private int AV36ContagemResultado_ContadorFSCod ;
      private int AV9ContagemResultado_SistemaCod ;
      private int AV12Modulo_Codigo ;
      private int AV38Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV39ContratoServicos_Codigo ;
      private int GX_INS69 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV35ContraradaOrigem_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A508ContagemResultado_Owner ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A1452ContagemResultado_SS ;
      private decimal AV18ContagemResultado_PFBFM ;
      private decimal A1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal AV16ContagemResultado_PFBFS ;
      private decimal AV17ContagemResultado_PFLFS ;
      private decimal AV26ContagemResultado_ValorPF ;
      private decimal AV28Servico_Percentual ;
      private decimal A512ContagemResultado_ValorPF ;
      private String scmdbuf ;
      private String A1598ContagemResultado_ServicoNome ;
      private String A484ContagemResultado_StatusDmn ;
      private String Gx_emsg ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime AV11ContagemResultado_DataCnt ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n146Modulo_Codigo ;
      private bool n457ContagemResultado_Demanda ;
      private bool n1598ContagemResultado_ServicoNome ;
      private bool n1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n465ContagemResultado_Link ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n514ContagemResultado_Observacao ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1452ContagemResultado_SS ;
      private bool n1515ContagemResultado_Evento ;
      private String AV32ContagemResultado_Observacao ;
      private String A465ContagemResultado_Link ;
      private String AV14ContagemResultado_Link ;
      private String A514ContagemResultado_Observacao ;
      private String AV8NewContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String AV29ContagemResultado_Demanda ;
      private String AV13ContagemResultado_Descricao ;
      private String A494ContagemResultado_Descricao ;
      private IGxSession AV27Websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P004J3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004J3_n1553ContagemResultado_CntSrvCod ;
      private int[] P004J3_A601ContagemResultado_Servico ;
      private bool[] P004J3_n601ContagemResultado_Servico ;
      private int[] P004J3_A456ContagemResultado_Codigo ;
      private int[] P004J3_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P004J3_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P004J3_A454ContagemResultado_ContadorFSCod ;
      private bool[] P004J3_n454ContagemResultado_ContadorFSCod ;
      private int[] P004J3_A489ContagemResultado_SistemaCod ;
      private bool[] P004J3_n489ContagemResultado_SistemaCod ;
      private int[] P004J3_A146Modulo_Codigo ;
      private bool[] P004J3_n146Modulo_Codigo ;
      private String[] P004J3_A457ContagemResultado_Demanda ;
      private bool[] P004J3_n457ContagemResultado_Demanda ;
      private String[] P004J3_A1598ContagemResultado_ServicoNome ;
      private bool[] P004J3_n1598ContagemResultado_ServicoNome ;
      private decimal[] P004J3_A1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool[] P004J3_n1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal[] P004J3_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P004J3_n1596ContagemResultado_CntSrvPrc ;
      private int[] P004J3_A1603ContagemResultado_CntCod ;
      private bool[] P004J3_n1603ContagemResultado_CntCod ;
      private decimal[] P004J3_A684ContagemResultado_PFBFSUltima ;
      private decimal[] P004J3_A685ContagemResultado_PFLFSUltima ;
      private int[] P004J4_A155Servico_Codigo ;
      private int[] P004J4_A74Contrato_Codigo ;
      private int[] P004J4_A160ContratoServicos_Codigo ;
      private int[] P004J5_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Demandas_Demanda ))]
      private IGxCollection AV30Sdt_Demandas ;
      private SdtSDT_Demandas_Demanda AV31Sdt_Demanda ;
   }

   public class prc_novaosvinculada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004J3 ;
          prmP004J3 = new Object[] {
          new Object[] {"@AV15ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004J4 ;
          prmP004J4 = new Object[] {
          new Object[] {"@AV38Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004J5 ;
          prmP004J5 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0}
          } ;
          String cmdBufferP004J5 ;
          cmdBufferP004J5=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_LiqLogCod], [ContagemResultado_DataCadastro], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_CntSrvCod], [ContagemResultado_TipoRegistro], [ContagemResultado_DataEntrega], [ContagemResultado_DemandaFM], [ContagemResultado_Evidencia], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_HoraEntrega], [ContagemResultado_FncUsrCod], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoInicialDias], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], "
          + " [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_ServicoSS], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_Link, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_LoteAceiteCod, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_Responsavel, @ContagemResultado_LiqLogCod, @ContagemResultado_DataCadastro, @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_CntSrvCod, @ContagemResultado_TipoRegistro, convert( DATETIME, '17530101', 112 ), '', '', convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), '', convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101',"
          + " 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(int, 0), convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          def= new CursorDef[] {
              new CursorDef("P004J3", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_ContadorFSCod], T1.[ContagemResultado_SistemaCod], T1.[Modulo_Codigo], T1.[ContagemResultado_Demanda], T3.[Servico_Nome] AS ContagemResultado_ServicoNome, T2.[Servico_VlrUnidadeContratada] AS ContagemResultado_CntSrvVlrUndCnt, T2.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T4.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV15ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004J3,1,0,false,true )
             ,new CursorDef("P004J4", "SELECT TOP 1 [Servico_Codigo], [Contrato_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV38Contrato_Codigo) AND ([Servico_Codigo] = @AV25ContagemResultado_Servico) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004J4,1,0,false,true )
             ,new CursorDef("P004J5", cmdBufferP004J5, GxErrorMask.GX_NOMASK,prmP004J5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(13) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(14) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[22]);
                }
                stmt.SetParameter(13, (int)parms[23]);
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(16, (bool)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(21, (DateTime)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(23, (short)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[45]);
                }
                stmt.SetParameter(25, (short)parms[46]);
                return;
       }
    }

 }

}
