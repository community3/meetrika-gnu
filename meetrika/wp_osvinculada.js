/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:14:10.21
*/
gx.evt.autoSkip = false;
gx.define('wp_osvinculada', false, function () {
   this.ServerClass =  "wp_osvinculada" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV20WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.A52Contratada_AreaTrabalhoCod=gx.fn.getIntegerValue("CONTRATADA_AREATRABALHOCOD",'.') ;
      this.A490ContagemResultado_ContratadaCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CONTRATADACOD",'.') ;
      this.A457ContagemResultado_Demanda=gx.fn.getControlValue("CONTAGEMRESULTADO_DEMANDA") ;
      this.A456ContagemResultado_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CODIGO",'.') ;
      this.A684ContagemResultado_PFBFSUltima=gx.fn.getDecimalValue("CONTAGEMRESULTADO_PFBFSULTIMA",'.',',') ;
      this.A685ContagemResultado_PFLFSUltima=gx.fn.getDecimalValue("CONTAGEMRESULTADO_PFLFSULTIMA",'.',',') ;
      this.A801ContagemResultado_ServicoSigla=gx.fn.getControlValue("CONTAGEMRESULTADO_SERVICOSIGLA") ;
      this.AV23Retorno=gx.fn.getIntegerValue("vRETORNO",'.') ;
      this.AV22QuemCadastrou=gx.fn.getControlValue("vQUEMCADASTROU") ;
      this.AV5ContagemResultado_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CODIGO",'.') ;
      this.AV24Sdt_Demanda=gx.fn.getControlValue("vSDT_DEMANDA") ;
      this.AV11ContagemResultado_PFBFM=gx.fn.getDecimalValue("vCONTAGEMRESULTADO_PFBFM",'.',',') ;
      this.AV13ContagemResultado_PFLFM=gx.fn.getDecimalValue("vCONTAGEMRESULTADO_PFLFM",'.',',') ;
      this.AV25Sdt_Demandas=gx.fn.getControlValue("vSDT_DEMANDAS") ;
   };
   this.Validv_Contagemresultado_contratadacod=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADO_CONTRATADACOD");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contratada_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Validv_Contratada_codigo",["gx.O.AV17Contratada_Codigo", "gx.O.AV21ContagemResultado_ContadorFMCod", "gx.O.AV15ContagemResultado_Servico"],["AV21ContagemResultado_ContadorFMCod", "AV15ContagemResultado_Servico"]);
      return true;
   }
   this.Validv_Contagemresultado_datacnt=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADO_DATACNT");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e18cf1_client=function()
   {
      this.clearMessages();
      if ( ! ((0==this.AV21ContagemResultado_ContadorFMCod)) )
      {
         gx.fn.setCtrlProperty("BTNENTER","Enabled", true );
         gx.fn.setCtrlProperty("BTNENTER","Tooltiptext", "Confirmar" );
      }
      this.refreshOutputs([{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]);
   };
   this.e19cf1_client=function()
   {
      this.clearMessages();
      if ( (new gx.date.gxdate('').compare(this.AV7ContagemResultado_DataCnt)==0) )
      {
         this.addMessage("Informe a data do Serviço!");
         gx.fn.setCtrlProperty("BTNENTER","Enabled", false );
         gx.fn.setCtrlProperty("BTNENTER","Tooltiptext", "Informe a data do Serviço!" );
      }
      else
      {
         gx.fn.setCtrlProperty("BTNENTER","Enabled", true );
         gx.fn.setCtrlProperty("BTNENTER","Tooltiptext", "Confirmar" );
      }
      this.refreshOutputs([{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]);
   };
   this.e12cf2_client=function()
   {
      this.executeServerEvent("VCONTAGEMRESULTADO_CONTRATADACOD.CLICK", true, null, false, true);
   };
   this.e13cf2_client=function()
   {
      this.executeServerEvent("VCONTAGEMRESULTADO_DEMANDA.ISVALID", true, null, false, true);
   };
   this.e14cf2_client=function()
   {
      this.executeServerEvent("VCONTAGEMRESULTADO_SERVICO.CLICK", true, null, false, true);
   };
   this.e15cf2_client=function()
   {
      this.executeServerEvent("VNEWCONTAGEMRESULTADO_DEMANDA.ISVALID", true, null, false, true);
   };
   this.e16cf2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e20cf1_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,10,13,15,17,19,22,25,27,30,32,35,37,40,42,45,47,50,52,55,57,60,62];
   this.GXLastCtrlId =62;
   GXValidFnc[2]={fld:"TABLE1",grid:0};
   GXValidFnc[8]={fld:"TEXTBLOCK12", format:0,grid:0};
   GXValidFnc[10]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultado_contratadacod,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_CONTRATADACOD",gxz:"ZV6ContagemResultado_ContratadaCod",gxold:"OV6ContagemResultado_ContratadaCod",gxvar:"AV6ContagemResultado_ContratadaCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV6ContagemResultado_ContratadaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV6ContagemResultado_ContratadaCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTAGEMRESULTADO_CONTRATADACOD",gx.O.AV6ContagemResultado_ContratadaCod)},c2v:function(){if(this.val()!==undefined)gx.O.AV6ContagemResultado_ContratadaCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CONTRATADACOD",'.')},nac:gx.falseFn};
   GXValidFnc[13]={fld:"TEXTBLOCK2", format:0,grid:0};
   GXValidFnc[15]={lvl:0,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:'e13cf2_client',rgrid:[],fld:"vCONTAGEMRESULTADO_DEMANDA",gxz:"ZV8ContagemResultado_Demanda",gxold:"OV8ContagemResultado_Demanda",gxvar:"AV8ContagemResultado_Demanda",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV8ContagemResultado_Demanda=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV8ContagemResultado_Demanda=Value},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_DEMANDA",gx.O.AV8ContagemResultado_Demanda,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV8ContagemResultado_Demanda=this.val()},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_DEMANDA")},nac:gx.falseFn};
   GXValidFnc[17]={fld:"TEXTBLOCK8", format:0,grid:0};
   GXValidFnc[19]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_SERVICOSIGLA",gxz:"ZV28ContagemResultado_ServicoSigla",gxold:"OV28ContagemResultado_ServicoSigla",gxvar:"AV28ContagemResultado_ServicoSigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV28ContagemResultado_ServicoSigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV28ContagemResultado_ServicoSigla=Value},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_SERVICOSIGLA",gx.O.AV28ContagemResultado_ServicoSigla,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV28ContagemResultado_ServicoSigla=this.val()},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_SERVICOSIGLA")},nac:gx.falseFn};
   GXValidFnc[22]={fld:"TABLE3",grid:0};
   GXValidFnc[25]={fld:"TEXTBLOCK9", format:0,grid:0};
   GXValidFnc[27]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_PFBFS",gxz:"ZV12ContagemResultado_PFBFS",gxold:"OV12ContagemResultado_PFBFS",gxvar:"AV12ContagemResultado_PFBFS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV12ContagemResultado_PFBFS=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV12ContagemResultado_PFBFS=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vCONTAGEMRESULTADO_PFBFS",gx.O.AV12ContagemResultado_PFBFS,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV12ContagemResultado_PFBFS=this.val()},val:function(){return gx.fn.getDecimalValue("vCONTAGEMRESULTADO_PFBFS",'.',',')},nac:gx.falseFn};
   GXValidFnc[30]={fld:"TEXTBLOCK10", format:0,grid:0};
   GXValidFnc[32]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_PFLFS",gxz:"ZV14ContagemResultado_PFLFS",gxold:"OV14ContagemResultado_PFLFS",gxvar:"AV14ContagemResultado_PFLFS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14ContagemResultado_PFLFS=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV14ContagemResultado_PFLFS=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vCONTAGEMRESULTADO_PFLFS",gx.O.AV14ContagemResultado_PFLFS,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV14ContagemResultado_PFLFS=this.val()},val:function(){return gx.fn.getDecimalValue("vCONTAGEMRESULTADO_PFLFS",'.',',')},nac:gx.falseFn};
   GXValidFnc[35]={fld:"TEXTBLOCK1", format:0,grid:0};
   GXValidFnc[37]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATADAORIGEMCOD",gxz:"ZV27ContratadaOrigemCod",gxold:"OV27ContratadaOrigemCod",gxvar:"AV27ContratadaOrigemCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV27ContratadaOrigemCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV27ContratadaOrigemCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTRATADAORIGEMCOD",gx.O.AV27ContratadaOrigemCod)},c2v:function(){if(this.val()!==undefined)gx.O.AV27ContratadaOrigemCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATADAORIGEMCOD",'.')},nac:gx.falseFn};
   GXValidFnc[40]={fld:"TEXTBLOCK7", format:0,grid:0};
   GXValidFnc[42]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contratada_codigo,isvalid:null,rgrid:[],fld:"vCONTRATADA_CODIGO",gxz:"ZV17Contratada_Codigo",gxold:"OV17Contratada_Codigo",gxvar:"AV17Contratada_Codigo",ucs:[],op:[52,47],ip:[52,47,42],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV17Contratada_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Contratada_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTRATADA_CODIGO",gx.O.AV17Contratada_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV17Contratada_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATADA_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[45]={fld:"TEXTBLOCK6", format:0,grid:0};
   GXValidFnc[47]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_CONTADORFMCOD",gxz:"ZV21ContagemResultado_ContadorFMCod",gxold:"OV21ContagemResultado_ContadorFMCod",gxvar:"AV21ContagemResultado_ContadorFMCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV21ContagemResultado_ContadorFMCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21ContagemResultado_ContadorFMCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTAGEMRESULTADO_CONTADORFMCOD",gx.O.AV21ContagemResultado_ContadorFMCod)},c2v:function(){if(this.val()!==undefined)gx.O.AV21ContagemResultado_ContadorFMCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CONTADORFMCOD",'.')},nac:gx.falseFn};
   GXValidFnc[50]={fld:"TEXTBLOCK3", format:0,grid:0};
   GXValidFnc[52]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_SERVICO",gxz:"ZV15ContagemResultado_Servico",gxold:"OV15ContagemResultado_Servico",gxvar:"AV15ContagemResultado_Servico",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV15ContagemResultado_Servico=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV15ContagemResultado_Servico=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vCONTAGEMRESULTADO_SERVICO",gx.O.AV15ContagemResultado_Servico)},c2v:function(){if(this.val()!==undefined)gx.O.AV15ContagemResultado_Servico=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTAGEMRESULTADO_SERVICO",'.')},nac:gx.falseFn};
   GXValidFnc[55]={fld:"TEXTBLOCK4", format:0,grid:0};
   GXValidFnc[57]={lvl:0,type:"svchar",len:30,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:'e15cf2_client',rgrid:[],fld:"vNEWCONTAGEMRESULTADO_DEMANDA",gxz:"ZV19NewContagemResultado_Demanda",gxold:"OV19NewContagemResultado_Demanda",gxvar:"AV19NewContagemResultado_Demanda",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV19NewContagemResultado_Demanda=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19NewContagemResultado_Demanda=Value},v2c:function(){gx.fn.setControlValue("vNEWCONTAGEMRESULTADO_DEMANDA",gx.O.AV19NewContagemResultado_Demanda,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV19NewContagemResultado_Demanda=this.val()},val:function(){return gx.fn.getControlValue("vNEWCONTAGEMRESULTADO_DEMANDA")},nac:gx.falseFn};
   GXValidFnc[60]={fld:"TEXTBLOCK5", format:0,grid:0};
   GXValidFnc[62]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultado_datacnt,isvalid:'e19cf1_client',rgrid:[],fld:"vCONTAGEMRESULTADO_DATACNT",gxz:"ZV7ContagemResultado_DataCnt",gxold:"OV7ContagemResultado_DataCnt",gxvar:"AV7ContagemResultado_DataCnt",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV7ContagemResultado_DataCnt=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV7ContagemResultado_DataCnt=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vCONTAGEMRESULTADO_DATACNT",gx.O.AV7ContagemResultado_DataCnt,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV7ContagemResultado_DataCnt=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vCONTAGEMRESULTADO_DATACNT")},nac:gx.falseFn};
   this.AV6ContagemResultado_ContratadaCod = 0 ;
   this.ZV6ContagemResultado_ContratadaCod = 0 ;
   this.OV6ContagemResultado_ContratadaCod = 0 ;
   this.AV8ContagemResultado_Demanda = "" ;
   this.ZV8ContagemResultado_Demanda = "" ;
   this.OV8ContagemResultado_Demanda = "" ;
   this.AV28ContagemResultado_ServicoSigla = "" ;
   this.ZV28ContagemResultado_ServicoSigla = "" ;
   this.OV28ContagemResultado_ServicoSigla = "" ;
   this.AV12ContagemResultado_PFBFS = 0 ;
   this.ZV12ContagemResultado_PFBFS = 0 ;
   this.OV12ContagemResultado_PFBFS = 0 ;
   this.AV14ContagemResultado_PFLFS = 0 ;
   this.ZV14ContagemResultado_PFLFS = 0 ;
   this.OV14ContagemResultado_PFLFS = 0 ;
   this.AV27ContratadaOrigemCod = 0 ;
   this.ZV27ContratadaOrigemCod = 0 ;
   this.OV27ContratadaOrigemCod = 0 ;
   this.AV17Contratada_Codigo = 0 ;
   this.ZV17Contratada_Codigo = 0 ;
   this.OV17Contratada_Codigo = 0 ;
   this.AV21ContagemResultado_ContadorFMCod = 0 ;
   this.ZV21ContagemResultado_ContadorFMCod = 0 ;
   this.OV21ContagemResultado_ContadorFMCod = 0 ;
   this.AV15ContagemResultado_Servico = 0 ;
   this.ZV15ContagemResultado_Servico = 0 ;
   this.OV15ContagemResultado_Servico = 0 ;
   this.AV19NewContagemResultado_Demanda = "" ;
   this.ZV19NewContagemResultado_Demanda = "" ;
   this.OV19NewContagemResultado_Demanda = "" ;
   this.AV7ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.ZV7ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.OV7ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.AV6ContagemResultado_ContratadaCod = 0 ;
   this.AV8ContagemResultado_Demanda = "" ;
   this.AV28ContagemResultado_ServicoSigla = "" ;
   this.AV12ContagemResultado_PFBFS = 0 ;
   this.AV14ContagemResultado_PFLFS = 0 ;
   this.AV27ContratadaOrigemCod = 0 ;
   this.AV17Contratada_Codigo = 0 ;
   this.AV21ContagemResultado_ContadorFMCod = 0 ;
   this.AV15ContagemResultado_Servico = 0 ;
   this.AV19NewContagemResultado_Demanda = "" ;
   this.AV7ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.A457ContagemResultado_Demanda = "" ;
   this.A490ContagemResultado_ContratadaCod = 0 ;
   this.A52Contratada_AreaTrabalhoCod = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A801ContagemResultado_ServicoSigla = "" ;
   this.A684ContagemResultado_PFBFSUltima = 0 ;
   this.A685ContagemResultado_PFLFSUltima = 0 ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.A601ContagemResultado_Servico = 0 ;
   this.A69ContratadaUsuario_UsuarioCod = 0 ;
   this.A1228ContratadaUsuario_AreaTrabalhoCod = 0 ;
   this.A66ContratadaUsuario_ContratadaCod = 0 ;
   this.AV20WWPContext = {} ;
   this.AV23Retorno = 0 ;
   this.AV22QuemCadastrou = "" ;
   this.AV5ContagemResultado_Codigo = 0 ;
   this.AV24Sdt_Demanda = {} ;
   this.AV11ContagemResultado_PFBFM = 0 ;
   this.AV13ContagemResultado_PFLFM = 0 ;
   this.AV25Sdt_Demandas = [ ] ;
   this.Events = {"e12cf2_client": ["VCONTAGEMRESULTADO_CONTRATADACOD.CLICK", true] ,"e13cf2_client": ["VCONTAGEMRESULTADO_DEMANDA.ISVALID", true] ,"e14cf2_client": ["VCONTAGEMRESULTADO_SERVICO.CLICK", true] ,"e15cf2_client": ["VNEWCONTAGEMRESULTADO_DEMANDA.ISVALID", true] ,"e16cf2_client": ["ENTER", true] ,"e20cf1_client": ["CANCEL", true] ,"e18cf1_client": ["VCONTAGEMRESULTADO_CONTADORFMCOD.CLICK", false] ,"e19cf1_client": ["VCONTAGEMRESULTADO_DATACNT.ISVALID", false]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["VCONTAGEMRESULTADO_CONTRATADACOD.CLICK"] = [[{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}],[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28ContagemResultado_ServicoSigla',fld:'vCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}]];
   this.EvtParms["VCONTAGEMRESULTADO_DEMANDA.ISVALID"] = [[{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}],[{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContagemResultado_PFBFS',fld:'vCONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV14ContagemResultado_PFLFS',fld:'vCONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV28ContagemResultado_ServicoSigla',fld:'vCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]];
   this.EvtParms["VCONTAGEMRESULTADO_SERVICO.CLICK"] = [[{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{ctrl:'vCONTAGEMRESULTADO_SERVICO'}],[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}]];
   this.EvtParms["VNEWCONTAGEMRESULTADO_DEMANDA.ISVALID"] = [[{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23Retorno',fld:'vRETORNO',pic:'ZZZ9',nv:0},{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV22QuemCadastrou',fld:'vQUEMCADASTROU',pic:'',nv:''}],[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]];
   this.EvtParms["VCONTAGEMRESULTADO_CONTADORFMCOD.CLICK"] = [[{av:'AV21ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0}],[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]];
   this.EvtParms["VCONTAGEMRESULTADO_DATACNT.ISVALID"] = [[{av:'AV7ContagemResultado_DataCnt',fld:'vCONTAGEMRESULTADO_DATACNT',pic:'',nv:''}],[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'}]];
   this.EvtParms["ENTER"] = [[{av:'AV22QuemCadastrou',fld:'vQUEMCADASTROU',pic:'',nv:''},{av:'AV23Retorno',fld:'vRETORNO',pic:'ZZZ9',nv:0},{av:'AV20WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV19NewContagemResultado_Demanda',fld:'vNEWCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV6ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV8ContagemResultado_Demanda',fld:'vCONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV15ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV21ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_DataCnt',fld:'vCONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'AV17Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Sdt_Demanda',fld:'vSDT_DEMANDA',pic:'',nv:null},{av:'AV27ContratadaOrigemCod',fld:'vCONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'AV11ContagemResultado_PFBFM',fld:'vCONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV13ContagemResultado_PFLFM',fld:'vCONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV25Sdt_Demandas',fld:'vSDT_DEMANDAS',pic:'',nv:null}],[{ctrl:'BTNENTER',prop:'Enabled'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{av:'AV5ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24Sdt_Demanda',fld:'vSDT_DEMANDA',pic:'',nv:null},{av:'AV25Sdt_Demandas',fld:'vSDT_DEMANDAS',pic:'',nv:null}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV20WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A52Contratada_AreaTrabalhoCod", "CONTRATADA_AREATRABALHOCOD", 0, "int");
   this.setVCMap("A490ContagemResultado_ContratadaCod", "CONTAGEMRESULTADO_CONTRATADACOD", 0, "int");
   this.setVCMap("A457ContagemResultado_Demanda", "CONTAGEMRESULTADO_DEMANDA", 0, "svchar");
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("A684ContagemResultado_PFBFSUltima", "CONTAGEMRESULTADO_PFBFSULTIMA", 0, "decimal");
   this.setVCMap("A685ContagemResultado_PFLFSUltima", "CONTAGEMRESULTADO_PFLFSULTIMA", 0, "decimal");
   this.setVCMap("A801ContagemResultado_ServicoSigla", "CONTAGEMRESULTADO_SERVICOSIGLA", 0, "char");
   this.setVCMap("AV23Retorno", "vRETORNO", 0, "int");
   this.setVCMap("AV22QuemCadastrou", "vQUEMCADASTROU", 0, "char");
   this.setVCMap("AV5ContagemResultado_Codigo", "vCONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV24Sdt_Demanda", "vSDT_DEMANDA", 0, "SDT_Demandas.Demanda");
   this.setVCMap("AV11ContagemResultado_PFBFM", "vCONTAGEMRESULTADO_PFBFM", 0, "decimal");
   this.setVCMap("AV13ContagemResultado_PFLFM", "vCONTAGEMRESULTADO_PFLFM", 0, "decimal");
   this.setVCMap("AV25Sdt_Demandas", "vSDT_DEMANDAS", 0, "CollSDT_Demandas.Demanda");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_osvinculada);
