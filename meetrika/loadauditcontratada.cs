/*
               File: LoadAuditContratada
        Description: Load Audit Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:29.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditcontratada : GXProcedure
   {
      public loadauditcontratada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditcontratada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Contratada_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Contratada_Codigo = aP2_Contratada_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Contratada_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditcontratada objloadauditcontratada;
         objloadauditcontratada = new loadauditcontratada();
         objloadauditcontratada.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditcontratada.AV10AuditingObject = aP1_AuditingObject;
         objloadauditcontratada.AV16Contratada_Codigo = aP2_Contratada_Codigo;
         objloadauditcontratada.AV14ActualMode = aP3_ActualMode;
         objloadauditcontratada.context.SetSubmitInitialConfig(context);
         objloadauditcontratada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditcontratada);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditcontratada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00WH2 */
         pr_default.execute(0, new Object[] {AV16Contratada_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A39Contratada_Codigo = P00WH2_A39Contratada_Codigo[0];
            A1128Contratada_LogoNomeArq = P00WH2_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = P00WH2_n1128Contratada_LogoNomeArq[0];
            A1129Contratada_LogoTipoArq = P00WH2_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = P00WH2_n1129Contratada_LogoTipoArq[0];
            A52Contratada_AreaTrabalhoCod = P00WH2_A52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P00WH2_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00WH2_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = P00WH2_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = P00WH2_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = P00WH2_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = P00WH2_n1595Contratada_AreaTrbSrvPdr[0];
            A40Contratada_PessoaCod = P00WH2_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00WH2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WH2_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00WH2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WH2_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P00WH2_A518Pessoa_IE[0];
            n518Pessoa_IE = P00WH2_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P00WH2_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00WH2_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P00WH2_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00WH2_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P00WH2_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00WH2_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P00WH2_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00WH2_n523Pessoa_Fax[0];
            A438Contratada_Sigla = P00WH2_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = P00WH2_A516Contratada_TipoFabrica[0];
            A342Contratada_BancoNome = P00WH2_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = P00WH2_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = P00WH2_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = P00WH2_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = P00WH2_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = P00WH2_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = P00WH2_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = P00WH2_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = P00WH2_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = P00WH2_n51Contratada_ContaCorrente[0];
            A349Contratada_MunicipioCod = P00WH2_A349Contratada_MunicipioCod[0];
            n349Contratada_MunicipioCod = P00WH2_n349Contratada_MunicipioCod[0];
            A350Contratada_UF = P00WH2_A350Contratada_UF[0];
            n350Contratada_UF = P00WH2_n350Contratada_UF[0];
            A524Contratada_OS = P00WH2_A524Contratada_OS[0];
            n524Contratada_OS = P00WH2_n524Contratada_OS[0];
            A1451Contratada_SS = P00WH2_A1451Contratada_SS[0];
            n1451Contratada_SS = P00WH2_n1451Contratada_SS[0];
            A530Contratada_Lote = P00WH2_A530Contratada_Lote[0];
            n530Contratada_Lote = P00WH2_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = P00WH2_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = P00WH2_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = P00WH2_A1867Contratada_OSPreferencial[0];
            n1867Contratada_OSPreferencial = P00WH2_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = P00WH2_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = P00WH2_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = P00WH2_A43Contratada_Ativo[0];
            A1127Contratada_LogoArquivo = P00WH2_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = P00WH2_n1127Contratada_LogoArquivo[0];
            A53Contratada_AreaTrabalhoDes = P00WH2_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00WH2_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = P00WH2_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = P00WH2_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = P00WH2_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = P00WH2_n1595Contratada_AreaTrbSrvPdr[0];
            A41Contratada_PessoaNom = P00WH2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WH2_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00WH2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WH2_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P00WH2_A518Pessoa_IE[0];
            n518Pessoa_IE = P00WH2_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P00WH2_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00WH2_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P00WH2_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00WH2_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P00WH2_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00WH2_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P00WH2_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00WH2_n523Pessoa_Fax[0];
            A350Contratada_UF = P00WH2_A350Contratada_UF[0];
            n350Contratada_UF = P00WH2_n350Contratada_UF[0];
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Contratada";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrabalhoDes";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A53Contratada_AreaTrabalhoDes;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrbClcPFnl";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�cluclo P Final";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1592Contratada_AreaTrbClcPFnl;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrbSrvPdr";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o padr�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A41Contratada_PessoaNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCNPJ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = " CNPJ da Pessoa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A42Contratada_PessoaCNPJ;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_IE";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inscri��o Estadual";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A518Pessoa_IE;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_Endereco";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Endere�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A519Pessoa_Endereco;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_CEP";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CEP";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A521Pessoa_CEP;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_Telefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A522Pessoa_Telefone;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_Fax";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fax";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A523Pessoa_Fax;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A438Contratada_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_TipoFabrica";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A516Contratada_TipoFabrica;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_BancoNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Banco";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A342Contratada_BancoNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_BancoNro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A343Contratada_BancoNro;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AgenciaNome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ag�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A344Contratada_AgenciaNome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AgenciaNro";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A345Contratada_AgenciaNro;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_ContaCorrente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Conta Corrente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A51Contratada_ContaCorrente;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_MunicipioCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Municipio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_UF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A350Contratada_UF;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_OS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�ltima OS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_SS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�ltima SS";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Lote";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�ltimo Lote";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_LogoArquivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Logomarca";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1127Contratada_LogoArquivo;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_LogoNomeArq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome de arquivo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1128Contratada_LogoNomeArq;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_LogoTipoArq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1129Contratada_LogoTipoArq;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_UsaOSistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usa o Sistema";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1481Contratada_UsaOSistema);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_OSPreferencial";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preferencial";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1867Contratada_OSPreferencial);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_CntPadrao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato padr�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A43Contratada_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00WH3 */
         pr_default.execute(1, new Object[] {AV16Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1128Contratada_LogoNomeArq = P00WH3_A1128Contratada_LogoNomeArq[0];
            n1128Contratada_LogoNomeArq = P00WH3_n1128Contratada_LogoNomeArq[0];
            A1129Contratada_LogoTipoArq = P00WH3_A1129Contratada_LogoTipoArq[0];
            n1129Contratada_LogoTipoArq = P00WH3_n1129Contratada_LogoTipoArq[0];
            A39Contratada_Codigo = P00WH3_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00WH3_A52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P00WH3_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00WH3_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = P00WH3_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = P00WH3_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = P00WH3_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = P00WH3_n1595Contratada_AreaTrbSrvPdr[0];
            A40Contratada_PessoaCod = P00WH3_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00WH3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WH3_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00WH3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WH3_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P00WH3_A518Pessoa_IE[0];
            n518Pessoa_IE = P00WH3_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P00WH3_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00WH3_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P00WH3_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00WH3_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P00WH3_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00WH3_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P00WH3_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00WH3_n523Pessoa_Fax[0];
            A438Contratada_Sigla = P00WH3_A438Contratada_Sigla[0];
            A516Contratada_TipoFabrica = P00WH3_A516Contratada_TipoFabrica[0];
            A342Contratada_BancoNome = P00WH3_A342Contratada_BancoNome[0];
            n342Contratada_BancoNome = P00WH3_n342Contratada_BancoNome[0];
            A343Contratada_BancoNro = P00WH3_A343Contratada_BancoNro[0];
            n343Contratada_BancoNro = P00WH3_n343Contratada_BancoNro[0];
            A344Contratada_AgenciaNome = P00WH3_A344Contratada_AgenciaNome[0];
            n344Contratada_AgenciaNome = P00WH3_n344Contratada_AgenciaNome[0];
            A345Contratada_AgenciaNro = P00WH3_A345Contratada_AgenciaNro[0];
            n345Contratada_AgenciaNro = P00WH3_n345Contratada_AgenciaNro[0];
            A51Contratada_ContaCorrente = P00WH3_A51Contratada_ContaCorrente[0];
            n51Contratada_ContaCorrente = P00WH3_n51Contratada_ContaCorrente[0];
            A349Contratada_MunicipioCod = P00WH3_A349Contratada_MunicipioCod[0];
            n349Contratada_MunicipioCod = P00WH3_n349Contratada_MunicipioCod[0];
            A350Contratada_UF = P00WH3_A350Contratada_UF[0];
            n350Contratada_UF = P00WH3_n350Contratada_UF[0];
            A524Contratada_OS = P00WH3_A524Contratada_OS[0];
            n524Contratada_OS = P00WH3_n524Contratada_OS[0];
            A1451Contratada_SS = P00WH3_A1451Contratada_SS[0];
            n1451Contratada_SS = P00WH3_n1451Contratada_SS[0];
            A530Contratada_Lote = P00WH3_A530Contratada_Lote[0];
            n530Contratada_Lote = P00WH3_n530Contratada_Lote[0];
            A1481Contratada_UsaOSistema = P00WH3_A1481Contratada_UsaOSistema[0];
            n1481Contratada_UsaOSistema = P00WH3_n1481Contratada_UsaOSistema[0];
            A1867Contratada_OSPreferencial = P00WH3_A1867Contratada_OSPreferencial[0];
            n1867Contratada_OSPreferencial = P00WH3_n1867Contratada_OSPreferencial[0];
            A1953Contratada_CntPadrao = P00WH3_A1953Contratada_CntPadrao[0];
            n1953Contratada_CntPadrao = P00WH3_n1953Contratada_CntPadrao[0];
            A43Contratada_Ativo = P00WH3_A43Contratada_Ativo[0];
            A1127Contratada_LogoArquivo = P00WH3_A1127Contratada_LogoArquivo[0];
            n1127Contratada_LogoArquivo = P00WH3_n1127Contratada_LogoArquivo[0];
            A53Contratada_AreaTrabalhoDes = P00WH3_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00WH3_n53Contratada_AreaTrabalhoDes[0];
            A1592Contratada_AreaTrbClcPFnl = P00WH3_A1592Contratada_AreaTrbClcPFnl[0];
            n1592Contratada_AreaTrbClcPFnl = P00WH3_n1592Contratada_AreaTrbClcPFnl[0];
            A1595Contratada_AreaTrbSrvPdr = P00WH3_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = P00WH3_n1595Contratada_AreaTrbSrvPdr[0];
            A41Contratada_PessoaNom = P00WH3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00WH3_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00WH3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00WH3_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P00WH3_A518Pessoa_IE[0];
            n518Pessoa_IE = P00WH3_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P00WH3_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P00WH3_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P00WH3_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P00WH3_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P00WH3_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P00WH3_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P00WH3_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P00WH3_n523Pessoa_Fax[0];
            A350Contratada_UF = P00WH3_A350Contratada_UF[0];
            n350Contratada_UF = P00WH3_n350Contratada_UF[0];
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Contratada";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrabalhoDes";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�rea de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A53Contratada_AreaTrabalhoDes;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrbClcPFnl";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�cluclo P Final";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1592Contratada_AreaTrbClcPFnl;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AreaTrbSrvPdr";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o padr�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A41Contratada_PessoaNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_PessoaCNPJ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = " CNPJ da Pessoa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A42Contratada_PessoaCNPJ;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_IE";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Inscri��o Estadual";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A518Pessoa_IE;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_Endereco";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Endere�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A519Pessoa_Endereco;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_CEP";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "CEP";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A521Pessoa_CEP;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_Telefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Telefone";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A522Pessoa_Telefone;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Pessoa_Fax";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Fax";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A523Pessoa_Fax;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A438Contratada_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_TipoFabrica";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A516Contratada_TipoFabrica;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_BancoNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Banco";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A342Contratada_BancoNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_BancoNro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A343Contratada_BancoNro;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AgenciaNome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ag�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A344Contratada_AgenciaNome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_AgenciaNro";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A345Contratada_AgenciaNro;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_ContaCorrente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Conta Corrente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A51Contratada_ContaCorrente;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_MunicipioCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Municipio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_UF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Estado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A350Contratada_UF;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_OS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�ltima OS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_SS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�ltima SS";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Lote";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "�ltimo Lote";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_LogoArquivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Logomarca";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1127Contratada_LogoArquivo;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_LogoNomeArq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome de arquivo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1128Contratada_LogoNomeArq;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_LogoTipoArq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1129Contratada_LogoTipoArq;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_UsaOSistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usa o Sistema";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1481Contratada_UsaOSistema);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_OSPreferencial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Preferencial";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1867Contratada_OSPreferencial);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_CntPadrao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contrato padr�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Contratada_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A43Contratada_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00WH3_A39Contratada_Codigo[0] == A39Contratada_Codigo ) )
                  {
                     A1128Contratada_LogoNomeArq = P00WH3_A1128Contratada_LogoNomeArq[0];
                     n1128Contratada_LogoNomeArq = P00WH3_n1128Contratada_LogoNomeArq[0];
                     A1129Contratada_LogoTipoArq = P00WH3_A1129Contratada_LogoTipoArq[0];
                     n1129Contratada_LogoTipoArq = P00WH3_n1129Contratada_LogoTipoArq[0];
                     A52Contratada_AreaTrabalhoCod = P00WH3_A52Contratada_AreaTrabalhoCod[0];
                     A53Contratada_AreaTrabalhoDes = P00WH3_A53Contratada_AreaTrabalhoDes[0];
                     n53Contratada_AreaTrabalhoDes = P00WH3_n53Contratada_AreaTrabalhoDes[0];
                     A1592Contratada_AreaTrbClcPFnl = P00WH3_A1592Contratada_AreaTrbClcPFnl[0];
                     n1592Contratada_AreaTrbClcPFnl = P00WH3_n1592Contratada_AreaTrbClcPFnl[0];
                     A1595Contratada_AreaTrbSrvPdr = P00WH3_A1595Contratada_AreaTrbSrvPdr[0];
                     n1595Contratada_AreaTrbSrvPdr = P00WH3_n1595Contratada_AreaTrbSrvPdr[0];
                     A40Contratada_PessoaCod = P00WH3_A40Contratada_PessoaCod[0];
                     A41Contratada_PessoaNom = P00WH3_A41Contratada_PessoaNom[0];
                     n41Contratada_PessoaNom = P00WH3_n41Contratada_PessoaNom[0];
                     A42Contratada_PessoaCNPJ = P00WH3_A42Contratada_PessoaCNPJ[0];
                     n42Contratada_PessoaCNPJ = P00WH3_n42Contratada_PessoaCNPJ[0];
                     A518Pessoa_IE = P00WH3_A518Pessoa_IE[0];
                     n518Pessoa_IE = P00WH3_n518Pessoa_IE[0];
                     A519Pessoa_Endereco = P00WH3_A519Pessoa_Endereco[0];
                     n519Pessoa_Endereco = P00WH3_n519Pessoa_Endereco[0];
                     A521Pessoa_CEP = P00WH3_A521Pessoa_CEP[0];
                     n521Pessoa_CEP = P00WH3_n521Pessoa_CEP[0];
                     A522Pessoa_Telefone = P00WH3_A522Pessoa_Telefone[0];
                     n522Pessoa_Telefone = P00WH3_n522Pessoa_Telefone[0];
                     A523Pessoa_Fax = P00WH3_A523Pessoa_Fax[0];
                     n523Pessoa_Fax = P00WH3_n523Pessoa_Fax[0];
                     A438Contratada_Sigla = P00WH3_A438Contratada_Sigla[0];
                     A516Contratada_TipoFabrica = P00WH3_A516Contratada_TipoFabrica[0];
                     A342Contratada_BancoNome = P00WH3_A342Contratada_BancoNome[0];
                     n342Contratada_BancoNome = P00WH3_n342Contratada_BancoNome[0];
                     A343Contratada_BancoNro = P00WH3_A343Contratada_BancoNro[0];
                     n343Contratada_BancoNro = P00WH3_n343Contratada_BancoNro[0];
                     A344Contratada_AgenciaNome = P00WH3_A344Contratada_AgenciaNome[0];
                     n344Contratada_AgenciaNome = P00WH3_n344Contratada_AgenciaNome[0];
                     A345Contratada_AgenciaNro = P00WH3_A345Contratada_AgenciaNro[0];
                     n345Contratada_AgenciaNro = P00WH3_n345Contratada_AgenciaNro[0];
                     A51Contratada_ContaCorrente = P00WH3_A51Contratada_ContaCorrente[0];
                     n51Contratada_ContaCorrente = P00WH3_n51Contratada_ContaCorrente[0];
                     A349Contratada_MunicipioCod = P00WH3_A349Contratada_MunicipioCod[0];
                     n349Contratada_MunicipioCod = P00WH3_n349Contratada_MunicipioCod[0];
                     A350Contratada_UF = P00WH3_A350Contratada_UF[0];
                     n350Contratada_UF = P00WH3_n350Contratada_UF[0];
                     A524Contratada_OS = P00WH3_A524Contratada_OS[0];
                     n524Contratada_OS = P00WH3_n524Contratada_OS[0];
                     A1451Contratada_SS = P00WH3_A1451Contratada_SS[0];
                     n1451Contratada_SS = P00WH3_n1451Contratada_SS[0];
                     A530Contratada_Lote = P00WH3_A530Contratada_Lote[0];
                     n530Contratada_Lote = P00WH3_n530Contratada_Lote[0];
                     A1481Contratada_UsaOSistema = P00WH3_A1481Contratada_UsaOSistema[0];
                     n1481Contratada_UsaOSistema = P00WH3_n1481Contratada_UsaOSistema[0];
                     A1867Contratada_OSPreferencial = P00WH3_A1867Contratada_OSPreferencial[0];
                     n1867Contratada_OSPreferencial = P00WH3_n1867Contratada_OSPreferencial[0];
                     A1953Contratada_CntPadrao = P00WH3_A1953Contratada_CntPadrao[0];
                     n1953Contratada_CntPadrao = P00WH3_n1953Contratada_CntPadrao[0];
                     A43Contratada_Ativo = P00WH3_A43Contratada_Ativo[0];
                     A1127Contratada_LogoArquivo = P00WH3_A1127Contratada_LogoArquivo[0];
                     n1127Contratada_LogoArquivo = P00WH3_n1127Contratada_LogoArquivo[0];
                     A53Contratada_AreaTrabalhoDes = P00WH3_A53Contratada_AreaTrabalhoDes[0];
                     n53Contratada_AreaTrabalhoDes = P00WH3_n53Contratada_AreaTrabalhoDes[0];
                     A1592Contratada_AreaTrbClcPFnl = P00WH3_A1592Contratada_AreaTrbClcPFnl[0];
                     n1592Contratada_AreaTrbClcPFnl = P00WH3_n1592Contratada_AreaTrbClcPFnl[0];
                     A1595Contratada_AreaTrbSrvPdr = P00WH3_A1595Contratada_AreaTrbSrvPdr[0];
                     n1595Contratada_AreaTrbSrvPdr = P00WH3_n1595Contratada_AreaTrbSrvPdr[0];
                     A41Contratada_PessoaNom = P00WH3_A41Contratada_PessoaNom[0];
                     n41Contratada_PessoaNom = P00WH3_n41Contratada_PessoaNom[0];
                     A42Contratada_PessoaCNPJ = P00WH3_A42Contratada_PessoaCNPJ[0];
                     n42Contratada_PessoaCNPJ = P00WH3_n42Contratada_PessoaCNPJ[0];
                     A518Pessoa_IE = P00WH3_A518Pessoa_IE[0];
                     n518Pessoa_IE = P00WH3_n518Pessoa_IE[0];
                     A519Pessoa_Endereco = P00WH3_A519Pessoa_Endereco[0];
                     n519Pessoa_Endereco = P00WH3_n519Pessoa_Endereco[0];
                     A521Pessoa_CEP = P00WH3_A521Pessoa_CEP[0];
                     n521Pessoa_CEP = P00WH3_n521Pessoa_CEP[0];
                     A522Pessoa_Telefone = P00WH3_A522Pessoa_Telefone[0];
                     n522Pessoa_Telefone = P00WH3_n522Pessoa_Telefone[0];
                     A523Pessoa_Fax = P00WH3_A523Pessoa_Fax[0];
                     n523Pessoa_Fax = P00WH3_n523Pessoa_Fax[0];
                     A350Contratada_UF = P00WH3_A350Contratada_UF[0];
                     n350Contratada_UF = P00WH3_n350Contratada_UF[0];
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_AreaTrabalhoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_AreaTrabalhoDes") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A53Contratada_AreaTrabalhoDes;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_AreaTrbClcPFnl") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1592Contratada_AreaTrbClcPFnl;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_AreaTrbSrvPdr") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A41Contratada_PessoaNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_PessoaCNPJ") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A42Contratada_PessoaCNPJ;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Pessoa_IE") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A518Pessoa_IE;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Pessoa_Endereco") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A519Pessoa_Endereco;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Pessoa_CEP") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A521Pessoa_CEP;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Pessoa_Telefone") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A522Pessoa_Telefone;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Pessoa_Fax") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A523Pessoa_Fax;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_Sigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A438Contratada_Sigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_TipoFabrica") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A516Contratada_TipoFabrica;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_BancoNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A342Contratada_BancoNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_BancoNro") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A343Contratada_BancoNro;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_AgenciaNome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A344Contratada_AgenciaNome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_AgenciaNro") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A345Contratada_AgenciaNro;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_ContaCorrente") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A51Contratada_ContaCorrente;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_MunicipioCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_UF") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A350Contratada_UF;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_OS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A524Contratada_OS), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_SS") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1451Contratada_SS), 8, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_Lote") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A530Contratada_Lote), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_LogoArquivo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1127Contratada_LogoArquivo;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_LogoNomeArq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1128Contratada_LogoNomeArq;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_LogoTipoArq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1129Contratada_LogoTipoArq;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_UsaOSistema") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1481Contratada_UsaOSistema);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_OSPreferencial") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1867Contratada_OSPreferencial);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_CntPadrao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1953Contratada_CntPadrao), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Contratada_Ativo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A43Contratada_Ativo);
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00WH2_A39Contratada_Codigo = new int[1] ;
         P00WH2_A1128Contratada_LogoNomeArq = new String[] {""} ;
         P00WH2_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         P00WH2_A1129Contratada_LogoTipoArq = new String[] {""} ;
         P00WH2_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         P00WH2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00WH2_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00WH2_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00WH2_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         P00WH2_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         P00WH2_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         P00WH2_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         P00WH2_A40Contratada_PessoaCod = new int[1] ;
         P00WH2_A41Contratada_PessoaNom = new String[] {""} ;
         P00WH2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00WH2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00WH2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00WH2_A518Pessoa_IE = new String[] {""} ;
         P00WH2_n518Pessoa_IE = new bool[] {false} ;
         P00WH2_A519Pessoa_Endereco = new String[] {""} ;
         P00WH2_n519Pessoa_Endereco = new bool[] {false} ;
         P00WH2_A521Pessoa_CEP = new String[] {""} ;
         P00WH2_n521Pessoa_CEP = new bool[] {false} ;
         P00WH2_A522Pessoa_Telefone = new String[] {""} ;
         P00WH2_n522Pessoa_Telefone = new bool[] {false} ;
         P00WH2_A523Pessoa_Fax = new String[] {""} ;
         P00WH2_n523Pessoa_Fax = new bool[] {false} ;
         P00WH2_A438Contratada_Sigla = new String[] {""} ;
         P00WH2_A516Contratada_TipoFabrica = new String[] {""} ;
         P00WH2_A342Contratada_BancoNome = new String[] {""} ;
         P00WH2_n342Contratada_BancoNome = new bool[] {false} ;
         P00WH2_A343Contratada_BancoNro = new String[] {""} ;
         P00WH2_n343Contratada_BancoNro = new bool[] {false} ;
         P00WH2_A344Contratada_AgenciaNome = new String[] {""} ;
         P00WH2_n344Contratada_AgenciaNome = new bool[] {false} ;
         P00WH2_A345Contratada_AgenciaNro = new String[] {""} ;
         P00WH2_n345Contratada_AgenciaNro = new bool[] {false} ;
         P00WH2_A51Contratada_ContaCorrente = new String[] {""} ;
         P00WH2_n51Contratada_ContaCorrente = new bool[] {false} ;
         P00WH2_A349Contratada_MunicipioCod = new int[1] ;
         P00WH2_n349Contratada_MunicipioCod = new bool[] {false} ;
         P00WH2_A350Contratada_UF = new String[] {""} ;
         P00WH2_n350Contratada_UF = new bool[] {false} ;
         P00WH2_A524Contratada_OS = new int[1] ;
         P00WH2_n524Contratada_OS = new bool[] {false} ;
         P00WH2_A1451Contratada_SS = new int[1] ;
         P00WH2_n1451Contratada_SS = new bool[] {false} ;
         P00WH2_A530Contratada_Lote = new short[1] ;
         P00WH2_n530Contratada_Lote = new bool[] {false} ;
         P00WH2_A1481Contratada_UsaOSistema = new bool[] {false} ;
         P00WH2_n1481Contratada_UsaOSistema = new bool[] {false} ;
         P00WH2_A1867Contratada_OSPreferencial = new bool[] {false} ;
         P00WH2_n1867Contratada_OSPreferencial = new bool[] {false} ;
         P00WH2_A1953Contratada_CntPadrao = new int[1] ;
         P00WH2_n1953Contratada_CntPadrao = new bool[] {false} ;
         P00WH2_A43Contratada_Ativo = new bool[] {false} ;
         P00WH2_A1127Contratada_LogoArquivo = new String[] {""} ;
         P00WH2_n1127Contratada_LogoArquivo = new bool[] {false} ;
         A1128Contratada_LogoNomeArq = "";
         A1129Contratada_LogoTipoArq = "";
         A53Contratada_AreaTrabalhoDes = "";
         A1592Contratada_AreaTrbClcPFnl = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         A438Contratada_Sigla = "";
         A516Contratada_TipoFabrica = "";
         A342Contratada_BancoNome = "";
         A343Contratada_BancoNro = "";
         A344Contratada_AgenciaNome = "";
         A345Contratada_AgenciaNro = "";
         A51Contratada_ContaCorrente = "";
         A350Contratada_UF = "";
         A1127Contratada_LogoArquivo = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00WH3_A1128Contratada_LogoNomeArq = new String[] {""} ;
         P00WH3_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         P00WH3_A1129Contratada_LogoTipoArq = new String[] {""} ;
         P00WH3_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         P00WH3_A39Contratada_Codigo = new int[1] ;
         P00WH3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00WH3_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00WH3_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00WH3_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         P00WH3_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         P00WH3_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         P00WH3_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         P00WH3_A40Contratada_PessoaCod = new int[1] ;
         P00WH3_A41Contratada_PessoaNom = new String[] {""} ;
         P00WH3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00WH3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00WH3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00WH3_A518Pessoa_IE = new String[] {""} ;
         P00WH3_n518Pessoa_IE = new bool[] {false} ;
         P00WH3_A519Pessoa_Endereco = new String[] {""} ;
         P00WH3_n519Pessoa_Endereco = new bool[] {false} ;
         P00WH3_A521Pessoa_CEP = new String[] {""} ;
         P00WH3_n521Pessoa_CEP = new bool[] {false} ;
         P00WH3_A522Pessoa_Telefone = new String[] {""} ;
         P00WH3_n522Pessoa_Telefone = new bool[] {false} ;
         P00WH3_A523Pessoa_Fax = new String[] {""} ;
         P00WH3_n523Pessoa_Fax = new bool[] {false} ;
         P00WH3_A438Contratada_Sigla = new String[] {""} ;
         P00WH3_A516Contratada_TipoFabrica = new String[] {""} ;
         P00WH3_A342Contratada_BancoNome = new String[] {""} ;
         P00WH3_n342Contratada_BancoNome = new bool[] {false} ;
         P00WH3_A343Contratada_BancoNro = new String[] {""} ;
         P00WH3_n343Contratada_BancoNro = new bool[] {false} ;
         P00WH3_A344Contratada_AgenciaNome = new String[] {""} ;
         P00WH3_n344Contratada_AgenciaNome = new bool[] {false} ;
         P00WH3_A345Contratada_AgenciaNro = new String[] {""} ;
         P00WH3_n345Contratada_AgenciaNro = new bool[] {false} ;
         P00WH3_A51Contratada_ContaCorrente = new String[] {""} ;
         P00WH3_n51Contratada_ContaCorrente = new bool[] {false} ;
         P00WH3_A349Contratada_MunicipioCod = new int[1] ;
         P00WH3_n349Contratada_MunicipioCod = new bool[] {false} ;
         P00WH3_A350Contratada_UF = new String[] {""} ;
         P00WH3_n350Contratada_UF = new bool[] {false} ;
         P00WH3_A524Contratada_OS = new int[1] ;
         P00WH3_n524Contratada_OS = new bool[] {false} ;
         P00WH3_A1451Contratada_SS = new int[1] ;
         P00WH3_n1451Contratada_SS = new bool[] {false} ;
         P00WH3_A530Contratada_Lote = new short[1] ;
         P00WH3_n530Contratada_Lote = new bool[] {false} ;
         P00WH3_A1481Contratada_UsaOSistema = new bool[] {false} ;
         P00WH3_n1481Contratada_UsaOSistema = new bool[] {false} ;
         P00WH3_A1867Contratada_OSPreferencial = new bool[] {false} ;
         P00WH3_n1867Contratada_OSPreferencial = new bool[] {false} ;
         P00WH3_A1953Contratada_CntPadrao = new int[1] ;
         P00WH3_n1953Contratada_CntPadrao = new bool[] {false} ;
         P00WH3_A43Contratada_Ativo = new bool[] {false} ;
         P00WH3_A1127Contratada_LogoArquivo = new String[] {""} ;
         P00WH3_n1127Contratada_LogoArquivo = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditcontratada__default(),
            new Object[][] {
                new Object[] {
               P00WH2_A39Contratada_Codigo, P00WH2_A1128Contratada_LogoNomeArq, P00WH2_n1128Contratada_LogoNomeArq, P00WH2_A1129Contratada_LogoTipoArq, P00WH2_n1129Contratada_LogoTipoArq, P00WH2_A52Contratada_AreaTrabalhoCod, P00WH2_A53Contratada_AreaTrabalhoDes, P00WH2_n53Contratada_AreaTrabalhoDes, P00WH2_A1592Contratada_AreaTrbClcPFnl, P00WH2_n1592Contratada_AreaTrbClcPFnl,
               P00WH2_A1595Contratada_AreaTrbSrvPdr, P00WH2_n1595Contratada_AreaTrbSrvPdr, P00WH2_A40Contratada_PessoaCod, P00WH2_A41Contratada_PessoaNom, P00WH2_n41Contratada_PessoaNom, P00WH2_A42Contratada_PessoaCNPJ, P00WH2_n42Contratada_PessoaCNPJ, P00WH2_A518Pessoa_IE, P00WH2_n518Pessoa_IE, P00WH2_A519Pessoa_Endereco,
               P00WH2_n519Pessoa_Endereco, P00WH2_A521Pessoa_CEP, P00WH2_n521Pessoa_CEP, P00WH2_A522Pessoa_Telefone, P00WH2_n522Pessoa_Telefone, P00WH2_A523Pessoa_Fax, P00WH2_n523Pessoa_Fax, P00WH2_A438Contratada_Sigla, P00WH2_A516Contratada_TipoFabrica, P00WH2_A342Contratada_BancoNome,
               P00WH2_n342Contratada_BancoNome, P00WH2_A343Contratada_BancoNro, P00WH2_n343Contratada_BancoNro, P00WH2_A344Contratada_AgenciaNome, P00WH2_n344Contratada_AgenciaNome, P00WH2_A345Contratada_AgenciaNro, P00WH2_n345Contratada_AgenciaNro, P00WH2_A51Contratada_ContaCorrente, P00WH2_n51Contratada_ContaCorrente, P00WH2_A349Contratada_MunicipioCod,
               P00WH2_n349Contratada_MunicipioCod, P00WH2_A350Contratada_UF, P00WH2_n350Contratada_UF, P00WH2_A524Contratada_OS, P00WH2_n524Contratada_OS, P00WH2_A1451Contratada_SS, P00WH2_n1451Contratada_SS, P00WH2_A530Contratada_Lote, P00WH2_n530Contratada_Lote, P00WH2_A1481Contratada_UsaOSistema,
               P00WH2_n1481Contratada_UsaOSistema, P00WH2_A1867Contratada_OSPreferencial, P00WH2_n1867Contratada_OSPreferencial, P00WH2_A1953Contratada_CntPadrao, P00WH2_n1953Contratada_CntPadrao, P00WH2_A43Contratada_Ativo, P00WH2_A1127Contratada_LogoArquivo, P00WH2_n1127Contratada_LogoArquivo
               }
               , new Object[] {
               P00WH3_A1128Contratada_LogoNomeArq, P00WH3_n1128Contratada_LogoNomeArq, P00WH3_A1129Contratada_LogoTipoArq, P00WH3_n1129Contratada_LogoTipoArq, P00WH3_A39Contratada_Codigo, P00WH3_A52Contratada_AreaTrabalhoCod, P00WH3_A53Contratada_AreaTrabalhoDes, P00WH3_n53Contratada_AreaTrabalhoDes, P00WH3_A1592Contratada_AreaTrbClcPFnl, P00WH3_n1592Contratada_AreaTrbClcPFnl,
               P00WH3_A1595Contratada_AreaTrbSrvPdr, P00WH3_n1595Contratada_AreaTrbSrvPdr, P00WH3_A40Contratada_PessoaCod, P00WH3_A41Contratada_PessoaNom, P00WH3_n41Contratada_PessoaNom, P00WH3_A42Contratada_PessoaCNPJ, P00WH3_n42Contratada_PessoaCNPJ, P00WH3_A518Pessoa_IE, P00WH3_n518Pessoa_IE, P00WH3_A519Pessoa_Endereco,
               P00WH3_n519Pessoa_Endereco, P00WH3_A521Pessoa_CEP, P00WH3_n521Pessoa_CEP, P00WH3_A522Pessoa_Telefone, P00WH3_n522Pessoa_Telefone, P00WH3_A523Pessoa_Fax, P00WH3_n523Pessoa_Fax, P00WH3_A438Contratada_Sigla, P00WH3_A516Contratada_TipoFabrica, P00WH3_A342Contratada_BancoNome,
               P00WH3_n342Contratada_BancoNome, P00WH3_A343Contratada_BancoNro, P00WH3_n343Contratada_BancoNro, P00WH3_A344Contratada_AgenciaNome, P00WH3_n344Contratada_AgenciaNome, P00WH3_A345Contratada_AgenciaNro, P00WH3_n345Contratada_AgenciaNro, P00WH3_A51Contratada_ContaCorrente, P00WH3_n51Contratada_ContaCorrente, P00WH3_A349Contratada_MunicipioCod,
               P00WH3_n349Contratada_MunicipioCod, P00WH3_A350Contratada_UF, P00WH3_n350Contratada_UF, P00WH3_A524Contratada_OS, P00WH3_n524Contratada_OS, P00WH3_A1451Contratada_SS, P00WH3_n1451Contratada_SS, P00WH3_A530Contratada_Lote, P00WH3_n530Contratada_Lote, P00WH3_A1481Contratada_UsaOSistema,
               P00WH3_n1481Contratada_UsaOSistema, P00WH3_A1867Contratada_OSPreferencial, P00WH3_n1867Contratada_OSPreferencial, P00WH3_A1953Contratada_CntPadrao, P00WH3_n1953Contratada_CntPadrao, P00WH3_A43Contratada_Ativo, P00WH3_A1127Contratada_LogoArquivo, P00WH3_n1127Contratada_LogoArquivo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A530Contratada_Lote ;
      private int AV16Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int A40Contratada_PessoaCod ;
      private int A349Contratada_MunicipioCod ;
      private int A524Contratada_OS ;
      private int A1451Contratada_SS ;
      private int A1953Contratada_CntPadrao ;
      private int AV21GXV1 ;
      private int AV23GXV2 ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A1128Contratada_LogoNomeArq ;
      private String A1129Contratada_LogoTipoArq ;
      private String A1592Contratada_AreaTrbClcPFnl ;
      private String A41Contratada_PessoaNom ;
      private String A518Pessoa_IE ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private String A438Contratada_Sigla ;
      private String A516Contratada_TipoFabrica ;
      private String A342Contratada_BancoNome ;
      private String A343Contratada_BancoNro ;
      private String A344Contratada_AgenciaNome ;
      private String A345Contratada_AgenciaNro ;
      private String A51Contratada_ContaCorrente ;
      private String A350Contratada_UF ;
      private bool returnInSub ;
      private bool n1128Contratada_LogoNomeArq ;
      private bool n1129Contratada_LogoTipoArq ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n1592Contratada_AreaTrbClcPFnl ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n342Contratada_BancoNome ;
      private bool n343Contratada_BancoNro ;
      private bool n344Contratada_AgenciaNome ;
      private bool n345Contratada_AgenciaNro ;
      private bool n51Contratada_ContaCorrente ;
      private bool n349Contratada_MunicipioCod ;
      private bool n350Contratada_UF ;
      private bool n524Contratada_OS ;
      private bool n1451Contratada_SS ;
      private bool n530Contratada_Lote ;
      private bool A1481Contratada_UsaOSistema ;
      private bool n1481Contratada_UsaOSistema ;
      private bool A1867Contratada_OSPreferencial ;
      private bool n1867Contratada_OSPreferencial ;
      private bool n1953Contratada_CntPadrao ;
      private bool A43Contratada_Ativo ;
      private bool n1127Contratada_LogoArquivo ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String A1127Contratada_LogoArquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private int[] P00WH2_A39Contratada_Codigo ;
      private String[] P00WH2_A1128Contratada_LogoNomeArq ;
      private bool[] P00WH2_n1128Contratada_LogoNomeArq ;
      private String[] P00WH2_A1129Contratada_LogoTipoArq ;
      private bool[] P00WH2_n1129Contratada_LogoTipoArq ;
      private int[] P00WH2_A52Contratada_AreaTrabalhoCod ;
      private String[] P00WH2_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00WH2_n53Contratada_AreaTrabalhoDes ;
      private String[] P00WH2_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] P00WH2_n1592Contratada_AreaTrbClcPFnl ;
      private int[] P00WH2_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] P00WH2_n1595Contratada_AreaTrbSrvPdr ;
      private int[] P00WH2_A40Contratada_PessoaCod ;
      private String[] P00WH2_A41Contratada_PessoaNom ;
      private bool[] P00WH2_n41Contratada_PessoaNom ;
      private String[] P00WH2_A42Contratada_PessoaCNPJ ;
      private bool[] P00WH2_n42Contratada_PessoaCNPJ ;
      private String[] P00WH2_A518Pessoa_IE ;
      private bool[] P00WH2_n518Pessoa_IE ;
      private String[] P00WH2_A519Pessoa_Endereco ;
      private bool[] P00WH2_n519Pessoa_Endereco ;
      private String[] P00WH2_A521Pessoa_CEP ;
      private bool[] P00WH2_n521Pessoa_CEP ;
      private String[] P00WH2_A522Pessoa_Telefone ;
      private bool[] P00WH2_n522Pessoa_Telefone ;
      private String[] P00WH2_A523Pessoa_Fax ;
      private bool[] P00WH2_n523Pessoa_Fax ;
      private String[] P00WH2_A438Contratada_Sigla ;
      private String[] P00WH2_A516Contratada_TipoFabrica ;
      private String[] P00WH2_A342Contratada_BancoNome ;
      private bool[] P00WH2_n342Contratada_BancoNome ;
      private String[] P00WH2_A343Contratada_BancoNro ;
      private bool[] P00WH2_n343Contratada_BancoNro ;
      private String[] P00WH2_A344Contratada_AgenciaNome ;
      private bool[] P00WH2_n344Contratada_AgenciaNome ;
      private String[] P00WH2_A345Contratada_AgenciaNro ;
      private bool[] P00WH2_n345Contratada_AgenciaNro ;
      private String[] P00WH2_A51Contratada_ContaCorrente ;
      private bool[] P00WH2_n51Contratada_ContaCorrente ;
      private int[] P00WH2_A349Contratada_MunicipioCod ;
      private bool[] P00WH2_n349Contratada_MunicipioCod ;
      private String[] P00WH2_A350Contratada_UF ;
      private bool[] P00WH2_n350Contratada_UF ;
      private int[] P00WH2_A524Contratada_OS ;
      private bool[] P00WH2_n524Contratada_OS ;
      private int[] P00WH2_A1451Contratada_SS ;
      private bool[] P00WH2_n1451Contratada_SS ;
      private short[] P00WH2_A530Contratada_Lote ;
      private bool[] P00WH2_n530Contratada_Lote ;
      private bool[] P00WH2_A1481Contratada_UsaOSistema ;
      private bool[] P00WH2_n1481Contratada_UsaOSistema ;
      private bool[] P00WH2_A1867Contratada_OSPreferencial ;
      private bool[] P00WH2_n1867Contratada_OSPreferencial ;
      private int[] P00WH2_A1953Contratada_CntPadrao ;
      private bool[] P00WH2_n1953Contratada_CntPadrao ;
      private bool[] P00WH2_A43Contratada_Ativo ;
      private String[] P00WH2_A1127Contratada_LogoArquivo ;
      private bool[] P00WH2_n1127Contratada_LogoArquivo ;
      private String[] P00WH3_A1128Contratada_LogoNomeArq ;
      private bool[] P00WH3_n1128Contratada_LogoNomeArq ;
      private String[] P00WH3_A1129Contratada_LogoTipoArq ;
      private bool[] P00WH3_n1129Contratada_LogoTipoArq ;
      private int[] P00WH3_A39Contratada_Codigo ;
      private int[] P00WH3_A52Contratada_AreaTrabalhoCod ;
      private String[] P00WH3_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00WH3_n53Contratada_AreaTrabalhoDes ;
      private String[] P00WH3_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] P00WH3_n1592Contratada_AreaTrbClcPFnl ;
      private int[] P00WH3_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] P00WH3_n1595Contratada_AreaTrbSrvPdr ;
      private int[] P00WH3_A40Contratada_PessoaCod ;
      private String[] P00WH3_A41Contratada_PessoaNom ;
      private bool[] P00WH3_n41Contratada_PessoaNom ;
      private String[] P00WH3_A42Contratada_PessoaCNPJ ;
      private bool[] P00WH3_n42Contratada_PessoaCNPJ ;
      private String[] P00WH3_A518Pessoa_IE ;
      private bool[] P00WH3_n518Pessoa_IE ;
      private String[] P00WH3_A519Pessoa_Endereco ;
      private bool[] P00WH3_n519Pessoa_Endereco ;
      private String[] P00WH3_A521Pessoa_CEP ;
      private bool[] P00WH3_n521Pessoa_CEP ;
      private String[] P00WH3_A522Pessoa_Telefone ;
      private bool[] P00WH3_n522Pessoa_Telefone ;
      private String[] P00WH3_A523Pessoa_Fax ;
      private bool[] P00WH3_n523Pessoa_Fax ;
      private String[] P00WH3_A438Contratada_Sigla ;
      private String[] P00WH3_A516Contratada_TipoFabrica ;
      private String[] P00WH3_A342Contratada_BancoNome ;
      private bool[] P00WH3_n342Contratada_BancoNome ;
      private String[] P00WH3_A343Contratada_BancoNro ;
      private bool[] P00WH3_n343Contratada_BancoNro ;
      private String[] P00WH3_A344Contratada_AgenciaNome ;
      private bool[] P00WH3_n344Contratada_AgenciaNome ;
      private String[] P00WH3_A345Contratada_AgenciaNro ;
      private bool[] P00WH3_n345Contratada_AgenciaNro ;
      private String[] P00WH3_A51Contratada_ContaCorrente ;
      private bool[] P00WH3_n51Contratada_ContaCorrente ;
      private int[] P00WH3_A349Contratada_MunicipioCod ;
      private bool[] P00WH3_n349Contratada_MunicipioCod ;
      private String[] P00WH3_A350Contratada_UF ;
      private bool[] P00WH3_n350Contratada_UF ;
      private int[] P00WH3_A524Contratada_OS ;
      private bool[] P00WH3_n524Contratada_OS ;
      private int[] P00WH3_A1451Contratada_SS ;
      private bool[] P00WH3_n1451Contratada_SS ;
      private short[] P00WH3_A530Contratada_Lote ;
      private bool[] P00WH3_n530Contratada_Lote ;
      private bool[] P00WH3_A1481Contratada_UsaOSistema ;
      private bool[] P00WH3_n1481Contratada_UsaOSistema ;
      private bool[] P00WH3_A1867Contratada_OSPreferencial ;
      private bool[] P00WH3_n1867Contratada_OSPreferencial ;
      private int[] P00WH3_A1953Contratada_CntPadrao ;
      private bool[] P00WH3_n1953Contratada_CntPadrao ;
      private bool[] P00WH3_A43Contratada_Ativo ;
      private String[] P00WH3_A1127Contratada_LogoArquivo ;
      private bool[] P00WH3_n1127Contratada_LogoArquivo ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditcontratada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00WH2 ;
          prmP00WH2 = new Object[] {
          new Object[] {"@AV16Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00WH3 ;
          prmP00WH3 = new Object[] {
          new Object[] {"@AV16Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00WH2", "SELECT T1.[Contratada_Codigo], T1.[Contratada_LogoNomeArq], T1.[Contratada_LogoTipoArq], T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T2.[AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, T2.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Pessoa_IE], T3.[Pessoa_Endereco], T3.[Pessoa_CEP], T3.[Pessoa_Telefone], T3.[Pessoa_Fax], T1.[Contratada_Sigla], T1.[Contratada_TipoFabrica], T1.[Contratada_BancoNome], T1.[Contratada_BancoNro], T1.[Contratada_AgenciaNome], T1.[Contratada_AgenciaNro], T1.[Contratada_ContaCorrente], T1.[Contratada_MunicipioCod] AS Contratada_MunicipioCod, T4.[Estado_UF] AS Contratada_UF, T1.[Contratada_OS], T1.[Contratada_SS], T1.[Contratada_Lote], T1.[Contratada_UsaOSistema], T1.[Contratada_OSPreferencial], T1.[Contratada_CntPadrao], T1.[Contratada_Ativo], T1.[Contratada_LogoArquivo] FROM ((([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T4 WITH (NOLOCK) ON T4.[Municipio_Codigo] = T1.[Contratada_MunicipioCod]) WHERE T1.[Contratada_Codigo] = @AV16Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WH2,1,0,false,true )
             ,new CursorDef("P00WH3", "SELECT T1.[Contratada_LogoNomeArq], T1.[Contratada_LogoTipoArq], T1.[Contratada_Codigo], T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T2.[AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, T2.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Pessoa_IE], T3.[Pessoa_Endereco], T3.[Pessoa_CEP], T3.[Pessoa_Telefone], T3.[Pessoa_Fax], T1.[Contratada_Sigla], T1.[Contratada_TipoFabrica], T1.[Contratada_BancoNome], T1.[Contratada_BancoNro], T1.[Contratada_AgenciaNome], T1.[Contratada_AgenciaNro], T1.[Contratada_ContaCorrente], T1.[Contratada_MunicipioCod] AS Contratada_MunicipioCod, T4.[Estado_UF] AS Contratada_UF, T1.[Contratada_OS], T1.[Contratada_SS], T1.[Contratada_Lote], T1.[Contratada_UsaOSistema], T1.[Contratada_OSPreferencial], T1.[Contratada_CntPadrao], T1.[Contratada_Ativo], T1.[Contratada_LogoArquivo] FROM ((([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T4 WITH (NOLOCK) ON T4.[Municipio_Codigo] = T1.[Contratada_MunicipioCod]) WHERE T1.[Contratada_Codigo] = @AV16Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00WH3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 10) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 15) ;
                ((String[]) buf[28])[0] = rslt.getString(17, 1) ;
                ((String[]) buf[29])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getString(19, 6) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getString(20, 50) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getString(21, 10) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getString(22, 10) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((int[]) buf[39])[0] = rslt.getInt(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getString(24, 2) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((int[]) buf[43])[0] = rslt.getInt(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((int[]) buf[45])[0] = rslt.getInt(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((short[]) buf[47])[0] = rslt.getShort(27) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                ((bool[]) buf[49])[0] = rslt.getBool(28) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(28);
                ((bool[]) buf[51])[0] = rslt.getBool(29) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(29);
                ((int[]) buf[53])[0] = rslt.getInt(30) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(30);
                ((bool[]) buf[55])[0] = rslt.getBool(31) ;
                ((String[]) buf[56])[0] = rslt.getBLOBFile(32, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((String[]) buf[13])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 10) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 15) ;
                ((String[]) buf[28])[0] = rslt.getString(17, 1) ;
                ((String[]) buf[29])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getString(19, 6) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getString(20, 50) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getString(21, 10) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getString(22, 10) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((int[]) buf[39])[0] = rslt.getInt(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getString(24, 2) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((int[]) buf[43])[0] = rslt.getInt(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((int[]) buf[45])[0] = rslt.getInt(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((short[]) buf[47])[0] = rslt.getShort(27) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                ((bool[]) buf[49])[0] = rslt.getBool(28) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(28);
                ((bool[]) buf[51])[0] = rslt.getBool(29) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(29);
                ((int[]) buf[53])[0] = rslt.getInt(30) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(30);
                ((bool[]) buf[55])[0] = rslt.getBool(31) ;
                ((String[]) buf[56])[0] = rslt.getBLOBFile(32, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
