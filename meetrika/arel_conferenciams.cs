/*
               File: REL_ConferenciaMS
        Description: Relat�rio de Confer�ncia Ministerio Saude
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:5:25.19
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_conferenciams : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV11Arquivo = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV12Arquivo2 = GetNextPar( );
                  AV84Aba = GetNextPar( );
                  AV30Opcao = GetNextPar( );
                  AV14ColDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV15ColPB = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16ColPL = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV38PraLinha = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV10Acao = GetNextPar( );
                  AV82Contratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV87FileName = GetNextPar( );
                  AV89ColPBFM = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV90ColPLFM = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV92DemandaFM = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_conferenciams( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_conferenciams( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Arquivo ,
                           String aP1_Arquivo2 ,
                           String aP2_Aba ,
                           String aP3_Opcao ,
                           short aP4_ColDmn ,
                           short aP5_ColPB ,
                           short aP6_ColPL ,
                           short aP7_PraLinha ,
                           String aP8_Acao ,
                           int aP9_Contratada ,
                           String aP10_FileName ,
                           short aP11_ColPBFM ,
                           short aP12_ColPLFM ,
                           ref String aP13_DemandaFM )
      {
         this.AV11Arquivo = aP0_Arquivo;
         this.AV12Arquivo2 = aP1_Arquivo2;
         this.AV84Aba = aP2_Aba;
         this.AV30Opcao = aP3_Opcao;
         this.AV14ColDmn = aP4_ColDmn;
         this.AV15ColPB = aP5_ColPB;
         this.AV16ColPL = aP6_ColPL;
         this.AV38PraLinha = aP7_PraLinha;
         this.AV10Acao = aP8_Acao;
         this.AV82Contratada = aP9_Contratada;
         this.AV87FileName = aP10_FileName;
         this.AV89ColPBFM = aP11_ColPBFM;
         this.AV90ColPLFM = aP12_ColPLFM;
         this.AV92DemandaFM = aP13_DemandaFM;
         initialize();
         executePrivate();
         aP13_DemandaFM=this.AV92DemandaFM;
      }

      public String executeUdp( String aP0_Arquivo ,
                                String aP1_Arquivo2 ,
                                String aP2_Aba ,
                                String aP3_Opcao ,
                                short aP4_ColDmn ,
                                short aP5_ColPB ,
                                short aP6_ColPL ,
                                short aP7_PraLinha ,
                                String aP8_Acao ,
                                int aP9_Contratada ,
                                String aP10_FileName ,
                                short aP11_ColPBFM ,
                                short aP12_ColPLFM )
      {
         this.AV11Arquivo = aP0_Arquivo;
         this.AV12Arquivo2 = aP1_Arquivo2;
         this.AV84Aba = aP2_Aba;
         this.AV30Opcao = aP3_Opcao;
         this.AV14ColDmn = aP4_ColDmn;
         this.AV15ColPB = aP5_ColPB;
         this.AV16ColPL = aP6_ColPL;
         this.AV38PraLinha = aP7_PraLinha;
         this.AV10Acao = aP8_Acao;
         this.AV82Contratada = aP9_Contratada;
         this.AV87FileName = aP10_FileName;
         this.AV89ColPBFM = aP11_ColPBFM;
         this.AV90ColPLFM = aP12_ColPLFM;
         this.AV92DemandaFM = aP13_DemandaFM;
         initialize();
         executePrivate();
         aP13_DemandaFM=this.AV92DemandaFM;
         return AV92DemandaFM ;
      }

      public void executeSubmit( String aP0_Arquivo ,
                                 String aP1_Arquivo2 ,
                                 String aP2_Aba ,
                                 String aP3_Opcao ,
                                 short aP4_ColDmn ,
                                 short aP5_ColPB ,
                                 short aP6_ColPL ,
                                 short aP7_PraLinha ,
                                 String aP8_Acao ,
                                 int aP9_Contratada ,
                                 String aP10_FileName ,
                                 short aP11_ColPBFM ,
                                 short aP12_ColPLFM ,
                                 ref String aP13_DemandaFM )
      {
         arel_conferenciams objarel_conferenciams;
         objarel_conferenciams = new arel_conferenciams();
         objarel_conferenciams.AV11Arquivo = aP0_Arquivo;
         objarel_conferenciams.AV12Arquivo2 = aP1_Arquivo2;
         objarel_conferenciams.AV84Aba = aP2_Aba;
         objarel_conferenciams.AV30Opcao = aP3_Opcao;
         objarel_conferenciams.AV14ColDmn = aP4_ColDmn;
         objarel_conferenciams.AV15ColPB = aP5_ColPB;
         objarel_conferenciams.AV16ColPL = aP6_ColPL;
         objarel_conferenciams.AV38PraLinha = aP7_PraLinha;
         objarel_conferenciams.AV10Acao = aP8_Acao;
         objarel_conferenciams.AV82Contratada = aP9_Contratada;
         objarel_conferenciams.AV87FileName = aP10_FileName;
         objarel_conferenciams.AV89ColPBFM = aP11_ColPBFM;
         objarel_conferenciams.AV90ColPLFM = aP12_ColPLFM;
         objarel_conferenciams.AV92DemandaFM = aP13_DemandaFM;
         objarel_conferenciams.context.SetSubmitInitialConfig(context);
         objarel_conferenciams.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_conferenciams);
         aP13_DemandaFM=this.AV92DemandaFM;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_conferenciams)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 9, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV43WWPContext) ;
            /* Execute user subroutine: 'OPENFILE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Arquivo2)) )
            {
               AV66NomeArq = "Arquivo: " + AV87FileName;
            }
            else
            {
               AV66NomeArq = "Arquivo SGCD: " + StringUtil.Substring( AV11Arquivo, AV65i, StringUtil.Len( AV11Arquivo));
               AV65i = (short)(StringUtil.StringSearchRev( AV11Arquivo, "\\", -1)+1);
               AV67NomeArq2 = "Arquivo JIRA: " + StringUtil.Substring( AV12Arquivo2, AV65i, StringUtil.Len( AV12Arquivo2));
            }
            if ( ! (0==AV82Contratada) )
            {
               /* Using cursor P003F2 */
               pr_default.execute(0, new Object[] {AV82Contratada});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A40Contratada_PessoaCod = P003F2_A40Contratada_PessoaCod[0];
                  A39Contratada_Codigo = P003F2_A39Contratada_Codigo[0];
                  A41Contratada_PessoaNom = P003F2_A41Contratada_PessoaNom[0];
                  n41Contratada_PessoaNom = P003F2_n41Contratada_PessoaNom[0];
                  A41Contratada_PessoaNom = P003F2_A41Contratada_PessoaNom[0];
                  n41Contratada_PessoaNom = P003F2_n41Contratada_PessoaNom[0];
                  AV83SubTitulo = A41Contratada_PessoaNom;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(0);
            }
            if ( StringUtil.StrCmp(AV30Opcao, "ICNT") == 0 )
            {
               AV100GXLvl32 = 0;
               /* Using cursor P003F3 */
               pr_default.execute(1, new Object[] {AV43WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A516Contratada_TipoFabrica = P003F3_A516Contratada_TipoFabrica[0];
                  A52Contratada_AreaTrabalhoCod = P003F3_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P003F3_n52Contratada_AreaTrabalhoCod[0];
                  A39Contratada_Codigo = P003F3_A39Contratada_Codigo[0];
                  AV100GXLvl32 = 1;
                  AV58Contratada_Codigo = A39Contratada_Codigo;
                  AV68Ok = true;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV100GXLvl32 == 0 )
               {
                  AV27linha = "Contratada FM n�o existe: ";
                  H3F0( false, 21) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+21);
                  AV68Ok = false;
               }
               AV28Ln = 2;
               AV42Titulo = "Resultado da importa��o de contagens TRF";
               while ( AV68Ok && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ExcelDocument.get_Cells(AV28Ln, 1, 1, 1).Text)) )
               {
                  AV70Sistema_Sigla = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, 3, 1, 1).Text);
                  AV9Sistema_Codigo = 0;
                  AV101GXLvl47 = 0;
                  /* Using cursor P003F4 */
                  pr_default.execute(2, new Object[] {AV70Sistema_Sigla});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A129Sistema_Sigla = P003F4_A129Sistema_Sigla[0];
                     A127Sistema_Codigo = P003F4_A127Sistema_Codigo[0];
                     AV101GXLvl47 = 1;
                     AV9Sistema_Codigo = A127Sistema_Codigo;
                     AV26Flag = true;
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
                  if ( AV101GXLvl47 == 0 )
                  {
                     AV27linha = "Sistema n�o existe: " + AV70Sistema_Sigla;
                     AV39Semcadastrar = (short)(AV39Semcadastrar+1);
                     H3F0( false, 21) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+21);
                     AV26Flag = false;
                  }
                  if ( AV26Flag )
                  {
                     while ( StringUtil.StrCmp(StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, 3, 1, 1).Text), AV70Sistema_Sigla) == 0 )
                     {
                        AV59ContratadaUsuario_UsuarioPessoaNom = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, 7, 1, 1).Text);
                        AV102GXLvl60 = 0;
                        /* Using cursor P003F5 */
                        pr_default.execute(3, new Object[] {AV43WWPContext.gxTpr_Areatrabalho_codigo, AV59ContratadaUsuario_UsuarioPessoaNom});
                        while ( (pr_default.getStatus(3) != 101) )
                        {
                           A66ContratadaUsuario_ContratadaCod = P003F5_A66ContratadaUsuario_ContratadaCod[0];
                           A70ContratadaUsuario_UsuarioPessoaCod = P003F5_A70ContratadaUsuario_UsuarioPessoaCod[0];
                           n70ContratadaUsuario_UsuarioPessoaCod = P003F5_n70ContratadaUsuario_UsuarioPessoaCod[0];
                           A1228ContratadaUsuario_AreaTrabalhoCod = P003F5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                           n1228ContratadaUsuario_AreaTrabalhoCod = P003F5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                           A71ContratadaUsuario_UsuarioPessoaNom = P003F5_A71ContratadaUsuario_UsuarioPessoaNom[0];
                           n71ContratadaUsuario_UsuarioPessoaNom = P003F5_n71ContratadaUsuario_UsuarioPessoaNom[0];
                           A69ContratadaUsuario_UsuarioCod = P003F5_A69ContratadaUsuario_UsuarioCod[0];
                           A1228ContratadaUsuario_AreaTrabalhoCod = P003F5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                           n1228ContratadaUsuario_AreaTrabalhoCod = P003F5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                           A70ContratadaUsuario_UsuarioPessoaCod = P003F5_A70ContratadaUsuario_UsuarioPessoaCod[0];
                           n70ContratadaUsuario_UsuarioPessoaCod = P003F5_n70ContratadaUsuario_UsuarioPessoaCod[0];
                           A71ContratadaUsuario_UsuarioPessoaNom = P003F5_A71ContratadaUsuario_UsuarioPessoaNom[0];
                           n71ContratadaUsuario_UsuarioPessoaNom = P003F5_n71ContratadaUsuario_UsuarioPessoaNom[0];
                           AV102GXLvl60 = 1;
                           AV46ContagemResultado_ContadorFMCod = A69ContratadaUsuario_UsuarioCod;
                           AV64Flag2 = true;
                           pr_default.readNext(3);
                        }
                        pr_default.close(3);
                        if ( AV102GXLvl60 == 0 )
                        {
                           AV27linha = "Contador n�o existe: " + AV59ContratadaUsuario_UsuarioPessoaNom;
                           AV39Semcadastrar = (short)(AV39Semcadastrar+1);
                           H3F0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                           AV64Flag2 = false;
                        }
                        if ( AV64Flag2 )
                        {
                           AV93Demanda = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, 1, 1, 1).Text);
                           /* Execute user subroutine: 'LIMPARDEMANDA' */
                           S161 ();
                           if ( returnInSub )
                           {
                              this.cleanup();
                              if (true) return;
                           }
                           /*
                              INSERT RECORD ON TABLE ContagemResultado

                           */
                           A457ContagemResultado_Demanda = AV93Demanda;
                           n457ContagemResultado_Demanda = false;
                           A493ContagemResultado_DemandaFM = "71";
                           n493ContagemResultado_DemandaFM = false;
                           A508ContagemResultado_Owner = AV46ContagemResultado_ContadorFMCod;
                           A471ContagemResultado_DataDmn = context.localUtil.CToD( "09/12/14", 2);
                           A490ContagemResultado_ContratadaCod = AV58Contratada_Codigo;
                           n490ContagemResultado_ContratadaCod = false;
                           A489ContagemResultado_SistemaCod = AV9Sistema_Codigo;
                           n489ContagemResultado_SistemaCod = false;
                           A484ContagemResultado_StatusDmn = "R";
                           n484ContagemResultado_StatusDmn = false;
                           A468ContagemResultado_NaoCnfDmnCod = 0;
                           n468ContagemResultado_NaoCnfDmnCod = false;
                           n468ContagemResultado_NaoCnfDmnCod = true;
                           A454ContagemResultado_ContadorFSCod = 0;
                           n454ContagemResultado_ContadorFSCod = false;
                           n454ContagemResultado_ContadorFSCod = true;
                           A485ContagemResultado_EhValidacao = false;
                           n485ContagemResultado_EhValidacao = false;
                           A598ContagemResultado_Baseline = false;
                           n598ContagemResultado_Baseline = false;
                           A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
                           n1350ContagemResultado_DataCadastro = false;
                           A1452ContagemResultado_SS = 0;
                           n1452ContagemResultado_SS = false;
                           A1515ContagemResultado_Evento = 1;
                           n1515ContagemResultado_Evento = false;
                           A1583ContagemResultado_TipoRegistro = 1;
                           /* Using cursor P003F6 */
                           pr_default.execute(4, new Object[] {A471ContagemResultado_DataDmn, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, A508ContagemResultado_Owner, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, A1583ContagemResultado_TipoRegistro});
                           A456ContagemResultado_Codigo = P003F6_A456ContagemResultado_Codigo[0];
                           pr_default.close(4);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                           if ( (pr_default.getStatus(4) == 1) )
                           {
                              context.Gx_err = 1;
                              Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                           }
                           else
                           {
                              context.Gx_err = 0;
                              Gx_emsg = "";
                           }
                           /* End Insert */
                           AV88ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                           /*
                              INSERT RECORD ON TABLE ContagemResultadoContagens

                           */
                           A456ContagemResultado_Codigo = AV88ContagemResultado_Codigo;
                           A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
                           A511ContagemResultado_HoraCnt = Gx_time;
                           A470ContagemResultado_ContadorFMCod = AV46ContagemResultado_ContadorFMCod;
                           A460ContagemResultado_PFBFM = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, 9, 1, 1).Number);
                           n460ContagemResultado_PFBFM = false;
                           A461ContagemResultado_PFLFM = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, 10, 1, 1).Number);
                           n461ContagemResultado_PFLFM = false;
                           A483ContagemResultado_StatusCnt = 5;
                           A482ContagemResultadoContagens_Esforco = 10;
                           A517ContagemResultado_Ultima = true;
                           A469ContagemResultado_NaoCnfCntCod = 0;
                           n469ContagemResultado_NaoCnfCntCod = false;
                           n469ContagemResultado_NaoCnfCntCod = true;
                           /* Using cursor P003F7 */
                           pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima});
                           pr_default.close(5);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           if ( (pr_default.getStatus(5) == 1) )
                           {
                              context.Gx_err = 1;
                              Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                           }
                           else
                           {
                              context.Gx_err = 0;
                              Gx_emsg = "";
                           }
                           /* End Insert */
                           AV17Conferida = (short)(AV17Conferida+1);
                        }
                        AV28Ln = (short)(AV28Ln+1);
                     }
                  }
                  else
                  {
                     AV28Ln = (short)(AV28Ln+1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV30Opcao, "CNF1") == 0 )
            {
               AV28Ln = 2;
               while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ExcelDocument.get_Cells(AV28Ln, 5, 1, 1).Text)) )
               {
                  AV62Date = DateTimeUtil.ResetTime(AV24ExcelDocument.get_Cells(AV28Ln, 8, 1, 1).Date);
                  if ( (DateTime.MinValue==AV61DataIni) && (DateTime.MinValue==AV60DataFim) )
                  {
                     AV61DataIni = AV62Date;
                     AV60DataFim = AV62Date;
                  }
                  else if ( AV62Date < AV61DataIni )
                  {
                     AV61DataIni = AV62Date;
                  }
                  else if ( AV62Date > AV60DataFim )
                  {
                     AV60DataFim = AV62Date;
                  }
                  AV28Ln = (short)(AV28Ln+1);
               }
               AV28Ln = 2;
               AV42Titulo = "Resultado da Confer�ncia com planilha";
               AV69Periodo = "Per�odo: " + context.localUtil.DToC( AV61DataIni, 2, "/") + " - " + context.localUtil.DToC( AV60DataFim, 2, "/");
               while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ExcelDocument.get_Cells(AV28Ln, 5, 1, 1).Text)) )
               {
                  AV93Demanda = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, 5, 1, 1).Text);
                  /* Execute user subroutine: 'LIMPARDEMANDA' */
                  S161 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
                  AV62Date = DateTimeUtil.ResetTime(AV24ExcelDocument.get_Cells(AV28Ln, 8, 1, 1).Date);
                  AV26Flag = false;
                  AV103GXLvl134 = 0;
                  /* Using cursor P003F8 */
                  pr_default.execute(6, new Object[] {AV82Contratada, AV93Demanda});
                  while ( (pr_default.getStatus(6) != 101) )
                  {
                     A489ContagemResultado_SistemaCod = P003F8_A489ContagemResultado_SistemaCod[0];
                     n489ContagemResultado_SistemaCod = P003F8_n489ContagemResultado_SistemaCod[0];
                     A805ContagemResultado_ContratadaOrigemCod = P003F8_A805ContagemResultado_ContratadaOrigemCod[0];
                     n805ContagemResultado_ContratadaOrigemCod = P003F8_n805ContagemResultado_ContratadaOrigemCod[0];
                     A457ContagemResultado_Demanda = P003F8_A457ContagemResultado_Demanda[0];
                     n457ContagemResultado_Demanda = P003F8_n457ContagemResultado_Demanda[0];
                     A456ContagemResultado_Codigo = P003F8_A456ContagemResultado_Codigo[0];
                     A484ContagemResultado_StatusDmn = P003F8_A484ContagemResultado_StatusDmn[0];
                     n484ContagemResultado_StatusDmn = P003F8_n484ContagemResultado_StatusDmn[0];
                     A602ContagemResultado_OSVinculada = P003F8_A602ContagemResultado_OSVinculada[0];
                     n602ContagemResultado_OSVinculada = P003F8_n602ContagemResultado_OSVinculada[0];
                     A494ContagemResultado_Descricao = P003F8_A494ContagemResultado_Descricao[0];
                     n494ContagemResultado_Descricao = P003F8_n494ContagemResultado_Descricao[0];
                     A515ContagemResultado_SistemaCoord = P003F8_A515ContagemResultado_SistemaCoord[0];
                     n515ContagemResultado_SistemaCoord = P003F8_n515ContagemResultado_SistemaCoord[0];
                     A515ContagemResultado_SistemaCoord = P003F8_A515ContagemResultado_SistemaCoord[0];
                     n515ContagemResultado_SistemaCoord = P003F8_n515ContagemResultado_SistemaCoord[0];
                     OV48ContagemResultado_HoraCnt = AV48ContagemResultado_HoraCnt;
                     OV56ContagemResultado_Ultima = AV56ContagemResultado_Ultima;
                     OV57ContagemResultadoContagens_Esforco = AV57ContagemResultadoContagens_Esforco;
                     AV103GXLvl134 = 1;
                     AV34PFBFS = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, 9, 1, 1).Number);
                     AV33PFBFM = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, 10, 1, 1).Number);
                     if ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) )
                     {
                        AV68Ok = true;
                        AV88ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                        AV86ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
                        /* Execute user subroutine: 'STATUSOSVNC' */
                        S151 ();
                        if ( returnInSub )
                        {
                           pr_default.close(6);
                           this.cleanup();
                           if (true) return;
                        }
                        AV72ContagemResultadoErro_Tipo = "DE";
                        if ( String.IsNullOrEmpty(StringUtil.RTrim( A494ContagemResultado_Descricao)) )
                        {
                           AV27linha = "Demanda: " + AV93Demanda + "  -  Sem Descri��o";
                           /* Execute user subroutine: 'REGISTRAERRO' */
                           S121 ();
                           if ( returnInSub )
                           {
                              pr_default.close(6);
                              this.cleanup();
                              if (true) return;
                           }
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( A515ContagemResultado_SistemaCoord)) )
                           {
                              AV27linha = AV27linha + " e sem Coordena��o";
                              AV72ContagemResultadoErro_Tipo = "SC";
                              /* Execute user subroutine: 'REGISTRAERRO' */
                              S121 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(6);
                                 this.cleanup();
                                 if (true) return;
                              }
                           }
                           H3F0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                        }
                        else
                        {
                           /* Execute user subroutine: 'ATUALIZAERRO' */
                           S131 ();
                           if ( returnInSub )
                           {
                              pr_default.close(6);
                              this.cleanup();
                              if (true) return;
                           }
                           AV72ContagemResultadoErro_Tipo = "SC";
                           if ( String.IsNullOrEmpty(StringUtil.RTrim( A515ContagemResultado_SistemaCoord)) )
                           {
                              AV27linha = "Demanda: " + AV93Demanda + "  -  Sem Coordena��o";
                              H3F0( false, 21) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+21);
                              /* Execute user subroutine: 'REGISTRAERRO' */
                              S121 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(6);
                                 this.cleanup();
                                 if (true) return;
                              }
                           }
                           else
                           {
                              /* Execute user subroutine: 'ATUALIZAERRO' */
                              S131 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(6);
                                 this.cleanup();
                                 if (true) return;
                              }
                           }
                        }
                        /* Using cursor P003F9 */
                        pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(7) != 101) )
                        {
                           A517ContagemResultado_Ultima = P003F9_A517ContagemResultado_Ultima[0];
                           A473ContagemResultado_DataCnt = P003F9_A473ContagemResultado_DataCnt[0];
                           A511ContagemResultado_HoraCnt = P003F9_A511ContagemResultado_HoraCnt[0];
                           A463ContagemResultado_ParecerTcn = P003F9_A463ContagemResultado_ParecerTcn[0];
                           n463ContagemResultado_ParecerTcn = P003F9_n463ContagemResultado_ParecerTcn[0];
                           A460ContagemResultado_PFBFM = P003F9_A460ContagemResultado_PFBFM[0];
                           n460ContagemResultado_PFBFM = P003F9_n460ContagemResultado_PFBFM[0];
                           A458ContagemResultado_PFBFS = P003F9_A458ContagemResultado_PFBFS[0];
                           n458ContagemResultado_PFBFS = P003F9_n458ContagemResultado_PFBFS[0];
                           A461ContagemResultado_PFLFM = P003F9_A461ContagemResultado_PFLFM[0];
                           n461ContagemResultado_PFLFM = P003F9_n461ContagemResultado_PFLFM[0];
                           A459ContagemResultado_PFLFS = P003F9_A459ContagemResultado_PFLFS[0];
                           n459ContagemResultado_PFLFS = P003F9_n459ContagemResultado_PFLFS[0];
                           A470ContagemResultado_ContadorFMCod = P003F9_A470ContagemResultado_ContadorFMCod[0];
                           A462ContagemResultado_Divergencia = P003F9_A462ContagemResultado_Divergencia[0];
                           A469ContagemResultado_NaoCnfCntCod = P003F9_A469ContagemResultado_NaoCnfCntCod[0];
                           n469ContagemResultado_NaoCnfCntCod = P003F9_n469ContagemResultado_NaoCnfCntCod[0];
                           A483ContagemResultado_StatusCnt = P003F9_A483ContagemResultado_StatusCnt[0];
                           A482ContagemResultadoContagens_Esforco = P003F9_A482ContagemResultadoContagens_Esforco[0];
                           AV26Flag = (bool)(((A473ContagemResultado_DataCnt!=AV62Date)));
                           AV72ContagemResultadoErro_Tipo = "DA";
                           if ( AV26Flag )
                           {
                              AV27linha = "Demanda: " + AV93Demanda + "  -  Sistema: " + context.localUtil.DToC( A473ContagemResultado_DataCnt, 2, "/") + ", Planilha: " + context.localUtil.DToC( AV62Date, 2, "/");
                              H3F0( false, 21) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+21);
                              /* Execute user subroutine: 'REGISTRAERRO' */
                              S121 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(7);
                                 this.cleanup();
                                 if (true) return;
                              }
                              AV88ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                              AV47ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
                              AV48ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
                              AV50ContagemResultado_ParecerTcn = A463ContagemResultado_ParecerTcn;
                              AV51ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                              AV52ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
                              AV53ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                              AV54ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
                              AV46ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
                              AV19ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
                              AV49ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
                              AV55ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
                              AV56ContagemResultado_Ultima = A517ContagemResultado_Ultima;
                              AV57ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
                           }
                           else
                           {
                              /* Execute user subroutine: 'ATUALIZAERRO' */
                              S131 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(7);
                                 this.cleanup();
                                 if (true) return;
                              }
                           }
                           AV72ContagemResultadoErro_Tipo = "PF";
                           if ( ( AV34PFBFS != A458ContagemResultado_PFBFS ) || ( A460ContagemResultado_PFBFM != AV33PFBFM ) )
                           {
                              AV68Ok = false;
                              A483ContagemResultado_StatusCnt = 7;
                              AV55ContagemResultado_StatusCnt = 7;
                              AV27linha = "Diferen�a em PF Dmn: " + AV93Demanda + "  -  Sistema: (" + StringUtil.Trim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)) + "," + StringUtil.Trim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)) + "), Planilha: (" + StringUtil.Trim( StringUtil.Str( AV34PFBFS, 10, 3)) + "," + StringUtil.Trim( StringUtil.Str( AV33PFBFM, 10, 3)) + ")";
                              H3F0( false, 21) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+21);
                              /* Execute user subroutine: 'REGISTRAERRO' */
                              S121 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(7);
                                 this.cleanup();
                                 if (true) return;
                              }
                           }
                           else
                           {
                              /* Execute user subroutine: 'ATUALIZAERRO' */
                              S131 ();
                              if ( returnInSub )
                              {
                                 pr_default.close(7);
                                 this.cleanup();
                                 if (true) return;
                              }
                              A483ContagemResultado_StatusCnt = 5;
                              AV55ContagemResultado_StatusCnt = 5;
                              AV17Conferida = (short)(AV17Conferida+1);
                           }
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P003F10 */
                           pr_default.execute(8, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(8);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           if (true) break;
                           /* Using cursor P003F11 */
                           pr_default.execute(9, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(9);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           pr_default.readNext(7);
                        }
                        pr_default.close(7);
                        if ( AV68Ok )
                        {
                           A484ContagemResultado_StatusDmn = "C";
                           n484ContagemResultado_StatusDmn = false;
                        }
                        else
                        {
                           A484ContagemResultado_StatusDmn = "A";
                           n484ContagemResultado_StatusDmn = false;
                        }
                     }
                     else
                     {
                        AV26Flag = false;
                        AV27linha = "Demanda: " + AV93Demanda + "  -  N�o analisada! Status:" + gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                        H3F0( false, 21) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+21);
                     }
                     BatchSize = 100;
                     pr_default.initializeBatch( 10, BatchSize, this, "Executebatchp003f12");
                     /* Using cursor P003F12 */
                     pr_default.addRecord(10, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                     if ( pr_default.recordCount(10) == pr_default.getBatchSize(10) )
                     {
                        Executebatchp003f12( ) ;
                     }
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     pr_default.readNext(6);
                  }
                  if ( pr_default.getBatchSize(10) > 0 )
                  {
                     Executebatchp003f12( ) ;
                  }
                  pr_default.close(6);
                  if ( AV103GXLvl134 == 0 )
                  {
                     AV27linha = "Falta cadastrar demanda: " + StringUtil.Trim( AV93Demanda) + " ou contratada origem incorreta";
                     AV39Semcadastrar = (short)(AV39Semcadastrar+1);
                     H3F0( false, 21) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+21);
                  }
                  AV28Ln = (short)(AV28Ln+1);
               }
            }
            else if ( StringUtil.StrCmp(AV30Opcao, "CTRF") == 0 )
            {
               new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV43WWPContext) ;
               AV105GXLvl277 = 0;
               /* Using cursor P003F13 */
               pr_default.execute(11, new Object[] {AV43WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(11) != 101) )
               {
                  A39Contratada_Codigo = P003F13_A39Contratada_Codigo[0];
                  A516Contratada_TipoFabrica = P003F13_A516Contratada_TipoFabrica[0];
                  A52Contratada_AreaTrabalhoCod = P003F13_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P003F13_n52Contratada_AreaTrabalhoCod[0];
                  AV105GXLvl277 = 1;
                  /* Using cursor P003F14 */
                  pr_default.execute(12, new Object[] {A39Contratada_Codigo});
                  while ( (pr_default.getStatus(12) != 101) )
                  {
                     A452Contrato_CalculoDivergencia = P003F14_A452Contrato_CalculoDivergencia[0];
                     A453Contrato_IndiceDivergencia = P003F14_A453Contrato_IndiceDivergencia[0];
                     A74Contrato_Codigo = P003F14_A74Contrato_Codigo[0];
                     AV8CalculoDivergencia = A452Contrato_CalculoDivergencia;
                     AV23DivergenciaAceitavel = A453Contrato_IndiceDivergencia;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(12);
                  }
                  pr_default.close(12);
                  AV68Ok = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(11);
               }
               pr_default.close(11);
               if ( AV105GXLvl277 == 0 )
               {
                  AV68Ok = false;
               }
               AV28Ln = AV38PraLinha;
               AV42Titulo = "Confer�ncia e Atualiza��o com planilha TRF";
               if ( ! AV68Ok )
               {
                  AV27linha = "Contratada FM n�o existe nesta �rea de Trabalho.";
                  H3F0( false, 21) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+21);
               }
               else if ( StringUtil.StrCmp(AV10Acao, "C") == 0 )
               {
                  AV42Titulo = "Confer�ncia de valores cadastrados nas contagens";
                  while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text)) )
                  {
                     AV93Demanda = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text);
                     /* Execute user subroutine: 'LIMPARDEMANDA' */
                     S161 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV34PFBFS = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV15ColPB, 1, 1).Number);
                     AV36PFLFS = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV16ColPL, 1, 1).Number);
                     AV33PFBFM = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV89ColPBFM, 1, 1).Number);
                     AV35PFLFM = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV90ColPLFM, 1, 1).Number);
                     AV107GXLvl310 = 0;
                     pr_default.dynParam(13, new Object[]{ new Object[]{
                                                          AV92DemandaFM ,
                                                          A493ContagemResultado_DemandaFM ,
                                                          A484ContagemResultado_StatusDmn ,
                                                          A52Contratada_AreaTrabalhoCod ,
                                                          AV43WWPContext.gxTpr_Areatrabalho_codigo ,
                                                          A490ContagemResultado_ContratadaCod ,
                                                          AV82Contratada ,
                                                          AV93Demanda ,
                                                          A457ContagemResultado_Demanda },
                                                          new int[] {
                                                          TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                          TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                          }
                     });
                     /* Using cursor P003F15 */
                     pr_default.execute(13, new Object[] {AV93Demanda, AV43WWPContext.gxTpr_Areatrabalho_codigo, AV82Contratada, AV92DemandaFM});
                     while ( (pr_default.getStatus(13) != 101) )
                     {
                        A456ContagemResultado_Codigo = P003F15_A456ContagemResultado_Codigo[0];
                        A484ContagemResultado_StatusDmn = P003F15_A484ContagemResultado_StatusDmn[0];
                        n484ContagemResultado_StatusDmn = P003F15_n484ContagemResultado_StatusDmn[0];
                        A493ContagemResultado_DemandaFM = P003F15_A493ContagemResultado_DemandaFM[0];
                        n493ContagemResultado_DemandaFM = P003F15_n493ContagemResultado_DemandaFM[0];
                        A490ContagemResultado_ContratadaCod = P003F15_A490ContagemResultado_ContratadaCod[0];
                        n490ContagemResultado_ContratadaCod = P003F15_n490ContagemResultado_ContratadaCod[0];
                        A52Contratada_AreaTrabalhoCod = P003F15_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F15_n52Contratada_AreaTrabalhoCod[0];
                        A457ContagemResultado_Demanda = P003F15_A457ContagemResultado_Demanda[0];
                        n457ContagemResultado_Demanda = P003F15_n457ContagemResultado_Demanda[0];
                        A52Contratada_AreaTrabalhoCod = P003F15_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F15_n52Contratada_AreaTrabalhoCod[0];
                        AV107GXLvl310 = 1;
                        AV108GXLvl318 = 0;
                        /* Using cursor P003F16 */
                        pr_default.execute(14, new Object[] {A456ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(14) != 101) )
                        {
                           A517ContagemResultado_Ultima = P003F16_A517ContagemResultado_Ultima[0];
                           A459ContagemResultado_PFLFS = P003F16_A459ContagemResultado_PFLFS[0];
                           n459ContagemResultado_PFLFS = P003F16_n459ContagemResultado_PFLFS[0];
                           A458ContagemResultado_PFBFS = P003F16_A458ContagemResultado_PFBFS[0];
                           n458ContagemResultado_PFBFS = P003F16_n458ContagemResultado_PFBFS[0];
                           A461ContagemResultado_PFLFM = P003F16_A461ContagemResultado_PFLFM[0];
                           n461ContagemResultado_PFLFM = P003F16_n461ContagemResultado_PFLFM[0];
                           A460ContagemResultado_PFBFM = P003F16_A460ContagemResultado_PFBFM[0];
                           n460ContagemResultado_PFBFM = P003F16_n460ContagemResultado_PFBFM[0];
                           A473ContagemResultado_DataCnt = P003F16_A473ContagemResultado_DataCnt[0];
                           A511ContagemResultado_HoraCnt = P003F16_A511ContagemResultado_HoraCnt[0];
                           AV108GXLvl318 = 1;
                           if ( ( A460ContagemResultado_PFBFM != AV33PFBFM ) || ( A461ContagemResultado_PFLFM != AV35PFLFM ) || ( A458ContagemResultado_PFBFS != AV34PFBFS ) || ( A459ContagemResultado_PFLFS != AV36PFLFS ) )
                           {
                              AV91str = "Dmn: " + AV93Demanda + ", OS FM: " + StringUtil.Trim( A493ContagemResultado_DemandaFM) + " - Cadastro: (" + StringUtil.Trim( StringUtil.Str( A460ContagemResultado_PFBFM, 9, 3)) + "; " + StringUtil.Trim( StringUtil.Str( A461ContagemResultado_PFLFM, 9, 3)) + ";" + StringUtil.Trim( StringUtil.Str( A458ContagemResultado_PFBFS, 9, 3)) + "; " + StringUtil.Trim( StringUtil.Str( A459ContagemResultado_PFLFS, 9, 3)) + ") Planilha: (" + StringUtil.Trim( StringUtil.Str( AV34PFBFS, 9, 3)) + ";" + StringUtil.Trim( StringUtil.Str( AV36PFLFS, 9, 3)) + "; " + StringUtil.Trim( StringUtil.Str( AV33PFBFM, 9, 3)) + ";" + StringUtil.Trim( StringUtil.Str( AV35PFLFM, 9, 3)) + ") " + gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                              if ( StringUtil.Len( AV91str) > 150 )
                              {
                                 AV27linha = StringUtil.Substring( AV91str, 1, 150);
                                 H3F0( false, 21) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+21);
                                 AV27linha = StringUtil.Substring( AV91str, 151, 150);
                                 H3F0( false, 21) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+21);
                              }
                              else
                              {
                                 AV27linha = AV91str;
                                 H3F0( false, 21) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+21);
                              }
                           }
                           else
                           {
                              AV17Conferida = (short)(AV17Conferida+1);
                           }
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(14);
                        }
                        pr_default.close(14);
                        if ( AV108GXLvl318 == 0 )
                        {
                           AV27linha = "Dmn: " + AV93Demanda + " sem contagem.";
                           AV44SemCnt = (short)(AV44SemCnt+1);
                           H3F0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                        }
                        pr_default.readNext(13);
                     }
                     pr_default.close(13);
                     if ( AV107GXLvl310 == 0 )
                     {
                        AV27linha = "Falta cadastrar demanda: " + AV93Demanda;
                        AV39Semcadastrar = (short)(AV39Semcadastrar+1);
                        H3F0( false, 21) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+21);
                     }
                     AV28Ln = (short)(AV28Ln+1);
                  }
               }
               else if ( StringUtil.StrCmp(AV10Acao, "V") == 0 )
               {
                  while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text)) )
                  {
                     AV93Demanda = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text);
                     /* Execute user subroutine: 'LIMPARDEMANDA' */
                     S161 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV34PFBFS = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV15ColPB, 1, 1).Number);
                     AV36PFLFS = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV16ColPL, 1, 1).Number);
                     AV109GXLvl363 = 0;
                     pr_default.dynParam(15, new Object[]{ new Object[]{
                                                          AV92DemandaFM ,
                                                          A493ContagemResultado_DemandaFM ,
                                                          A484ContagemResultado_StatusDmn ,
                                                          A52Contratada_AreaTrabalhoCod ,
                                                          AV43WWPContext.gxTpr_Areatrabalho_codigo ,
                                                          A490ContagemResultado_ContratadaCod ,
                                                          AV82Contratada ,
                                                          AV93Demanda ,
                                                          A457ContagemResultado_Demanda },
                                                          new int[] {
                                                          TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                          TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                          }
                     });
                     /* Using cursor P003F17 */
                     pr_default.execute(15, new Object[] {AV93Demanda, AV43WWPContext.gxTpr_Areatrabalho_codigo, AV82Contratada, AV92DemandaFM});
                     while ( (pr_default.getStatus(15) != 101) )
                     {
                        A1553ContagemResultado_CntSrvCod = P003F17_A1553ContagemResultado_CntSrvCod[0];
                        n1553ContagemResultado_CntSrvCod = P003F17_n1553ContagemResultado_CntSrvCod[0];
                        A1603ContagemResultado_CntCod = P003F17_A1603ContagemResultado_CntCod[0];
                        n1603ContagemResultado_CntCod = P003F17_n1603ContagemResultado_CntCod[0];
                        A456ContagemResultado_Codigo = P003F17_A456ContagemResultado_Codigo[0];
                        A1617ContagemResultado_CntClcDvr = P003F17_A1617ContagemResultado_CntClcDvr[0];
                        n1617ContagemResultado_CntClcDvr = P003F17_n1617ContagemResultado_CntClcDvr[0];
                        A484ContagemResultado_StatusDmn = P003F17_A484ContagemResultado_StatusDmn[0];
                        n484ContagemResultado_StatusDmn = P003F17_n484ContagemResultado_StatusDmn[0];
                        A493ContagemResultado_DemandaFM = P003F17_A493ContagemResultado_DemandaFM[0];
                        n493ContagemResultado_DemandaFM = P003F17_n493ContagemResultado_DemandaFM[0];
                        A490ContagemResultado_ContratadaCod = P003F17_A490ContagemResultado_ContratadaCod[0];
                        n490ContagemResultado_ContratadaCod = P003F17_n490ContagemResultado_ContratadaCod[0];
                        A52Contratada_AreaTrabalhoCod = P003F17_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F17_n52Contratada_AreaTrabalhoCod[0];
                        A457ContagemResultado_Demanda = P003F17_A457ContagemResultado_Demanda[0];
                        n457ContagemResultado_Demanda = P003F17_n457ContagemResultado_Demanda[0];
                        A1603ContagemResultado_CntCod = P003F17_A1603ContagemResultado_CntCod[0];
                        n1603ContagemResultado_CntCod = P003F17_n1603ContagemResultado_CntCod[0];
                        A1617ContagemResultado_CntClcDvr = P003F17_A1617ContagemResultado_CntClcDvr[0];
                        n1617ContagemResultado_CntClcDvr = P003F17_n1617ContagemResultado_CntClcDvr[0];
                        A52Contratada_AreaTrabalhoCod = P003F17_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F17_n52Contratada_AreaTrabalhoCod[0];
                        AV109GXLvl363 = 1;
                        AV26Flag = false;
                        AV110GXLvl372 = 0;
                        /* Using cursor P003F18 */
                        pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(16) != 101) )
                        {
                           A517ContagemResultado_Ultima = P003F18_A517ContagemResultado_Ultima[0];
                           A460ContagemResultado_PFBFM = P003F18_A460ContagemResultado_PFBFM[0];
                           n460ContagemResultado_PFBFM = P003F18_n460ContagemResultado_PFBFM[0];
                           A461ContagemResultado_PFLFM = P003F18_A461ContagemResultado_PFLFM[0];
                           n461ContagemResultado_PFLFM = P003F18_n461ContagemResultado_PFLFM[0];
                           A483ContagemResultado_StatusCnt = P003F18_A483ContagemResultado_StatusCnt[0];
                           A458ContagemResultado_PFBFS = P003F18_A458ContagemResultado_PFBFS[0];
                           n458ContagemResultado_PFBFS = P003F18_n458ContagemResultado_PFBFS[0];
                           A459ContagemResultado_PFLFS = P003F18_A459ContagemResultado_PFLFS[0];
                           n459ContagemResultado_PFLFS = P003F18_n459ContagemResultado_PFLFS[0];
                           A462ContagemResultado_Divergencia = P003F18_A462ContagemResultado_Divergencia[0];
                           A473ContagemResultado_DataCnt = P003F18_A473ContagemResultado_DataCnt[0];
                           A511ContagemResultado_HoraCnt = P003F18_A511ContagemResultado_HoraCnt[0];
                           AV110GXLvl372 = 1;
                           AV26Flag = true;
                           AV51ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                           AV53ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                           AV8CalculoDivergencia = A1617ContagemResultado_CntClcDvr;
                           /* Execute user subroutine: 'CALCULADIVERGENCIA' */
                           S141 ();
                           if ( returnInSub )
                           {
                              pr_default.close(16);
                              this.cleanup();
                              if (true) return;
                           }
                           A483ContagemResultado_StatusCnt = AV55ContagemResultado_StatusCnt;
                           A458ContagemResultado_PFBFS = AV34PFBFS;
                           n458ContagemResultado_PFBFS = false;
                           A459ContagemResultado_PFLFS = AV36PFLFS;
                           n459ContagemResultado_PFLFS = false;
                           A462ContagemResultado_Divergencia = AV19ContagemResultado_Divergencia;
                           AV17Conferida = (short)(AV17Conferida+1);
                           AV27linha = "Dmn: " + AV93Demanda + "  -  Sistema FM: (" + StringUtil.Trim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)) + ") Planilha FS: (" + StringUtil.Trim( StringUtil.Str( AV34PFBFS, 10, 3)) + "," + StringUtil.Trim( StringUtil.Str( AV36PFLFS, 10, 3)) + ") " + "Status: " + gxdomainstatuscontagem.getDescription(context,AV55ContagemResultado_StatusCnt) + ((Convert.ToDecimal(0)==AV19ContagemResultado_Divergencia) ? "" : " "+StringUtil.Trim( StringUtil.Str( AV19ContagemResultado_Divergencia, 6, 2))+"%");
                           H3F0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P003F19 */
                           pr_default.execute(17, new Object[] {A483ContagemResultado_StatusCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(17);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           if (true) break;
                           /* Using cursor P003F20 */
                           pr_default.execute(18, new Object[] {A483ContagemResultado_StatusCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(18);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           pr_default.readNext(16);
                        }
                        pr_default.close(16);
                        if ( AV110GXLvl372 == 0 )
                        {
                           AV26Flag = false;
                           AV27linha = "Dmn: " + AV93Demanda + " sem contagem.";
                           H3F0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                           AV44SemCnt = (short)(AV44SemCnt+1);
                        }
                        if ( AV26Flag )
                        {
                           A484ContagemResultado_StatusDmn = AV21ContagemResultado_StatusDmn;
                           n484ContagemResultado_StatusDmn = false;
                        }
                        /* Using cursor P003F21 */
                        pr_default.execute(19, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                        pr_default.close(19);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                        pr_default.readNext(15);
                     }
                     pr_default.close(15);
                     if ( AV109GXLvl363 == 0 )
                     {
                        AV27linha = "Falta cadastrar demanda: " + AV93Demanda;
                        AV39Semcadastrar = (short)(AV39Semcadastrar+1);
                        H3F0( false, 21) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+21);
                     }
                     AV28Ln = (short)(AV28Ln+1);
                  }
               }
               else if ( StringUtil.StrCmp(AV10Acao, "A") == 0 )
               {
                  while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text)) )
                  {
                     AV93Demanda = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text);
                     /* Execute user subroutine: 'LIMPARDEMANDA' */
                     S161 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV34PFBFS = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV15ColPB, 1, 1).Number);
                     AV36PFLFS = (decimal)(AV24ExcelDocument.get_Cells(AV28Ln, AV16ColPL, 1, 1).Number);
                     AV111GXLvl420 = 0;
                     pr_default.dynParam(20, new Object[]{ new Object[]{
                                                          AV92DemandaFM ,
                                                          A493ContagemResultado_DemandaFM ,
                                                          A484ContagemResultado_StatusDmn ,
                                                          A52Contratada_AreaTrabalhoCod ,
                                                          AV43WWPContext.gxTpr_Areatrabalho_codigo ,
                                                          A490ContagemResultado_ContratadaCod ,
                                                          AV82Contratada ,
                                                          AV93Demanda ,
                                                          A457ContagemResultado_Demanda },
                                                          new int[] {
                                                          TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                          TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                          }
                     });
                     /* Using cursor P003F22 */
                     pr_default.execute(20, new Object[] {AV93Demanda, AV43WWPContext.gxTpr_Areatrabalho_codigo, AV82Contratada, AV92DemandaFM});
                     while ( (pr_default.getStatus(20) != 101) )
                     {
                        A1553ContagemResultado_CntSrvCod = P003F22_A1553ContagemResultado_CntSrvCod[0];
                        n1553ContagemResultado_CntSrvCod = P003F22_n1553ContagemResultado_CntSrvCod[0];
                        A1603ContagemResultado_CntCod = P003F22_A1603ContagemResultado_CntCod[0];
                        n1603ContagemResultado_CntCod = P003F22_n1603ContagemResultado_CntCod[0];
                        A456ContagemResultado_Codigo = P003F22_A456ContagemResultado_Codigo[0];
                        A1617ContagemResultado_CntClcDvr = P003F22_A1617ContagemResultado_CntClcDvr[0];
                        n1617ContagemResultado_CntClcDvr = P003F22_n1617ContagemResultado_CntClcDvr[0];
                        A484ContagemResultado_StatusDmn = P003F22_A484ContagemResultado_StatusDmn[0];
                        n484ContagemResultado_StatusDmn = P003F22_n484ContagemResultado_StatusDmn[0];
                        A493ContagemResultado_DemandaFM = P003F22_A493ContagemResultado_DemandaFM[0];
                        n493ContagemResultado_DemandaFM = P003F22_n493ContagemResultado_DemandaFM[0];
                        A490ContagemResultado_ContratadaCod = P003F22_A490ContagemResultado_ContratadaCod[0];
                        n490ContagemResultado_ContratadaCod = P003F22_n490ContagemResultado_ContratadaCod[0];
                        A52Contratada_AreaTrabalhoCod = P003F22_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F22_n52Contratada_AreaTrabalhoCod[0];
                        A457ContagemResultado_Demanda = P003F22_A457ContagemResultado_Demanda[0];
                        n457ContagemResultado_Demanda = P003F22_n457ContagemResultado_Demanda[0];
                        A1603ContagemResultado_CntCod = P003F22_A1603ContagemResultado_CntCod[0];
                        n1603ContagemResultado_CntCod = P003F22_n1603ContagemResultado_CntCod[0];
                        A1617ContagemResultado_CntClcDvr = P003F22_A1617ContagemResultado_CntClcDvr[0];
                        n1617ContagemResultado_CntClcDvr = P003F22_n1617ContagemResultado_CntClcDvr[0];
                        A52Contratada_AreaTrabalhoCod = P003F22_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F22_n52Contratada_AreaTrabalhoCod[0];
                        AV111GXLvl420 = 1;
                        /* Using cursor P003F23 */
                        pr_default.execute(21, new Object[] {A456ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(21) != 101) )
                        {
                           A517ContagemResultado_Ultima = P003F23_A517ContagemResultado_Ultima[0];
                           A458ContagemResultado_PFBFS = P003F23_A458ContagemResultado_PFBFS[0];
                           n458ContagemResultado_PFBFS = P003F23_n458ContagemResultado_PFBFS[0];
                           A459ContagemResultado_PFLFS = P003F23_A459ContagemResultado_PFLFS[0];
                           n459ContagemResultado_PFLFS = P003F23_n459ContagemResultado_PFLFS[0];
                           A460ContagemResultado_PFBFM = P003F23_A460ContagemResultado_PFBFM[0];
                           n460ContagemResultado_PFBFM = P003F23_n460ContagemResultado_PFBFM[0];
                           A483ContagemResultado_StatusCnt = P003F23_A483ContagemResultado_StatusCnt[0];
                           A461ContagemResultado_PFLFM = P003F23_A461ContagemResultado_PFLFM[0];
                           n461ContagemResultado_PFLFM = P003F23_n461ContagemResultado_PFLFM[0];
                           A462ContagemResultado_Divergencia = P003F23_A462ContagemResultado_Divergencia[0];
                           A473ContagemResultado_DataCnt = P003F23_A473ContagemResultado_DataCnt[0];
                           A511ContagemResultado_HoraCnt = P003F23_A511ContagemResultado_HoraCnt[0];
                           if ( ( AV34PFBFS + AV36PFLFS > Convert.ToDecimal( 0 )) )
                           {
                              A458ContagemResultado_PFBFS = AV34PFBFS;
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = AV36PFLFS;
                              n459ContagemResultado_PFLFS = false;
                           }
                           if ( (Convert.ToDecimal(0)==A460ContagemResultado_PFBFM) )
                           {
                              if ( ( AV34PFBFS + AV36PFLFS > Convert.ToDecimal( 0 )) )
                              {
                                 A483ContagemResultado_StatusCnt = 7;
                              }
                              AV21ContagemResultado_StatusDmn = "A";
                              AV27linha = "Demanda: " + AV93Demanda + " sem PFB FM para c�lculo de diverg�ncia";
                              H3F0( false, 21) ;
                              getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                              getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                              Gx_OldLine = Gx_line;
                              Gx_line = (int)(Gx_line+21);
                           }
                           else
                           {
                              GXt_decimal1 = AV19ContagemResultado_Divergencia;
                              new prc_calculardivergencia(context ).execute(  A1617ContagemResultado_CntClcDvr,  AV34PFBFS,  A460ContagemResultado_PFBFM,  AV36PFLFS,  A461ContagemResultado_PFLFM, out  GXt_decimal1) ;
                              AV19ContagemResultado_Divergencia = GXt_decimal1;
                              if ( AV19ContagemResultado_Divergencia > AV23DivergenciaAceitavel )
                              {
                                 if ( ( AV34PFBFS + AV36PFLFS > Convert.ToDecimal( 0 )) )
                                 {
                                    A462ContagemResultado_Divergencia = AV19ContagemResultado_Divergencia;
                                    A483ContagemResultado_StatusCnt = 7;
                                 }
                                 AV21ContagemResultado_StatusDmn = "A";
                                 AV27linha = "Demanda: " + AV93Demanda + " com diverg�ncia" + "  -  Sistema FM (" + StringUtil.Trim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)) + ") Planilha FS (" + StringUtil.Trim( StringUtil.Str( AV34PFBFS, 10, 3)) + "," + StringUtil.Trim( StringUtil.Str( AV36PFLFS, 10, 3)) + ") Div: " + StringUtil.Trim( StringUtil.Str( AV19ContagemResultado_Divergencia, 6, 2)) + "%";
                                 H3F0( false, 21) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+21);
                              }
                              else
                              {
                                 if ( ( AV34PFBFS + AV36PFLFS > Convert.ToDecimal( 0 )) )
                                 {
                                    A462ContagemResultado_Divergencia = AV19ContagemResultado_Divergencia;
                                    A483ContagemResultado_StatusCnt = 5;
                                 }
                                 AV21ContagemResultado_StatusDmn = "C";
                                 AV27linha = "Demanda: " + AV93Demanda + " Aprovada" + "  -  Sistema FM (" + StringUtil.Trim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)) + ") Planilha FS (" + StringUtil.Trim( StringUtil.Str( AV34PFBFS, 10, 3)) + "," + StringUtil.Trim( StringUtil.Str( AV36PFLFS, 10, 3)) + ") Div: " + StringUtil.Trim( StringUtil.Str( AV19ContagemResultado_Divergencia, 6, 2)) + "%";
                                 H3F0( false, 21) ;
                                 getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                                 getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                                 Gx_OldLine = Gx_line;
                                 Gx_line = (int)(Gx_line+21);
                              }
                           }
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P003F24 */
                           pr_default.execute(22, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(22);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           if (true) break;
                           /* Using cursor P003F25 */
                           pr_default.execute(23, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A483ContagemResultado_StatusCnt, A462ContagemResultado_Divergencia, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(23);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           pr_default.readNext(21);
                        }
                        pr_default.close(21);
                        if ( ( AV34PFBFS + AV36PFLFS > Convert.ToDecimal( 0 )) )
                        {
                           A484ContagemResultado_StatusDmn = AV21ContagemResultado_StatusDmn;
                           n484ContagemResultado_StatusDmn = false;
                        }
                        /* Using cursor P003F26 */
                        pr_default.execute(24, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                        pr_default.close(24);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                        pr_default.readNext(20);
                     }
                     pr_default.close(20);
                     if ( AV111GXLvl420 == 0 )
                     {
                        AV27linha = "Falta cadastrar demanda: " + AV93Demanda;
                        AV39Semcadastrar = (short)(AV39Semcadastrar+1);
                        H3F0( false, 21) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+21);
                     }
                     AV28Ln = (short)(AV28Ln+1);
                  }
               }
               else if ( StringUtil.StrCmp(AV10Acao, "Z") == 0 )
               {
                  while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text)) )
                  {
                     AV93Demanda = StringUtil.Trim( AV24ExcelDocument.get_Cells(AV28Ln, AV14ColDmn, 1, 1).Text);
                     /* Execute user subroutine: 'LIMPARDEMANDA' */
                     S161 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV113GXLvl486 = 0;
                     pr_default.dynParam(25, new Object[]{ new Object[]{
                                                          AV92DemandaFM ,
                                                          A493ContagemResultado_DemandaFM ,
                                                          A484ContagemResultado_StatusDmn ,
                                                          A52Contratada_AreaTrabalhoCod ,
                                                          AV43WWPContext.gxTpr_Areatrabalho_codigo ,
                                                          A490ContagemResultado_ContratadaCod ,
                                                          AV82Contratada ,
                                                          AV93Demanda ,
                                                          A457ContagemResultado_Demanda },
                                                          new int[] {
                                                          TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                          TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                          }
                     });
                     /* Using cursor P003F27 */
                     pr_default.execute(25, new Object[] {AV93Demanda, AV43WWPContext.gxTpr_Areatrabalho_codigo, AV82Contratada, AV92DemandaFM});
                     while ( (pr_default.getStatus(25) != 101) )
                     {
                        A456ContagemResultado_Codigo = P003F27_A456ContagemResultado_Codigo[0];
                        A484ContagemResultado_StatusDmn = P003F27_A484ContagemResultado_StatusDmn[0];
                        n484ContagemResultado_StatusDmn = P003F27_n484ContagemResultado_StatusDmn[0];
                        A493ContagemResultado_DemandaFM = P003F27_A493ContagemResultado_DemandaFM[0];
                        n493ContagemResultado_DemandaFM = P003F27_n493ContagemResultado_DemandaFM[0];
                        A490ContagemResultado_ContratadaCod = P003F27_A490ContagemResultado_ContratadaCod[0];
                        n490ContagemResultado_ContratadaCod = P003F27_n490ContagemResultado_ContratadaCod[0];
                        A52Contratada_AreaTrabalhoCod = P003F27_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F27_n52Contratada_AreaTrabalhoCod[0];
                        A457ContagemResultado_Demanda = P003F27_A457ContagemResultado_Demanda[0];
                        n457ContagemResultado_Demanda = P003F27_n457ContagemResultado_Demanda[0];
                        A52Contratada_AreaTrabalhoCod = P003F27_A52Contratada_AreaTrabalhoCod[0];
                        n52Contratada_AreaTrabalhoCod = P003F27_n52Contratada_AreaTrabalhoCod[0];
                        AV113GXLvl486 = 1;
                        AV114GXLvl493 = 0;
                        /* Using cursor P003F28 */
                        pr_default.execute(26, new Object[] {A456ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(26) != 101) )
                        {
                           A517ContagemResultado_Ultima = P003F28_A517ContagemResultado_Ultima[0];
                           A458ContagemResultado_PFBFS = P003F28_A458ContagemResultado_PFBFS[0];
                           n458ContagemResultado_PFBFS = P003F28_n458ContagemResultado_PFBFS[0];
                           A459ContagemResultado_PFLFS = P003F28_A459ContagemResultado_PFLFS[0];
                           n459ContagemResultado_PFLFS = P003F28_n459ContagemResultado_PFLFS[0];
                           A473ContagemResultado_DataCnt = P003F28_A473ContagemResultado_DataCnt[0];
                           A511ContagemResultado_HoraCnt = P003F28_A511ContagemResultado_HoraCnt[0];
                           AV114GXLvl493 = 1;
                           A458ContagemResultado_PFBFS = 0;
                           n458ContagemResultado_PFBFS = false;
                           A459ContagemResultado_PFLFS = 0;
                           n459ContagemResultado_PFLFS = false;
                           AV17Conferida = (short)(AV17Conferida+1);
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           /* Using cursor P003F29 */
                           pr_default.execute(27, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(27);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           if (true) break;
                           /* Using cursor P003F30 */
                           pr_default.execute(28, new Object[] {n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                           pr_default.close(28);
                           dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                           pr_default.readNext(26);
                        }
                        pr_default.close(26);
                        if ( AV114GXLvl493 == 0 )
                        {
                           AV44SemCnt = (short)(AV44SemCnt+1);
                        }
                        if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 )
                        {
                           A484ContagemResultado_StatusDmn = "R";
                           n484ContagemResultado_StatusDmn = false;
                        }
                        /* Using cursor P003F31 */
                        pr_default.execute(29, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                        pr_default.close(29);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                        pr_default.readNext(25);
                     }
                     pr_default.close(25);
                     if ( AV113GXLvl486 == 0 )
                     {
                        AV39Semcadastrar = (short)(AV39Semcadastrar+1);
                     }
                     AV28Ln = (short)(AV28Ln+1);
                  }
               }
            }
            AV24ExcelDocument.Close();
            AV27linha = "";
            H3F0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            H3F0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            if ( (0==AV38PraLinha) )
            {
               AV28Ln = (short)(AV28Ln-2);
            }
            else
            {
               AV28Ln = (short)(AV28Ln-AV38PraLinha);
            }
            if ( StringUtil.StrCmp(AV30Opcao, "CTRF") == 0 )
            {
               if ( StringUtil.StrCmp(AV10Acao, "Z") == 0 )
               {
                  AV27linha = StringUtil.Space( 10) + "Linhas processadas: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV28Ln), 4, 0));
                  AV27linha = AV27linha + ", sem contagem: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV44SemCnt), 4, 0));
                  AV27linha = AV27linha + ", sem cadastrar: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV39Semcadastrar), 4, 0));
                  AV27linha = AV27linha + ", zeradas: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV17Conferida), 4, 0));
               }
               else
               {
                  AV27linha = StringUtil.Space( 10) + "Linhas processadas: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV28Ln), 4, 0));
                  AV27linha = AV27linha + ((StringUtil.StrCmp(AV10Acao, "C")==0) ? ", conferidas: " : ", atualizadas: ") + StringUtil.Trim( StringUtil.Str( (decimal)(AV17Conferida), 4, 0));
                  AV27linha = AV27linha + ".    Sem contagem: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV44SemCnt), 4, 0));
                  AV27linha = AV27linha + ", sem cadastrar: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV39Semcadastrar), 4, 0));
                  AV28Ln = (short)(AV28Ln-AV17Conferida);
                  AV27linha = AV27linha + ". A revisar: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV28Ln), 4, 0));
               }
            }
            else
            {
               AV27linha = StringUtil.Space( 10) + "Linhas processadas: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV28Ln), 4, 0));
               AV27linha = AV27linha + ", atualizadas: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV17Conferida), 4, 0));
               AV27linha = AV27linha + ", sem cadastrar: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV39Semcadastrar), 4, 0));
               AV28Ln = (short)(AV28Ln-AV17Conferida);
               AV27linha = AV27linha + ". A revisar: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV28Ln), 4, 0));
            }
            H3F0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H3F0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENFILE' Routine */
         AV24ExcelDocument.Open(AV11Arquivo);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84Aba)) )
         {
            AV24ExcelDocument.SelectSheet(AV84Aba);
         }
      }

      protected void S121( )
      {
         /* 'REGISTRAERRO' Routine */
         AV115GXLvl561 = 0;
         /* Using cursor P003F32 */
         pr_default.execute(30, new Object[] {AV88ContagemResultado_Codigo, AV72ContagemResultadoErro_Tipo});
         while ( (pr_default.getStatus(30) != 101) )
         {
            A579ContagemResultadoErro_Tipo = P003F32_A579ContagemResultadoErro_Tipo[0];
            A456ContagemResultado_Codigo = P003F32_A456ContagemResultado_Codigo[0];
            A580ContagemResultadoErro_Data = P003F32_A580ContagemResultadoErro_Data[0];
            A581ContagemResultadoErro_Status = P003F32_A581ContagemResultadoErro_Status[0];
            AV115GXLvl561 = 1;
            A580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            A581ContagemResultadoErro_Status = "P";
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P003F33 */
            pr_default.execute(31, new Object[] {A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(31);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            if (true) break;
            /* Using cursor P003F34 */
            pr_default.execute(32, new Object[] {A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(32);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(30);
         if ( AV115GXLvl561 == 0 )
         {
            /* Using cursor P003F36 */
            pr_default.execute(33, new Object[] {AV88ContagemResultado_Codigo});
            while ( (pr_default.getStatus(33) != 101) )
            {
               A456ContagemResultado_Codigo = P003F36_A456ContagemResultado_Codigo[0];
               A508ContagemResultado_Owner = P003F36_A508ContagemResultado_Owner[0];
               A584ContagemResultado_ContadorFM = P003F36_A584ContagemResultado_ContadorFM[0];
               n584ContagemResultado_ContadorFM = P003F36_n584ContagemResultado_ContadorFM[0];
               A584ContagemResultado_ContadorFM = P003F36_A584ContagemResultado_ContadorFM[0];
               n584ContagemResultado_ContadorFM = P003F36_n584ContagemResultado_ContadorFM[0];
               if ( (0==A584ContagemResultado_ContadorFM) )
               {
                  AV74ContagemResultadoErro_ContadorFMCod = A508ContagemResultado_Owner;
               }
               else
               {
                  AV74ContagemResultadoErro_ContadorFMCod = A584ContagemResultado_ContadorFM;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(33);
            /*
               INSERT RECORD ON TABLE ContagemResultadoErro

            */
            A456ContagemResultado_Codigo = AV88ContagemResultado_Codigo;
            A579ContagemResultadoErro_Tipo = AV72ContagemResultadoErro_Tipo;
            A583ContagemResultadoErro_ContadorFMCod = AV74ContagemResultadoErro_ContadorFMCod;
            A580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            A581ContagemResultadoErro_Status = "P";
            /* Using cursor P003F37 */
            pr_default.execute(34, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo, A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A583ContagemResultadoErro_ContadorFMCod});
            pr_default.close(34);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            if ( (pr_default.getStatus(34) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
      }

      protected void S131( )
      {
         /* 'ATUALIZAERRO' Routine */
         /* Using cursor P003F38 */
         pr_default.execute(35, new Object[] {AV88ContagemResultado_Codigo, AV72ContagemResultadoErro_Tipo});
         while ( (pr_default.getStatus(35) != 101) )
         {
            A579ContagemResultadoErro_Tipo = P003F38_A579ContagemResultadoErro_Tipo[0];
            A456ContagemResultado_Codigo = P003F38_A456ContagemResultado_Codigo[0];
            A581ContagemResultadoErro_Status = P003F38_A581ContagemResultadoErro_Status[0];
            if ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "P") == 0 )
            {
               A581ContagemResultadoErro_Status = "C";
            }
            else if ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "E") == 0 )
            {
               A581ContagemResultadoErro_Status = "R";
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P003F39 */
            pr_default.execute(36, new Object[] {A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(36);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            if (true) break;
            /* Using cursor P003F40 */
            pr_default.execute(37, new Object[] {A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(37);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(35);
      }

      protected void S141( )
      {
         /* 'CALCULADIVERGENCIA' Routine */
         GXt_decimal1 = AV19ContagemResultado_Divergencia;
         new prc_calculardivergencia(context ).execute(  AV8CalculoDivergencia,  AV34PFBFS,  AV51ContagemResultado_PFBFM,  AV36PFLFS,  AV53ContagemResultado_PFLFM, out  GXt_decimal1) ;
         AV19ContagemResultado_Divergencia = GXt_decimal1;
         if ( AV19ContagemResultado_Divergencia > AV23DivergenciaAceitavel )
         {
            AV55ContagemResultado_StatusCnt = 7;
            AV21ContagemResultado_StatusDmn = "A";
         }
         else
         {
            AV55ContagemResultado_StatusCnt = 5;
            AV21ContagemResultado_StatusDmn = "R";
         }
      }

      protected void S151( )
      {
         /* 'STATUSOSVNC' Routine */
         pr_default.dynParam(38, new Object[]{ new Object[]{
                                              AV86ContagemResultado_OSVinculada ,
                                              A456ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada ,
                                              AV88ContagemResultado_Codigo ,
                                              A484ContagemResultado_StatusDmn },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P003F41 */
         pr_default.execute(38, new Object[] {AV86ContagemResultado_OSVinculada, AV88ContagemResultado_Codigo});
         while ( (pr_default.getStatus(38) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P003F41_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003F41_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P003F41_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003F41_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P003F41_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003F41_n484ContagemResultado_StatusDmn[0];
            A602ContagemResultado_OSVinculada = P003F41_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P003F41_n602ContagemResultado_OSVinculada[0];
            A456ContagemResultado_Codigo = P003F41_A456ContagemResultado_Codigo[0];
            A801ContagemResultado_ServicoSigla = P003F41_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003F41_n801ContagemResultado_ServicoSigla[0];
            A601ContagemResultado_Servico = P003F41_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003F41_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P003F41_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003F41_n801ContagemResultado_ServicoSigla[0];
            AV68Ok = false;
            AV27linha = "Demanda: " + AV93Demanda + " com " + StringUtil.Trim( A801ContagemResultado_ServicoSigla) + " em Aberto";
            H3F0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV27linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            pr_default.readNext(38);
         }
         pr_default.close(38);
      }

      protected void S161( )
      {
         /* 'LIMPARDEMANDA' Routine */
         AV93Demanda = StringUtil.Trim( AV93Demanda);
         AV93Demanda = StringUtil.StringReplace( AV93Demanda, "'", "");
         AV93Demanda = StringUtil.StringReplace( AV93Demanda, "\"", "");
         AV93Demanda = StringUtil.StringReplace( AV93Demanda, "�", "");
         if ( StringUtil.Len( AV93Demanda) > 30 )
         {
            AV93Demanda = StringUtil.Substring( AV93Demanda, 1, 28);
         }
      }

      protected void H3F0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 667, Gx_line+0, 702, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawLine(0, Gx_line+117, 811, Gx_line+117, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 0, Gx_line+0, 120, Gx_line+100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 700, Gx_line+5, 749, Gx_line+20, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 758, Gx_line+5, 824, Gx_line+20, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV66NomeArq, "")), 140, Gx_line+50, 662, Gx_line+65, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV83SubTitulo, "")), 140, Gx_line+67, 662, Gx_line+82, 1+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV69Periodo, "")), 325, Gx_line+29, 482, Gx_line+46, 1+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV42Titulo, "")), 150, Gx_line+0, 651, Gx_line+22, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+133);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      protected void Executebatchp003f12( )
      {
         /* Using cursor P003F12 */
         pr_default.executeBatch(10);
         pr_default.close(10);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "REL_ConferenciaMS");
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(10);
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV43WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV66NomeArq = "";
         AV67NomeArq2 = "";
         scmdbuf = "";
         P003F2_A40Contratada_PessoaCod = new int[1] ;
         P003F2_A39Contratada_Codigo = new int[1] ;
         P003F2_A41Contratada_PessoaNom = new String[] {""} ;
         P003F2_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         AV83SubTitulo = "";
         P003F3_A516Contratada_TipoFabrica = new String[] {""} ;
         P003F3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003F3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003F3_A39Contratada_Codigo = new int[1] ;
         A516Contratada_TipoFabrica = "";
         AV27linha = "";
         AV42Titulo = "";
         AV24ExcelDocument = new ExcelDocumentI();
         AV70Sistema_Sigla = "";
         P003F4_A129Sistema_Sigla = new String[] {""} ;
         P003F4_A127Sistema_Codigo = new int[1] ;
         A129Sistema_Sigla = "";
         AV59ContratadaUsuario_UsuarioPessoaNom = "";
         P003F5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P003F5_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P003F5_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P003F5_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P003F5_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P003F5_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P003F5_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P003F5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         AV93Demanda = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         P003F6_A456ContagemResultado_Codigo = new int[1] ;
         Gx_emsg = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         Gx_time = "";
         AV62Date = DateTime.MinValue;
         AV61DataIni = DateTime.MinValue;
         AV60DataFim = DateTime.MinValue;
         AV69Periodo = "";
         P003F8_A489ContagemResultado_SistemaCod = new int[1] ;
         P003F8_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003F8_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P003F8_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P003F8_A457ContagemResultado_Demanda = new String[] {""} ;
         P003F8_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003F8_A456ContagemResultado_Codigo = new int[1] ;
         P003F8_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003F8_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003F8_A602ContagemResultado_OSVinculada = new int[1] ;
         P003F8_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P003F8_A494ContagemResultado_Descricao = new String[] {""} ;
         P003F8_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003F8_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P003F8_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         A494ContagemResultado_Descricao = "";
         A515ContagemResultado_SistemaCoord = "";
         OV48ContagemResultado_HoraCnt = "";
         AV48ContagemResultado_HoraCnt = context.localUtil.Time( );
         AV56ContagemResultado_Ultima = true;
         AV57ContagemResultadoContagens_Esforco = 0;
         AV72ContagemResultadoErro_Tipo = "";
         P003F9_A456ContagemResultado_Codigo = new int[1] ;
         P003F9_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003F9_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003F9_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003F9_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P003F9_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P003F9_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003F9_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003F9_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003F9_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003F9_A461ContagemResultado_PFLFM = new decimal[1] ;
         P003F9_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P003F9_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003F9_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003F9_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P003F9_A462ContagemResultado_Divergencia = new decimal[1] ;
         P003F9_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P003F9_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P003F9_A483ContagemResultado_StatusCnt = new short[1] ;
         P003F9_A482ContagemResultadoContagens_Esforco = new short[1] ;
         A463ContagemResultado_ParecerTcn = "";
         AV47ContagemResultado_DataCnt = DateTime.MinValue;
         AV50ContagemResultado_ParecerTcn = "";
         P003F12_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003F12_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003F12_A456ContagemResultado_Codigo = new int[1] ;
         P003F13_A39Contratada_Codigo = new int[1] ;
         P003F13_A516Contratada_TipoFabrica = new String[] {""} ;
         P003F13_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003F13_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003F14_A39Contratada_Codigo = new int[1] ;
         P003F14_A452Contrato_CalculoDivergencia = new String[] {""} ;
         P003F14_A453Contrato_IndiceDivergencia = new decimal[1] ;
         P003F14_A74Contrato_Codigo = new int[1] ;
         A452Contrato_CalculoDivergencia = "";
         AV8CalculoDivergencia = "";
         P003F15_A456ContagemResultado_Codigo = new int[1] ;
         P003F15_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003F15_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003F15_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003F15_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003F15_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003F15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003F15_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003F15_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003F15_A457ContagemResultado_Demanda = new String[] {""} ;
         P003F15_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003F16_A456ContagemResultado_Codigo = new int[1] ;
         P003F16_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003F16_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003F16_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003F16_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003F16_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003F16_A461ContagemResultado_PFLFM = new decimal[1] ;
         P003F16_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P003F16_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003F16_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003F16_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003F16_A511ContagemResultado_HoraCnt = new String[] {""} ;
         AV91str = "";
         P003F17_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003F17_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003F17_A1603ContagemResultado_CntCod = new int[1] ;
         P003F17_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003F17_A456ContagemResultado_Codigo = new int[1] ;
         P003F17_A1617ContagemResultado_CntClcDvr = new String[] {""} ;
         P003F17_n1617ContagemResultado_CntClcDvr = new bool[] {false} ;
         P003F17_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003F17_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003F17_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003F17_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003F17_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003F17_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003F17_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003F17_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003F17_A457ContagemResultado_Demanda = new String[] {""} ;
         P003F17_n457ContagemResultado_Demanda = new bool[] {false} ;
         A1617ContagemResultado_CntClcDvr = "";
         P003F18_A456ContagemResultado_Codigo = new int[1] ;
         P003F18_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003F18_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003F18_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003F18_A461ContagemResultado_PFLFM = new decimal[1] ;
         P003F18_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P003F18_A483ContagemResultado_StatusCnt = new short[1] ;
         P003F18_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003F18_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003F18_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003F18_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003F18_A462ContagemResultado_Divergencia = new decimal[1] ;
         P003F18_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003F18_A511ContagemResultado_HoraCnt = new String[] {""} ;
         AV21ContagemResultado_StatusDmn = "";
         P003F22_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003F22_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003F22_A1603ContagemResultado_CntCod = new int[1] ;
         P003F22_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P003F22_A456ContagemResultado_Codigo = new int[1] ;
         P003F22_A1617ContagemResultado_CntClcDvr = new String[] {""} ;
         P003F22_n1617ContagemResultado_CntClcDvr = new bool[] {false} ;
         P003F22_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003F22_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003F22_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003F22_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003F22_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003F22_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003F22_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003F22_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003F22_A457ContagemResultado_Demanda = new String[] {""} ;
         P003F22_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003F23_A456ContagemResultado_Codigo = new int[1] ;
         P003F23_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003F23_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003F23_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003F23_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003F23_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003F23_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003F23_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003F23_A483ContagemResultado_StatusCnt = new short[1] ;
         P003F23_A461ContagemResultado_PFLFM = new decimal[1] ;
         P003F23_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P003F23_A462ContagemResultado_Divergencia = new decimal[1] ;
         P003F23_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003F23_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003F27_A456ContagemResultado_Codigo = new int[1] ;
         P003F27_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003F27_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003F27_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003F27_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003F27_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003F27_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003F27_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003F27_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003F27_A457ContagemResultado_Demanda = new String[] {""} ;
         P003F27_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003F28_A456ContagemResultado_Codigo = new int[1] ;
         P003F28_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003F28_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003F28_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003F28_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003F28_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003F28_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003F28_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003F32_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         P003F32_A456ContagemResultado_Codigo = new int[1] ;
         P003F32_A580ContagemResultadoErro_Data = new DateTime[] {DateTime.MinValue} ;
         P003F32_A581ContagemResultadoErro_Status = new String[] {""} ;
         A579ContagemResultadoErro_Tipo = "";
         A580ContagemResultadoErro_Data = (DateTime)(DateTime.MinValue);
         A581ContagemResultadoErro_Status = "";
         P003F36_A456ContagemResultado_Codigo = new int[1] ;
         P003F36_A508ContagemResultado_Owner = new int[1] ;
         P003F36_A584ContagemResultado_ContadorFM = new int[1] ;
         P003F36_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         P003F38_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         P003F38_A456ContagemResultado_Codigo = new int[1] ;
         P003F38_A581ContagemResultadoErro_Status = new String[] {""} ;
         P003F41_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003F41_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003F41_A601ContagemResultado_Servico = new int[1] ;
         P003F41_n601ContagemResultado_Servico = new bool[] {false} ;
         P003F41_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003F41_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003F41_A602ContagemResultado_OSVinculada = new int[1] ;
         P003F41_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P003F41_A456ContagemResultado_Codigo = new int[1] ;
         P003F41_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P003F41_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_conferenciams__default(),
            new Object[][] {
                new Object[] {
               P003F2_A40Contratada_PessoaCod, P003F2_A39Contratada_Codigo, P003F2_A41Contratada_PessoaNom, P003F2_n41Contratada_PessoaNom
               }
               , new Object[] {
               P003F3_A516Contratada_TipoFabrica, P003F3_A52Contratada_AreaTrabalhoCod, P003F3_A39Contratada_Codigo
               }
               , new Object[] {
               P003F4_A129Sistema_Sigla, P003F4_A127Sistema_Codigo
               }
               , new Object[] {
               P003F5_A66ContratadaUsuario_ContratadaCod, P003F5_A70ContratadaUsuario_UsuarioPessoaCod, P003F5_n70ContratadaUsuario_UsuarioPessoaCod, P003F5_A1228ContratadaUsuario_AreaTrabalhoCod, P003F5_n1228ContratadaUsuario_AreaTrabalhoCod, P003F5_A71ContratadaUsuario_UsuarioPessoaNom, P003F5_n71ContratadaUsuario_UsuarioPessoaNom, P003F5_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P003F6_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P003F8_A489ContagemResultado_SistemaCod, P003F8_n489ContagemResultado_SistemaCod, P003F8_A805ContagemResultado_ContratadaOrigemCod, P003F8_n805ContagemResultado_ContratadaOrigemCod, P003F8_A457ContagemResultado_Demanda, P003F8_n457ContagemResultado_Demanda, P003F8_A456ContagemResultado_Codigo, P003F8_A484ContagemResultado_StatusDmn, P003F8_n484ContagemResultado_StatusDmn, P003F8_A602ContagemResultado_OSVinculada,
               P003F8_n602ContagemResultado_OSVinculada, P003F8_A494ContagemResultado_Descricao, P003F8_n494ContagemResultado_Descricao, P003F8_A515ContagemResultado_SistemaCoord, P003F8_n515ContagemResultado_SistemaCoord
               }
               , new Object[] {
               P003F9_A456ContagemResultado_Codigo, P003F9_A517ContagemResultado_Ultima, P003F9_A473ContagemResultado_DataCnt, P003F9_A511ContagemResultado_HoraCnt, P003F9_A463ContagemResultado_ParecerTcn, P003F9_n463ContagemResultado_ParecerTcn, P003F9_A460ContagemResultado_PFBFM, P003F9_n460ContagemResultado_PFBFM, P003F9_A458ContagemResultado_PFBFS, P003F9_n458ContagemResultado_PFBFS,
               P003F9_A461ContagemResultado_PFLFM, P003F9_n461ContagemResultado_PFLFM, P003F9_A459ContagemResultado_PFLFS, P003F9_n459ContagemResultado_PFLFS, P003F9_A470ContagemResultado_ContadorFMCod, P003F9_A462ContagemResultado_Divergencia, P003F9_A469ContagemResultado_NaoCnfCntCod, P003F9_n469ContagemResultado_NaoCnfCntCod, P003F9_A483ContagemResultado_StatusCnt, P003F9_A482ContagemResultadoContagens_Esforco
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003F13_A39Contratada_Codigo, P003F13_A516Contratada_TipoFabrica, P003F13_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P003F14_A39Contratada_Codigo, P003F14_A452Contrato_CalculoDivergencia, P003F14_A453Contrato_IndiceDivergencia, P003F14_A74Contrato_Codigo
               }
               , new Object[] {
               P003F15_A456ContagemResultado_Codigo, P003F15_A484ContagemResultado_StatusDmn, P003F15_n484ContagemResultado_StatusDmn, P003F15_A493ContagemResultado_DemandaFM, P003F15_n493ContagemResultado_DemandaFM, P003F15_A490ContagemResultado_ContratadaCod, P003F15_n490ContagemResultado_ContratadaCod, P003F15_A52Contratada_AreaTrabalhoCod, P003F15_n52Contratada_AreaTrabalhoCod, P003F15_A457ContagemResultado_Demanda,
               P003F15_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P003F16_A456ContagemResultado_Codigo, P003F16_A517ContagemResultado_Ultima, P003F16_A459ContagemResultado_PFLFS, P003F16_n459ContagemResultado_PFLFS, P003F16_A458ContagemResultado_PFBFS, P003F16_n458ContagemResultado_PFBFS, P003F16_A461ContagemResultado_PFLFM, P003F16_n461ContagemResultado_PFLFM, P003F16_A460ContagemResultado_PFBFM, P003F16_n460ContagemResultado_PFBFM,
               P003F16_A473ContagemResultado_DataCnt, P003F16_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               P003F17_A1553ContagemResultado_CntSrvCod, P003F17_n1553ContagemResultado_CntSrvCod, P003F17_A1603ContagemResultado_CntCod, P003F17_n1603ContagemResultado_CntCod, P003F17_A456ContagemResultado_Codigo, P003F17_A1617ContagemResultado_CntClcDvr, P003F17_n1617ContagemResultado_CntClcDvr, P003F17_A484ContagemResultado_StatusDmn, P003F17_n484ContagemResultado_StatusDmn, P003F17_A493ContagemResultado_DemandaFM,
               P003F17_n493ContagemResultado_DemandaFM, P003F17_A490ContagemResultado_ContratadaCod, P003F17_n490ContagemResultado_ContratadaCod, P003F17_A52Contratada_AreaTrabalhoCod, P003F17_n52Contratada_AreaTrabalhoCod, P003F17_A457ContagemResultado_Demanda, P003F17_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P003F18_A456ContagemResultado_Codigo, P003F18_A517ContagemResultado_Ultima, P003F18_A460ContagemResultado_PFBFM, P003F18_n460ContagemResultado_PFBFM, P003F18_A461ContagemResultado_PFLFM, P003F18_n461ContagemResultado_PFLFM, P003F18_A483ContagemResultado_StatusCnt, P003F18_A458ContagemResultado_PFBFS, P003F18_n458ContagemResultado_PFBFS, P003F18_A459ContagemResultado_PFLFS,
               P003F18_n459ContagemResultado_PFLFS, P003F18_A462ContagemResultado_Divergencia, P003F18_A473ContagemResultado_DataCnt, P003F18_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003F22_A1553ContagemResultado_CntSrvCod, P003F22_n1553ContagemResultado_CntSrvCod, P003F22_A1603ContagemResultado_CntCod, P003F22_n1603ContagemResultado_CntCod, P003F22_A456ContagemResultado_Codigo, P003F22_A1617ContagemResultado_CntClcDvr, P003F22_n1617ContagemResultado_CntClcDvr, P003F22_A484ContagemResultado_StatusDmn, P003F22_n484ContagemResultado_StatusDmn, P003F22_A493ContagemResultado_DemandaFM,
               P003F22_n493ContagemResultado_DemandaFM, P003F22_A490ContagemResultado_ContratadaCod, P003F22_n490ContagemResultado_ContratadaCod, P003F22_A52Contratada_AreaTrabalhoCod, P003F22_n52Contratada_AreaTrabalhoCod, P003F22_A457ContagemResultado_Demanda, P003F22_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P003F23_A456ContagemResultado_Codigo, P003F23_A517ContagemResultado_Ultima, P003F23_A458ContagemResultado_PFBFS, P003F23_n458ContagemResultado_PFBFS, P003F23_A459ContagemResultado_PFLFS, P003F23_n459ContagemResultado_PFLFS, P003F23_A460ContagemResultado_PFBFM, P003F23_n460ContagemResultado_PFBFM, P003F23_A483ContagemResultado_StatusCnt, P003F23_A461ContagemResultado_PFLFM,
               P003F23_n461ContagemResultado_PFLFM, P003F23_A462ContagemResultado_Divergencia, P003F23_A473ContagemResultado_DataCnt, P003F23_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003F27_A456ContagemResultado_Codigo, P003F27_A484ContagemResultado_StatusDmn, P003F27_n484ContagemResultado_StatusDmn, P003F27_A493ContagemResultado_DemandaFM, P003F27_n493ContagemResultado_DemandaFM, P003F27_A490ContagemResultado_ContratadaCod, P003F27_n490ContagemResultado_ContratadaCod, P003F27_A52Contratada_AreaTrabalhoCod, P003F27_n52Contratada_AreaTrabalhoCod, P003F27_A457ContagemResultado_Demanda,
               P003F27_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P003F28_A456ContagemResultado_Codigo, P003F28_A517ContagemResultado_Ultima, P003F28_A458ContagemResultado_PFBFS, P003F28_n458ContagemResultado_PFBFS, P003F28_A459ContagemResultado_PFLFS, P003F28_n459ContagemResultado_PFLFS, P003F28_A473ContagemResultado_DataCnt, P003F28_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003F32_A579ContagemResultadoErro_Tipo, P003F32_A456ContagemResultado_Codigo, P003F32_A580ContagemResultadoErro_Data, P003F32_A581ContagemResultadoErro_Status
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003F36_A456ContagemResultado_Codigo, P003F36_A508ContagemResultado_Owner, P003F36_A584ContagemResultado_ContadorFM, P003F36_n584ContagemResultado_ContadorFM
               }
               , new Object[] {
               }
               , new Object[] {
               P003F38_A579ContagemResultadoErro_Tipo, P003F38_A456ContagemResultado_Codigo, P003F38_A581ContagemResultadoErro_Status
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P003F41_A1553ContagemResultado_CntSrvCod, P003F41_n1553ContagemResultado_CntSrvCod, P003F41_A601ContagemResultado_Servico, P003F41_n601ContagemResultado_Servico, P003F41_A484ContagemResultado_StatusDmn, P003F41_n484ContagemResultado_StatusDmn, P003F41_A602ContagemResultado_OSVinculada, P003F41_n602ContagemResultado_OSVinculada, P003F41_A456ContagemResultado_Codigo, P003F41_A801ContagemResultado_ServicoSigla,
               P003F41_n801ContagemResultado_ServicoSigla
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV14ColDmn ;
      private short AV15ColPB ;
      private short AV16ColPL ;
      private short AV38PraLinha ;
      private short AV89ColPBFM ;
      private short AV90ColPLFM ;
      private short GxWebError ;
      private short AV65i ;
      private short AV100GXLvl32 ;
      private short AV28Ln ;
      private short AV101GXLvl47 ;
      private short AV39Semcadastrar ;
      private short AV102GXLvl60 ;
      private short A1515ContagemResultado_Evento ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short AV17Conferida ;
      private short AV103GXLvl134 ;
      private short OV57ContagemResultadoContagens_Esforco ;
      private short AV57ContagemResultadoContagens_Esforco ;
      private short AV55ContagemResultado_StatusCnt ;
      private short AV105GXLvl277 ;
      private short AV107GXLvl310 ;
      private short AV108GXLvl318 ;
      private short AV44SemCnt ;
      private short AV109GXLvl363 ;
      private short AV110GXLvl372 ;
      private short AV111GXLvl420 ;
      private short AV113GXLvl486 ;
      private short AV114GXLvl493 ;
      private short AV115GXLvl561 ;
      private int AV82Contratada ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int AV58Contratada_Codigo ;
      private int Gx_OldLine ;
      private int AV9Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV46ContagemResultado_ContadorFMCod ;
      private int GX_INS69 ;
      private int A508ContagemResultado_Owner ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A1452ContagemResultado_SS ;
      private int A456ContagemResultado_Codigo ;
      private int AV88ContagemResultado_Codigo ;
      private int GX_INS72 ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV86ContagemResultado_OSVinculada ;
      private int AV49ContagemResultado_NaoCnfCntCod ;
      private int BatchSize ;
      private int A74Contrato_Codigo ;
      private int AV43WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV74ContagemResultadoErro_ContadorFMCod ;
      private int GX_INS75 ;
      private int A583ContagemResultadoErro_ContadorFMCod ;
      private int A601ContagemResultado_Servico ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal AV34PFBFS ;
      private decimal AV33PFBFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal AV51ContagemResultado_PFBFM ;
      private decimal AV52ContagemResultado_PFBFS ;
      private decimal AV53ContagemResultado_PFLFM ;
      private decimal AV54ContagemResultado_PFLFS ;
      private decimal AV19ContagemResultado_Divergencia ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal AV23DivergenciaAceitavel ;
      private decimal AV36PFLFS ;
      private decimal AV35PFLFM ;
      private decimal GXt_decimal1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV11Arquivo ;
      private String AV12Arquivo2 ;
      private String AV84Aba ;
      private String AV30Opcao ;
      private String AV10Acao ;
      private String AV87FileName ;
      private String AV66NomeArq ;
      private String AV67NomeArq2 ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String AV83SubTitulo ;
      private String A516Contratada_TipoFabrica ;
      private String AV27linha ;
      private String AV42Titulo ;
      private String AV70Sistema_Sigla ;
      private String A129Sistema_Sigla ;
      private String AV59ContratadaUsuario_UsuarioPessoaNom ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A484ContagemResultado_StatusDmn ;
      private String Gx_emsg ;
      private String A511ContagemResultado_HoraCnt ;
      private String Gx_time ;
      private String AV69Periodo ;
      private String OV48ContagemResultado_HoraCnt ;
      private String AV48ContagemResultado_HoraCnt ;
      private String AV72ContagemResultadoErro_Tipo ;
      private String A452Contrato_CalculoDivergencia ;
      private String AV8CalculoDivergencia ;
      private String AV91str ;
      private String A1617ContagemResultado_CntClcDvr ;
      private String AV21ContagemResultado_StatusDmn ;
      private String A579ContagemResultadoErro_Tipo ;
      private String A581ContagemResultadoErro_Status ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A580ContagemResultadoErro_Data ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV62Date ;
      private DateTime AV61DataIni ;
      private DateTime AV60DataFim ;
      private DateTime AV47ContagemResultado_DataCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n41Contratada_PessoaNom ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool AV68Ok ;
      private bool AV26Flag ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool AV64Flag2 ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1452ContagemResultado_SS ;
      private bool n1515ContagemResultado_Evento ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool A517ContagemResultado_Ultima ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n494ContagemResultado_Descricao ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool OV56ContagemResultado_Ultima ;
      private bool AV56ContagemResultado_Ultima ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1617ContagemResultado_CntClcDvr ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private String A463ContagemResultado_ParecerTcn ;
      private String AV50ContagemResultado_ParecerTcn ;
      private String AV92DemandaFM ;
      private String AV93Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A515ContagemResultado_SistemaCoord ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP13_DemandaFM ;
      private IDataStoreProvider pr_default ;
      private int[] P003F2_A40Contratada_PessoaCod ;
      private int[] P003F2_A39Contratada_Codigo ;
      private String[] P003F2_A41Contratada_PessoaNom ;
      private bool[] P003F2_n41Contratada_PessoaNom ;
      private String[] P003F3_A516Contratada_TipoFabrica ;
      private int[] P003F3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003F3_n52Contratada_AreaTrabalhoCod ;
      private int[] P003F3_A39Contratada_Codigo ;
      private String[] P003F4_A129Sistema_Sigla ;
      private int[] P003F4_A127Sistema_Codigo ;
      private int[] P003F5_A66ContratadaUsuario_ContratadaCod ;
      private int[] P003F5_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P003F5_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] P003F5_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P003F5_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] P003F5_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P003F5_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] P003F5_A69ContratadaUsuario_UsuarioCod ;
      private int[] P003F6_A456ContagemResultado_Codigo ;
      private int[] P003F8_A489ContagemResultado_SistemaCod ;
      private bool[] P003F8_n489ContagemResultado_SistemaCod ;
      private int[] P003F8_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P003F8_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] P003F8_A457ContagemResultado_Demanda ;
      private bool[] P003F8_n457ContagemResultado_Demanda ;
      private int[] P003F8_A456ContagemResultado_Codigo ;
      private String[] P003F8_A484ContagemResultado_StatusDmn ;
      private bool[] P003F8_n484ContagemResultado_StatusDmn ;
      private int[] P003F8_A602ContagemResultado_OSVinculada ;
      private bool[] P003F8_n602ContagemResultado_OSVinculada ;
      private String[] P003F8_A494ContagemResultado_Descricao ;
      private bool[] P003F8_n494ContagemResultado_Descricao ;
      private String[] P003F8_A515ContagemResultado_SistemaCoord ;
      private bool[] P003F8_n515ContagemResultado_SistemaCoord ;
      private int[] P003F9_A456ContagemResultado_Codigo ;
      private bool[] P003F9_A517ContagemResultado_Ultima ;
      private DateTime[] P003F9_A473ContagemResultado_DataCnt ;
      private String[] P003F9_A511ContagemResultado_HoraCnt ;
      private String[] P003F9_A463ContagemResultado_ParecerTcn ;
      private bool[] P003F9_n463ContagemResultado_ParecerTcn ;
      private decimal[] P003F9_A460ContagemResultado_PFBFM ;
      private bool[] P003F9_n460ContagemResultado_PFBFM ;
      private decimal[] P003F9_A458ContagemResultado_PFBFS ;
      private bool[] P003F9_n458ContagemResultado_PFBFS ;
      private decimal[] P003F9_A461ContagemResultado_PFLFM ;
      private bool[] P003F9_n461ContagemResultado_PFLFM ;
      private decimal[] P003F9_A459ContagemResultado_PFLFS ;
      private bool[] P003F9_n459ContagemResultado_PFLFS ;
      private int[] P003F9_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P003F9_A462ContagemResultado_Divergencia ;
      private int[] P003F9_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P003F9_n469ContagemResultado_NaoCnfCntCod ;
      private short[] P003F9_A483ContagemResultado_StatusCnt ;
      private short[] P003F9_A482ContagemResultadoContagens_Esforco ;
      private String[] P003F12_A484ContagemResultado_StatusDmn ;
      private bool[] P003F12_n484ContagemResultado_StatusDmn ;
      private int[] P003F12_A456ContagemResultado_Codigo ;
      private int[] P003F13_A39Contratada_Codigo ;
      private String[] P003F13_A516Contratada_TipoFabrica ;
      private int[] P003F13_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003F13_n52Contratada_AreaTrabalhoCod ;
      private int[] P003F14_A39Contratada_Codigo ;
      private String[] P003F14_A452Contrato_CalculoDivergencia ;
      private decimal[] P003F14_A453Contrato_IndiceDivergencia ;
      private int[] P003F14_A74Contrato_Codigo ;
      private int[] P003F15_A456ContagemResultado_Codigo ;
      private String[] P003F15_A484ContagemResultado_StatusDmn ;
      private bool[] P003F15_n484ContagemResultado_StatusDmn ;
      private String[] P003F15_A493ContagemResultado_DemandaFM ;
      private bool[] P003F15_n493ContagemResultado_DemandaFM ;
      private int[] P003F15_A490ContagemResultado_ContratadaCod ;
      private bool[] P003F15_n490ContagemResultado_ContratadaCod ;
      private int[] P003F15_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003F15_n52Contratada_AreaTrabalhoCod ;
      private String[] P003F15_A457ContagemResultado_Demanda ;
      private bool[] P003F15_n457ContagemResultado_Demanda ;
      private int[] P003F16_A456ContagemResultado_Codigo ;
      private bool[] P003F16_A517ContagemResultado_Ultima ;
      private decimal[] P003F16_A459ContagemResultado_PFLFS ;
      private bool[] P003F16_n459ContagemResultado_PFLFS ;
      private decimal[] P003F16_A458ContagemResultado_PFBFS ;
      private bool[] P003F16_n458ContagemResultado_PFBFS ;
      private decimal[] P003F16_A461ContagemResultado_PFLFM ;
      private bool[] P003F16_n461ContagemResultado_PFLFM ;
      private decimal[] P003F16_A460ContagemResultado_PFBFM ;
      private bool[] P003F16_n460ContagemResultado_PFBFM ;
      private DateTime[] P003F16_A473ContagemResultado_DataCnt ;
      private String[] P003F16_A511ContagemResultado_HoraCnt ;
      private int[] P003F17_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003F17_n1553ContagemResultado_CntSrvCod ;
      private int[] P003F17_A1603ContagemResultado_CntCod ;
      private bool[] P003F17_n1603ContagemResultado_CntCod ;
      private int[] P003F17_A456ContagemResultado_Codigo ;
      private String[] P003F17_A1617ContagemResultado_CntClcDvr ;
      private bool[] P003F17_n1617ContagemResultado_CntClcDvr ;
      private String[] P003F17_A484ContagemResultado_StatusDmn ;
      private bool[] P003F17_n484ContagemResultado_StatusDmn ;
      private String[] P003F17_A493ContagemResultado_DemandaFM ;
      private bool[] P003F17_n493ContagemResultado_DemandaFM ;
      private int[] P003F17_A490ContagemResultado_ContratadaCod ;
      private bool[] P003F17_n490ContagemResultado_ContratadaCod ;
      private int[] P003F17_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003F17_n52Contratada_AreaTrabalhoCod ;
      private String[] P003F17_A457ContagemResultado_Demanda ;
      private bool[] P003F17_n457ContagemResultado_Demanda ;
      private int[] P003F18_A456ContagemResultado_Codigo ;
      private bool[] P003F18_A517ContagemResultado_Ultima ;
      private decimal[] P003F18_A460ContagemResultado_PFBFM ;
      private bool[] P003F18_n460ContagemResultado_PFBFM ;
      private decimal[] P003F18_A461ContagemResultado_PFLFM ;
      private bool[] P003F18_n461ContagemResultado_PFLFM ;
      private short[] P003F18_A483ContagemResultado_StatusCnt ;
      private decimal[] P003F18_A458ContagemResultado_PFBFS ;
      private bool[] P003F18_n458ContagemResultado_PFBFS ;
      private decimal[] P003F18_A459ContagemResultado_PFLFS ;
      private bool[] P003F18_n459ContagemResultado_PFLFS ;
      private decimal[] P003F18_A462ContagemResultado_Divergencia ;
      private DateTime[] P003F18_A473ContagemResultado_DataCnt ;
      private String[] P003F18_A511ContagemResultado_HoraCnt ;
      private int[] P003F22_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003F22_n1553ContagemResultado_CntSrvCod ;
      private int[] P003F22_A1603ContagemResultado_CntCod ;
      private bool[] P003F22_n1603ContagemResultado_CntCod ;
      private int[] P003F22_A456ContagemResultado_Codigo ;
      private String[] P003F22_A1617ContagemResultado_CntClcDvr ;
      private bool[] P003F22_n1617ContagemResultado_CntClcDvr ;
      private String[] P003F22_A484ContagemResultado_StatusDmn ;
      private bool[] P003F22_n484ContagemResultado_StatusDmn ;
      private String[] P003F22_A493ContagemResultado_DemandaFM ;
      private bool[] P003F22_n493ContagemResultado_DemandaFM ;
      private int[] P003F22_A490ContagemResultado_ContratadaCod ;
      private bool[] P003F22_n490ContagemResultado_ContratadaCod ;
      private int[] P003F22_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003F22_n52Contratada_AreaTrabalhoCod ;
      private String[] P003F22_A457ContagemResultado_Demanda ;
      private bool[] P003F22_n457ContagemResultado_Demanda ;
      private int[] P003F23_A456ContagemResultado_Codigo ;
      private bool[] P003F23_A517ContagemResultado_Ultima ;
      private decimal[] P003F23_A458ContagemResultado_PFBFS ;
      private bool[] P003F23_n458ContagemResultado_PFBFS ;
      private decimal[] P003F23_A459ContagemResultado_PFLFS ;
      private bool[] P003F23_n459ContagemResultado_PFLFS ;
      private decimal[] P003F23_A460ContagemResultado_PFBFM ;
      private bool[] P003F23_n460ContagemResultado_PFBFM ;
      private short[] P003F23_A483ContagemResultado_StatusCnt ;
      private decimal[] P003F23_A461ContagemResultado_PFLFM ;
      private bool[] P003F23_n461ContagemResultado_PFLFM ;
      private decimal[] P003F23_A462ContagemResultado_Divergencia ;
      private DateTime[] P003F23_A473ContagemResultado_DataCnt ;
      private String[] P003F23_A511ContagemResultado_HoraCnt ;
      private int[] P003F27_A456ContagemResultado_Codigo ;
      private String[] P003F27_A484ContagemResultado_StatusDmn ;
      private bool[] P003F27_n484ContagemResultado_StatusDmn ;
      private String[] P003F27_A493ContagemResultado_DemandaFM ;
      private bool[] P003F27_n493ContagemResultado_DemandaFM ;
      private int[] P003F27_A490ContagemResultado_ContratadaCod ;
      private bool[] P003F27_n490ContagemResultado_ContratadaCod ;
      private int[] P003F27_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003F27_n52Contratada_AreaTrabalhoCod ;
      private String[] P003F27_A457ContagemResultado_Demanda ;
      private bool[] P003F27_n457ContagemResultado_Demanda ;
      private int[] P003F28_A456ContagemResultado_Codigo ;
      private bool[] P003F28_A517ContagemResultado_Ultima ;
      private decimal[] P003F28_A458ContagemResultado_PFBFS ;
      private bool[] P003F28_n458ContagemResultado_PFBFS ;
      private decimal[] P003F28_A459ContagemResultado_PFLFS ;
      private bool[] P003F28_n459ContagemResultado_PFLFS ;
      private DateTime[] P003F28_A473ContagemResultado_DataCnt ;
      private String[] P003F28_A511ContagemResultado_HoraCnt ;
      private String[] P003F32_A579ContagemResultadoErro_Tipo ;
      private int[] P003F32_A456ContagemResultado_Codigo ;
      private DateTime[] P003F32_A580ContagemResultadoErro_Data ;
      private String[] P003F32_A581ContagemResultadoErro_Status ;
      private int[] P003F36_A456ContagemResultado_Codigo ;
      private int[] P003F36_A508ContagemResultado_Owner ;
      private int[] P003F36_A584ContagemResultado_ContadorFM ;
      private bool[] P003F36_n584ContagemResultado_ContadorFM ;
      private String[] P003F38_A579ContagemResultadoErro_Tipo ;
      private int[] P003F38_A456ContagemResultado_Codigo ;
      private String[] P003F38_A581ContagemResultadoErro_Status ;
      private int[] P003F41_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003F41_n1553ContagemResultado_CntSrvCod ;
      private int[] P003F41_A601ContagemResultado_Servico ;
      private bool[] P003F41_n601ContagemResultado_Servico ;
      private String[] P003F41_A484ContagemResultado_StatusDmn ;
      private bool[] P003F41_n484ContagemResultado_StatusDmn ;
      private int[] P003F41_A602ContagemResultado_OSVinculada ;
      private bool[] P003F41_n602ContagemResultado_OSVinculada ;
      private int[] P003F41_A456ContagemResultado_Codigo ;
      private String[] P003F41_A801ContagemResultado_ServicoSigla ;
      private bool[] P003F41_n801ContagemResultado_ServicoSigla ;
      private ExcelDocumentI AV24ExcelDocument ;
      private wwpbaseobjects.SdtWWPContext AV43WWPContext ;
   }

   public class arel_conferenciams__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P003F15( IGxContext context ,
                                              String AV92DemandaFM ,
                                              String A493ContagemResultado_DemandaFM ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV43WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int AV82Contratada ,
                                              String AV93Demanda ,
                                              String A457ContagemResultado_Demanda )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [4] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Demanda] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Demanda] = @AV93Demanda)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (T2.[Contratada_AreaTrabalhoCod] = @AV43WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ContratadaCod] = @AV82Contratada)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92DemandaFM)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV92DemandaFM)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P003F17( IGxContext context ,
                                              String AV92DemandaFM ,
                                              String A493ContagemResultado_DemandaFM ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV43WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int AV82Contratada ,
                                              String AV93Demanda ,
                                              String A457ContagemResultado_Demanda )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T3.[Contrato_CalculoDivergencia] AS ContagemResultado_CntClcDvr, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T4.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Demanda] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Demanda] = @AV93Demanda)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (T4.[Contratada_AreaTrabalhoCod] = @AV43WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ContratadaCod] = @AV82Contratada)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92DemandaFM)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV92DemandaFM)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P003F22( IGxContext context ,
                                              String AV92DemandaFM ,
                                              String A493ContagemResultado_DemandaFM ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV43WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int AV82Contratada ,
                                              String AV93Demanda ,
                                              String A457ContagemResultado_Demanda )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [4] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T3.[Contrato_CalculoDivergencia] AS ContagemResultado_CntClcDvr, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T4.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Demanda] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Demanda] = @AV93Demanda)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (T4.[Contratada_AreaTrabalhoCod] = @AV43WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ContratadaCod] = @AV82Contratada)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92DemandaFM)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV92DemandaFM)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P003F27( IGxContext context ,
                                              String AV92DemandaFM ,
                                              String A493ContagemResultado_DemandaFM ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV43WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int AV82Contratada ,
                                              String AV93Demanda ,
                                              String A457ContagemResultado_Demanda )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [4] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Demanda] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_Demanda] = @AV93Demanda)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] <> 'X')";
         scmdbuf = scmdbuf + " and (T2.[Contratada_AreaTrabalhoCod] = @AV43WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_ContratadaCod] = @AV82Contratada)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92DemandaFM)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV92DemandaFM)";
         }
         else
         {
            GXv_int8[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      protected Object[] conditional_P003F41( IGxContext context ,
                                              int AV86ContagemResultado_OSVinculada ,
                                              int A456ContagemResultado_Codigo ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int AV88ContagemResultado_Codigo ,
                                              String A484ContagemResultado_StatusDmn )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int10 ;
         GXv_int10 = new short [2] ;
         Object[] GXv_Object11 ;
         GXv_Object11 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'A')";
         if ( ! (0==AV86ContagemResultado_OSVinculada) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV86ContagemResultado_OSVinculada)";
         }
         else
         {
            GXv_int10[0] = 1;
         }
         if ( (0==AV86ContagemResultado_OSVinculada) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_OSVinculada] = @AV88ContagemResultado_Codigo)";
         }
         else
         {
            GXv_int10[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn]";
         GXv_Object11[0] = scmdbuf;
         GXv_Object11[1] = GXv_int10;
         return GXv_Object11 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 13 :
                     return conditional_P003F15(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 15 :
                     return conditional_P003F17(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 20 :
                     return conditional_P003F22(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 25 :
                     return conditional_P003F27(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] );
               case 38 :
                     return conditional_P003F41(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (String)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new BatchUpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new UpdateCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new UpdateCursor(def[27])
         ,new UpdateCursor(def[28])
         ,new UpdateCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new UpdateCursor(def[31])
         ,new UpdateCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new UpdateCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new UpdateCursor(def[36])
         ,new UpdateCursor(def[37])
         ,new ForEachCursor(def[38])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003F2 ;
          prmP003F2 = new Object[] {
          new Object[] {"@AV82Contratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F3 ;
          prmP003F3 = new Object[] {
          new Object[] {"@AV43WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F4 ;
          prmP003F4 = new Object[] {
          new Object[] {"@AV70Sistema_Sigla",SqlDbType.Char,25,0}
          } ;
          Object[] prmP003F5 ;
          prmP003F5 = new Object[] {
          new Object[] {"@AV43WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV59ContratadaUsuario_UsuarioPessoaNom",SqlDbType.Char,100,0}
          } ;
          Object[] prmP003F6 ;
          prmP003F6 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0}
          } ;
          String cmdBufferP003F6 ;
          cmdBufferP003F6=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_SistemaCod], [ContagemResultado_Owner], [ContagemResultado_Baseline], [ContagemResultado_DataCadastro], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_TipoRegistro], [ContagemResultado_DataEntrega], [ContagemResultado_Link], [ContagemResultado_Descricao], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_ValorPF], [ContagemResultado_Evidencia], [ContagemResultado_LoteAceiteCod], [ContagemResultado_OSVinculada], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_LiqLogCod], [ContagemResultado_FncUsrCod], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoInicialDias], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], "
          + " [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_CntSrvCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_ServicoSS], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_SistemaCod, @ContagemResultado_Owner, @ContagemResultado_Baseline, @ContagemResultado_DataCadastro, @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_TipoRegistro, convert( DATETIME, '17530101', 112 ), '', '', convert(int, 0), '', convert(int, 0), '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), '', convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0),"
          + " convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(int, 0), convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP003F7 ;
          prmP003F7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP003F8 ;
          prmP003F8 = new Object[] {
          new Object[] {"@AV82Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV93Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP003F9 ;
          prmP003F9 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F10 ;
          prmP003F10 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F11 ;
          prmP003F11 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F12 ;
          prmP003F12 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F13 ;
          prmP003F13 = new Object[] {
          new Object[] {"@AV43WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F14 ;
          prmP003F14 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F16 ;
          prmP003F16 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F18 ;
          prmP003F18 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F19 ;
          prmP003F19 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F20 ;
          prmP003F20 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F21 ;
          prmP003F21 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F23 ;
          prmP003F23 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F24 ;
          prmP003F24 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F25 ;
          prmP003F25 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F26 ;
          prmP003F26 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F28 ;
          prmP003F28 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F29 ;
          prmP003F29 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F30 ;
          prmP003F30 = new Object[] {
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003F31 ;
          prmP003F31 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F32 ;
          prmP003F32 = new Object[] {
          new Object[] {"@AV88ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP003F33 ;
          prmP003F33 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP003F34 ;
          prmP003F34 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP003F36 ;
          prmP003F36 = new Object[] {
          new Object[] {"@AV88ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F37 ;
          prmP003F37 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoErro_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003F38 ;
          prmP003F38 = new Object[] {
          new Object[] {"@AV88ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP003F39 ;
          prmP003F39 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP003F40 ;
          prmP003F40 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP003F15 ;
          prmP003F15 = new Object[] {
          new Object[] {"@AV93Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV43WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP003F17 ;
          prmP003F17 = new Object[] {
          new Object[] {"@AV93Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV43WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP003F22 ;
          prmP003F22 = new Object[] {
          new Object[] {"@AV93Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV43WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP003F27 ;
          prmP003F27 = new Object[] {
          new Object[] {"@AV93Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV43WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82Contratada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV92DemandaFM",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP003F41 ;
          prmP003F41 = new Object[] {
          new Object[] {"@AV86ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV88ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003F2", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV82Contratada ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F2,1,0,false,true )
             ,new CursorDef("P003F3", "SELECT [Contratada_TipoFabrica], [Contratada_AreaTrabalhoCod], [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE ([Contratada_AreaTrabalhoCod] = @AV43WWPC_1Areatrabalho_codigo) AND ([Contratada_TipoFabrica] = 'M') ORDER BY [Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F3,100,0,false,false )
             ,new CursorDef("P003F4", "SELECT [Sistema_Sigla], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Sigla] = UPPER(RTRIM(LTRIM(@AV70Sistema_Sigla))) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F4,100,0,false,false )
             ,new CursorDef("P003F5", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE T2.[Contratada_AreaTrabalhoCod] = @AV43WWPC_1Areatrabalho_codigo and T4.[Pessoa_Nome] = @AV59ContratadaUsuario_UsuarioPessoaNom ORDER BY T2.[Contratada_AreaTrabalhoCod], T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F5,100,0,false,false )
             ,new CursorDef("P003F6", cmdBufferP003F6, GxErrorMask.GX_NOMASK,prmP003F6)
             ,new CursorDef("P003F7", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_Divergencia], [ContagemResultado_ParecerTcn], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', convert(int, 0), convert(int, 0), CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP003F7)
             ,new CursorDef("P003F8", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Descricao], T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE (T1.[ContagemResultado_ContratadaOrigemCod] = @AV82Contratada and T1.[ContagemResultado_Demanda] = @AV93Demanda) AND (T1.[ContagemResultado_StatusDmn] <> 'X') ORDER BY T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F8,1,0,true,false )
             ,new CursorDef("P003F9", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_PFBFM], [ContagemResultado_PFBFS], [ContagemResultado_PFLFM], [ContagemResultado_PFLFS], [ContagemResultado_ContadorFMCod], [ContagemResultado_Divergencia], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F9,1,0,true,true )
             ,new CursorDef("P003F10", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F10)
             ,new CursorDef("P003F11", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F11)
             ,new CursorDef("P003F12", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F12)
             ,new CursorDef("P003F13", "SELECT TOP 1 [Contratada_Codigo], [Contratada_TipoFabrica], [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE ([Contratada_AreaTrabalhoCod] = @AV43WWPC_1Areatrabalho_codigo) AND ([Contratada_TipoFabrica] = 'M') ORDER BY [Contratada_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F13,1,0,true,true )
             ,new CursorDef("P003F14", "SELECT TOP 1 [Contratada_Codigo], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F14,1,0,false,true )
             ,new CursorDef("P003F15", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F15,100,0,true,false )
             ,new CursorDef("P003F16", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_PFLFS], [ContagemResultado_PFBFS], [ContagemResultado_PFLFM], [ContagemResultado_PFBFM], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F16,1,0,false,true )
             ,new CursorDef("P003F17", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F17,1,0,true,false )
             ,new CursorDef("P003F18", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_StatusCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_Divergencia], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F18,1,0,true,true )
             ,new CursorDef("P003F19", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F19)
             ,new CursorDef("P003F20", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F20)
             ,new CursorDef("P003F21", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F21)
             ,new CursorDef("P003F22", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F22,1,0,true,false )
             ,new CursorDef("P003F23", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_StatusCnt], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F23,1,0,true,true )
             ,new CursorDef("P003F24", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F24)
             ,new CursorDef("P003F25", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS, [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt, [ContagemResultado_Divergencia]=@ContagemResultado_Divergencia  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F25)
             ,new CursorDef("P003F26", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F26)
             ,new CursorDef("P003F27", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F27,1,0,true,false )
             ,new CursorDef("P003F28", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F28,1,0,true,true )
             ,new CursorDef("P003F29", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F29)
             ,new CursorDef("P003F30", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_PFBFS]=@ContagemResultado_PFBFS, [ContagemResultado_PFLFS]=@ContagemResultado_PFLFS  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F30)
             ,new CursorDef("P003F31", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F31)
             ,new CursorDef("P003F32", "SELECT TOP 1 [ContagemResultadoErro_Tipo], [ContagemResultado_Codigo], [ContagemResultadoErro_Data], [ContagemResultadoErro_Status] FROM [ContagemResultadoErro] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV88ContagemResultado_Codigo and [ContagemResultadoErro_Tipo] = @AV72ContagemResultadoErro_Tipo ORDER BY [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F32,1,0,true,true )
             ,new CursorDef("P003F33", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Data]=@ContagemResultadoErro_Data, [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F33)
             ,new CursorDef("P003F34", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Data]=@ContagemResultadoErro_Data, [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F34)
             ,new CursorDef("P003F36", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Owner], COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV88ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F36,1,0,false,true )
             ,new CursorDef("P003F37", "INSERT INTO [ContagemResultadoErro]([ContagemResultado_Codigo], [ContagemResultadoErro_Tipo], [ContagemResultadoErro_Data], [ContagemResultadoErro_Status], [ContagemResultadoErro_ContadorFMCod]) VALUES(@ContagemResultado_Codigo, @ContagemResultadoErro_Tipo, @ContagemResultadoErro_Data, @ContagemResultadoErro_Status, @ContagemResultadoErro_ContadorFMCod)", GxErrorMask.GX_NOMASK,prmP003F37)
             ,new CursorDef("P003F38", "SELECT TOP 1 [ContagemResultadoErro_Tipo], [ContagemResultado_Codigo], [ContagemResultadoErro_Status] FROM [ContagemResultadoErro] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV88ContagemResultado_Codigo and [ContagemResultadoErro_Tipo] = @AV72ContagemResultadoErro_Tipo ORDER BY [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F38,1,0,true,true )
             ,new CursorDef("P003F39", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F39)
             ,new CursorDef("P003F40", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003F40)
             ,new CursorDef("P003F41", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003F41,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 5) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((short[]) buf[18])[0] = rslt.getShort(13) ;
                ((short[]) buf[19])[0] = rslt.getShort(14) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[11])[0] = rslt.getString(8, 5) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 5) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 5) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 5) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 35 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                stmt.SetParameter(10, (int)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(11, (bool)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(12, (DateTime)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[25]);
                }
                stmt.SetParameter(15, (short)parms[26]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (int)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                stmt.SetParameter(8, (short)parms[10]);
                stmt.SetParameter(9, (short)parms[11]);
                stmt.SetParameter(10, (bool)parms[12]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (short)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[4]);
                }
                stmt.SetParameter(4, (decimal)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 18 :
                stmt.SetParameter(1, (short)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[4]);
                }
                stmt.SetParameter(4, (decimal)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 20 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (short)parms[4]);
                stmt.SetParameter(4, (decimal)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (short)parms[4]);
                stmt.SetParameter(4, (decimal)parms[5]);
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 25 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (DateTime)parms[5]);
                stmt.SetParameter(5, (String)parms[6]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (DateTime)parms[5]);
                stmt.SetParameter(5, (String)parms[6]);
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 31 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 32 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 36 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 37 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 38 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
       }
    }

 }

}
