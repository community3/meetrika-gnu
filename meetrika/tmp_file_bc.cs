/*
               File: Tmp_File_BC
        Description: Arquivo a processar:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:37.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tmp_file_bc : GXHttpHandler, IGxSilentTrn
   {
      public tmp_file_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tmp_file_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1R73( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1R73( ) ;
         standaloneModal( ) ;
         AddRow1R73( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z435File_UsuarioCod = A435File_UsuarioCod;
               Z504File_Row = A504File_Row;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1R0( )
      {
         BeforeValidate1R73( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1R73( ) ;
            }
            else
            {
               CheckExtendedTable1R73( ) ;
               if ( AnyError == 0 )
               {
                  ZM1R73( 2) ;
               }
               CloseExtendedTableCursors1R73( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1R73( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z505File_Col1 = A505File_Col1;
            Z506File_Col2 = A506File_Col2;
            Z507File_Col3 = A507File_Col3;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -1 )
         {
            Z504File_Row = A504File_Row;
            Z505File_Col1 = A505File_Col1;
            Z506File_Col2 = A506File_Col2;
            Z507File_Col3 = A507File_Col3;
            Z435File_UsuarioCod = A435File_UsuarioCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1R73( )
      {
         /* Using cursor BC001R5 */
         pr_default.execute(3, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound73 = 1;
            A505File_Col1 = BC001R5_A505File_Col1[0];
            n505File_Col1 = BC001R5_n505File_Col1[0];
            A506File_Col2 = BC001R5_A506File_Col2[0];
            n506File_Col2 = BC001R5_n506File_Col2[0];
            A507File_Col3 = BC001R5_A507File_Col3[0];
            n507File_Col3 = BC001R5_n507File_Col3[0];
            ZM1R73( -1) ;
         }
         pr_default.close(3);
         OnLoadActions1R73( ) ;
      }

      protected void OnLoadActions1R73( )
      {
      }

      protected void CheckExtendedTable1R73( )
      {
         standaloneModal( ) ;
         /* Using cursor BC001R4 */
         pr_default.execute(2, new Object[] {A435File_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'File_Usuario'.", "ForeignKeyNotFound", 1, "FILE_USUARIOCOD");
            AnyError = 1;
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors1R73( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1R73( )
      {
         /* Using cursor BC001R6 */
         pr_default.execute(4, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound73 = 1;
         }
         else
         {
            RcdFound73 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001R3 */
         pr_default.execute(1, new Object[] {A435File_UsuarioCod, A504File_Row});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1R73( 1) ;
            RcdFound73 = 1;
            A504File_Row = BC001R3_A504File_Row[0];
            A505File_Col1 = BC001R3_A505File_Col1[0];
            n505File_Col1 = BC001R3_n505File_Col1[0];
            A506File_Col2 = BC001R3_A506File_Col2[0];
            n506File_Col2 = BC001R3_n506File_Col2[0];
            A507File_Col3 = BC001R3_A507File_Col3[0];
            n507File_Col3 = BC001R3_n507File_Col3[0];
            A435File_UsuarioCod = BC001R3_A435File_UsuarioCod[0];
            Z435File_UsuarioCod = A435File_UsuarioCod;
            Z504File_Row = A504File_Row;
            sMode73 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1R73( ) ;
            if ( AnyError == 1 )
            {
               RcdFound73 = 0;
               InitializeNonKey1R73( ) ;
            }
            Gx_mode = sMode73;
         }
         else
         {
            RcdFound73 = 0;
            InitializeNonKey1R73( ) ;
            sMode73 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode73;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1R0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1R73( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001R2 */
            pr_default.execute(0, new Object[] {A435File_UsuarioCod, A504File_Row});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tmp_File"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z505File_Col1, BC001R2_A505File_Col1[0]) != 0 ) || ( StringUtil.StrCmp(Z506File_Col2, BC001R2_A506File_Col2[0]) != 0 ) || ( StringUtil.StrCmp(Z507File_Col3, BC001R2_A507File_Col3[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Tmp_File"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1R73( )
      {
         BeforeValidate1R73( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1R73( 0) ;
            CheckOptimisticConcurrency1R73( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1R73( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1R73( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001R7 */
                     pr_default.execute(5, new Object[] {A504File_Row, n505File_Col1, A505File_Col1, n506File_Col2, A506File_Col2, n507File_Col3, A507File_Col3, A435File_UsuarioCod});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Tmp_File") ;
                     if ( (pr_default.getStatus(5) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1R73( ) ;
            }
            EndLevel1R73( ) ;
         }
         CloseExtendedTableCursors1R73( ) ;
      }

      protected void Update1R73( )
      {
         BeforeValidate1R73( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1R73( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1R73( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1R73( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001R8 */
                     pr_default.execute(6, new Object[] {n505File_Col1, A505File_Col1, n506File_Col2, A506File_Col2, n507File_Col3, A507File_Col3, A435File_UsuarioCod, A504File_Row});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Tmp_File") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tmp_File"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1R73( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1R73( ) ;
         }
         CloseExtendedTableCursors1R73( ) ;
      }

      protected void DeferredUpdate1R73( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1R73( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1R73( ) ;
            AfterConfirm1R73( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1R73( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001R9 */
                  pr_default.execute(7, new Object[] {A435File_UsuarioCod, A504File_Row});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("Tmp_File") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode73 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1R73( ) ;
         Gx_mode = sMode73;
      }

      protected void OnDeleteControls1R73( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1R73( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1R73( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1R73( )
      {
         /* Using cursor BC001R10 */
         pr_default.execute(8, new Object[] {A435File_UsuarioCod, A504File_Row});
         RcdFound73 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound73 = 1;
            A504File_Row = BC001R10_A504File_Row[0];
            A505File_Col1 = BC001R10_A505File_Col1[0];
            n505File_Col1 = BC001R10_n505File_Col1[0];
            A506File_Col2 = BC001R10_A506File_Col2[0];
            n506File_Col2 = BC001R10_n506File_Col2[0];
            A507File_Col3 = BC001R10_A507File_Col3[0];
            n507File_Col3 = BC001R10_n507File_Col3[0];
            A435File_UsuarioCod = BC001R10_A435File_UsuarioCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1R73( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound73 = 0;
         ScanKeyLoad1R73( ) ;
      }

      protected void ScanKeyLoad1R73( )
      {
         sMode73 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound73 = 1;
            A504File_Row = BC001R10_A504File_Row[0];
            A505File_Col1 = BC001R10_A505File_Col1[0];
            n505File_Col1 = BC001R10_n505File_Col1[0];
            A506File_Col2 = BC001R10_A506File_Col2[0];
            n506File_Col2 = BC001R10_n506File_Col2[0];
            A507File_Col3 = BC001R10_A507File_Col3[0];
            n507File_Col3 = BC001R10_n507File_Col3[0];
            A435File_UsuarioCod = BC001R10_A435File_UsuarioCod[0];
         }
         Gx_mode = sMode73;
      }

      protected void ScanKeyEnd1R73( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm1R73( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1R73( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1R73( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1R73( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1R73( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1R73( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1R73( )
      {
      }

      protected void AddRow1R73( )
      {
         VarsToRow73( bcTmp_File) ;
      }

      protected void ReadRow1R73( )
      {
         RowToVars73( bcTmp_File, 1) ;
      }

      protected void InitializeNonKey1R73( )
      {
         A505File_Col1 = "";
         n505File_Col1 = false;
         A506File_Col2 = "";
         n506File_Col2 = false;
         A507File_Col3 = "";
         n507File_Col3 = false;
         Z505File_Col1 = "";
         Z506File_Col2 = "";
         Z507File_Col3 = "";
      }

      protected void InitAll1R73( )
      {
         A435File_UsuarioCod = 0;
         A504File_Row = 0;
         InitializeNonKey1R73( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow73( SdtTmp_File obj73 )
      {
         obj73.gxTpr_Mode = Gx_mode;
         obj73.gxTpr_File_col1 = A505File_Col1;
         obj73.gxTpr_File_col2 = A506File_Col2;
         obj73.gxTpr_File_col3 = A507File_Col3;
         obj73.gxTpr_File_usuariocod = A435File_UsuarioCod;
         obj73.gxTpr_File_row = A504File_Row;
         obj73.gxTpr_File_usuariocod_Z = Z435File_UsuarioCod;
         obj73.gxTpr_File_row_Z = Z504File_Row;
         obj73.gxTpr_File_col1_Z = Z505File_Col1;
         obj73.gxTpr_File_col2_Z = Z506File_Col2;
         obj73.gxTpr_File_col3_Z = Z507File_Col3;
         obj73.gxTpr_File_col1_N = (short)(Convert.ToInt16(n505File_Col1));
         obj73.gxTpr_File_col2_N = (short)(Convert.ToInt16(n506File_Col2));
         obj73.gxTpr_File_col3_N = (short)(Convert.ToInt16(n507File_Col3));
         obj73.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow73( SdtTmp_File obj73 )
      {
         obj73.gxTpr_File_usuariocod = A435File_UsuarioCod;
         obj73.gxTpr_File_row = A504File_Row;
         return  ;
      }

      public void RowToVars73( SdtTmp_File obj73 ,
                               int forceLoad )
      {
         Gx_mode = obj73.gxTpr_Mode;
         A505File_Col1 = obj73.gxTpr_File_col1;
         n505File_Col1 = false;
         A506File_Col2 = obj73.gxTpr_File_col2;
         n506File_Col2 = false;
         A507File_Col3 = obj73.gxTpr_File_col3;
         n507File_Col3 = false;
         A435File_UsuarioCod = obj73.gxTpr_File_usuariocod;
         A504File_Row = obj73.gxTpr_File_row;
         Z435File_UsuarioCod = obj73.gxTpr_File_usuariocod_Z;
         Z504File_Row = obj73.gxTpr_File_row_Z;
         Z505File_Col1 = obj73.gxTpr_File_col1_Z;
         Z506File_Col2 = obj73.gxTpr_File_col2_Z;
         Z507File_Col3 = obj73.gxTpr_File_col3_Z;
         n505File_Col1 = (bool)(Convert.ToBoolean(obj73.gxTpr_File_col1_N));
         n506File_Col2 = (bool)(Convert.ToBoolean(obj73.gxTpr_File_col2_N));
         n507File_Col3 = (bool)(Convert.ToBoolean(obj73.gxTpr_File_col3_N));
         Gx_mode = obj73.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A435File_UsuarioCod = (int)getParm(obj,0);
         A504File_Row = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1R73( ) ;
         ScanKeyStart1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001R11 */
            pr_default.execute(9, new Object[] {A435File_UsuarioCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'File_Usuario'.", "ForeignKeyNotFound", 1, "FILE_USUARIOCOD");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z435File_UsuarioCod = A435File_UsuarioCod;
            Z504File_Row = A504File_Row;
         }
         ZM1R73( -1) ;
         OnLoadActions1R73( ) ;
         AddRow1R73( ) ;
         ScanKeyEnd1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars73( bcTmp_File, 0) ;
         ScanKeyStart1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001R11 */
            pr_default.execute(9, new Object[] {A435File_UsuarioCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'File_Usuario'.", "ForeignKeyNotFound", 1, "FILE_USUARIOCOD");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z435File_UsuarioCod = A435File_UsuarioCod;
            Z504File_Row = A504File_Row;
         }
         ZM1R73( -1) ;
         OnLoadActions1R73( ) ;
         AddRow1R73( ) ;
         ScanKeyEnd1R73( ) ;
         if ( RcdFound73 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars73( bcTmp_File, 0) ;
         nKeyPressed = 1;
         GetKey1R73( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1R73( ) ;
         }
         else
         {
            if ( RcdFound73 == 1 )
            {
               if ( ( A435File_UsuarioCod != Z435File_UsuarioCod ) || ( A504File_Row != Z504File_Row ) )
               {
                  A435File_UsuarioCod = Z435File_UsuarioCod;
                  A504File_Row = Z504File_Row;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1R73( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A435File_UsuarioCod != Z435File_UsuarioCod ) || ( A504File_Row != Z504File_Row ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1R73( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1R73( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow73( bcTmp_File) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars73( bcTmp_File, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1R73( ) ;
         if ( RcdFound73 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A435File_UsuarioCod != Z435File_UsuarioCod ) || ( A504File_Row != Z504File_Row ) )
            {
               A435File_UsuarioCod = Z435File_UsuarioCod;
               A504File_Row = Z504File_Row;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A435File_UsuarioCod != Z435File_UsuarioCod ) || ( A504File_Row != Z504File_Row ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "Tmp_File_BC");
         VarsToRow73( bcTmp_File) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcTmp_File.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcTmp_File.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcTmp_File )
         {
            bcTmp_File = (SdtTmp_File)(sdt);
            if ( StringUtil.StrCmp(bcTmp_File.gxTpr_Mode, "") == 0 )
            {
               bcTmp_File.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow73( bcTmp_File) ;
            }
            else
            {
               RowToVars73( bcTmp_File, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcTmp_File.gxTpr_Mode, "") == 0 )
            {
               bcTmp_File.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars73( bcTmp_File, 1) ;
         return  ;
      }

      public SdtTmp_File Tmp_File_BC
      {
         get {
            return bcTmp_File ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z505File_Col1 = "";
         A505File_Col1 = "";
         Z506File_Col2 = "";
         A506File_Col2 = "";
         Z507File_Col3 = "";
         A507File_Col3 = "";
         BC001R5_A504File_Row = new int[1] ;
         BC001R5_A505File_Col1 = new String[] {""} ;
         BC001R5_n505File_Col1 = new bool[] {false} ;
         BC001R5_A506File_Col2 = new String[] {""} ;
         BC001R5_n506File_Col2 = new bool[] {false} ;
         BC001R5_A507File_Col3 = new String[] {""} ;
         BC001R5_n507File_Col3 = new bool[] {false} ;
         BC001R5_A435File_UsuarioCod = new int[1] ;
         BC001R4_A435File_UsuarioCod = new int[1] ;
         BC001R6_A435File_UsuarioCod = new int[1] ;
         BC001R6_A504File_Row = new int[1] ;
         BC001R3_A504File_Row = new int[1] ;
         BC001R3_A505File_Col1 = new String[] {""} ;
         BC001R3_n505File_Col1 = new bool[] {false} ;
         BC001R3_A506File_Col2 = new String[] {""} ;
         BC001R3_n506File_Col2 = new bool[] {false} ;
         BC001R3_A507File_Col3 = new String[] {""} ;
         BC001R3_n507File_Col3 = new bool[] {false} ;
         BC001R3_A435File_UsuarioCod = new int[1] ;
         sMode73 = "";
         BC001R2_A504File_Row = new int[1] ;
         BC001R2_A505File_Col1 = new String[] {""} ;
         BC001R2_n505File_Col1 = new bool[] {false} ;
         BC001R2_A506File_Col2 = new String[] {""} ;
         BC001R2_n506File_Col2 = new bool[] {false} ;
         BC001R2_A507File_Col3 = new String[] {""} ;
         BC001R2_n507File_Col3 = new bool[] {false} ;
         BC001R2_A435File_UsuarioCod = new int[1] ;
         BC001R10_A504File_Row = new int[1] ;
         BC001R10_A505File_Col1 = new String[] {""} ;
         BC001R10_n505File_Col1 = new bool[] {false} ;
         BC001R10_A506File_Col2 = new String[] {""} ;
         BC001R10_n506File_Col2 = new bool[] {false} ;
         BC001R10_A507File_Col3 = new String[] {""} ;
         BC001R10_n507File_Col3 = new bool[] {false} ;
         BC001R10_A435File_UsuarioCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC001R11_A435File_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tmp_file_bc__default(),
            new Object[][] {
                new Object[] {
               BC001R2_A504File_Row, BC001R2_A505File_Col1, BC001R2_n505File_Col1, BC001R2_A506File_Col2, BC001R2_n506File_Col2, BC001R2_A507File_Col3, BC001R2_n507File_Col3, BC001R2_A435File_UsuarioCod
               }
               , new Object[] {
               BC001R3_A504File_Row, BC001R3_A505File_Col1, BC001R3_n505File_Col1, BC001R3_A506File_Col2, BC001R3_n506File_Col2, BC001R3_A507File_Col3, BC001R3_n507File_Col3, BC001R3_A435File_UsuarioCod
               }
               , new Object[] {
               BC001R4_A435File_UsuarioCod
               }
               , new Object[] {
               BC001R5_A504File_Row, BC001R5_A505File_Col1, BC001R5_n505File_Col1, BC001R5_A506File_Col2, BC001R5_n506File_Col2, BC001R5_A507File_Col3, BC001R5_n507File_Col3, BC001R5_A435File_UsuarioCod
               }
               , new Object[] {
               BC001R6_A435File_UsuarioCod, BC001R6_A504File_Row
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001R10_A504File_Row, BC001R10_A505File_Col1, BC001R10_n505File_Col1, BC001R10_A506File_Col2, BC001R10_n506File_Col2, BC001R10_A507File_Col3, BC001R10_n507File_Col3, BC001R10_A435File_UsuarioCod
               }
               , new Object[] {
               BC001R11_A435File_UsuarioCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound73 ;
      private int trnEnded ;
      private int Z435File_UsuarioCod ;
      private int A435File_UsuarioCod ;
      private int Z504File_Row ;
      private int A504File_Row ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode73 ;
      private bool n505File_Col1 ;
      private bool n506File_Col2 ;
      private bool n507File_Col3 ;
      private String Z505File_Col1 ;
      private String A505File_Col1 ;
      private String Z506File_Col2 ;
      private String A506File_Col2 ;
      private String Z507File_Col3 ;
      private String A507File_Col3 ;
      private SdtTmp_File bcTmp_File ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC001R5_A504File_Row ;
      private String[] BC001R5_A505File_Col1 ;
      private bool[] BC001R5_n505File_Col1 ;
      private String[] BC001R5_A506File_Col2 ;
      private bool[] BC001R5_n506File_Col2 ;
      private String[] BC001R5_A507File_Col3 ;
      private bool[] BC001R5_n507File_Col3 ;
      private int[] BC001R5_A435File_UsuarioCod ;
      private int[] BC001R4_A435File_UsuarioCod ;
      private int[] BC001R6_A435File_UsuarioCod ;
      private int[] BC001R6_A504File_Row ;
      private int[] BC001R3_A504File_Row ;
      private String[] BC001R3_A505File_Col1 ;
      private bool[] BC001R3_n505File_Col1 ;
      private String[] BC001R3_A506File_Col2 ;
      private bool[] BC001R3_n506File_Col2 ;
      private String[] BC001R3_A507File_Col3 ;
      private bool[] BC001R3_n507File_Col3 ;
      private int[] BC001R3_A435File_UsuarioCod ;
      private int[] BC001R2_A504File_Row ;
      private String[] BC001R2_A505File_Col1 ;
      private bool[] BC001R2_n505File_Col1 ;
      private String[] BC001R2_A506File_Col2 ;
      private bool[] BC001R2_n506File_Col2 ;
      private String[] BC001R2_A507File_Col3 ;
      private bool[] BC001R2_n507File_Col3 ;
      private int[] BC001R2_A435File_UsuarioCod ;
      private int[] BC001R10_A504File_Row ;
      private String[] BC001R10_A505File_Col1 ;
      private bool[] BC001R10_n505File_Col1 ;
      private String[] BC001R10_A506File_Col2 ;
      private bool[] BC001R10_n506File_Col2 ;
      private String[] BC001R10_A507File_Col3 ;
      private bool[] BC001R10_n507File_Col3 ;
      private int[] BC001R10_A435File_UsuarioCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC001R11_A435File_UsuarioCod ;
   }

   public class tmp_file_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001R5 ;
          prmBC001R5 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmBC001R4 ;
          prmBC001R4 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001R6 ;
          prmBC001R6 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmBC001R3 ;
          prmBC001R3 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmBC001R2 ;
          prmBC001R2 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmBC001R7 ;
          prmBC001R7 = new Object[] {
          new Object[] {"@File_Row",SqlDbType.Int,8,0} ,
          new Object[] {"@File_Col1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001R8 ;
          prmBC001R8 = new Object[] {
          new Object[] {"@File_Col1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_Col3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmBC001R9 ;
          prmBC001R9 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmBC001R10 ;
          prmBC001R10 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@File_Row",SqlDbType.Int,8,0}
          } ;
          Object[] prmBC001R11 ;
          prmBC001R11 = new Object[] {
          new Object[] {"@File_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001R2", "SELECT [File_Row], [File_Col1], [File_Col2], [File_Col3], [File_UsuarioCod] AS File_UsuarioCod FROM [Tmp_File] WITH (UPDLOCK) WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001R2,1,0,true,false )
             ,new CursorDef("BC001R3", "SELECT [File_Row], [File_Col1], [File_Col2], [File_Col3], [File_UsuarioCod] AS File_UsuarioCod FROM [Tmp_File] WITH (NOLOCK) WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001R3,1,0,true,false )
             ,new CursorDef("BC001R4", "SELECT [Usuario_Codigo] AS File_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @File_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001R4,1,0,true,false )
             ,new CursorDef("BC001R5", "SELECT TM1.[File_Row], TM1.[File_Col1], TM1.[File_Col2], TM1.[File_Col3], TM1.[File_UsuarioCod] AS File_UsuarioCod FROM [Tmp_File] TM1 WITH (NOLOCK) WHERE TM1.[File_UsuarioCod] = @File_UsuarioCod and TM1.[File_Row] = @File_Row ORDER BY TM1.[File_UsuarioCod], TM1.[File_Row]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001R5,100,0,true,false )
             ,new CursorDef("BC001R6", "SELECT [File_UsuarioCod] AS File_UsuarioCod, [File_Row] FROM [Tmp_File] WITH (NOLOCK) WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001R6,1,0,true,false )
             ,new CursorDef("BC001R7", "INSERT INTO [Tmp_File]([File_Row], [File_Col1], [File_Col2], [File_Col3], [File_UsuarioCod]) VALUES(@File_Row, @File_Col1, @File_Col2, @File_Col3, @File_UsuarioCod)", GxErrorMask.GX_NOMASK,prmBC001R7)
             ,new CursorDef("BC001R8", "UPDATE [Tmp_File] SET [File_Col1]=@File_Col1, [File_Col2]=@File_Col2, [File_Col3]=@File_Col3  WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row", GxErrorMask.GX_NOMASK,prmBC001R8)
             ,new CursorDef("BC001R9", "DELETE FROM [Tmp_File]  WHERE [File_UsuarioCod] = @File_UsuarioCod AND [File_Row] = @File_Row", GxErrorMask.GX_NOMASK,prmBC001R9)
             ,new CursorDef("BC001R10", "SELECT TM1.[File_Row], TM1.[File_Col1], TM1.[File_Col2], TM1.[File_Col3], TM1.[File_UsuarioCod] AS File_UsuarioCod FROM [Tmp_File] TM1 WITH (NOLOCK) WHERE TM1.[File_UsuarioCod] = @File_UsuarioCod and TM1.[File_Row] = @File_Row ORDER BY TM1.[File_UsuarioCod], TM1.[File_Row]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001R10,100,0,true,false )
             ,new CursorDef("BC001R11", "SELECT [Usuario_Codigo] AS File_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @File_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001R11,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
