/*
               File: ServicoPrioridade
        Description: Prioridade padr�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:20.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoprioridade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ServicoPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoPrioridade_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICOPRIORIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ServicoPrioridade_Codigo), "ZZZZZ9")));
               A1439ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynServicoPrioridade_SrvCod.Name = "SERVICOPRIORIDADE_SRVCOD";
         dynServicoPrioridade_SrvCod.WebTags = "";
         dynServicoPrioridade_SrvCod.removeAllItems();
         /* Using cursor T003Q5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            dynServicoPrioridade_SrvCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T003Q5_A155Servico_Codigo[0]), 6, 0)), T003Q5_A608Servico_Nome[0], 0);
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( dynServicoPrioridade_SrvCod.ItemCount > 0 )
         {
            if ( (0==A1439ServicoPrioridade_SrvCod) )
            {
               A1439ServicoPrioridade_SrvCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
            }
            A1439ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( dynServicoPrioridade_SrvCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Prioridade padr�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtServicoPrioridade_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servicoprioridade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicoprioridade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ServicoPrioridade_Codigo ,
                           ref int aP2_ServicoPrioridade_SrvCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ServicoPrioridade_Codigo = aP1_ServicoPrioridade_Codigo;
         this.A1439ServicoPrioridade_SrvCod = aP2_ServicoPrioridade_SrvCod;
         executePrivate();
         aP2_ServicoPrioridade_SrvCod=this.A1439ServicoPrioridade_SrvCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynServicoPrioridade_SrvCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynServicoPrioridade_SrvCod.ItemCount > 0 )
         {
            A1439ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( dynServicoPrioridade_SrvCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3Q171( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3Q171e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoPrioridade_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0, ",", "")), ((edtServicoPrioridade_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoPrioridade_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServicoPrioridade_Codigo_Visible, edtServicoPrioridade_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoPrioridade.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3Q171( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3Q171( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3Q171e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_3Q171( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_3Q171e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3Q171e( true) ;
         }
         else
         {
            wb_table1_2_3Q171e( false) ;
         }
      }

      protected void wb_table3_31_3Q171( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_3Q171e( true) ;
         }
         else
         {
            wb_table3_31_3Q171e( false) ;
         }
      }

      protected void wb_table2_5_3Q171( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3Q171( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3Q171e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3Q171e( true) ;
         }
         else
         {
            wb_table2_5_3Q171e( false) ;
         }
      }

      protected void wb_table4_13_3Q171( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoprioridade_srvcod_Internalname, "Servi�o", "", "", lblTextblockservicoprioridade_srvcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServicoPrioridade_SrvCod, dynServicoPrioridade_SrvCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)), 1, dynServicoPrioridade_SrvCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServicoPrioridade_SrvCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ServicoPrioridade.htm");
            dynServicoPrioridade_SrvCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoPrioridade_SrvCod_Internalname, "Values", (String)(dynServicoPrioridade_SrvCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoprioridade_nome_Internalname, "Nome", "", "", lblTextblockservicoprioridade_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServicoPrioridade_Nome_Internalname, StringUtil.RTrim( A1441ServicoPrioridade_Nome), StringUtil.RTrim( context.localUtil.Format( A1441ServicoPrioridade_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoPrioridade_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoPrioridade_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicoprioridade_finalidade_Internalname, "Finalidade", "", "", lblTextblockservicoprioridade_finalidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtServicoPrioridade_Finalidade_Internalname, A1442ServicoPrioridade_Finalidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, 1, edtServicoPrioridade_Finalidade_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ServicoPrioridade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3Q171e( true) ;
         }
         else
         {
            wb_table4_13_3Q171e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113Q2 */
         E113Q2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynServicoPrioridade_SrvCod.CurrentValue = cgiGet( dynServicoPrioridade_SrvCod_Internalname);
               A1439ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( cgiGet( dynServicoPrioridade_SrvCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
               A1441ServicoPrioridade_Nome = StringUtil.Upper( cgiGet( edtServicoPrioridade_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1441ServicoPrioridade_Nome", A1441ServicoPrioridade_Nome);
               A1442ServicoPrioridade_Finalidade = cgiGet( edtServicoPrioridade_Finalidade_Internalname);
               n1442ServicoPrioridade_Finalidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1442ServicoPrioridade_Finalidade", A1442ServicoPrioridade_Finalidade);
               n1442ServicoPrioridade_Finalidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1442ServicoPrioridade_Finalidade)) ? true : false);
               A1440ServicoPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoPrioridade_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
               /* Read saved values. */
               Z1440ServicoPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1440ServicoPrioridade_Codigo"), ",", "."));
               Z1441ServicoPrioridade_Nome = cgiGet( "Z1441ServicoPrioridade_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ServicoPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSERVICOPRIORIDADE_CODIGO"), ",", "."));
               AV11Insert_ServicoPrioridade_SrvCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICOPRIORIDADE_SRVCOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ServicoPrioridade";
               A1440ServicoPrioridade_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoPrioridade_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1440ServicoPrioridade_Codigo != Z1440ServicoPrioridade_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("servicoprioridade:[SecurityCheckFailed value for]"+"ServicoPrioridade_Codigo:"+context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("servicoprioridade:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1440ServicoPrioridade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode171 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode171;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound171 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3Q0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SERVICOPRIORIDADE_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtServicoPrioridade_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113Q2 */
                           E113Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123Q2 */
                           E123Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123Q2 */
            E123Q2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3Q171( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3Q171( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3Q0( )
      {
         BeforeValidate3Q171( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3Q171( ) ;
            }
            else
            {
               CheckExtendedTable3Q171( ) ;
               CloseExtendedTableCursors3Q171( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3Q0( )
      {
      }

      protected void E113Q2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ServicoPrioridade_SrvCod") == 0 )
               {
                  AV11Insert_ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ServicoPrioridade_SrvCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtServicoPrioridade_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoPrioridade_Codigo_Visible), 5, 0)));
      }

      protected void E123Q2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwservicoprioridade.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A1439ServicoPrioridade_SrvCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3Q171( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1441ServicoPrioridade_Nome = T003Q3_A1441ServicoPrioridade_Nome[0];
            }
            else
            {
               Z1441ServicoPrioridade_Nome = A1441ServicoPrioridade_Nome;
            }
         }
         if ( GX_JID == -7 )
         {
            Z1439ServicoPrioridade_SrvCod = A1439ServicoPrioridade_SrvCod;
            Z1440ServicoPrioridade_Codigo = A1440ServicoPrioridade_Codigo;
            Z1441ServicoPrioridade_Nome = A1441ServicoPrioridade_Nome;
            Z1442ServicoPrioridade_Finalidade = A1442ServicoPrioridade_Finalidade;
         }
      }

      protected void standaloneNotModal( )
      {
         dynServicoPrioridade_SrvCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoPrioridade_SrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoPrioridade_SrvCod.Enabled), 5, 0)));
         edtServicoPrioridade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoPrioridade_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "ServicoPrioridade";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         dynServicoPrioridade_SrvCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoPrioridade_SrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoPrioridade_SrvCod.Enabled), 5, 0)));
         edtServicoPrioridade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoPrioridade_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ServicoPrioridade_Codigo) )
         {
            A1440ServicoPrioridade_Codigo = AV7ServicoPrioridade_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
         }
         /* Using cursor T003Q4 */
         pr_default.execute(2, new Object[] {A1439ServicoPrioridade_SrvCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Servico Prioridade_Servico'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ServicoPrioridade_SrvCod) )
            {
               A1439ServicoPrioridade_SrvCod = AV11Insert_ServicoPrioridade_SrvCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
            }
         }
      }

      protected void Load3Q171( )
      {
         /* Using cursor T003Q6 */
         pr_default.execute(4, new Object[] {A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound171 = 1;
            A1441ServicoPrioridade_Nome = T003Q6_A1441ServicoPrioridade_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1441ServicoPrioridade_Nome", A1441ServicoPrioridade_Nome);
            A1442ServicoPrioridade_Finalidade = T003Q6_A1442ServicoPrioridade_Finalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1442ServicoPrioridade_Finalidade", A1442ServicoPrioridade_Finalidade);
            n1442ServicoPrioridade_Finalidade = T003Q6_n1442ServicoPrioridade_Finalidade[0];
            ZM3Q171( -7) ;
         }
         pr_default.close(4);
         OnLoadActions3Q171( ) ;
      }

      protected void OnLoadActions3Q171( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ServicoPrioridade_SrvCod) )
         {
            A1439ServicoPrioridade_SrvCod = AV11Insert_ServicoPrioridade_SrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
         }
      }

      protected void CheckExtendedTable3Q171( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ServicoPrioridade_SrvCod) )
         {
            A1439ServicoPrioridade_SrvCod = AV11Insert_ServicoPrioridade_SrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1439ServicoPrioridade_SrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1439ServicoPrioridade_SrvCod), 6, 0)));
         }
      }

      protected void CloseExtendedTableCursors3Q171( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3Q171( )
      {
         /* Using cursor T003Q7 */
         pr_default.execute(5, new Object[] {A1440ServicoPrioridade_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound171 = 1;
         }
         else
         {
            RcdFound171 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003Q3 */
         pr_default.execute(1, new Object[] {A1440ServicoPrioridade_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T003Q3_A1439ServicoPrioridade_SrvCod[0] == A1439ServicoPrioridade_SrvCod ) )
         {
            ZM3Q171( 7) ;
            RcdFound171 = 1;
            A1440ServicoPrioridade_Codigo = T003Q3_A1440ServicoPrioridade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
            A1441ServicoPrioridade_Nome = T003Q3_A1441ServicoPrioridade_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1441ServicoPrioridade_Nome", A1441ServicoPrioridade_Nome);
            A1442ServicoPrioridade_Finalidade = T003Q3_A1442ServicoPrioridade_Finalidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1442ServicoPrioridade_Finalidade", A1442ServicoPrioridade_Finalidade);
            n1442ServicoPrioridade_Finalidade = T003Q3_n1442ServicoPrioridade_Finalidade[0];
            Z1440ServicoPrioridade_Codigo = A1440ServicoPrioridade_Codigo;
            sMode171 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3Q171( ) ;
            if ( AnyError == 1 )
            {
               RcdFound171 = 0;
               InitializeNonKey3Q171( ) ;
            }
            Gx_mode = sMode171;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound171 = 0;
            InitializeNonKey3Q171( ) ;
            sMode171 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode171;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3Q171( ) ;
         if ( RcdFound171 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound171 = 0;
         /* Using cursor T003Q8 */
         pr_default.execute(6, new Object[] {A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003Q8_A1440ServicoPrioridade_Codigo[0] < A1440ServicoPrioridade_Codigo ) ) && ( T003Q8_A1439ServicoPrioridade_SrvCod[0] == A1439ServicoPrioridade_SrvCod ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003Q8_A1440ServicoPrioridade_Codigo[0] > A1440ServicoPrioridade_Codigo ) ) && ( T003Q8_A1439ServicoPrioridade_SrvCod[0] == A1439ServicoPrioridade_SrvCod ) )
            {
               A1440ServicoPrioridade_Codigo = T003Q8_A1440ServicoPrioridade_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
               RcdFound171 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound171 = 0;
         /* Using cursor T003Q9 */
         pr_default.execute(7, new Object[] {A1440ServicoPrioridade_Codigo, A1439ServicoPrioridade_SrvCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003Q9_A1440ServicoPrioridade_Codigo[0] > A1440ServicoPrioridade_Codigo ) ) && ( T003Q9_A1439ServicoPrioridade_SrvCod[0] == A1439ServicoPrioridade_SrvCod ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003Q9_A1440ServicoPrioridade_Codigo[0] < A1440ServicoPrioridade_Codigo ) ) && ( T003Q9_A1439ServicoPrioridade_SrvCod[0] == A1439ServicoPrioridade_SrvCod ) )
            {
               A1440ServicoPrioridade_Codigo = T003Q9_A1440ServicoPrioridade_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
               RcdFound171 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3Q171( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtServicoPrioridade_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3Q171( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound171 == 1 )
            {
               if ( A1440ServicoPrioridade_Codigo != Z1440ServicoPrioridade_Codigo )
               {
                  A1440ServicoPrioridade_Codigo = Z1440ServicoPrioridade_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICOPRIORIDADE_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServicoPrioridade_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtServicoPrioridade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3Q171( ) ;
                  GX_FocusControl = edtServicoPrioridade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1440ServicoPrioridade_Codigo != Z1440ServicoPrioridade_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtServicoPrioridade_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3Q171( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICOPRIORIDADE_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtServicoPrioridade_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtServicoPrioridade_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3Q171( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1440ServicoPrioridade_Codigo != Z1440ServicoPrioridade_Codigo )
         {
            A1440ServicoPrioridade_Codigo = Z1440ServicoPrioridade_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICOPRIORIDADE_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServicoPrioridade_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtServicoPrioridade_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3Q171( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003Q2 */
            pr_default.execute(0, new Object[] {A1440ServicoPrioridade_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoPrioridade"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1441ServicoPrioridade_Nome, T003Q2_A1441ServicoPrioridade_Nome[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1441ServicoPrioridade_Nome, T003Q2_A1441ServicoPrioridade_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("servicoprioridade:[seudo value changed for attri]"+"ServicoPrioridade_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1441ServicoPrioridade_Nome);
                  GXUtil.WriteLogRaw("Current: ",T003Q2_A1441ServicoPrioridade_Nome[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoPrioridade"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3Q171( )
      {
         BeforeValidate3Q171( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3Q171( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3Q171( 0) ;
            CheckOptimisticConcurrency3Q171( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3Q171( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3Q171( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003Q10 */
                     pr_default.execute(8, new Object[] {A1439ServicoPrioridade_SrvCod, A1441ServicoPrioridade_Nome, n1442ServicoPrioridade_Finalidade, A1442ServicoPrioridade_Finalidade});
                     A1440ServicoPrioridade_Codigo = T003Q10_A1440ServicoPrioridade_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoPrioridade") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3Q0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3Q171( ) ;
            }
            EndLevel3Q171( ) ;
         }
         CloseExtendedTableCursors3Q171( ) ;
      }

      protected void Update3Q171( )
      {
         BeforeValidate3Q171( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3Q171( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3Q171( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3Q171( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3Q171( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003Q11 */
                     pr_default.execute(9, new Object[] {A1439ServicoPrioridade_SrvCod, A1441ServicoPrioridade_Nome, n1442ServicoPrioridade_Finalidade, A1442ServicoPrioridade_Finalidade, A1440ServicoPrioridade_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoPrioridade") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoPrioridade"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3Q171( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3Q171( ) ;
         }
         CloseExtendedTableCursors3Q171( ) ;
      }

      protected void DeferredUpdate3Q171( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3Q171( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3Q171( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3Q171( ) ;
            AfterConfirm3Q171( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3Q171( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003Q12 */
                  pr_default.execute(10, new Object[] {A1440ServicoPrioridade_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoPrioridade") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode171 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3Q171( ) ;
         Gx_mode = sMode171;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3Q171( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3Q171( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3Q171( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ServicoPrioridade");
            if ( AnyError == 0 )
            {
               ConfirmValues3Q0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ServicoPrioridade");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3Q171( )
      {
         /* Scan By routine */
         /* Using cursor T003Q13 */
         pr_default.execute(11, new Object[] {A1439ServicoPrioridade_SrvCod});
         RcdFound171 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound171 = 1;
            A1440ServicoPrioridade_Codigo = T003Q13_A1440ServicoPrioridade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3Q171( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound171 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound171 = 1;
            A1440ServicoPrioridade_Codigo = T003Q13_A1440ServicoPrioridade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3Q171( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3Q171( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3Q171( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3Q171( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3Q171( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3Q171( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3Q171( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3Q171( )
      {
         dynServicoPrioridade_SrvCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoPrioridade_SrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoPrioridade_SrvCod.Enabled), 5, 0)));
         edtServicoPrioridade_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoPrioridade_Nome_Enabled), 5, 0)));
         edtServicoPrioridade_Finalidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Finalidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoPrioridade_Finalidade_Enabled), 5, 0)));
         edtServicoPrioridade_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoPrioridade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoPrioridade_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3Q0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117282111");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ServicoPrioridade_Codigo) + "," + UrlEncode("" +A1439ServicoPrioridade_SrvCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1440ServicoPrioridade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1441ServicoPrioridade_Nome", StringUtil.RTrim( Z1441ServicoPrioridade_Nome));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSERVICOPRIORIDADE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoPrioridade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICOPRIORIDADE_SRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ServicoPrioridade_SrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICOPRIORIDADE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ServicoPrioridade_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ServicoPrioridade";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicoprioridade:[SendSecurityCheck value for]"+"ServicoPrioridade_Codigo:"+context.localUtil.Format( (decimal)(A1440ServicoPrioridade_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("servicoprioridade:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servicoprioridade.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ServicoPrioridade_Codigo) + "," + UrlEncode("" +A1439ServicoPrioridade_SrvCod) ;
      }

      public override String GetPgmname( )
      {
         return "ServicoPrioridade" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prioridade padr�o" ;
      }

      protected void InitializeNonKey3Q171( )
      {
         A1441ServicoPrioridade_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1441ServicoPrioridade_Nome", A1441ServicoPrioridade_Nome);
         A1442ServicoPrioridade_Finalidade = "";
         n1442ServicoPrioridade_Finalidade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1442ServicoPrioridade_Finalidade", A1442ServicoPrioridade_Finalidade);
         n1442ServicoPrioridade_Finalidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1442ServicoPrioridade_Finalidade)) ? true : false);
         Z1441ServicoPrioridade_Nome = "";
      }

      protected void InitAll3Q171( )
      {
         A1440ServicoPrioridade_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1440ServicoPrioridade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1440ServicoPrioridade_Codigo), 6, 0)));
         InitializeNonKey3Q171( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117282124");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servicoprioridade.js", "?20203117282124");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockservicoprioridade_srvcod_Internalname = "TEXTBLOCKSERVICOPRIORIDADE_SRVCOD";
         dynServicoPrioridade_SrvCod_Internalname = "SERVICOPRIORIDADE_SRVCOD";
         lblTextblockservicoprioridade_nome_Internalname = "TEXTBLOCKSERVICOPRIORIDADE_NOME";
         edtServicoPrioridade_Nome_Internalname = "SERVICOPRIORIDADE_NOME";
         lblTextblockservicoprioridade_finalidade_Internalname = "TEXTBLOCKSERVICOPRIORIDADE_FINALIDADE";
         edtServicoPrioridade_Finalidade_Internalname = "SERVICOPRIORIDADE_FINALIDADE";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtServicoPrioridade_Codigo_Internalname = "SERVICOPRIORIDADE_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Prioridade padr�o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Prioridade padr�o";
         edtServicoPrioridade_Finalidade_Enabled = 1;
         edtServicoPrioridade_Nome_Jsonclick = "";
         edtServicoPrioridade_Nome_Enabled = 1;
         dynServicoPrioridade_SrvCod_Jsonclick = "";
         dynServicoPrioridade_SrvCod.Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtServicoPrioridade_Codigo_Jsonclick = "";
         edtServicoPrioridade_Codigo_Enabled = 0;
         edtServicoPrioridade_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLASERVICOPRIORIDADE_SRVCOD3Q1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICOPRIORIDADE_SRVCOD_data3Q1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICOPRIORIDADE_SRVCOD_html3Q1( )
      {
         int gxdynajaxvalue ;
         GXDLASERVICOPRIORIDADE_SRVCOD_data3Q1( ) ;
         gxdynajaxindex = 1;
         dynServicoPrioridade_SrvCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServicoPrioridade_SrvCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICOPRIORIDADE_SRVCOD_data3Q1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T003Q14 */
         pr_default.execute(12);
         while ( (pr_default.getStatus(12) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003Q14_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T003Q14_A608Servico_Nome[0]));
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ServicoPrioridade_Codigo',fld:'vSERVICOPRIORIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1439ServicoPrioridade_SrvCod',fld:'SERVICOPRIORIDADE_SRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123Q2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1441ServicoPrioridade_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T003Q5_A155Servico_Codigo = new int[1] ;
         T003Q5_A608Servico_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockservicoprioridade_srvcod_Jsonclick = "";
         lblTextblockservicoprioridade_nome_Jsonclick = "";
         A1441ServicoPrioridade_Nome = "";
         lblTextblockservicoprioridade_finalidade_Jsonclick = "";
         A1442ServicoPrioridade_Finalidade = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode171 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1442ServicoPrioridade_Finalidade = "";
         T003Q4_A1439ServicoPrioridade_SrvCod = new int[1] ;
         T003Q6_A1439ServicoPrioridade_SrvCod = new int[1] ;
         T003Q6_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003Q6_A1441ServicoPrioridade_Nome = new String[] {""} ;
         T003Q6_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         T003Q6_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         T003Q7_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003Q3_A1439ServicoPrioridade_SrvCod = new int[1] ;
         T003Q3_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003Q3_A1441ServicoPrioridade_Nome = new String[] {""} ;
         T003Q3_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         T003Q3_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         T003Q8_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003Q8_A1439ServicoPrioridade_SrvCod = new int[1] ;
         T003Q9_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003Q9_A1439ServicoPrioridade_SrvCod = new int[1] ;
         T003Q2_A1439ServicoPrioridade_SrvCod = new int[1] ;
         T003Q2_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003Q2_A1441ServicoPrioridade_Nome = new String[] {""} ;
         T003Q2_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         T003Q2_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         T003Q10_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003Q13_A1440ServicoPrioridade_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003Q14_A155Servico_Codigo = new int[1] ;
         T003Q14_A608Servico_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoprioridade__default(),
            new Object[][] {
                new Object[] {
               T003Q2_A1439ServicoPrioridade_SrvCod, T003Q2_A1440ServicoPrioridade_Codigo, T003Q2_A1441ServicoPrioridade_Nome, T003Q2_A1442ServicoPrioridade_Finalidade, T003Q2_n1442ServicoPrioridade_Finalidade
               }
               , new Object[] {
               T003Q3_A1439ServicoPrioridade_SrvCod, T003Q3_A1440ServicoPrioridade_Codigo, T003Q3_A1441ServicoPrioridade_Nome, T003Q3_A1442ServicoPrioridade_Finalidade, T003Q3_n1442ServicoPrioridade_Finalidade
               }
               , new Object[] {
               T003Q4_A1439ServicoPrioridade_SrvCod
               }
               , new Object[] {
               T003Q5_A155Servico_Codigo, T003Q5_A608Servico_Nome
               }
               , new Object[] {
               T003Q6_A1439ServicoPrioridade_SrvCod, T003Q6_A1440ServicoPrioridade_Codigo, T003Q6_A1441ServicoPrioridade_Nome, T003Q6_A1442ServicoPrioridade_Finalidade, T003Q6_n1442ServicoPrioridade_Finalidade
               }
               , new Object[] {
               T003Q7_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               T003Q8_A1440ServicoPrioridade_Codigo, T003Q8_A1439ServicoPrioridade_SrvCod
               }
               , new Object[] {
               T003Q9_A1440ServicoPrioridade_Codigo, T003Q9_A1439ServicoPrioridade_SrvCod
               }
               , new Object[] {
               T003Q10_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003Q13_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               T003Q14_A155Servico_Codigo, T003Q14_A608Servico_Nome
               }
            }
         );
         Z1439ServicoPrioridade_SrvCod = 0;
         A1439ServicoPrioridade_SrvCod = 0;
         AV13Pgmname = "ServicoPrioridade";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound171 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7ServicoPrioridade_Codigo ;
      private int wcpOA1439ServicoPrioridade_SrvCod ;
      private int Z1440ServicoPrioridade_Codigo ;
      private int AV7ServicoPrioridade_Codigo ;
      private int A1439ServicoPrioridade_SrvCod ;
      private int trnEnded ;
      private int A1440ServicoPrioridade_Codigo ;
      private int edtServicoPrioridade_Codigo_Enabled ;
      private int edtServicoPrioridade_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtServicoPrioridade_Nome_Enabled ;
      private int edtServicoPrioridade_Finalidade_Enabled ;
      private int AV11Insert_ServicoPrioridade_SrvCod ;
      private int AV14GXV1 ;
      private int Z1439ServicoPrioridade_SrvCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1441ServicoPrioridade_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtServicoPrioridade_Nome_Internalname ;
      private String edtServicoPrioridade_Codigo_Internalname ;
      private String edtServicoPrioridade_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservicoprioridade_srvcod_Internalname ;
      private String lblTextblockservicoprioridade_srvcod_Jsonclick ;
      private String dynServicoPrioridade_SrvCod_Internalname ;
      private String dynServicoPrioridade_SrvCod_Jsonclick ;
      private String lblTextblockservicoprioridade_nome_Internalname ;
      private String lblTextblockservicoprioridade_nome_Jsonclick ;
      private String A1441ServicoPrioridade_Nome ;
      private String edtServicoPrioridade_Nome_Jsonclick ;
      private String lblTextblockservicoprioridade_finalidade_Internalname ;
      private String lblTextblockservicoprioridade_finalidade_Jsonclick ;
      private String edtServicoPrioridade_Finalidade_Internalname ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode171 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1442ServicoPrioridade_Finalidade ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A1442ServicoPrioridade_Finalidade ;
      private String Z1442ServicoPrioridade_Finalidade ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IDataStoreProvider pr_default ;
      private int[] T003Q5_A155Servico_Codigo ;
      private String[] T003Q5_A608Servico_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ServicoPrioridade_SrvCod ;
      private GXCombobox dynServicoPrioridade_SrvCod ;
      private int[] T003Q4_A1439ServicoPrioridade_SrvCod ;
      private int[] T003Q6_A1439ServicoPrioridade_SrvCod ;
      private int[] T003Q6_A1440ServicoPrioridade_Codigo ;
      private String[] T003Q6_A1441ServicoPrioridade_Nome ;
      private String[] T003Q6_A1442ServicoPrioridade_Finalidade ;
      private bool[] T003Q6_n1442ServicoPrioridade_Finalidade ;
      private int[] T003Q7_A1440ServicoPrioridade_Codigo ;
      private int[] T003Q3_A1439ServicoPrioridade_SrvCod ;
      private int[] T003Q3_A1440ServicoPrioridade_Codigo ;
      private String[] T003Q3_A1441ServicoPrioridade_Nome ;
      private String[] T003Q3_A1442ServicoPrioridade_Finalidade ;
      private bool[] T003Q3_n1442ServicoPrioridade_Finalidade ;
      private int[] T003Q8_A1440ServicoPrioridade_Codigo ;
      private int[] T003Q8_A1439ServicoPrioridade_SrvCod ;
      private int[] T003Q9_A1440ServicoPrioridade_Codigo ;
      private int[] T003Q9_A1439ServicoPrioridade_SrvCod ;
      private int[] T003Q2_A1439ServicoPrioridade_SrvCod ;
      private int[] T003Q2_A1440ServicoPrioridade_Codigo ;
      private String[] T003Q2_A1441ServicoPrioridade_Nome ;
      private String[] T003Q2_A1442ServicoPrioridade_Finalidade ;
      private bool[] T003Q2_n1442ServicoPrioridade_Finalidade ;
      private int[] T003Q10_A1440ServicoPrioridade_Codigo ;
      private int[] T003Q13_A1440ServicoPrioridade_Codigo ;
      private int[] T003Q14_A155Servico_Codigo ;
      private String[] T003Q14_A608Servico_Nome ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class servicoprioridade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003Q5 ;
          prmT003Q5 = new Object[] {
          } ;
          Object[] prmT003Q4 ;
          prmT003Q4 = new Object[] {
          new Object[] {"@ServicoPrioridade_SrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q6 ;
          prmT003Q6 = new Object[] {
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoPrioridade_SrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q7 ;
          prmT003Q7 = new Object[] {
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q3 ;
          prmT003Q3 = new Object[] {
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q8 ;
          prmT003Q8 = new Object[] {
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoPrioridade_SrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q9 ;
          prmT003Q9 = new Object[] {
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoPrioridade_SrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q2 ;
          prmT003Q2 = new Object[] {
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q10 ;
          prmT003Q10 = new Object[] {
          new Object[] {"@ServicoPrioridade_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ServicoPrioridade_Finalidade",SqlDbType.VarChar,500,0}
          } ;
          Object[] prmT003Q11 ;
          prmT003Q11 = new Object[] {
          new Object[] {"@ServicoPrioridade_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@ServicoPrioridade_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q12 ;
          prmT003Q12 = new Object[] {
          new Object[] {"@ServicoPrioridade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q13 ;
          prmT003Q13 = new Object[] {
          new Object[] {"@ServicoPrioridade_SrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Q14 ;
          prmT003Q14 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T003Q2", "SELECT [ServicoPrioridade_SrvCod] AS ServicoPrioridade_SrvCod, [ServicoPrioridade_Codigo], [ServicoPrioridade_Nome], [ServicoPrioridade_Finalidade] FROM [ServicoPrioridade] WITH (UPDLOCK) WHERE [ServicoPrioridade_Codigo] = @ServicoPrioridade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q2,1,0,true,false )
             ,new CursorDef("T003Q3", "SELECT [ServicoPrioridade_SrvCod] AS ServicoPrioridade_SrvCod, [ServicoPrioridade_Codigo], [ServicoPrioridade_Nome], [ServicoPrioridade_Finalidade] FROM [ServicoPrioridade] WITH (NOLOCK) WHERE [ServicoPrioridade_Codigo] = @ServicoPrioridade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q3,1,0,true,false )
             ,new CursorDef("T003Q4", "SELECT [Servico_Codigo] AS ServicoPrioridade_SrvCod FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoPrioridade_SrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q4,1,0,true,false )
             ,new CursorDef("T003Q5", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q5,0,0,true,false )
             ,new CursorDef("T003Q6", "SELECT TM1.[ServicoPrioridade_SrvCod] AS ServicoPrioridade_SrvCod, TM1.[ServicoPrioridade_Codigo], TM1.[ServicoPrioridade_Nome], TM1.[ServicoPrioridade_Finalidade] FROM [ServicoPrioridade] TM1 WITH (NOLOCK) WHERE TM1.[ServicoPrioridade_Codigo] = @ServicoPrioridade_Codigo and TM1.[ServicoPrioridade_SrvCod] = @ServicoPrioridade_SrvCod ORDER BY TM1.[ServicoPrioridade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q6,100,0,true,false )
             ,new CursorDef("T003Q7", "SELECT [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK) WHERE [ServicoPrioridade_Codigo] = @ServicoPrioridade_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q7,1,0,true,false )
             ,new CursorDef("T003Q8", "SELECT TOP 1 [ServicoPrioridade_Codigo], [ServicoPrioridade_SrvCod] AS ServicoPrioridade_SrvCod FROM [ServicoPrioridade] WITH (NOLOCK) WHERE ( [ServicoPrioridade_Codigo] > @ServicoPrioridade_Codigo) and [ServicoPrioridade_SrvCod] = @ServicoPrioridade_SrvCod ORDER BY [ServicoPrioridade_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q8,1,0,true,true )
             ,new CursorDef("T003Q9", "SELECT TOP 1 [ServicoPrioridade_Codigo], [ServicoPrioridade_SrvCod] AS ServicoPrioridade_SrvCod FROM [ServicoPrioridade] WITH (NOLOCK) WHERE ( [ServicoPrioridade_Codigo] < @ServicoPrioridade_Codigo) and [ServicoPrioridade_SrvCod] = @ServicoPrioridade_SrvCod ORDER BY [ServicoPrioridade_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q9,1,0,true,true )
             ,new CursorDef("T003Q10", "INSERT INTO [ServicoPrioridade]([ServicoPrioridade_SrvCod], [ServicoPrioridade_Nome], [ServicoPrioridade_Finalidade]) VALUES(@ServicoPrioridade_SrvCod, @ServicoPrioridade_Nome, @ServicoPrioridade_Finalidade); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003Q10)
             ,new CursorDef("T003Q11", "UPDATE [ServicoPrioridade] SET [ServicoPrioridade_SrvCod]=@ServicoPrioridade_SrvCod, [ServicoPrioridade_Nome]=@ServicoPrioridade_Nome, [ServicoPrioridade_Finalidade]=@ServicoPrioridade_Finalidade  WHERE [ServicoPrioridade_Codigo] = @ServicoPrioridade_Codigo", GxErrorMask.GX_NOMASK,prmT003Q11)
             ,new CursorDef("T003Q12", "DELETE FROM [ServicoPrioridade]  WHERE [ServicoPrioridade_Codigo] = @ServicoPrioridade_Codigo", GxErrorMask.GX_NOMASK,prmT003Q12)
             ,new CursorDef("T003Q13", "SELECT [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK) WHERE [ServicoPrioridade_SrvCod] = @ServicoPrioridade_SrvCod ORDER BY [ServicoPrioridade_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q13,100,0,true,false )
             ,new CursorDef("T003Q14", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Q14,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
