/*
               File: WWUsuarioNotifica
        Description:  Usuario Notifica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:13:12.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwusuarionotifica : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwusuarionotifica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwusuarionotifica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
               AV39UsuarioNotifica_NoStatus1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UsuarioNotifica_NoStatus1", AV39UsuarioNotifica_NoStatus1);
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
               AV41UsuarioNotifica_NoStatus2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41UsuarioNotifica_NoStatus2", AV41UsuarioNotifica_NoStatus2);
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV42DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0)));
               AV43UsuarioNotifica_NoStatus3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43UsuarioNotifica_NoStatus3", AV43UsuarioNotifica_NoStatus3);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV45TFUsuarioNotifica_NoStatus = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUsuarioNotifica_NoStatus", AV45TFUsuarioNotifica_NoStatus);
               AV46TFUsuarioNotifica_NoStatus_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFUsuarioNotifica_NoStatus_Sel", AV46TFUsuarioNotifica_NoStatus_Sel);
               AV26ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
               AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace", AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV67Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               A2077UsuarioNotifica_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAS02( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTS02( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203119131311");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwusuarionotifica.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIONOTIFICA_NOSTATUS1", AV39UsuarioNotifica_NoStatus1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIONOTIFICA_NOSTATUS2", AV41UsuarioNotifica_NoStatus2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIONOTIFICA_NOSTATUS3", AV43UsuarioNotifica_NoStatus3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIONOTIFICA_NOSTATUS", AV45TFUsuarioNotifica_NoStatus);
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIONOTIFICA_NOSTATUS_SEL", AV46TFUsuarioNotifica_NoStatus_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV30ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV30ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV48DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIONOTIFICA_NOSTATUSTITLEFILTERDATA", AV44UsuarioNotifica_NoStatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIONOTIFICA_NOSTATUSTITLEFILTERDATA", AV44UsuarioNotifica_NoStatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV67Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Caption", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Tooltip", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Cls", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Filteredtext_set", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Includesortasc", StringUtil.BoolToStr( Ddo_usuarionotifica_nostatus_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_usuarionotifica_nostatus_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Sortedstatus", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Includefilter", StringUtil.BoolToStr( Ddo_usuarionotifica_nostatus_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Filtertype", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Filterisrange", StringUtil.BoolToStr( Ddo_usuarionotifica_nostatus_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Includedatalist", StringUtil.BoolToStr( Ddo_usuarionotifica_nostatus_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Datalisttype", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Datalistproc", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuarionotifica_nostatus_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Sortasc", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Sortdsc", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Loadingdata", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Cleanfilter", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Noresultsfound", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Searchbuttontext", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Activeeventkey", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Filteredtext_get", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIONOTIFICA_NOSTATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_usuarionotifica_nostatus_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WES02( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTS02( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwusuarionotifica.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWUsuarioNotifica" ;
      }

      public override String GetPgmdesc( )
      {
         return " Usuario Notifica" ;
      }

      protected void WBS00( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_S02( true) ;
         }
         else
         {
            wb_table1_2_S02( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWUsuarioNotifica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarionotifica_nostatus_Internalname, AV45TFUsuarioNotifica_NoStatus, StringUtil.RTrim( context.localUtil.Format( AV45TFUsuarioNotifica_NoStatus, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarionotifica_nostatus_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarionotifica_nostatus_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioNotifica.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarionotifica_nostatus_sel_Internalname, AV46TFUsuarioNotifica_NoStatus_Sel, StringUtil.RTrim( context.localUtil.Format( AV46TFUsuarioNotifica_NoStatus_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarionotifica_nostatus_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarionotifica_nostatus_sel_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioNotifica.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIONOTIFICA_NOSTATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Internalname, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioNotifica.htm");
         }
         wbLoad = true;
      }

      protected void STARTS02( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Usuario Notifica", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPS00( ) ;
      }

      protected void WSS02( )
      {
         STARTS02( ) ;
         EVTS02( ) ;
      }

      protected void EVTS02( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11S02 */
                              E11S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12S02 */
                              E12S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIONOTIFICA_NOSTATUS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13S02 */
                              E13S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14S02 */
                              E14S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15S02 */
                              E15S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16S02 */
                              E16S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17S02 */
                              E17S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18S02 */
                              E18S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19S02 */
                              E19S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20S02 */
                              E20S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21S02 */
                              E21S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22S02 */
                              E22S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23S02 */
                              E23S02 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV35Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)) ? AV64Update_GXI : context.convertURL( context.PathToRelativeUrl( AV35Update))));
                              AV36Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)) ? AV65Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV36Delete))));
                              AV37Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)) ? AV66Display_GXI : context.convertURL( context.PathToRelativeUrl( AV37Display))));
                              A2077UsuarioNotifica_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuarioNotifica_Codigo_Internalname), ",", "."));
                              A2076UsuarioNotifica_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtUsuarioNotifica_UsuarioCod_Internalname), ",", "."));
                              A2075UsuarioNotifica_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtUsuarioNotifica_AreaTrabalhoCod_Internalname), ",", "."));
                              A2079UsuarioNotifica_NoStatus = cgiGet( edtUsuarioNotifica_NoStatus_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24S02 */
                                    E24S02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25S02 */
                                    E25S02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26S02 */
                                    E26S02 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV38DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuarionotifica_nostatus1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIONOTIFICA_NOSTATUS1"), AV39UsuarioNotifica_NoStatus1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV40DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuarionotifica_nostatus2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIONOTIFICA_NOSTATUS2"), AV41UsuarioNotifica_NoStatus2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV42DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuarionotifica_nostatus3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIONOTIFICA_NOSTATUS3"), AV43UsuarioNotifica_NoStatus3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuarionotifica_nostatus Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIONOTIFICA_NOSTATUS"), AV45TFUsuarioNotifica_NoStatus) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuarionotifica_nostatus_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIONOTIFICA_NOSTATUS_SEL"), AV46TFUsuarioNotifica_NoStatus_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WES02( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAS02( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("USUARIONOTIFICA_NOSTATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("USUARIONOTIFICA_NOSTATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Cont�m", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("USUARIONOTIFICA_NOSTATUS", "Status", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Cont�m", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV42DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV38DynamicFiltersOperator1 ,
                                       String AV39UsuarioNotifica_NoStatus1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       short AV40DynamicFiltersOperator2 ,
                                       String AV41UsuarioNotifica_NoStatus2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       short AV42DynamicFiltersOperator3 ,
                                       String AV43UsuarioNotifica_NoStatus3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       String AV45TFUsuarioNotifica_NoStatus ,
                                       String AV46TFUsuarioNotifica_NoStatus_Sel ,
                                       short AV26ManageFiltersExecutionStep ,
                                       String AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV67Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       int A2077UsuarioNotifica_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFS02( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIONOTIFICA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2076UsuarioNotifica_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIONOTIFICA_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIONOTIFICA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_NOSTATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2079UsuarioNotifica_NoStatus, ""))));
         GxWebStd.gx_hidden_field( context, "USUARIONOTIFICA_NOSTATUS", A2079UsuarioNotifica_NoStatus);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV42DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFS02( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV67Pgmname = "WWUsuarioNotifica";
         context.Gx_err = 0;
      }

      protected void RFS02( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E25S02 */
         E25S02 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 ,
                                                 AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 ,
                                                 AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 ,
                                                 AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 ,
                                                 AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 ,
                                                 AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 ,
                                                 AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 ,
                                                 AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 ,
                                                 AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 ,
                                                 AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 ,
                                                 AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 ,
                                                 AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel ,
                                                 AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus ,
                                                 A2079UsuarioNotifica_NoStatus ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = StringUtil.Concat( StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1), "%", "");
            lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = StringUtil.Concat( StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1), "%", "");
            lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = StringUtil.Concat( StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2), "%", "");
            lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = StringUtil.Concat( StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2), "%", "");
            lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = StringUtil.Concat( StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3), "%", "");
            lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = StringUtil.Concat( StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3), "%", "");
            lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = StringUtil.Concat( StringUtil.RTrim( AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus), "%", "");
            /* Using cursor H00S02 */
            pr_default.execute(0, new Object[] {lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1, lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1, lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2, lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2, lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3, lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3, lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus, AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2079UsuarioNotifica_NoStatus = H00S02_A2079UsuarioNotifica_NoStatus[0];
               A2075UsuarioNotifica_AreaTrabalhoCod = H00S02_A2075UsuarioNotifica_AreaTrabalhoCod[0];
               A2076UsuarioNotifica_UsuarioCod = H00S02_A2076UsuarioNotifica_UsuarioCod[0];
               A2077UsuarioNotifica_Codigo = H00S02_A2077UsuarioNotifica_Codigo[0];
               /* Execute user event: E26S02 */
               E26S02 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBS00( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = AV39UsuarioNotifica_NoStatus1;
         AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = AV41UsuarioNotifica_NoStatus2;
         AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 = AV42DynamicFiltersOperator3;
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = AV43UsuarioNotifica_NoStatus3;
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = AV45TFUsuarioNotifica_NoStatus;
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = AV46TFUsuarioNotifica_NoStatus_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 ,
                                              AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 ,
                                              AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 ,
                                              AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 ,
                                              AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 ,
                                              AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 ,
                                              AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 ,
                                              AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 ,
                                              AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 ,
                                              AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 ,
                                              AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 ,
                                              AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel ,
                                              AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus ,
                                              A2079UsuarioNotifica_NoStatus ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = StringUtil.Concat( StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1), "%", "");
         lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = StringUtil.Concat( StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1), "%", "");
         lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = StringUtil.Concat( StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2), "%", "");
         lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = StringUtil.Concat( StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2), "%", "");
         lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = StringUtil.Concat( StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3), "%", "");
         lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = StringUtil.Concat( StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3), "%", "");
         lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = StringUtil.Concat( StringUtil.RTrim( AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus), "%", "");
         /* Using cursor H00S03 */
         pr_default.execute(1, new Object[] {lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1, lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1, lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2, lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2, lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3, lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3, lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus, AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel});
         GRID_nRecordCount = H00S03_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = AV39UsuarioNotifica_NoStatus1;
         AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = AV41UsuarioNotifica_NoStatus2;
         AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 = AV42DynamicFiltersOperator3;
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = AV43UsuarioNotifica_NoStatus3;
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = AV45TFUsuarioNotifica_NoStatus;
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = AV46TFUsuarioNotifica_NoStatus_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = AV39UsuarioNotifica_NoStatus1;
         AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = AV41UsuarioNotifica_NoStatus2;
         AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 = AV42DynamicFiltersOperator3;
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = AV43UsuarioNotifica_NoStatus3;
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = AV45TFUsuarioNotifica_NoStatus;
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = AV46TFUsuarioNotifica_NoStatus_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = AV39UsuarioNotifica_NoStatus1;
         AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = AV41UsuarioNotifica_NoStatus2;
         AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 = AV42DynamicFiltersOperator3;
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = AV43UsuarioNotifica_NoStatus3;
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = AV45TFUsuarioNotifica_NoStatus;
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = AV46TFUsuarioNotifica_NoStatus_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = AV39UsuarioNotifica_NoStatus1;
         AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = AV41UsuarioNotifica_NoStatus2;
         AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 = AV42DynamicFiltersOperator3;
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = AV43UsuarioNotifica_NoStatus3;
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = AV45TFUsuarioNotifica_NoStatus;
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = AV46TFUsuarioNotifica_NoStatus_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = AV39UsuarioNotifica_NoStatus1;
         AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = AV41UsuarioNotifica_NoStatus2;
         AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 = AV42DynamicFiltersOperator3;
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = AV43UsuarioNotifica_NoStatus3;
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = AV45TFUsuarioNotifica_NoStatus;
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = AV46TFUsuarioNotifica_NoStatus_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPS00( )
      {
         /* Before Start, stand alone formulas. */
         AV67Pgmname = "WWUsuarioNotifica";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24S02 */
         E24S02 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV30ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV48DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIONOTIFICA_NOSTATUSTITLEFILTERDATA"), AV44UsuarioNotifica_NoStatusTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV38DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
            AV39UsuarioNotifica_NoStatus1 = cgiGet( edtavUsuarionotifica_nostatus1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UsuarioNotifica_NoStatus1", AV39UsuarioNotifica_NoStatus1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV40DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
            AV41UsuarioNotifica_NoStatus2 = cgiGet( edtavUsuarionotifica_nostatus2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41UsuarioNotifica_NoStatus2", AV41UsuarioNotifica_NoStatus2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV42DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0)));
            AV43UsuarioNotifica_NoStatus3 = cgiGet( edtavUsuarionotifica_nostatus3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43UsuarioNotifica_NoStatus3", AV43UsuarioNotifica_NoStatus3);
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV26ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            }
            AV45TFUsuarioNotifica_NoStatus = cgiGet( edtavTfusuarionotifica_nostatus_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUsuarioNotifica_NoStatus", AV45TFUsuarioNotifica_NoStatus);
            AV46TFUsuarioNotifica_NoStatus_Sel = cgiGet( edtavTfusuarionotifica_nostatus_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFUsuarioNotifica_NoStatus_Sel", AV46TFUsuarioNotifica_NoStatus_Sel);
            AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace = cgiGet( edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace", AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV33GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV34GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_usuarionotifica_nostatus_Caption = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Caption");
            Ddo_usuarionotifica_nostatus_Tooltip = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Tooltip");
            Ddo_usuarionotifica_nostatus_Cls = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Cls");
            Ddo_usuarionotifica_nostatus_Filteredtext_set = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Filteredtext_set");
            Ddo_usuarionotifica_nostatus_Selectedvalue_set = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Selectedvalue_set");
            Ddo_usuarionotifica_nostatus_Dropdownoptionstype = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Dropdownoptionstype");
            Ddo_usuarionotifica_nostatus_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Titlecontrolidtoreplace");
            Ddo_usuarionotifica_nostatus_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Includesortasc"));
            Ddo_usuarionotifica_nostatus_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Includesortdsc"));
            Ddo_usuarionotifica_nostatus_Sortedstatus = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Sortedstatus");
            Ddo_usuarionotifica_nostatus_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Includefilter"));
            Ddo_usuarionotifica_nostatus_Filtertype = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Filtertype");
            Ddo_usuarionotifica_nostatus_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Filterisrange"));
            Ddo_usuarionotifica_nostatus_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Includedatalist"));
            Ddo_usuarionotifica_nostatus_Datalisttype = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Datalisttype");
            Ddo_usuarionotifica_nostatus_Datalistproc = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Datalistproc");
            Ddo_usuarionotifica_nostatus_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuarionotifica_nostatus_Sortasc = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Sortasc");
            Ddo_usuarionotifica_nostatus_Sortdsc = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Sortdsc");
            Ddo_usuarionotifica_nostatus_Loadingdata = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Loadingdata");
            Ddo_usuarionotifica_nostatus_Cleanfilter = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Cleanfilter");
            Ddo_usuarionotifica_nostatus_Noresultsfound = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Noresultsfound");
            Ddo_usuarionotifica_nostatus_Searchbuttontext = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_usuarionotifica_nostatus_Activeeventkey = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Activeeventkey");
            Ddo_usuarionotifica_nostatus_Filteredtext_get = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Filteredtext_get");
            Ddo_usuarionotifica_nostatus_Selectedvalue_get = cgiGet( "DDO_USUARIONOTIFICA_NOSTATUS_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV38DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIONOTIFICA_NOSTATUS1"), AV39UsuarioNotifica_NoStatus1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV40DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIONOTIFICA_NOSTATUS2"), AV41UsuarioNotifica_NoStatus2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV42DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIONOTIFICA_NOSTATUS3"), AV43UsuarioNotifica_NoStatus3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIONOTIFICA_NOSTATUS"), AV45TFUsuarioNotifica_NoStatus) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIONOTIFICA_NOSTATUS_SEL"), AV46TFUsuarioNotifica_NoStatus_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24S02 */
         E24S02 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24S02( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "USUARIONOTIFICA_NOSTATUS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "USUARIONOTIFICA_NOSTATUS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "USUARIONOTIFICA_NOSTATUS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfusuarionotifica_nostatus_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuarionotifica_nostatus_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarionotifica_nostatus_Visible), 5, 0)));
         edtavTfusuarionotifica_nostatus_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuarionotifica_nostatus_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarionotifica_nostatus_sel_Visible), 5, 0)));
         Ddo_usuarionotifica_nostatus_Titlecontrolidtoreplace = subGrid_Internalname+"_UsuarioNotifica_NoStatus";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "TitleControlIdToReplace", Ddo_usuarionotifica_nostatus_Titlecontrolidtoreplace);
         AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace = Ddo_usuarionotifica_nostatus_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace", AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace);
         edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Usuario Notifica";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Status", 0);
         cmbavOrderedby.addItem("2", "Codigo", 0);
         cmbavOrderedby.addItem("3", "Usu�rio", 0);
         cmbavOrderedby.addItem("4", "de Trabalho", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV48DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV48DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25S02( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV44UsuarioNotifica_NoStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV26ManageFiltersExecutionStep == 1 )
         {
            AV26ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV26ManageFiltersExecutionStep == 2 )
         {
            AV26ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuarioNotifica_Codigo_Titleformat = 2;
         edtUsuarioNotifica_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Codigo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_Codigo_Internalname, "Title", edtUsuarioNotifica_Codigo_Title);
         edtUsuarioNotifica_UsuarioCod_Titleformat = 2;
         edtUsuarioNotifica_UsuarioCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_UsuarioCod_Internalname, "Title", edtUsuarioNotifica_UsuarioCod_Title);
         edtUsuarioNotifica_AreaTrabalhoCod_Titleformat = 2;
         edtUsuarioNotifica_AreaTrabalhoCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_AreaTrabalhoCod_Internalname, "Title", edtUsuarioNotifica_AreaTrabalhoCod_Title);
         edtUsuarioNotifica_NoStatus_Titleformat = 2;
         edtUsuarioNotifica_NoStatus_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "No Status", AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioNotifica_NoStatus_Internalname, "Title", edtUsuarioNotifica_NoStatus_Title);
         AV33GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GridCurrentPage), 10, 0)));
         AV34GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34GridPageCount), 10, 0)));
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = AV39UsuarioNotifica_NoStatus1;
         AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = AV41UsuarioNotifica_NoStatus2;
         AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 = AV42DynamicFiltersOperator3;
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = AV43UsuarioNotifica_NoStatus3;
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = AV45TFUsuarioNotifica_NoStatus;
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = AV46TFUsuarioNotifica_NoStatus_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44UsuarioNotifica_NoStatusTitleFilterData", AV44UsuarioNotifica_NoStatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30ManageFiltersData", AV30ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12S02( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV32PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV32PageToGo) ;
         }
      }

      protected void E13S02( )
      {
         /* Ddo_usuarionotifica_nostatus_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuarionotifica_nostatus_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarionotifica_nostatus_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "SortedStatus", Ddo_usuarionotifica_nostatus_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarionotifica_nostatus_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarionotifica_nostatus_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "SortedStatus", Ddo_usuarionotifica_nostatus_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarionotifica_nostatus_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFUsuarioNotifica_NoStatus = Ddo_usuarionotifica_nostatus_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUsuarioNotifica_NoStatus", AV45TFUsuarioNotifica_NoStatus);
            AV46TFUsuarioNotifica_NoStatus_Sel = Ddo_usuarionotifica_nostatus_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFUsuarioNotifica_NoStatus_Sel", AV46TFUsuarioNotifica_NoStatus_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26S02( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("usuarionotifica.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2077UsuarioNotifica_Codigo);
            AV35Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV35Update);
            AV64Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV35Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV35Update);
            AV64Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("usuarionotifica.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A2077UsuarioNotifica_Codigo);
            AV36Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV36Delete);
            AV65Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV36Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV36Delete);
            AV65Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewusuarionotifica.aspx") + "?" + UrlEncode("" +A2077UsuarioNotifica_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV37Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV37Display);
            AV66Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV37Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV37Display);
            AV66Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E14S02( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E19S02( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15S02( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20S02( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21S02( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16S02( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22S02( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17S02( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV38DynamicFiltersOperator1, AV39UsuarioNotifica_NoStatus1, AV18DynamicFiltersSelector2, AV40DynamicFiltersOperator2, AV41UsuarioNotifica_NoStatus2, AV21DynamicFiltersSelector3, AV42DynamicFiltersOperator3, AV43UsuarioNotifica_NoStatus3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV45TFUsuarioNotifica_NoStatus, AV46TFUsuarioNotifica_NoStatus_Sel, AV26ManageFiltersExecutionStep, AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace, AV6WWPContext, AV67Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A2077UsuarioNotifica_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23S02( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11S02( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWUsuarioNotificaFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV26ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWUsuarioNotificaFilters")), new Object[] {});
            AV26ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV26ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV27ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWUsuarioNotificaFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV27ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV67Pgmname+"GridState",  AV27ManageFiltersXml) ;
               AV10GridState.FromXml(AV27ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E18S02( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("usuarionotifica.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_usuarionotifica_nostatus_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "SortedStatus", Ddo_usuarionotifica_nostatus_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_usuarionotifica_nostatus_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "SortedStatus", Ddo_usuarionotifica_nostatus_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUsuarionotifica_nostatus1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuarionotifica_nostatus1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuarionotifica_nostatus1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIONOTIFICA_NOSTATUS") == 0 )
         {
            edtavUsuarionotifica_nostatus1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuarionotifica_nostatus1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuarionotifica_nostatus1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUsuarionotifica_nostatus2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuarionotifica_nostatus2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuarionotifica_nostatus2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "USUARIONOTIFICA_NOSTATUS") == 0 )
         {
            edtavUsuarionotifica_nostatus2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuarionotifica_nostatus2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuarionotifica_nostatus2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUsuarionotifica_nostatus3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuarionotifica_nostatus3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuarionotifica_nostatus3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "USUARIONOTIFICA_NOSTATUS") == 0 )
         {
            edtavUsuarionotifica_nostatus3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuarionotifica_nostatus3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuarionotifica_nostatus3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "USUARIONOTIFICA_NOSTATUS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV40DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
         AV41UsuarioNotifica_NoStatus2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41UsuarioNotifica_NoStatus2", AV41UsuarioNotifica_NoStatus2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "USUARIONOTIFICA_NOSTATUS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV42DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0)));
         AV43UsuarioNotifica_NoStatus3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43UsuarioNotifica_NoStatus3", AV43UsuarioNotifica_NoStatus3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV30ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV31ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV31ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV31ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV31ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV31ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV31ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV31ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV31ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         AV28ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWUsuarioNotificaFilters"), "");
         AV68GXV1 = 1;
         while ( AV68GXV1 <= AV28ManageFiltersItems.Count )
         {
            AV29ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV28ManageFiltersItems.Item(AV68GXV1));
            AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV31ManageFiltersDataItem.gxTpr_Title = AV29ManageFiltersItem.gxTpr_Title;
            AV31ManageFiltersDataItem.gxTpr_Eventkey = AV29ManageFiltersItem.gxTpr_Title;
            AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV31ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
            if ( AV30ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV68GXV1 = (int)(AV68GXV1+1);
         }
         if ( AV30ManageFiltersData.Count > 3 )
         {
            AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV31ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
            AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV31ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV31ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV31ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV31ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV31ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV30ManageFiltersData.Add(AV31ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV45TFUsuarioNotifica_NoStatus = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUsuarioNotifica_NoStatus", AV45TFUsuarioNotifica_NoStatus);
         Ddo_usuarionotifica_nostatus_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "FilteredText_set", Ddo_usuarionotifica_nostatus_Filteredtext_set);
         AV46TFUsuarioNotifica_NoStatus_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFUsuarioNotifica_NoStatus_Sel", AV46TFUsuarioNotifica_NoStatus_Sel);
         Ddo_usuarionotifica_nostatus_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "SelectedValue_set", Ddo_usuarionotifica_nostatus_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "USUARIONOTIFICA_NOSTATUS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV38DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
         AV39UsuarioNotifica_NoStatus1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UsuarioNotifica_NoStatus1", AV39UsuarioNotifica_NoStatus1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get(AV67Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV67Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV25Session.Get(AV67Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV69GXV2 = 1;
         while ( AV69GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV69GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIONOTIFICA_NOSTATUS") == 0 )
            {
               AV45TFUsuarioNotifica_NoStatus = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFUsuarioNotifica_NoStatus", AV45TFUsuarioNotifica_NoStatus);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFUsuarioNotifica_NoStatus)) )
               {
                  Ddo_usuarionotifica_nostatus_Filteredtext_set = AV45TFUsuarioNotifica_NoStatus;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "FilteredText_set", Ddo_usuarionotifica_nostatus_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIONOTIFICA_NOSTATUS_SEL") == 0 )
            {
               AV46TFUsuarioNotifica_NoStatus_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFUsuarioNotifica_NoStatus_Sel", AV46TFUsuarioNotifica_NoStatus_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFUsuarioNotifica_NoStatus_Sel)) )
               {
                  Ddo_usuarionotifica_nostatus_Selectedvalue_set = AV46TFUsuarioNotifica_NoStatus_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarionotifica_nostatus_Internalname, "SelectedValue_set", Ddo_usuarionotifica_nostatus_Selectedvalue_set);
               }
            }
            AV69GXV2 = (int)(AV69GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIONOTIFICA_NOSTATUS") == 0 )
            {
               AV38DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)));
               AV39UsuarioNotifica_NoStatus1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39UsuarioNotifica_NoStatus1", AV39UsuarioNotifica_NoStatus1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "USUARIONOTIFICA_NOSTATUS") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)));
                  AV41UsuarioNotifica_NoStatus2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41UsuarioNotifica_NoStatus2", AV41UsuarioNotifica_NoStatus2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "USUARIONOTIFICA_NOSTATUS") == 0 )
                  {
                     AV42DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0)));
                     AV43UsuarioNotifica_NoStatus3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43UsuarioNotifica_NoStatus3", AV43UsuarioNotifica_NoStatus3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV25Session.Get(AV67Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFUsuarioNotifica_NoStatus)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIONOTIFICA_NOSTATUS";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFUsuarioNotifica_NoStatus;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFUsuarioNotifica_NoStatus_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIONOTIFICA_NOSTATUS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFUsuarioNotifica_NoStatus_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV67Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39UsuarioNotifica_NoStatus1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV39UsuarioNotifica_NoStatus1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV38DynamicFiltersOperator1;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41UsuarioNotifica_NoStatus2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41UsuarioNotifica_NoStatus2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV40DynamicFiltersOperator2;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV43UsuarioNotifica_NoStatus3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV43UsuarioNotifica_NoStatus3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV42DynamicFiltersOperator3;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV67Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "UsuarioNotifica";
         AV25Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_S02( true) ;
         }
         else
         {
            wb_table2_8_S02( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_S02( true) ;
         }
         else
         {
            wb_table3_82_S02( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_S02e( true) ;
         }
         else
         {
            wb_table1_2_S02e( false) ;
         }
      }

      protected void wb_table3_82_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_S02( true) ;
         }
         else
         {
            wb_table4_85_S02( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_S02e( true) ;
         }
         else
         {
            wb_table3_82_S02e( false) ;
         }
      }

      protected void wb_table4_85_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuarioNotifica_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuarioNotifica_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuarioNotifica_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuarioNotifica_UsuarioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuarioNotifica_UsuarioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuarioNotifica_UsuarioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuarioNotifica_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuarioNotifica_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuarioNotifica_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuarioNotifica_NoStatus_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuarioNotifica_NoStatus_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuarioNotifica_NoStatus_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV37Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuarioNotifica_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuarioNotifica_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuarioNotifica_UsuarioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuarioNotifica_UsuarioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuarioNotifica_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuarioNotifica_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2079UsuarioNotifica_NoStatus);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuarioNotifica_NoStatus_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuarioNotifica_NoStatus_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_S02e( true) ;
         }
         else
         {
            wb_table4_85_S02e( false) ;
         }
      }

      protected void wb_table2_8_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_S02( true) ;
         }
         else
         {
            wb_table5_11_S02( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_S02( true) ;
         }
         else
         {
            wb_table6_23_S02( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_S02e( true) ;
         }
         else
         {
            wb_table2_8_S02e( false) ;
         }
      }

      protected void wb_table6_23_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_S02( true) ;
         }
         else
         {
            wb_table7_28_S02( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_S02e( true) ;
         }
         else
         {
            wb_table6_23_S02e( false) ;
         }
      }

      protected void wb_table7_28_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWUsuarioNotifica.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_S02( true) ;
         }
         else
         {
            wb_table8_37_S02( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioNotifica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWUsuarioNotifica.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_S02( true) ;
         }
         else
         {
            wb_table9_54_S02( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioNotifica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWUsuarioNotifica.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_S02( true) ;
         }
         else
         {
            wb_table10_71_S02( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_S02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_S02e( true) ;
         }
         else
         {
            wb_table7_28_S02e( false) ;
         }
      }

      protected void wb_table10_71_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWUsuarioNotifica.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuarionotifica_nostatus3_Internalname, AV43UsuarioNotifica_NoStatus3, StringUtil.RTrim( context.localUtil.Format( AV43UsuarioNotifica_NoStatus3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuarionotifica_nostatus3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuarionotifica_nostatus3_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_S02e( true) ;
         }
         else
         {
            wb_table10_71_S02e( false) ;
         }
      }

      protected void wb_table9_54_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWUsuarioNotifica.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV40DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuarionotifica_nostatus2_Internalname, AV41UsuarioNotifica_NoStatus2, StringUtil.RTrim( context.localUtil.Format( AV41UsuarioNotifica_NoStatus2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuarionotifica_nostatus2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuarionotifica_nostatus2_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_S02e( true) ;
         }
         else
         {
            wb_table9_54_S02e( false) ;
         }
      }

      protected void wb_table8_37_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWUsuarioNotifica.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV38DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuarionotifica_nostatus1_Internalname, AV39UsuarioNotifica_NoStatus1, StringUtil.RTrim( context.localUtil.Format( AV39UsuarioNotifica_NoStatus1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuarionotifica_nostatus1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuarionotifica_nostatus1_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_S02e( true) ;
         }
         else
         {
            wb_table8_37_S02e( false) ;
         }
      }

      protected void wb_table5_11_S02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblUsuarionotificatitle_Internalname, "Usuario Notifica", "", "", lblUsuarionotificatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWUsuarioNotifica.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWUsuarioNotifica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_S02e( true) ;
         }
         else
         {
            wb_table5_11_S02e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAS02( ) ;
         WSS02( ) ;
         WES02( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203119131729");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwusuarionotifica.js", "?20203119131729");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_idx;
         edtUsuarioNotifica_Codigo_Internalname = "USUARIONOTIFICA_CODIGO_"+sGXsfl_88_idx;
         edtUsuarioNotifica_UsuarioCod_Internalname = "USUARIONOTIFICA_USUARIOCOD_"+sGXsfl_88_idx;
         edtUsuarioNotifica_AreaTrabalhoCod_Internalname = "USUARIONOTIFICA_AREATRABALHOCOD_"+sGXsfl_88_idx;
         edtUsuarioNotifica_NoStatus_Internalname = "USUARIONOTIFICA_NOSTATUS_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_88_fel_idx;
         edtUsuarioNotifica_Codigo_Internalname = "USUARIONOTIFICA_CODIGO_"+sGXsfl_88_fel_idx;
         edtUsuarioNotifica_UsuarioCod_Internalname = "USUARIONOTIFICA_USUARIOCOD_"+sGXsfl_88_fel_idx;
         edtUsuarioNotifica_AreaTrabalhoCod_Internalname = "USUARIONOTIFICA_AREATRABALHOCOD_"+sGXsfl_88_fel_idx;
         edtUsuarioNotifica_NoStatus_Internalname = "USUARIONOTIFICA_NOSTATUS_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBS00( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV64Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Update)) ? AV64Update_GXI : context.PathToRelativeUrl( AV35Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV36Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV65Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Delete)) ? AV65Delete_GXI : context.PathToRelativeUrl( AV36Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV36Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV37Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV37Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV66Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV37Display)) ? AV66Display_GXI : context.PathToRelativeUrl( AV37Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV37Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuarioNotifica_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2077UsuarioNotifica_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuarioNotifica_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuarioNotifica_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2076UsuarioNotifica_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2076UsuarioNotifica_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuarioNotifica_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuarioNotifica_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuarioNotifica_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuarioNotifica_NoStatus_Internalname,(String)A2079UsuarioNotifica_NoStatus,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuarioNotifica_NoStatus_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A2077UsuarioNotifica_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_USUARIOCOD"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A2076UsuarioNotifica_UsuarioCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_AREATRABALHOCOD"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A2075UsuarioNotifica_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIONOTIFICA_NOSTATUS"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A2079UsuarioNotifica_NoStatus, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblUsuarionotificatitle_Internalname = "USUARIONOTIFICATITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavUsuarionotifica_nostatus1_Internalname = "vUSUARIONOTIFICA_NOSTATUS1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavUsuarionotifica_nostatus2_Internalname = "vUSUARIONOTIFICA_NOSTATUS2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavUsuarionotifica_nostatus3_Internalname = "vUSUARIONOTIFICA_NOSTATUS3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtUsuarioNotifica_Codigo_Internalname = "USUARIONOTIFICA_CODIGO";
         edtUsuarioNotifica_UsuarioCod_Internalname = "USUARIONOTIFICA_USUARIOCOD";
         edtUsuarioNotifica_AreaTrabalhoCod_Internalname = "USUARIONOTIFICA_AREATRABALHOCOD";
         edtUsuarioNotifica_NoStatus_Internalname = "USUARIONOTIFICA_NOSTATUS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfusuarionotifica_nostatus_Internalname = "vTFUSUARIONOTIFICA_NOSTATUS";
         edtavTfusuarionotifica_nostatus_sel_Internalname = "vTFUSUARIONOTIFICA_NOSTATUS_SEL";
         Ddo_usuarionotifica_nostatus_Internalname = "DDO_USUARIONOTIFICA_NOSTATUS";
         edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Internalname = "vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtUsuarioNotifica_NoStatus_Jsonclick = "";
         edtUsuarioNotifica_AreaTrabalhoCod_Jsonclick = "";
         edtUsuarioNotifica_UsuarioCod_Jsonclick = "";
         edtUsuarioNotifica_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavUsuarionotifica_nostatus1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavUsuarionotifica_nostatus2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavUsuarionotifica_nostatus3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtUsuarioNotifica_NoStatus_Titleformat = 0;
         edtUsuarioNotifica_AreaTrabalhoCod_Titleformat = 0;
         edtUsuarioNotifica_UsuarioCod_Titleformat = 0;
         edtUsuarioNotifica_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavUsuarionotifica_nostatus3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavUsuarionotifica_nostatus2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavUsuarionotifica_nostatus1_Visible = 1;
         edtUsuarioNotifica_NoStatus_Title = "No Status";
         edtUsuarioNotifica_AreaTrabalhoCod_Title = "de Trabalho";
         edtUsuarioNotifica_UsuarioCod_Title = "Usu�rio";
         edtUsuarioNotifica_Codigo_Title = "Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Visible = 1;
         edtavTfusuarionotifica_nostatus_sel_Jsonclick = "";
         edtavTfusuarionotifica_nostatus_sel_Visible = 1;
         edtavTfusuarionotifica_nostatus_Jsonclick = "";
         edtavTfusuarionotifica_nostatus_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_usuarionotifica_nostatus_Searchbuttontext = "Pesquisar";
         Ddo_usuarionotifica_nostatus_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuarionotifica_nostatus_Cleanfilter = "Limpar pesquisa";
         Ddo_usuarionotifica_nostatus_Loadingdata = "Carregando dados...";
         Ddo_usuarionotifica_nostatus_Sortdsc = "Ordenar de Z � A";
         Ddo_usuarionotifica_nostatus_Sortasc = "Ordenar de A � Z";
         Ddo_usuarionotifica_nostatus_Datalistupdateminimumcharacters = 0;
         Ddo_usuarionotifica_nostatus_Datalistproc = "GetWWUsuarioNotificaFilterData";
         Ddo_usuarionotifica_nostatus_Datalisttype = "Dynamic";
         Ddo_usuarionotifica_nostatus_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuarionotifica_nostatus_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuarionotifica_nostatus_Filtertype = "Character";
         Ddo_usuarionotifica_nostatus_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuarionotifica_nostatus_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuarionotifica_nostatus_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuarionotifica_nostatus_Titlecontrolidtoreplace = "";
         Ddo_usuarionotifica_nostatus_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuarionotifica_nostatus_Cls = "ColumnSettings";
         Ddo_usuarionotifica_nostatus_Tooltip = "Op��es";
         Ddo_usuarionotifica_nostatus_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Usuario Notifica";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV44UsuarioNotifica_NoStatusTitleFilterData',fld:'vUSUARIONOTIFICA_NOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtUsuarioNotifica_Codigo_Titleformat',ctrl:'USUARIONOTIFICA_CODIGO',prop:'Titleformat'},{av:'edtUsuarioNotifica_Codigo_Title',ctrl:'USUARIONOTIFICA_CODIGO',prop:'Title'},{av:'edtUsuarioNotifica_UsuarioCod_Titleformat',ctrl:'USUARIONOTIFICA_USUARIOCOD',prop:'Titleformat'},{av:'edtUsuarioNotifica_UsuarioCod_Title',ctrl:'USUARIONOTIFICA_USUARIOCOD',prop:'Title'},{av:'edtUsuarioNotifica_AreaTrabalhoCod_Titleformat',ctrl:'USUARIONOTIFICA_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtUsuarioNotifica_AreaTrabalhoCod_Title',ctrl:'USUARIONOTIFICA_AREATRABALHOCOD',prop:'Title'},{av:'edtUsuarioNotifica_NoStatus_Titleformat',ctrl:'USUARIONOTIFICA_NOSTATUS',prop:'Titleformat'},{av:'edtUsuarioNotifica_NoStatus_Title',ctrl:'USUARIONOTIFICA_NOSTATUS',prop:'Title'},{av:'AV33GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV34GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV30ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12S02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_USUARIONOTIFICA_NOSTATUS.ONOPTIONCLICKED","{handler:'E13S02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuarionotifica_nostatus_Activeeventkey',ctrl:'DDO_USUARIONOTIFICA_NOSTATUS',prop:'ActiveEventKey'},{av:'Ddo_usuarionotifica_nostatus_Filteredtext_get',ctrl:'DDO_USUARIONOTIFICA_NOSTATUS',prop:'FilteredText_get'},{av:'Ddo_usuarionotifica_nostatus_Selectedvalue_get',ctrl:'DDO_USUARIONOTIFICA_NOSTATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuarionotifica_nostatus_Sortedstatus',ctrl:'DDO_USUARIONOTIFICA_NOSTATUS',prop:'SortedStatus'},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26S02',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV35Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV36Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV37Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14S02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E19S02',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15S02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUsuarionotifica_nostatus2_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuarionotifica_nostatus3_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUsuarionotifica_nostatus1_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E20S02',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavUsuarionotifica_nostatus1_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E21S02',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16S02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUsuarionotifica_nostatus2_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuarionotifica_nostatus3_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUsuarionotifica_nostatus1_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E22S02',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavUsuarionotifica_nostatus2_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17S02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavUsuarionotifica_nostatus2_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuarionotifica_nostatus3_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavUsuarionotifica_nostatus1_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E23S02',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavUsuarionotifica_nostatus3_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11S02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace',fld:'vDDO_USUARIONOTIFICA_NOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV26ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV45TFUsuarioNotifica_NoStatus',fld:'vTFUSUARIONOTIFICA_NOSTATUS',pic:'',nv:''},{av:'Ddo_usuarionotifica_nostatus_Filteredtext_set',ctrl:'DDO_USUARIONOTIFICA_NOSTATUS',prop:'FilteredText_set'},{av:'AV46TFUsuarioNotifica_NoStatus_Sel',fld:'vTFUSUARIONOTIFICA_NOSTATUS_SEL',pic:'',nv:''},{av:'Ddo_usuarionotifica_nostatus_Selectedvalue_set',ctrl:'DDO_USUARIONOTIFICA_NOSTATUS',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV38DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV39UsuarioNotifica_NoStatus1',fld:'vUSUARIONOTIFICA_NOSTATUS1',pic:'',nv:''},{av:'Ddo_usuarionotifica_nostatus_Sortedstatus',ctrl:'DDO_USUARIONOTIFICA_NOSTATUS',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41UsuarioNotifica_NoStatus2',fld:'vUSUARIONOTIFICA_NOSTATUS2',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV43UsuarioNotifica_NoStatus3',fld:'vUSUARIONOTIFICA_NOSTATUS3',pic:'',nv:''},{av:'edtavUsuarionotifica_nostatus1_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavUsuarionotifica_nostatus2_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavUsuarionotifica_nostatus3_Visible',ctrl:'vUSUARIONOTIFICA_NOSTATUS3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E18S02',iparms:[{av:'A2077UsuarioNotifica_Codigo',fld:'USUARIONOTIFICA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_usuarionotifica_nostatus_Activeeventkey = "";
         Ddo_usuarionotifica_nostatus_Filteredtext_get = "";
         Ddo_usuarionotifica_nostatus_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV39UsuarioNotifica_NoStatus1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV41UsuarioNotifica_NoStatus2 = "";
         AV21DynamicFiltersSelector3 = "";
         AV43UsuarioNotifica_NoStatus3 = "";
         AV45TFUsuarioNotifica_NoStatus = "";
         AV46TFUsuarioNotifica_NoStatus_Sel = "";
         AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV67Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV30ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV44UsuarioNotifica_NoStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_usuarionotifica_nostatus_Filteredtext_set = "";
         Ddo_usuarionotifica_nostatus_Selectedvalue_set = "";
         Ddo_usuarionotifica_nostatus_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV35Update = "";
         AV64Update_GXI = "";
         AV36Delete = "";
         AV65Delete_GXI = "";
         AV37Display = "";
         AV66Display_GXI = "";
         A2079UsuarioNotifica_NoStatus = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = "";
         lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = "";
         lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = "";
         lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = "";
         AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 = "";
         AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 = "";
         AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 = "";
         AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 = "";
         AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 = "";
         AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 = "";
         AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel = "";
         AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus = "";
         H00S02_A2079UsuarioNotifica_NoStatus = new String[] {""} ;
         H00S02_A2075UsuarioNotifica_AreaTrabalhoCod = new int[1] ;
         H00S02_A2076UsuarioNotifica_UsuarioCod = new int[1] ;
         H00S02_A2077UsuarioNotifica_Codigo = new int[1] ;
         H00S03_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV27ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV31ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV28ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV29ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV25Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblUsuarionotificatitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwusuarionotifica__default(),
            new Object[][] {
                new Object[] {
               H00S02_A2079UsuarioNotifica_NoStatus, H00S02_A2075UsuarioNotifica_AreaTrabalhoCod, H00S02_A2076UsuarioNotifica_UsuarioCod, H00S02_A2077UsuarioNotifica_Codigo
               }
               , new Object[] {
               H00S03_AGRID_nRecordCount
               }
            }
         );
         AV67Pgmname = "WWUsuarioNotifica";
         /* GeneXus formulas. */
         AV67Pgmname = "WWUsuarioNotifica";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV38DynamicFiltersOperator1 ;
      private short AV40DynamicFiltersOperator2 ;
      private short AV42DynamicFiltersOperator3 ;
      private short AV26ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 ;
      private short AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 ;
      private short AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 ;
      private short edtUsuarioNotifica_Codigo_Titleformat ;
      private short edtUsuarioNotifica_UsuarioCod_Titleformat ;
      private short edtUsuarioNotifica_AreaTrabalhoCod_Titleformat ;
      private short edtUsuarioNotifica_NoStatus_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A2077UsuarioNotifica_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_usuarionotifica_nostatus_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfusuarionotifica_nostatus_Visible ;
      private int edtavTfusuarionotifica_nostatus_sel_Visible ;
      private int edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Visible ;
      private int A2076UsuarioNotifica_UsuarioCod ;
      private int A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV32PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavUsuarionotifica_nostatus1_Visible ;
      private int edtavUsuarionotifica_nostatus2_Visible ;
      private int edtavUsuarionotifica_nostatus3_Visible ;
      private int AV68GXV1 ;
      private int AV69GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV33GridCurrentPage ;
      private long AV34GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_usuarionotifica_nostatus_Activeeventkey ;
      private String Ddo_usuarionotifica_nostatus_Filteredtext_get ;
      private String Ddo_usuarionotifica_nostatus_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV67Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_usuarionotifica_nostatus_Caption ;
      private String Ddo_usuarionotifica_nostatus_Tooltip ;
      private String Ddo_usuarionotifica_nostatus_Cls ;
      private String Ddo_usuarionotifica_nostatus_Filteredtext_set ;
      private String Ddo_usuarionotifica_nostatus_Selectedvalue_set ;
      private String Ddo_usuarionotifica_nostatus_Dropdownoptionstype ;
      private String Ddo_usuarionotifica_nostatus_Titlecontrolidtoreplace ;
      private String Ddo_usuarionotifica_nostatus_Sortedstatus ;
      private String Ddo_usuarionotifica_nostatus_Filtertype ;
      private String Ddo_usuarionotifica_nostatus_Datalisttype ;
      private String Ddo_usuarionotifica_nostatus_Datalistproc ;
      private String Ddo_usuarionotifica_nostatus_Sortasc ;
      private String Ddo_usuarionotifica_nostatus_Sortdsc ;
      private String Ddo_usuarionotifica_nostatus_Loadingdata ;
      private String Ddo_usuarionotifica_nostatus_Cleanfilter ;
      private String Ddo_usuarionotifica_nostatus_Noresultsfound ;
      private String Ddo_usuarionotifica_nostatus_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfusuarionotifica_nostatus_Internalname ;
      private String edtavTfusuarionotifica_nostatus_Jsonclick ;
      private String edtavTfusuarionotifica_nostatus_sel_Internalname ;
      private String edtavTfusuarionotifica_nostatus_sel_Jsonclick ;
      private String edtavDdo_usuarionotifica_nostatustitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtUsuarioNotifica_Codigo_Internalname ;
      private String edtUsuarioNotifica_UsuarioCod_Internalname ;
      private String edtUsuarioNotifica_AreaTrabalhoCod_Internalname ;
      private String edtUsuarioNotifica_NoStatus_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavUsuarionotifica_nostatus1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavUsuarionotifica_nostatus2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavUsuarionotifica_nostatus3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_usuarionotifica_nostatus_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtUsuarioNotifica_Codigo_Title ;
      private String edtUsuarioNotifica_UsuarioCod_Title ;
      private String edtUsuarioNotifica_AreaTrabalhoCod_Title ;
      private String edtUsuarioNotifica_NoStatus_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavUsuarionotifica_nostatus3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavUsuarionotifica_nostatus2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavUsuarionotifica_nostatus1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblUsuarionotificatitle_Internalname ;
      private String lblUsuarionotificatitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUsuarioNotifica_Codigo_Jsonclick ;
      private String edtUsuarioNotifica_UsuarioCod_Jsonclick ;
      private String edtUsuarioNotifica_AreaTrabalhoCod_Jsonclick ;
      private String edtUsuarioNotifica_NoStatus_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_usuarionotifica_nostatus_Includesortasc ;
      private bool Ddo_usuarionotifica_nostatus_Includesortdsc ;
      private bool Ddo_usuarionotifica_nostatus_Includefilter ;
      private bool Ddo_usuarionotifica_nostatus_Filterisrange ;
      private bool Ddo_usuarionotifica_nostatus_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 ;
      private bool AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV35Update_IsBlob ;
      private bool AV36Delete_IsBlob ;
      private bool AV37Display_IsBlob ;
      private String AV27ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV39UsuarioNotifica_NoStatus1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV41UsuarioNotifica_NoStatus2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV43UsuarioNotifica_NoStatus3 ;
      private String AV45TFUsuarioNotifica_NoStatus ;
      private String AV46TFUsuarioNotifica_NoStatus_Sel ;
      private String AV47ddo_UsuarioNotifica_NoStatusTitleControlIdToReplace ;
      private String AV64Update_GXI ;
      private String AV65Delete_GXI ;
      private String AV66Display_GXI ;
      private String A2079UsuarioNotifica_NoStatus ;
      private String lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 ;
      private String lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 ;
      private String lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 ;
      private String lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus ;
      private String AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 ;
      private String AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 ;
      private String AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 ;
      private String AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 ;
      private String AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 ;
      private String AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 ;
      private String AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel ;
      private String AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus ;
      private String AV35Update ;
      private String AV36Delete ;
      private String AV37Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV25Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00S02_A2079UsuarioNotifica_NoStatus ;
      private int[] H00S02_A2075UsuarioNotifica_AreaTrabalhoCod ;
      private int[] H00S02_A2076UsuarioNotifica_UsuarioCod ;
      private int[] H00S02_A2077UsuarioNotifica_Codigo ;
      private long[] H00S03_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44UsuarioNotifica_NoStatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV28ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV31ManageFiltersDataItem ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV29ManageFiltersItem ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV48DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwusuarionotifica__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00S02( IGxContext context ,
                                             String AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 ,
                                             short AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 ,
                                             String AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 ,
                                             bool AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 ,
                                             short AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 ,
                                             String AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 ,
                                             bool AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 ,
                                             String AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 ,
                                             short AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 ,
                                             String AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 ,
                                             String AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel ,
                                             String AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus ,
                                             String A2079UsuarioNotifica_NoStatus ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [UsuarioNotifica_NoStatus], [UsuarioNotifica_AreaTrabalhoCod], [UsuarioNotifica_UsuarioCod], [UsuarioNotifica_Codigo]";
         sFromString = " FROM [UsuarioNotifica] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like '%' + @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like '%' + @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like '%' + @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like '%' + @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like '%' + @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like '%' + @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] = @AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] = @AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_NoStatus]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_NoStatus] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_UsuarioCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_UsuarioCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_AreaTrabalhoCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [UsuarioNotifica_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00S03( IGxContext context ,
                                             String AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1 ,
                                             short AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 ,
                                             String AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1 ,
                                             bool AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 ,
                                             String AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2 ,
                                             short AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 ,
                                             String AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2 ,
                                             bool AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 ,
                                             String AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3 ,
                                             short AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 ,
                                             String AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3 ,
                                             String AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel ,
                                             String AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus ,
                                             String A2079UsuarioNotifica_NoStatus ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [8] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [UsuarioNotifica] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like '%' + @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like '%' + @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV51WWUsuarioNotificaDS_1_Dynamicfiltersselector1, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV52WWUsuarioNotificaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like '%' + @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like '%' + @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV54WWUsuarioNotificaDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWUsuarioNotificaDS_5_Dynamicfiltersselector2, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV56WWUsuarioNotificaDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like '%' + @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like '%' + @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV58WWUsuarioNotificaDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV59WWUsuarioNotificaDS_9_Dynamicfiltersselector3, "USUARIONOTIFICA_NOSTATUS") == 0 ) && ( AV60WWUsuarioNotificaDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] like @lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] like @lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UsuarioNotifica_NoStatus] = @AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UsuarioNotifica_NoStatus] = @AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00S02(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] );
               case 1 :
                     return conditional_H00S03(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00S02 ;
          prmH00S02 = new Object[] {
          new Object[] {"@lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00S03 ;
          prmH00S03 = new Object[] {
          new Object[] {"@lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV53WWUsuarioNotificaDS_3_Usuarionotifica_nostatus1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV57WWUsuarioNotificaDS_7_Usuarionotifica_nostatus2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV61WWUsuarioNotificaDS_11_Usuarionotifica_nostatus3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV62WWUsuarioNotificaDS_12_Tfusuarionotifica_nostatus",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV63WWUsuarioNotificaDS_13_Tfusuarionotifica_nostatus_sel",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00S02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00S02,11,0,true,false )
             ,new CursorDef("H00S03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00S03,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

}
