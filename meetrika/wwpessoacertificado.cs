/*
               File: WWPessoaCertificado
        Description:  Certificados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:12:5.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwpessoacertificado : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwpessoacertificado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwpessoacertificado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_116 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_116_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_116_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV66PessoaCertificado_Emissao1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PessoaCertificado_Emissao1", context.localUtil.Format(AV66PessoaCertificado_Emissao1, "99/99/99"));
               AV67PessoaCertificado_Emissao_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67PessoaCertificado_Emissao_To1", context.localUtil.Format(AV67PessoaCertificado_Emissao_To1, "99/99/99"));
               AV68PessoaCertificado_Validade1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68PessoaCertificado_Validade1", context.localUtil.Format(AV68PessoaCertificado_Validade1, "99/99/99"));
               AV69PessoaCertificado_Validade_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69PessoaCertificado_Validade_To1", context.localUtil.Format(AV69PessoaCertificado_Validade_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV70PessoaCertificado_Emissao2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PessoaCertificado_Emissao2", context.localUtil.Format(AV70PessoaCertificado_Emissao2, "99/99/99"));
               AV71PessoaCertificado_Emissao_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PessoaCertificado_Emissao_To2", context.localUtil.Format(AV71PessoaCertificado_Emissao_To2, "99/99/99"));
               AV72PessoaCertificado_Validade2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72PessoaCertificado_Validade2", context.localUtil.Format(AV72PessoaCertificado_Validade2, "99/99/99"));
               AV73PessoaCertificado_Validade_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PessoaCertificado_Validade_To2", context.localUtil.Format(AV73PessoaCertificado_Validade_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV74PessoaCertificado_Emissao3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74PessoaCertificado_Emissao3", context.localUtil.Format(AV74PessoaCertificado_Emissao3, "99/99/99"));
               AV75PessoaCertificado_Emissao_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PessoaCertificado_Emissao_To3", context.localUtil.Format(AV75PessoaCertificado_Emissao_To3, "99/99/99"));
               AV76PessoaCertificado_Validade3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76PessoaCertificado_Validade3", context.localUtil.Format(AV76PessoaCertificado_Validade3, "99/99/99"));
               AV77PessoaCertificado_Validade_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77PessoaCertificado_Validade_To3", context.localUtil.Format(AV77PessoaCertificado_Validade_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV50TFPessoaCertificado_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFPessoaCertificado_Nome", AV50TFPessoaCertificado_Nome);
               AV51TFPessoaCertificado_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFPessoaCertificado_Nome_Sel", AV51TFPessoaCertificado_Nome_Sel);
               AV44TFPessoaCertificado_Emissao = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFPessoaCertificado_Emissao", context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"));
               AV45TFPessoaCertificado_Emissao_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"));
               AV54TFPessoaCertificado_Validade = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFPessoaCertificado_Validade", context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"));
               AV55TFPessoaCertificado_Validade_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPessoaCertificado_Validade_To", context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"));
               AV29ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV29ManageFiltersExecutionStep), 1, 0));
               AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace", AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace);
               AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace", AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace);
               AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace", AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace);
               AV107Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A2010PessoaCertificado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV78PessoaCertificado_PesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PARA2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTRA2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311912637");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwpessoacertificado.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_EMISSAO1", context.localUtil.Format(AV66PessoaCertificado_Emissao1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_EMISSAO_TO1", context.localUtil.Format(AV67PessoaCertificado_Emissao_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_VALIDADE1", context.localUtil.Format(AV68PessoaCertificado_Validade1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_VALIDADE_TO1", context.localUtil.Format(AV69PessoaCertificado_Validade_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_EMISSAO2", context.localUtil.Format(AV70PessoaCertificado_Emissao2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_EMISSAO_TO2", context.localUtil.Format(AV71PessoaCertificado_Emissao_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_VALIDADE2", context.localUtil.Format(AV72PessoaCertificado_Validade2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_VALIDADE_TO2", context.localUtil.Format(AV73PessoaCertificado_Validade_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_EMISSAO3", context.localUtil.Format(AV74PessoaCertificado_Emissao3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_EMISSAO_TO3", context.localUtil.Format(AV75PessoaCertificado_Emissao_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_VALIDADE3", context.localUtil.Format(AV76PessoaCertificado_Validade3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPESSOACERTIFICADO_VALIDADE_TO3", context.localUtil.Format(AV77PessoaCertificado_Validade_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOACERTIFICADO_NOME", StringUtil.RTrim( AV50TFPessoaCertificado_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOACERTIFICADO_NOME_SEL", StringUtil.RTrim( AV51TFPessoaCertificado_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOACERTIFICADO_EMISSAO", context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOACERTIFICADO_EMISSAO_TO", context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOACERTIFICADO_VALIDADE", context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPESSOACERTIFICADO_VALIDADE_TO", context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_116", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_116), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV33ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV33ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV59DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV59DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOACERTIFICADO_NOMETITLEFILTERDATA", AV49PessoaCertificado_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOACERTIFICADO_NOMETITLEFILTERDATA", AV49PessoaCertificado_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA", AV43PessoaCertificado_EmissaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA", AV43PessoaCertificado_EmissaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA", AV53PessoaCertificado_ValidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA", AV53PessoaCertificado_ValidadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV107Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPESSOACERTIFICADO_PESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78PessoaCertificado_PesCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Caption", StringUtil.RTrim( Ddo_pessoacertificado_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Tooltip", StringUtil.RTrim( Ddo_pessoacertificado_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Cls", StringUtil.RTrim( Ddo_pessoacertificado_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_pessoacertificado_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_pessoacertificado_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoacertificado_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoacertificado_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_pessoacertificado_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Filtertype", StringUtil.RTrim( Ddo_pessoacertificado_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_pessoacertificado_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Datalisttype", StringUtil.RTrim( Ddo_pessoacertificado_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Datalistproc", StringUtil.RTrim( Ddo_pessoacertificado_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Sortasc", StringUtil.RTrim( Ddo_pessoacertificado_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Sortdsc", StringUtil.RTrim( Ddo_pessoacertificado_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Loadingdata", StringUtil.RTrim( Ddo_pessoacertificado_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_pessoacertificado_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_pessoacertificado_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_pessoacertificado_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Caption", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Tooltip", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Cls", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_set", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_set", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Includesortasc", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Sortedstatus", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Includefilter", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Filtertype", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Filterisrange", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Includedatalist", StringUtil.BoolToStr( Ddo_pessoacertificado_emissao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Sortasc", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Sortdsc", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Cleanfilter", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterfrom", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterto", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Searchbuttontext", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Caption", StringUtil.RTrim( Ddo_pessoacertificado_validade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Tooltip", StringUtil.RTrim( Ddo_pessoacertificado_validade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Cls", StringUtil.RTrim( Ddo_pessoacertificado_validade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_set", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_pessoacertificado_validade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_pessoacertificado_validade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Sortedstatus", StringUtil.RTrim( Ddo_pessoacertificado_validade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Includefilter", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Filtertype", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_pessoacertificado_validade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Sortasc", StringUtil.RTrim( Ddo_pessoacertificado_validade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Sortdsc", StringUtil.RTrim( Ddo_pessoacertificado_validade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Cleanfilter", StringUtil.RTrim( Ddo_pessoacertificado_validade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_pessoacertificado_validade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterto", StringUtil.RTrim( Ddo_pessoacertificado_validade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_pessoacertificado_validade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_pessoacertificado_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_pessoacertificado_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_pessoacertificado_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Activeeventkey", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_get", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_get", StringUtil.RTrim( Ddo_pessoacertificado_emissao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Activeeventkey", StringUtil.RTrim( Ddo_pessoacertificado_validade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_pessoacertificado_validade_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WERA2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTRA2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwpessoacertificado.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWPessoaCertificado" ;
      }

      public override String GetPgmdesc( )
      {
         return " Certificados" ;
      }

      protected void WBRA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_RA2( true) ;
         }
         else
         {
            wb_table1_2_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(129, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(130, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV29ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_nome_Internalname, StringUtil.RTrim( AV50TFPessoaCertificado_Nome), StringUtil.RTrim( context.localUtil.Format( AV50TFPessoaCertificado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoaCertificado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_nome_sel_Internalname, StringUtil.RTrim( AV51TFPessoaCertificado_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV51TFPessoaCertificado_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPessoaCertificado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_emissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_emissao_Internalname, context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"), context.localUtil.Format( AV44TFPessoaCertificado_Emissao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_emissao_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_emissao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_emissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_emissao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_emissao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_emissao_to_Internalname, context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"), context.localUtil.Format( AV45TFPessoaCertificado_Emissao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_emissao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_emissao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_emissao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_emissao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_pessoacertificado_emissaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_emissaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_emissaoauxdate_Internalname, context.localUtil.Format(AV46DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"), context.localUtil.Format( AV46DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_emissaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_emissaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_emissaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_emissaoauxdateto_Internalname, context.localUtil.Format(AV47DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV47DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_emissaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_emissaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_validade_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_validade_Internalname, context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"), context.localUtil.Format( AV54TFPessoaCertificado_Validade, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_validade_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_validade_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_validade_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_validade_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfpessoacertificado_validade_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfpessoacertificado_validade_to_Internalname, context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"), context.localUtil.Format( AV55TFPessoaCertificado_Validade_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpessoacertificado_validade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfpessoacertificado_validade_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavTfpessoacertificado_validade_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfpessoacertificado_validade_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_pessoacertificado_validadeauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_validadeauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_validadeauxdate_Internalname, context.localUtil.Format(AV56DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"), context.localUtil.Format( AV56DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,142);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_validadeauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_validadeauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_pessoacertificado_validadeauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_pessoacertificado_validadeauxdateto_Internalname, context.localUtil.Format(AV57DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"), context.localUtil.Format( AV57DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_pessoacertificado_validadeauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_pessoacertificado_validadeauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOACERTIFICADO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", 0, edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoaCertificado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOACERTIFICADO_EMISSAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,147);\"", 0, edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoaCertificado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PESSOACERTIFICADO_VALIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_116_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"", 0, edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPessoaCertificado.htm");
         }
         wbLoad = true;
      }

      protected void STARTRA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Certificados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPRA0( ) ;
      }

      protected void WSRA2( )
      {
         STARTRA2( ) ;
         EVTRA2( ) ;
      }

      protected void EVTRA2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11RA2 */
                              E11RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12RA2 */
                              E12RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOACERTIFICADO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13RA2 */
                              E13RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOACERTIFICADO_EMISSAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14RA2 */
                              E14RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PESSOACERTIFICADO_VALIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15RA2 */
                              E15RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16RA2 */
                              E16RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17RA2 */
                              E17RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18RA2 */
                              E18RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19RA2 */
                              E19RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20RA2 */
                              E20RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21RA2 */
                              E21RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22RA2 */
                              E22RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23RA2 */
                              E23RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24RA2 */
                              E24RA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_116_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1162( ) ;
                              AV63Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV63Update)) ? AV104Update_GXI : context.convertURL( context.PathToRelativeUrl( AV63Update))));
                              AV64Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV64Delete)) ? AV105Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV64Delete))));
                              AV65Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV65Display)) ? AV106Display_GXI : context.convertURL( context.PathToRelativeUrl( AV65Display))));
                              A2010PessoaCertificado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPessoaCertificado_Codigo_Internalname), ",", "."));
                              A2011PessoaCertificado_PesCod = (int)(context.localUtil.CToN( cgiGet( edtPessoaCertificado_PesCod_Internalname), ",", "."));
                              A2013PessoaCertificado_Nome = StringUtil.Upper( cgiGet( edtPessoaCertificado_Nome_Internalname));
                              A2012PessoaCertificado_Emissao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPessoaCertificado_Emissao_Internalname), 0));
                              A2014PessoaCertificado_Validade = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPessoaCertificado_Validade_Internalname), 0));
                              n2014PessoaCertificado_Validade = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25RA2 */
                                    E25RA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26RA2 */
                                    E26RA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27RA2 */
                                    E27RA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_emissao1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO1"), 0) != AV66PessoaCertificado_Emissao1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_emissao_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO_TO1"), 0) != AV67PessoaCertificado_Emissao_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_validade1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE1"), 0) != AV68PessoaCertificado_Validade1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_validade_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE_TO1"), 0) != AV69PessoaCertificado_Validade_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_emissao2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO2"), 0) != AV70PessoaCertificado_Emissao2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_emissao_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO_TO2"), 0) != AV71PessoaCertificado_Emissao_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_validade2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE2"), 0) != AV72PessoaCertificado_Validade2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_validade_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE_TO2"), 0) != AV73PessoaCertificado_Validade_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_emissao3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO3"), 0) != AV74PessoaCertificado_Emissao3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_emissao_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO_TO3"), 0) != AV75PessoaCertificado_Emissao_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_validade3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE3"), 0) != AV76PessoaCertificado_Validade3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Pessoacertificado_validade_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE_TO3"), 0) != AV77PessoaCertificado_Validade_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoacertificado_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOACERTIFICADO_NOME"), AV50TFPessoaCertificado_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoacertificado_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOACERTIFICADO_NOME_SEL"), AV51TFPessoaCertificado_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoacertificado_emissao Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_EMISSAO"), 0) != AV44TFPessoaCertificado_Emissao )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoacertificado_emissao_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_EMISSAO_TO"), 0) != AV45TFPessoaCertificado_Emissao_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoacertificado_validade Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_VALIDADE"), 0) != AV54TFPessoaCertificado_Validade )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfpessoacertificado_validade_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_VALIDADE_TO"), 0) != AV55TFPessoaCertificado_Validade_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PARA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PESSOACERTIFICADO_EMISSAO", "Emiss�o", 0);
            cmbavDynamicfiltersselector1.addItem("PESSOACERTIFICADO_VALIDADE", "Validade", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PESSOACERTIFICADO_EMISSAO", "Emiss�o", 0);
            cmbavDynamicfiltersselector2.addItem("PESSOACERTIFICADO_VALIDADE", "Validade", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("PESSOACERTIFICADO_EMISSAO", "Emiss�o", 0);
            cmbavDynamicfiltersselector3.addItem("PESSOACERTIFICADO_VALIDADE", "Validade", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1162( ) ;
         while ( nGXsfl_116_idx <= nRC_GXsfl_116 )
         {
            sendrow_1162( ) ;
            nGXsfl_116_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_116_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_116_idx+1));
            sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
            SubsflControlProps_1162( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV66PessoaCertificado_Emissao1 ,
                                       DateTime AV67PessoaCertificado_Emissao_To1 ,
                                       DateTime AV68PessoaCertificado_Validade1 ,
                                       DateTime AV69PessoaCertificado_Validade_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV70PessoaCertificado_Emissao2 ,
                                       DateTime AV71PessoaCertificado_Emissao_To2 ,
                                       DateTime AV72PessoaCertificado_Validade2 ,
                                       DateTime AV73PessoaCertificado_Validade_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV74PessoaCertificado_Emissao3 ,
                                       DateTime AV75PessoaCertificado_Emissao_To3 ,
                                       DateTime AV76PessoaCertificado_Validade3 ,
                                       DateTime AV77PessoaCertificado_Validade_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV50TFPessoaCertificado_Nome ,
                                       String AV51TFPessoaCertificado_Nome_Sel ,
                                       DateTime AV44TFPessoaCertificado_Emissao ,
                                       DateTime AV45TFPessoaCertificado_Emissao_To ,
                                       DateTime AV54TFPessoaCertificado_Validade ,
                                       DateTime AV55TFPessoaCertificado_Validade_To ,
                                       short AV29ManageFiltersExecutionStep ,
                                       String AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace ,
                                       String AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace ,
                                       String AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace ,
                                       String AV107Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A2010PessoaCertificado_Codigo ,
                                       int AV78PessoaCertificado_PesCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFRA2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PESSOACERTIFICADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2010PessoaCertificado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_PESCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2011PessoaCertificado_PesCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PESSOACERTIFICADO_PESCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2011PessoaCertificado_PesCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2013PessoaCertificado_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "PESSOACERTIFICADO_NOME", StringUtil.RTrim( A2013PessoaCertificado_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_EMISSAO", GetSecureSignedToken( "", A2012PessoaCertificado_Emissao));
         GxWebStd.gx_hidden_field( context, "PESSOACERTIFICADO_EMISSAO", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_VALIDADE", GetSecureSignedToken( "", A2014PessoaCertificado_Validade));
         GxWebStd.gx_hidden_field( context, "PESSOACERTIFICADO_VALIDADE", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV107Pgmname = "WWPessoaCertificado";
         context.Gx_err = 0;
      }

      protected void RFRA2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 116;
         /* Execute user event: E26RA2 */
         E26RA2 ();
         nGXsfl_116_idx = 1;
         sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
         SubsflControlProps_1162( ) ;
         nGXsfl_116_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1162( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ,
                                                 AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ,
                                                 AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ,
                                                 AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ,
                                                 AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ,
                                                 AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ,
                                                 AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ,
                                                 AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ,
                                                 AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ,
                                                 AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ,
                                                 AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ,
                                                 AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ,
                                                 AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ,
                                                 AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ,
                                                 AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ,
                                                 AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ,
                                                 AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ,
                                                 AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ,
                                                 AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ,
                                                 AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ,
                                                 AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ,
                                                 AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ,
                                                 AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ,
                                                 A2012PessoaCertificado_Emissao ,
                                                 A2014PessoaCertificado_Validade ,
                                                 A2013PessoaCertificado_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome), 50, "%");
            /* Using cursor H00RA2 */
            pr_default.execute(0, new Object[] {AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1, AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1, AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1, AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1, AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2, AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2, AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2, AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2, AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3, AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3, AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3, AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3, lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome, AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel, AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao, AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to, AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade, AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_116_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2014PessoaCertificado_Validade = H00RA2_A2014PessoaCertificado_Validade[0];
               n2014PessoaCertificado_Validade = H00RA2_n2014PessoaCertificado_Validade[0];
               A2012PessoaCertificado_Emissao = H00RA2_A2012PessoaCertificado_Emissao[0];
               A2013PessoaCertificado_Nome = H00RA2_A2013PessoaCertificado_Nome[0];
               A2011PessoaCertificado_PesCod = H00RA2_A2011PessoaCertificado_PesCod[0];
               A2010PessoaCertificado_Codigo = H00RA2_A2010PessoaCertificado_Codigo[0];
               /* Execute user event: E27RA2 */
               E27RA2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 116;
            WBRA0( ) ;
         }
         nGXsfl_116_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV66PessoaCertificado_Emissao1;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV67PessoaCertificado_Emissao_To1;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV68PessoaCertificado_Validade1;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV69PessoaCertificado_Validade_To1;
         AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV70PessoaCertificado_Emissao2;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV71PessoaCertificado_Emissao_To2;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV72PessoaCertificado_Validade2;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV73PessoaCertificado_Validade_To2;
         AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV74PessoaCertificado_Emissao3;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV75PessoaCertificado_Emissao_To3;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV76PessoaCertificado_Validade3;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV77PessoaCertificado_Validade_To3;
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV50TFPessoaCertificado_Nome;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV51TFPessoaCertificado_Nome_Sel;
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV44TFPessoaCertificado_Emissao;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV45TFPessoaCertificado_Emissao_To;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV54TFPessoaCertificado_Validade;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV55TFPessoaCertificado_Validade_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ,
                                              AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ,
                                              AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ,
                                              AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ,
                                              AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ,
                                              AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ,
                                              AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ,
                                              AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ,
                                              AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ,
                                              AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ,
                                              AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ,
                                              AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ,
                                              AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ,
                                              AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ,
                                              AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ,
                                              AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ,
                                              AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ,
                                              AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ,
                                              AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ,
                                              AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ,
                                              AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ,
                                              AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ,
                                              AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ,
                                              A2012PessoaCertificado_Emissao ,
                                              A2014PessoaCertificado_Validade ,
                                              A2013PessoaCertificado_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = StringUtil.PadR( StringUtil.RTrim( AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome), 50, "%");
         /* Using cursor H00RA3 */
         pr_default.execute(1, new Object[] {AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1, AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1, AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1, AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1, AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2, AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2, AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2, AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2, AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3, AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3, AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3, AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3, lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome, AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel, AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao, AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to, AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade, AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to});
         GRID_nRecordCount = H00RA3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV66PessoaCertificado_Emissao1;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV67PessoaCertificado_Emissao_To1;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV68PessoaCertificado_Validade1;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV69PessoaCertificado_Validade_To1;
         AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV70PessoaCertificado_Emissao2;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV71PessoaCertificado_Emissao_To2;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV72PessoaCertificado_Validade2;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV73PessoaCertificado_Validade_To2;
         AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV74PessoaCertificado_Emissao3;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV75PessoaCertificado_Emissao_To3;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV76PessoaCertificado_Validade3;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV77PessoaCertificado_Validade_To3;
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV50TFPessoaCertificado_Nome;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV51TFPessoaCertificado_Nome_Sel;
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV44TFPessoaCertificado_Emissao;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV45TFPessoaCertificado_Emissao_To;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV54TFPessoaCertificado_Validade;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV55TFPessoaCertificado_Validade_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV66PessoaCertificado_Emissao1;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV67PessoaCertificado_Emissao_To1;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV68PessoaCertificado_Validade1;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV69PessoaCertificado_Validade_To1;
         AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV70PessoaCertificado_Emissao2;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV71PessoaCertificado_Emissao_To2;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV72PessoaCertificado_Validade2;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV73PessoaCertificado_Validade_To2;
         AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV74PessoaCertificado_Emissao3;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV75PessoaCertificado_Emissao_To3;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV76PessoaCertificado_Validade3;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV77PessoaCertificado_Validade_To3;
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV50TFPessoaCertificado_Nome;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV51TFPessoaCertificado_Nome_Sel;
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV44TFPessoaCertificado_Emissao;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV45TFPessoaCertificado_Emissao_To;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV54TFPessoaCertificado_Validade;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV55TFPessoaCertificado_Validade_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV66PessoaCertificado_Emissao1;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV67PessoaCertificado_Emissao_To1;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV68PessoaCertificado_Validade1;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV69PessoaCertificado_Validade_To1;
         AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV70PessoaCertificado_Emissao2;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV71PessoaCertificado_Emissao_To2;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV72PessoaCertificado_Validade2;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV73PessoaCertificado_Validade_To2;
         AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV74PessoaCertificado_Emissao3;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV75PessoaCertificado_Emissao_To3;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV76PessoaCertificado_Validade3;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV77PessoaCertificado_Validade_To3;
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV50TFPessoaCertificado_Nome;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV51TFPessoaCertificado_Nome_Sel;
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV44TFPessoaCertificado_Emissao;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV45TFPessoaCertificado_Emissao_To;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV54TFPessoaCertificado_Validade;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV55TFPessoaCertificado_Validade_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV66PessoaCertificado_Emissao1;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV67PessoaCertificado_Emissao_To1;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV68PessoaCertificado_Validade1;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV69PessoaCertificado_Validade_To1;
         AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV70PessoaCertificado_Emissao2;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV71PessoaCertificado_Emissao_To2;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV72PessoaCertificado_Validade2;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV73PessoaCertificado_Validade_To2;
         AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV74PessoaCertificado_Emissao3;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV75PessoaCertificado_Emissao_To3;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV76PessoaCertificado_Validade3;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV77PessoaCertificado_Validade_To3;
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV50TFPessoaCertificado_Nome;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV51TFPessoaCertificado_Nome_Sel;
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV44TFPessoaCertificado_Emissao;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV45TFPessoaCertificado_Emissao_To;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV54TFPessoaCertificado_Validade;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV55TFPessoaCertificado_Validade_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV66PessoaCertificado_Emissao1;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV67PessoaCertificado_Emissao_To1;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV68PessoaCertificado_Validade1;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV69PessoaCertificado_Validade_To1;
         AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV70PessoaCertificado_Emissao2;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV71PessoaCertificado_Emissao_To2;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV72PessoaCertificado_Validade2;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV73PessoaCertificado_Validade_To2;
         AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV74PessoaCertificado_Emissao3;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV75PessoaCertificado_Emissao_To3;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV76PessoaCertificado_Validade3;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV77PessoaCertificado_Validade_To3;
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV50TFPessoaCertificado_Nome;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV51TFPessoaCertificado_Nome_Sel;
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV44TFPessoaCertificado_Emissao;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV45TFPessoaCertificado_Emissao_To;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV54TFPessoaCertificado_Validade;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV55TFPessoaCertificado_Validade_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPRA0( )
      {
         /* Before Start, stand alone formulas. */
         AV107Pgmname = "WWPessoaCertificado";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25RA2 */
         E25RA2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV33ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV59DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOACERTIFICADO_NOMETITLEFILTERDATA"), AV49PessoaCertificado_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA"), AV43PessoaCertificado_EmissaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA"), AV53PessoaCertificado_ValidadeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_emissao1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Emissao1"}), 1, "vPESSOACERTIFICADO_EMISSAO1");
               GX_FocusControl = edtavPessoacertificado_emissao1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66PessoaCertificado_Emissao1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PessoaCertificado_Emissao1", context.localUtil.Format(AV66PessoaCertificado_Emissao1, "99/99/99"));
            }
            else
            {
               AV66PessoaCertificado_Emissao1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_emissao1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PessoaCertificado_Emissao1", context.localUtil.Format(AV66PessoaCertificado_Emissao1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_emissao_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Emissao_To1"}), 1, "vPESSOACERTIFICADO_EMISSAO_TO1");
               GX_FocusControl = edtavPessoacertificado_emissao_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67PessoaCertificado_Emissao_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67PessoaCertificado_Emissao_To1", context.localUtil.Format(AV67PessoaCertificado_Emissao_To1, "99/99/99"));
            }
            else
            {
               AV67PessoaCertificado_Emissao_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_emissao_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67PessoaCertificado_Emissao_To1", context.localUtil.Format(AV67PessoaCertificado_Emissao_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_validade1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Validade1"}), 1, "vPESSOACERTIFICADO_VALIDADE1");
               GX_FocusControl = edtavPessoacertificado_validade1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68PessoaCertificado_Validade1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68PessoaCertificado_Validade1", context.localUtil.Format(AV68PessoaCertificado_Validade1, "99/99/99"));
            }
            else
            {
               AV68PessoaCertificado_Validade1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_validade1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68PessoaCertificado_Validade1", context.localUtil.Format(AV68PessoaCertificado_Validade1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_validade_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Validade_To1"}), 1, "vPESSOACERTIFICADO_VALIDADE_TO1");
               GX_FocusControl = edtavPessoacertificado_validade_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69PessoaCertificado_Validade_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69PessoaCertificado_Validade_To1", context.localUtil.Format(AV69PessoaCertificado_Validade_To1, "99/99/99"));
            }
            else
            {
               AV69PessoaCertificado_Validade_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_validade_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69PessoaCertificado_Validade_To1", context.localUtil.Format(AV69PessoaCertificado_Validade_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_emissao2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Emissao2"}), 1, "vPESSOACERTIFICADO_EMISSAO2");
               GX_FocusControl = edtavPessoacertificado_emissao2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70PessoaCertificado_Emissao2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PessoaCertificado_Emissao2", context.localUtil.Format(AV70PessoaCertificado_Emissao2, "99/99/99"));
            }
            else
            {
               AV70PessoaCertificado_Emissao2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_emissao2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PessoaCertificado_Emissao2", context.localUtil.Format(AV70PessoaCertificado_Emissao2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_emissao_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Emissao_To2"}), 1, "vPESSOACERTIFICADO_EMISSAO_TO2");
               GX_FocusControl = edtavPessoacertificado_emissao_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71PessoaCertificado_Emissao_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PessoaCertificado_Emissao_To2", context.localUtil.Format(AV71PessoaCertificado_Emissao_To2, "99/99/99"));
            }
            else
            {
               AV71PessoaCertificado_Emissao_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_emissao_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PessoaCertificado_Emissao_To2", context.localUtil.Format(AV71PessoaCertificado_Emissao_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_validade2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Validade2"}), 1, "vPESSOACERTIFICADO_VALIDADE2");
               GX_FocusControl = edtavPessoacertificado_validade2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV72PessoaCertificado_Validade2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72PessoaCertificado_Validade2", context.localUtil.Format(AV72PessoaCertificado_Validade2, "99/99/99"));
            }
            else
            {
               AV72PessoaCertificado_Validade2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_validade2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72PessoaCertificado_Validade2", context.localUtil.Format(AV72PessoaCertificado_Validade2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_validade_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Validade_To2"}), 1, "vPESSOACERTIFICADO_VALIDADE_TO2");
               GX_FocusControl = edtavPessoacertificado_validade_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73PessoaCertificado_Validade_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PessoaCertificado_Validade_To2", context.localUtil.Format(AV73PessoaCertificado_Validade_To2, "99/99/99"));
            }
            else
            {
               AV73PessoaCertificado_Validade_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_validade_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PessoaCertificado_Validade_To2", context.localUtil.Format(AV73PessoaCertificado_Validade_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_emissao3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Emissao3"}), 1, "vPESSOACERTIFICADO_EMISSAO3");
               GX_FocusControl = edtavPessoacertificado_emissao3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74PessoaCertificado_Emissao3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74PessoaCertificado_Emissao3", context.localUtil.Format(AV74PessoaCertificado_Emissao3, "99/99/99"));
            }
            else
            {
               AV74PessoaCertificado_Emissao3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_emissao3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74PessoaCertificado_Emissao3", context.localUtil.Format(AV74PessoaCertificado_Emissao3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_emissao_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Emissao_To3"}), 1, "vPESSOACERTIFICADO_EMISSAO_TO3");
               GX_FocusControl = edtavPessoacertificado_emissao_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75PessoaCertificado_Emissao_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PessoaCertificado_Emissao_To3", context.localUtil.Format(AV75PessoaCertificado_Emissao_To3, "99/99/99"));
            }
            else
            {
               AV75PessoaCertificado_Emissao_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_emissao_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PessoaCertificado_Emissao_To3", context.localUtil.Format(AV75PessoaCertificado_Emissao_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_validade3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Validade3"}), 1, "vPESSOACERTIFICADO_VALIDADE3");
               GX_FocusControl = edtavPessoacertificado_validade3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76PessoaCertificado_Validade3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76PessoaCertificado_Validade3", context.localUtil.Format(AV76PessoaCertificado_Validade3, "99/99/99"));
            }
            else
            {
               AV76PessoaCertificado_Validade3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_validade3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76PessoaCertificado_Validade3", context.localUtil.Format(AV76PessoaCertificado_Validade3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPessoacertificado_validade_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Pessoa Certificado_Validade_To3"}), 1, "vPESSOACERTIFICADO_VALIDADE_TO3");
               GX_FocusControl = edtavPessoacertificado_validade_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77PessoaCertificado_Validade_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77PessoaCertificado_Validade_To3", context.localUtil.Format(AV77PessoaCertificado_Validade_To3, "99/99/99"));
            }
            else
            {
               AV77PessoaCertificado_Validade_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPessoacertificado_validade_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77PessoaCertificado_Validade_To3", context.localUtil.Format(AV77PessoaCertificado_Validade_To3, "99/99/99"));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV29ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV29ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV29ManageFiltersExecutionStep), 1, 0));
            }
            AV50TFPessoaCertificado_Nome = StringUtil.Upper( cgiGet( edtavTfpessoacertificado_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFPessoaCertificado_Nome", AV50TFPessoaCertificado_Nome);
            AV51TFPessoaCertificado_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfpessoacertificado_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFPessoaCertificado_Nome_Sel", AV51TFPessoaCertificado_Nome_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_emissao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Emissao"}), 1, "vTFPESSOACERTIFICADO_EMISSAO");
               GX_FocusControl = edtavTfpessoacertificado_emissao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFPessoaCertificado_Emissao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFPessoaCertificado_Emissao", context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"));
            }
            else
            {
               AV44TFPessoaCertificado_Emissao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_emissao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFPessoaCertificado_Emissao", context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_emissao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Emissao_To"}), 1, "vTFPESSOACERTIFICADO_EMISSAO_TO");
               GX_FocusControl = edtavTfpessoacertificado_emissao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFPessoaCertificado_Emissao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"));
            }
            else
            {
               AV45TFPessoaCertificado_Emissao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_emissao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_emissaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Emissao Aux Date"}), 1, "vDDO_PESSOACERTIFICADO_EMISSAOAUXDATE");
               GX_FocusControl = edtavDdo_pessoacertificado_emissaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46DDO_PessoaCertificado_EmissaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46DDO_PessoaCertificado_EmissaoAuxDate", context.localUtil.Format(AV46DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"));
            }
            else
            {
               AV46DDO_PessoaCertificado_EmissaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_emissaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46DDO_PessoaCertificado_EmissaoAuxDate", context.localUtil.Format(AV46DDO_PessoaCertificado_EmissaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_emissaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Emissao Aux Date To"}), 1, "vDDO_PESSOACERTIFICADO_EMISSAOAUXDATETO");
               GX_FocusControl = edtavDdo_pessoacertificado_emissaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47DDO_PessoaCertificado_EmissaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_PessoaCertificado_EmissaoAuxDateTo", context.localUtil.Format(AV47DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV47DDO_PessoaCertificado_EmissaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_emissaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_PessoaCertificado_EmissaoAuxDateTo", context.localUtil.Format(AV47DDO_PessoaCertificado_EmissaoAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_validade_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Validade"}), 1, "vTFPESSOACERTIFICADO_VALIDADE");
               GX_FocusControl = edtavTfpessoacertificado_validade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFPessoaCertificado_Validade = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFPessoaCertificado_Validade", context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"));
            }
            else
            {
               AV54TFPessoaCertificado_Validade = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_validade_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFPessoaCertificado_Validade", context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfpessoacertificado_validade_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFPessoa Certificado_Validade_To"}), 1, "vTFPESSOACERTIFICADO_VALIDADE_TO");
               GX_FocusControl = edtavTfpessoacertificado_validade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFPessoaCertificado_Validade_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPessoaCertificado_Validade_To", context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"));
            }
            else
            {
               AV55TFPessoaCertificado_Validade_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfpessoacertificado_validade_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPessoaCertificado_Validade_To", context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_validadeauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Validade Aux Date"}), 1, "vDDO_PESSOACERTIFICADO_VALIDADEAUXDATE");
               GX_FocusControl = edtavDdo_pessoacertificado_validadeauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56DDO_PessoaCertificado_ValidadeAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_PessoaCertificado_ValidadeAuxDate", context.localUtil.Format(AV56DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"));
            }
            else
            {
               AV56DDO_PessoaCertificado_ValidadeAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_validadeauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56DDO_PessoaCertificado_ValidadeAuxDate", context.localUtil.Format(AV56DDO_PessoaCertificado_ValidadeAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_pessoacertificado_validadeauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Pessoa Certificado_Validade Aux Date To"}), 1, "vDDO_PESSOACERTIFICADO_VALIDADEAUXDATETO");
               GX_FocusControl = edtavDdo_pessoacertificado_validadeauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57DDO_PessoaCertificado_ValidadeAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_PessoaCertificado_ValidadeAuxDateTo", context.localUtil.Format(AV57DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"));
            }
            else
            {
               AV57DDO_PessoaCertificado_ValidadeAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_pessoacertificado_validadeauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_PessoaCertificado_ValidadeAuxDateTo", context.localUtil.Format(AV57DDO_PessoaCertificado_ValidadeAuxDateTo, "99/99/99"));
            }
            AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace = cgiGet( edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace", AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace);
            AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = cgiGet( edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace", AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace);
            AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = cgiGet( edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace", AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_116 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_116"), ",", "."));
            AV61GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV62GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_pessoacertificado_nome_Caption = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Caption");
            Ddo_pessoacertificado_nome_Tooltip = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Tooltip");
            Ddo_pessoacertificado_nome_Cls = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Cls");
            Ddo_pessoacertificado_nome_Filteredtext_set = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Filteredtext_set");
            Ddo_pessoacertificado_nome_Selectedvalue_set = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Selectedvalue_set");
            Ddo_pessoacertificado_nome_Dropdownoptionstype = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Dropdownoptionstype");
            Ddo_pessoacertificado_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Titlecontrolidtoreplace");
            Ddo_pessoacertificado_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_NOME_Includesortasc"));
            Ddo_pessoacertificado_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_NOME_Includesortdsc"));
            Ddo_pessoacertificado_nome_Sortedstatus = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Sortedstatus");
            Ddo_pessoacertificado_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_NOME_Includefilter"));
            Ddo_pessoacertificado_nome_Filtertype = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Filtertype");
            Ddo_pessoacertificado_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_NOME_Filterisrange"));
            Ddo_pessoacertificado_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_NOME_Includedatalist"));
            Ddo_pessoacertificado_nome_Datalisttype = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Datalisttype");
            Ddo_pessoacertificado_nome_Datalistproc = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Datalistproc");
            Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PESSOACERTIFICADO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_pessoacertificado_nome_Sortasc = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Sortasc");
            Ddo_pessoacertificado_nome_Sortdsc = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Sortdsc");
            Ddo_pessoacertificado_nome_Loadingdata = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Loadingdata");
            Ddo_pessoacertificado_nome_Cleanfilter = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Cleanfilter");
            Ddo_pessoacertificado_nome_Noresultsfound = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Noresultsfound");
            Ddo_pessoacertificado_nome_Searchbuttontext = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Searchbuttontext");
            Ddo_pessoacertificado_emissao_Caption = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Caption");
            Ddo_pessoacertificado_emissao_Tooltip = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Tooltip");
            Ddo_pessoacertificado_emissao_Cls = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Cls");
            Ddo_pessoacertificado_emissao_Filteredtext_set = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_set");
            Ddo_pessoacertificado_emissao_Filteredtextto_set = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_set");
            Ddo_pessoacertificado_emissao_Dropdownoptionstype = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Dropdownoptionstype");
            Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Titlecontrolidtoreplace");
            Ddo_pessoacertificado_emissao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Includesortasc"));
            Ddo_pessoacertificado_emissao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Includesortdsc"));
            Ddo_pessoacertificado_emissao_Sortedstatus = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Sortedstatus");
            Ddo_pessoacertificado_emissao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Includefilter"));
            Ddo_pessoacertificado_emissao_Filtertype = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Filtertype");
            Ddo_pessoacertificado_emissao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Filterisrange"));
            Ddo_pessoacertificado_emissao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Includedatalist"));
            Ddo_pessoacertificado_emissao_Sortasc = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Sortasc");
            Ddo_pessoacertificado_emissao_Sortdsc = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Sortdsc");
            Ddo_pessoacertificado_emissao_Cleanfilter = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Cleanfilter");
            Ddo_pessoacertificado_emissao_Rangefilterfrom = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterfrom");
            Ddo_pessoacertificado_emissao_Rangefilterto = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Rangefilterto");
            Ddo_pessoacertificado_emissao_Searchbuttontext = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Searchbuttontext");
            Ddo_pessoacertificado_validade_Caption = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Caption");
            Ddo_pessoacertificado_validade_Tooltip = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Tooltip");
            Ddo_pessoacertificado_validade_Cls = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Cls");
            Ddo_pessoacertificado_validade_Filteredtext_set = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_set");
            Ddo_pessoacertificado_validade_Filteredtextto_set = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_set");
            Ddo_pessoacertificado_validade_Dropdownoptionstype = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Dropdownoptionstype");
            Ddo_pessoacertificado_validade_Titlecontrolidtoreplace = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Titlecontrolidtoreplace");
            Ddo_pessoacertificado_validade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Includesortasc"));
            Ddo_pessoacertificado_validade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Includesortdsc"));
            Ddo_pessoacertificado_validade_Sortedstatus = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Sortedstatus");
            Ddo_pessoacertificado_validade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Includefilter"));
            Ddo_pessoacertificado_validade_Filtertype = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Filtertype");
            Ddo_pessoacertificado_validade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Filterisrange"));
            Ddo_pessoacertificado_validade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Includedatalist"));
            Ddo_pessoacertificado_validade_Sortasc = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Sortasc");
            Ddo_pessoacertificado_validade_Sortdsc = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Sortdsc");
            Ddo_pessoacertificado_validade_Cleanfilter = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Cleanfilter");
            Ddo_pessoacertificado_validade_Rangefilterfrom = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterfrom");
            Ddo_pessoacertificado_validade_Rangefilterto = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Rangefilterto");
            Ddo_pessoacertificado_validade_Searchbuttontext = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_pessoacertificado_nome_Activeeventkey = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Activeeventkey");
            Ddo_pessoacertificado_nome_Filteredtext_get = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Filteredtext_get");
            Ddo_pessoacertificado_nome_Selectedvalue_get = cgiGet( "DDO_PESSOACERTIFICADO_NOME_Selectedvalue_get");
            Ddo_pessoacertificado_emissao_Activeeventkey = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Activeeventkey");
            Ddo_pessoacertificado_emissao_Filteredtext_get = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtext_get");
            Ddo_pessoacertificado_emissao_Filteredtextto_get = cgiGet( "DDO_PESSOACERTIFICADO_EMISSAO_Filteredtextto_get");
            Ddo_pessoacertificado_validade_Activeeventkey = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Activeeventkey");
            Ddo_pessoacertificado_validade_Filteredtext_get = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtext_get");
            Ddo_pessoacertificado_validade_Filteredtextto_get = cgiGet( "DDO_PESSOACERTIFICADO_VALIDADE_Filteredtextto_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO1"), 0) != AV66PessoaCertificado_Emissao1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO_TO1"), 0) != AV67PessoaCertificado_Emissao_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE1"), 0) != AV68PessoaCertificado_Validade1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE_TO1"), 0) != AV69PessoaCertificado_Validade_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO2"), 0) != AV70PessoaCertificado_Emissao2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO_TO2"), 0) != AV71PessoaCertificado_Emissao_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE2"), 0) != AV72PessoaCertificado_Validade2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE_TO2"), 0) != AV73PessoaCertificado_Validade_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO3"), 0) != AV74PessoaCertificado_Emissao3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_EMISSAO_TO3"), 0) != AV75PessoaCertificado_Emissao_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE3"), 0) != AV76PessoaCertificado_Validade3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPESSOACERTIFICADO_VALIDADE_TO3"), 0) != AV77PessoaCertificado_Validade_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOACERTIFICADO_NOME"), AV50TFPessoaCertificado_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPESSOACERTIFICADO_NOME_SEL"), AV51TFPessoaCertificado_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_EMISSAO"), 0) != AV44TFPessoaCertificado_Emissao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_EMISSAO_TO"), 0) != AV45TFPessoaCertificado_Emissao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_VALIDADE"), 0) != AV54TFPessoaCertificado_Validade )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFPESSOACERTIFICADO_VALIDADE_TO"), 0) != AV55TFPessoaCertificado_Validade_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25RA2 */
         E25RA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25RA2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "PESSOACERTIFICADO_EMISSAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "PESSOACERTIFICADO_EMISSAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "PESSOACERTIFICADO_EMISSAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfpessoacertificado_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoacertificado_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_nome_Visible), 5, 0)));
         edtavTfpessoacertificado_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoacertificado_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_nome_sel_Visible), 5, 0)));
         edtavTfpessoacertificado_emissao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoacertificado_emissao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_emissao_Visible), 5, 0)));
         edtavTfpessoacertificado_emissao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoacertificado_emissao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_emissao_to_Visible), 5, 0)));
         edtavTfpessoacertificado_validade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoacertificado_validade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_validade_Visible), 5, 0)));
         edtavTfpessoacertificado_validade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpessoacertificado_validade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpessoacertificado_validade_to_Visible), 5, 0)));
         Ddo_pessoacertificado_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_PessoaCertificado_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "TitleControlIdToReplace", Ddo_pessoacertificado_nome_Titlecontrolidtoreplace);
         AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace = Ddo_pessoacertificado_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace", AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace);
         edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace = subGrid_Internalname+"_PessoaCertificado_Emissao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "TitleControlIdToReplace", Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace);
         AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace", AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace);
         edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_pessoacertificado_validade_Titlecontrolidtoreplace = subGrid_Internalname+"_PessoaCertificado_Validade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "TitleControlIdToReplace", Ddo_pessoacertificado_validade_Titlecontrolidtoreplace);
         AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = Ddo_pessoacertificado_validade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace", AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace);
         edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Certificados";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Pessoa", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Emiss�o", 0);
         cmbavOrderedby.addItem("4", "Validade", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV59DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV59DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26RA2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV49PessoaCertificado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43PessoaCertificado_EmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53PessoaCertificado_ValidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV29ManageFiltersExecutionStep == 1 )
         {
            AV29ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV29ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV29ManageFiltersExecutionStep == 2 )
         {
            AV29ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV29ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPessoaCertificado_Nome_Titleformat = 2;
         edtPessoaCertificado_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Nome_Internalname, "Title", edtPessoaCertificado_Nome_Title);
         edtPessoaCertificado_Emissao_Titleformat = 2;
         edtPessoaCertificado_Emissao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Emiss�o", AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Emissao_Internalname, "Title", edtPessoaCertificado_Emissao_Title);
         edtPessoaCertificado_Validade_Titleformat = 2;
         edtPessoaCertificado_Validade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Validade", AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoaCertificado_Validade_Internalname, "Title", edtPessoaCertificado_Validade_Title);
         AV61GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61GridCurrentPage), 10, 0)));
         AV62GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridPageCount), 10, 0)));
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = AV66PessoaCertificado_Emissao1;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = AV67PessoaCertificado_Emissao_To1;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = AV68PessoaCertificado_Validade1;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = AV69PessoaCertificado_Validade_To1;
         AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = AV70PessoaCertificado_Emissao2;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = AV71PessoaCertificado_Emissao_To2;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = AV72PessoaCertificado_Validade2;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = AV73PessoaCertificado_Validade_To2;
         AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = AV74PessoaCertificado_Emissao3;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = AV75PessoaCertificado_Emissao_To3;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = AV76PessoaCertificado_Validade3;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = AV77PessoaCertificado_Validade_To3;
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = AV50TFPessoaCertificado_Nome;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = AV51TFPessoaCertificado_Nome_Sel;
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = AV44TFPessoaCertificado_Emissao;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = AV45TFPessoaCertificado_Emissao_To;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = AV54TFPessoaCertificado_Validade;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = AV55TFPessoaCertificado_Validade_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49PessoaCertificado_NomeTitleFilterData", AV49PessoaCertificado_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43PessoaCertificado_EmissaoTitleFilterData", AV43PessoaCertificado_EmissaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53PessoaCertificado_ValidadeTitleFilterData", AV53PessoaCertificado_ValidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33ManageFiltersData", AV33ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12RA2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV60PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV60PageToGo) ;
         }
      }

      protected void E13RA2( )
      {
         /* Ddo_pessoacertificado_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoacertificado_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFPessoaCertificado_Nome = Ddo_pessoacertificado_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFPessoaCertificado_Nome", AV50TFPessoaCertificado_Nome);
            AV51TFPessoaCertificado_Nome_Sel = Ddo_pessoacertificado_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFPessoaCertificado_Nome_Sel", AV51TFPessoaCertificado_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14RA2( )
      {
         /* Ddo_pessoacertificado_emissao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoacertificado_emissao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_emissao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_emissao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_emissao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_emissao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFPessoaCertificado_Emissao = context.localUtil.CToD( Ddo_pessoacertificado_emissao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFPessoaCertificado_Emissao", context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"));
            AV45TFPessoaCertificado_Emissao_To = context.localUtil.CToD( Ddo_pessoacertificado_emissao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15RA2( )
      {
         /* Ddo_pessoacertificado_validade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_pessoacertificado_validade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_validade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_validade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_pessoacertificado_validade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_pessoacertificado_validade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFPessoaCertificado_Validade = context.localUtil.CToD( Ddo_pessoacertificado_validade_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFPessoaCertificado_Validade", context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"));
            AV55TFPessoaCertificado_Validade_To = context.localUtil.CToD( Ddo_pessoacertificado_validade_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPessoaCertificado_Validade_To", context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27RA2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("pessoacertificado.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A2010PessoaCertificado_Codigo) + "," + UrlEncode("" +AV78PessoaCertificado_PesCod);
            AV63Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV63Update);
            AV104Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV63Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV63Update);
            AV104Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("pessoacertificado.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A2010PessoaCertificado_Codigo) + "," + UrlEncode("" +AV78PessoaCertificado_PesCod);
            AV64Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV64Delete);
            AV105Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV64Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV64Delete);
            AV105Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewpessoacertificado.aspx") + "?" + UrlEncode("" +A2010PessoaCertificado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV65Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV65Display);
            AV106Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV65Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV65Display);
            AV106Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 116;
         }
         sendrow_1162( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_116_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(116, GridRow);
         }
      }

      protected void E16RA2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20RA2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17RA2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21RA2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22RA2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18RA2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23RA2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19RA2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV66PessoaCertificado_Emissao1, AV67PessoaCertificado_Emissao_To1, AV68PessoaCertificado_Validade1, AV69PessoaCertificado_Validade_To1, AV19DynamicFiltersSelector2, AV70PessoaCertificado_Emissao2, AV71PessoaCertificado_Emissao_To2, AV72PessoaCertificado_Validade2, AV73PessoaCertificado_Validade_To2, AV23DynamicFiltersSelector3, AV74PessoaCertificado_Emissao3, AV75PessoaCertificado_Emissao_To3, AV76PessoaCertificado_Validade3, AV77PessoaCertificado_Validade_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV50TFPessoaCertificado_Nome, AV51TFPessoaCertificado_Nome_Sel, AV44TFPessoaCertificado_Emissao, AV45TFPessoaCertificado_Emissao_To, AV54TFPessoaCertificado_Validade, AV55TFPessoaCertificado_Validade_To, AV29ManageFiltersExecutionStep, AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace, AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace, AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace, AV107Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, AV6WWPContext, A2010PessoaCertificado_Codigo, AV78PessoaCertificado_PesCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24RA2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11RA2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S232 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWPessoaCertificadoFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV29ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV29ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWPessoaCertificadoFilters")), new Object[] {});
            AV29ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV29ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV30ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWPessoaCertificadoFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV30ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV30ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV107Pgmname+"GridState",  AV30ManageFiltersXml) ;
               AV10GridState.FromXml(AV30ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S222 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_pessoacertificado_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
         Ddo_pessoacertificado_emissao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
         Ddo_pessoacertificado_validade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_pessoacertificado_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "SortedStatus", Ddo_pessoacertificado_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_pessoacertificado_emissao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "SortedStatus", Ddo_pessoacertificado_emissao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_pessoacertificado_validade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "SortedStatus", Ddo_pessoacertificado_validade_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_emissao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible), 5, 0)));
         tblTablemergeddynamicfilterspessoacertificado_validade1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_validade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_validade1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOACERTIFICADO_EMISSAO") == 0 )
         {
            tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_emissao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOACERTIFICADO_VALIDADE") == 0 )
         {
            tblTablemergeddynamicfilterspessoacertificado_validade1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_validade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_validade1_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_emissao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible), 5, 0)));
         tblTablemergeddynamicfilterspessoacertificado_validade2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_validade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_validade2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PESSOACERTIFICADO_EMISSAO") == 0 )
         {
            tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_emissao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PESSOACERTIFICADO_VALIDADE") == 0 )
         {
            tblTablemergeddynamicfilterspessoacertificado_validade2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_validade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_validade2_Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_emissao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible), 5, 0)));
         tblTablemergeddynamicfilterspessoacertificado_validade3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_validade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_validade3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PESSOACERTIFICADO_EMISSAO") == 0 )
         {
            tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_emissao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PESSOACERTIFICADO_VALIDADE") == 0 )
         {
            tblTablemergeddynamicfilterspessoacertificado_validade3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterspessoacertificado_validade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterspessoacertificado_validade3_Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "PESSOACERTIFICADO_EMISSAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV70PessoaCertificado_Emissao2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PessoaCertificado_Emissao2", context.localUtil.Format(AV70PessoaCertificado_Emissao2, "99/99/99"));
         AV71PessoaCertificado_Emissao_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PessoaCertificado_Emissao_To2", context.localUtil.Format(AV71PessoaCertificado_Emissao_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "PESSOACERTIFICADO_EMISSAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV74PessoaCertificado_Emissao3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74PessoaCertificado_Emissao3", context.localUtil.Format(AV74PessoaCertificado_Emissao3, "99/99/99"));
         AV75PessoaCertificado_Emissao_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PessoaCertificado_Emissao_To3", context.localUtil.Format(AV75PessoaCertificado_Emissao_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV33ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV34ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV34ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV34ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV34ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV34ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV33ManageFiltersData.Add(AV34ManageFiltersDataItem, 0);
         AV34ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV34ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV34ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV34ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV34ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV33ManageFiltersData.Add(AV34ManageFiltersDataItem, 0);
         AV34ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV34ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV33ManageFiltersData.Add(AV34ManageFiltersDataItem, 0);
         AV31ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWPessoaCertificadoFilters"), "");
         AV108GXV1 = 1;
         while ( AV108GXV1 <= AV31ManageFiltersItems.Count )
         {
            AV32ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV31ManageFiltersItems.Item(AV108GXV1));
            AV34ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV34ManageFiltersDataItem.gxTpr_Title = AV32ManageFiltersItem.gxTpr_Title;
            AV34ManageFiltersDataItem.gxTpr_Eventkey = AV32ManageFiltersItem.gxTpr_Title;
            AV34ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV34ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV33ManageFiltersData.Add(AV34ManageFiltersDataItem, 0);
            if ( AV33ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV108GXV1 = (int)(AV108GXV1+1);
         }
         if ( AV33ManageFiltersData.Count > 3 )
         {
            AV34ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV34ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV33ManageFiltersData.Add(AV34ManageFiltersDataItem, 0);
            AV34ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV34ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV34ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV34ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV34ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV34ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV33ManageFiltersData.Add(AV34ManageFiltersDataItem, 0);
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV50TFPessoaCertificado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFPessoaCertificado_Nome", AV50TFPessoaCertificado_Nome);
         Ddo_pessoacertificado_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "FilteredText_set", Ddo_pessoacertificado_nome_Filteredtext_set);
         AV51TFPessoaCertificado_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFPessoaCertificado_Nome_Sel", AV51TFPessoaCertificado_Nome_Sel);
         Ddo_pessoacertificado_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "SelectedValue_set", Ddo_pessoacertificado_nome_Selectedvalue_set);
         AV44TFPessoaCertificado_Emissao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFPessoaCertificado_Emissao", context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"));
         Ddo_pessoacertificado_emissao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "FilteredText_set", Ddo_pessoacertificado_emissao_Filteredtext_set);
         AV45TFPessoaCertificado_Emissao_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"));
         Ddo_pessoacertificado_emissao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "FilteredTextTo_set", Ddo_pessoacertificado_emissao_Filteredtextto_set);
         AV54TFPessoaCertificado_Validade = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFPessoaCertificado_Validade", context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"));
         Ddo_pessoacertificado_validade_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "FilteredText_set", Ddo_pessoacertificado_validade_Filteredtext_set);
         AV55TFPessoaCertificado_Validade_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPessoaCertificado_Validade_To", context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"));
         Ddo_pessoacertificado_validade_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "FilteredTextTo_set", Ddo_pessoacertificado_validade_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "PESSOACERTIFICADO_EMISSAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV66PessoaCertificado_Emissao1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PessoaCertificado_Emissao1", context.localUtil.Format(AV66PessoaCertificado_Emissao1, "99/99/99"));
         AV67PessoaCertificado_Emissao_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67PessoaCertificado_Emissao_To1", context.localUtil.Format(AV67PessoaCertificado_Emissao_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get(AV107Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV107Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV28Session.Get(AV107Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV109GXV2 = 1;
         while ( AV109GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV109GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_NOME") == 0 )
            {
               AV50TFPessoaCertificado_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFPessoaCertificado_Nome", AV50TFPessoaCertificado_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFPessoaCertificado_Nome)) )
               {
                  Ddo_pessoacertificado_nome_Filteredtext_set = AV50TFPessoaCertificado_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "FilteredText_set", Ddo_pessoacertificado_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_NOME_SEL") == 0 )
            {
               AV51TFPessoaCertificado_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFPessoaCertificado_Nome_Sel", AV51TFPessoaCertificado_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFPessoaCertificado_Nome_Sel)) )
               {
                  Ddo_pessoacertificado_nome_Selectedvalue_set = AV51TFPessoaCertificado_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_nome_Internalname, "SelectedValue_set", Ddo_pessoacertificado_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_EMISSAO") == 0 )
            {
               AV44TFPessoaCertificado_Emissao = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFPessoaCertificado_Emissao", context.localUtil.Format(AV44TFPessoaCertificado_Emissao, "99/99/99"));
               AV45TFPessoaCertificado_Emissao_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFPessoaCertificado_Emissao_To", context.localUtil.Format(AV45TFPessoaCertificado_Emissao_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV44TFPessoaCertificado_Emissao) )
               {
                  Ddo_pessoacertificado_emissao_Filteredtext_set = context.localUtil.DToC( AV44TFPessoaCertificado_Emissao, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "FilteredText_set", Ddo_pessoacertificado_emissao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV45TFPessoaCertificado_Emissao_To) )
               {
                  Ddo_pessoacertificado_emissao_Filteredtextto_set = context.localUtil.DToC( AV45TFPessoaCertificado_Emissao_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_emissao_Internalname, "FilteredTextTo_set", Ddo_pessoacertificado_emissao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPESSOACERTIFICADO_VALIDADE") == 0 )
            {
               AV54TFPessoaCertificado_Validade = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFPessoaCertificado_Validade", context.localUtil.Format(AV54TFPessoaCertificado_Validade, "99/99/99"));
               AV55TFPessoaCertificado_Validade_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFPessoaCertificado_Validade_To", context.localUtil.Format(AV55TFPessoaCertificado_Validade_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV54TFPessoaCertificado_Validade) )
               {
                  Ddo_pessoacertificado_validade_Filteredtext_set = context.localUtil.DToC( AV54TFPessoaCertificado_Validade, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "FilteredText_set", Ddo_pessoacertificado_validade_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV55TFPessoaCertificado_Validade_To) )
               {
                  Ddo_pessoacertificado_validade_Filteredtextto_set = context.localUtil.DToC( AV55TFPessoaCertificado_Validade_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_pessoacertificado_validade_Internalname, "FilteredTextTo_set", Ddo_pessoacertificado_validade_Filteredtextto_set);
               }
            }
            AV109GXV2 = (int)(AV109GXV2+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOACERTIFICADO_EMISSAO") == 0 )
            {
               AV66PessoaCertificado_Emissao1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66PessoaCertificado_Emissao1", context.localUtil.Format(AV66PessoaCertificado_Emissao1, "99/99/99"));
               AV67PessoaCertificado_Emissao_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67PessoaCertificado_Emissao_To1", context.localUtil.Format(AV67PessoaCertificado_Emissao_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOACERTIFICADO_VALIDADE") == 0 )
            {
               AV68PessoaCertificado_Validade1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68PessoaCertificado_Validade1", context.localUtil.Format(AV68PessoaCertificado_Validade1, "99/99/99"));
               AV69PessoaCertificado_Validade_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69PessoaCertificado_Validade_To1", context.localUtil.Format(AV69PessoaCertificado_Validade_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PESSOACERTIFICADO_EMISSAO") == 0 )
               {
                  AV70PessoaCertificado_Emissao2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70PessoaCertificado_Emissao2", context.localUtil.Format(AV70PessoaCertificado_Emissao2, "99/99/99"));
                  AV71PessoaCertificado_Emissao_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PessoaCertificado_Emissao_To2", context.localUtil.Format(AV71PessoaCertificado_Emissao_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PESSOACERTIFICADO_VALIDADE") == 0 )
               {
                  AV72PessoaCertificado_Validade2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72PessoaCertificado_Validade2", context.localUtil.Format(AV72PessoaCertificado_Validade2, "99/99/99"));
                  AV73PessoaCertificado_Validade_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73PessoaCertificado_Validade_To2", context.localUtil.Format(AV73PessoaCertificado_Validade_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PESSOACERTIFICADO_EMISSAO") == 0 )
                  {
                     AV74PessoaCertificado_Emissao3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74PessoaCertificado_Emissao3", context.localUtil.Format(AV74PessoaCertificado_Emissao3, "99/99/99"));
                     AV75PessoaCertificado_Emissao_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75PessoaCertificado_Emissao_To3", context.localUtil.Format(AV75PessoaCertificado_Emissao_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PESSOACERTIFICADO_VALIDADE") == 0 )
                  {
                     AV76PessoaCertificado_Validade3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76PessoaCertificado_Validade3", context.localUtil.Format(AV76PessoaCertificado_Validade3, "99/99/99"));
                     AV77PessoaCertificado_Validade_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77PessoaCertificado_Validade_To3", context.localUtil.Format(AV77PessoaCertificado_Validade_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV28Session.Get(AV107Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFPessoaCertificado_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFPessoaCertificado_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFPessoaCertificado_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFPessoaCertificado_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV44TFPessoaCertificado_Emissao) && (DateTime.MinValue==AV45TFPessoaCertificado_Emissao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_EMISSAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV44TFPessoaCertificado_Emissao, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV45TFPessoaCertificado_Emissao_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV54TFPessoaCertificado_Validade) && (DateTime.MinValue==AV55TFPessoaCertificado_Validade_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPESSOACERTIFICADO_VALIDADE";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV54TFPessoaCertificado_Validade, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV55TFPessoaCertificado_Validade_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV107Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ! ( (DateTime.MinValue==AV66PessoaCertificado_Emissao1) && (DateTime.MinValue==AV67PessoaCertificado_Emissao_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV66PessoaCertificado_Emissao1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV67PessoaCertificado_Emissao_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ! ( (DateTime.MinValue==AV68PessoaCertificado_Validade1) && (DateTime.MinValue==AV69PessoaCertificado_Validade_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV68PessoaCertificado_Validade1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV69PessoaCertificado_Validade_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ! ( (DateTime.MinValue==AV70PessoaCertificado_Emissao2) && (DateTime.MinValue==AV71PessoaCertificado_Emissao_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV70PessoaCertificado_Emissao2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV71PessoaCertificado_Emissao_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ! ( (DateTime.MinValue==AV72PessoaCertificado_Validade2) && (DateTime.MinValue==AV73PessoaCertificado_Validade_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV72PessoaCertificado_Validade2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV73PessoaCertificado_Validade_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ! ( (DateTime.MinValue==AV74PessoaCertificado_Emissao3) && (DateTime.MinValue==AV75PessoaCertificado_Emissao_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV74PessoaCertificado_Emissao3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV75PessoaCertificado_Emissao_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ! ( (DateTime.MinValue==AV76PessoaCertificado_Validade3) && (DateTime.MinValue==AV77PessoaCertificado_Validade_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV76PessoaCertificado_Validade3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV77PessoaCertificado_Validade_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV107Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "PessoaCertificado";
         AV28Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_RA2( true) ;
         }
         else
         {
            wb_table2_8_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_110_RA2( true) ;
         }
         else
         {
            wb_table3_110_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_110_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RA2e( true) ;
         }
         else
         {
            wb_table1_2_RA2e( false) ;
         }
      }

      protected void wb_table3_110_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_113_RA2( true) ;
         }
         else
         {
            wb_table4_113_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table4_113_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_110_RA2e( true) ;
         }
         else
         {
            wb_table3_110_RA2e( false) ;
         }
      }

      protected void wb_table4_113_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"116\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Pessoa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoaCertificado_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoaCertificado_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoaCertificado_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoaCertificado_Emissao_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoaCertificado_Emissao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoaCertificado_Emissao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoaCertificado_Validade_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoaCertificado_Validade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoaCertificado_Validade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV63Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV64Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV65Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2010PessoaCertificado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2011PessoaCertificado_PesCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2013PessoaCertificado_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoaCertificado_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoaCertificado_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoaCertificado_Emissao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoaCertificado_Emissao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoaCertificado_Validade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoaCertificado_Validade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 116 )
         {
            wbEnd = 0;
            nRC_GXsfl_116 = (short)(nGXsfl_116_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_113_RA2e( true) ;
         }
         else
         {
            wb_table4_113_RA2e( false) ;
         }
      }

      protected void wb_table2_8_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_RA2( true) ;
         }
         else
         {
            wb_table5_11_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_21_RA2( true) ;
         }
         else
         {
            wb_table6_21_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_RA2e( true) ;
         }
         else
         {
            wb_table2_8_RA2e( false) ;
         }
      }

      protected void wb_table6_21_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_26_RA2( true) ;
         }
         else
         {
            wb_table7_26_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table7_26_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_RA2e( true) ;
         }
         else
         {
            wb_table6_21_RA2e( false) ;
         }
      }

      protected void wb_table7_26_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WWPessoaCertificado.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_35_RA2( true) ;
         }
         else
         {
            wb_table8_35_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table8_35_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table9_43_RA2( true) ;
         }
         else
         {
            wb_table9_43_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table9_43_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWPessoaCertificado.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_62_RA2( true) ;
         }
         else
         {
            wb_table10_62_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table10_62_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table11_70_RA2( true) ;
         }
         else
         {
            wb_table11_70_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table11_70_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "", true, "HLP_WWPessoaCertificado.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_89_RA2( true) ;
         }
         else
         {
            wb_table12_89_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table12_89_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table13_97_RA2( true) ;
         }
         else
         {
            wb_table13_97_RA2( false) ;
         }
         return  ;
      }

      protected void wb_table13_97_RA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_26_RA2e( true) ;
         }
         else
         {
            wb_table7_26_RA2e( false) ;
         }
      }

      protected void wb_table13_97_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterspessoacertificado_validade3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterspessoacertificado_validade3_Internalname, tblTablemergeddynamicfilterspessoacertificado_validade3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_validade3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_validade3_Internalname, context.localUtil.Format(AV76PessoaCertificado_Validade3, "99/99/99"), context.localUtil.Format( AV76PessoaCertificado_Validade3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_validade3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_validade3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterspessoacertificado_validade_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterspessoacertificado_validade_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_validade_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_validade_to3_Internalname, context.localUtil.Format(AV77PessoaCertificado_Validade_To3, "99/99/99"), context.localUtil.Format( AV77PessoaCertificado_Validade_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_validade_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_validade_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_97_RA2e( true) ;
         }
         else
         {
            wb_table13_97_RA2e( false) ;
         }
      }

      protected void wb_table12_89_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterspessoacertificado_emissao3_Internalname, tblTablemergeddynamicfilterspessoacertificado_emissao3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_emissao3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_emissao3_Internalname, context.localUtil.Format(AV74PessoaCertificado_Emissao3, "99/99/99"), context.localUtil.Format( AV74PessoaCertificado_Emissao3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_emissao3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_emissao3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterspessoacertificado_emissao_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterspessoacertificado_emissao_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_emissao_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_emissao_to3_Internalname, context.localUtil.Format(AV75PessoaCertificado_Emissao_To3, "99/99/99"), context.localUtil.Format( AV75PessoaCertificado_Emissao_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_emissao_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_emissao_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_89_RA2e( true) ;
         }
         else
         {
            wb_table12_89_RA2e( false) ;
         }
      }

      protected void wb_table11_70_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterspessoacertificado_validade2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterspessoacertificado_validade2_Internalname, tblTablemergeddynamicfilterspessoacertificado_validade2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_validade2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_validade2_Internalname, context.localUtil.Format(AV72PessoaCertificado_Validade2, "99/99/99"), context.localUtil.Format( AV72PessoaCertificado_Validade2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_validade2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_validade2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterspessoacertificado_validade_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterspessoacertificado_validade_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_validade_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_validade_to2_Internalname, context.localUtil.Format(AV73PessoaCertificado_Validade_To2, "99/99/99"), context.localUtil.Format( AV73PessoaCertificado_Validade_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_validade_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_validade_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_70_RA2e( true) ;
         }
         else
         {
            wb_table11_70_RA2e( false) ;
         }
      }

      protected void wb_table10_62_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterspessoacertificado_emissao2_Internalname, tblTablemergeddynamicfilterspessoacertificado_emissao2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_emissao2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_emissao2_Internalname, context.localUtil.Format(AV70PessoaCertificado_Emissao2, "99/99/99"), context.localUtil.Format( AV70PessoaCertificado_Emissao2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_emissao2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_emissao2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterspessoacertificado_emissao_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterspessoacertificado_emissao_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_emissao_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_emissao_to2_Internalname, context.localUtil.Format(AV71PessoaCertificado_Emissao_To2, "99/99/99"), context.localUtil.Format( AV71PessoaCertificado_Emissao_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_emissao_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_emissao_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_62_RA2e( true) ;
         }
         else
         {
            wb_table10_62_RA2e( false) ;
         }
      }

      protected void wb_table9_43_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterspessoacertificado_validade1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterspessoacertificado_validade1_Internalname, tblTablemergeddynamicfilterspessoacertificado_validade1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_validade1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_validade1_Internalname, context.localUtil.Format(AV68PessoaCertificado_Validade1, "99/99/99"), context.localUtil.Format( AV68PessoaCertificado_Validade1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_validade1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_validade1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterspessoacertificado_validade_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterspessoacertificado_validade_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_validade_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_validade_to1_Internalname, context.localUtil.Format(AV69PessoaCertificado_Validade_To1, "99/99/99"), context.localUtil.Format( AV69PessoaCertificado_Validade_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_validade_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_validade_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_43_RA2e( true) ;
         }
         else
         {
            wb_table9_43_RA2e( false) ;
         }
      }

      protected void wb_table8_35_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterspessoacertificado_emissao1_Internalname, tblTablemergeddynamicfilterspessoacertificado_emissao1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_emissao1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_emissao1_Internalname, context.localUtil.Format(AV66PessoaCertificado_Emissao1, "99/99/99"), context.localUtil.Format( AV66PessoaCertificado_Emissao1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_emissao1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_emissao1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterspessoacertificado_emissao_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterspessoacertificado_emissao_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_116_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPessoacertificado_emissao_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPessoacertificado_emissao_to1_Internalname, context.localUtil.Format(AV67PessoaCertificado_Emissao_To1, "99/99/99"), context.localUtil.Format( AV67PessoaCertificado_Emissao_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoacertificado_emissao_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            GxWebStd.gx_bitmap( context, edtavPessoacertificado_emissao_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_35_RA2e( true) ;
         }
         else
         {
            wb_table8_35_RA2e( false) ;
         }
      }

      protected void wb_table5_11_RA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPessoacertificadotitle_Internalname, "Certificados", "", "", lblPessoacertificadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_116_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WWPessoaCertificado.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_116_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWPessoaCertificado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_RA2e( true) ;
         }
         else
         {
            wb_table5_11_RA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARA2( ) ;
         WSRA2( ) ;
         WERA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203119121421");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwpessoacertificado.js", "?20203119121422");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1162( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_116_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_116_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_116_idx;
         edtPessoaCertificado_Codigo_Internalname = "PESSOACERTIFICADO_CODIGO_"+sGXsfl_116_idx;
         edtPessoaCertificado_PesCod_Internalname = "PESSOACERTIFICADO_PESCOD_"+sGXsfl_116_idx;
         edtPessoaCertificado_Nome_Internalname = "PESSOACERTIFICADO_NOME_"+sGXsfl_116_idx;
         edtPessoaCertificado_Emissao_Internalname = "PESSOACERTIFICADO_EMISSAO_"+sGXsfl_116_idx;
         edtPessoaCertificado_Validade_Internalname = "PESSOACERTIFICADO_VALIDADE_"+sGXsfl_116_idx;
      }

      protected void SubsflControlProps_fel_1162( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_116_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_116_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_116_fel_idx;
         edtPessoaCertificado_Codigo_Internalname = "PESSOACERTIFICADO_CODIGO_"+sGXsfl_116_fel_idx;
         edtPessoaCertificado_PesCod_Internalname = "PESSOACERTIFICADO_PESCOD_"+sGXsfl_116_fel_idx;
         edtPessoaCertificado_Nome_Internalname = "PESSOACERTIFICADO_NOME_"+sGXsfl_116_fel_idx;
         edtPessoaCertificado_Emissao_Internalname = "PESSOACERTIFICADO_EMISSAO_"+sGXsfl_116_fel_idx;
         edtPessoaCertificado_Validade_Internalname = "PESSOACERTIFICADO_VALIDADE_"+sGXsfl_116_fel_idx;
      }

      protected void sendrow_1162( )
      {
         SubsflControlProps_1162( ) ;
         WBRA0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_116_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_116_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_116_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV63Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV63Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV104Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV63Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV63Update)) ? AV104Update_GXI : context.PathToRelativeUrl( AV63Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV63Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV64Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV64Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV105Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV64Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV64Delete)) ? AV105Delete_GXI : context.PathToRelativeUrl( AV64Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV64Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV65Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV65Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV106Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV65Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV65Display)) ? AV106Display_GXI : context.PathToRelativeUrl( AV65Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV65Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2010PessoaCertificado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_PesCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2011PessoaCertificado_PesCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2011PessoaCertificado_PesCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_PesCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_Nome_Internalname,StringUtil.RTrim( A2013PessoaCertificado_Nome),StringUtil.RTrim( context.localUtil.Format( A2013PessoaCertificado_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_Emissao_Internalname,context.localUtil.Format(A2012PessoaCertificado_Emissao, "99/99/99"),context.localUtil.Format( A2012PessoaCertificado_Emissao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_Emissao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoaCertificado_Validade_Internalname,context.localUtil.Format(A2014PessoaCertificado_Validade, "99/99/99"),context.localUtil.Format( A2014PessoaCertificado_Validade, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoaCertificado_Validade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)116,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_CODIGO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A2010PessoaCertificado_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_PESCOD"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, context.localUtil.Format( (decimal)(A2011PessoaCertificado_PesCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_NOME"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, StringUtil.RTrim( context.localUtil.Format( A2013PessoaCertificado_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_EMISSAO"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, A2012PessoaCertificado_Emissao));
            GxWebStd.gx_hidden_field( context, "gxhash_PESSOACERTIFICADO_VALIDADE"+"_"+sGXsfl_116_idx, GetSecureSignedToken( sGXsfl_116_idx, A2014PessoaCertificado_Validade));
            GridContainer.AddRow(GridRow);
            nGXsfl_116_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_116_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_116_idx+1));
            sGXsfl_116_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_116_idx), 4, 0)), 4, "0");
            SubsflControlProps_1162( ) ;
         }
         /* End function sendrow_1162 */
      }

      protected void init_default_properties( )
      {
         lblPessoacertificadotitle_Internalname = "PESSOACERTIFICADOTITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavPessoacertificado_emissao1_Internalname = "vPESSOACERTIFICADO_EMISSAO1";
         lblDynamicfilterspessoacertificado_emissao_rangemiddletext1_Internalname = "DYNAMICFILTERSPESSOACERTIFICADO_EMISSAO_RANGEMIDDLETEXT1";
         edtavPessoacertificado_emissao_to1_Internalname = "vPESSOACERTIFICADO_EMISSAO_TO1";
         tblTablemergeddynamicfilterspessoacertificado_emissao1_Internalname = "TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO1";
         edtavPessoacertificado_validade1_Internalname = "vPESSOACERTIFICADO_VALIDADE1";
         lblDynamicfilterspessoacertificado_validade_rangemiddletext1_Internalname = "DYNAMICFILTERSPESSOACERTIFICADO_VALIDADE_RANGEMIDDLETEXT1";
         edtavPessoacertificado_validade_to1_Internalname = "vPESSOACERTIFICADO_VALIDADE_TO1";
         tblTablemergeddynamicfilterspessoacertificado_validade1_Internalname = "TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavPessoacertificado_emissao2_Internalname = "vPESSOACERTIFICADO_EMISSAO2";
         lblDynamicfilterspessoacertificado_emissao_rangemiddletext2_Internalname = "DYNAMICFILTERSPESSOACERTIFICADO_EMISSAO_RANGEMIDDLETEXT2";
         edtavPessoacertificado_emissao_to2_Internalname = "vPESSOACERTIFICADO_EMISSAO_TO2";
         tblTablemergeddynamicfilterspessoacertificado_emissao2_Internalname = "TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO2";
         edtavPessoacertificado_validade2_Internalname = "vPESSOACERTIFICADO_VALIDADE2";
         lblDynamicfilterspessoacertificado_validade_rangemiddletext2_Internalname = "DYNAMICFILTERSPESSOACERTIFICADO_VALIDADE_RANGEMIDDLETEXT2";
         edtavPessoacertificado_validade_to2_Internalname = "vPESSOACERTIFICADO_VALIDADE_TO2";
         tblTablemergeddynamicfilterspessoacertificado_validade2_Internalname = "TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavPessoacertificado_emissao3_Internalname = "vPESSOACERTIFICADO_EMISSAO3";
         lblDynamicfilterspessoacertificado_emissao_rangemiddletext3_Internalname = "DYNAMICFILTERSPESSOACERTIFICADO_EMISSAO_RANGEMIDDLETEXT3";
         edtavPessoacertificado_emissao_to3_Internalname = "vPESSOACERTIFICADO_EMISSAO_TO3";
         tblTablemergeddynamicfilterspessoacertificado_emissao3_Internalname = "TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO3";
         edtavPessoacertificado_validade3_Internalname = "vPESSOACERTIFICADO_VALIDADE3";
         lblDynamicfilterspessoacertificado_validade_rangemiddletext3_Internalname = "DYNAMICFILTERSPESSOACERTIFICADO_VALIDADE_RANGEMIDDLETEXT3";
         edtavPessoacertificado_validade_to3_Internalname = "vPESSOACERTIFICADO_VALIDADE_TO3";
         tblTablemergeddynamicfilterspessoacertificado_validade3_Internalname = "TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtPessoaCertificado_Codigo_Internalname = "PESSOACERTIFICADO_CODIGO";
         edtPessoaCertificado_PesCod_Internalname = "PESSOACERTIFICADO_PESCOD";
         edtPessoaCertificado_Nome_Internalname = "PESSOACERTIFICADO_NOME";
         edtPessoaCertificado_Emissao_Internalname = "PESSOACERTIFICADO_EMISSAO";
         edtPessoaCertificado_Validade_Internalname = "PESSOACERTIFICADO_VALIDADE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfpessoacertificado_nome_Internalname = "vTFPESSOACERTIFICADO_NOME";
         edtavTfpessoacertificado_nome_sel_Internalname = "vTFPESSOACERTIFICADO_NOME_SEL";
         edtavTfpessoacertificado_emissao_Internalname = "vTFPESSOACERTIFICADO_EMISSAO";
         edtavTfpessoacertificado_emissao_to_Internalname = "vTFPESSOACERTIFICADO_EMISSAO_TO";
         edtavDdo_pessoacertificado_emissaoauxdate_Internalname = "vDDO_PESSOACERTIFICADO_EMISSAOAUXDATE";
         edtavDdo_pessoacertificado_emissaoauxdateto_Internalname = "vDDO_PESSOACERTIFICADO_EMISSAOAUXDATETO";
         divDdo_pessoacertificado_emissaoauxdates_Internalname = "DDO_PESSOACERTIFICADO_EMISSAOAUXDATES";
         edtavTfpessoacertificado_validade_Internalname = "vTFPESSOACERTIFICADO_VALIDADE";
         edtavTfpessoacertificado_validade_to_Internalname = "vTFPESSOACERTIFICADO_VALIDADE_TO";
         edtavDdo_pessoacertificado_validadeauxdate_Internalname = "vDDO_PESSOACERTIFICADO_VALIDADEAUXDATE";
         edtavDdo_pessoacertificado_validadeauxdateto_Internalname = "vDDO_PESSOACERTIFICADO_VALIDADEAUXDATETO";
         divDdo_pessoacertificado_validadeauxdates_Internalname = "DDO_PESSOACERTIFICADO_VALIDADEAUXDATES";
         Ddo_pessoacertificado_nome_Internalname = "DDO_PESSOACERTIFICADO_NOME";
         edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname = "vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_pessoacertificado_emissao_Internalname = "DDO_PESSOACERTIFICADO_EMISSAO";
         edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname = "vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE";
         Ddo_pessoacertificado_validade_Internalname = "DDO_PESSOACERTIFICADO_VALIDADE";
         edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname = "vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtPessoaCertificado_Validade_Jsonclick = "";
         edtPessoaCertificado_Emissao_Jsonclick = "";
         edtPessoaCertificado_Nome_Jsonclick = "";
         edtPessoaCertificado_PesCod_Jsonclick = "";
         edtPessoaCertificado_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         edtavPessoacertificado_emissao_to1_Jsonclick = "";
         edtavPessoacertificado_emissao1_Jsonclick = "";
         edtavPessoacertificado_validade_to1_Jsonclick = "";
         edtavPessoacertificado_validade1_Jsonclick = "";
         edtavPessoacertificado_emissao_to2_Jsonclick = "";
         edtavPessoacertificado_emissao2_Jsonclick = "";
         edtavPessoacertificado_validade_to2_Jsonclick = "";
         edtavPessoacertificado_validade2_Jsonclick = "";
         edtavPessoacertificado_emissao_to3_Jsonclick = "";
         edtavPessoacertificado_emissao3_Jsonclick = "";
         edtavPessoacertificado_validade_to3_Jsonclick = "";
         edtavPessoacertificado_validade3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtPessoaCertificado_Validade_Titleformat = 0;
         edtPessoaCertificado_Emissao_Titleformat = 0;
         edtPessoaCertificado_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterspessoacertificado_validade3_Visible = 1;
         tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible = 1;
         tblTablemergeddynamicfilterspessoacertificado_validade2_Visible = 1;
         tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible = 1;
         tblTablemergeddynamicfilterspessoacertificado_validade1_Visible = 1;
         tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible = 1;
         edtPessoaCertificado_Validade_Title = "Validade";
         edtPessoaCertificado_Emissao_Title = "Emiss�o";
         edtPessoaCertificado_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_pessoacertificado_validadeauxdateto_Jsonclick = "";
         edtavDdo_pessoacertificado_validadeauxdate_Jsonclick = "";
         edtavTfpessoacertificado_validade_to_Jsonclick = "";
         edtavTfpessoacertificado_validade_to_Visible = 1;
         edtavTfpessoacertificado_validade_Jsonclick = "";
         edtavTfpessoacertificado_validade_Visible = 1;
         edtavDdo_pessoacertificado_emissaoauxdateto_Jsonclick = "";
         edtavDdo_pessoacertificado_emissaoauxdate_Jsonclick = "";
         edtavTfpessoacertificado_emissao_to_Jsonclick = "";
         edtavTfpessoacertificado_emissao_to_Visible = 1;
         edtavTfpessoacertificado_emissao_Jsonclick = "";
         edtavTfpessoacertificado_emissao_Visible = 1;
         edtavTfpessoacertificado_nome_sel_Jsonclick = "";
         edtavTfpessoacertificado_nome_sel_Visible = 1;
         edtavTfpessoacertificado_nome_Jsonclick = "";
         edtavTfpessoacertificado_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_pessoacertificado_validade_Searchbuttontext = "Pesquisar";
         Ddo_pessoacertificado_validade_Rangefilterto = "At�";
         Ddo_pessoacertificado_validade_Rangefilterfrom = "Desde";
         Ddo_pessoacertificado_validade_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoacertificado_validade_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoacertificado_validade_Sortasc = "Ordenar de A � Z";
         Ddo_pessoacertificado_validade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_pessoacertificado_validade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Filtertype = "Date";
         Ddo_pessoacertificado_validade_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_validade_Titlecontrolidtoreplace = "";
         Ddo_pessoacertificado_validade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoacertificado_validade_Cls = "ColumnSettings";
         Ddo_pessoacertificado_validade_Tooltip = "Op��es";
         Ddo_pessoacertificado_validade_Caption = "";
         Ddo_pessoacertificado_emissao_Searchbuttontext = "Pesquisar";
         Ddo_pessoacertificado_emissao_Rangefilterto = "At�";
         Ddo_pessoacertificado_emissao_Rangefilterfrom = "Desde";
         Ddo_pessoacertificado_emissao_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoacertificado_emissao_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoacertificado_emissao_Sortasc = "Ordenar de A � Z";
         Ddo_pessoacertificado_emissao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_pessoacertificado_emissao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Filtertype = "Date";
         Ddo_pessoacertificado_emissao_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace = "";
         Ddo_pessoacertificado_emissao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoacertificado_emissao_Cls = "ColumnSettings";
         Ddo_pessoacertificado_emissao_Tooltip = "Op��es";
         Ddo_pessoacertificado_emissao_Caption = "";
         Ddo_pessoacertificado_nome_Searchbuttontext = "Pesquisar";
         Ddo_pessoacertificado_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_pessoacertificado_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_pessoacertificado_nome_Loadingdata = "Carregando dados...";
         Ddo_pessoacertificado_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_pessoacertificado_nome_Sortasc = "Ordenar de A � Z";
         Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters = 0;
         Ddo_pessoacertificado_nome_Datalistproc = "GetWWPessoaCertificadoFilterData";
         Ddo_pessoacertificado_nome_Datalisttype = "Dynamic";
         Ddo_pessoacertificado_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_pessoacertificado_nome_Filtertype = "Character";
         Ddo_pessoacertificado_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_pessoacertificado_nome_Titlecontrolidtoreplace = "";
         Ddo_pessoacertificado_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_pessoacertificado_nome_Cls = "ColumnSettings";
         Ddo_pessoacertificado_nome_Tooltip = "Op��es";
         Ddo_pessoacertificado_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Certificados";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV49PessoaCertificado_NomeTitleFilterData',fld:'vPESSOACERTIFICADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV43PessoaCertificado_EmissaoTitleFilterData',fld:'vPESSOACERTIFICADO_EMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV53PessoaCertificado_ValidadeTitleFilterData',fld:'vPESSOACERTIFICADO_VALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtPessoaCertificado_Nome_Titleformat',ctrl:'PESSOACERTIFICADO_NOME',prop:'Titleformat'},{av:'edtPessoaCertificado_Nome_Title',ctrl:'PESSOACERTIFICADO_NOME',prop:'Title'},{av:'edtPessoaCertificado_Emissao_Titleformat',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Titleformat'},{av:'edtPessoaCertificado_Emissao_Title',ctrl:'PESSOACERTIFICADO_EMISSAO',prop:'Title'},{av:'edtPessoaCertificado_Validade_Titleformat',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Titleformat'},{av:'edtPessoaCertificado_Validade_Title',ctrl:'PESSOACERTIFICADO_VALIDADE',prop:'Title'},{av:'AV61GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV62GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV33ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PESSOACERTIFICADO_NOME.ONOPTIONCLICKED","{handler:'E13RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoacertificado_nome_Activeeventkey',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'ActiveEventKey'},{av:'Ddo_pessoacertificado_nome_Filteredtext_get',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'FilteredText_get'},{av:'Ddo_pessoacertificado_nome_Selectedvalue_get',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoacertificado_nome_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SortedStatus'},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pessoacertificado_emissao_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_validade_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOACERTIFICADO_EMISSAO.ONOPTIONCLICKED","{handler:'E14RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoacertificado_emissao_Activeeventkey',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'ActiveEventKey'},{av:'Ddo_pessoacertificado_emissao_Filteredtext_get',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'FilteredText_get'},{av:'Ddo_pessoacertificado_emissao_Filteredtextto_get',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoacertificado_emissao_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'SortedStatus'},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'Ddo_pessoacertificado_nome_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_validade_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PESSOACERTIFICADO_VALIDADE.ONOPTIONCLICKED","{handler:'E15RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_pessoacertificado_validade_Activeeventkey',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'ActiveEventKey'},{av:'Ddo_pessoacertificado_validade_Filteredtext_get',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'FilteredText_get'},{av:'Ddo_pessoacertificado_validade_Filteredtextto_get',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_pessoacertificado_validade_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'SortedStatus'},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'Ddo_pessoacertificado_nome_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_emissao_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27RA2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV63Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV64Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV65Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20RA2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21RA2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22RA2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23RA2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24RA2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE3',prop:'Visible'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11RA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_EMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace',fld:'vDDO_PESSOACERTIFICADO_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV107Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A2010PessoaCertificado_Codigo',fld:'PESSOACERTIFICADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV78PessoaCertificado_PesCod',fld:'vPESSOACERTIFICADO_PESCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV29ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50TFPessoaCertificado_Nome',fld:'vTFPESSOACERTIFICADO_NOME',pic:'@!',nv:''},{av:'Ddo_pessoacertificado_nome_Filteredtext_set',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'FilteredText_set'},{av:'AV51TFPessoaCertificado_Nome_Sel',fld:'vTFPESSOACERTIFICADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_pessoacertificado_nome_Selectedvalue_set',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SelectedValue_set'},{av:'AV44TFPessoaCertificado_Emissao',fld:'vTFPESSOACERTIFICADO_EMISSAO',pic:'',nv:''},{av:'Ddo_pessoacertificado_emissao_Filteredtext_set',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'FilteredText_set'},{av:'AV45TFPessoaCertificado_Emissao_To',fld:'vTFPESSOACERTIFICADO_EMISSAO_TO',pic:'',nv:''},{av:'Ddo_pessoacertificado_emissao_Filteredtextto_set',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'FilteredTextTo_set'},{av:'AV54TFPessoaCertificado_Validade',fld:'vTFPESSOACERTIFICADO_VALIDADE',pic:'',nv:''},{av:'Ddo_pessoacertificado_validade_Filteredtext_set',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'FilteredText_set'},{av:'AV55TFPessoaCertificado_Validade_To',fld:'vTFPESSOACERTIFICADO_VALIDADE_TO',pic:'',nv:''},{av:'Ddo_pessoacertificado_validade_Filteredtextto_set',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV66PessoaCertificado_Emissao1',fld:'vPESSOACERTIFICADO_EMISSAO1',pic:'',nv:''},{av:'AV67PessoaCertificado_Emissao_To1',fld:'vPESSOACERTIFICADO_EMISSAO_TO1',pic:'',nv:''},{av:'Ddo_pessoacertificado_nome_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_NOME',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_emissao_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_EMISSAO',prop:'SortedStatus'},{av:'Ddo_pessoacertificado_validade_Sortedstatus',ctrl:'DDO_PESSOACERTIFICADO_VALIDADE',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV68PessoaCertificado_Validade1',fld:'vPESSOACERTIFICADO_VALIDADE1',pic:'',nv:''},{av:'AV69PessoaCertificado_Validade_To1',fld:'vPESSOACERTIFICADO_VALIDADE_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV70PessoaCertificado_Emissao2',fld:'vPESSOACERTIFICADO_EMISSAO2',pic:'',nv:''},{av:'AV71PessoaCertificado_Emissao_To2',fld:'vPESSOACERTIFICADO_EMISSAO_TO2',pic:'',nv:''},{av:'AV72PessoaCertificado_Validade2',fld:'vPESSOACERTIFICADO_VALIDADE2',pic:'',nv:''},{av:'AV73PessoaCertificado_Validade_To2',fld:'vPESSOACERTIFICADO_VALIDADE_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV74PessoaCertificado_Emissao3',fld:'vPESSOACERTIFICADO_EMISSAO3',pic:'',nv:''},{av:'AV75PessoaCertificado_Emissao_To3',fld:'vPESSOACERTIFICADO_EMISSAO_TO3',pic:'',nv:''},{av:'AV76PessoaCertificado_Validade3',fld:'vPESSOACERTIFICADO_VALIDADE3',pic:'',nv:''},{av:'AV77PessoaCertificado_Validade_To3',fld:'vPESSOACERTIFICADO_VALIDADE_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE1',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE2',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_EMISSAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterspessoacertificado_validade3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSPESSOACERTIFICADO_VALIDADE3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_pessoacertificado_nome_Activeeventkey = "";
         Ddo_pessoacertificado_nome_Filteredtext_get = "";
         Ddo_pessoacertificado_nome_Selectedvalue_get = "";
         Ddo_pessoacertificado_emissao_Activeeventkey = "";
         Ddo_pessoacertificado_emissao_Filteredtext_get = "";
         Ddo_pessoacertificado_emissao_Filteredtextto_get = "";
         Ddo_pessoacertificado_validade_Activeeventkey = "";
         Ddo_pessoacertificado_validade_Filteredtext_get = "";
         Ddo_pessoacertificado_validade_Filteredtextto_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV66PessoaCertificado_Emissao1 = DateTime.MinValue;
         AV67PessoaCertificado_Emissao_To1 = DateTime.MinValue;
         AV68PessoaCertificado_Validade1 = DateTime.MinValue;
         AV69PessoaCertificado_Validade_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV70PessoaCertificado_Emissao2 = DateTime.MinValue;
         AV71PessoaCertificado_Emissao_To2 = DateTime.MinValue;
         AV72PessoaCertificado_Validade2 = DateTime.MinValue;
         AV73PessoaCertificado_Validade_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV74PessoaCertificado_Emissao3 = DateTime.MinValue;
         AV75PessoaCertificado_Emissao_To3 = DateTime.MinValue;
         AV76PessoaCertificado_Validade3 = DateTime.MinValue;
         AV77PessoaCertificado_Validade_To3 = DateTime.MinValue;
         AV50TFPessoaCertificado_Nome = "";
         AV51TFPessoaCertificado_Nome_Sel = "";
         AV44TFPessoaCertificado_Emissao = DateTime.MinValue;
         AV45TFPessoaCertificado_Emissao_To = DateTime.MinValue;
         AV54TFPessoaCertificado_Validade = DateTime.MinValue;
         AV55TFPessoaCertificado_Validade_To = DateTime.MinValue;
         AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace = "";
         AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace = "";
         AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace = "";
         AV107Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV33ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV49PessoaCertificado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43PessoaCertificado_EmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53PessoaCertificado_ValidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_pessoacertificado_nome_Filteredtext_set = "";
         Ddo_pessoacertificado_nome_Selectedvalue_set = "";
         Ddo_pessoacertificado_nome_Sortedstatus = "";
         Ddo_pessoacertificado_emissao_Filteredtext_set = "";
         Ddo_pessoacertificado_emissao_Filteredtextto_set = "";
         Ddo_pessoacertificado_emissao_Sortedstatus = "";
         Ddo_pessoacertificado_validade_Filteredtext_set = "";
         Ddo_pessoacertificado_validade_Filteredtextto_set = "";
         Ddo_pessoacertificado_validade_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV46DDO_PessoaCertificado_EmissaoAuxDate = DateTime.MinValue;
         AV47DDO_PessoaCertificado_EmissaoAuxDateTo = DateTime.MinValue;
         AV56DDO_PessoaCertificado_ValidadeAuxDate = DateTime.MinValue;
         AV57DDO_PessoaCertificado_ValidadeAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV63Update = "";
         AV104Update_GXI = "";
         AV64Delete = "";
         AV105Delete_GXI = "";
         AV65Display = "";
         AV106Display_GXI = "";
         A2013PessoaCertificado_Nome = "";
         A2012PessoaCertificado_Emissao = DateTime.MinValue;
         A2014PessoaCertificado_Validade = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = "";
         AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 = "";
         AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 = DateTime.MinValue;
         AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 = DateTime.MinValue;
         AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 = DateTime.MinValue;
         AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 = DateTime.MinValue;
         AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 = "";
         AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 = DateTime.MinValue;
         AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 = DateTime.MinValue;
         AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 = DateTime.MinValue;
         AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 = DateTime.MinValue;
         AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 = "";
         AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 = DateTime.MinValue;
         AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 = DateTime.MinValue;
         AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 = DateTime.MinValue;
         AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 = DateTime.MinValue;
         AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel = "";
         AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome = "";
         AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao = DateTime.MinValue;
         AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to = DateTime.MinValue;
         AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade = DateTime.MinValue;
         AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to = DateTime.MinValue;
         H00RA2_A2014PessoaCertificado_Validade = new DateTime[] {DateTime.MinValue} ;
         H00RA2_n2014PessoaCertificado_Validade = new bool[] {false} ;
         H00RA2_A2012PessoaCertificado_Emissao = new DateTime[] {DateTime.MinValue} ;
         H00RA2_A2013PessoaCertificado_Nome = new String[] {""} ;
         H00RA2_A2011PessoaCertificado_PesCod = new int[1] ;
         H00RA2_A2010PessoaCertificado_Codigo = new int[1] ;
         H00RA3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV30ManageFiltersXml = "";
         GXt_char2 = "";
         AV34ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV31ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV32ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV28Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterspessoacertificado_validade_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterspessoacertificado_emissao_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterspessoacertificado_validade_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterspessoacertificado_emissao_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterspessoacertificado_validade_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterspessoacertificado_emissao_rangemiddletext1_Jsonclick = "";
         lblPessoacertificadotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwpessoacertificado__default(),
            new Object[][] {
                new Object[] {
               H00RA2_A2014PessoaCertificado_Validade, H00RA2_n2014PessoaCertificado_Validade, H00RA2_A2012PessoaCertificado_Emissao, H00RA2_A2013PessoaCertificado_Nome, H00RA2_A2011PessoaCertificado_PesCod, H00RA2_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               H00RA3_AGRID_nRecordCount
               }
            }
         );
         AV107Pgmname = "WWPessoaCertificado";
         /* GeneXus formulas. */
         AV107Pgmname = "WWPessoaCertificado";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_116 ;
      private short nGXsfl_116_idx=1 ;
      private short AV13OrderedBy ;
      private short AV29ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_116_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtPessoaCertificado_Nome_Titleformat ;
      private short edtPessoaCertificado_Emissao_Titleformat ;
      private short edtPessoaCertificado_Validade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A2010PessoaCertificado_Codigo ;
      private int AV78PessoaCertificado_PesCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_pessoacertificado_nome_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfpessoacertificado_nome_Visible ;
      private int edtavTfpessoacertificado_nome_sel_Visible ;
      private int edtavTfpessoacertificado_emissao_Visible ;
      private int edtavTfpessoacertificado_emissao_to_Visible ;
      private int edtavTfpessoacertificado_validade_Visible ;
      private int edtavTfpessoacertificado_validade_to_Visible ;
      private int edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Visible ;
      private int A2011PessoaCertificado_PesCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV60PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterspessoacertificado_emissao1_Visible ;
      private int tblTablemergeddynamicfilterspessoacertificado_validade1_Visible ;
      private int tblTablemergeddynamicfilterspessoacertificado_emissao2_Visible ;
      private int tblTablemergeddynamicfilterspessoacertificado_validade2_Visible ;
      private int tblTablemergeddynamicfilterspessoacertificado_emissao3_Visible ;
      private int tblTablemergeddynamicfilterspessoacertificado_validade3_Visible ;
      private int AV108GXV1 ;
      private int AV109GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV61GridCurrentPage ;
      private long AV62GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_pessoacertificado_nome_Activeeventkey ;
      private String Ddo_pessoacertificado_nome_Filteredtext_get ;
      private String Ddo_pessoacertificado_nome_Selectedvalue_get ;
      private String Ddo_pessoacertificado_emissao_Activeeventkey ;
      private String Ddo_pessoacertificado_emissao_Filteredtext_get ;
      private String Ddo_pessoacertificado_emissao_Filteredtextto_get ;
      private String Ddo_pessoacertificado_validade_Activeeventkey ;
      private String Ddo_pessoacertificado_validade_Filteredtext_get ;
      private String Ddo_pessoacertificado_validade_Filteredtextto_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_116_idx="0001" ;
      private String AV50TFPessoaCertificado_Nome ;
      private String AV51TFPessoaCertificado_Nome_Sel ;
      private String AV107Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_pessoacertificado_nome_Caption ;
      private String Ddo_pessoacertificado_nome_Tooltip ;
      private String Ddo_pessoacertificado_nome_Cls ;
      private String Ddo_pessoacertificado_nome_Filteredtext_set ;
      private String Ddo_pessoacertificado_nome_Selectedvalue_set ;
      private String Ddo_pessoacertificado_nome_Dropdownoptionstype ;
      private String Ddo_pessoacertificado_nome_Titlecontrolidtoreplace ;
      private String Ddo_pessoacertificado_nome_Sortedstatus ;
      private String Ddo_pessoacertificado_nome_Filtertype ;
      private String Ddo_pessoacertificado_nome_Datalisttype ;
      private String Ddo_pessoacertificado_nome_Datalistproc ;
      private String Ddo_pessoacertificado_nome_Sortasc ;
      private String Ddo_pessoacertificado_nome_Sortdsc ;
      private String Ddo_pessoacertificado_nome_Loadingdata ;
      private String Ddo_pessoacertificado_nome_Cleanfilter ;
      private String Ddo_pessoacertificado_nome_Noresultsfound ;
      private String Ddo_pessoacertificado_nome_Searchbuttontext ;
      private String Ddo_pessoacertificado_emissao_Caption ;
      private String Ddo_pessoacertificado_emissao_Tooltip ;
      private String Ddo_pessoacertificado_emissao_Cls ;
      private String Ddo_pessoacertificado_emissao_Filteredtext_set ;
      private String Ddo_pessoacertificado_emissao_Filteredtextto_set ;
      private String Ddo_pessoacertificado_emissao_Dropdownoptionstype ;
      private String Ddo_pessoacertificado_emissao_Titlecontrolidtoreplace ;
      private String Ddo_pessoacertificado_emissao_Sortedstatus ;
      private String Ddo_pessoacertificado_emissao_Filtertype ;
      private String Ddo_pessoacertificado_emissao_Sortasc ;
      private String Ddo_pessoacertificado_emissao_Sortdsc ;
      private String Ddo_pessoacertificado_emissao_Cleanfilter ;
      private String Ddo_pessoacertificado_emissao_Rangefilterfrom ;
      private String Ddo_pessoacertificado_emissao_Rangefilterto ;
      private String Ddo_pessoacertificado_emissao_Searchbuttontext ;
      private String Ddo_pessoacertificado_validade_Caption ;
      private String Ddo_pessoacertificado_validade_Tooltip ;
      private String Ddo_pessoacertificado_validade_Cls ;
      private String Ddo_pessoacertificado_validade_Filteredtext_set ;
      private String Ddo_pessoacertificado_validade_Filteredtextto_set ;
      private String Ddo_pessoacertificado_validade_Dropdownoptionstype ;
      private String Ddo_pessoacertificado_validade_Titlecontrolidtoreplace ;
      private String Ddo_pessoacertificado_validade_Sortedstatus ;
      private String Ddo_pessoacertificado_validade_Filtertype ;
      private String Ddo_pessoacertificado_validade_Sortasc ;
      private String Ddo_pessoacertificado_validade_Sortdsc ;
      private String Ddo_pessoacertificado_validade_Cleanfilter ;
      private String Ddo_pessoacertificado_validade_Rangefilterfrom ;
      private String Ddo_pessoacertificado_validade_Rangefilterto ;
      private String Ddo_pessoacertificado_validade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfpessoacertificado_nome_Internalname ;
      private String edtavTfpessoacertificado_nome_Jsonclick ;
      private String edtavTfpessoacertificado_nome_sel_Internalname ;
      private String edtavTfpessoacertificado_nome_sel_Jsonclick ;
      private String edtavTfpessoacertificado_emissao_Internalname ;
      private String edtavTfpessoacertificado_emissao_Jsonclick ;
      private String edtavTfpessoacertificado_emissao_to_Internalname ;
      private String edtavTfpessoacertificado_emissao_to_Jsonclick ;
      private String divDdo_pessoacertificado_emissaoauxdates_Internalname ;
      private String edtavDdo_pessoacertificado_emissaoauxdate_Internalname ;
      private String edtavDdo_pessoacertificado_emissaoauxdate_Jsonclick ;
      private String edtavDdo_pessoacertificado_emissaoauxdateto_Internalname ;
      private String edtavDdo_pessoacertificado_emissaoauxdateto_Jsonclick ;
      private String edtavTfpessoacertificado_validade_Internalname ;
      private String edtavTfpessoacertificado_validade_Jsonclick ;
      private String edtavTfpessoacertificado_validade_to_Internalname ;
      private String edtavTfpessoacertificado_validade_to_Jsonclick ;
      private String divDdo_pessoacertificado_validadeauxdates_Internalname ;
      private String edtavDdo_pessoacertificado_validadeauxdate_Internalname ;
      private String edtavDdo_pessoacertificado_validadeauxdate_Jsonclick ;
      private String edtavDdo_pessoacertificado_validadeauxdateto_Internalname ;
      private String edtavDdo_pessoacertificado_validadeauxdateto_Jsonclick ;
      private String edtavDdo_pessoacertificado_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoacertificado_emissaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_pessoacertificado_validadetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtPessoaCertificado_Codigo_Internalname ;
      private String edtPessoaCertificado_PesCod_Internalname ;
      private String A2013PessoaCertificado_Nome ;
      private String edtPessoaCertificado_Nome_Internalname ;
      private String edtPessoaCertificado_Emissao_Internalname ;
      private String edtPessoaCertificado_Validade_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ;
      private String AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ;
      private String AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavPessoacertificado_emissao1_Internalname ;
      private String edtavPessoacertificado_emissao_to1_Internalname ;
      private String edtavPessoacertificado_validade1_Internalname ;
      private String edtavPessoacertificado_validade_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavPessoacertificado_emissao2_Internalname ;
      private String edtavPessoacertificado_emissao_to2_Internalname ;
      private String edtavPessoacertificado_validade2_Internalname ;
      private String edtavPessoacertificado_validade_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavPessoacertificado_emissao3_Internalname ;
      private String edtavPessoacertificado_emissao_to3_Internalname ;
      private String edtavPessoacertificado_validade3_Internalname ;
      private String edtavPessoacertificado_validade_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_pessoacertificado_nome_Internalname ;
      private String Ddo_pessoacertificado_emissao_Internalname ;
      private String Ddo_pessoacertificado_validade_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtPessoaCertificado_Nome_Title ;
      private String edtPessoaCertificado_Emissao_Title ;
      private String edtPessoaCertificado_Validade_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String GXt_char2 ;
      private String tblTablemergeddynamicfilterspessoacertificado_emissao1_Internalname ;
      private String tblTablemergeddynamicfilterspessoacertificado_validade1_Internalname ;
      private String tblTablemergeddynamicfilterspessoacertificado_emissao2_Internalname ;
      private String tblTablemergeddynamicfilterspessoacertificado_validade2_Internalname ;
      private String tblTablemergeddynamicfilterspessoacertificado_emissao3_Internalname ;
      private String tblTablemergeddynamicfilterspessoacertificado_validade3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavPessoacertificado_validade3_Jsonclick ;
      private String lblDynamicfilterspessoacertificado_validade_rangemiddletext3_Internalname ;
      private String lblDynamicfilterspessoacertificado_validade_rangemiddletext3_Jsonclick ;
      private String edtavPessoacertificado_validade_to3_Jsonclick ;
      private String edtavPessoacertificado_emissao3_Jsonclick ;
      private String lblDynamicfilterspessoacertificado_emissao_rangemiddletext3_Internalname ;
      private String lblDynamicfilterspessoacertificado_emissao_rangemiddletext3_Jsonclick ;
      private String edtavPessoacertificado_emissao_to3_Jsonclick ;
      private String edtavPessoacertificado_validade2_Jsonclick ;
      private String lblDynamicfilterspessoacertificado_validade_rangemiddletext2_Internalname ;
      private String lblDynamicfilterspessoacertificado_validade_rangemiddletext2_Jsonclick ;
      private String edtavPessoacertificado_validade_to2_Jsonclick ;
      private String edtavPessoacertificado_emissao2_Jsonclick ;
      private String lblDynamicfilterspessoacertificado_emissao_rangemiddletext2_Internalname ;
      private String lblDynamicfilterspessoacertificado_emissao_rangemiddletext2_Jsonclick ;
      private String edtavPessoacertificado_emissao_to2_Jsonclick ;
      private String edtavPessoacertificado_validade1_Jsonclick ;
      private String lblDynamicfilterspessoacertificado_validade_rangemiddletext1_Internalname ;
      private String lblDynamicfilterspessoacertificado_validade_rangemiddletext1_Jsonclick ;
      private String edtavPessoacertificado_validade_to1_Jsonclick ;
      private String edtavPessoacertificado_emissao1_Jsonclick ;
      private String lblDynamicfilterspessoacertificado_emissao_rangemiddletext1_Internalname ;
      private String lblDynamicfilterspessoacertificado_emissao_rangemiddletext1_Jsonclick ;
      private String edtavPessoacertificado_emissao_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblPessoacertificadotitle_Internalname ;
      private String lblPessoacertificadotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_116_fel_idx="0001" ;
      private String ROClassString ;
      private String edtPessoaCertificado_Codigo_Jsonclick ;
      private String edtPessoaCertificado_PesCod_Jsonclick ;
      private String edtPessoaCertificado_Nome_Jsonclick ;
      private String edtPessoaCertificado_Emissao_Jsonclick ;
      private String edtPessoaCertificado_Validade_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV66PessoaCertificado_Emissao1 ;
      private DateTime AV67PessoaCertificado_Emissao_To1 ;
      private DateTime AV68PessoaCertificado_Validade1 ;
      private DateTime AV69PessoaCertificado_Validade_To1 ;
      private DateTime AV70PessoaCertificado_Emissao2 ;
      private DateTime AV71PessoaCertificado_Emissao_To2 ;
      private DateTime AV72PessoaCertificado_Validade2 ;
      private DateTime AV73PessoaCertificado_Validade_To2 ;
      private DateTime AV74PessoaCertificado_Emissao3 ;
      private DateTime AV75PessoaCertificado_Emissao_To3 ;
      private DateTime AV76PessoaCertificado_Validade3 ;
      private DateTime AV77PessoaCertificado_Validade_To3 ;
      private DateTime AV44TFPessoaCertificado_Emissao ;
      private DateTime AV45TFPessoaCertificado_Emissao_To ;
      private DateTime AV54TFPessoaCertificado_Validade ;
      private DateTime AV55TFPessoaCertificado_Validade_To ;
      private DateTime AV46DDO_PessoaCertificado_EmissaoAuxDate ;
      private DateTime AV47DDO_PessoaCertificado_EmissaoAuxDateTo ;
      private DateTime AV56DDO_PessoaCertificado_ValidadeAuxDate ;
      private DateTime AV57DDO_PessoaCertificado_ValidadeAuxDateTo ;
      private DateTime A2012PessoaCertificado_Emissao ;
      private DateTime A2014PessoaCertificado_Validade ;
      private DateTime AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ;
      private DateTime AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ;
      private DateTime AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ;
      private DateTime AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ;
      private DateTime AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ;
      private DateTime AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ;
      private DateTime AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ;
      private DateTime AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ;
      private DateTime AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ;
      private DateTime AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ;
      private DateTime AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ;
      private DateTime AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ;
      private DateTime AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ;
      private DateTime AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ;
      private DateTime AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ;
      private DateTime AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_pessoacertificado_nome_Includesortasc ;
      private bool Ddo_pessoacertificado_nome_Includesortdsc ;
      private bool Ddo_pessoacertificado_nome_Includefilter ;
      private bool Ddo_pessoacertificado_nome_Filterisrange ;
      private bool Ddo_pessoacertificado_nome_Includedatalist ;
      private bool Ddo_pessoacertificado_emissao_Includesortasc ;
      private bool Ddo_pessoacertificado_emissao_Includesortdsc ;
      private bool Ddo_pessoacertificado_emissao_Includefilter ;
      private bool Ddo_pessoacertificado_emissao_Filterisrange ;
      private bool Ddo_pessoacertificado_emissao_Includedatalist ;
      private bool Ddo_pessoacertificado_validade_Includesortasc ;
      private bool Ddo_pessoacertificado_validade_Includesortdsc ;
      private bool Ddo_pessoacertificado_validade_Includefilter ;
      private bool Ddo_pessoacertificado_validade_Filterisrange ;
      private bool Ddo_pessoacertificado_validade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2014PessoaCertificado_Validade ;
      private bool AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ;
      private bool AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV63Update_IsBlob ;
      private bool AV64Delete_IsBlob ;
      private bool AV65Display_IsBlob ;
      private String AV30ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV52ddo_PessoaCertificado_NomeTitleControlIdToReplace ;
      private String AV48ddo_PessoaCertificado_EmissaoTitleControlIdToReplace ;
      private String AV58ddo_PessoaCertificado_ValidadeTitleControlIdToReplace ;
      private String AV104Update_GXI ;
      private String AV105Delete_GXI ;
      private String AV106Display_GXI ;
      private String AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ;
      private String AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ;
      private String AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ;
      private String AV63Update ;
      private String AV64Delete ;
      private String AV65Display ;
      private IGxSession AV28Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private DateTime[] H00RA2_A2014PessoaCertificado_Validade ;
      private bool[] H00RA2_n2014PessoaCertificado_Validade ;
      private DateTime[] H00RA2_A2012PessoaCertificado_Emissao ;
      private String[] H00RA2_A2013PessoaCertificado_Nome ;
      private int[] H00RA2_A2011PessoaCertificado_PesCod ;
      private int[] H00RA2_A2010PessoaCertificado_Codigo ;
      private long[] H00RA3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV31ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49PessoaCertificado_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43PessoaCertificado_EmissaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53PessoaCertificado_ValidadeTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV32ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV34ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV59DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwpessoacertificado__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RA2( IGxContext context ,
                                             String AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ,
                                             DateTime AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ,
                                             DateTime AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ,
                                             DateTime AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ,
                                             bool AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ,
                                             String AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ,
                                             DateTime AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ,
                                             DateTime AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ,
                                             DateTime AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ,
                                             bool AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ,
                                             String AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ,
                                             DateTime AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ,
                                             DateTime AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ,
                                             DateTime AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ,
                                             DateTime AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ,
                                             String AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ,
                                             String AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ,
                                             DateTime AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ,
                                             DateTime AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ,
                                             DateTime AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ,
                                             DateTime AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ,
                                             DateTime A2012PessoaCertificado_Emissao ,
                                             DateTime A2014PessoaCertificado_Validade ,
                                             String A2013PessoaCertificado_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [23] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [PessoaCertificado_Validade], [PessoaCertificado_Emissao], [PessoaCertificado_Nome], [PessoaCertificado_PesCod], [PessoaCertificado_Codigo]";
         sFromString = " FROM [PessoaCertificado] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Nome] like @lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Nome] like @lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Nome] = @AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Nome] = @AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_PesCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Emissao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Emissao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Validade]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Validade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [PessoaCertificado_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00RA3( IGxContext context ,
                                             String AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1 ,
                                             DateTime AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1 ,
                                             DateTime AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1 ,
                                             DateTime AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1 ,
                                             bool AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 ,
                                             String AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2 ,
                                             DateTime AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2 ,
                                             DateTime AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2 ,
                                             DateTime AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2 ,
                                             bool AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 ,
                                             String AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3 ,
                                             DateTime AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3 ,
                                             DateTime AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3 ,
                                             DateTime AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3 ,
                                             DateTime AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3 ,
                                             String AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel ,
                                             String AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome ,
                                             DateTime AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao ,
                                             DateTime AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to ,
                                             DateTime AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade ,
                                             DateTime AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to ,
                                             DateTime A2012PessoaCertificado_Emissao ,
                                             DateTime A2014PessoaCertificado_Validade ,
                                             String A2013PessoaCertificado_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [18] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [PessoaCertificado] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWPessoaCertificadoDS_1_Dynamicfiltersselector1, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV86WWPessoaCertificadoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWPessoaCertificadoDS_7_Dynamicfiltersselector2, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_EMISSAO") == 0 ) && ( ! (DateTime.MinValue==AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV92WWPessoaCertificadoDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV93WWPessoaCertificadoDS_13_Dynamicfiltersselector3, "PESSOACERTIFICADO_VALIDADE") == 0 ) && ( ! (DateTime.MinValue==AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Nome] like @lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Nome] like @lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Nome] = @AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Nome] = @AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] >= @AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] >= @AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Emissao] <= @AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Emissao] <= @AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] >= @AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] >= @AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PessoaCertificado_Validade] <= @AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([PessoaCertificado_Validade] <= @AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00RA2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 1 :
                     return conditional_H00RA3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RA2 ;
          prmH00RA2 = new Object[] {
          new Object[] {"@AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00RA3 ;
          prmH00RA3 = new Object[] {
          new Object[] {"@AV82WWPessoaCertificadoDS_2_Pessoacertificado_emissao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV83WWPessoaCertificadoDS_3_Pessoacertificado_emissao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84WWPessoaCertificadoDS_4_Pessoacertificado_validade1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWPessoaCertificadoDS_5_Pessoacertificado_validade_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWPessoaCertificadoDS_8_Pessoacertificado_emissao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWPessoaCertificadoDS_9_Pessoacertificado_emissao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV90WWPessoaCertificadoDS_10_Pessoacertificado_validade2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV91WWPessoaCertificadoDS_11_Pessoacertificado_validade_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WWPessoaCertificadoDS_14_Pessoacertificado_emissao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV95WWPessoaCertificadoDS_15_Pessoacertificado_emissao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWPessoaCertificadoDS_16_Pessoacertificado_validade3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWPessoaCertificadoDS_17_Pessoacertificado_validade_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV98WWPessoaCertificadoDS_18_Tfpessoacertificado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV99WWPessoaCertificadoDS_19_Tfpessoacertificado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWPessoaCertificadoDS_20_Tfpessoacertificado_emissao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWPessoaCertificadoDS_21_Tfpessoacertificado_emissao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV102WWPessoaCertificadoDS_22_Tfpessoacertificado_validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWPessoaCertificadoDS_23_Tfpessoacertificado_validade_to",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RA2,11,0,true,false )
             ,new CursorDef("H00RA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RA3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                return;
       }
    }

 }

}
