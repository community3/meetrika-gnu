/*
               File: Email
        Description: Email
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:13.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class email : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Email_Guid = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Email_Guid", AV7Email_Guid.ToString());
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vEMAIL_GUID", GetSecureSignedToken( "", AV7Email_Guid));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Email", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtEmail_Titulo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public email( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public email( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           Guid aP1_Email_Guid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Email_Guid = aP1_Email_Guid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_44183( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_44183e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Guid_Internalname, A1665Email_Guid.ToString(), A1665Email_Guid.ToString(), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Guid_Jsonclick, 0, "Attribute", "", "", "", edtEmail_Guid_Visible, edtEmail_Guid_Enabled, 1, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Email.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_44183( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_44183( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_44183e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_44183( true) ;
         }
         return  ;
      }

      protected void wb_table3_36_44183e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_44183e( true) ;
         }
         else
         {
            wb_table1_2_44183e( false) ;
         }
      }

      protected void wb_table3_36_44183( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_44183e( true) ;
         }
         else
         {
            wb_table3_36_44183e( false) ;
         }
      }

      protected void wb_table2_5_44183( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_44183( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_44183e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_44183e( true) ;
         }
         else
         {
            wb_table2_5_44183e( false) ;
         }
      }

      protected void wb_table4_13_44183( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_titulo_Internalname, "Assunto", "", "", lblTextblockemail_titulo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Titulo_Internalname, A1669Email_Titulo, StringUtil.RTrim( context.localUtil.Format( A1669Email_Titulo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Titulo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtEmail_Titulo_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_corpo_Internalname, "Corpo", "", "", lblTextblockemail_corpo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtEmail_Corpo_Internalname, A1670Email_Corpo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtEmail_Corpo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_proc_replace_Internalname, "Procedure Replace", "", "", lblTextblockemail_proc_replace_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Proc_Replace_Internalname, A1671Email_Proc_Replace, StringUtil.RTrim( context.localUtil.Format( A1671Email_Proc_Replace, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Proc_Replace_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtEmail_Proc_Replace_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockemail_key_Internalname, "Key", "", "", lblTextblockemail_key_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmail_Key_Internalname, A1672Email_Key, StringUtil.RTrim( context.localUtil.Format( A1672Email_Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmail_Key_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtEmail_Key_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Email.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_44183e( true) ;
         }
         else
         {
            wb_table4_13_44183e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11442 */
         E11442 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1669Email_Titulo = cgiGet( edtEmail_Titulo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
               A1670Email_Corpo = cgiGet( edtEmail_Corpo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
               A1671Email_Proc_Replace = cgiGet( edtEmail_Proc_Replace_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1671Email_Proc_Replace", A1671Email_Proc_Replace);
               A1672Email_Key = cgiGet( edtEmail_Key_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1672Email_Key", A1672Email_Key);
               if ( StringUtil.StrCmp(cgiGet( edtEmail_Guid_Internalname), "") == 0 )
               {
                  A1665Email_Guid = (Guid)(System.Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
               }
               else
               {
                  try
                  {
                     A1665Email_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmail_Guid_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "EMAIL_GUID");
                     AnyError = 1;
                     GX_FocusControl = edtEmail_Guid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               /* Read saved values. */
               Z1665Email_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( "Z1665Email_Guid")));
               Z1669Email_Titulo = cgiGet( "Z1669Email_Titulo");
               Z1671Email_Proc_Replace = cgiGet( "Z1671Email_Proc_Replace");
               Z1672Email_Key = cgiGet( "Z1672Email_Key");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Email_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( "vEMAIL_GUID")));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Email";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1665Email_Guid != Z1665Email_Guid ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("email:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1665Email_Guid = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                  getEqualNoModal( ) ;
                  if ( ! (System.Guid.Empty==AV7Email_Guid) )
                  {
                     A1665Email_Guid = (Guid)(AV7Email_Guid);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                  }
                  else
                  {
                     if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (System.Guid.Empty==A1665Email_Guid) && ( Gx_BScreen == 0 ) )
                     {
                        A1665Email_Guid = (Guid)(Guid.NewGuid( ));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                     }
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode183 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     if ( ! (System.Guid.Empty==AV7Email_Guid) )
                     {
                        A1665Email_Guid = (Guid)(AV7Email_Guid);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                     }
                     else
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (System.Guid.Empty==A1665Email_Guid) && ( Gx_BScreen == 0 ) )
                        {
                           A1665Email_Guid = (Guid)(Guid.NewGuid( ));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                        }
                     }
                     Gx_mode = sMode183;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound183 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_440( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "EMAIL_GUID");
                        AnyError = 1;
                        GX_FocusControl = edtEmail_Guid_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11442 */
                           E11442 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12442 */
                           E12442 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12442 */
            E12442 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll44183( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes44183( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_440( )
      {
         BeforeValidate44183( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls44183( ) ;
            }
            else
            {
               CheckExtendedTable44183( ) ;
               CloseExtendedTableCursors44183( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption440( )
      {
      }

      protected void E11442( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtEmail_Guid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Guid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Guid_Visible), 5, 0)));
      }

      protected void E12442( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwemail.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM44183( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1669Email_Titulo = T00443_A1669Email_Titulo[0];
               Z1671Email_Proc_Replace = T00443_A1671Email_Proc_Replace[0];
               Z1672Email_Key = T00443_A1672Email_Key[0];
            }
            else
            {
               Z1669Email_Titulo = A1669Email_Titulo;
               Z1671Email_Proc_Replace = A1671Email_Proc_Replace;
               Z1672Email_Key = A1672Email_Key;
            }
         }
         if ( GX_JID == -8 )
         {
            Z1665Email_Guid = (Guid)(A1665Email_Guid);
            Z1669Email_Titulo = A1669Email_Titulo;
            Z1670Email_Corpo = A1670Email_Corpo;
            Z1671Email_Proc_Replace = A1671Email_Proc_Replace;
            Z1672Email_Key = A1672Email_Key;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (System.Guid.Empty==AV7Email_Guid) )
         {
            edtEmail_Guid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Guid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Guid_Enabled), 5, 0)));
         }
         else
         {
            edtEmail_Guid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Guid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Guid_Enabled), 5, 0)));
         }
         if ( ! (System.Guid.Empty==AV7Email_Guid) )
         {
            edtEmail_Guid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Guid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Guid_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ! (System.Guid.Empty==AV7Email_Guid) )
         {
            A1665Email_Guid = (Guid)(AV7Email_Guid);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (System.Guid.Empty==A1665Email_Guid) && ( Gx_BScreen == 0 ) )
            {
               A1665Email_Guid = (Guid)(Guid.NewGuid( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load44183( )
      {
         /* Using cursor T00444 */
         pr_default.execute(2, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound183 = 1;
            A1669Email_Titulo = T00444_A1669Email_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
            A1670Email_Corpo = T00444_A1670Email_Corpo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
            A1671Email_Proc_Replace = T00444_A1671Email_Proc_Replace[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1671Email_Proc_Replace", A1671Email_Proc_Replace);
            A1672Email_Key = T00444_A1672Email_Key[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1672Email_Key", A1672Email_Key);
            ZM44183( -8) ;
         }
         pr_default.close(2);
         OnLoadActions44183( ) ;
      }

      protected void OnLoadActions44183( )
      {
      }

      protected void CheckExtendedTable44183( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1669Email_Titulo)) )
         {
            GX_msglist.addItem("Assunto � obrigat�rio.", 1, "EMAIL_TITULO");
            AnyError = 1;
            GX_FocusControl = edtEmail_Titulo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1670Email_Corpo)) )
         {
            GX_msglist.addItem("Corpo � obrigat�rio.", 1, "EMAIL_CORPO");
            AnyError = 1;
            GX_FocusControl = edtEmail_Corpo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1672Email_Key)) )
         {
            GX_msglist.addItem("Key � obrigat�rio.", 1, "EMAIL_KEY");
            AnyError = 1;
            GX_FocusControl = edtEmail_Key_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors44183( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey44183( )
      {
         /* Using cursor T00445 */
         pr_default.execute(3, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound183 = 1;
         }
         else
         {
            RcdFound183 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00443 */
         pr_default.execute(1, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM44183( 8) ;
            RcdFound183 = 1;
            A1665Email_Guid = (Guid)((Guid)(T00443_A1665Email_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
            A1669Email_Titulo = T00443_A1669Email_Titulo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
            A1670Email_Corpo = T00443_A1670Email_Corpo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
            A1671Email_Proc_Replace = T00443_A1671Email_Proc_Replace[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1671Email_Proc_Replace", A1671Email_Proc_Replace);
            A1672Email_Key = T00443_A1672Email_Key[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1672Email_Key", A1672Email_Key);
            Z1665Email_Guid = (Guid)(A1665Email_Guid);
            sMode183 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load44183( ) ;
            if ( AnyError == 1 )
            {
               RcdFound183 = 0;
               InitializeNonKey44183( ) ;
            }
            Gx_mode = sMode183;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound183 = 0;
            InitializeNonKey44183( ) ;
            sMode183 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode183;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey44183( ) ;
         if ( RcdFound183 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound183 = 0;
         /* Using cursor T00446 */
         pr_default.execute(4, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( GuidUtil.Compare(T00446_A1665Email_Guid[0], A1665Email_Guid, 1) < 0 ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( GuidUtil.Compare(T00446_A1665Email_Guid[0], A1665Email_Guid, 1) > 0 ) ) )
            {
               A1665Email_Guid = (Guid)((Guid)(T00446_A1665Email_Guid[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
               RcdFound183 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound183 = 0;
         /* Using cursor T00447 */
         pr_default.execute(5, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( GuidUtil.Compare(T00447_A1665Email_Guid[0], A1665Email_Guid, 1) > 0 ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( GuidUtil.Compare(T00447_A1665Email_Guid[0], A1665Email_Guid, 1) < 0 ) ) )
            {
               A1665Email_Guid = (Guid)((Guid)(T00447_A1665Email_Guid[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
               RcdFound183 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey44183( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtEmail_Titulo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert44183( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound183 == 1 )
            {
               if ( A1665Email_Guid != Z1665Email_Guid )
               {
                  A1665Email_Guid = (Guid)(Z1665Email_Guid);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "EMAIL_GUID");
                  AnyError = 1;
                  GX_FocusControl = edtEmail_Guid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtEmail_Titulo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update44183( ) ;
                  GX_FocusControl = edtEmail_Titulo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1665Email_Guid != Z1665Email_Guid )
               {
                  /* Insert record */
                  GX_FocusControl = edtEmail_Titulo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert44183( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "EMAIL_GUID");
                     AnyError = 1;
                     GX_FocusControl = edtEmail_Guid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtEmail_Titulo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert44183( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1665Email_Guid != Z1665Email_Guid )
         {
            A1665Email_Guid = (Guid)(Z1665Email_Guid);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "EMAIL_GUID");
            AnyError = 1;
            GX_FocusControl = edtEmail_Guid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtEmail_Titulo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency44183( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00442 */
            pr_default.execute(0, new Object[] {A1665Email_Guid});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1669Email_Titulo, T00442_A1669Email_Titulo[0]) != 0 ) || ( StringUtil.StrCmp(Z1671Email_Proc_Replace, T00442_A1671Email_Proc_Replace[0]) != 0 ) || ( StringUtil.StrCmp(Z1672Email_Key, T00442_A1672Email_Key[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1669Email_Titulo, T00442_A1669Email_Titulo[0]) != 0 )
               {
                  GXUtil.WriteLog("email:[seudo value changed for attri]"+"Email_Titulo");
                  GXUtil.WriteLogRaw("Old: ",Z1669Email_Titulo);
                  GXUtil.WriteLogRaw("Current: ",T00442_A1669Email_Titulo[0]);
               }
               if ( StringUtil.StrCmp(Z1671Email_Proc_Replace, T00442_A1671Email_Proc_Replace[0]) != 0 )
               {
                  GXUtil.WriteLog("email:[seudo value changed for attri]"+"Email_Proc_Replace");
                  GXUtil.WriteLogRaw("Old: ",Z1671Email_Proc_Replace);
                  GXUtil.WriteLogRaw("Current: ",T00442_A1671Email_Proc_Replace[0]);
               }
               if ( StringUtil.StrCmp(Z1672Email_Key, T00442_A1672Email_Key[0]) != 0 )
               {
                  GXUtil.WriteLog("email:[seudo value changed for attri]"+"Email_Key");
                  GXUtil.WriteLogRaw("Old: ",Z1672Email_Key);
                  GXUtil.WriteLogRaw("Current: ",T00442_A1672Email_Key[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Email"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert44183( )
      {
         BeforeValidate44183( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable44183( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM44183( 0) ;
            CheckOptimisticConcurrency44183( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm44183( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert44183( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00448 */
                     pr_default.execute(6, new Object[] {A1669Email_Titulo, A1670Email_Corpo, A1671Email_Proc_Replace, A1672Email_Key, A1665Email_Guid});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Email") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load44183( ) ;
            }
            EndLevel44183( ) ;
         }
         CloseExtendedTableCursors44183( ) ;
      }

      protected void Update44183( )
      {
         BeforeValidate44183( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable44183( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency44183( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm44183( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate44183( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00449 */
                     pr_default.execute(7, new Object[] {A1669Email_Titulo, A1670Email_Corpo, A1671Email_Proc_Replace, A1672Email_Key, A1665Email_Guid});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Email") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate44183( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel44183( ) ;
         }
         CloseExtendedTableCursors44183( ) ;
      }

      protected void DeferredUpdate44183( )
      {
      }

      protected void delete( )
      {
         BeforeValidate44183( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency44183( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls44183( ) ;
            AfterConfirm44183( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete44183( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004410 */
                  pr_default.execute(8, new Object[] {A1665Email_Guid});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Email") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode183 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel44183( ) ;
         Gx_mode = sMode183;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls44183( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004411 */
            pr_default.execute(9, new Object[] {A1665Email_Guid});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Email_Instancia"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel44183( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete44183( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Email");
            if ( AnyError == 0 )
            {
               ConfirmValues440( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Email");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart44183( )
      {
         /* Scan By routine */
         /* Using cursor T004412 */
         pr_default.execute(10);
         RcdFound183 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound183 = 1;
            A1665Email_Guid = (Guid)((Guid)(T004412_A1665Email_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext44183( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound183 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound183 = 1;
            A1665Email_Guid = (Guid)((Guid)(T004412_A1665Email_Guid[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
         }
      }

      protected void ScanEnd44183( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm44183( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert44183( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate44183( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete44183( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete44183( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate44183( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes44183( )
      {
         edtEmail_Titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Titulo_Enabled), 5, 0)));
         edtEmail_Corpo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Corpo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Corpo_Enabled), 5, 0)));
         edtEmail_Proc_Replace_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Proc_Replace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Proc_Replace_Enabled), 5, 0)));
         edtEmail_Key_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Key_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Key_Enabled), 5, 0)));
         edtEmail_Guid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Guid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmail_Guid_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues440( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117291411");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("email.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(AV7Email_Guid.ToString())+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1665Email_Guid", Z1665Email_Guid.ToString());
         GxWebStd.gx_hidden_field( context, "Z1669Email_Titulo", Z1669Email_Titulo);
         GxWebStd.gx_hidden_field( context, "Z1671Email_Proc_Replace", Z1671Email_Proc_Replace);
         GxWebStd.gx_hidden_field( context, "Z1672Email_Key", Z1672Email_Key);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vEMAIL_GUID", AV7Email_Guid.ToString());
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vEMAIL_GUID", GetSecureSignedToken( "", AV7Email_Guid));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Email";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("email:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("email.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(AV7Email_Guid.ToString()) ;
      }

      public override String GetPgmname( )
      {
         return "Email" ;
      }

      public override String GetPgmdesc( )
      {
         return "Email" ;
      }

      protected void InitializeNonKey44183( )
      {
         A1669Email_Titulo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1669Email_Titulo", A1669Email_Titulo);
         A1670Email_Corpo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1670Email_Corpo", A1670Email_Corpo);
         A1671Email_Proc_Replace = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1671Email_Proc_Replace", A1671Email_Proc_Replace);
         A1672Email_Key = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1672Email_Key", A1672Email_Key);
         Z1669Email_Titulo = "";
         Z1671Email_Proc_Replace = "";
         Z1672Email_Key = "";
      }

      protected void InitAll44183( )
      {
         A1665Email_Guid = (Guid)(Guid.NewGuid( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1665Email_Guid", A1665Email_Guid.ToString());
         InitializeNonKey44183( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117291425");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("email.js", "?20203117291425");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockemail_titulo_Internalname = "TEXTBLOCKEMAIL_TITULO";
         edtEmail_Titulo_Internalname = "EMAIL_TITULO";
         lblTextblockemail_corpo_Internalname = "TEXTBLOCKEMAIL_CORPO";
         edtEmail_Corpo_Internalname = "EMAIL_CORPO";
         lblTextblockemail_proc_replace_Internalname = "TEXTBLOCKEMAIL_PROC_REPLACE";
         edtEmail_Proc_Replace_Internalname = "EMAIL_PROC_REPLACE";
         lblTextblockemail_key_Internalname = "TEXTBLOCKEMAIL_KEY";
         edtEmail_Key_Internalname = "EMAIL_KEY";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtEmail_Guid_Internalname = "EMAIL_GUID";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Email";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Email";
         edtEmail_Key_Jsonclick = "";
         edtEmail_Key_Enabled = 1;
         edtEmail_Proc_Replace_Jsonclick = "";
         edtEmail_Proc_Replace_Enabled = 1;
         edtEmail_Corpo_Enabled = 1;
         edtEmail_Titulo_Jsonclick = "";
         edtEmail_Titulo_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtEmail_Guid_Jsonclick = "";
         edtEmail_Guid_Enabled = 1;
         edtEmail_Guid_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Email_Guid',fld:'vEMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12442',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV7Email_Guid = (Guid)(System.Guid.Empty);
         Z1665Email_Guid = (Guid)(System.Guid.Empty);
         Z1669Email_Titulo = "";
         Z1671Email_Proc_Replace = "";
         Z1672Email_Key = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockemail_titulo_Jsonclick = "";
         A1669Email_Titulo = "";
         lblTextblockemail_corpo_Jsonclick = "";
         A1670Email_Corpo = "";
         lblTextblockemail_proc_replace_Jsonclick = "";
         A1671Email_Proc_Replace = "";
         lblTextblockemail_key_Jsonclick = "";
         A1672Email_Key = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode183 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z1670Email_Corpo = "";
         T00444_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T00444_A1669Email_Titulo = new String[] {""} ;
         T00444_A1670Email_Corpo = new String[] {""} ;
         T00444_A1671Email_Proc_Replace = new String[] {""} ;
         T00444_A1672Email_Key = new String[] {""} ;
         T00445_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T00443_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T00443_A1669Email_Titulo = new String[] {""} ;
         T00443_A1670Email_Corpo = new String[] {""} ;
         T00443_A1671Email_Proc_Replace = new String[] {""} ;
         T00443_A1672Email_Key = new String[] {""} ;
         T00446_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T00447_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T00442_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         T00442_A1669Email_Titulo = new String[] {""} ;
         T00442_A1670Email_Corpo = new String[] {""} ;
         T00442_A1671Email_Proc_Replace = new String[] {""} ;
         T00442_A1672Email_Key = new String[] {""} ;
         T004411_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         T004412_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.email__default(),
            new Object[][] {
                new Object[] {
               T00442_A1665Email_Guid, T00442_A1669Email_Titulo, T00442_A1670Email_Corpo, T00442_A1671Email_Proc_Replace, T00442_A1672Email_Key
               }
               , new Object[] {
               T00443_A1665Email_Guid, T00443_A1669Email_Titulo, T00443_A1670Email_Corpo, T00443_A1671Email_Proc_Replace, T00443_A1672Email_Key
               }
               , new Object[] {
               T00444_A1665Email_Guid, T00444_A1669Email_Titulo, T00444_A1670Email_Corpo, T00444_A1671Email_Proc_Replace, T00444_A1672Email_Key
               }
               , new Object[] {
               T00445_A1665Email_Guid
               }
               , new Object[] {
               T00446_A1665Email_Guid
               }
               , new Object[] {
               T00447_A1665Email_Guid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004411_A1666Email_Instancia_Guid
               }
               , new Object[] {
               T004412_A1665Email_Guid
               }
            }
         );
         Z1665Email_Guid = (Guid)(Guid.NewGuid( ));
         A1665Email_Guid = (Guid)(Guid.NewGuid( ));
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound183 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int trnEnded ;
      private int edtEmail_Guid_Visible ;
      private int edtEmail_Guid_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtEmail_Titulo_Enabled ;
      private int edtEmail_Corpo_Enabled ;
      private int edtEmail_Proc_Replace_Enabled ;
      private int edtEmail_Key_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtEmail_Titulo_Internalname ;
      private String TempTags ;
      private String edtEmail_Guid_Internalname ;
      private String edtEmail_Guid_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockemail_titulo_Internalname ;
      private String lblTextblockemail_titulo_Jsonclick ;
      private String edtEmail_Titulo_Jsonclick ;
      private String lblTextblockemail_corpo_Internalname ;
      private String lblTextblockemail_corpo_Jsonclick ;
      private String edtEmail_Corpo_Internalname ;
      private String lblTextblockemail_proc_replace_Internalname ;
      private String lblTextblockemail_proc_replace_Jsonclick ;
      private String edtEmail_Proc_Replace_Internalname ;
      private String edtEmail_Proc_Replace_Jsonclick ;
      private String lblTextblockemail_key_Internalname ;
      private String lblTextblockemail_key_Jsonclick ;
      private String edtEmail_Key_Internalname ;
      private String edtEmail_Key_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode183 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A1670Email_Corpo ;
      private String Z1670Email_Corpo ;
      private String Z1669Email_Titulo ;
      private String Z1671Email_Proc_Replace ;
      private String Z1672Email_Key ;
      private String A1669Email_Titulo ;
      private String A1671Email_Proc_Replace ;
      private String A1672Email_Key ;
      private Guid wcpOAV7Email_Guid ;
      private Guid Z1665Email_Guid ;
      private Guid AV7Email_Guid ;
      private Guid A1665Email_Guid ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] T00444_A1665Email_Guid ;
      private String[] T00444_A1669Email_Titulo ;
      private String[] T00444_A1670Email_Corpo ;
      private String[] T00444_A1671Email_Proc_Replace ;
      private String[] T00444_A1672Email_Key ;
      private Guid[] T00445_A1665Email_Guid ;
      private Guid[] T00443_A1665Email_Guid ;
      private String[] T00443_A1669Email_Titulo ;
      private String[] T00443_A1670Email_Corpo ;
      private String[] T00443_A1671Email_Proc_Replace ;
      private String[] T00443_A1672Email_Key ;
      private Guid[] T00446_A1665Email_Guid ;
      private Guid[] T00447_A1665Email_Guid ;
      private Guid[] T00442_A1665Email_Guid ;
      private String[] T00442_A1669Email_Titulo ;
      private String[] T00442_A1670Email_Corpo ;
      private String[] T00442_A1671Email_Proc_Replace ;
      private String[] T00442_A1672Email_Key ;
      private Guid[] T004411_A1666Email_Instancia_Guid ;
      private Guid[] T004412_A1665Email_Guid ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class email__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00444 ;
          prmT00444 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00445 ;
          prmT00445 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00443 ;
          prmT00443 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00446 ;
          prmT00446 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00447 ;
          prmT00447 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00442 ;
          prmT00442 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00448 ;
          prmT00448 = new Object[] {
          new Object[] {"@Email_Titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Corpo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Proc_Replace",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Key",SqlDbType.VarChar,40,0} ,
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT00449 ;
          prmT00449 = new Object[] {
          new Object[] {"@Email_Titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Corpo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Proc_Replace",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Key",SqlDbType.VarChar,40,0} ,
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004410 ;
          prmT004410 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004411 ;
          prmT004411 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmT004412 ;
          prmT004412 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00442", "SELECT [Email_Guid], [Email_Titulo], [Email_Corpo], [Email_Proc_Replace], [Email_Key] FROM [Email] WITH (UPDLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00442,1,0,true,false )
             ,new CursorDef("T00443", "SELECT [Email_Guid], [Email_Titulo], [Email_Corpo], [Email_Proc_Replace], [Email_Key] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00443,1,0,true,false )
             ,new CursorDef("T00444", "SELECT TM1.[Email_Guid], TM1.[Email_Titulo], TM1.[Email_Corpo], TM1.[Email_Proc_Replace], TM1.[Email_Key] FROM [Email] TM1 WITH (NOLOCK) WHERE TM1.[Email_Guid] = @Email_Guid ORDER BY TM1.[Email_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00444,100,0,true,false )
             ,new CursorDef("T00445", "SELECT [Email_Guid] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00445,1,0,true,false )
             ,new CursorDef("T00446", "SELECT TOP 1 [Email_Guid] FROM [Email] WITH (NOLOCK) WHERE ( [Email_Guid] > @Email_Guid) ORDER BY [Email_Guid]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00446,1,0,true,true )
             ,new CursorDef("T00447", "SELECT TOP 1 [Email_Guid] FROM [Email] WITH (NOLOCK) WHERE ( [Email_Guid] < @Email_Guid) ORDER BY [Email_Guid] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00447,1,0,true,true )
             ,new CursorDef("T00448", "INSERT INTO [Email]([Email_Titulo], [Email_Corpo], [Email_Proc_Replace], [Email_Key], [Email_Guid]) VALUES(@Email_Titulo, @Email_Corpo, @Email_Proc_Replace, @Email_Key, @Email_Guid)", GxErrorMask.GX_NOMASK,prmT00448)
             ,new CursorDef("T00449", "UPDATE [Email] SET [Email_Titulo]=@Email_Titulo, [Email_Corpo]=@Email_Corpo, [Email_Proc_Replace]=@Email_Proc_Replace, [Email_Key]=@Email_Key  WHERE [Email_Guid] = @Email_Guid", GxErrorMask.GX_NOMASK,prmT00449)
             ,new CursorDef("T004410", "DELETE FROM [Email]  WHERE [Email_Guid] = @Email_Guid", GxErrorMask.GX_NOMASK,prmT004410)
             ,new CursorDef("T004411", "SELECT TOP 1 [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmT004411,1,0,true,true )
             ,new CursorDef("T004412", "SELECT [Email_Guid] FROM [Email] WITH (NOLOCK) ORDER BY [Email_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004412,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                return;
             case 1 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                return;
             case 2 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                return;
             case 3 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 4 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 5 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 9 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 10 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (Guid)parms[4]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (Guid)parms[4]);
                return;
             case 8 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
       }
    }

 }

}
