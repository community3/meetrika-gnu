/*
               File: type_SdtGAMApplicationEnvironment
        Description: GAMApplicationEnvironment
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:10.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMApplicationEnvironment : GxUserType, IGxExternalObject
   {
      public SdtGAMApplicationEnvironment( )
      {
         initialize();
      }

      public SdtGAMApplicationEnvironment( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMApplicationEnvironment_externalReference == null )
         {
            GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
         }
         returntostring = "";
         returntostring = (String)(GAMApplicationEnvironment_externalReference.ToString());
         return returntostring ;
      }

      public long gxTpr_Id
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.Id ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.Id = value;
         }

      }

      public long gxTpr_Applicationid
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.ApplicationId ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.ApplicationId = value;
         }

      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.GUID ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.GUID = value;
         }

      }

      public String gxTpr_Host
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.Host ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.Host = value;
         }

      }

      public int gxTpr_Port
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.Port ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.Port = value;
         }

      }

      public String gxTpr_Virtualdirectory
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.VirtualDirectory ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.VirtualDirectory = value;
         }

      }

      public bool gxTpr_Secureprotocol
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.SecureProtocol ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.SecureProtocol = value;
         }

      }

      public String gxTpr_Programpackage
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.ProgramPackage ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.ProgramPackage = value;
         }

      }

      public String gxTpr_Programextension
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.ProgramExtension ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.ProgramExtension = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.Name ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.Name = value;
         }

      }

      public int gxTpr_Order
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.Order ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.Order = value;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.DateCreated ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.UserCreated ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.DateUpdated ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference.UserUpdated ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            GAMApplicationEnvironment_externalReference.UserUpdated = value;
         }

      }

      public IGxCollection gxTpr_Descriptions
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMDescription", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm0 ;
            externalParm0 = GAMApplicationEnvironment_externalReference.Descriptions;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMDescription>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), intValue.ExternalInstance);
            GAMApplicationEnvironment_externalReference.Descriptions = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMApplicationEnvironment_externalReference == null )
            {
               GAMApplicationEnvironment_externalReference = new Artech.Security.GAMApplicationEnvironment(context);
            }
            return GAMApplicationEnvironment_externalReference ;
         }

         set {
            GAMApplicationEnvironment_externalReference = (Artech.Security.GAMApplicationEnvironment)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMApplicationEnvironment GAMApplicationEnvironment_externalReference=null ;
   }

}
