/*
               File: PRC_Enviar_Email_New
        Description: PRC_Enviar_Email_New
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:48.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_enviar_email_new : GXProcedure
   {
      public prc_enviar_email_new( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_enviar_email_new( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Address ,
                           String aP1_Name ,
                           String aP2_Subject ,
                           String aP3_Text )
      {
         this.AV9Address = aP0_Address;
         this.AV10Name = aP1_Name;
         this.AV11Subject = aP2_Subject;
         this.AV12Text = aP3_Text;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Address ,
                                 String aP1_Name ,
                                 String aP2_Subject ,
                                 String aP3_Text )
      {
         prc_enviar_email_new objprc_enviar_email_new;
         objprc_enviar_email_new = new prc_enviar_email_new();
         objprc_enviar_email_new.AV9Address = aP0_Address;
         objprc_enviar_email_new.AV10Name = aP1_Name;
         objprc_enviar_email_new.AV11Subject = aP2_Subject;
         objprc_enviar_email_new.AV12Text = aP3_Text;
         objprc_enviar_email_new.context.SetSubmitInitialConfig(context);
         objprc_enviar_email_new.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_enviar_email_new);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_enviar_email_new)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV15WWPContext) ;
         /* Using cursor P00C92 */
         pr_default.execute(0, new Object[] {AV15WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P00C92_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00C92_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = P00C92_A5AreaTrabalho_Codigo[0];
            A548Contratante_EmailSdaUser = P00C92_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00C92_n548Contratante_EmailSdaUser[0];
            A547Contratante_EmailSdaHost = P00C92_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00C92_n547Contratante_EmailSdaHost[0];
            A552Contratante_EmailSdaPort = P00C92_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00C92_n552Contratante_EmailSdaPort[0];
            A551Contratante_EmailSdaAut = P00C92_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00C92_n551Contratante_EmailSdaAut[0];
            A1048Contratante_EmailSdaSec = P00C92_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00C92_n1048Contratante_EmailSdaSec[0];
            A550Contratante_EmailSdaKey = P00C92_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00C92_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = P00C92_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00C92_n549Contratante_EmailSdaPass[0];
            A548Contratante_EmailSdaUser = P00C92_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = P00C92_n548Contratante_EmailSdaUser[0];
            A547Contratante_EmailSdaHost = P00C92_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = P00C92_n547Contratante_EmailSdaHost[0];
            A552Contratante_EmailSdaPort = P00C92_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = P00C92_n552Contratante_EmailSdaPort[0];
            A551Contratante_EmailSdaAut = P00C92_A551Contratante_EmailSdaAut[0];
            n551Contratante_EmailSdaAut = P00C92_n551Contratante_EmailSdaAut[0];
            A1048Contratante_EmailSdaSec = P00C92_A1048Contratante_EmailSdaSec[0];
            n1048Contratante_EmailSdaSec = P00C92_n1048Contratante_EmailSdaSec[0];
            A550Contratante_EmailSdaKey = P00C92_A550Contratante_EmailSdaKey[0];
            n550Contratante_EmailSdaKey = P00C92_n550Contratante_EmailSdaKey[0];
            A549Contratante_EmailSdaPass = P00C92_A549Contratante_EmailSdaPass[0];
            n549Contratante_EmailSdaPass = P00C92_n549Contratante_EmailSdaPass[0];
            AV13MailMessage.From.Address = StringUtil.Trim( A548Contratante_EmailSdaUser);
            AV13MailMessage.From.Name = StringUtil.Trim( A548Contratante_EmailSdaUser);
            AV8SmtpSession.Host = StringUtil.Trim( A547Contratante_EmailSdaHost);
            AV8SmtpSession.Port = A552Contratante_EmailSdaPort;
            AV8SmtpSession.Authentication = (short)((A551Contratante_EmailSdaAut ? 1 : 0));
            AV8SmtpSession.Secure = A1048Contratante_EmailSdaSec;
            AV8SmtpSession.UserName = StringUtil.Trim( A548Contratante_EmailSdaUser);
            AV8SmtpSession.Password = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
            AV8SmtpSession.Sender.Address = StringUtil.Trim( A548Contratante_EmailSdaUser);
            AV8SmtpSession.Sender.Name = StringUtil.Trim( A548Contratante_EmailSdaUser);
            AV13MailMessage.HTMLText = AV12Text;
            AV13MailMessage.Subject = AV11Subject;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV14MailRecipient = new GeneXus.Mail.GXMailRecipient();
         AV14MailRecipient.Address = AV9Address;
         AV14MailRecipient.Name = AV10Name;
         AV13MailMessage.To.Add(AV14MailRecipient) ;
         AV8SmtpSession.Login();
         if ( AV8SmtpSession.ErrCode == 0 )
         {
            AV8SmtpSession.Send(AV13MailMessage);
            if ( AV8SmtpSession.ErrCode != 0 )
            {
               GX_msglist.addItem("Erro Send:"+AV8SmtpSession.ErrDescription);
            }
            AV8SmtpSession.Logout();
         }
         else
         {
            GX_msglist.addItem("Erro Login:"+AV8SmtpSession.ErrDescription);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV15WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00C92_A29Contratante_Codigo = new int[1] ;
         P00C92_n29Contratante_Codigo = new bool[] {false} ;
         P00C92_A5AreaTrabalho_Codigo = new int[1] ;
         P00C92_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00C92_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00C92_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00C92_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00C92_A552Contratante_EmailSdaPort = new short[1] ;
         P00C92_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00C92_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00C92_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00C92_A1048Contratante_EmailSdaSec = new short[1] ;
         P00C92_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         P00C92_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00C92_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00C92_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00C92_n549Contratante_EmailSdaPass = new bool[] {false} ;
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         A550Contratante_EmailSdaKey = "";
         A549Contratante_EmailSdaPass = "";
         AV13MailMessage = new GeneXus.Mail.GXMailMessage();
         AV8SmtpSession = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         AV14MailRecipient = new GeneXus.Mail.GXMailRecipient();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_enviar_email_new__default(),
            new Object[][] {
                new Object[] {
               P00C92_A29Contratante_Codigo, P00C92_n29Contratante_Codigo, P00C92_A5AreaTrabalho_Codigo, P00C92_A548Contratante_EmailSdaUser, P00C92_n548Contratante_EmailSdaUser, P00C92_A547Contratante_EmailSdaHost, P00C92_n547Contratante_EmailSdaHost, P00C92_A552Contratante_EmailSdaPort, P00C92_n552Contratante_EmailSdaPort, P00C92_A551Contratante_EmailSdaAut,
               P00C92_n551Contratante_EmailSdaAut, P00C92_A1048Contratante_EmailSdaSec, P00C92_n1048Contratante_EmailSdaSec, P00C92_A550Contratante_EmailSdaKey, P00C92_n550Contratante_EmailSdaKey, P00C92_A549Contratante_EmailSdaPass, P00C92_n549Contratante_EmailSdaPass
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private String scmdbuf ;
      private String A550Contratante_EmailSdaKey ;
      private bool n29Contratante_Codigo ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n552Contratante_EmailSdaPort ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n549Contratante_EmailSdaPass ;
      private String AV12Text ;
      private String AV9Address ;
      private String AV10Name ;
      private String AV11Subject ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private String A549Contratante_EmailSdaPass ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00C92_A29Contratante_Codigo ;
      private bool[] P00C92_n29Contratante_Codigo ;
      private int[] P00C92_A5AreaTrabalho_Codigo ;
      private String[] P00C92_A548Contratante_EmailSdaUser ;
      private bool[] P00C92_n548Contratante_EmailSdaUser ;
      private String[] P00C92_A547Contratante_EmailSdaHost ;
      private bool[] P00C92_n547Contratante_EmailSdaHost ;
      private short[] P00C92_A552Contratante_EmailSdaPort ;
      private bool[] P00C92_n552Contratante_EmailSdaPort ;
      private bool[] P00C92_A551Contratante_EmailSdaAut ;
      private bool[] P00C92_n551Contratante_EmailSdaAut ;
      private short[] P00C92_A1048Contratante_EmailSdaSec ;
      private bool[] P00C92_n1048Contratante_EmailSdaSec ;
      private String[] P00C92_A550Contratante_EmailSdaKey ;
      private bool[] P00C92_n550Contratante_EmailSdaKey ;
      private String[] P00C92_A549Contratante_EmailSdaPass ;
      private bool[] P00C92_n549Contratante_EmailSdaPass ;
      private GeneXus.Mail.GXMailMessage AV13MailMessage ;
      private GeneXus.Mail.GXMailRecipient AV14MailRecipient ;
      private GeneXus.Mail.GXSMTPSession AV8SmtpSession ;
      private wwpbaseobjects.SdtWWPContext AV15WWPContext ;
   }

   public class prc_enviar_email_new__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00C92 ;
          prmP00C92 = new Object[] {
          new Object[] {"@AV15WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00C92", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaHost], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaAut], T2.[Contratante_EmailSdaSec], T2.[Contratante_EmailSdaKey], T2.[Contratante_EmailSdaPass] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV15WWPC_1Areatrabalho_codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C92,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 32) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
