/*
               File: Prc_AreaTrabalho_Sistema
        Description: Prc_Area Trabalho_Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:4.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_areatrabalho_sistema : GXProcedure
   {
      public prc_areatrabalho_sistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_areatrabalho_sistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           out IGxCollection aP1_SDT_AreaTrabalho_SistemaCollection )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV10SDT_AreaTrabalho_SistemaCollection = new GxObjectCollection( context, "SDT_AreaTrabalho_Sistema", "GxEv3Up14_Meetrika", "SdtSDT_AreaTrabalho_Sistema", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_SDT_AreaTrabalho_SistemaCollection=this.AV10SDT_AreaTrabalho_SistemaCollection;
      }

      public IGxCollection executeUdp( int aP0_AreaTrabalho_Codigo )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV10SDT_AreaTrabalho_SistemaCollection = new GxObjectCollection( context, "SDT_AreaTrabalho_Sistema", "GxEv3Up14_Meetrika", "SdtSDT_AreaTrabalho_Sistema", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_SDT_AreaTrabalho_SistemaCollection=this.AV10SDT_AreaTrabalho_SistemaCollection;
         return AV10SDT_AreaTrabalho_SistemaCollection ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 out IGxCollection aP1_SDT_AreaTrabalho_SistemaCollection )
      {
         prc_areatrabalho_sistema objprc_areatrabalho_sistema;
         objprc_areatrabalho_sistema = new prc_areatrabalho_sistema();
         objprc_areatrabalho_sistema.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_areatrabalho_sistema.AV10SDT_AreaTrabalho_SistemaCollection = new GxObjectCollection( context, "SDT_AreaTrabalho_Sistema", "GxEv3Up14_Meetrika", "SdtSDT_AreaTrabalho_Sistema", "GeneXus.Programs") ;
         objprc_areatrabalho_sistema.context.SetSubmitInitialConfig(context);
         objprc_areatrabalho_sistema.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_areatrabalho_sistema);
         aP1_SDT_AreaTrabalho_SistemaCollection=this.AV10SDT_AreaTrabalho_SistemaCollection;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_areatrabalho_sistema)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00D82 */
         pr_default.execute(0, new Object[] {AV8AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A135Sistema_AreaTrabalhoCod = P00D82_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = P00D82_A127Sistema_Codigo[0];
            A416Sistema_Nome = P00D82_A416Sistema_Nome[0];
            A128Sistema_Descricao = P00D82_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00D82_n128Sistema_Descricao[0];
            A129Sistema_Sigla = P00D82_A129Sistema_Sigla[0];
            AV9SDT_AreaTrabalho_Sistema.gxTpr_External_id = A127Sistema_Codigo;
            AV9SDT_AreaTrabalho_Sistema.gxTpr_Name = A416Sistema_Nome;
            AV9SDT_AreaTrabalho_Sistema.gxTpr_Description = A128Sistema_Descricao;
            AV9SDT_AreaTrabalho_Sistema.gxTpr_Acronym = A129Sistema_Sigla;
            AV9SDT_AreaTrabalho_Sistema.gxTpr_Cost = 100;
            AV9SDT_AreaTrabalho_Sistema.gxTpr_Deadline = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV9SDT_AreaTrabalho_Sistema.gxTpr_Created_at = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV9SDT_AreaTrabalho_Sistema.gxTpr_Updated_at = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV10SDT_AreaTrabalho_SistemaCollection.Add(AV9SDT_AreaTrabalho_Sistema, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00D82_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00D82_A127Sistema_Codigo = new int[1] ;
         P00D82_A416Sistema_Nome = new String[] {""} ;
         P00D82_A128Sistema_Descricao = new String[] {""} ;
         P00D82_n128Sistema_Descricao = new bool[] {false} ;
         P00D82_A129Sistema_Sigla = new String[] {""} ;
         A416Sistema_Nome = "";
         A128Sistema_Descricao = "";
         A129Sistema_Sigla = "";
         AV9SDT_AreaTrabalho_Sistema = new SdtSDT_AreaTrabalho_Sistema(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_areatrabalho_sistema__default(),
            new Object[][] {
                new Object[] {
               P00D82_A135Sistema_AreaTrabalhoCod, P00D82_A127Sistema_Codigo, P00D82_A416Sistema_Nome, P00D82_A128Sistema_Descricao, P00D82_n128Sistema_Descricao, P00D82_A129Sistema_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8AreaTrabalho_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private String scmdbuf ;
      private String A129Sistema_Sigla ;
      private bool n128Sistema_Descricao ;
      private String A128Sistema_Descricao ;
      private String A416Sistema_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00D82_A135Sistema_AreaTrabalhoCod ;
      private int[] P00D82_A127Sistema_Codigo ;
      private String[] P00D82_A416Sistema_Nome ;
      private String[] P00D82_A128Sistema_Descricao ;
      private bool[] P00D82_n128Sistema_Descricao ;
      private String[] P00D82_A129Sistema_Sigla ;
      private IGxCollection aP1_SDT_AreaTrabalho_SistemaCollection ;
      [ObjectCollection(ItemType=typeof( SdtSDT_AreaTrabalho_Sistema ))]
      private IGxCollection AV10SDT_AreaTrabalho_SistemaCollection ;
      private SdtSDT_AreaTrabalho_Sistema AV9SDT_AreaTrabalho_Sistema ;
   }

   public class prc_areatrabalho_sistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D82 ;
          prmP00D82 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D82", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Codigo], [Sistema_Nome], [Sistema_Descricao], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_AreaTrabalhoCod] = @AV8AreaTrabalho_Codigo ORDER BY [Sistema_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D82,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 25) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.prc_areatrabalho_sistema_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class prc_areatrabalho_sistema_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( int AreaTrabalho_Codigo ,
                         out GxGenericCollection<SdtSDT_AreaTrabalho_Sistema_RESTInterface> SDT_AreaTrabalho_SistemaCollection )
    {
       SDT_AreaTrabalho_SistemaCollection = new GxGenericCollection<SdtSDT_AreaTrabalho_Sistema_RESTInterface>() ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("prc_areatrabalho_sistema") )
          {
             return  ;
          }
          prc_areatrabalho_sistema worker = new prc_areatrabalho_sistema(context) ;
          worker.IsMain = RunAsMain ;
          IGxCollection gxrSDT_AreaTrabalho_SistemaCollection = new GxObjectCollection() ;
          worker.execute(AreaTrabalho_Codigo,out gxrSDT_AreaTrabalho_SistemaCollection );
          worker.cleanup( );
          SDT_AreaTrabalho_SistemaCollection = new GxGenericCollection<SdtSDT_AreaTrabalho_Sistema_RESTInterface>(gxrSDT_AreaTrabalho_SistemaCollection) ;
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
