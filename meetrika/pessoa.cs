/*
               File: Pessoa
        Description: Pessoas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:15:35.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pessoa : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PESSOA_MUNICIPIOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPESSOA_MUNICIPIOCOD0B12( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A503Pessoa_MunicipioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n503Pessoa_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A503Pessoa_MunicipioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Pessoa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Pessoa_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPESSOA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Pessoa_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbPessoa_TipoPessoa.Name = "PESSOA_TIPOPESSOA";
         cmbPessoa_TipoPessoa.WebTags = "";
         cmbPessoa_TipoPessoa.addItem("", "(Nenhum)", 0);
         cmbPessoa_TipoPessoa.addItem("F", "F�sica", 0);
         cmbPessoa_TipoPessoa.addItem("J", "Jur�dica", 0);
         if ( cmbPessoa_TipoPessoa.ItemCount > 0 )
         {
            A36Pessoa_TipoPessoa = cmbPessoa_TipoPessoa.getValidValue(A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
         }
         dynPessoa_MunicipioCod.Name = "PESSOA_MUNICIPIOCOD";
         dynPessoa_MunicipioCod.WebTags = "";
         chkPessoa_Ativo.Name = "PESSOA_ATIVO";
         chkPessoa_Ativo.WebTags = "";
         chkPessoa_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPessoa_Ativo_Internalname, "TitleCaption", chkPessoa_Ativo.Caption);
         chkPessoa_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Pessoas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtPessoa_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public pessoa( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public pessoa( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Pessoa_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Pessoa_Codigo = aP1_Pessoa_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbPessoa_TipoPessoa = new GXCombobox();
         dynPessoa_MunicipioCod = new GXCombobox();
         chkPessoa_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbPessoa_TipoPessoa.ItemCount > 0 )
         {
            A36Pessoa_TipoPessoa = cmbPessoa_TipoPessoa.getValidValue(A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
         }
         if ( dynPessoa_MunicipioCod.ItemCount > 0 )
         {
            A503Pessoa_MunicipioCod = (int)(NumberUtil.Val( dynPessoa_MunicipioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0))), "."));
            n503Pessoa_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0B12( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0B12e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A34Pessoa_Codigo), 6, 0, ",", "")), ((edtPessoa_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtPessoa_Codigo_Visible, edtPessoa_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Pessoa.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0B12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPessoatitle_Internalname, "Pessoas", "", "", lblPessoatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_0B12( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_0B12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_0B12( true) ;
         }
         return  ;
      }

      protected void wb_table3_74_0B12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0B12e( true) ;
         }
         else
         {
            wb_table1_2_0B12e( false) ;
         }
      }

      protected void wb_table3_74_0B12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_0B12e( true) ;
         }
         else
         {
            wb_table3_74_0B12e( false) ;
         }
      }

      protected void wb_table2_8_0B12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_0B12( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_0B12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0B12e( true) ;
         }
         else
         {
            wb_table2_8_0B12e( false) ;
         }
      }

      protected void wb_table4_16_0B12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_nome_Internalname, "Nome", "", "", lblTextblockpessoa_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoa_Nome_Internalname, StringUtil.RTrim( A35Pessoa_Nome), StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Nome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtPessoa_Nome_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_tipopessoa_Internalname, "Tipo da Pessoa", "", "", lblTextblockpessoa_tipopessoa_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbPessoa_TipoPessoa, cmbPessoa_TipoPessoa_Internalname, StringUtil.RTrim( A36Pessoa_TipoPessoa), 1, cmbPessoa_TipoPessoa_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbPessoa_TipoPessoa.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_Pessoa.htm");
            cmbPessoa_TipoPessoa.CurrentValue = StringUtil.RTrim( A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPessoa_TipoPessoa_Internalname, "Values", (String)(cmbPessoa_TipoPessoa.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_docto_Internalname, lblTextblockpessoa_docto_Caption, "", "", lblTextblockpessoa_docto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoa_Docto_Internalname, A37Pessoa_Docto, StringUtil.RTrim( context.localUtil.Format( A37Pessoa_Docto, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Docto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoa_Docto_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_ie_Internalname, "Insc. Estadual", "", "", lblTextblockpessoa_ie_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoa_IE_Internalname, StringUtil.RTrim( A518Pessoa_IE), StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_IE_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoa_IE_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "IE", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_endereco_Internalname, "Endere�o", "", "", lblTextblockpessoa_endereco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoa_Endereco_Internalname, A519Pessoa_Endereco, StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Endereco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoa_Endereco_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_municipiocod_Internalname, "Municipio", "", "", lblTextblockpessoa_municipiocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynPessoa_MunicipioCod, dynPessoa_MunicipioCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)), 1, dynPessoa_MunicipioCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynPessoa_MunicipioCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_Pessoa.htm");
            dynPessoa_MunicipioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPessoa_MunicipioCod_Internalname, "Values", (String)(dynPessoa_MunicipioCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_uf_Internalname, "UF", "", "", lblTextblockpessoa_uf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_UF_Internalname, StringUtil.RTrim( A520Pessoa_UF), StringUtil.RTrim( context.localUtil.Format( A520Pessoa_UF, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_UF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoa_UF_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "UF", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_cep_Internalname, "CEP", "", "", lblTextblockpessoa_cep_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoa_CEP_Internalname, StringUtil.RTrim( A521Pessoa_CEP), StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_CEP_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoa_CEP_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_telefone_Internalname, "Telefone", "", "", lblTextblockpessoa_telefone_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoa_Telefone_Internalname, StringUtil.RTrim( A522Pessoa_Telefone), StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Telefone_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoa_Telefone_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_fax_Internalname, "Fax", "", "", lblTextblockpessoa_fax_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPessoa_Fax_Internalname, StringUtil.RTrim( A523Pessoa_Fax), StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Fax_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtPessoa_Fax_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_ativo_Internalname, "Ativo?", "", "", lblTextblockpessoa_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Pessoa.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkPessoa_Ativo_Internalname, StringUtil.BoolToStr( A38Pessoa_Ativo), "", "", 1, chkPessoa_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(71, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_0B12e( true) ;
         }
         else
         {
            wb_table4_16_0B12e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110B2 */
         E110B2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A35Pessoa_Nome = StringUtil.Upper( cgiGet( edtPessoa_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35Pessoa_Nome", A35Pessoa_Nome);
               cmbPessoa_TipoPessoa.CurrentValue = cgiGet( cmbPessoa_TipoPessoa_Internalname);
               A36Pessoa_TipoPessoa = cgiGet( cmbPessoa_TipoPessoa_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
               A37Pessoa_Docto = cgiGet( edtPessoa_Docto_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37Pessoa_Docto", A37Pessoa_Docto);
               A518Pessoa_IE = cgiGet( edtPessoa_IE_Internalname);
               n518Pessoa_IE = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A518Pessoa_IE", A518Pessoa_IE);
               n518Pessoa_IE = (String.IsNullOrEmpty(StringUtil.RTrim( A518Pessoa_IE)) ? true : false);
               A519Pessoa_Endereco = cgiGet( edtPessoa_Endereco_Internalname);
               n519Pessoa_Endereco = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
               n519Pessoa_Endereco = (String.IsNullOrEmpty(StringUtil.RTrim( A519Pessoa_Endereco)) ? true : false);
               dynPessoa_MunicipioCod.CurrentValue = cgiGet( dynPessoa_MunicipioCod_Internalname);
               A503Pessoa_MunicipioCod = (int)(NumberUtil.Val( cgiGet( dynPessoa_MunicipioCod_Internalname), "."));
               n503Pessoa_MunicipioCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
               n503Pessoa_MunicipioCod = ((0==A503Pessoa_MunicipioCod) ? true : false);
               A520Pessoa_UF = StringUtil.Upper( cgiGet( edtPessoa_UF_Internalname));
               n520Pessoa_UF = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A520Pessoa_UF", A520Pessoa_UF);
               A521Pessoa_CEP = cgiGet( edtPessoa_CEP_Internalname);
               n521Pessoa_CEP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A521Pessoa_CEP", A521Pessoa_CEP);
               n521Pessoa_CEP = (String.IsNullOrEmpty(StringUtil.RTrim( A521Pessoa_CEP)) ? true : false);
               A522Pessoa_Telefone = cgiGet( edtPessoa_Telefone_Internalname);
               n522Pessoa_Telefone = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
               n522Pessoa_Telefone = (String.IsNullOrEmpty(StringUtil.RTrim( A522Pessoa_Telefone)) ? true : false);
               A523Pessoa_Fax = cgiGet( edtPessoa_Fax_Internalname);
               n523Pessoa_Fax = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A523Pessoa_Fax", A523Pessoa_Fax);
               n523Pessoa_Fax = (String.IsNullOrEmpty(StringUtil.RTrim( A523Pessoa_Fax)) ? true : false);
               A38Pessoa_Ativo = StringUtil.StrToBool( cgiGet( chkPessoa_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
               A34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPessoa_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
               /* Read saved values. */
               Z34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z34Pessoa_Codigo"), ",", "."));
               Z35Pessoa_Nome = cgiGet( "Z35Pessoa_Nome");
               Z36Pessoa_TipoPessoa = cgiGet( "Z36Pessoa_TipoPessoa");
               Z37Pessoa_Docto = cgiGet( "Z37Pessoa_Docto");
               Z518Pessoa_IE = cgiGet( "Z518Pessoa_IE");
               n518Pessoa_IE = (String.IsNullOrEmpty(StringUtil.RTrim( A518Pessoa_IE)) ? true : false);
               Z519Pessoa_Endereco = cgiGet( "Z519Pessoa_Endereco");
               n519Pessoa_Endereco = (String.IsNullOrEmpty(StringUtil.RTrim( A519Pessoa_Endereco)) ? true : false);
               Z521Pessoa_CEP = cgiGet( "Z521Pessoa_CEP");
               n521Pessoa_CEP = (String.IsNullOrEmpty(StringUtil.RTrim( A521Pessoa_CEP)) ? true : false);
               Z522Pessoa_Telefone = cgiGet( "Z522Pessoa_Telefone");
               n522Pessoa_Telefone = (String.IsNullOrEmpty(StringUtil.RTrim( A522Pessoa_Telefone)) ? true : false);
               Z523Pessoa_Fax = cgiGet( "Z523Pessoa_Fax");
               n523Pessoa_Fax = (String.IsNullOrEmpty(StringUtil.RTrim( A523Pessoa_Fax)) ? true : false);
               Z38Pessoa_Ativo = StringUtil.StrToBool( cgiGet( "Z38Pessoa_Ativo"));
               Z503Pessoa_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( "Z503Pessoa_MunicipioCod"), ",", "."));
               n503Pessoa_MunicipioCod = ((0==A503Pessoa_MunicipioCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N503Pessoa_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( "N503Pessoa_MunicipioCod"), ",", "."));
               n503Pessoa_MunicipioCod = ((0==A503Pessoa_MunicipioCod) ? true : false);
               AV7Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPESSOA_CODIGO"), ",", "."));
               AV13Insert_Pessoa_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PESSOA_MUNICIPIOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Pessoa";
               A34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPessoa_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A34Pessoa_Codigo != Z34Pessoa_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("pessoa:[SecurityCheckFailed value for]"+"Pessoa_Codigo:"+context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("pessoa:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A34Pessoa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode12 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode12;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound12 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0B0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PESSOA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtPessoa_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110B2 */
                           E110B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120B2 */
                           E120B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120B2 */
            E120B2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0B12( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0B12( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0B0( )
      {
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0B12( ) ;
            }
            else
            {
               CheckExtendedTable0B12( ) ;
               CloseExtendedTableCursors0B12( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0B0( )
      {
      }

      protected void E110B2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Pessoa_MunicipioCod") == 0 )
               {
                  AV13Insert_Pessoa_MunicipioCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Pessoa_MunicipioCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtPessoa_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Codigo_Visible), 5, 0)));
      }

      protected void E120B2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwpessoa.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0B12( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z35Pessoa_Nome = T000B3_A35Pessoa_Nome[0];
               Z36Pessoa_TipoPessoa = T000B3_A36Pessoa_TipoPessoa[0];
               Z37Pessoa_Docto = T000B3_A37Pessoa_Docto[0];
               Z518Pessoa_IE = T000B3_A518Pessoa_IE[0];
               Z519Pessoa_Endereco = T000B3_A519Pessoa_Endereco[0];
               Z521Pessoa_CEP = T000B3_A521Pessoa_CEP[0];
               Z522Pessoa_Telefone = T000B3_A522Pessoa_Telefone[0];
               Z523Pessoa_Fax = T000B3_A523Pessoa_Fax[0];
               Z38Pessoa_Ativo = T000B3_A38Pessoa_Ativo[0];
               Z503Pessoa_MunicipioCod = T000B3_A503Pessoa_MunicipioCod[0];
            }
            else
            {
               Z35Pessoa_Nome = A35Pessoa_Nome;
               Z36Pessoa_TipoPessoa = A36Pessoa_TipoPessoa;
               Z37Pessoa_Docto = A37Pessoa_Docto;
               Z518Pessoa_IE = A518Pessoa_IE;
               Z519Pessoa_Endereco = A519Pessoa_Endereco;
               Z521Pessoa_CEP = A521Pessoa_CEP;
               Z522Pessoa_Telefone = A522Pessoa_Telefone;
               Z523Pessoa_Fax = A523Pessoa_Fax;
               Z38Pessoa_Ativo = A38Pessoa_Ativo;
               Z503Pessoa_MunicipioCod = A503Pessoa_MunicipioCod;
            }
         }
         if ( GX_JID == -12 )
         {
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            Z35Pessoa_Nome = A35Pessoa_Nome;
            Z36Pessoa_TipoPessoa = A36Pessoa_TipoPessoa;
            Z37Pessoa_Docto = A37Pessoa_Docto;
            Z518Pessoa_IE = A518Pessoa_IE;
            Z519Pessoa_Endereco = A519Pessoa_Endereco;
            Z521Pessoa_CEP = A521Pessoa_CEP;
            Z522Pessoa_Telefone = A522Pessoa_Telefone;
            Z523Pessoa_Fax = A523Pessoa_Fax;
            Z38Pessoa_Ativo = A38Pessoa_Ativo;
            Z503Pessoa_MunicipioCod = A503Pessoa_MunicipioCod;
            Z520Pessoa_UF = A520Pessoa_UF;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAPESSOA_MUNICIPIOCOD_html0B12( ) ;
         edtPessoa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV14Pgmname = "Pessoa";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtPessoa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Pessoa_Codigo) )
         {
            A34Pessoa_Codigo = AV7Pessoa_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Pessoa_MunicipioCod) )
         {
            dynPessoa_MunicipioCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPessoa_MunicipioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynPessoa_MunicipioCod.Enabled), 5, 0)));
         }
         else
         {
            dynPessoa_MunicipioCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPessoa_MunicipioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynPessoa_MunicipioCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A38Pessoa_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A38Pessoa_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Pessoa_MunicipioCod) )
            {
               A503Pessoa_MunicipioCod = AV13Insert_Pessoa_MunicipioCod;
               n503Pessoa_MunicipioCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
            }
            else
            {
               if ( (0==A503Pessoa_MunicipioCod) )
               {
                  A503Pessoa_MunicipioCod = 0;
                  n503Pessoa_MunicipioCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
                  n503Pessoa_MunicipioCod = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
               }
            }
            /* Using cursor T000B4 */
            pr_default.execute(2, new Object[] {n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
            A520Pessoa_UF = T000B4_A520Pessoa_UF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A520Pessoa_UF", A520Pessoa_UF);
            n520Pessoa_UF = T000B4_n520Pessoa_UF[0];
            pr_default.close(2);
         }
      }

      protected void Load0B12( )
      {
         /* Using cursor T000B5 */
         pr_default.execute(3, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound12 = 1;
            A35Pessoa_Nome = T000B5_A35Pessoa_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35Pessoa_Nome", A35Pessoa_Nome);
            A36Pessoa_TipoPessoa = T000B5_A36Pessoa_TipoPessoa[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
            A37Pessoa_Docto = T000B5_A37Pessoa_Docto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37Pessoa_Docto", A37Pessoa_Docto);
            A518Pessoa_IE = T000B5_A518Pessoa_IE[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A518Pessoa_IE", A518Pessoa_IE);
            n518Pessoa_IE = T000B5_n518Pessoa_IE[0];
            A519Pessoa_Endereco = T000B5_A519Pessoa_Endereco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
            n519Pessoa_Endereco = T000B5_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = T000B5_A521Pessoa_CEP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A521Pessoa_CEP", A521Pessoa_CEP);
            n521Pessoa_CEP = T000B5_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = T000B5_A522Pessoa_Telefone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
            n522Pessoa_Telefone = T000B5_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = T000B5_A523Pessoa_Fax[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A523Pessoa_Fax", A523Pessoa_Fax);
            n523Pessoa_Fax = T000B5_n523Pessoa_Fax[0];
            A38Pessoa_Ativo = T000B5_A38Pessoa_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
            A503Pessoa_MunicipioCod = T000B5_A503Pessoa_MunicipioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
            n503Pessoa_MunicipioCod = T000B5_n503Pessoa_MunicipioCod[0];
            A520Pessoa_UF = T000B5_A520Pessoa_UF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A520Pessoa_UF", A520Pessoa_UF);
            n520Pessoa_UF = T000B5_n520Pessoa_UF[0];
            ZM0B12( -12) ;
         }
         pr_default.close(3);
         OnLoadActions0B12( ) ;
      }

      protected void OnLoadActions0B12( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Pessoa_MunicipioCod) )
         {
            A503Pessoa_MunicipioCod = AV13Insert_Pessoa_MunicipioCod;
            n503Pessoa_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
         }
         else
         {
            if ( (0==A503Pessoa_MunicipioCod) )
            {
               A503Pessoa_MunicipioCod = 0;
               n503Pessoa_MunicipioCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
               n503Pessoa_MunicipioCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "F") == 0 )
         {
            lblTextblockpessoa_docto_Caption = "CPF";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_docto_Internalname, "Caption", lblTextblockpessoa_docto_Caption);
         }
         else
         {
            if ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "J") == 0 )
            {
               lblTextblockpessoa_docto_Caption = "CNPJ";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_docto_Internalname, "Caption", lblTextblockpessoa_docto_Caption);
            }
         }
      }

      protected void CheckExtendedTable0B12( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Pessoa_MunicipioCod) )
         {
            A503Pessoa_MunicipioCod = AV13Insert_Pessoa_MunicipioCod;
            n503Pessoa_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
         }
         else
         {
            if ( (0==A503Pessoa_MunicipioCod) )
            {
               A503Pessoa_MunicipioCod = 0;
               n503Pessoa_MunicipioCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
               n503Pessoa_MunicipioCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
            }
         }
         /* Using cursor T000B6 */
         pr_default.execute(4, new Object[] {A37Pessoa_Docto, A34Pessoa_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"CPF ou CNPJ"}), 1, "PESSOA_DOCTO");
            AnyError = 1;
            GX_FocusControl = edtPessoa_Docto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(4);
         if ( ! ( ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "F") == 0 ) || ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "J") == 0 ) ) )
         {
            GX_msglist.addItem("Campo <F>�sica ou <J>ur�dica fora do intervalo", "OutOfRange", 1, "PESSOA_TIPOPESSOA");
            AnyError = 1;
            GX_FocusControl = cmbPessoa_TipoPessoa_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "F") == 0 )
         {
            lblTextblockpessoa_docto_Caption = "CPF";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_docto_Internalname, "Caption", lblTextblockpessoa_docto_Caption);
         }
         else
         {
            if ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "J") == 0 )
            {
               lblTextblockpessoa_docto_Caption = "CNPJ";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_docto_Internalname, "Caption", lblTextblockpessoa_docto_Caption);
            }
         }
         /* Using cursor T000B4 */
         pr_default.execute(2, new Object[] {n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A503Pessoa_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa_Municipio'.", "ForeignKeyNotFound", 1, "PESSOA_MUNICIPIOCOD");
               AnyError = 1;
               GX_FocusControl = dynPessoa_MunicipioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A520Pessoa_UF = T000B4_A520Pessoa_UF[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A520Pessoa_UF", A520Pessoa_UF);
         n520Pessoa_UF = T000B4_n520Pessoa_UF[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0B12( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_14( int A503Pessoa_MunicipioCod )
      {
         /* Using cursor T000B7 */
         pr_default.execute(5, new Object[] {n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A503Pessoa_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa_Municipio'.", "ForeignKeyNotFound", 1, "PESSOA_MUNICIPIOCOD");
               AnyError = 1;
               GX_FocusControl = dynPessoa_MunicipioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A520Pessoa_UF = T000B7_A520Pessoa_UF[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A520Pessoa_UF", A520Pessoa_UF);
         n520Pessoa_UF = T000B7_n520Pessoa_UF[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A520Pessoa_UF))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void GetKey0B12( )
      {
         /* Using cursor T000B8 */
         pr_default.execute(6, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound12 = 1;
         }
         else
         {
            RcdFound12 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000B3 */
         pr_default.execute(1, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0B12( 12) ;
            RcdFound12 = 1;
            A34Pessoa_Codigo = T000B3_A34Pessoa_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
            A35Pessoa_Nome = T000B3_A35Pessoa_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35Pessoa_Nome", A35Pessoa_Nome);
            A36Pessoa_TipoPessoa = T000B3_A36Pessoa_TipoPessoa[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
            A37Pessoa_Docto = T000B3_A37Pessoa_Docto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37Pessoa_Docto", A37Pessoa_Docto);
            A518Pessoa_IE = T000B3_A518Pessoa_IE[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A518Pessoa_IE", A518Pessoa_IE);
            n518Pessoa_IE = T000B3_n518Pessoa_IE[0];
            A519Pessoa_Endereco = T000B3_A519Pessoa_Endereco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
            n519Pessoa_Endereco = T000B3_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = T000B3_A521Pessoa_CEP[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A521Pessoa_CEP", A521Pessoa_CEP);
            n521Pessoa_CEP = T000B3_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = T000B3_A522Pessoa_Telefone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
            n522Pessoa_Telefone = T000B3_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = T000B3_A523Pessoa_Fax[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A523Pessoa_Fax", A523Pessoa_Fax);
            n523Pessoa_Fax = T000B3_n523Pessoa_Fax[0];
            A38Pessoa_Ativo = T000B3_A38Pessoa_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
            A503Pessoa_MunicipioCod = T000B3_A503Pessoa_MunicipioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
            n503Pessoa_MunicipioCod = T000B3_n503Pessoa_MunicipioCod[0];
            Z34Pessoa_Codigo = A34Pessoa_Codigo;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0B12( ) ;
            if ( AnyError == 1 )
            {
               RcdFound12 = 0;
               InitializeNonKey0B12( ) ;
            }
            Gx_mode = sMode12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound12 = 0;
            InitializeNonKey0B12( ) ;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0B12( ) ;
         if ( RcdFound12 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound12 = 0;
         /* Using cursor T000B9 */
         pr_default.execute(7, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000B9_A34Pessoa_Codigo[0] < A34Pessoa_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000B9_A34Pessoa_Codigo[0] > A34Pessoa_Codigo ) ) )
            {
               A34Pessoa_Codigo = T000B9_A34Pessoa_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
               RcdFound12 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound12 = 0;
         /* Using cursor T000B10 */
         pr_default.execute(8, new Object[] {A34Pessoa_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000B10_A34Pessoa_Codigo[0] > A34Pessoa_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000B10_A34Pessoa_Codigo[0] < A34Pessoa_Codigo ) ) )
            {
               A34Pessoa_Codigo = T000B10_A34Pessoa_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
               RcdFound12 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0B12( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtPessoa_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0B12( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound12 == 1 )
            {
               if ( A34Pessoa_Codigo != Z34Pessoa_Codigo )
               {
                  A34Pessoa_Codigo = Z34Pessoa_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PESSOA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtPessoa_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtPessoa_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0B12( ) ;
                  GX_FocusControl = edtPessoa_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A34Pessoa_Codigo != Z34Pessoa_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtPessoa_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0B12( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PESSOA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtPessoa_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtPessoa_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0B12( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A34Pessoa_Codigo != Z34Pessoa_Codigo )
         {
            A34Pessoa_Codigo = Z34Pessoa_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PESSOA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPessoa_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtPessoa_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0B12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000B2 */
            pr_default.execute(0, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Pessoa"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z35Pessoa_Nome, T000B2_A35Pessoa_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z36Pessoa_TipoPessoa, T000B2_A36Pessoa_TipoPessoa[0]) != 0 ) || ( StringUtil.StrCmp(Z37Pessoa_Docto, T000B2_A37Pessoa_Docto[0]) != 0 ) || ( StringUtil.StrCmp(Z518Pessoa_IE, T000B2_A518Pessoa_IE[0]) != 0 ) || ( StringUtil.StrCmp(Z519Pessoa_Endereco, T000B2_A519Pessoa_Endereco[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z521Pessoa_CEP, T000B2_A521Pessoa_CEP[0]) != 0 ) || ( StringUtil.StrCmp(Z522Pessoa_Telefone, T000B2_A522Pessoa_Telefone[0]) != 0 ) || ( StringUtil.StrCmp(Z523Pessoa_Fax, T000B2_A523Pessoa_Fax[0]) != 0 ) || ( Z38Pessoa_Ativo != T000B2_A38Pessoa_Ativo[0] ) || ( Z503Pessoa_MunicipioCod != T000B2_A503Pessoa_MunicipioCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z35Pessoa_Nome, T000B2_A35Pessoa_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z35Pessoa_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A35Pessoa_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z36Pessoa_TipoPessoa, T000B2_A36Pessoa_TipoPessoa[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_TipoPessoa");
                  GXUtil.WriteLogRaw("Old: ",Z36Pessoa_TipoPessoa);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A36Pessoa_TipoPessoa[0]);
               }
               if ( StringUtil.StrCmp(Z37Pessoa_Docto, T000B2_A37Pessoa_Docto[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_Docto");
                  GXUtil.WriteLogRaw("Old: ",Z37Pessoa_Docto);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A37Pessoa_Docto[0]);
               }
               if ( StringUtil.StrCmp(Z518Pessoa_IE, T000B2_A518Pessoa_IE[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_IE");
                  GXUtil.WriteLogRaw("Old: ",Z518Pessoa_IE);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A518Pessoa_IE[0]);
               }
               if ( StringUtil.StrCmp(Z519Pessoa_Endereco, T000B2_A519Pessoa_Endereco[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_Endereco");
                  GXUtil.WriteLogRaw("Old: ",Z519Pessoa_Endereco);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A519Pessoa_Endereco[0]);
               }
               if ( StringUtil.StrCmp(Z521Pessoa_CEP, T000B2_A521Pessoa_CEP[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_CEP");
                  GXUtil.WriteLogRaw("Old: ",Z521Pessoa_CEP);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A521Pessoa_CEP[0]);
               }
               if ( StringUtil.StrCmp(Z522Pessoa_Telefone, T000B2_A522Pessoa_Telefone[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_Telefone");
                  GXUtil.WriteLogRaw("Old: ",Z522Pessoa_Telefone);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A522Pessoa_Telefone[0]);
               }
               if ( StringUtil.StrCmp(Z523Pessoa_Fax, T000B2_A523Pessoa_Fax[0]) != 0 )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_Fax");
                  GXUtil.WriteLogRaw("Old: ",Z523Pessoa_Fax);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A523Pessoa_Fax[0]);
               }
               if ( Z38Pessoa_Ativo != T000B2_A38Pessoa_Ativo[0] )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z38Pessoa_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A38Pessoa_Ativo[0]);
               }
               if ( Z503Pessoa_MunicipioCod != T000B2_A503Pessoa_MunicipioCod[0] )
               {
                  GXUtil.WriteLog("pessoa:[seudo value changed for attri]"+"Pessoa_MunicipioCod");
                  GXUtil.WriteLogRaw("Old: ",Z503Pessoa_MunicipioCod);
                  GXUtil.WriteLogRaw("Current: ",T000B2_A503Pessoa_MunicipioCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Pessoa"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0B12( )
      {
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0B12( 0) ;
            CheckOptimisticConcurrency0B12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0B12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0B12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000B11 */
                     pr_default.execute(9, new Object[] {A35Pessoa_Nome, A36Pessoa_TipoPessoa, A37Pessoa_Docto, n518Pessoa_IE, A518Pessoa_IE, n519Pessoa_Endereco, A519Pessoa_Endereco, n521Pessoa_CEP, A521Pessoa_CEP, n522Pessoa_Telefone, A522Pessoa_Telefone, n523Pessoa_Fax, A523Pessoa_Fax, A38Pessoa_Ativo, n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
                     A34Pessoa_Codigo = T000B11_A34Pessoa_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Pessoa") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0B0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0B12( ) ;
            }
            EndLevel0B12( ) ;
         }
         CloseExtendedTableCursors0B12( ) ;
      }

      protected void Update0B12( )
      {
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0B12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0B12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0B12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000B12 */
                     pr_default.execute(10, new Object[] {A35Pessoa_Nome, A36Pessoa_TipoPessoa, A37Pessoa_Docto, n518Pessoa_IE, A518Pessoa_IE, n519Pessoa_Endereco, A519Pessoa_Endereco, n521Pessoa_CEP, A521Pessoa_CEP, n522Pessoa_Telefone, A522Pessoa_Telefone, n523Pessoa_Fax, A523Pessoa_Fax, A38Pessoa_Ativo, n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod, A34Pessoa_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Pessoa") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Pessoa"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0B12( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0B12( ) ;
         }
         CloseExtendedTableCursors0B12( ) ;
      }

      protected void DeferredUpdate0B12( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0B12( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0B12( ) ;
            AfterConfirm0B12( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0B12( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000B13 */
                  pr_default.execute(11, new Object[] {A34Pessoa_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("Pessoa") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode12 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0B12( ) ;
         Gx_mode = sMode12;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0B12( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "F") == 0 )
            {
               lblTextblockpessoa_docto_Caption = "CPF";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_docto_Internalname, "Caption", lblTextblockpessoa_docto_Caption);
            }
            else
            {
               if ( StringUtil.StrCmp(A36Pessoa_TipoPessoa, "J") == 0 )
               {
                  lblTextblockpessoa_docto_Caption = "CNPJ";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockpessoa_docto_Internalname, "Caption", lblTextblockpessoa_docto_Caption);
               }
            }
            /* Using cursor T000B14 */
            pr_default.execute(12, new Object[] {n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
            A520Pessoa_UF = T000B14_A520Pessoa_UF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A520Pessoa_UF", A520Pessoa_UF);
            n520Pessoa_UF = T000B14_n520Pessoa_UF[0];
            pr_default.close(12);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000B15 */
            pr_default.execute(13, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Certificados"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T000B16 */
            pr_default.execute(14, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratante"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T000B17 */
            pr_default.execute(15, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Ocorrencia Notificacao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor T000B18 */
            pr_default.execute(16, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T000B19 */
            pr_default.execute(17, new Object[] {A34Pessoa_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contratada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
         }
      }

      protected void EndLevel0B12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0B12( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores( "Pessoa");
            if ( AnyError == 0 )
            {
               ConfirmValues0B0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores( "Pessoa");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0B12( )
      {
         /* Scan By routine */
         /* Using cursor T000B20 */
         pr_default.execute(18);
         RcdFound12 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound12 = 1;
            A34Pessoa_Codigo = T000B20_A34Pessoa_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0B12( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound12 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound12 = 1;
            A34Pessoa_Codigo = T000B20_A34Pessoa_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0B12( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm0B12( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0B12( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0B12( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0B12( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0B12( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0B12( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0B12( )
      {
         edtPessoa_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Nome_Enabled), 5, 0)));
         cmbPessoa_TipoPessoa.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPessoa_TipoPessoa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPessoa_TipoPessoa.Enabled), 5, 0)));
         edtPessoa_Docto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Docto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Docto_Enabled), 5, 0)));
         edtPessoa_IE_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_IE_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_IE_Enabled), 5, 0)));
         edtPessoa_Endereco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Endereco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Endereco_Enabled), 5, 0)));
         dynPessoa_MunicipioCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynPessoa_MunicipioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynPessoa_MunicipioCod.Enabled), 5, 0)));
         edtPessoa_UF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_UF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_UF_Enabled), 5, 0)));
         edtPessoa_CEP_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_CEP_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_CEP_Enabled), 5, 0)));
         edtPessoa_Telefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Telefone_Enabled), 5, 0)));
         edtPessoa_Fax_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Fax_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Fax_Enabled), 5, 0)));
         chkPessoa_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPessoa_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkPessoa_Ativo.Enabled), 5, 0)));
         edtPessoa_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPessoa_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0B0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117153747");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("pessoa.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Pessoa_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z34Pessoa_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z34Pessoa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z35Pessoa_Nome", StringUtil.RTrim( Z35Pessoa_Nome));
         GxWebStd.gx_hidden_field( context, "Z36Pessoa_TipoPessoa", StringUtil.RTrim( Z36Pessoa_TipoPessoa));
         GxWebStd.gx_hidden_field( context, "Z37Pessoa_Docto", Z37Pessoa_Docto);
         GxWebStd.gx_hidden_field( context, "Z518Pessoa_IE", StringUtil.RTrim( Z518Pessoa_IE));
         GxWebStd.gx_hidden_field( context, "Z519Pessoa_Endereco", Z519Pessoa_Endereco);
         GxWebStd.gx_hidden_field( context, "Z521Pessoa_CEP", StringUtil.RTrim( Z521Pessoa_CEP));
         GxWebStd.gx_hidden_field( context, "Z522Pessoa_Telefone", StringUtil.RTrim( Z522Pessoa_Telefone));
         GxWebStd.gx_hidden_field( context, "Z523Pessoa_Fax", StringUtil.RTrim( Z523Pessoa_Fax));
         GxWebStd.gx_boolean_hidden_field( context, "Z38Pessoa_Ativo", Z38Pessoa_Ativo);
         GxWebStd.gx_hidden_field( context, "Z503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z503Pessoa_MunicipioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A503Pessoa_MunicipioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPESSOA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Pessoa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PESSOA_MUNICIPIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Pessoa_MunicipioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPESSOA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Pessoa_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Pessoa";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("pessoa:[SendSecurityCheck value for]"+"Pessoa_Codigo:"+context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("pessoa:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("pessoa.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Pessoa_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Pessoa" ;
      }

      public override String GetPgmdesc( )
      {
         return "Pessoas" ;
      }

      protected void InitializeNonKey0B12( )
      {
         A503Pessoa_MunicipioCod = 0;
         n503Pessoa_MunicipioCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
         n503Pessoa_MunicipioCod = ((0==A503Pessoa_MunicipioCod) ? true : false);
         A35Pessoa_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35Pessoa_Nome", A35Pessoa_Nome);
         A36Pessoa_TipoPessoa = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
         A37Pessoa_Docto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37Pessoa_Docto", A37Pessoa_Docto);
         A518Pessoa_IE = "";
         n518Pessoa_IE = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A518Pessoa_IE", A518Pessoa_IE);
         n518Pessoa_IE = (String.IsNullOrEmpty(StringUtil.RTrim( A518Pessoa_IE)) ? true : false);
         A519Pessoa_Endereco = "";
         n519Pessoa_Endereco = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
         n519Pessoa_Endereco = (String.IsNullOrEmpty(StringUtil.RTrim( A519Pessoa_Endereco)) ? true : false);
         A520Pessoa_UF = "";
         n520Pessoa_UF = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A520Pessoa_UF", A520Pessoa_UF);
         A521Pessoa_CEP = "";
         n521Pessoa_CEP = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A521Pessoa_CEP", A521Pessoa_CEP);
         n521Pessoa_CEP = (String.IsNullOrEmpty(StringUtil.RTrim( A521Pessoa_CEP)) ? true : false);
         A522Pessoa_Telefone = "";
         n522Pessoa_Telefone = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
         n522Pessoa_Telefone = (String.IsNullOrEmpty(StringUtil.RTrim( A522Pessoa_Telefone)) ? true : false);
         A523Pessoa_Fax = "";
         n523Pessoa_Fax = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A523Pessoa_Fax", A523Pessoa_Fax);
         n523Pessoa_Fax = (String.IsNullOrEmpty(StringUtil.RTrim( A523Pessoa_Fax)) ? true : false);
         A38Pessoa_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
         Z35Pessoa_Nome = "";
         Z36Pessoa_TipoPessoa = "";
         Z37Pessoa_Docto = "";
         Z518Pessoa_IE = "";
         Z519Pessoa_Endereco = "";
         Z521Pessoa_CEP = "";
         Z522Pessoa_Telefone = "";
         Z523Pessoa_Fax = "";
         Z38Pessoa_Ativo = false;
         Z503Pessoa_MunicipioCod = 0;
      }

      protected void InitAll0B12( )
      {
         A34Pessoa_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
         InitializeNonKey0B12( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A38Pessoa_Ativo = i38Pessoa_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117153778");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("pessoa.js", "?20203117153778");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblPessoatitle_Internalname = "PESSOATITLE";
         lblTextblockpessoa_nome_Internalname = "TEXTBLOCKPESSOA_NOME";
         edtPessoa_Nome_Internalname = "PESSOA_NOME";
         lblTextblockpessoa_tipopessoa_Internalname = "TEXTBLOCKPESSOA_TIPOPESSOA";
         cmbPessoa_TipoPessoa_Internalname = "PESSOA_TIPOPESSOA";
         lblTextblockpessoa_docto_Internalname = "TEXTBLOCKPESSOA_DOCTO";
         edtPessoa_Docto_Internalname = "PESSOA_DOCTO";
         lblTextblockpessoa_ie_Internalname = "TEXTBLOCKPESSOA_IE";
         edtPessoa_IE_Internalname = "PESSOA_IE";
         lblTextblockpessoa_endereco_Internalname = "TEXTBLOCKPESSOA_ENDERECO";
         edtPessoa_Endereco_Internalname = "PESSOA_ENDERECO";
         lblTextblockpessoa_municipiocod_Internalname = "TEXTBLOCKPESSOA_MUNICIPIOCOD";
         dynPessoa_MunicipioCod_Internalname = "PESSOA_MUNICIPIOCOD";
         lblTextblockpessoa_uf_Internalname = "TEXTBLOCKPESSOA_UF";
         edtPessoa_UF_Internalname = "PESSOA_UF";
         lblTextblockpessoa_cep_Internalname = "TEXTBLOCKPESSOA_CEP";
         edtPessoa_CEP_Internalname = "PESSOA_CEP";
         lblTextblockpessoa_telefone_Internalname = "TEXTBLOCKPESSOA_TELEFONE";
         edtPessoa_Telefone_Internalname = "PESSOA_TELEFONE";
         lblTextblockpessoa_fax_Internalname = "TEXTBLOCKPESSOA_FAX";
         edtPessoa_Fax_Internalname = "PESSOA_FAX";
         lblTextblockpessoa_ativo_Internalname = "TEXTBLOCKPESSOA_ATIVO";
         chkPessoa_Ativo_Internalname = "PESSOA_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtPessoa_Codigo_Internalname = "PESSOA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Pessoas";
         chkPessoa_Ativo.Enabled = 1;
         edtPessoa_Fax_Jsonclick = "";
         edtPessoa_Fax_Enabled = 1;
         edtPessoa_Telefone_Jsonclick = "";
         edtPessoa_Telefone_Enabled = 1;
         edtPessoa_CEP_Jsonclick = "";
         edtPessoa_CEP_Enabled = 1;
         edtPessoa_UF_Jsonclick = "";
         edtPessoa_UF_Enabled = 0;
         dynPessoa_MunicipioCod_Jsonclick = "";
         dynPessoa_MunicipioCod.Enabled = 1;
         edtPessoa_Endereco_Jsonclick = "";
         edtPessoa_Endereco_Enabled = 1;
         edtPessoa_IE_Jsonclick = "";
         edtPessoa_IE_Enabled = 1;
         edtPessoa_Docto_Jsonclick = "";
         edtPessoa_Docto_Enabled = 1;
         lblTextblockpessoa_docto_Caption = "Documento";
         cmbPessoa_TipoPessoa_Jsonclick = "";
         cmbPessoa_TipoPessoa.Enabled = 1;
         edtPessoa_Nome_Jsonclick = "";
         edtPessoa_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtPessoa_Codigo_Jsonclick = "";
         edtPessoa_Codigo_Enabled = 0;
         edtPessoa_Codigo_Visible = 1;
         chkPessoa_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPESSOA_MUNICIPIOCOD0B12( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPESSOA_MUNICIPIOCOD_data0B12( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPESSOA_MUNICIPIOCOD_html0B12( )
      {
         int gxdynajaxvalue ;
         GXDLAPESSOA_MUNICIPIOCOD_data0B12( ) ;
         gxdynajaxindex = 1;
         dynPessoa_MunicipioCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynPessoa_MunicipioCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPESSOA_MUNICIPIOCOD_data0B12( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000B21 */
         pr_default.execute(19);
         while ( (pr_default.getStatus(19) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000B21_A503Pessoa_MunicipioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000B21_A26Municipio_Nome[0]));
            pr_default.readNext(19);
         }
         pr_default.close(19);
      }

      public void Valid_Pessoa_docto( String GX_Parm1 ,
                                      int GX_Parm2 )
      {
         A37Pessoa_Docto = GX_Parm1;
         A34Pessoa_Codigo = GX_Parm2;
         /* Using cursor T000B22 */
         pr_default.execute(20, new Object[] {A37Pessoa_Docto, A34Pessoa_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"CPF ou CNPJ"}), 1, "PESSOA_DOCTO");
            AnyError = 1;
            GX_FocusControl = edtPessoa_Docto_Internalname;
         }
         pr_default.close(20);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Pessoa_municipiocod( int GX_Parm1 ,
                                             GXCombobox dynGX_Parm2 ,
                                             String GX_Parm3 )
      {
         AV13Insert_Pessoa_MunicipioCod = GX_Parm1;
         dynPessoa_MunicipioCod = dynGX_Parm2;
         A503Pessoa_MunicipioCod = (int)(NumberUtil.Val( dynPessoa_MunicipioCod.CurrentValue, "."));
         n503Pessoa_MunicipioCod = false;
         A520Pessoa_UF = GX_Parm3;
         n520Pessoa_UF = false;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Pessoa_MunicipioCod) )
         {
            A503Pessoa_MunicipioCod = AV13Insert_Pessoa_MunicipioCod;
            n503Pessoa_MunicipioCod = false;
         }
         else
         {
            if ( (0==A503Pessoa_MunicipioCod) )
            {
               A503Pessoa_MunicipioCod = 0;
               n503Pessoa_MunicipioCod = false;
               n503Pessoa_MunicipioCod = true;
            }
         }
         /* Using cursor T000B23 */
         pr_default.execute(21, new Object[] {n503Pessoa_MunicipioCod, A503Pessoa_MunicipioCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            if ( ! ( (0==A503Pessoa_MunicipioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa_Municipio'.", "ForeignKeyNotFound", 1, "PESSOA_MUNICIPIOCOD");
               AnyError = 1;
               GX_FocusControl = dynPessoa_MunicipioCod_Internalname;
            }
         }
         A520Pessoa_UF = T000B23_A520Pessoa_UF[0];
         n520Pessoa_UF = T000B23_n520Pessoa_UF[0];
         pr_default.close(21);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A520Pessoa_UF = "";
            n520Pessoa_UF = false;
         }
         GXAPESSOA_MUNICIPIOCOD_html0B12( ) ;
         dynPessoa_MunicipioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0));
         if ( dynPessoa_MunicipioCod.ItemCount > 0 )
         {
            A503Pessoa_MunicipioCod = (int)(NumberUtil.Val( dynPessoa_MunicipioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0))), "."));
            n503Pessoa_MunicipioCod = false;
         }
         dynPessoa_MunicipioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0));
         isValidOutput.Add(dynPessoa_MunicipioCod);
         isValidOutput.Add(StringUtil.RTrim( A520Pessoa_UF));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Pessoa_Codigo',fld:'vPESSOA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120B2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(21);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z35Pessoa_Nome = "";
         Z36Pessoa_TipoPessoa = "";
         Z37Pessoa_Docto = "";
         Z518Pessoa_IE = "";
         Z519Pessoa_Endereco = "";
         Z521Pessoa_CEP = "";
         Z522Pessoa_Telefone = "";
         Z523Pessoa_Fax = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A36Pessoa_TipoPessoa = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblPessoatitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockpessoa_nome_Jsonclick = "";
         A35Pessoa_Nome = "";
         lblTextblockpessoa_tipopessoa_Jsonclick = "";
         lblTextblockpessoa_docto_Jsonclick = "";
         A37Pessoa_Docto = "";
         lblTextblockpessoa_ie_Jsonclick = "";
         A518Pessoa_IE = "";
         lblTextblockpessoa_endereco_Jsonclick = "";
         A519Pessoa_Endereco = "";
         lblTextblockpessoa_municipiocod_Jsonclick = "";
         lblTextblockpessoa_uf_Jsonclick = "";
         A520Pessoa_UF = "";
         lblTextblockpessoa_cep_Jsonclick = "";
         A521Pessoa_CEP = "";
         lblTextblockpessoa_telefone_Jsonclick = "";
         A522Pessoa_Telefone = "";
         lblTextblockpessoa_fax_Jsonclick = "";
         A523Pessoa_Fax = "";
         lblTextblockpessoa_ativo_Jsonclick = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode12 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z520Pessoa_UF = "";
         T000B4_A520Pessoa_UF = new String[] {""} ;
         T000B4_n520Pessoa_UF = new bool[] {false} ;
         T000B5_A34Pessoa_Codigo = new int[1] ;
         T000B5_A35Pessoa_Nome = new String[] {""} ;
         T000B5_A36Pessoa_TipoPessoa = new String[] {""} ;
         T000B5_A37Pessoa_Docto = new String[] {""} ;
         T000B5_A518Pessoa_IE = new String[] {""} ;
         T000B5_n518Pessoa_IE = new bool[] {false} ;
         T000B5_A519Pessoa_Endereco = new String[] {""} ;
         T000B5_n519Pessoa_Endereco = new bool[] {false} ;
         T000B5_A521Pessoa_CEP = new String[] {""} ;
         T000B5_n521Pessoa_CEP = new bool[] {false} ;
         T000B5_A522Pessoa_Telefone = new String[] {""} ;
         T000B5_n522Pessoa_Telefone = new bool[] {false} ;
         T000B5_A523Pessoa_Fax = new String[] {""} ;
         T000B5_n523Pessoa_Fax = new bool[] {false} ;
         T000B5_A38Pessoa_Ativo = new bool[] {false} ;
         T000B5_A503Pessoa_MunicipioCod = new int[1] ;
         T000B5_n503Pessoa_MunicipioCod = new bool[] {false} ;
         T000B5_A520Pessoa_UF = new String[] {""} ;
         T000B5_n520Pessoa_UF = new bool[] {false} ;
         T000B6_A37Pessoa_Docto = new String[] {""} ;
         T000B7_A520Pessoa_UF = new String[] {""} ;
         T000B7_n520Pessoa_UF = new bool[] {false} ;
         T000B8_A34Pessoa_Codigo = new int[1] ;
         T000B3_A34Pessoa_Codigo = new int[1] ;
         T000B3_A35Pessoa_Nome = new String[] {""} ;
         T000B3_A36Pessoa_TipoPessoa = new String[] {""} ;
         T000B3_A37Pessoa_Docto = new String[] {""} ;
         T000B3_A518Pessoa_IE = new String[] {""} ;
         T000B3_n518Pessoa_IE = new bool[] {false} ;
         T000B3_A519Pessoa_Endereco = new String[] {""} ;
         T000B3_n519Pessoa_Endereco = new bool[] {false} ;
         T000B3_A521Pessoa_CEP = new String[] {""} ;
         T000B3_n521Pessoa_CEP = new bool[] {false} ;
         T000B3_A522Pessoa_Telefone = new String[] {""} ;
         T000B3_n522Pessoa_Telefone = new bool[] {false} ;
         T000B3_A523Pessoa_Fax = new String[] {""} ;
         T000B3_n523Pessoa_Fax = new bool[] {false} ;
         T000B3_A38Pessoa_Ativo = new bool[] {false} ;
         T000B3_A503Pessoa_MunicipioCod = new int[1] ;
         T000B3_n503Pessoa_MunicipioCod = new bool[] {false} ;
         T000B9_A34Pessoa_Codigo = new int[1] ;
         T000B10_A34Pessoa_Codigo = new int[1] ;
         T000B2_A34Pessoa_Codigo = new int[1] ;
         T000B2_A35Pessoa_Nome = new String[] {""} ;
         T000B2_A36Pessoa_TipoPessoa = new String[] {""} ;
         T000B2_A37Pessoa_Docto = new String[] {""} ;
         T000B2_A518Pessoa_IE = new String[] {""} ;
         T000B2_n518Pessoa_IE = new bool[] {false} ;
         T000B2_A519Pessoa_Endereco = new String[] {""} ;
         T000B2_n519Pessoa_Endereco = new bool[] {false} ;
         T000B2_A521Pessoa_CEP = new String[] {""} ;
         T000B2_n521Pessoa_CEP = new bool[] {false} ;
         T000B2_A522Pessoa_Telefone = new String[] {""} ;
         T000B2_n522Pessoa_Telefone = new bool[] {false} ;
         T000B2_A523Pessoa_Fax = new String[] {""} ;
         T000B2_n523Pessoa_Fax = new bool[] {false} ;
         T000B2_A38Pessoa_Ativo = new bool[] {false} ;
         T000B2_A503Pessoa_MunicipioCod = new int[1] ;
         T000B2_n503Pessoa_MunicipioCod = new bool[] {false} ;
         T000B11_A34Pessoa_Codigo = new int[1] ;
         T000B14_A520Pessoa_UF = new String[] {""} ;
         T000B14_n520Pessoa_UF = new bool[] {false} ;
         T000B15_A2010PessoaCertificado_Codigo = new int[1] ;
         T000B16_A29Contratante_Codigo = new int[1] ;
         T000B17_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         T000B18_A1Usuario_Codigo = new int[1] ;
         T000B19_A39Contratada_Codigo = new int[1] ;
         T000B20_A34Pessoa_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000B21_A503Pessoa_MunicipioCod = new int[1] ;
         T000B21_n503Pessoa_MunicipioCod = new bool[] {false} ;
         T000B21_A26Municipio_Nome = new String[] {""} ;
         T000B21_n26Municipio_Nome = new bool[] {false} ;
         T000B22_A37Pessoa_Docto = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T000B23_A520Pessoa_UF = new String[] {""} ;
         T000B23_n520Pessoa_UF = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pessoa__default(),
            new Object[][] {
                new Object[] {
               T000B2_A34Pessoa_Codigo, T000B2_A35Pessoa_Nome, T000B2_A36Pessoa_TipoPessoa, T000B2_A37Pessoa_Docto, T000B2_A518Pessoa_IE, T000B2_n518Pessoa_IE, T000B2_A519Pessoa_Endereco, T000B2_n519Pessoa_Endereco, T000B2_A521Pessoa_CEP, T000B2_n521Pessoa_CEP,
               T000B2_A522Pessoa_Telefone, T000B2_n522Pessoa_Telefone, T000B2_A523Pessoa_Fax, T000B2_n523Pessoa_Fax, T000B2_A38Pessoa_Ativo, T000B2_A503Pessoa_MunicipioCod, T000B2_n503Pessoa_MunicipioCod
               }
               , new Object[] {
               T000B3_A34Pessoa_Codigo, T000B3_A35Pessoa_Nome, T000B3_A36Pessoa_TipoPessoa, T000B3_A37Pessoa_Docto, T000B3_A518Pessoa_IE, T000B3_n518Pessoa_IE, T000B3_A519Pessoa_Endereco, T000B3_n519Pessoa_Endereco, T000B3_A521Pessoa_CEP, T000B3_n521Pessoa_CEP,
               T000B3_A522Pessoa_Telefone, T000B3_n522Pessoa_Telefone, T000B3_A523Pessoa_Fax, T000B3_n523Pessoa_Fax, T000B3_A38Pessoa_Ativo, T000B3_A503Pessoa_MunicipioCod, T000B3_n503Pessoa_MunicipioCod
               }
               , new Object[] {
               T000B4_A520Pessoa_UF, T000B4_n520Pessoa_UF
               }
               , new Object[] {
               T000B5_A34Pessoa_Codigo, T000B5_A35Pessoa_Nome, T000B5_A36Pessoa_TipoPessoa, T000B5_A37Pessoa_Docto, T000B5_A518Pessoa_IE, T000B5_n518Pessoa_IE, T000B5_A519Pessoa_Endereco, T000B5_n519Pessoa_Endereco, T000B5_A521Pessoa_CEP, T000B5_n521Pessoa_CEP,
               T000B5_A522Pessoa_Telefone, T000B5_n522Pessoa_Telefone, T000B5_A523Pessoa_Fax, T000B5_n523Pessoa_Fax, T000B5_A38Pessoa_Ativo, T000B5_A503Pessoa_MunicipioCod, T000B5_n503Pessoa_MunicipioCod, T000B5_A520Pessoa_UF, T000B5_n520Pessoa_UF
               }
               , new Object[] {
               T000B6_A37Pessoa_Docto
               }
               , new Object[] {
               T000B7_A520Pessoa_UF, T000B7_n520Pessoa_UF
               }
               , new Object[] {
               T000B8_A34Pessoa_Codigo
               }
               , new Object[] {
               T000B9_A34Pessoa_Codigo
               }
               , new Object[] {
               T000B10_A34Pessoa_Codigo
               }
               , new Object[] {
               T000B11_A34Pessoa_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000B14_A520Pessoa_UF, T000B14_n520Pessoa_UF
               }
               , new Object[] {
               T000B15_A2010PessoaCertificado_Codigo
               }
               , new Object[] {
               T000B16_A29Contratante_Codigo
               }
               , new Object[] {
               T000B17_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               T000B18_A1Usuario_Codigo
               }
               , new Object[] {
               T000B19_A39Contratada_Codigo
               }
               , new Object[] {
               T000B20_A34Pessoa_Codigo
               }
               , new Object[] {
               T000B21_A503Pessoa_MunicipioCod, T000B21_A26Municipio_Nome, T000B21_n26Municipio_Nome
               }
               , new Object[] {
               T000B22_A37Pessoa_Docto
               }
               , new Object[] {
               T000B23_A520Pessoa_UF, T000B23_n520Pessoa_UF
               }
            }
         );
         Z38Pessoa_Ativo = true;
         A38Pessoa_Ativo = true;
         i38Pessoa_Ativo = true;
         AV14Pgmname = "Pessoa";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound12 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Pessoa_Codigo ;
      private int Z34Pessoa_Codigo ;
      private int Z503Pessoa_MunicipioCod ;
      private int N503Pessoa_MunicipioCod ;
      private int A503Pessoa_MunicipioCod ;
      private int AV7Pessoa_Codigo ;
      private int trnEnded ;
      private int A34Pessoa_Codigo ;
      private int edtPessoa_Codigo_Enabled ;
      private int edtPessoa_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtPessoa_Nome_Enabled ;
      private int edtPessoa_Docto_Enabled ;
      private int edtPessoa_IE_Enabled ;
      private int edtPessoa_Endereco_Enabled ;
      private int edtPessoa_UF_Enabled ;
      private int edtPessoa_CEP_Enabled ;
      private int edtPessoa_Telefone_Enabled ;
      private int edtPessoa_Fax_Enabled ;
      private int AV13Insert_Pessoa_MunicipioCod ;
      private int AV15GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z35Pessoa_Nome ;
      private String Z36Pessoa_TipoPessoa ;
      private String Z518Pessoa_IE ;
      private String Z521Pessoa_CEP ;
      private String Z522Pessoa_Telefone ;
      private String Z523Pessoa_Fax ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A36Pessoa_TipoPessoa ;
      private String chkPessoa_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtPessoa_Nome_Internalname ;
      private String edtPessoa_Codigo_Internalname ;
      private String edtPessoa_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblPessoatitle_Internalname ;
      private String lblPessoatitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockpessoa_nome_Internalname ;
      private String lblTextblockpessoa_nome_Jsonclick ;
      private String A35Pessoa_Nome ;
      private String edtPessoa_Nome_Jsonclick ;
      private String lblTextblockpessoa_tipopessoa_Internalname ;
      private String lblTextblockpessoa_tipopessoa_Jsonclick ;
      private String cmbPessoa_TipoPessoa_Internalname ;
      private String cmbPessoa_TipoPessoa_Jsonclick ;
      private String lblTextblockpessoa_docto_Internalname ;
      private String lblTextblockpessoa_docto_Caption ;
      private String lblTextblockpessoa_docto_Jsonclick ;
      private String edtPessoa_Docto_Internalname ;
      private String edtPessoa_Docto_Jsonclick ;
      private String lblTextblockpessoa_ie_Internalname ;
      private String lblTextblockpessoa_ie_Jsonclick ;
      private String edtPessoa_IE_Internalname ;
      private String A518Pessoa_IE ;
      private String edtPessoa_IE_Jsonclick ;
      private String lblTextblockpessoa_endereco_Internalname ;
      private String lblTextblockpessoa_endereco_Jsonclick ;
      private String edtPessoa_Endereco_Internalname ;
      private String edtPessoa_Endereco_Jsonclick ;
      private String lblTextblockpessoa_municipiocod_Internalname ;
      private String lblTextblockpessoa_municipiocod_Jsonclick ;
      private String dynPessoa_MunicipioCod_Internalname ;
      private String dynPessoa_MunicipioCod_Jsonclick ;
      private String lblTextblockpessoa_uf_Internalname ;
      private String lblTextblockpessoa_uf_Jsonclick ;
      private String edtPessoa_UF_Internalname ;
      private String A520Pessoa_UF ;
      private String edtPessoa_UF_Jsonclick ;
      private String lblTextblockpessoa_cep_Internalname ;
      private String lblTextblockpessoa_cep_Jsonclick ;
      private String edtPessoa_CEP_Internalname ;
      private String A521Pessoa_CEP ;
      private String edtPessoa_CEP_Jsonclick ;
      private String lblTextblockpessoa_telefone_Internalname ;
      private String lblTextblockpessoa_telefone_Jsonclick ;
      private String edtPessoa_Telefone_Internalname ;
      private String A522Pessoa_Telefone ;
      private String edtPessoa_Telefone_Jsonclick ;
      private String lblTextblockpessoa_fax_Internalname ;
      private String lblTextblockpessoa_fax_Jsonclick ;
      private String edtPessoa_Fax_Internalname ;
      private String A523Pessoa_Fax ;
      private String edtPessoa_Fax_Jsonclick ;
      private String lblTextblockpessoa_ativo_Internalname ;
      private String lblTextblockpessoa_ativo_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode12 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z520Pessoa_UF ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool Z38Pessoa_Ativo ;
      private bool entryPointCalled ;
      private bool n503Pessoa_MunicipioCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A38Pessoa_Ativo ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n520Pessoa_UF ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i38Pessoa_Ativo ;
      private String Z37Pessoa_Docto ;
      private String Z519Pessoa_Endereco ;
      private String A37Pessoa_Docto ;
      private String A519Pessoa_Endereco ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbPessoa_TipoPessoa ;
      private GXCombobox dynPessoa_MunicipioCod ;
      private GXCheckbox chkPessoa_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T000B4_A520Pessoa_UF ;
      private bool[] T000B4_n520Pessoa_UF ;
      private int[] T000B5_A34Pessoa_Codigo ;
      private String[] T000B5_A35Pessoa_Nome ;
      private String[] T000B5_A36Pessoa_TipoPessoa ;
      private String[] T000B5_A37Pessoa_Docto ;
      private String[] T000B5_A518Pessoa_IE ;
      private bool[] T000B5_n518Pessoa_IE ;
      private String[] T000B5_A519Pessoa_Endereco ;
      private bool[] T000B5_n519Pessoa_Endereco ;
      private String[] T000B5_A521Pessoa_CEP ;
      private bool[] T000B5_n521Pessoa_CEP ;
      private String[] T000B5_A522Pessoa_Telefone ;
      private bool[] T000B5_n522Pessoa_Telefone ;
      private String[] T000B5_A523Pessoa_Fax ;
      private bool[] T000B5_n523Pessoa_Fax ;
      private bool[] T000B5_A38Pessoa_Ativo ;
      private int[] T000B5_A503Pessoa_MunicipioCod ;
      private bool[] T000B5_n503Pessoa_MunicipioCod ;
      private String[] T000B5_A520Pessoa_UF ;
      private bool[] T000B5_n520Pessoa_UF ;
      private String[] T000B6_A37Pessoa_Docto ;
      private String[] T000B7_A520Pessoa_UF ;
      private bool[] T000B7_n520Pessoa_UF ;
      private int[] T000B8_A34Pessoa_Codigo ;
      private int[] T000B3_A34Pessoa_Codigo ;
      private String[] T000B3_A35Pessoa_Nome ;
      private String[] T000B3_A36Pessoa_TipoPessoa ;
      private String[] T000B3_A37Pessoa_Docto ;
      private String[] T000B3_A518Pessoa_IE ;
      private bool[] T000B3_n518Pessoa_IE ;
      private String[] T000B3_A519Pessoa_Endereco ;
      private bool[] T000B3_n519Pessoa_Endereco ;
      private String[] T000B3_A521Pessoa_CEP ;
      private bool[] T000B3_n521Pessoa_CEP ;
      private String[] T000B3_A522Pessoa_Telefone ;
      private bool[] T000B3_n522Pessoa_Telefone ;
      private String[] T000B3_A523Pessoa_Fax ;
      private bool[] T000B3_n523Pessoa_Fax ;
      private bool[] T000B3_A38Pessoa_Ativo ;
      private int[] T000B3_A503Pessoa_MunicipioCod ;
      private bool[] T000B3_n503Pessoa_MunicipioCod ;
      private int[] T000B9_A34Pessoa_Codigo ;
      private int[] T000B10_A34Pessoa_Codigo ;
      private int[] T000B2_A34Pessoa_Codigo ;
      private String[] T000B2_A35Pessoa_Nome ;
      private String[] T000B2_A36Pessoa_TipoPessoa ;
      private String[] T000B2_A37Pessoa_Docto ;
      private String[] T000B2_A518Pessoa_IE ;
      private bool[] T000B2_n518Pessoa_IE ;
      private String[] T000B2_A519Pessoa_Endereco ;
      private bool[] T000B2_n519Pessoa_Endereco ;
      private String[] T000B2_A521Pessoa_CEP ;
      private bool[] T000B2_n521Pessoa_CEP ;
      private String[] T000B2_A522Pessoa_Telefone ;
      private bool[] T000B2_n522Pessoa_Telefone ;
      private String[] T000B2_A523Pessoa_Fax ;
      private bool[] T000B2_n523Pessoa_Fax ;
      private bool[] T000B2_A38Pessoa_Ativo ;
      private int[] T000B2_A503Pessoa_MunicipioCod ;
      private bool[] T000B2_n503Pessoa_MunicipioCod ;
      private int[] T000B11_A34Pessoa_Codigo ;
      private String[] T000B14_A520Pessoa_UF ;
      private bool[] T000B14_n520Pessoa_UF ;
      private int[] T000B15_A2010PessoaCertificado_Codigo ;
      private int[] T000B16_A29Contratante_Codigo ;
      private int[] T000B17_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] T000B18_A1Usuario_Codigo ;
      private int[] T000B19_A39Contratada_Codigo ;
      private int[] T000B20_A34Pessoa_Codigo ;
      private int[] T000B21_A503Pessoa_MunicipioCod ;
      private bool[] T000B21_n503Pessoa_MunicipioCod ;
      private String[] T000B21_A26Municipio_Nome ;
      private bool[] T000B21_n26Municipio_Nome ;
      private String[] T000B22_A37Pessoa_Docto ;
      private String[] T000B23_A520Pessoa_UF ;
      private bool[] T000B23_n520Pessoa_UF ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class pessoa__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000B5 ;
          prmT000B5 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B6 ;
          prmT000B6 = new Object[] {
          new Object[] {"@Pessoa_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B4 ;
          prmT000B4 = new Object[] {
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B7 ;
          prmT000B7 = new Object[] {
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B8 ;
          prmT000B8 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B3 ;
          prmT000B3 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B9 ;
          prmT000B9 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B10 ;
          prmT000B10 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B2 ;
          prmT000B2 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B11 ;
          prmT000B11 = new Object[] {
          new Object[] {"@Pessoa_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@Pessoa_TipoPessoa",SqlDbType.Char,1,0} ,
          new Object[] {"@Pessoa_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Pessoa_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Pessoa_CEP",SqlDbType.Char,10,0} ,
          new Object[] {"@Pessoa_Telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Fax",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B12 ;
          prmT000B12 = new Object[] {
          new Object[] {"@Pessoa_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@Pessoa_TipoPessoa",SqlDbType.Char,1,0} ,
          new Object[] {"@Pessoa_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Pessoa_IE",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Endereco",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Pessoa_CEP",SqlDbType.Char,10,0} ,
          new Object[] {"@Pessoa_Telefone",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Fax",SqlDbType.Char,15,0} ,
          new Object[] {"@Pessoa_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B13 ;
          prmT000B13 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B14 ;
          prmT000B14 = new Object[] {
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B15 ;
          prmT000B15 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B16 ;
          prmT000B16 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B17 ;
          prmT000B17 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B18 ;
          prmT000B18 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B19 ;
          prmT000B19 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B20 ;
          prmT000B20 = new Object[] {
          } ;
          Object[] prmT000B21 ;
          prmT000B21 = new Object[] {
          } ;
          Object[] prmT000B22 ;
          prmT000B22 = new Object[] {
          new Object[] {"@Pessoa_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000B23 ;
          prmT000B23 = new Object[] {
          new Object[] {"@Pessoa_MunicipioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000B2", "SELECT [Pessoa_Codigo], [Pessoa_Nome], [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax], [Pessoa_Ativo], [Pessoa_MunicipioCod] AS Pessoa_MunicipioCod FROM [Pessoa] WITH (UPDLOCK) WHERE [Pessoa_Codigo] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B2,1,0,true,false )
             ,new CursorDef("T000B3", "SELECT [Pessoa_Codigo], [Pessoa_Nome], [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax], [Pessoa_Ativo], [Pessoa_MunicipioCod] AS Pessoa_MunicipioCod FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B3,1,0,true,false )
             ,new CursorDef("T000B4", "SELECT [Estado_UF] AS Pessoa_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Pessoa_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B4,1,0,true,false )
             ,new CursorDef("T000B5", "SELECT TM1.[Pessoa_Codigo], TM1.[Pessoa_Nome], TM1.[Pessoa_TipoPessoa], TM1.[Pessoa_Docto], TM1.[Pessoa_IE], TM1.[Pessoa_Endereco], TM1.[Pessoa_CEP], TM1.[Pessoa_Telefone], TM1.[Pessoa_Fax], TM1.[Pessoa_Ativo], TM1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T2.[Estado_UF] AS Pessoa_UF FROM ([Pessoa] TM1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = TM1.[Pessoa_MunicipioCod]) WHERE TM1.[Pessoa_Codigo] = @Pessoa_Codigo ORDER BY TM1.[Pessoa_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000B5,100,0,true,false )
             ,new CursorDef("T000B6", "SELECT [Pessoa_Docto] FROM [Pessoa] WITH (NOLOCK) WHERE ([Pessoa_Docto] = @Pessoa_Docto) AND (Not ( [Pessoa_Codigo] = @Pessoa_Codigo)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B6,1,0,true,false )
             ,new CursorDef("T000B7", "SELECT [Estado_UF] AS Pessoa_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Pessoa_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B7,1,0,true,false )
             ,new CursorDef("T000B8", "SELECT [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Pessoa_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000B8,1,0,true,false )
             ,new CursorDef("T000B9", "SELECT TOP 1 [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE ( [Pessoa_Codigo] > @Pessoa_Codigo) ORDER BY [Pessoa_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000B9,1,0,true,true )
             ,new CursorDef("T000B10", "SELECT TOP 1 [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) WHERE ( [Pessoa_Codigo] < @Pessoa_Codigo) ORDER BY [Pessoa_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000B10,1,0,true,true )
             ,new CursorDef("T000B11", "INSERT INTO [Pessoa]([Pessoa_Nome], [Pessoa_TipoPessoa], [Pessoa_Docto], [Pessoa_IE], [Pessoa_Endereco], [Pessoa_CEP], [Pessoa_Telefone], [Pessoa_Fax], [Pessoa_Ativo], [Pessoa_MunicipioCod]) VALUES(@Pessoa_Nome, @Pessoa_TipoPessoa, @Pessoa_Docto, @Pessoa_IE, @Pessoa_Endereco, @Pessoa_CEP, @Pessoa_Telefone, @Pessoa_Fax, @Pessoa_Ativo, @Pessoa_MunicipioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000B11)
             ,new CursorDef("T000B12", "UPDATE [Pessoa] SET [Pessoa_Nome]=@Pessoa_Nome, [Pessoa_TipoPessoa]=@Pessoa_TipoPessoa, [Pessoa_Docto]=@Pessoa_Docto, [Pessoa_IE]=@Pessoa_IE, [Pessoa_Endereco]=@Pessoa_Endereco, [Pessoa_CEP]=@Pessoa_CEP, [Pessoa_Telefone]=@Pessoa_Telefone, [Pessoa_Fax]=@Pessoa_Fax, [Pessoa_Ativo]=@Pessoa_Ativo, [Pessoa_MunicipioCod]=@Pessoa_MunicipioCod  WHERE [Pessoa_Codigo] = @Pessoa_Codigo", GxErrorMask.GX_NOMASK,prmT000B12)
             ,new CursorDef("T000B13", "DELETE FROM [Pessoa]  WHERE [Pessoa_Codigo] = @Pessoa_Codigo", GxErrorMask.GX_NOMASK,prmT000B13)
             ,new CursorDef("T000B14", "SELECT [Estado_UF] AS Pessoa_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Pessoa_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B14,1,0,true,false )
             ,new CursorDef("T000B15", "SELECT TOP 1 [PessoaCertificado_Codigo] FROM [PessoaCertificado] WITH (NOLOCK) WHERE [PessoaCertificado_PesCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B15,1,0,true,true )
             ,new CursorDef("T000B16", "SELECT TOP 1 [Contratante_Codigo] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_PessoaCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B16,1,0,true,true )
             ,new CursorDef("T000B17", "SELECT TOP 1 [ContratoOcorrenciaNotificacao_Codigo] FROM [ContratoOcorrenciaNotificacao] WITH (NOLOCK) WHERE [ContratoOcorrenciaNotificacao_Responsavel] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B17,1,0,true,true )
             ,new CursorDef("T000B18", "SELECT TOP 1 [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_PessoaCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B18,1,0,true,true )
             ,new CursorDef("T000B19", "SELECT TOP 1 [Contratada_Codigo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_PessoaCod] = @Pessoa_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B19,1,0,true,true )
             ,new CursorDef("T000B20", "SELECT [Pessoa_Codigo] FROM [Pessoa] WITH (NOLOCK) ORDER BY [Pessoa_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000B20,100,0,true,false )
             ,new CursorDef("T000B21", "SELECT [Municipio_Codigo] AS Pessoa_MunicipioCod, [Municipio_Nome] FROM [Municipio] WITH (NOLOCK) ORDER BY [Municipio_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B21,0,0,true,false )
             ,new CursorDef("T000B22", "SELECT [Pessoa_Docto] FROM [Pessoa] WITH (NOLOCK) WHERE ([Pessoa_Docto] = @Pessoa_Docto) AND (Not ( [Pessoa_Codigo] = @Pessoa_Codigo)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B22,1,0,true,false )
             ,new CursorDef("T000B23", "SELECT [Estado_UF] AS Pessoa_UF FROM [Municipio] WITH (NOLOCK) WHERE [Municipio_Codigo] = @Pessoa_MunicipioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000B23,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 2) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                stmt.SetParameter(9, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[15]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                stmt.SetParameter(9, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[15]);
                }
                stmt.SetParameter(11, (int)parms[16]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
