/*
               File: type_SdtSDT_RedmineStatus
        Description: SDT_RedmineStatus
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:5.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "issue_statuses" )]
   [XmlType(TypeName =  "issue_statuses" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineStatus_issue_status ))]
   [Serializable]
   public class SdtSDT_RedmineStatus : GxUserType
   {
      public SdtSDT_RedmineStatus( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineStatus_Type = "";
      }

      public SdtSDT_RedmineStatus( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineStatus deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineStatus)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineStatus obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Issue_statuss = deserialized.gxTpr_Issue_statuss;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_RedmineStatus_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss != null )
            {
               gxTv_SdtSDT_RedmineStatus_Issue_statuss.ClearCollection();
            }
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp(oReader.LocalName, "issue_status") == 0 )
               {
                  if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss == null )
                  {
                     gxTv_SdtSDT_RedmineStatus_Issue_statuss = new GxObjectCollection( context, "SDT_RedmineStatus.issue_status", "", "SdtSDT_RedmineStatus_issue_status", "GeneXus.Programs");
                  }
                  GXSoapError = gxTv_SdtSDT_RedmineStatus_Issue_statuss.readxmlcollection(oReader, "", "issue_status");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "issue_statuses";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_RedmineStatus_Type));
         if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineStatus_Issue_statuss.writexmlcollection(oWriter, "", sNameSpace1, "issue_status", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("type", gxTv_SdtSDT_RedmineStatus_Type, false);
         if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss != null )
         {
            AddObjectProperty("issue_statuss", gxTv_SdtSDT_RedmineStatus_Issue_statuss, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_RedmineStatus_Type ;
         }

         set {
            gxTv_SdtSDT_RedmineStatus_Type = (String)(value);
         }

      }

      public class gxTv_SdtSDT_RedmineStatus_Issue_statuss_SdtSDT_RedmineStatus_issue_status_80compatibility:SdtSDT_RedmineStatus_issue_status {}
      [  SoapElement( ElementName = "issue_status" )]
      [  XmlElement( ElementName = "issue_status" , Namespace = "" , Type= typeof( SdtSDT_RedmineStatus_issue_status ))]
      public GxObjectCollection gxTpr_Issue_statuss_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss == null )
            {
               gxTv_SdtSDT_RedmineStatus_Issue_statuss = new GxObjectCollection( context, "SDT_RedmineStatus.issue_status", "", "SdtSDT_RedmineStatus_issue_status", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_RedmineStatus_Issue_statuss ;
         }

         set {
            if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss == null )
            {
               gxTv_SdtSDT_RedmineStatus_Issue_statuss = new GxObjectCollection( context, "SDT_RedmineStatus.issue_status", "", "SdtSDT_RedmineStatus_issue_status", "GeneXus.Programs");
            }
            gxTv_SdtSDT_RedmineStatus_Issue_statuss = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Issue_statuss
      {
         get {
            if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss == null )
            {
               gxTv_SdtSDT_RedmineStatus_Issue_statuss = new GxObjectCollection( context, "SDT_RedmineStatus.issue_status", "", "SdtSDT_RedmineStatus_issue_status", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_RedmineStatus_Issue_statuss ;
         }

         set {
            gxTv_SdtSDT_RedmineStatus_Issue_statuss = value;
         }

      }

      public void gxTv_SdtSDT_RedmineStatus_Issue_statuss_SetNull( )
      {
         gxTv_SdtSDT_RedmineStatus_Issue_statuss = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineStatus_Issue_statuss_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineStatus_Issue_statuss == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineStatus_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineStatus_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineStatus_issue_status ))]
      protected IGxCollection gxTv_SdtSDT_RedmineStatus_Issue_statuss=null ;
   }

   [DataContract(Name = @"SDT_RedmineStatus", Namespace = "")]
   public class SdtSDT_RedmineStatus_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineStatus>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineStatus_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineStatus_RESTInterface( SdtSDT_RedmineStatus psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "issue_statuss" , Order = 1 )]
      public GxGenericCollection<SdtSDT_RedmineStatus_issue_status_RESTInterface> gxTpr_Issue_statuss
      {
         get {
            return new GxGenericCollection<SdtSDT_RedmineStatus_issue_status_RESTInterface>(sdt.gxTpr_Issue_statuss) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Issue_statuss);
         }

      }

      public SdtSDT_RedmineStatus sdt
      {
         get {
            return (SdtSDT_RedmineStatus)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineStatus() ;
         }
      }

   }

}
