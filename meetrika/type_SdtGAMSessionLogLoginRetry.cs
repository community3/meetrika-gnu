/*
               File: type_SdtGAMSessionLogLoginRetry
        Description: GAMSessionLogLoginRetry
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:41.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMSessionLogLoginRetry : GxUserType, IGxExternalObject
   {
      public SdtGAMSessionLogLoginRetry( )
      {
         initialize();
      }

      public SdtGAMSessionLogLoginRetry( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMSessionLogLoginRetry_externalReference == null )
         {
            GAMSessionLogLoginRetry_externalReference = new Artech.Security.GAMSessionLogLoginRetry(context);
         }
         returntostring = "";
         returntostring = (String)(GAMSessionLogLoginRetry_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Userlogin
      {
         get {
            if ( GAMSessionLogLoginRetry_externalReference == null )
            {
               GAMSessionLogLoginRetry_externalReference = new Artech.Security.GAMSessionLogLoginRetry(context);
            }
            return GAMSessionLogLoginRetry_externalReference.UserLogin ;
         }

         set {
            if ( GAMSessionLogLoginRetry_externalReference == null )
            {
               GAMSessionLogLoginRetry_externalReference = new Artech.Security.GAMSessionLogLoginRetry(context);
            }
            GAMSessionLogLoginRetry_externalReference.UserLogin = value;
         }

      }

      public short gxTpr_Number
      {
         get {
            if ( GAMSessionLogLoginRetry_externalReference == null )
            {
               GAMSessionLogLoginRetry_externalReference = new Artech.Security.GAMSessionLogLoginRetry(context);
            }
            return GAMSessionLogLoginRetry_externalReference.Number ;
         }

         set {
            if ( GAMSessionLogLoginRetry_externalReference == null )
            {
               GAMSessionLogLoginRetry_externalReference = new Artech.Security.GAMSessionLogLoginRetry(context);
            }
            GAMSessionLogLoginRetry_externalReference.Number = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMSessionLogLoginRetry_externalReference == null )
            {
               GAMSessionLogLoginRetry_externalReference = new Artech.Security.GAMSessionLogLoginRetry(context);
            }
            return GAMSessionLogLoginRetry_externalReference ;
         }

         set {
            GAMSessionLogLoginRetry_externalReference = (Artech.Security.GAMSessionLogLoginRetry)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMSessionLogLoginRetry GAMSessionLogLoginRetry_externalReference=null ;
   }

}
