/*
               File: GetPromptContratoTermoAditivoFilterData
        Description: Get Prompt Contrato Termo Aditivo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:50.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratotermoaditivofilterdata : GXProcedure
   {
      public getpromptcontratotermoaditivofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratotermoaditivofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratotermoaditivofilterdata objgetpromptcontratotermoaditivofilterdata;
         objgetpromptcontratotermoaditivofilterdata = new getpromptcontratotermoaditivofilterdata();
         objgetpromptcontratotermoaditivofilterdata.AV20DDOName = aP0_DDOName;
         objgetpromptcontratotermoaditivofilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetpromptcontratotermoaditivofilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratotermoaditivofilterdata.AV24OptionsJson = "" ;
         objgetpromptcontratotermoaditivofilterdata.AV27OptionsDescJson = "" ;
         objgetpromptcontratotermoaditivofilterdata.AV29OptionIndexesJson = "" ;
         objgetpromptcontratotermoaditivofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratotermoaditivofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratotermoaditivofilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratotermoaditivofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("PromptContratoTermoAditivoGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoTermoAditivoGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("PromptContratoTermoAditivoGridState"), "");
         }
         AV61GXV1 = 1;
         while ( AV61GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV61GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOTERMOADITIVO_DATAINICIO") == 0 )
            {
               AV12TFContratoTermoAditivo_DataInicio = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV13TFContratoTermoAditivo_DataInicio_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOTERMOADITIVO_DATAFIM") == 0 )
            {
               AV14TFContratoTermoAditivo_DataFim = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContratoTermoAditivo_DataFim_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFCONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
            {
               AV16TFContratoTermoAditivo_DataAssinatura = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContratoTermoAditivo_DataAssinatura_To = context.localUtil.CToD( AV34GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV61GXV1 = (int)(AV61GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
            {
               AV37ContratoTermoAditivo_DataInicio1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
               AV38ContratoTermoAditivo_DataInicio_To1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
            {
               AV39ContratoTermoAditivo_DataFim1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
               AV40ContratoTermoAditivo_DataFim_To1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
            {
               AV41ContratoTermoAditivo_DataAssinatura1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
               AV42ContratoTermoAditivo_DataAssinatura_To1 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
               {
                  AV45ContratoTermoAditivo_DataInicio2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                  AV46ContratoTermoAditivo_DataInicio_To2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
               {
                  AV47ContratoTermoAditivo_DataFim2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                  AV48ContratoTermoAditivo_DataFim_To2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
               {
                  AV49ContratoTermoAditivo_DataAssinatura2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                  AV50ContratoTermoAditivo_DataAssinatura_To2 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV51DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV52DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 )
                  {
                     AV53ContratoTermoAditivo_DataInicio3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                     AV54ContratoTermoAditivo_DataInicio_To3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 )
                  {
                     AV55ContratoTermoAditivo_DataFim3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                     AV56ContratoTermoAditivo_DataFim_To3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 )
                  {
                     AV57ContratoTermoAditivo_DataAssinatura3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Value, 2);
                     AV58ContratoTermoAditivo_DataAssinatura_To3 = context.localUtil.CToD( AV35GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV18SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV36DynamicFiltersSelector1 ,
                                              AV37ContratoTermoAditivo_DataInicio1 ,
                                              AV38ContratoTermoAditivo_DataInicio_To1 ,
                                              AV39ContratoTermoAditivo_DataFim1 ,
                                              AV40ContratoTermoAditivo_DataFim_To1 ,
                                              AV41ContratoTermoAditivo_DataAssinatura1 ,
                                              AV42ContratoTermoAditivo_DataAssinatura_To1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45ContratoTermoAditivo_DataInicio2 ,
                                              AV46ContratoTermoAditivo_DataInicio_To2 ,
                                              AV47ContratoTermoAditivo_DataFim2 ,
                                              AV48ContratoTermoAditivo_DataFim_To2 ,
                                              AV49ContratoTermoAditivo_DataAssinatura2 ,
                                              AV50ContratoTermoAditivo_DataAssinatura_To2 ,
                                              AV51DynamicFiltersEnabled3 ,
                                              AV52DynamicFiltersSelector3 ,
                                              AV53ContratoTermoAditivo_DataInicio3 ,
                                              AV54ContratoTermoAditivo_DataInicio_To3 ,
                                              AV55ContratoTermoAditivo_DataFim3 ,
                                              AV56ContratoTermoAditivo_DataFim_To3 ,
                                              AV57ContratoTermoAditivo_DataAssinatura3 ,
                                              AV58ContratoTermoAditivo_DataAssinatura_To3 ,
                                              AV11TFContrato_Numero_Sel ,
                                              AV10TFContrato_Numero ,
                                              AV12TFContratoTermoAditivo_DataInicio ,
                                              AV13TFContratoTermoAditivo_DataInicio_To ,
                                              AV14TFContratoTermoAditivo_DataFim ,
                                              AV15TFContratoTermoAditivo_DataFim_To ,
                                              AV16TFContratoTermoAditivo_DataAssinatura ,
                                              AV17TFContratoTermoAditivo_DataAssinatura_To ,
                                              A316ContratoTermoAditivo_DataInicio ,
                                              A317ContratoTermoAditivo_DataFim ,
                                              A318ContratoTermoAditivo_DataAssinatura ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING
                                              }
         });
         lV10TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV10TFContrato_Numero), 20, "%");
         /* Using cursor P00KI2 */
         pr_default.execute(0, new Object[] {AV37ContratoTermoAditivo_DataInicio1, AV38ContratoTermoAditivo_DataInicio_To1, AV39ContratoTermoAditivo_DataFim1, AV40ContratoTermoAditivo_DataFim_To1, AV41ContratoTermoAditivo_DataAssinatura1, AV42ContratoTermoAditivo_DataAssinatura_To1, AV45ContratoTermoAditivo_DataInicio2, AV46ContratoTermoAditivo_DataInicio_To2, AV47ContratoTermoAditivo_DataFim2, AV48ContratoTermoAditivo_DataFim_To2, AV49ContratoTermoAditivo_DataAssinatura2, AV50ContratoTermoAditivo_DataAssinatura_To2, AV53ContratoTermoAditivo_DataInicio3, AV54ContratoTermoAditivo_DataInicio_To3, AV55ContratoTermoAditivo_DataFim3, AV56ContratoTermoAditivo_DataFim_To3, AV57ContratoTermoAditivo_DataAssinatura3, AV58ContratoTermoAditivo_DataAssinatura_To3, lV10TFContrato_Numero, AV11TFContrato_Numero_Sel, AV12TFContratoTermoAditivo_DataInicio, AV13TFContratoTermoAditivo_DataInicio_To, AV14TFContratoTermoAditivo_DataFim, AV15TFContratoTermoAditivo_DataFim_To, AV16TFContratoTermoAditivo_DataAssinatura, AV17TFContratoTermoAditivo_DataAssinatura_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKI2 = false;
            A74Contrato_Codigo = P00KI2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KI2_A77Contrato_Numero[0];
            A318ContratoTermoAditivo_DataAssinatura = P00KI2_A318ContratoTermoAditivo_DataAssinatura[0];
            n318ContratoTermoAditivo_DataAssinatura = P00KI2_n318ContratoTermoAditivo_DataAssinatura[0];
            A317ContratoTermoAditivo_DataFim = P00KI2_A317ContratoTermoAditivo_DataFim[0];
            n317ContratoTermoAditivo_DataFim = P00KI2_n317ContratoTermoAditivo_DataFim[0];
            A316ContratoTermoAditivo_DataInicio = P00KI2_A316ContratoTermoAditivo_DataInicio[0];
            n316ContratoTermoAditivo_DataInicio = P00KI2_n316ContratoTermoAditivo_DataInicio[0];
            A315ContratoTermoAditivo_Codigo = P00KI2_A315ContratoTermoAditivo_Codigo[0];
            A77Contrato_Numero = P00KI2_A77Contrato_Numero[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KI2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKKI2 = false;
               A74Contrato_Codigo = P00KI2_A74Contrato_Codigo[0];
               A315ContratoTermoAditivo_Codigo = P00KI2_A315ContratoTermoAditivo_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKKI2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV22Option = A77Contrato_Numero;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKI2 )
            {
               BRKKI2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFContratoTermoAditivo_DataInicio = DateTime.MinValue;
         AV13TFContratoTermoAditivo_DataInicio_To = DateTime.MinValue;
         AV14TFContratoTermoAditivo_DataFim = DateTime.MinValue;
         AV15TFContratoTermoAditivo_DataFim_To = DateTime.MinValue;
         AV16TFContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         AV17TFContratoTermoAditivo_DataAssinatura_To = DateTime.MinValue;
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37ContratoTermoAditivo_DataInicio1 = DateTime.MinValue;
         AV38ContratoTermoAditivo_DataInicio_To1 = DateTime.MinValue;
         AV39ContratoTermoAditivo_DataFim1 = DateTime.MinValue;
         AV40ContratoTermoAditivo_DataFim_To1 = DateTime.MinValue;
         AV41ContratoTermoAditivo_DataAssinatura1 = DateTime.MinValue;
         AV42ContratoTermoAditivo_DataAssinatura_To1 = DateTime.MinValue;
         AV44DynamicFiltersSelector2 = "";
         AV45ContratoTermoAditivo_DataInicio2 = DateTime.MinValue;
         AV46ContratoTermoAditivo_DataInicio_To2 = DateTime.MinValue;
         AV47ContratoTermoAditivo_DataFim2 = DateTime.MinValue;
         AV48ContratoTermoAditivo_DataFim_To2 = DateTime.MinValue;
         AV49ContratoTermoAditivo_DataAssinatura2 = DateTime.MinValue;
         AV50ContratoTermoAditivo_DataAssinatura_To2 = DateTime.MinValue;
         AV52DynamicFiltersSelector3 = "";
         AV53ContratoTermoAditivo_DataInicio3 = DateTime.MinValue;
         AV54ContratoTermoAditivo_DataInicio_To3 = DateTime.MinValue;
         AV55ContratoTermoAditivo_DataFim3 = DateTime.MinValue;
         AV56ContratoTermoAditivo_DataFim_To3 = DateTime.MinValue;
         AV57ContratoTermoAditivo_DataAssinatura3 = DateTime.MinValue;
         AV58ContratoTermoAditivo_DataAssinatura_To3 = DateTime.MinValue;
         scmdbuf = "";
         lV10TFContrato_Numero = "";
         A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         A318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         A77Contrato_Numero = "";
         P00KI2_A74Contrato_Codigo = new int[1] ;
         P00KI2_A77Contrato_Numero = new String[] {""} ;
         P00KI2_A318ContratoTermoAditivo_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         P00KI2_n318ContratoTermoAditivo_DataAssinatura = new bool[] {false} ;
         P00KI2_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00KI2_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         P00KI2_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         P00KI2_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         P00KI2_A315ContratoTermoAditivo_Codigo = new int[1] ;
         AV22Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratotermoaditivofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KI2_A74Contrato_Codigo, P00KI2_A77Contrato_Numero, P00KI2_A318ContratoTermoAditivo_DataAssinatura, P00KI2_n318ContratoTermoAditivo_DataAssinatura, P00KI2_A317ContratoTermoAditivo_DataFim, P00KI2_n317ContratoTermoAditivo_DataFim, P00KI2_A316ContratoTermoAditivo_DataInicio, P00KI2_n316ContratoTermoAditivo_DataInicio, P00KI2_A315ContratoTermoAditivo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV61GXV1 ;
      private int A74Contrato_Codigo ;
      private int A315ContratoTermoAditivo_Codigo ;
      private long AV30count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String scmdbuf ;
      private String lV10TFContrato_Numero ;
      private String A77Contrato_Numero ;
      private DateTime AV12TFContratoTermoAditivo_DataInicio ;
      private DateTime AV13TFContratoTermoAditivo_DataInicio_To ;
      private DateTime AV14TFContratoTermoAditivo_DataFim ;
      private DateTime AV15TFContratoTermoAditivo_DataFim_To ;
      private DateTime AV16TFContratoTermoAditivo_DataAssinatura ;
      private DateTime AV17TFContratoTermoAditivo_DataAssinatura_To ;
      private DateTime AV37ContratoTermoAditivo_DataInicio1 ;
      private DateTime AV38ContratoTermoAditivo_DataInicio_To1 ;
      private DateTime AV39ContratoTermoAditivo_DataFim1 ;
      private DateTime AV40ContratoTermoAditivo_DataFim_To1 ;
      private DateTime AV41ContratoTermoAditivo_DataAssinatura1 ;
      private DateTime AV42ContratoTermoAditivo_DataAssinatura_To1 ;
      private DateTime AV45ContratoTermoAditivo_DataInicio2 ;
      private DateTime AV46ContratoTermoAditivo_DataInicio_To2 ;
      private DateTime AV47ContratoTermoAditivo_DataFim2 ;
      private DateTime AV48ContratoTermoAditivo_DataFim_To2 ;
      private DateTime AV49ContratoTermoAditivo_DataAssinatura2 ;
      private DateTime AV50ContratoTermoAditivo_DataAssinatura_To2 ;
      private DateTime AV53ContratoTermoAditivo_DataInicio3 ;
      private DateTime AV54ContratoTermoAditivo_DataInicio_To3 ;
      private DateTime AV55ContratoTermoAditivo_DataFim3 ;
      private DateTime AV56ContratoTermoAditivo_DataFim_To3 ;
      private DateTime AV57ContratoTermoAditivo_DataAssinatura3 ;
      private DateTime AV58ContratoTermoAditivo_DataAssinatura_To3 ;
      private DateTime A316ContratoTermoAditivo_DataInicio ;
      private DateTime A317ContratoTermoAditivo_DataFim ;
      private DateTime A318ContratoTermoAditivo_DataAssinatura ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV51DynamicFiltersEnabled3 ;
      private bool BRKKI2 ;
      private bool n318ContratoTermoAditivo_DataAssinatura ;
      private bool n317ContratoTermoAditivo_DataFim ;
      private bool n316ContratoTermoAditivo_DataInicio ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV52DynamicFiltersSelector3 ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00KI2_A74Contrato_Codigo ;
      private String[] P00KI2_A77Contrato_Numero ;
      private DateTime[] P00KI2_A318ContratoTermoAditivo_DataAssinatura ;
      private bool[] P00KI2_n318ContratoTermoAditivo_DataAssinatura ;
      private DateTime[] P00KI2_A317ContratoTermoAditivo_DataFim ;
      private bool[] P00KI2_n317ContratoTermoAditivo_DataFim ;
      private DateTime[] P00KI2_A316ContratoTermoAditivo_DataInicio ;
      private bool[] P00KI2_n316ContratoTermoAditivo_DataInicio ;
      private int[] P00KI2_A315ContratoTermoAditivo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getpromptcontratotermoaditivofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KI2( IGxContext context ,
                                             String AV36DynamicFiltersSelector1 ,
                                             DateTime AV37ContratoTermoAditivo_DataInicio1 ,
                                             DateTime AV38ContratoTermoAditivo_DataInicio_To1 ,
                                             DateTime AV39ContratoTermoAditivo_DataFim1 ,
                                             DateTime AV40ContratoTermoAditivo_DataFim_To1 ,
                                             DateTime AV41ContratoTermoAditivo_DataAssinatura1 ,
                                             DateTime AV42ContratoTermoAditivo_DataAssinatura_To1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             DateTime AV45ContratoTermoAditivo_DataInicio2 ,
                                             DateTime AV46ContratoTermoAditivo_DataInicio_To2 ,
                                             DateTime AV47ContratoTermoAditivo_DataFim2 ,
                                             DateTime AV48ContratoTermoAditivo_DataFim_To2 ,
                                             DateTime AV49ContratoTermoAditivo_DataAssinatura2 ,
                                             DateTime AV50ContratoTermoAditivo_DataAssinatura_To2 ,
                                             bool AV51DynamicFiltersEnabled3 ,
                                             String AV52DynamicFiltersSelector3 ,
                                             DateTime AV53ContratoTermoAditivo_DataInicio3 ,
                                             DateTime AV54ContratoTermoAditivo_DataInicio_To3 ,
                                             DateTime AV55ContratoTermoAditivo_DataFim3 ,
                                             DateTime AV56ContratoTermoAditivo_DataFim_To3 ,
                                             DateTime AV57ContratoTermoAditivo_DataAssinatura3 ,
                                             DateTime AV58ContratoTermoAditivo_DataAssinatura_To3 ,
                                             String AV11TFContrato_Numero_Sel ,
                                             String AV10TFContrato_Numero ,
                                             DateTime AV12TFContratoTermoAditivo_DataInicio ,
                                             DateTime AV13TFContratoTermoAditivo_DataInicio_To ,
                                             DateTime AV14TFContratoTermoAditivo_DataFim ,
                                             DateTime AV15TFContratoTermoAditivo_DataFim_To ,
                                             DateTime AV16TFContratoTermoAditivo_DataAssinatura ,
                                             DateTime AV17TFContratoTermoAditivo_DataAssinatura_To ,
                                             DateTime A316ContratoTermoAditivo_DataInicio ,
                                             DateTime A317ContratoTermoAditivo_DataFim ,
                                             DateTime A318ContratoTermoAditivo_DataAssinatura ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [26] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Contrato_Numero], T1.[ContratoTermoAditivo_DataAssinatura], T1.[ContratoTermoAditivo_DataFim], T1.[ContratoTermoAditivo_DataInicio], T1.[ContratoTermoAditivo_Codigo] FROM ([ContratoTermoAditivo] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV37ContratoTermoAditivo_DataInicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV37ContratoTermoAditivo_DataInicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV37ContratoTermoAditivo_DataInicio1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV38ContratoTermoAditivo_DataInicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV38ContratoTermoAditivo_DataInicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV38ContratoTermoAditivo_DataInicio_To1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV39ContratoTermoAditivo_DataFim1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV39ContratoTermoAditivo_DataFim1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV39ContratoTermoAditivo_DataFim1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV40ContratoTermoAditivo_DataFim_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV40ContratoTermoAditivo_DataFim_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV40ContratoTermoAditivo_DataFim_To1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV41ContratoTermoAditivo_DataAssinatura1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV41ContratoTermoAditivo_DataAssinatura1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV41ContratoTermoAditivo_DataAssinatura1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV42ContratoTermoAditivo_DataAssinatura_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV42ContratoTermoAditivo_DataAssinatura_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV42ContratoTermoAditivo_DataAssinatura_To1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV45ContratoTermoAditivo_DataInicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV45ContratoTermoAditivo_DataInicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV45ContratoTermoAditivo_DataInicio2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV46ContratoTermoAditivo_DataInicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV46ContratoTermoAditivo_DataInicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV46ContratoTermoAditivo_DataInicio_To2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV47ContratoTermoAditivo_DataFim2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV47ContratoTermoAditivo_DataFim2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV47ContratoTermoAditivo_DataFim2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV48ContratoTermoAditivo_DataFim_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV48ContratoTermoAditivo_DataFim_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV48ContratoTermoAditivo_DataFim_To2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV49ContratoTermoAditivo_DataAssinatura2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV49ContratoTermoAditivo_DataAssinatura2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV49ContratoTermoAditivo_DataAssinatura2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV50ContratoTermoAditivo_DataAssinatura_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV50ContratoTermoAditivo_DataAssinatura_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV50ContratoTermoAditivo_DataAssinatura_To2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV53ContratoTermoAditivo_DataInicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV53ContratoTermoAditivo_DataInicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV53ContratoTermoAditivo_DataInicio3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV54ContratoTermoAditivo_DataInicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV54ContratoTermoAditivo_DataInicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV54ContratoTermoAditivo_DataInicio_To3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoTermoAditivo_DataFim3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV55ContratoTermoAditivo_DataFim3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV55ContratoTermoAditivo_DataFim3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAFIM") == 0 ) && ( ! (DateTime.MinValue==AV56ContratoTermoAditivo_DataFim_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV56ContratoTermoAditivo_DataFim_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV56ContratoTermoAditivo_DataFim_To3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV57ContratoTermoAditivo_DataAssinatura3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV57ContratoTermoAditivo_DataAssinatura3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV57ContratoTermoAditivo_DataAssinatura3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV51DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV52DynamicFiltersSelector3, "CONTRATOTERMOADITIVO_DATAASSINATURA") == 0 ) && ( ! (DateTime.MinValue==AV58ContratoTermoAditivo_DataAssinatura_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV58ContratoTermoAditivo_DataAssinatura_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV58ContratoTermoAditivo_DataAssinatura_To3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV10TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV11TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFContratoTermoAditivo_DataInicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] >= @AV12TFContratoTermoAditivo_DataInicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] >= @AV12TFContratoTermoAditivo_DataInicio)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFContratoTermoAditivo_DataInicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataInicio] <= @AV13TFContratoTermoAditivo_DataInicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataInicio] <= @AV13TFContratoTermoAditivo_DataInicio_To)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContratoTermoAditivo_DataFim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] >= @AV14TFContratoTermoAditivo_DataFim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] >= @AV14TFContratoTermoAditivo_DataFim)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContratoTermoAditivo_DataFim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataFim] <= @AV15TFContratoTermoAditivo_DataFim_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataFim] <= @AV15TFContratoTermoAditivo_DataFim_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFContratoTermoAditivo_DataAssinatura) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV16TFContratoTermoAditivo_DataAssinatura)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] >= @AV16TFContratoTermoAditivo_DataAssinatura)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFContratoTermoAditivo_DataAssinatura_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV17TFContratoTermoAditivo_DataAssinatura_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoTermoAditivo_DataAssinatura] <= @AV17TFContratoTermoAditivo_DataAssinatura_To)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KI2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (DateTime)dynConstraints[33] , (String)dynConstraints[34] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KI2 ;
          prmP00KI2 = new Object[] {
          new Object[] {"@AV37ContratoTermoAditivo_DataInicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV38ContratoTermoAditivo_DataInicio_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV39ContratoTermoAditivo_DataFim1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40ContratoTermoAditivo_DataFim_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV41ContratoTermoAditivo_DataAssinatura1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV42ContratoTermoAditivo_DataAssinatura_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45ContratoTermoAditivo_DataInicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46ContratoTermoAditivo_DataInicio_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47ContratoTermoAditivo_DataFim2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48ContratoTermoAditivo_DataFim_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContratoTermoAditivo_DataAssinatura2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV50ContratoTermoAditivo_DataAssinatura_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53ContratoTermoAditivo_DataInicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54ContratoTermoAditivo_DataInicio_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContratoTermoAditivo_DataFim3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContratoTermoAditivo_DataFim_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV57ContratoTermoAditivo_DataAssinatura3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV58ContratoTermoAditivo_DataAssinatura_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV10TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV11TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV12TFContratoTermoAditivo_DataInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFContratoTermoAditivo_DataInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV14TFContratoTermoAditivo_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContratoTermoAditivo_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16TFContratoTermoAditivo_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFContratoTermoAditivo_DataAssinatura_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KI2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratotermoaditivofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratotermoaditivofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratotermoaditivofilterdata") )
          {
             return  ;
          }
          getpromptcontratotermoaditivofilterdata worker = new getpromptcontratotermoaditivofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
