/*
               File: WP_ServicosDePara
        Description: Servicos (De Para)
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:20:31.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_servicosdepara : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_servicosdepara( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_servicosdepara( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Origem ,
                           int aP1_Contratada_Codigo )
      {
         this.AV6Origem = aP0_Origem;
         this.AV35Contratada_Codigo = aP1_Contratada_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavTracker = new GXCombobox();
         cmbavEstado = new GXCombobox();
         cmbavServico_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV6Origem = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Origem", AV6Origem);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Origem, ""))));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV35Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contratada_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35Contratada_Codigo), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAKY2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTKY2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221203119");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_servicosdepara.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6Origem)) + "," + UrlEncode("" +AV35Contratada_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A827ContratoServicos_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGEM", StringUtil.RTrim( A1466ContratoServicosDePara_Origem));
         GxWebStd.gx_hidden_field( context, "vORIGEM", StringUtil.RTrim( AV6Origem));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGEMDSC2", A1470ContratoServicosDePara_OrigemDsc2);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGEMDSC", A1468ContratoServicosDePara_OrigemDsc);
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1465ContratoServicosDePara_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGENID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1467ContratoServicosDePara_OrigenId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSDEPARA_ORIGENID2", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1469ContratoServicosDePara_OrigenId2), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSDEPARA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23ContratoServicosDePara_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vORIGEMDSC", AV7OrigemDsc);
         GxWebStd.gx_hidden_field( context, "vORIGEMDSC2", AV18OrigemDsc2);
         GxWebStd.gx_hidden_field( context, "GXC1", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40000GXC1), 9, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Origem, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Origem, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Height", StringUtil.RTrim( Confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Width", StringUtil.RTrim( Desvinculapanel_Width));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Title", StringUtil.RTrim( Desvinculapanel_Title));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Confirmtext", StringUtil.RTrim( Desvinculapanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Buttonyestext", StringUtil.RTrim( Desvinculapanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Buttonnotext", StringUtil.RTrim( Desvinculapanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Buttoncanceltext", StringUtil.RTrim( Desvinculapanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Confirmtype", StringUtil.RTrim( Desvinculapanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GxWebStd.gx_hidden_field( context, "vTRACKER_Text", StringUtil.RTrim( cmbavTracker.Description));
         GxWebStd.gx_hidden_field( context, "vESTADO_Text", StringUtil.RTrim( cmbavEstado.Description));
         GxWebStd.gx_hidden_field( context, "DESVINCULAPANEL_Result", StringUtil.RTrim( Desvinculapanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEKY2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTKY2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_servicosdepara.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6Origem)) + "," + UrlEncode("" +AV35Contratada_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ServicosDePara" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servicos (De Para)" ;
      }

      protected void WBKY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            wb_table1_4_KY2( true) ;
         }
         else
         {
            wb_table1_4_KY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_4_KY2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_30_KY2( true) ;
         }
         else
         {
            wb_table2_30_KY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_30_KY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_ServicosDePara.htm");
         }
         wbLoad = true;
      }

      protected void STARTKY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Servicos (De Para)", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKY0( ) ;
      }

      protected void WSKY2( )
      {
         STARTKY2( ) ;
         EVTKY2( ) ;
      }

      protected void EVTKY2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11KY2 */
                              E11KY2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DESVINCULAPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12KY2 */
                              E12KY2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13KY2 */
                              E13KY2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E14KY2 */
                                    E14KY2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'FECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15KY2 */
                              E15KY2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16KY2 */
                              E16KY2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAKY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavTracker.Name = "vTRACKER";
            cmbavTracker.WebTags = "";
            cmbavTracker.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 18, 0)), "(Selecionar)", 0);
            if ( cmbavTracker.ItemCount > 0 )
            {
               AV15Tracker = (long)(NumberUtil.Val( cmbavTracker.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)));
            }
            cmbavEstado.Name = "vESTADO";
            cmbavEstado.WebTags = "";
            cmbavEstado.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 18, 0)), "(Selecionar)", 0);
            if ( cmbavEstado.ItemCount > 0 )
            {
               AV16Estado = (long)(NumberUtil.Val( cmbavEstado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16Estado), 18, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Estado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)));
            }
            cmbavServico_codigo.Name = "vSERVICO_CODIGO";
            cmbavServico_codigo.WebTags = "";
            cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Selecionar)", 0);
            if ( cmbavServico_codigo.ItemCount > 0 )
            {
               AV11Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11Servico_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Servico_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavTracker_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavTracker.ItemCount > 0 )
         {
            AV15Tracker = (long)(NumberUtil.Val( cmbavTracker.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)));
         }
         if ( cmbavEstado.ItemCount > 0 )
         {
            AV16Estado = (long)(NumberUtil.Val( cmbavEstado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16Estado), 18, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Estado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)));
         }
         if ( cmbavServico_codigo.ItemCount > 0 )
         {
            AV11Servico_Codigo = (int)(NumberUtil.Val( cmbavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Servico_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFKY2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E16KY2 */
            E16KY2 ();
            WBKY0( ) ;
         }
      }

      protected void STRUPKY0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H00KY3 */
         pr_default.execute(0, new Object[] {AV22ContratoServicos_Codigo});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000GXC1 = H00KY3_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(0);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13KY2 */
         E13KY2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavTracker.CurrentValue = cgiGet( cmbavTracker_Internalname);
            AV15Tracker = (long)(NumberUtil.Val( cgiGet( cmbavTracker_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)));
            cmbavEstado.CurrentValue = cgiGet( cmbavEstado_Internalname);
            AV16Estado = (long)(NumberUtil.Val( cgiGet( cmbavEstado_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Estado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)));
            cmbavServico_codigo.CurrentValue = cgiGet( cmbavServico_codigo_Internalname);
            AV11Servico_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Servico_Codigo), 6, 0)));
            /* Read saved values. */
            Confirmpanel_Width = cgiGet( "CONFIRMPANEL_Width");
            Confirmpanel_Height = cgiGet( "CONFIRMPANEL_Height");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Desvinculapanel_Width = cgiGet( "DESVINCULAPANEL_Width");
            Desvinculapanel_Title = cgiGet( "DESVINCULAPANEL_Title");
            Desvinculapanel_Confirmtext = cgiGet( "DESVINCULAPANEL_Confirmtext");
            Desvinculapanel_Buttonyestext = cgiGet( "DESVINCULAPANEL_Buttonyestext");
            Desvinculapanel_Buttonnotext = cgiGet( "DESVINCULAPANEL_Buttonnotext");
            Desvinculapanel_Buttoncanceltext = cgiGet( "DESVINCULAPANEL_Buttoncanceltext");
            Desvinculapanel_Confirmtype = cgiGet( "DESVINCULAPANEL_Confirmtype");
            Confirmpanel_Result = cgiGet( "CONFIRMPANEL_Result");
            Desvinculapanel_Result = cgiGet( "DESVINCULAPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13KY2 */
         E13KY2 ();
         if (returnInSub) return;
      }

      protected void E13KY2( )
      {
         /* Start Routine */
         AV26Trackers.FromXml(AV14WebSession.Get("Trackers"), "SDT_RedmineTrackers.trackerCollection");
         AV25Estados.FromXml(AV14WebSession.Get("Estados"), "SDT_RedmineStatus.issue_statusCollection");
         AV38GXV1 = 1;
         while ( AV38GXV1 <= AV26Trackers.Count )
         {
            AV32TrackerItem = ((SdtSDT_RedmineTrackers_tracker)AV26Trackers.Item(AV38GXV1));
            cmbavTracker.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV32TrackerItem.gxTpr_Id), 18, 0)), AV32TrackerItem.gxTpr_Name, 0);
            AV15Tracker = AV32TrackerItem.gxTpr_Id;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)));
            AV38GXV1 = (int)(AV38GXV1+1);
         }
         AV39GXV2 = 1;
         while ( AV39GXV2 <= AV25Estados.Count )
         {
            AV30EstadoItem = ((SdtSDT_RedmineStatus_issue_status)AV25Estados.Item(AV39GXV2));
            cmbavEstado.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV30EstadoItem.gxTpr_Id), 18, 0)), AV30EstadoItem.gxTpr_Name, 0);
            AV16Estado = AV30EstadoItem.gxTpr_Id;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Estado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)));
            AV39GXV2 = (int)(AV39GXV2+1);
         }
         if ( cmbavTracker.ItemCount > 1 )
         {
            AV15Tracker = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)));
         }
         if ( cmbavEstado.ItemCount > 1 )
         {
            AV16Estado = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Estado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)));
         }
         /* Using cursor H00KY4 */
         pr_default.execute(1, new Object[] {AV35Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A74Contrato_Codigo = H00KY4_A74Contrato_Codigo[0];
            A39Contratada_Codigo = H00KY4_A39Contratada_Codigo[0];
            A638ContratoServicos_Ativo = H00KY4_A638ContratoServicos_Ativo[0];
            A826ContratoServicos_ServicoSigla = H00KY4_A826ContratoServicos_ServicoSigla[0];
            A605Servico_Sigla = H00KY4_A605Servico_Sigla[0];
            A155Servico_Codigo = H00KY4_A155Servico_Codigo[0];
            A39Contratada_Codigo = H00KY4_A39Contratada_Codigo[0];
            A605Servico_Sigla = H00KY4_A605Servico_Sigla[0];
            A827ContratoServicos_ServicoCod = A155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A827ContratoServicos_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)));
            cmbavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A827ContratoServicos_ServicoCod), 6, 0)), A826ContratoServicos_ServicoSigla, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      public void GXEnter( )
      {
         /* Execute user event: E14KY2 */
         E14KY2 ();
         if (returnInSub) return;
      }

      protected void E14KY2( )
      {
         /* Enter Routine */
         if ( (0==AV15Tracker) && (0==AV16Estado) )
         {
            GX_msglist.addItem("Selecione o Tracker e/ou Estado a vincular!");
         }
         else if ( (0==AV11Servico_Codigo) )
         {
            GX_msglist.addItem("Selecione o Servi�o a ser vinculado!");
         }
         else
         {
            this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
      }

      protected void E11KY2( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            AV5i = 0;
            /* Using cursor H00KY5 */
            pr_default.execute(2, new Object[] {AV35Contratada_Codigo, AV11Servico_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A74Contrato_Codigo = H00KY5_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = H00KY5_A160ContratoServicos_Codigo[0];
               A827ContratoServicos_ServicoCod = H00KY5_A827ContratoServicos_ServicoCod[0];
               A39Contratada_Codigo = H00KY5_A39Contratada_Codigo[0];
               A155Servico_Codigo = H00KY5_A155Servico_Codigo[0];
               A39Contratada_Codigo = H00KY5_A39Contratada_Codigo[0];
               AV22ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContratoServicos_Codigo), 6, 0)));
               /* Using cursor H00KY6 */
               pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo, AV6Origem});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A1466ContratoServicosDePara_Origem = H00KY6_A1466ContratoServicosDePara_Origem[0];
                  A1470ContratoServicosDePara_OrigemDsc2 = H00KY6_A1470ContratoServicosDePara_OrigemDsc2[0];
                  n1470ContratoServicosDePara_OrigemDsc2 = H00KY6_n1470ContratoServicosDePara_OrigemDsc2[0];
                  A1468ContratoServicosDePara_OrigemDsc = H00KY6_A1468ContratoServicosDePara_OrigemDsc[0];
                  n1468ContratoServicosDePara_OrigemDsc = H00KY6_n1468ContratoServicosDePara_OrigemDsc[0];
                  A1465ContratoServicosDePara_Codigo = H00KY6_A1465ContratoServicosDePara_Codigo[0];
                  A1467ContratoServicosDePara_OrigenId = H00KY6_A1467ContratoServicosDePara_OrigenId[0];
                  n1467ContratoServicosDePara_OrigenId = H00KY6_n1467ContratoServicosDePara_OrigenId[0];
                  A1469ContratoServicosDePara_OrigenId2 = H00KY6_A1469ContratoServicosDePara_OrigenId2[0];
                  n1469ContratoServicosDePara_OrigenId2 = H00KY6_n1469ContratoServicosDePara_OrigenId2[0];
                  Desvinculapanel_Confirmtext = "Servico j� vinculado com ";
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( A1470ContratoServicosDePara_OrigemDsc2)) )
                  {
                     Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+StringUtil.Trim( A1468ContratoServicosDePara_OrigemDsc);
                     context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
                  }
                  else
                  {
                     if ( String.IsNullOrEmpty(StringUtil.RTrim( A1468ContratoServicosDePara_OrigemDsc)) )
                     {
                        Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+StringUtil.Trim( A1470ContratoServicosDePara_OrigemDsc2);
                        context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
                     }
                     else
                     {
                        Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+StringUtil.Trim( A1468ContratoServicosDePara_OrigemDsc);
                        context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
                        Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+" e "+StringUtil.Trim( A1470ContratoServicosDePara_OrigemDsc2);
                        context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
                     }
                  }
                  AV23ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoServicosDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ContratoServicosDePara_Codigo), 6, 0)));
                  AV15Tracker = A1467ContratoServicosDePara_OrigenId;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Tracker", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)));
                  AV16Estado = A1469ContratoServicosDePara_OrigenId2;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Estado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)));
                  AV7OrigemDsc = A1468ContratoServicosDePara_OrigemDsc;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7OrigemDsc", AV7OrigemDsc);
                  AV18OrigemDsc2 = A1470ContratoServicosDePara_OrigemDsc2;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18OrigemDsc2", AV18OrigemDsc2);
                  AV5i = (short)(AV5i+1);
                  pr_default.readNext(3);
               }
               pr_default.close(3);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
            if ( (0==AV5i) )
            {
               /* Execute user subroutine: 'VINCULAR' */
               S112 ();
               if (returnInSub) return;
            }
            else
            {
               if ( AV5i > 1 )
               {
                  Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+" e outros!";
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
               }
               else
               {
                  Desvinculapanel_Confirmtext = Desvinculapanel_Confirmtext+"!";
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Desvinculapanel_Internalname, "ConfirmText", Desvinculapanel_Confirmtext);
               }
               this.executeUsercontrolMethod("", false, "DESVINCULAPANELContainer", "Confirm", "", new Object[] {});
            }
         }
         cmbavTracker.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTracker_Internalname, "Values", cmbavTracker.ToJavascriptSource());
         cmbavEstado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Estado), 18, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEstado_Internalname, "Values", cmbavEstado.ToJavascriptSource());
      }

      protected void E12KY2( )
      {
         /* Desvinculapanel_Close Routine */
         if ( StringUtil.StrCmp(Desvinculapanel_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'VINCULAR' */
            S112 ();
            if (returnInSub) return;
         }
         else if ( StringUtil.StrCmp(Desvinculapanel_Result, "Cancel") == 0 )
         {
            AV12ContratoServicosDePara.Load(AV22ContratoServicos_Codigo, AV23ContratoServicosDePara_Codigo);
            AV12ContratoServicosDePara.Delete();
            if ( AV12ContratoServicosDePara.Success() )
            {
               context.CommitDataStores( "WP_ServicosDePara");
               if ( ! (0==AV15Tracker) )
               {
                  cmbavTracker.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)), AV7OrigemDsc, 0);
               }
               if ( ! (0==AV16Estado) )
               {
                  cmbavEstado.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)), AV18OrigemDsc2, 0);
               }
            }
         }
         cmbavTracker.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTracker_Internalname, "Values", cmbavTracker.ToJavascriptSource());
         cmbavEstado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Estado), 18, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEstado_Internalname, "Values", cmbavEstado.ToJavascriptSource());
      }

      protected void S112( )
      {
         /* 'VINCULAR' Routine */
         /* Using cursor H00KY8 */
         pr_default.execute(4, new Object[] {AV22ContratoServicos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A40000GXC1 = H00KY8_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(4);
         AV12ContratoServicosDePara = new SdtContratoServicosDePara(context);
         AV12ContratoServicosDePara.gxTpr_Contratoservicos_codigo = AV22ContratoServicos_Codigo;
         AV12ContratoServicosDePara.gxTpr_Contratoservicosdepara_codigo = (int)(A40000GXC1+1);
         AV12ContratoServicosDePara.gxTpr_Contratoservicosdepara_origem = AV6Origem;
         if ( AV15Tracker > 0 )
         {
            AV12ContratoServicosDePara.gxTpr_Contratoservicosdepara_origenid = AV15Tracker;
            AV12ContratoServicosDePara.gxTpr_Contratoservicosdepara_origemdsc = cmbavTracker.Description;
         }
         if ( AV16Estado > 0 )
         {
            AV12ContratoServicosDePara.gxTpr_Contratoservicosdepara_origenid2 = AV16Estado;
            AV12ContratoServicosDePara.gxTpr_Contratoservicosdepara_origemdsc2 = cmbavEstado.Description;
         }
         AV12ContratoServicosDePara.Save();
         if ( AV12ContratoServicosDePara.Success() )
         {
            context.CommitDataStores( "WP_ServicosDePara");
         }
      }

      protected void E15KY2( )
      {
         /* 'Fechar' Routine */
         /* Execute user subroutine: 'DOFECHAR' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'DOFECHAR' Routine */
         AV14WebSession.Remove("Trackers");
         AV14WebSession.Remove("Estados");
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E16KY2( )
      {
         /* Load Routine */
      }

      protected void wb_table2_30_KY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DESVINCULAPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_30_KY2e( true) ;
         }
         else
         {
            wb_table2_30_KY2e( false) ;
         }
      }

      protected void wb_table1_4_KY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_7_KY2( true) ;
         }
         else
         {
            wb_table3_7_KY2( false) ;
         }
         return  ;
      }

      protected void wb_table3_7_KY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_4_KY2e( true) ;
         }
         else
         {
            wb_table1_4_KY2e( false) ;
         }
      }

      protected void wb_table3_7_KY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 10, 4, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "De Tracker", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTracker, cmbavTracker_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0)), 1, cmbavTracker_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "", true, "HLP_WP_ServicosDePara.htm");
            cmbavTracker.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Tracker), 18, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTracker_Internalname, "Values", (String)(cmbavTracker.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "E/ou Estado", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavEstado, cmbavEstado_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16Estado), 18, 0)), 1, cmbavEstado_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WP_ServicosDePara.htm");
            cmbavEstado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16Estado), 18, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEstado_Internalname, "Values", (String)(cmbavEstado.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Para Servi�o", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavServico_codigo, cmbavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV11Servico_Codigo), 6, 0)), 1, cmbavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_WP_ServicosDePara.htm");
            cmbavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavServico_codigo_Internalname, "Values", (String)(cmbavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:50px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Vincular", bttButton1_Jsonclick, 5, "Vincular", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ServicosDePara.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Fechar", bttButton2_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'FECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ServicosDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_7_KY2e( true) ;
         }
         else
         {
            wb_table3_7_KY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV6Origem = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Origem", AV6Origem);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vORIGEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Origem, ""))));
         AV35Contratada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV35Contratada_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKY2( ) ;
         WSKY2( ) ;
         WEKY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221203199");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_servicosdepara.js", "?202031221203199");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavTracker_Internalname = "vTRACKER";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         cmbavEstado_Internalname = "vESTADO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         cmbavServico_codigo_Internalname = "vSERVICO_CODIGO";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         tblTable2_Internalname = "TABLE2";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         Desvinculapanel_Internalname = "DESVINCULAPANEL";
         tblTable3_Internalname = "TABLE3";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavServico_codigo_Jsonclick = "";
         cmbavEstado_Jsonclick = "";
         cmbavTracker_Jsonclick = "";
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         cmbavEstado.Description = "";
         cmbavTracker.Description = "";
         Desvinculapanel_Confirmtype = "2";
         Desvinculapanel_Buttoncanceltext = "Eliminar";
         Desvinculapanel_Buttonnotext = "Voltar";
         Desvinculapanel_Buttonyestext = "Vincular";
         Desvinculapanel_Confirmtext = "Do you want to continue?";
         Desvinculapanel_Title = "Aviso";
         Confirmpanel_Confirmtype = "1";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Sim";
         Confirmpanel_Confirmtext = "Confirma o vinculo com o Servi�o";
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "Aten��o";
         Confirmpanel_Height = "50";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Servicos (De Para)";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E14KY2',iparms:[{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV11Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11KY2',iparms:[{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A827ContratoServicos_ServicoCod',fld:'CONTRATOSERVICOS_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1466ContratoServicosDePara_Origem',fld:'CONTRATOSERVICOSDEPARA_ORIGEM',pic:'',nv:''},{av:'AV6Origem',fld:'vORIGEM',pic:'',hsh:true,nv:''},{av:'A1470ContratoServicosDePara_OrigemDsc2',fld:'CONTRATOSERVICOSDEPARA_ORIGEMDSC2',pic:'',nv:''},{av:'A1468ContratoServicosDePara_OrigemDsc',fld:'CONTRATOSERVICOSDEPARA_ORIGEMDSC',pic:'',nv:''},{av:'A1465ContratoServicosDePara_Codigo',fld:'CONTRATOSERVICOSDEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1467ContratoServicosDePara_OrigenId',fld:'CONTRATOSERVICOSDEPARA_ORIGENID',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'A1469ContratoServicosDePara_OrigenId2',fld:'CONTRATOSERVICOSDEPARA_ORIGENID2',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'cmbavTracker'},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'cmbavEstado'}],oparms:[{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Desvinculapanel_Confirmtext',ctrl:'DESVINCULAPANEL',prop:'ConfirmText'},{av:'AV23ContratoServicosDePara_Codigo',fld:'vCONTRATOSERVICOSDEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV7OrigemDsc',fld:'vORIGEMDSC',pic:'',nv:''},{av:'AV18OrigemDsc2',fld:'vORIGEMDSC2',pic:'',nv:''}]}");
         setEventMetadata("DESVINCULAPANEL.CLOSE","{handler:'E12KY2',iparms:[{av:'Desvinculapanel_Result',ctrl:'DESVINCULAPANEL',prop:'Result'},{av:'AV22ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV23ContratoServicosDePara_Codigo',fld:'vCONTRATOSERVICOSDEPARA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV7OrigemDsc',fld:'vORIGEMDSC',pic:'',nv:''},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV18OrigemDsc2',fld:'vORIGEMDSC2',pic:'',nv:''},{av:'AV6Origem',fld:'vORIGEM',pic:'',hsh:true,nv:''},{av:'cmbavTracker'},{av:'cmbavEstado'}],oparms:[{av:'AV15Tracker',fld:'vTRACKER',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0},{av:'AV16Estado',fld:'vESTADO',pic:'ZZZZZZZZZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'FECHAR'","{handler:'E15KY2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV6Origem = "";
         Confirmpanel_Result = "";
         Desvinculapanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1466ContratoServicosDePara_Origem = "";
         A1470ContratoServicosDePara_OrigemDsc2 = "";
         A1468ContratoServicosDePara_OrigemDsc = "";
         AV7OrigemDsc = "";
         AV18OrigemDsc2 = "";
         Confirmpanel_Width = "";
         Desvinculapanel_Width = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00KY3_A40000GXC1 = new int[1] ;
         AV26Trackers = new GxObjectCollection( context, "SDT_RedmineTrackers.tracker", "", "SdtSDT_RedmineTrackers_tracker", "GeneXus.Programs");
         AV14WebSession = context.GetSession();
         AV25Estados = new GxObjectCollection( context, "SDT_RedmineStatus.issue_status", "", "SdtSDT_RedmineStatus_issue_status", "GeneXus.Programs");
         AV32TrackerItem = new SdtSDT_RedmineTrackers_tracker(context);
         AV30EstadoItem = new SdtSDT_RedmineStatus_issue_status(context);
         H00KY4_A160ContratoServicos_Codigo = new int[1] ;
         H00KY4_A74Contrato_Codigo = new int[1] ;
         H00KY4_A39Contratada_Codigo = new int[1] ;
         H00KY4_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00KY4_A826ContratoServicos_ServicoSigla = new String[] {""} ;
         H00KY4_A605Servico_Sigla = new String[] {""} ;
         H00KY4_A155Servico_Codigo = new int[1] ;
         A826ContratoServicos_ServicoSigla = "";
         A605Servico_Sigla = "";
         H00KY5_A74Contrato_Codigo = new int[1] ;
         H00KY5_A160ContratoServicos_Codigo = new int[1] ;
         H00KY5_A827ContratoServicos_ServicoCod = new int[1] ;
         H00KY5_A39Contratada_Codigo = new int[1] ;
         H00KY5_A155Servico_Codigo = new int[1] ;
         H00KY6_A160ContratoServicos_Codigo = new int[1] ;
         H00KY6_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         H00KY6_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         H00KY6_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         H00KY6_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         H00KY6_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         H00KY6_A1465ContratoServicosDePara_Codigo = new int[1] ;
         H00KY6_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         H00KY6_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         H00KY6_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         H00KY6_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         AV12ContratoServicosDePara = new SdtContratoServicosDePara(context);
         H00KY8_A40000GXC1 = new int[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_servicosdepara__default(),
            new Object[][] {
                new Object[] {
               H00KY3_A40000GXC1
               }
               , new Object[] {
               H00KY4_A160ContratoServicos_Codigo, H00KY4_A74Contrato_Codigo, H00KY4_A39Contratada_Codigo, H00KY4_A638ContratoServicos_Ativo, H00KY4_A826ContratoServicos_ServicoSigla, H00KY4_A605Servico_Sigla, H00KY4_A155Servico_Codigo
               }
               , new Object[] {
               H00KY5_A74Contrato_Codigo, H00KY5_A160ContratoServicos_Codigo, H00KY5_A827ContratoServicos_ServicoCod, H00KY5_A39Contratada_Codigo, H00KY5_A155Servico_Codigo
               }
               , new Object[] {
               H00KY6_A160ContratoServicos_Codigo, H00KY6_A1466ContratoServicosDePara_Origem, H00KY6_A1470ContratoServicosDePara_OrigemDsc2, H00KY6_n1470ContratoServicosDePara_OrigemDsc2, H00KY6_A1468ContratoServicosDePara_OrigemDsc, H00KY6_n1468ContratoServicosDePara_OrigemDsc, H00KY6_A1465ContratoServicosDePara_Codigo, H00KY6_A1467ContratoServicosDePara_OrigenId, H00KY6_n1467ContratoServicosDePara_OrigenId, H00KY6_A1469ContratoServicosDePara_OrigenId2,
               H00KY6_n1469ContratoServicosDePara_OrigenId2
               }
               , new Object[] {
               H00KY8_A40000GXC1
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV5i ;
      private short nGXWrapped ;
      private int AV35Contratada_Codigo ;
      private int wcpOAV35Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int A827ContratoServicos_ServicoCod ;
      private int A160ContratoServicos_Codigo ;
      private int A1465ContratoServicosDePara_Codigo ;
      private int AV22ContratoServicos_Codigo ;
      private int AV23ContratoServicosDePara_Codigo ;
      private int A40000GXC1 ;
      private int lblTbjava_Visible ;
      private int AV11Servico_Codigo ;
      private int AV38GXV1 ;
      private int AV39GXV2 ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int idxLst ;
      private long A1467ContratoServicosDePara_OrigenId ;
      private long A1469ContratoServicosDePara_OrigenId2 ;
      private long AV15Tracker ;
      private long AV16Estado ;
      private String AV6Origem ;
      private String wcpOAV6Origem ;
      private String Confirmpanel_Result ;
      private String Desvinculapanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1466ContratoServicosDePara_Origem ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Height ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Confirmtype ;
      private String Desvinculapanel_Width ;
      private String Desvinculapanel_Title ;
      private String Desvinculapanel_Confirmtext ;
      private String Desvinculapanel_Buttonyestext ;
      private String Desvinculapanel_Buttonnotext ;
      private String Desvinculapanel_Buttoncanceltext ;
      private String Desvinculapanel_Confirmtype ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavTracker_Internalname ;
      private String scmdbuf ;
      private String cmbavEstado_Internalname ;
      private String cmbavServico_codigo_Internalname ;
      private String A826ContratoServicos_ServicoSigla ;
      private String A605Servico_Sigla ;
      private String Desvinculapanel_Internalname ;
      private String sStyleString ;
      private String tblTable3_Internalname ;
      private String tblTable2_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String cmbavTracker_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String cmbavEstado_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String cmbavServico_codigo_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String Confirmpanel_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A638ContratoServicos_Ativo ;
      private bool n1470ContratoServicosDePara_OrigemDsc2 ;
      private bool n1468ContratoServicosDePara_OrigemDsc ;
      private bool n1467ContratoServicosDePara_OrigenId ;
      private bool n1469ContratoServicosDePara_OrigenId2 ;
      private String A1470ContratoServicosDePara_OrigemDsc2 ;
      private String A1468ContratoServicosDePara_OrigemDsc ;
      private String AV7OrigemDsc ;
      private String AV18OrigemDsc2 ;
      private IGxSession AV14WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavTracker ;
      private GXCombobox cmbavEstado ;
      private GXCombobox cmbavServico_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00KY3_A40000GXC1 ;
      private int[] H00KY4_A160ContratoServicos_Codigo ;
      private int[] H00KY4_A74Contrato_Codigo ;
      private int[] H00KY4_A39Contratada_Codigo ;
      private bool[] H00KY4_A638ContratoServicos_Ativo ;
      private String[] H00KY4_A826ContratoServicos_ServicoSigla ;
      private String[] H00KY4_A605Servico_Sigla ;
      private int[] H00KY4_A155Servico_Codigo ;
      private int[] H00KY5_A74Contrato_Codigo ;
      private int[] H00KY5_A160ContratoServicos_Codigo ;
      private int[] H00KY5_A827ContratoServicos_ServicoCod ;
      private int[] H00KY5_A39Contratada_Codigo ;
      private int[] H00KY5_A155Servico_Codigo ;
      private int[] H00KY6_A160ContratoServicos_Codigo ;
      private String[] H00KY6_A1466ContratoServicosDePara_Origem ;
      private String[] H00KY6_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] H00KY6_n1470ContratoServicosDePara_OrigemDsc2 ;
      private String[] H00KY6_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] H00KY6_n1468ContratoServicosDePara_OrigemDsc ;
      private int[] H00KY6_A1465ContratoServicosDePara_Codigo ;
      private long[] H00KY6_A1467ContratoServicosDePara_OrigenId ;
      private bool[] H00KY6_n1467ContratoServicosDePara_OrigenId ;
      private long[] H00KY6_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] H00KY6_n1469ContratoServicosDePara_OrigenId2 ;
      private int[] H00KY8_A40000GXC1 ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineStatus_issue_status ))]
      private IGxCollection AV25Estados ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineTrackers_tracker ))]
      private IGxCollection AV26Trackers ;
      private GXWebForm Form ;
      private SdtSDT_RedmineStatus_issue_status AV30EstadoItem ;
      private SdtSDT_RedmineTrackers_tracker AV32TrackerItem ;
      private SdtContratoServicosDePara AV12ContratoServicosDePara ;
   }

   public class wp_servicosdepara__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KY3 ;
          prmH00KY3 = new Object[] {
          new Object[] {"@AV22ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KY4 ;
          prmH00KY4 = new Object[] {
          new Object[] {"@AV35Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KY5 ;
          prmH00KY5 = new Object[] {
          new Object[] {"@AV35Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00KY6 ;
          prmH00KY6 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6Origem",SqlDbType.Char,2,0}
          } ;
          Object[] prmH00KY8 ;
          prmH00KY8 = new Object[] {
          new Object[] {"@AV22ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KY3", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV22ContratoServicos_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KY3,1,0,true,false )
             ,new CursorDef("H00KY4", "SELECT T1.[ContratoServicos_Codigo], T1.[Contrato_Codigo], T2.[Contratada_Codigo], T1.[ContratoServicos_Ativo], T3.[Servico_Sigla] AS ContratoServicos_ServicoSigla, T3.[Servico_Sigla], T1.[Servico_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T2.[Contratada_Codigo] = @AV35Contratada_Codigo) ORDER BY [ContratoServicos_ServicoSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KY4,100,0,false,false )
             ,new CursorDef("H00KY5", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo] AS ContratoServicos_ServicoCod, T2.[Contratada_Codigo], T1.[Servico_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T2.[Contratada_Codigo] = @AV35Contratada_Codigo) AND (T1.[Servico_Codigo] = @AV11Servico_Codigo) ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KY5,1,0,true,true )
             ,new CursorDef("H00KY6", "SELECT [ContratoServicos_Codigo], [ContratoServicosDePara_Origem], [ContratoServicosDePara_OrigemDsc2], [ContratoServicosDePara_OrigemDsc], [ContratoServicosDePara_Codigo], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_OrigenId2] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE ([ContratoServicos_Codigo] = @ContratoServicos_Codigo) AND ([ContratoServicosDePara_Origem] = @AV6Origem) ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KY6,100,0,false,false )
             ,new CursorDef("H00KY8", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV22ContratoServicos_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KY8,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((long[]) buf[7])[0] = rslt.getLong(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((long[]) buf[9])[0] = rslt.getLong(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
