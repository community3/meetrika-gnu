/*
               File: GetContratoContratoClausulasWCFilterData
        Description: Get Contrato Contrato Clausulas WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/28/2019 16:48:32.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratoclausulaswcfilterdata : GXProcedure
   {
      public getcontratocontratoclausulaswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratoclausulaswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratoclausulaswcfilterdata objgetcontratocontratoclausulaswcfilterdata;
         objgetcontratocontratoclausulaswcfilterdata = new getcontratocontratoclausulaswcfilterdata();
         objgetcontratocontratoclausulaswcfilterdata.AV16DDOName = aP0_DDOName;
         objgetcontratocontratoclausulaswcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetcontratocontratoclausulaswcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratoclausulaswcfilterdata.AV20OptionsJson = "" ;
         objgetcontratocontratoclausulaswcfilterdata.AV23OptionsDescJson = "" ;
         objgetcontratocontratoclausulaswcfilterdata.AV25OptionIndexesJson = "" ;
         objgetcontratocontratoclausulaswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratoclausulaswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratoclausulaswcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratoclausulaswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATOCLAUSULAS_ITEM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOCLAUSULAS_ITEMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATOCLAUSULAS_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOCLAUSULAS_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("ContratoContratoClausulasWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoClausulasWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("ContratoContratoClausulasWCGridState"), "");
         }
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV35GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM") == 0 )
            {
               AV10TFContratoClausulas_Item = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM_SEL") == 0 )
            {
               AV11TFContratoClausulas_Item_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO") == 0 )
            {
               AV12TFContratoClausulas_Descricao = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO_SEL") == 0 )
            {
               AV13TFContratoClausulas_Descricao_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV32Contrato_Codigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV35GXV1 = (int)(AV35GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOCLAUSULAS_ITEMOPTIONS' Routine */
         AV10TFContratoClausulas_Item = AV14SearchTxt;
         AV11TFContratoClausulas_Item_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFContratoClausulas_Item_Sel ,
                                              AV10TFContratoClausulas_Item ,
                                              AV13TFContratoClausulas_Descricao_Sel ,
                                              AV12TFContratoClausulas_Descricao ,
                                              A153ContratoClausulas_Item ,
                                              A154ContratoClausulas_Descricao ,
                                              AV32Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoClausulas_Item = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoClausulas_Item), 10, "%");
         lV12TFContratoClausulas_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoClausulas_Descricao), "%", "");
         /* Using cursor P00UC2 */
         pr_default.execute(0, new Object[] {AV32Contrato_Codigo, lV10TFContratoClausulas_Item, AV11TFContratoClausulas_Item_Sel, lV12TFContratoClausulas_Descricao, AV13TFContratoClausulas_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUC2 = false;
            A74Contrato_Codigo = P00UC2_A74Contrato_Codigo[0];
            A153ContratoClausulas_Item = P00UC2_A153ContratoClausulas_Item[0];
            A154ContratoClausulas_Descricao = P00UC2_A154ContratoClausulas_Descricao[0];
            A152ContratoClausulas_Codigo = P00UC2_A152ContratoClausulas_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UC2_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UC2_A153ContratoClausulas_Item[0], A153ContratoClausulas_Item) == 0 ) )
            {
               BRKUC2 = false;
               A152ContratoClausulas_Codigo = P00UC2_A152ContratoClausulas_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKUC2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A153ContratoClausulas_Item)) )
            {
               AV18Option = A153ContratoClausulas_Item;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUC2 )
            {
               BRKUC2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOCLAUSULAS_DESCRICAOOPTIONS' Routine */
         AV12TFContratoClausulas_Descricao = AV14SearchTxt;
         AV13TFContratoClausulas_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV11TFContratoClausulas_Item_Sel ,
                                              AV10TFContratoClausulas_Item ,
                                              AV13TFContratoClausulas_Descricao_Sel ,
                                              AV12TFContratoClausulas_Descricao ,
                                              A153ContratoClausulas_Item ,
                                              A154ContratoClausulas_Descricao ,
                                              AV32Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoClausulas_Item = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoClausulas_Item), 10, "%");
         lV12TFContratoClausulas_Descricao = StringUtil.Concat( StringUtil.RTrim( AV12TFContratoClausulas_Descricao), "%", "");
         /* Using cursor P00UC3 */
         pr_default.execute(1, new Object[] {AV32Contrato_Codigo, lV10TFContratoClausulas_Item, AV11TFContratoClausulas_Item_Sel, lV12TFContratoClausulas_Descricao, AV13TFContratoClausulas_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUC4 = false;
            A74Contrato_Codigo = P00UC3_A74Contrato_Codigo[0];
            A154ContratoClausulas_Descricao = P00UC3_A154ContratoClausulas_Descricao[0];
            A153ContratoClausulas_Item = P00UC3_A153ContratoClausulas_Item[0];
            A152ContratoClausulas_Codigo = P00UC3_A152ContratoClausulas_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00UC3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UC3_A154ContratoClausulas_Descricao[0], A154ContratoClausulas_Descricao) == 0 ) )
            {
               BRKUC4 = false;
               A152ContratoClausulas_Codigo = P00UC3_A152ContratoClausulas_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKUC4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A154ContratoClausulas_Descricao)) )
            {
               AV18Option = A154ContratoClausulas_Descricao;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUC4 )
            {
               BRKUC4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoClausulas_Item = "";
         AV11TFContratoClausulas_Item_Sel = "";
         AV12TFContratoClausulas_Descricao = "";
         AV13TFContratoClausulas_Descricao_Sel = "";
         scmdbuf = "";
         lV10TFContratoClausulas_Item = "";
         lV12TFContratoClausulas_Descricao = "";
         A153ContratoClausulas_Item = "";
         A154ContratoClausulas_Descricao = "";
         P00UC2_A74Contrato_Codigo = new int[1] ;
         P00UC2_A153ContratoClausulas_Item = new String[] {""} ;
         P00UC2_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00UC2_A152ContratoClausulas_Codigo = new int[1] ;
         AV18Option = "";
         P00UC3_A74Contrato_Codigo = new int[1] ;
         P00UC3_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00UC3_A153ContratoClausulas_Item = new String[] {""} ;
         P00UC3_A152ContratoClausulas_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratoclausulaswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UC2_A74Contrato_Codigo, P00UC2_A153ContratoClausulas_Item, P00UC2_A154ContratoClausulas_Descricao, P00UC2_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               P00UC3_A74Contrato_Codigo, P00UC3_A154ContratoClausulas_Descricao, P00UC3_A153ContratoClausulas_Item, P00UC3_A152ContratoClausulas_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV35GXV1 ;
      private int AV32Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A152ContratoClausulas_Codigo ;
      private long AV26count ;
      private String AV10TFContratoClausulas_Item ;
      private String AV11TFContratoClausulas_Item_Sel ;
      private String scmdbuf ;
      private String lV10TFContratoClausulas_Item ;
      private String A153ContratoClausulas_Item ;
      private bool returnInSub ;
      private bool BRKUC2 ;
      private bool BRKUC4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String A154ContratoClausulas_Descricao ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV12TFContratoClausulas_Descricao ;
      private String AV13TFContratoClausulas_Descricao_Sel ;
      private String lV12TFContratoClausulas_Descricao ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UC2_A74Contrato_Codigo ;
      private String[] P00UC2_A153ContratoClausulas_Item ;
      private String[] P00UC2_A154ContratoClausulas_Descricao ;
      private int[] P00UC2_A152ContratoClausulas_Codigo ;
      private int[] P00UC3_A74Contrato_Codigo ;
      private String[] P00UC3_A154ContratoClausulas_Descricao ;
      private String[] P00UC3_A153ContratoClausulas_Item ;
      private int[] P00UC3_A152ContratoClausulas_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
   }

   public class getcontratocontratoclausulaswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UC2( IGxContext context ,
                                             String AV11TFContratoClausulas_Item_Sel ,
                                             String AV10TFContratoClausulas_Item ,
                                             String AV13TFContratoClausulas_Descricao_Sel ,
                                             String AV12TFContratoClausulas_Descricao ,
                                             String A153ContratoClausulas_Item ,
                                             String A154ContratoClausulas_Descricao ,
                                             int AV32Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Contrato_Codigo], [ContratoClausulas_Item], [ContratoClausulas_Descricao], [ContratoClausulas_Codigo] FROM [ContratoClausulas] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV32Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoClausulas_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoClausulas_Item)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Item] like @lV10TFContratoClausulas_Item)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoClausulas_Item_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Item] = @AV11TFContratoClausulas_Item_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoClausulas_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Descricao] like @lV12TFContratoClausulas_Descricao)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Descricao] = @AV13TFContratoClausulas_Descricao_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo], [ContratoClausulas_Item]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UC3( IGxContext context ,
                                             String AV11TFContratoClausulas_Item_Sel ,
                                             String AV10TFContratoClausulas_Item ,
                                             String AV13TFContratoClausulas_Descricao_Sel ,
                                             String AV12TFContratoClausulas_Descricao ,
                                             String A153ContratoClausulas_Item ,
                                             String A154ContratoClausulas_Descricao ,
                                             int AV32Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [5] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Contrato_Codigo], [ContratoClausulas_Descricao], [ContratoClausulas_Item], [ContratoClausulas_Codigo] FROM [ContratoClausulas] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV32Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoClausulas_Item_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoClausulas_Item)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Item] like @lV10TFContratoClausulas_Item)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoClausulas_Item_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Item] = @AV11TFContratoClausulas_Item_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoClausulas_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Descricao] like @lV12TFContratoClausulas_Descricao)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoClausulas_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoClausulas_Descricao] = @AV13TFContratoClausulas_Descricao_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo], [ContratoClausulas_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UC2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 1 :
                     return conditional_P00UC3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UC2 ;
          prmP00UC2 = new Object[] {
          new Object[] {"@AV32Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoClausulas_Item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV11TFContratoClausulas_Item_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV12TFContratoClausulas_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFContratoClausulas_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00UC3 ;
          prmP00UC3 = new Object[] {
          new Object[] {"@AV32Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoClausulas_Item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV11TFContratoClausulas_Item_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV12TFContratoClausulas_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFContratoClausulas_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UC2,100,0,true,false )
             ,new CursorDef("P00UC3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UC3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratoclausulaswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratoclausulaswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratoclausulaswcfilterdata") )
          {
             return  ;
          }
          getcontratocontratoclausulaswcfilterdata worker = new getcontratocontratoclausulaswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
