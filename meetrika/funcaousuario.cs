/*
               File: FuncaoUsuario
        Description: Fun��es de Usu�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:11.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaousuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel3"+"_"+"FUNCAOUSUARIO_PF") == 0 )
         {
            A161FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX3ASAFUNCAOUSUARIO_PF0Y35( A161FuncaoUsuario_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoUsuario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAOUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoUsuario_Codigo), "ZZZZZ9")));
               A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkFuncaoUsuario_Ativo.Name = "FUNCAOUSUARIO_ATIVO";
         chkFuncaoUsuario_Ativo.WebTags = "";
         chkFuncaoUsuario_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoUsuario_Ativo_Internalname, "TitleCaption", chkFuncaoUsuario_Ativo.Caption);
         chkFuncaoUsuario_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Fun��es de Usu�rio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcaousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_FuncaoUsuario_Codigo ,
                           ref int aP2_Sistema_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7FuncaoUsuario_Codigo = aP1_FuncaoUsuario_Codigo;
         this.A127Sistema_Codigo = aP2_Sistema_Codigo;
         executePrivate();
         aP2_Sistema_Codigo=this.A127Sistema_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkFuncaoUsuario_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0Y35( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0Y35e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoUsuario_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")), ((edtFuncaoUsuario_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoUsuario_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoUsuario_Codigo_Visible, edtFuncaoUsuario_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoUsuario.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0Y35( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0Y35( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0Y35e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_0Y35( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_0Y35e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0Y35e( true) ;
         }
         else
         {
            wb_table1_2_0Y35e( false) ;
         }
      }

      protected void wb_table3_31_0Y35( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_0Y35e( true) ;
         }
         else
         {
            wb_table3_31_0Y35e( false) ;
         }
      }

      protected void wb_table2_5_0Y35( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0Y35( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0Y35e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0Y35e( true) ;
         }
         else
         {
            wb_table2_5_0Y35e( false) ;
         }
      }

      protected void wb_table4_13_0Y35( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaousuario_nome_Internalname, "Nome", "", "", lblTextblockfuncaousuario_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoUsuario_Nome_Internalname, A162FuncaoUsuario_Nome, StringUtil.RTrim( context.localUtil.Format( A162FuncaoUsuario_Nome, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoUsuario_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFuncaoUsuario_Nome_Enabled, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaousuario_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncaousuario_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoUsuario_Descricao_Internalname, A163FuncaoUsuario_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtFuncaoUsuario_Descricao_Enabled, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaousuario_ativo_Internalname, "Ativo", "", "", lblTextblockfuncaousuario_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockfuncaousuario_ativo_Visible, 1, 0, "HLP_FuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFuncaoUsuario_Ativo_Internalname, StringUtil.BoolToStr( A164FuncaoUsuario_Ativo), "", "", chkFuncaoUsuario_Ativo.Visible, chkFuncaoUsuario_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(28, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0Y35e( true) ;
         }
         else
         {
            wb_table4_13_0Y35e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E110Y2 */
         E110Y2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A162FuncaoUsuario_Nome = cgiGet( edtFuncaoUsuario_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
               A163FuncaoUsuario_Descricao = cgiGet( edtFuncaoUsuario_Descricao_Internalname);
               n163FuncaoUsuario_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A163FuncaoUsuario_Descricao", A163FuncaoUsuario_Descricao);
               n163FuncaoUsuario_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A163FuncaoUsuario_Descricao)) ? true : false);
               A164FuncaoUsuario_Ativo = StringUtil.StrToBool( cgiGet( chkFuncaoUsuario_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A164FuncaoUsuario_Ativo", A164FuncaoUsuario_Ativo);
               A161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               /* Read saved values. */
               Z161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z161FuncaoUsuario_Codigo"), ",", "."));
               Z162FuncaoUsuario_Nome = cgiGet( "Z162FuncaoUsuario_Nome");
               Z164FuncaoUsuario_Ativo = StringUtil.StrToBool( cgiGet( "Z164FuncaoUsuario_Ativo"));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "N127Sistema_Codigo"), ",", "."));
               A396FuncaoUsuario_PF = context.localUtil.CToN( cgiGet( "FUNCAOUSUARIO_PF"), ",", ".");
               AV7FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( "vFUNCAOUSUARIO_CODIGO"), ",", "."));
               AV12Insert_Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMA_CODIGO"), ",", "."));
               A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "SISTEMA_CODIGO"), ",", "."));
               A416Sistema_Nome = cgiGet( "SISTEMA_NOME");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "FuncaoUsuario";
               A161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("funcaousuario:[SecurityCheckFailed value for]"+"FuncaoUsuario_Codigo:"+context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("funcaousuario:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A161FuncaoUsuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode35 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode35;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound35 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0Y0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "FUNCAOUSUARIO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E110Y2 */
                           E110Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120Y2 */
                           E120Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E120Y2 */
            E120Y2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0Y35( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0Y35( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0Y0( )
      {
         BeforeValidate0Y35( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0Y35( ) ;
            }
            else
            {
               CheckExtendedTable0Y35( ) ;
               CloseExtendedTableCursors0Y35( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0Y0( )
      {
      }

      protected void E110Y2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV11TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV11TrnContextAtt.gxTpr_Attributename, "Sistema_Codigo") == 0 )
               {
                  AV12Insert_Sistema_Codigo = (int)(NumberUtil.Val( AV11TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Sistema_Codigo), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtFuncaoUsuario_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Codigo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            lblTextblockfuncaousuario_ativo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockfuncaousuario_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockfuncaousuario_ativo_Visible), 5, 0)));
            chkFuncaoUsuario_Ativo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoUsuario_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncaoUsuario_Ativo.Visible), 5, 0)));
         }
      }

      protected void E120Y2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwfuncaousuario.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A127Sistema_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0Y35( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z162FuncaoUsuario_Nome = T000Y3_A162FuncaoUsuario_Nome[0];
               Z164FuncaoUsuario_Ativo = T000Y3_A164FuncaoUsuario_Ativo[0];
            }
            else
            {
               Z162FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
               Z164FuncaoUsuario_Ativo = A164FuncaoUsuario_Ativo;
            }
         }
         if ( GX_JID == -11 )
         {
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            Z162FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
            Z163FuncaoUsuario_Descricao = A163FuncaoUsuario_Descricao;
            Z164FuncaoUsuario_Ativo = A164FuncaoUsuario_Ativo;
            Z416Sistema_Nome = A416Sistema_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         edtFuncaoUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV13Pgmname = "FuncaoUsuario";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtFuncaoUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7FuncaoUsuario_Codigo) )
         {
            A161FuncaoUsuario_Codigo = AV7FuncaoUsuario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         }
         /* Using cursor T000Y4 */
         pr_default.execute(2, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A416Sistema_Nome = T000Y4_A416Sistema_Nome[0];
         pr_default.close(2);
         Dvpanel_tableattributes_Title = "Fun��o de Usu�rio do sistema: "+A416Sistema_Nome;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A164FuncaoUsuario_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A164FuncaoUsuario_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A164FuncaoUsuario_Ativo", A164FuncaoUsuario_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            GXt_int1 = (short)(A396FuncaoUsuario_PF);
            new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            A396FuncaoUsuario_PF = (decimal)(GXt_int1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Sistema_Codigo) )
            {
               A127Sistema_Codigo = AV12Insert_Sistema_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            }
         }
      }

      protected void Load0Y35( )
      {
         /* Using cursor T000Y5 */
         pr_default.execute(3, new Object[] {A161FuncaoUsuario_Codigo, A127Sistema_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound35 = 1;
            A162FuncaoUsuario_Nome = T000Y5_A162FuncaoUsuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
            A163FuncaoUsuario_Descricao = T000Y5_A163FuncaoUsuario_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A163FuncaoUsuario_Descricao", A163FuncaoUsuario_Descricao);
            n163FuncaoUsuario_Descricao = T000Y5_n163FuncaoUsuario_Descricao[0];
            A416Sistema_Nome = T000Y5_A416Sistema_Nome[0];
            A164FuncaoUsuario_Ativo = T000Y5_A164FuncaoUsuario_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A164FuncaoUsuario_Ativo", A164FuncaoUsuario_Ativo);
            ZM0Y35( -11) ;
         }
         pr_default.close(3);
         OnLoadActions0Y35( ) ;
      }

      protected void OnLoadActions0Y35( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Sistema_Codigo) )
         {
            A127Sistema_Codigo = AV12Insert_Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         GXt_int1 = (short)(A396FuncaoUsuario_PF);
         new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         A396FuncaoUsuario_PF = (decimal)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
      }

      protected void CheckExtendedTable0Y35( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Sistema_Codigo) )
         {
            A127Sistema_Codigo = AV12Insert_Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         GXt_int1 = (short)(A396FuncaoUsuario_PF);
         new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         A396FuncaoUsuario_PF = (decimal)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A162FuncaoUsuario_Nome)) )
         {
            GX_msglist.addItem("Fun��o de Usu�rio � obrigat�rio.", 1, "FUNCAOUSUARIO_NOME");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0Y35( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0Y35( )
      {
         /* Using cursor T000Y6 */
         pr_default.execute(4, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound35 = 1;
         }
         else
         {
            RcdFound35 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000Y3 */
         pr_default.execute(1, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T000Y3_A127Sistema_Codigo[0] == A127Sistema_Codigo ) )
         {
            ZM0Y35( 11) ;
            RcdFound35 = 1;
            A161FuncaoUsuario_Codigo = T000Y3_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            A162FuncaoUsuario_Nome = T000Y3_A162FuncaoUsuario_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
            A163FuncaoUsuario_Descricao = T000Y3_A163FuncaoUsuario_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A163FuncaoUsuario_Descricao", A163FuncaoUsuario_Descricao);
            n163FuncaoUsuario_Descricao = T000Y3_n163FuncaoUsuario_Descricao[0];
            A164FuncaoUsuario_Ativo = T000Y3_A164FuncaoUsuario_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A164FuncaoUsuario_Ativo", A164FuncaoUsuario_Ativo);
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
            sMode35 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0Y35( ) ;
            if ( AnyError == 1 )
            {
               RcdFound35 = 0;
               InitializeNonKey0Y35( ) ;
            }
            Gx_mode = sMode35;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound35 = 0;
            InitializeNonKey0Y35( ) ;
            sMode35 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode35;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0Y35( ) ;
         if ( RcdFound35 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound35 = 0;
         /* Using cursor T000Y7 */
         pr_default.execute(5, new Object[] {A161FuncaoUsuario_Codigo, A127Sistema_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T000Y7_A161FuncaoUsuario_Codigo[0] < A161FuncaoUsuario_Codigo ) ) && ( T000Y7_A127Sistema_Codigo[0] == A127Sistema_Codigo ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T000Y7_A161FuncaoUsuario_Codigo[0] > A161FuncaoUsuario_Codigo ) ) && ( T000Y7_A127Sistema_Codigo[0] == A127Sistema_Codigo ) )
            {
               A161FuncaoUsuario_Codigo = T000Y7_A161FuncaoUsuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               RcdFound35 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void move_previous( )
      {
         RcdFound35 = 0;
         /* Using cursor T000Y8 */
         pr_default.execute(6, new Object[] {A161FuncaoUsuario_Codigo, A127Sistema_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T000Y8_A161FuncaoUsuario_Codigo[0] > A161FuncaoUsuario_Codigo ) ) && ( T000Y8_A127Sistema_Codigo[0] == A127Sistema_Codigo ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T000Y8_A161FuncaoUsuario_Codigo[0] < A161FuncaoUsuario_Codigo ) ) && ( T000Y8_A127Sistema_Codigo[0] == A127Sistema_Codigo ) )
            {
               A161FuncaoUsuario_Codigo = T000Y8_A161FuncaoUsuario_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
               RcdFound35 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0Y35( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0Y35( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound35 == 1 )
            {
               if ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo )
               {
                  A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0Y35( ) ;
                  GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0Y35( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAOUSUARIO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0Y35( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A161FuncaoUsuario_Codigo != Z161FuncaoUsuario_Codigo )
         {
            A161FuncaoUsuario_Codigo = Z161FuncaoUsuario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoUsuario_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFuncaoUsuario_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0Y35( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000Y2 */
            pr_default.execute(0, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ModuloFuncoes"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z162FuncaoUsuario_Nome, T000Y2_A162FuncaoUsuario_Nome[0]) != 0 ) || ( Z164FuncaoUsuario_Ativo != T000Y2_A164FuncaoUsuario_Ativo[0] ) )
            {
               if ( StringUtil.StrCmp(Z162FuncaoUsuario_Nome, T000Y2_A162FuncaoUsuario_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaousuario:[seudo value changed for attri]"+"FuncaoUsuario_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z162FuncaoUsuario_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000Y2_A162FuncaoUsuario_Nome[0]);
               }
               if ( Z164FuncaoUsuario_Ativo != T000Y2_A164FuncaoUsuario_Ativo[0] )
               {
                  GXUtil.WriteLog("funcaousuario:[seudo value changed for attri]"+"FuncaoUsuario_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z164FuncaoUsuario_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000Y2_A164FuncaoUsuario_Ativo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ModuloFuncoes"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0Y35( )
      {
         BeforeValidate0Y35( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0Y35( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0Y35( 0) ;
            CheckOptimisticConcurrency0Y35( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0Y35( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0Y35( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000Y9 */
                     pr_default.execute(7, new Object[] {A127Sistema_Codigo, A162FuncaoUsuario_Nome, n163FuncaoUsuario_Descricao, A163FuncaoUsuario_Descricao, A164FuncaoUsuario_Ativo});
                     A161FuncaoUsuario_Codigo = T000Y9_A161FuncaoUsuario_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ModuloFuncoes") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0Y0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0Y35( ) ;
            }
            EndLevel0Y35( ) ;
         }
         CloseExtendedTableCursors0Y35( ) ;
      }

      protected void Update0Y35( )
      {
         BeforeValidate0Y35( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0Y35( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0Y35( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0Y35( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0Y35( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000Y10 */
                     pr_default.execute(8, new Object[] {A127Sistema_Codigo, A162FuncaoUsuario_Nome, n163FuncaoUsuario_Descricao, A163FuncaoUsuario_Descricao, A164FuncaoUsuario_Ativo, A161FuncaoUsuario_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ModuloFuncoes") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ModuloFuncoes"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0Y35( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0Y35( ) ;
         }
         CloseExtendedTableCursors0Y35( ) ;
      }

      protected void DeferredUpdate0Y35( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0Y35( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0Y35( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0Y35( ) ;
            AfterConfirm0Y35( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0Y35( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000Y11 */
                  pr_default.execute(9, new Object[] {A161FuncaoUsuario_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("ModuloFuncoes") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode35 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0Y35( ) ;
         Gx_mode = sMode35;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0Y35( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = (short)(A396FuncaoUsuario_PF);
            new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
            A396FuncaoUsuario_PF = (decimal)(GXt_int1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000Y12 */
            pr_default.execute(10, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes Itens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor T000Y13 */
            pr_default.execute(11, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcoes Usuario Funcoes APF"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor T000Y14 */
            pr_default.execute(12, new Object[] {A161FuncaoUsuario_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Modulo Funcoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void EndLevel0Y35( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0Y35( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "FuncaoUsuario");
            if ( AnyError == 0 )
            {
               ConfirmValues0Y0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "FuncaoUsuario");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0Y35( )
      {
         /* Scan By routine */
         /* Using cursor T000Y15 */
         pr_default.execute(13, new Object[] {A127Sistema_Codigo});
         RcdFound35 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound35 = 1;
            A161FuncaoUsuario_Codigo = T000Y15_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0Y35( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound35 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound35 = 1;
            A161FuncaoUsuario_Codigo = T000Y15_A161FuncaoUsuario_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0Y35( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm0Y35( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0Y35( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0Y35( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0Y35( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0Y35( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0Y35( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0Y35( )
      {
         edtFuncaoUsuario_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Nome_Enabled), 5, 0)));
         edtFuncaoUsuario_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Descricao_Enabled), 5, 0)));
         chkFuncaoUsuario_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncaoUsuario_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFuncaoUsuario_Ativo.Enabled), 5, 0)));
         edtFuncaoUsuario_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoUsuario_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0Y0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117181217");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaousuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoUsuario_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z162FuncaoUsuario_Nome", Z162FuncaoUsuario_Nome);
         GxWebStd.gx_boolean_hidden_field( context, "Z164FuncaoUsuario_Ativo", Z164FuncaoUsuario_Ativo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_PF", StringUtil.LTrim( StringUtil.NToC( A396FuncaoUsuario_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAOUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7FuncaoUsuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "FuncaoUsuario";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("funcaousuario:[SendSecurityCheck value for]"+"FuncaoUsuario_Codigo:"+context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("funcaousuario:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcaousuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7FuncaoUsuario_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "FuncaoUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Fun��es de Usu�rio" ;
      }

      protected void InitializeNonKey0Y35( )
      {
         A396FuncaoUsuario_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
         A162FuncaoUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A162FuncaoUsuario_Nome", A162FuncaoUsuario_Nome);
         A163FuncaoUsuario_Descricao = "";
         n163FuncaoUsuario_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A163FuncaoUsuario_Descricao", A163FuncaoUsuario_Descricao);
         n163FuncaoUsuario_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A163FuncaoUsuario_Descricao)) ? true : false);
         A164FuncaoUsuario_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A164FuncaoUsuario_Ativo", A164FuncaoUsuario_Ativo);
         Z162FuncaoUsuario_Nome = "";
         Z164FuncaoUsuario_Ativo = false;
      }

      protected void InitAll0Y35( )
      {
         A161FuncaoUsuario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         InitializeNonKey0Y35( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A164FuncaoUsuario_Ativo = i164FuncaoUsuario_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A164FuncaoUsuario_Ativo", A164FuncaoUsuario_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117181237");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcaousuario.js", "?20203117181237");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaousuario_nome_Internalname = "TEXTBLOCKFUNCAOUSUARIO_NOME";
         edtFuncaoUsuario_Nome_Internalname = "FUNCAOUSUARIO_NOME";
         lblTextblockfuncaousuario_descricao_Internalname = "TEXTBLOCKFUNCAOUSUARIO_DESCRICAO";
         edtFuncaoUsuario_Descricao_Internalname = "FUNCAOUSUARIO_DESCRICAO";
         lblTextblockfuncaousuario_ativo_Internalname = "TEXTBLOCKFUNCAOUSUARIO_ATIVO";
         chkFuncaoUsuario_Ativo_Internalname = "FUNCAOUSUARIO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtFuncaoUsuario_Codigo_Internalname = "FUNCAOUSUARIO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Fun��es de Usu�rio";
         Dvpanel_tableattributes_Title = "Fun��o de Usu�rio";
         chkFuncaoUsuario_Ativo.Enabled = 1;
         chkFuncaoUsuario_Ativo.Visible = 1;
         lblTextblockfuncaousuario_ativo_Visible = 1;
         edtFuncaoUsuario_Descricao_Enabled = 1;
         edtFuncaoUsuario_Nome_Jsonclick = "";
         edtFuncaoUsuario_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtFuncaoUsuario_Codigo_Jsonclick = "";
         edtFuncaoUsuario_Codigo_Enabled = 0;
         edtFuncaoUsuario_Codigo_Visible = 1;
         chkFuncaoUsuario_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX3ASAFUNCAOUSUARIO_PF0Y35( int A161FuncaoUsuario_Codigo )
      {
         GXt_int1 = (short)(A396FuncaoUsuario_PF);
         new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A161FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A161FuncaoUsuario_Codigo), 6, 0)));
         A396FuncaoUsuario_PF = (decimal)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A396FuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( A396FuncaoUsuario_PF, 14, 5)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A396FuncaoUsuario_PF, 14, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Funcaousuario_codigo( int GX_Parm1 ,
                                              decimal GX_Parm2 )
      {
         A161FuncaoUsuario_Codigo = GX_Parm1;
         A396FuncaoUsuario_PF = GX_Parm2;
         GXt_int1 = (short)(A396FuncaoUsuario_PF);
         new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
         A396FuncaoUsuario_PF = (decimal)(GXt_int1);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A396FuncaoUsuario_PF, 14, 5, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120Y2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z162FuncaoUsuario_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockfuncaousuario_nome_Jsonclick = "";
         A162FuncaoUsuario_Nome = "";
         lblTextblockfuncaousuario_descricao_Jsonclick = "";
         A163FuncaoUsuario_Descricao = "";
         lblTextblockfuncaousuario_ativo_Jsonclick = "";
         A416Sistema_Nome = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode35 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z163FuncaoUsuario_Descricao = "";
         Z416Sistema_Nome = "";
         T000Y4_A416Sistema_Nome = new String[] {""} ;
         T000Y5_A127Sistema_Codigo = new int[1] ;
         T000Y5_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y5_A162FuncaoUsuario_Nome = new String[] {""} ;
         T000Y5_A163FuncaoUsuario_Descricao = new String[] {""} ;
         T000Y5_n163FuncaoUsuario_Descricao = new bool[] {false} ;
         T000Y5_A416Sistema_Nome = new String[] {""} ;
         T000Y5_A164FuncaoUsuario_Ativo = new bool[] {false} ;
         T000Y6_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y3_A127Sistema_Codigo = new int[1] ;
         T000Y3_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y3_A162FuncaoUsuario_Nome = new String[] {""} ;
         T000Y3_A163FuncaoUsuario_Descricao = new String[] {""} ;
         T000Y3_n163FuncaoUsuario_Descricao = new bool[] {false} ;
         T000Y3_A164FuncaoUsuario_Ativo = new bool[] {false} ;
         T000Y7_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y7_A127Sistema_Codigo = new int[1] ;
         T000Y8_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y8_A127Sistema_Codigo = new int[1] ;
         T000Y2_A127Sistema_Codigo = new int[1] ;
         T000Y2_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y2_A162FuncaoUsuario_Nome = new String[] {""} ;
         T000Y2_A163FuncaoUsuario_Descricao = new String[] {""} ;
         T000Y2_n163FuncaoUsuario_Descricao = new bool[] {false} ;
         T000Y2_A164FuncaoUsuario_Ativo = new bool[] {false} ;
         T000Y9_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y12_A447SolicitacoesItens_Codigo = new int[1] ;
         T000Y13_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y13_A165FuncaoAPF_Codigo = new int[1] ;
         T000Y14_A146Modulo_Codigo = new int[1] ;
         T000Y14_A161FuncaoUsuario_Codigo = new int[1] ;
         T000Y15_A161FuncaoUsuario_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaousuario__default(),
            new Object[][] {
                new Object[] {
               T000Y2_A127Sistema_Codigo, T000Y2_A161FuncaoUsuario_Codigo, T000Y2_A162FuncaoUsuario_Nome, T000Y2_A163FuncaoUsuario_Descricao, T000Y2_n163FuncaoUsuario_Descricao, T000Y2_A164FuncaoUsuario_Ativo
               }
               , new Object[] {
               T000Y3_A127Sistema_Codigo, T000Y3_A161FuncaoUsuario_Codigo, T000Y3_A162FuncaoUsuario_Nome, T000Y3_A163FuncaoUsuario_Descricao, T000Y3_n163FuncaoUsuario_Descricao, T000Y3_A164FuncaoUsuario_Ativo
               }
               , new Object[] {
               T000Y4_A416Sistema_Nome
               }
               , new Object[] {
               T000Y5_A127Sistema_Codigo, T000Y5_A161FuncaoUsuario_Codigo, T000Y5_A162FuncaoUsuario_Nome, T000Y5_A163FuncaoUsuario_Descricao, T000Y5_n163FuncaoUsuario_Descricao, T000Y5_A416Sistema_Nome, T000Y5_A164FuncaoUsuario_Ativo
               }
               , new Object[] {
               T000Y6_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T000Y7_A161FuncaoUsuario_Codigo, T000Y7_A127Sistema_Codigo
               }
               , new Object[] {
               T000Y8_A161FuncaoUsuario_Codigo, T000Y8_A127Sistema_Codigo
               }
               , new Object[] {
               T000Y9_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000Y12_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               T000Y13_A161FuncaoUsuario_Codigo, T000Y13_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               T000Y14_A146Modulo_Codigo, T000Y14_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               T000Y15_A161FuncaoUsuario_Codigo
               }
            }
         );
         N127Sistema_Codigo = 0;
         Z127Sistema_Codigo = 0;
         A127Sistema_Codigo = 0;
         Z164FuncaoUsuario_Ativo = true;
         A164FuncaoUsuario_Ativo = true;
         i164FuncaoUsuario_Ativo = true;
         AV13Pgmname = "FuncaoUsuario";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound35 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short GXt_int1 ;
      private short wbTemp ;
      private int wcpOAV7FuncaoUsuario_Codigo ;
      private int wcpOA127Sistema_Codigo ;
      private int Z161FuncaoUsuario_Codigo ;
      private int N127Sistema_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int AV7FuncaoUsuario_Codigo ;
      private int A127Sistema_Codigo ;
      private int trnEnded ;
      private int edtFuncaoUsuario_Codigo_Enabled ;
      private int edtFuncaoUsuario_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtFuncaoUsuario_Nome_Enabled ;
      private int edtFuncaoUsuario_Descricao_Enabled ;
      private int lblTextblockfuncaousuario_ativo_Visible ;
      private int AV12Insert_Sistema_Codigo ;
      private int AV14GXV1 ;
      private int Z127Sistema_Codigo ;
      private int idxLst ;
      private decimal A396FuncaoUsuario_PF ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkFuncaoUsuario_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFuncaoUsuario_Nome_Internalname ;
      private String edtFuncaoUsuario_Codigo_Internalname ;
      private String edtFuncaoUsuario_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaousuario_nome_Internalname ;
      private String lblTextblockfuncaousuario_nome_Jsonclick ;
      private String edtFuncaoUsuario_Nome_Jsonclick ;
      private String lblTextblockfuncaousuario_descricao_Internalname ;
      private String lblTextblockfuncaousuario_descricao_Jsonclick ;
      private String edtFuncaoUsuario_Descricao_Internalname ;
      private String lblTextblockfuncaousuario_ativo_Internalname ;
      private String lblTextblockfuncaousuario_ativo_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode35 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Dvpanel_tableattributes_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool Z164FuncaoUsuario_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A164FuncaoUsuario_Ativo ;
      private bool n163FuncaoUsuario_Descricao ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool i164FuncaoUsuario_Ativo ;
      private String A163FuncaoUsuario_Descricao ;
      private String Z163FuncaoUsuario_Descricao ;
      private String Z162FuncaoUsuario_Nome ;
      private String A162FuncaoUsuario_Nome ;
      private String A416Sistema_Nome ;
      private String Z416Sistema_Nome ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Sistema_Codigo ;
      private GXCheckbox chkFuncaoUsuario_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T000Y4_A416Sistema_Nome ;
      private int[] T000Y5_A127Sistema_Codigo ;
      private int[] T000Y5_A161FuncaoUsuario_Codigo ;
      private String[] T000Y5_A162FuncaoUsuario_Nome ;
      private String[] T000Y5_A163FuncaoUsuario_Descricao ;
      private bool[] T000Y5_n163FuncaoUsuario_Descricao ;
      private String[] T000Y5_A416Sistema_Nome ;
      private bool[] T000Y5_A164FuncaoUsuario_Ativo ;
      private int[] T000Y6_A161FuncaoUsuario_Codigo ;
      private int[] T000Y3_A127Sistema_Codigo ;
      private int[] T000Y3_A161FuncaoUsuario_Codigo ;
      private String[] T000Y3_A162FuncaoUsuario_Nome ;
      private String[] T000Y3_A163FuncaoUsuario_Descricao ;
      private bool[] T000Y3_n163FuncaoUsuario_Descricao ;
      private bool[] T000Y3_A164FuncaoUsuario_Ativo ;
      private int[] T000Y7_A161FuncaoUsuario_Codigo ;
      private int[] T000Y7_A127Sistema_Codigo ;
      private int[] T000Y8_A161FuncaoUsuario_Codigo ;
      private int[] T000Y8_A127Sistema_Codigo ;
      private int[] T000Y2_A127Sistema_Codigo ;
      private int[] T000Y2_A161FuncaoUsuario_Codigo ;
      private String[] T000Y2_A162FuncaoUsuario_Nome ;
      private String[] T000Y2_A163FuncaoUsuario_Descricao ;
      private bool[] T000Y2_n163FuncaoUsuario_Descricao ;
      private bool[] T000Y2_A164FuncaoUsuario_Ativo ;
      private int[] T000Y9_A161FuncaoUsuario_Codigo ;
      private int[] T000Y12_A447SolicitacoesItens_Codigo ;
      private int[] T000Y13_A161FuncaoUsuario_Codigo ;
      private int[] T000Y13_A165FuncaoAPF_Codigo ;
      private int[] T000Y14_A146Modulo_Codigo ;
      private int[] T000Y14_A161FuncaoUsuario_Codigo ;
      private int[] T000Y15_A161FuncaoUsuario_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV11TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class funcaousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000Y4 ;
          prmT000Y4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y5 ;
          prmT000Y5 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y6 ;
          prmT000Y6 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y3 ;
          prmT000Y3 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y7 ;
          prmT000Y7 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y8 ;
          prmT000Y8 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y2 ;
          prmT000Y2 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y9 ;
          prmT000Y9 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoUsuario_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoUsuario_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT000Y10 ;
          prmT000Y10 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@FuncaoUsuario_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@FuncaoUsuario_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y11 ;
          prmT000Y11 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y12 ;
          prmT000Y12 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y13 ;
          prmT000Y13 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y14 ;
          prmT000Y14 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000Y15 ;
          prmT000Y15 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000Y2", "SELECT [Sistema_Codigo], [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome], [FuncaoUsuario_Descricao], [FuncaoUsuario_Ativo] FROM [ModuloFuncoes] WITH (UPDLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y2,1,0,true,false )
             ,new CursorDef("T000Y3", "SELECT [Sistema_Codigo], [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome], [FuncaoUsuario_Descricao], [FuncaoUsuario_Ativo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y3,1,0,true,false )
             ,new CursorDef("T000Y4", "SELECT [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y4,1,0,true,false )
             ,new CursorDef("T000Y5", "SELECT TM1.[Sistema_Codigo], TM1.[FuncaoUsuario_Codigo], TM1.[FuncaoUsuario_Nome], TM1.[FuncaoUsuario_Descricao], T2.[Sistema_Nome], TM1.[FuncaoUsuario_Ativo] FROM ([ModuloFuncoes] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[Sistema_Codigo]) WHERE TM1.[FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo and TM1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY TM1.[FuncaoUsuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y5,100,0,true,false )
             ,new CursorDef("T000Y6", "SELECT [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y6,1,0,true,false )
             ,new CursorDef("T000Y7", "SELECT TOP 1 [FuncaoUsuario_Codigo], [Sistema_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE ( [FuncaoUsuario_Codigo] > @FuncaoUsuario_Codigo) and [Sistema_Codigo] = @Sistema_Codigo ORDER BY [FuncaoUsuario_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y7,1,0,true,true )
             ,new CursorDef("T000Y8", "SELECT TOP 1 [FuncaoUsuario_Codigo], [Sistema_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE ( [FuncaoUsuario_Codigo] < @FuncaoUsuario_Codigo) and [Sistema_Codigo] = @Sistema_Codigo ORDER BY [FuncaoUsuario_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y8,1,0,true,true )
             ,new CursorDef("T000Y9", "INSERT INTO [ModuloFuncoes]([Sistema_Codigo], [FuncaoUsuario_Nome], [FuncaoUsuario_Descricao], [FuncaoUsuario_Ativo]) VALUES(@Sistema_Codigo, @FuncaoUsuario_Nome, @FuncaoUsuario_Descricao, @FuncaoUsuario_Ativo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000Y9)
             ,new CursorDef("T000Y10", "UPDATE [ModuloFuncoes] SET [Sistema_Codigo]=@Sistema_Codigo, [FuncaoUsuario_Nome]=@FuncaoUsuario_Nome, [FuncaoUsuario_Descricao]=@FuncaoUsuario_Descricao, [FuncaoUsuario_Ativo]=@FuncaoUsuario_Ativo  WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo", GxErrorMask.GX_NOMASK,prmT000Y10)
             ,new CursorDef("T000Y11", "DELETE FROM [ModuloFuncoes]  WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo", GxErrorMask.GX_NOMASK,prmT000Y11)
             ,new CursorDef("T000Y12", "SELECT TOP 1 [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y12,1,0,true,true )
             ,new CursorDef("T000Y13", "SELECT TOP 1 [FuncaoUsuario_Codigo], [FuncaoAPF_Codigo] FROM [FuncoesUsuarioFuncoesAPF] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y13,1,0,true,true )
             ,new CursorDef("T000Y14", "SELECT TOP 1 [Modulo_Codigo], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes1] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y14,1,0,true,true )
             ,new CursorDef("T000Y15", "SELECT [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ORDER BY [FuncaoUsuario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000Y15,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (bool)parms[4]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (bool)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
