/*
               File: PRC_AutorizacaoConsumo_SaldoContratoReservar
        Description: PRC_Autorizacao Consumo_Saldo Contrato Reservar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:3.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_autorizacaoconsumo_saldocontratoreservar : GXProcedure
   {
      public prc_autorizacaoconsumo_saldocontratoreservar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_autorizacaoconsumo_saldocontratoreservar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AutorizacaoConsumo_Codigo )
      {
         this.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_AutorizacaoConsumo_Codigo )
      {
         prc_autorizacaoconsumo_saldocontratoreservar objprc_autorizacaoconsumo_saldocontratoreservar;
         objprc_autorizacaoconsumo_saldocontratoreservar = new prc_autorizacaoconsumo_saldocontratoreservar();
         objprc_autorizacaoconsumo_saldocontratoreservar.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         objprc_autorizacaoconsumo_saldocontratoreservar.context.SetSubmitInitialConfig(context);
         objprc_autorizacaoconsumo_saldocontratoreservar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_autorizacaoconsumo_saldocontratoreservar);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_autorizacaoconsumo_saldocontratoreservar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00D22 */
         pr_default.execute(0, new Object[] {AV8AutorizacaoConsumo_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1774AutorizacaoConsumo_Codigo = P00D22_A1774AutorizacaoConsumo_Codigo[0];
            A74Contrato_Codigo = P00D22_A74Contrato_Codigo[0];
            A1777AutorizacaoConsumo_UnidadeMedicaoCod = P00D22_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
            A1779AutorizacaoConsumo_Quantidade = P00D22_A1779AutorizacaoConsumo_Quantidade[0];
            A1782AutorizacaoConsumo_Valor = P00D22_A1782AutorizacaoConsumo_Valor[0];
            AV9Contrato_Codigo = A74Contrato_Codigo;
            AV13AutorizacaoConsumo_UnidadeMedicaoCod = A1777AutorizacaoConsumo_UnidadeMedicaoCod;
            AV10AutorizacaoConsumo_Quantidade = A1779AutorizacaoConsumo_Quantidade;
            AV12AutorizacaoConsumo_Valor = A1782AutorizacaoConsumo_Valor;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00D23 */
         pr_default.execute(1, new Object[] {AV9Contrato_Codigo, AV13AutorizacaoConsumo_UnidadeMedicaoCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1781SaldoContrato_Ativo = P00D23_A1781SaldoContrato_Ativo[0];
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00D23_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P00D23_A74Contrato_Codigo[0];
            A1561SaldoContrato_Codigo = P00D23_A1561SaldoContrato_Codigo[0];
            AV11SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         new prc_saldocontratoreservar(context ).execute(  AV11SaldoContrato_Codigo,  AV9Contrato_Codigo,  0,  0,  AV8AutorizacaoConsumo_Codigo,  AV12AutorizacaoConsumo_Valor) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00D22_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         P00D22_A74Contrato_Codigo = new int[1] ;
         P00D22_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         P00D22_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         P00D22_A1782AutorizacaoConsumo_Valor = new decimal[1] ;
         P00D23_A1781SaldoContrato_Ativo = new bool[] {false} ;
         P00D23_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         P00D23_A74Contrato_Codigo = new int[1] ;
         P00D23_A1561SaldoContrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_autorizacaoconsumo_saldocontratoreservar__default(),
            new Object[][] {
                new Object[] {
               P00D22_A1774AutorizacaoConsumo_Codigo, P00D22_A74Contrato_Codigo, P00D22_A1777AutorizacaoConsumo_UnidadeMedicaoCod, P00D22_A1779AutorizacaoConsumo_Quantidade, P00D22_A1782AutorizacaoConsumo_Valor
               }
               , new Object[] {
               P00D23_A1781SaldoContrato_Ativo, P00D23_A1783SaldoContrato_UnidadeMedicao_Codigo, P00D23_A74Contrato_Codigo, P00D23_A1561SaldoContrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1779AutorizacaoConsumo_Quantidade ;
      private short AV10AutorizacaoConsumo_Quantidade ;
      private int AV8AutorizacaoConsumo_Codigo ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int AV9Contrato_Codigo ;
      private int AV13AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int AV11SaldoContrato_Codigo ;
      private decimal A1782AutorizacaoConsumo_Valor ;
      private decimal AV12AutorizacaoConsumo_Valor ;
      private String scmdbuf ;
      private bool A1781SaldoContrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00D22_A1774AutorizacaoConsumo_Codigo ;
      private int[] P00D22_A74Contrato_Codigo ;
      private int[] P00D22_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private short[] P00D22_A1779AutorizacaoConsumo_Quantidade ;
      private decimal[] P00D22_A1782AutorizacaoConsumo_Valor ;
      private bool[] P00D23_A1781SaldoContrato_Ativo ;
      private int[] P00D23_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] P00D23_A74Contrato_Codigo ;
      private int[] P00D23_A1561SaldoContrato_Codigo ;
   }

   public class prc_autorizacaoconsumo_saldocontratoreservar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D22 ;
          prmP00D22 = new Object[] {
          new Object[] {"@AV8AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00D23 ;
          prmP00D23 = new Object[] {
          new Object[] {"@AV9Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13AutorizacaoConsumo_UnidadeMedicaoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D22", "SELECT [AutorizacaoConsumo_Codigo], [Contrato_Codigo], [AutorizacaoConsumo_UnidadeMedicaoCod], [AutorizacaoConsumo_Quantidade], [AutorizacaoConsumo_Valor] FROM [AutorizacaoConsumo] WITH (NOLOCK) WHERE [AutorizacaoConsumo_Codigo] = @AV8AutorizacaoConsumo_Codigo ORDER BY [AutorizacaoConsumo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D22,1,0,false,true )
             ,new CursorDef("P00D23", "SELECT [SaldoContrato_Ativo], [SaldoContrato_UnidadeMedicao_Codigo], [Contrato_Codigo], [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV9Contrato_Codigo) AND ([SaldoContrato_Ativo] = 1) AND ([SaldoContrato_UnidadeMedicao_Codigo] = @AV13AutorizacaoConsumo_UnidadeMedicaoCod) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D23,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
