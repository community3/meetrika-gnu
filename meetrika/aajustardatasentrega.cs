/*
               File: AjustarDatasEntrega
        Description: Ajustar Datas de Entrega
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:8:51.81
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aajustardatasentrega : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aajustardatasentrega( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aajustardatasentrega( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aajustardatasentrega objaajustardatasentrega;
         objaajustardatasentrega = new aajustardatasentrega();
         objaajustardatasentrega.context.SetSubmitInitialConfig(context);
         objaajustardatasentrega.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaajustardatasentrega);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aajustardatasentrega)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 2;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*2));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            /* Optimized group. */
            /* Using cursor P00W82 */
            pr_default.execute(0);
            cV8SemDtPrv = P00W82_AV8SemDtPrv[0];
            pr_default.close(0);
            AV8SemDtPrv = (int)(AV8SemDtPrv+cV8SemDtPrv*1);
            /* End optimized group. */
            /* Optimized group. */
            /* Using cursor P00W83 */
            pr_default.execute(1);
            cV9SemDtEnt = P00W83_AV9SemDtEnt[0];
            pr_default.close(1);
            AV9SemDtEnt = (int)(AV9SemDtEnt+cV9SemDtEnt*1);
            /* End optimized group. */
            HW80( false, 82) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Sem data prevista:", 75, Gx_line+17, 168, Gx_line+31, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8SemDtPrv), "ZZZZZZZ9")), 200, Gx_line+17, 251, Gx_line+32, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Sem data entrega", 75, Gx_line+33, 164, Gx_line+47, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SemDtEnt), "ZZZZZZZ9")), 200, Gx_line+33, 251, Gx_line+48, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Id", 42, Gx_line+67, 52, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Data Dmn", 100, Gx_line+67, 151, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Ini", 183, Gx_line+67, 195, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Mais", 208, Gx_line+67, 232, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("1� Ent", 283, Gx_line+67, 314, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Real", 375, Gx_line+67, 399, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Prazo", 458, Gx_line+67, 487, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Prazo Ext", 550, Gx_line+67, 598, Gx_line+81, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Novo ciclo", 742, Gx_line+67, 796, Gx_line+81, 1+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+82);
            /* Using cursor P00W86 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00W86_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00W86_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P00W86_A456ContagemResultado_Codigo[0];
               A1237ContagemResultado_PrazoMaisDias = P00W86_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = P00W86_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = P00W86_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = P00W86_n1227ContagemResultado_PrazoInicialDias[0];
               A1611ContagemResultado_PrzTpDias = P00W86_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P00W86_n1611ContagemResultado_PrzTpDias[0];
               A471ContagemResultado_DataDmn = P00W86_A471ContagemResultado_DataDmn[0];
               A1351ContagemResultado_DataPrevista = P00W86_A1351ContagemResultado_DataPrevista[0];
               n1351ContagemResultado_DataPrevista = P00W86_n1351ContagemResultado_DataPrevista[0];
               A566ContagemResultado_DataUltCnt = P00W86_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P00W86_n566ContagemResultado_DataUltCnt[0];
               A40000GXC1 = P00W86_A40000GXC1[0];
               n40000GXC1 = P00W86_n40000GXC1[0];
               A1611ContagemResultado_PrzTpDias = P00W86_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = P00W86_n1611ContagemResultado_PrzTpDias[0];
               A566ContagemResultado_DataUltCnt = P00W86_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P00W86_n566ContagemResultado_DataUltCnt[0];
               A40000GXC1 = P00W86_A40000GXC1[0];
               n40000GXC1 = P00W86_n40000GXC1[0];
               AV10DataPrevista = (DateTime)(DateTime.MinValue);
               AV16Dias = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               if ( AV16Dias > 0 )
               {
                  GXt_dtime1 = AV10DataPrevista;
                  GXt_dtime2 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
                  new prc_adddiasuteis(context ).execute(  GXt_dtime2,  AV16Dias,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime1) ;
                  AV10DataPrevista = GXt_dtime1;
               }
               else
               {
                  /* Using cursor P00W87 */
                  pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A511ContagemResultado_HoraCnt = P00W87_A511ContagemResultado_HoraCnt[0];
                     A473ContagemResultado_DataCnt = P00W87_A473ContagemResultado_DataCnt[0];
                     AV10DataPrevista = DateTimeUtil.ResetTime( A473ContagemResultado_DataCnt ) ;
                     AV10DataPrevista = DateTimeUtil.TAdd( AV10DataPrevista, (int)(3600*(NumberUtil.Val( StringUtil.Substring( A511ContagemResultado_HoraCnt, 1, 2), "."))));
                     AV10DataPrevista = DateTimeUtil.TAdd( AV10DataPrevista, (int)(60*(NumberUtil.Val( StringUtil.Substring( A511ContagemResultado_HoraCnt, 4, 2), "."))));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(3);
                  }
                  pr_default.close(3);
               }
               HW80( false, 15) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), 42, Gx_line+0, 81, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), 100, Gx_line+0, 149, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1227ContagemResultado_PrazoInicialDias), "ZZZ9")), 175, Gx_line+0, 201, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A1237ContagemResultado_PrazoMaisDias), "ZZZ9")), 208, Gx_line+0, 234, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"), 367, Gx_line+0, 416, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV10DataPrevista, "99/99/99 99:99"), 267, Gx_line+0, 347, Gx_line+15, 2+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+15);
               AV24Codigo = 0;
               AV27Acoes = "SIM";
               /* Using cursor P00W88 */
               pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo, AV27Acoes});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A892LogResponsavel_DemandaCod = P00W88_A892LogResponsavel_DemandaCod[0];
                  n892LogResponsavel_DemandaCod = P00W88_n892LogResponsavel_DemandaCod[0];
                  A1177LogResponsavel_Prazo = P00W88_A1177LogResponsavel_Prazo[0];
                  n1177LogResponsavel_Prazo = P00W88_n1177LogResponsavel_Prazo[0];
                  A894LogResponsavel_Acao = P00W88_A894LogResponsavel_Acao[0];
                  A1797LogResponsavel_Codigo = P00W88_A1797LogResponsavel_Codigo[0];
                  AV10DataPrevista = A1177LogResponsavel_Prazo;
                  AV24Codigo = (int)(A1797LogResponsavel_Codigo);
                  /* Noskip command */
                  Gx_line = Gx_OldLine;
                  HW80( false, 17) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99"), 433, Gx_line+0, 513, Gx_line+15, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+17);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               AV25DataCnt = A40000GXC1;
               /* Using cursor P00W89 */
               pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo, AV25DataCnt, AV24Codigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A1553ContagemResultado_CntSrvCod = P00W89_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W89_n1553ContagemResultado_CntSrvCod[0];
                  A891LogResponsavel_UsuarioCod = P00W89_A891LogResponsavel_UsuarioCod[0];
                  n891LogResponsavel_UsuarioCod = P00W89_n891LogResponsavel_UsuarioCod[0];
                  A490ContagemResultado_ContratadaCod = P00W89_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W89_n490ContagemResultado_ContratadaCod[0];
                  A1611ContagemResultado_PrzTpDias = P00W89_A1611ContagemResultado_PrzTpDias[0];
                  n1611ContagemResultado_PrzTpDias = P00W89_n1611ContagemResultado_PrzTpDias[0];
                  A893LogResponsavel_DataHora = P00W89_A893LogResponsavel_DataHora[0];
                  A1177LogResponsavel_Prazo = P00W89_A1177LogResponsavel_Prazo[0];
                  n1177LogResponsavel_Prazo = P00W89_n1177LogResponsavel_Prazo[0];
                  A1797LogResponsavel_Codigo = P00W89_A1797LogResponsavel_Codigo[0];
                  A894LogResponsavel_Acao = P00W89_A894LogResponsavel_Acao[0];
                  A896LogResponsavel_Owner = P00W89_A896LogResponsavel_Owner[0];
                  A892LogResponsavel_DemandaCod = P00W89_A892LogResponsavel_DemandaCod[0];
                  n892LogResponsavel_DemandaCod = P00W89_n892LogResponsavel_DemandaCod[0];
                  A1553ContagemResultado_CntSrvCod = P00W89_A1553ContagemResultado_CntSrvCod[0];
                  n1553ContagemResultado_CntSrvCod = P00W89_n1553ContagemResultado_CntSrvCod[0];
                  A490ContagemResultado_ContratadaCod = P00W89_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00W89_n490ContagemResultado_ContratadaCod[0];
                  A1611ContagemResultado_PrzTpDias = P00W89_A1611ContagemResultado_PrzTpDias[0];
                  n1611ContagemResultado_PrzTpDias = P00W89_n1611ContagemResultado_PrzTpDias[0];
                  GXt_boolean3 = A1149LogResponsavel_OwnerEhContratante;
                  new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean3) ;
                  A1149LogResponsavel_OwnerEhContratante = GXt_boolean3;
                  if ( A1149LogResponsavel_OwnerEhContratante )
                  {
                     AV11Exit = false;
                     /* Using cursor P00W810 */
                     pr_default.execute(6, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod});
                     while ( (pr_default.getStatus(6) != 101) )
                     {
                        A66ContratadaUsuario_ContratadaCod = P00W810_A66ContratadaUsuario_ContratadaCod[0];
                        A69ContratadaUsuario_UsuarioCod = P00W810_A69ContratadaUsuario_UsuarioCod[0];
                        if ( AV16Dias > 0 )
                        {
                           GXt_dtime2 = AV10DataPrevista;
                           new prc_adddiasuteis(context ).execute(  A893LogResponsavel_DataHora,  AV16Dias,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime2) ;
                           AV10DataPrevista = GXt_dtime2;
                           AV10DataPrevista = DateTimeUtil.TAdd( AV10DataPrevista, 3600*(DateTimeUtil.Hour( A1177LogResponsavel_Prazo)));
                           AV10DataPrevista = DateTimeUtil.TAdd( AV10DataPrevista, 60*(DateTimeUtil.Minute( A1177LogResponsavel_Prazo)));
                        }
                        else
                        {
                           AV10DataPrevista = A1177LogResponsavel_Prazo;
                        }
                        AV24Codigo = (int)(A1797LogResponsavel_Codigo);
                        /* Noskip command */
                        Gx_line = Gx_OldLine;
                        HW80( false, 17) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99"), 533, Gx_line+0, 613, Gx_line+15, 1+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+17);
                        AV11Exit = true;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(6);
                     if ( AV11Exit )
                     {
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                  }
                  pr_default.readNext(5);
               }
               pr_default.close(5);
               AV27Acoes = "RE";
               AV17NovoCiclo = "";
               /* Using cursor P00W811 */
               pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo, AV24Codigo, AV27Acoes, AV25DataCnt});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  A893LogResponsavel_DataHora = P00W811_A893LogResponsavel_DataHora[0];
                  A894LogResponsavel_Acao = P00W811_A894LogResponsavel_Acao[0];
                  A1797LogResponsavel_Codigo = P00W811_A1797LogResponsavel_Codigo[0];
                  A1177LogResponsavel_Prazo = P00W811_A1177LogResponsavel_Prazo[0];
                  n1177LogResponsavel_Prazo = P00W811_n1177LogResponsavel_Prazo[0];
                  A896LogResponsavel_Owner = P00W811_A896LogResponsavel_Owner[0];
                  A892LogResponsavel_DemandaCod = P00W811_A892LogResponsavel_DemandaCod[0];
                  n892LogResponsavel_DemandaCod = P00W811_n892LogResponsavel_DemandaCod[0];
                  GXt_boolean3 = A1149LogResponsavel_OwnerEhContratante;
                  new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean3) ;
                  A1149LogResponsavel_OwnerEhContratante = GXt_boolean3;
                  if ( A1149LogResponsavel_OwnerEhContratante )
                  {
                     AV17NovoCiclo = "*";
                     AV18ContagemResultado_Codigo = A892LogResponsavel_DemandaCod;
                     AV24Codigo = (int)(A1797LogResponsavel_Codigo);
                     AV10DataPrevista = A1177LogResponsavel_Prazo;
                     new prc_novocicloexecucao(context ).execute(  A892LogResponsavel_DemandaCod,  A893LogResponsavel_DataHora,  A1177LogResponsavel_Prazo,  A1611ContagemResultado_PrzTpDias,  false) ;
                     /* Execute user subroutine: 'ENCERRARCICLO' */
                     S111 ();
                     if ( returnInSub )
                     {
                        pr_default.close(7);
                        this.cleanup();
                        if (true) return;
                     }
                  }
                  pr_default.readNext(7);
               }
               pr_default.close(7);
               A1351ContagemResultado_DataPrevista = AV10DataPrevista;
               n1351ContagemResultado_DataPrevista = false;
               /* Noskip command */
               Gx_line = Gx_OldLine;
               HW80( false, 15) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV10DataPrevista, "99/99/99 99:99"), 642, Gx_line+0, 722, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17NovoCiclo, "")), 760, Gx_line+0, 777, Gx_line+15, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+15);
               /* Using cursor P00W812 */
               pr_default.execute(8, new Object[] {n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, A456ContagemResultado_Codigo});
               pr_default.close(8);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            /* Eject command */
            Gx_OldLine = Gx_line;
            Gx_line = (int)(P_lines+1);
            /* Using cursor P00W815 */
            pr_default.execute(9);
            while ( (pr_default.getStatus(9) != 101) )
            {
               A456ContagemResultado_Codigo = P00W815_A456ContagemResultado_Codigo[0];
               A1349ContagemResultado_DataExecucao = P00W815_A1349ContagemResultado_DataExecucao[0];
               n1349ContagemResultado_DataExecucao = P00W815_n1349ContagemResultado_DataExecucao[0];
               A2017ContagemResultado_DataEntregaReal = P00W815_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = P00W815_n2017ContagemResultado_DataEntregaReal[0];
               A566ContagemResultado_DataUltCnt = P00W815_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P00W815_n566ContagemResultado_DataUltCnt[0];
               A825ContagemResultado_HoraUltCnt = P00W815_A825ContagemResultado_HoraUltCnt[0];
               n825ContagemResultado_HoraUltCnt = P00W815_n825ContagemResultado_HoraUltCnt[0];
               A566ContagemResultado_DataUltCnt = P00W815_A566ContagemResultado_DataUltCnt[0];
               n566ContagemResultado_DataUltCnt = P00W815_n566ContagemResultado_DataUltCnt[0];
               A825ContagemResultado_HoraUltCnt = P00W815_A825ContagemResultado_HoraUltCnt[0];
               n825ContagemResultado_HoraUltCnt = P00W815_n825ContagemResultado_HoraUltCnt[0];
               AV26DataExecucao = (DateTime)(DateTime.MinValue);
               /* Using cursor P00W816 */
               pr_default.execute(10, new Object[] {A456ContagemResultado_Codigo});
               while ( (pr_default.getStatus(10) != 101) )
               {
                  A511ContagemResultado_HoraCnt = P00W816_A511ContagemResultado_HoraCnt[0];
                  A473ContagemResultado_DataCnt = P00W816_A473ContagemResultado_DataCnt[0];
                  AV26DataExecucao = DateTimeUtil.ResetTime( A473ContagemResultado_DataCnt ) ;
                  AV26DataExecucao = DateTimeUtil.TAdd( AV26DataExecucao, (int)(3600*(NumberUtil.Val( StringUtil.Substring( A511ContagemResultado_HoraCnt, 1, 2), "."))));
                  AV26DataExecucao = DateTimeUtil.TAdd( AV26DataExecucao, (int)(60*(NumberUtil.Val( StringUtil.Substring( A511ContagemResultado_HoraCnt, 4, 2), "."))));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(10);
               }
               pr_default.close(10);
               A1349ContagemResultado_DataExecucao = AV26DataExecucao;
               n1349ContagemResultado_DataExecucao = false;
               A2017ContagemResultado_DataEntregaReal = DateTimeUtil.ResetTime( A566ContagemResultado_DataUltCnt ) ;
               n2017ContagemResultado_DataEntregaReal = false;
               A2017ContagemResultado_DataEntregaReal = DateTimeUtil.TAdd( A2017ContagemResultado_DataEntregaReal, (int)(3600*(NumberUtil.Val( StringUtil.Substring( A825ContagemResultado_HoraUltCnt, 1, 2), "."))));
               n2017ContagemResultado_DataEntregaReal = false;
               A2017ContagemResultado_DataEntregaReal = DateTimeUtil.TAdd( A2017ContagemResultado_DataEntregaReal, (int)(60*(NumberUtil.Val( StringUtil.Substring( A825ContagemResultado_HoraUltCnt, 4, 2), "."))));
               n2017ContagemResultado_DataEntregaReal = false;
               HW80( false, 17) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( A1349ContagemResultado_DataExecucao, "99/99/99 99:99"), 142, Gx_line+0, 222, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A2017ContagemResultado_DataEntregaReal, "99/99/99 99:99"), 242, Gx_line+0, 322, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), 58, Gx_line+0, 97, Gx_line+15, 2+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+17);
               /* Using cursor P00W817 */
               pr_default.execute(11, new Object[] {n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n2017ContagemResultado_DataEntregaReal, A2017ContagemResultado_DataEntregaReal, A456ContagemResultado_Codigo});
               pr_default.close(11);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               pr_default.readNext(9);
            }
            pr_default.close(9);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HW80( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ENCERRARCICLO' Routine */
         AV27Acoes = "VDR";
         /* Using cursor P00W818 */
         pr_default.execute(12, new Object[] {AV18ContagemResultado_Codigo, AV24Codigo, AV27Acoes});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A894LogResponsavel_Acao = P00W818_A894LogResponsavel_Acao[0];
            A1797LogResponsavel_Codigo = P00W818_A1797LogResponsavel_Codigo[0];
            A893LogResponsavel_DataHora = P00W818_A893LogResponsavel_DataHora[0];
            A896LogResponsavel_Owner = P00W818_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = P00W818_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00W818_n892LogResponsavel_DemandaCod[0];
            GXt_boolean3 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean3) ;
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean3;
            if ( ! A1149LogResponsavel_OwnerEhContratante )
            {
               new prc_encerrarcicloexecucao(context ).execute(  A892LogResponsavel_DemandaCod,  A893LogResponsavel_DataHora) ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      protected void HW80( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 17, Gx_line+0, 66, Gx_line+15, 2+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 75, Gx_line+0, 168, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 700, Gx_line+0, 739, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 758, Gx_line+0, 807, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 742, Gx_line+0, 756, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 667, Gx_line+0, 702, Gx_line+14, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+17);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Ajuste de Datas", 350, Gx_line+0, 486, Gx_line+25, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+33);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "AjustarDatasEntrega");
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P00W82_AV8SemDtPrv = new int[1] ;
         P00W83_AV9SemDtEnt = new int[1] ;
         P00W86_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00W86_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00W86_A456ContagemResultado_Codigo = new int[1] ;
         P00W86_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P00W86_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P00W86_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00W86_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00W86_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00W86_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00W86_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00W86_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00W86_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00W86_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00W86_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P00W86_A40000GXC1 = new DateTime[] {DateTime.MinValue} ;
         P00W86_n40000GXC1 = new bool[] {false} ;
         A1611ContagemResultado_PrzTpDias = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A40000GXC1 = (DateTime)(DateTime.MinValue);
         AV10DataPrevista = (DateTime)(DateTime.MinValue);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         P00W87_A456ContagemResultado_Codigo = new int[1] ;
         P00W87_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00W87_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         AV27Acoes = "";
         P00W88_A892LogResponsavel_DemandaCod = new int[1] ;
         P00W88_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00W88_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00W88_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P00W88_A894LogResponsavel_Acao = new String[] {""} ;
         P00W88_A1797LogResponsavel_Codigo = new long[1] ;
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A894LogResponsavel_Acao = "";
         AV25DataCnt = (DateTime)(DateTime.MinValue);
         P00W89_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00W89_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00W89_A891LogResponsavel_UsuarioCod = new int[1] ;
         P00W89_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         P00W89_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00W89_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00W89_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00W89_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00W89_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00W89_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00W89_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P00W89_A1797LogResponsavel_Codigo = new long[1] ;
         P00W89_A894LogResponsavel_Acao = new String[] {""} ;
         P00W89_A896LogResponsavel_Owner = new int[1] ;
         P00W89_A892LogResponsavel_DemandaCod = new int[1] ;
         P00W89_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         P00W810_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00W810_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         AV17NovoCiclo = "";
         P00W811_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00W811_A894LogResponsavel_Acao = new String[] {""} ;
         P00W811_A1797LogResponsavel_Codigo = new long[1] ;
         P00W811_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         P00W811_n1177LogResponsavel_Prazo = new bool[] {false} ;
         P00W811_A896LogResponsavel_Owner = new int[1] ;
         P00W811_A892LogResponsavel_DemandaCod = new int[1] ;
         P00W811_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00W815_A456ContagemResultado_Codigo = new int[1] ;
         P00W815_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00W815_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00W815_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00W815_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P00W815_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00W815_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P00W815_A825ContagemResultado_HoraUltCnt = new String[] {""} ;
         P00W815_n825ContagemResultado_HoraUltCnt = new bool[] {false} ;
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A825ContagemResultado_HoraUltCnt = "";
         AV26DataExecucao = (DateTime)(DateTime.MinValue);
         P00W816_A456ContagemResultado_Codigo = new int[1] ;
         P00W816_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P00W816_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00W818_A894LogResponsavel_Acao = new String[] {""} ;
         P00W818_A1797LogResponsavel_Codigo = new long[1] ;
         P00W818_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00W818_A896LogResponsavel_Owner = new int[1] ;
         P00W818_A892LogResponsavel_DemandaCod = new int[1] ;
         P00W818_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aajustardatasentrega__default(),
            new Object[][] {
                new Object[] {
               P00W82_AV8SemDtPrv
               }
               , new Object[] {
               P00W83_AV9SemDtEnt
               }
               , new Object[] {
               P00W86_A1553ContagemResultado_CntSrvCod, P00W86_n1553ContagemResultado_CntSrvCod, P00W86_A456ContagemResultado_Codigo, P00W86_A1237ContagemResultado_PrazoMaisDias, P00W86_n1237ContagemResultado_PrazoMaisDias, P00W86_A1227ContagemResultado_PrazoInicialDias, P00W86_n1227ContagemResultado_PrazoInicialDias, P00W86_A1611ContagemResultado_PrzTpDias, P00W86_n1611ContagemResultado_PrzTpDias, P00W86_A471ContagemResultado_DataDmn,
               P00W86_A1351ContagemResultado_DataPrevista, P00W86_n1351ContagemResultado_DataPrevista, P00W86_A566ContagemResultado_DataUltCnt, P00W86_n566ContagemResultado_DataUltCnt, P00W86_A40000GXC1, P00W86_n40000GXC1
               }
               , new Object[] {
               P00W87_A456ContagemResultado_Codigo, P00W87_A511ContagemResultado_HoraCnt, P00W87_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               P00W88_A892LogResponsavel_DemandaCod, P00W88_n892LogResponsavel_DemandaCod, P00W88_A1177LogResponsavel_Prazo, P00W88_n1177LogResponsavel_Prazo, P00W88_A894LogResponsavel_Acao, P00W88_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P00W89_A1553ContagemResultado_CntSrvCod, P00W89_n1553ContagemResultado_CntSrvCod, P00W89_A891LogResponsavel_UsuarioCod, P00W89_n891LogResponsavel_UsuarioCod, P00W89_A490ContagemResultado_ContratadaCod, P00W89_n490ContagemResultado_ContratadaCod, P00W89_A1611ContagemResultado_PrzTpDias, P00W89_n1611ContagemResultado_PrzTpDias, P00W89_A893LogResponsavel_DataHora, P00W89_A1177LogResponsavel_Prazo,
               P00W89_n1177LogResponsavel_Prazo, P00W89_A1797LogResponsavel_Codigo, P00W89_A894LogResponsavel_Acao, P00W89_A896LogResponsavel_Owner, P00W89_A892LogResponsavel_DemandaCod, P00W89_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               P00W810_A66ContratadaUsuario_ContratadaCod, P00W810_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P00W811_A893LogResponsavel_DataHora, P00W811_A894LogResponsavel_Acao, P00W811_A1797LogResponsavel_Codigo, P00W811_A1177LogResponsavel_Prazo, P00W811_n1177LogResponsavel_Prazo, P00W811_A896LogResponsavel_Owner, P00W811_A892LogResponsavel_DemandaCod, P00W811_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00W815_A456ContagemResultado_Codigo, P00W815_A1349ContagemResultado_DataExecucao, P00W815_n1349ContagemResultado_DataExecucao, P00W815_A2017ContagemResultado_DataEntregaReal, P00W815_n2017ContagemResultado_DataEntregaReal, P00W815_A566ContagemResultado_DataUltCnt, P00W815_n566ContagemResultado_DataUltCnt, P00W815_A825ContagemResultado_HoraUltCnt, P00W815_n825ContagemResultado_HoraUltCnt
               }
               , new Object[] {
               P00W816_A456ContagemResultado_Codigo, P00W816_A511ContagemResultado_HoraCnt, P00W816_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               P00W818_A894LogResponsavel_Acao, P00W818_A1797LogResponsavel_Codigo, P00W818_A893LogResponsavel_DataHora, P00W818_A896LogResponsavel_Owner, P00W818_A892LogResponsavel_DemandaCod, P00W818_n892LogResponsavel_DemandaCod
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV16Dias ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int cV8SemDtPrv ;
      private int AV8SemDtPrv ;
      private int cV9SemDtEnt ;
      private int AV9SemDtEnt ;
      private int Gx_OldLine ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV24Codigo ;
      private int A892LogResponsavel_DemandaCod ;
      private int A891LogResponsavel_UsuarioCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A896LogResponsavel_Owner ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV18ContagemResultado_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV27Acoes ;
      private String A894LogResponsavel_Acao ;
      private String AV17NovoCiclo ;
      private String A825ContagemResultado_HoraUltCnt ;
      private String Gx_time ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A40000GXC1 ;
      private DateTime AV10DataPrevista ;
      private DateTime GXt_dtime1 ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime AV25DataCnt ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime GXt_dtime2 ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime AV26DataExecucao ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n566ContagemResultado_DataUltCnt ;
      private bool n40000GXC1 ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool AV11Exit ;
      private bool returnInSub ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n825ContagemResultado_HoraUltCnt ;
      private bool GXt_boolean3 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00W82_AV8SemDtPrv ;
      private int[] P00W83_AV9SemDtEnt ;
      private int[] P00W86_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00W86_n1553ContagemResultado_CntSrvCod ;
      private int[] P00W86_A456ContagemResultado_Codigo ;
      private short[] P00W86_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P00W86_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P00W86_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00W86_n1227ContagemResultado_PrazoInicialDias ;
      private String[] P00W86_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00W86_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P00W86_A471ContagemResultado_DataDmn ;
      private DateTime[] P00W86_A1351ContagemResultado_DataPrevista ;
      private bool[] P00W86_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00W86_A566ContagemResultado_DataUltCnt ;
      private bool[] P00W86_n566ContagemResultado_DataUltCnt ;
      private DateTime[] P00W86_A40000GXC1 ;
      private bool[] P00W86_n40000GXC1 ;
      private int[] P00W87_A456ContagemResultado_Codigo ;
      private String[] P00W87_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00W87_A473ContagemResultado_DataCnt ;
      private int[] P00W88_A892LogResponsavel_DemandaCod ;
      private bool[] P00W88_n892LogResponsavel_DemandaCod ;
      private DateTime[] P00W88_A1177LogResponsavel_Prazo ;
      private bool[] P00W88_n1177LogResponsavel_Prazo ;
      private String[] P00W88_A894LogResponsavel_Acao ;
      private long[] P00W88_A1797LogResponsavel_Codigo ;
      private int[] P00W89_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00W89_n1553ContagemResultado_CntSrvCod ;
      private int[] P00W89_A891LogResponsavel_UsuarioCod ;
      private bool[] P00W89_n891LogResponsavel_UsuarioCod ;
      private int[] P00W89_A490ContagemResultado_ContratadaCod ;
      private bool[] P00W89_n490ContagemResultado_ContratadaCod ;
      private String[] P00W89_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00W89_n1611ContagemResultado_PrzTpDias ;
      private DateTime[] P00W89_A893LogResponsavel_DataHora ;
      private DateTime[] P00W89_A1177LogResponsavel_Prazo ;
      private bool[] P00W89_n1177LogResponsavel_Prazo ;
      private long[] P00W89_A1797LogResponsavel_Codigo ;
      private String[] P00W89_A894LogResponsavel_Acao ;
      private int[] P00W89_A896LogResponsavel_Owner ;
      private int[] P00W89_A892LogResponsavel_DemandaCod ;
      private bool[] P00W89_n892LogResponsavel_DemandaCod ;
      private int[] P00W810_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00W810_A69ContratadaUsuario_UsuarioCod ;
      private DateTime[] P00W811_A893LogResponsavel_DataHora ;
      private String[] P00W811_A894LogResponsavel_Acao ;
      private long[] P00W811_A1797LogResponsavel_Codigo ;
      private DateTime[] P00W811_A1177LogResponsavel_Prazo ;
      private bool[] P00W811_n1177LogResponsavel_Prazo ;
      private int[] P00W811_A896LogResponsavel_Owner ;
      private int[] P00W811_A892LogResponsavel_DemandaCod ;
      private bool[] P00W811_n892LogResponsavel_DemandaCod ;
      private int[] P00W815_A456ContagemResultado_Codigo ;
      private DateTime[] P00W815_A1349ContagemResultado_DataExecucao ;
      private bool[] P00W815_n1349ContagemResultado_DataExecucao ;
      private DateTime[] P00W815_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00W815_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P00W815_A566ContagemResultado_DataUltCnt ;
      private bool[] P00W815_n566ContagemResultado_DataUltCnt ;
      private String[] P00W815_A825ContagemResultado_HoraUltCnt ;
      private bool[] P00W815_n825ContagemResultado_HoraUltCnt ;
      private int[] P00W816_A456ContagemResultado_Codigo ;
      private String[] P00W816_A511ContagemResultado_HoraCnt ;
      private DateTime[] P00W816_A473ContagemResultado_DataCnt ;
      private String[] P00W818_A894LogResponsavel_Acao ;
      private long[] P00W818_A1797LogResponsavel_Codigo ;
      private DateTime[] P00W818_A893LogResponsavel_DataHora ;
      private int[] P00W818_A896LogResponsavel_Owner ;
      private int[] P00W818_A892LogResponsavel_DemandaCod ;
      private bool[] P00W818_n892LogResponsavel_DemandaCod ;
   }

   public class aajustardatasentrega__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W82 ;
          prmP00W82 = new Object[] {
          } ;
          Object[] prmP00W83 ;
          prmP00W83 = new Object[] {
          } ;
          Object[] prmP00W86 ;
          prmP00W86 = new Object[] {
          } ;
          Object[] prmP00W87 ;
          prmP00W87 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W88 ;
          prmP00W88 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Acoes",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00W89 ;
          prmP00W89 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25DataCnt",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W810 ;
          prmP00W810 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W811 ;
          prmP00W811 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Acoes",SqlDbType.Char,20,0} ,
          new Object[] {"@AV25DataCnt",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00W812 ;
          prmP00W812 = new Object[] {
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W815 ;
          prmP00W815 = new Object[] {
          } ;
          Object[] prmP00W816 ;
          prmP00W816 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W817 ;
          prmP00W817 = new Object[] {
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataEntregaReal",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W818 ;
          prmP00W818 = new Object[] {
          new Object[] {"@AV18ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27Acoes",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W82", "SELECT COUNT(*) FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_DataPrevista] = convert( DATETIME, '17530101', 112 )) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W82,1,0,true,false )
             ,new CursorDef("P00W83", "SELECT COUNT(*) FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_DataExecucao] = convert( DATETIME, '17530101', 112 )) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W83,1,0,true,false )
             ,new CursorDef("P00W86", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T4.[GXC1], convert( DATETIME, '17530101', 112 )) AS GXC1 FROM ((([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MAX(T5.[ContagemResultadoExecucao_Fim]) AS GXC1, T6.[ContagemResultado_Codigo] FROM [ContagemResultadoExecucao] T5 WITH (NOLOCK),  [ContagemResultado] T6 WITH (UPDLOCK) WHERE T5.[ContagemResultadoExecucao_OSCod] = T6.[ContagemResultado_Codigo] GROUP BY T6.[ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = 0 ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W86,1,0,true,true )
             ,new CursorDef("P00W87", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W87,1,0,false,true )
             ,new CursorDef("P00W88", "SELECT TOP 1 [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, [LogResponsavel_Prazo], [LogResponsavel_Acao], [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @ContagemResultado_Codigo) AND ((CHARINDEX(RTRIM([LogResponsavel_Acao]), @AV27Acoes)) > 0) AND (Not ([LogResponsavel_Prazo] = convert( DATETIME, '17530101', 112 ))) ORDER BY [LogResponsavel_DemandaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W88,1,0,false,true )
             ,new CursorDef("P00W89", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[LogResponsavel_UsuarioCod], T2.[ContagemResultado_ContratadaCod], T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[LogResponsavel_DataHora], T1.[LogResponsavel_Prazo], T1.[LogResponsavel_Codigo], T1.[LogResponsavel_Acao], T1.[LogResponsavel_Owner], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @ContagemResultado_Codigo) AND (T1.[LogResponsavel_DataHora] < @AV25DataCnt) AND (T1.[LogResponsavel_Acao] = 'E') AND (T1.[LogResponsavel_Codigo] > @AV24Codigo) ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W89,100,0,true,false )
             ,new CursorDef("P00W810", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContagemResultado_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @LogResponsavel_UsuarioCod ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W810,1,0,true,true )
             ,new CursorDef("P00W811", "SELECT [LogResponsavel_DataHora], [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_Prazo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @ContagemResultado_Codigo and [LogResponsavel_Codigo] > @AV24Codigo) AND ((CHARINDEX(RTRIM([LogResponsavel_Acao]), @AV27Acoes)) > 0) AND ([LogResponsavel_DataHora] > @AV25DataCnt) ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W811,100,0,true,false )
             ,new CursorDef("P00W812", "UPDATE [ContagemResultado] SET [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W812)
             ,new CursorDef("P00W815", "SELECT T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_DataEntregaReal], COALESCE( T2.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T3.[ContagemResultado_HoraUltCnt], '') AS ContagemResultado_HoraUltCnt FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_HoraCnt]) AS ContagemResultado_HoraUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W815,1,0,true,false )
             ,new CursorDef("P00W816", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W816,1,0,false,true )
             ,new CursorDef("P00W817", "UPDATE [ContagemResultado] SET [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_DataEntregaReal]=@ContagemResultado_DataEntregaReal  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W817)
             ,new CursorDef("P00W818", "SELECT [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_DataHora], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @AV18ContagemResultado_Codigo and [LogResponsavel_Codigo] > @AV24Codigo) AND ((CHARINDEX(RTRIM([LogResponsavel_Acao]), @AV27Acoes)) > 0) ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W818,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[14])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((long[]) buf[5])[0] = rslt.getLong(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(5) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((long[]) buf[11])[0] = rslt.getLong(7) ;
                ((String[]) buf[12])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameterDatetime(4, (DateTime)parms[3]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
