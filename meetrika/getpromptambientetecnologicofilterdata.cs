/*
               File: GetPromptAmbienteTecnologicoFilterData
        Description: Get Prompt Ambiente Tecnologico Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:1.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptambientetecnologicofilterdata : GXProcedure
   {
      public getpromptambientetecnologicofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptambientetecnologicofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
         return AV28OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptambientetecnologicofilterdata objgetpromptambientetecnologicofilterdata;
         objgetpromptambientetecnologicofilterdata = new getpromptambientetecnologicofilterdata();
         objgetpromptambientetecnologicofilterdata.AV19DDOName = aP0_DDOName;
         objgetpromptambientetecnologicofilterdata.AV17SearchTxt = aP1_SearchTxt;
         objgetpromptambientetecnologicofilterdata.AV18SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptambientetecnologicofilterdata.AV23OptionsJson = "" ;
         objgetpromptambientetecnologicofilterdata.AV26OptionsDescJson = "" ;
         objgetpromptambientetecnologicofilterdata.AV28OptionIndexesJson = "" ;
         objgetpromptambientetecnologicofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptambientetecnologicofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptambientetecnologicofilterdata);
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptambientetecnologicofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Options = (IGxCollection)(new GxSimpleCollection());
         AV25OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV27OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_AMBIENTETECNOLOGICO_AREATRABALHODES") == 0 )
         {
            /* Execute user subroutine: 'LOADAMBIENTETECNOLOGICO_AREATRABALHODESOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV23OptionsJson = AV22Options.ToJSonString(false);
         AV26OptionsDescJson = AV25OptionsDesc.ToJSonString(false);
         AV28OptionIndexesJson = AV27OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get("PromptAmbienteTecnologicoGridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptAmbienteTecnologicoGridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV30Session.Get("PromptAmbienteTecnologicoGridState"), "");
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV33GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "AMBIENTETECNOLOGICO_AREATRABALHOCOD") == 0 )
            {
               AV35AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV10TFAmbienteTecnologico_Descricao = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV11TFAmbienteTecnologico_Descricao_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_AREATRABALHOCOD") == 0 )
            {
               AV12TFAmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
               AV13TFAmbienteTecnologico_AreaTrabalhoCod_To = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_AREATRABALHODES") == 0 )
            {
               AV14TFAmbienteTecnologico_AreaTrabalhoDes = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_AREATRABALHODES_SEL") == 0 )
            {
               AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_ATIVO_SEL") == 0 )
            {
               AV16TFAmbienteTecnologico_Ativo_Sel = (short)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
         if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV34GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV37AmbienteTecnologico_Descricao1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV34GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
               {
                  AV40AmbienteTecnologico_Descricao2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV34GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 )
                  {
                     AV43AmbienteTecnologico_Descricao3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADAMBIENTETECNOLOGICO_DESCRICAOOPTIONS' Routine */
         AV10TFAmbienteTecnologico_Descricao = AV17SearchTxt;
         AV11TFAmbienteTecnologico_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV35AmbienteTecnologico_AreaTrabalhoCod ,
                                              AV36DynamicFiltersSelector1 ,
                                              AV37AmbienteTecnologico_Descricao1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40AmbienteTecnologico_Descricao2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43AmbienteTecnologico_Descricao3 ,
                                              AV11TFAmbienteTecnologico_Descricao_Sel ,
                                              AV10TFAmbienteTecnologico_Descricao ,
                                              AV12TFAmbienteTecnologico_AreaTrabalhoCod ,
                                              AV13TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                              AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                              AV14TFAmbienteTecnologico_AreaTrabalhoDes ,
                                              AV16TFAmbienteTecnologico_Ativo_Sel ,
                                              A728AmbienteTecnologico_AreaTrabalhoCod ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A729AmbienteTecnologico_AreaTrabalhoDes ,
                                              A353AmbienteTecnologico_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV37AmbienteTecnologico_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV37AmbienteTecnologico_Descricao1), "%", "");
         lV40AmbienteTecnologico_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV40AmbienteTecnologico_Descricao2), "%", "");
         lV43AmbienteTecnologico_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV43AmbienteTecnologico_Descricao3), "%", "");
         lV10TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFAmbienteTecnologico_Descricao), "%", "");
         lV14TFAmbienteTecnologico_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV14TFAmbienteTecnologico_AreaTrabalhoDes), "%", "");
         /* Using cursor P00LW2 */
         pr_default.execute(0, new Object[] {AV35AmbienteTecnologico_AreaTrabalhoCod, lV37AmbienteTecnologico_Descricao1, lV40AmbienteTecnologico_Descricao2, lV43AmbienteTecnologico_Descricao3, lV10TFAmbienteTecnologico_Descricao, AV11TFAmbienteTecnologico_Descricao_Sel, AV12TFAmbienteTecnologico_AreaTrabalhoCod, AV13TFAmbienteTecnologico_AreaTrabalhoCod_To, lV14TFAmbienteTecnologico_AreaTrabalhoDes, AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLW2 = false;
            A352AmbienteTecnologico_Descricao = P00LW2_A352AmbienteTecnologico_Descricao[0];
            A353AmbienteTecnologico_Ativo = P00LW2_A353AmbienteTecnologico_Ativo[0];
            A729AmbienteTecnologico_AreaTrabalhoDes = P00LW2_A729AmbienteTecnologico_AreaTrabalhoDes[0];
            n729AmbienteTecnologico_AreaTrabalhoDes = P00LW2_n729AmbienteTecnologico_AreaTrabalhoDes[0];
            A728AmbienteTecnologico_AreaTrabalhoCod = P00LW2_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            A351AmbienteTecnologico_Codigo = P00LW2_A351AmbienteTecnologico_Codigo[0];
            A729AmbienteTecnologico_AreaTrabalhoDes = P00LW2_A729AmbienteTecnologico_AreaTrabalhoDes[0];
            n729AmbienteTecnologico_AreaTrabalhoDes = P00LW2_n729AmbienteTecnologico_AreaTrabalhoDes[0];
            AV29count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LW2_A352AmbienteTecnologico_Descricao[0], A352AmbienteTecnologico_Descricao) == 0 ) )
            {
               BRKLW2 = false;
               A351AmbienteTecnologico_Codigo = P00LW2_A351AmbienteTecnologico_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKLW2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A352AmbienteTecnologico_Descricao)) )
            {
               AV21Option = A352AmbienteTecnologico_Descricao;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLW2 )
            {
               BRKLW2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADAMBIENTETECNOLOGICO_AREATRABALHODESOPTIONS' Routine */
         AV14TFAmbienteTecnologico_AreaTrabalhoDes = AV17SearchTxt;
         AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV35AmbienteTecnologico_AreaTrabalhoCod ,
                                              AV36DynamicFiltersSelector1 ,
                                              AV37AmbienteTecnologico_Descricao1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40AmbienteTecnologico_Descricao2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43AmbienteTecnologico_Descricao3 ,
                                              AV11TFAmbienteTecnologico_Descricao_Sel ,
                                              AV10TFAmbienteTecnologico_Descricao ,
                                              AV12TFAmbienteTecnologico_AreaTrabalhoCod ,
                                              AV13TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                              AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                              AV14TFAmbienteTecnologico_AreaTrabalhoDes ,
                                              AV16TFAmbienteTecnologico_Ativo_Sel ,
                                              A728AmbienteTecnologico_AreaTrabalhoCod ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A729AmbienteTecnologico_AreaTrabalhoDes ,
                                              A353AmbienteTecnologico_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV37AmbienteTecnologico_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV37AmbienteTecnologico_Descricao1), "%", "");
         lV40AmbienteTecnologico_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV40AmbienteTecnologico_Descricao2), "%", "");
         lV43AmbienteTecnologico_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV43AmbienteTecnologico_Descricao3), "%", "");
         lV10TFAmbienteTecnologico_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFAmbienteTecnologico_Descricao), "%", "");
         lV14TFAmbienteTecnologico_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV14TFAmbienteTecnologico_AreaTrabalhoDes), "%", "");
         /* Using cursor P00LW3 */
         pr_default.execute(1, new Object[] {AV35AmbienteTecnologico_AreaTrabalhoCod, lV37AmbienteTecnologico_Descricao1, lV40AmbienteTecnologico_Descricao2, lV43AmbienteTecnologico_Descricao3, lV10TFAmbienteTecnologico_Descricao, AV11TFAmbienteTecnologico_Descricao_Sel, AV12TFAmbienteTecnologico_AreaTrabalhoCod, AV13TFAmbienteTecnologico_AreaTrabalhoCod_To, lV14TFAmbienteTecnologico_AreaTrabalhoDes, AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKLW4 = false;
            A728AmbienteTecnologico_AreaTrabalhoCod = P00LW3_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            A353AmbienteTecnologico_Ativo = P00LW3_A353AmbienteTecnologico_Ativo[0];
            A729AmbienteTecnologico_AreaTrabalhoDes = P00LW3_A729AmbienteTecnologico_AreaTrabalhoDes[0];
            n729AmbienteTecnologico_AreaTrabalhoDes = P00LW3_n729AmbienteTecnologico_AreaTrabalhoDes[0];
            A352AmbienteTecnologico_Descricao = P00LW3_A352AmbienteTecnologico_Descricao[0];
            A351AmbienteTecnologico_Codigo = P00LW3_A351AmbienteTecnologico_Codigo[0];
            A729AmbienteTecnologico_AreaTrabalhoDes = P00LW3_A729AmbienteTecnologico_AreaTrabalhoDes[0];
            n729AmbienteTecnologico_AreaTrabalhoDes = P00LW3_n729AmbienteTecnologico_AreaTrabalhoDes[0];
            AV29count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00LW3_A728AmbienteTecnologico_AreaTrabalhoCod[0] == A728AmbienteTecnologico_AreaTrabalhoCod ) )
            {
               BRKLW4 = false;
               A351AmbienteTecnologico_Codigo = P00LW3_A351AmbienteTecnologico_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKLW4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A729AmbienteTecnologico_AreaTrabalhoDes)) )
            {
               AV21Option = A729AmbienteTecnologico_AreaTrabalhoDes;
               AV20InsertIndex = 1;
               while ( ( AV20InsertIndex <= AV22Options.Count ) && ( StringUtil.StrCmp(((String)AV22Options.Item(AV20InsertIndex)), AV21Option) < 0 ) )
               {
                  AV20InsertIndex = (int)(AV20InsertIndex+1);
               }
               AV22Options.Add(AV21Option, AV20InsertIndex);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), AV20InsertIndex);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLW4 )
            {
               BRKLW4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Options = new GxSimpleCollection();
         AV25OptionsDesc = new GxSimpleCollection();
         AV27OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV33GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAmbienteTecnologico_Descricao = "";
         AV11TFAmbienteTecnologico_Descricao_Sel = "";
         AV14TFAmbienteTecnologico_AreaTrabalhoDes = "";
         AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel = "";
         AV34GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37AmbienteTecnologico_Descricao1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV40AmbienteTecnologico_Descricao2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV43AmbienteTecnologico_Descricao3 = "";
         scmdbuf = "";
         lV37AmbienteTecnologico_Descricao1 = "";
         lV40AmbienteTecnologico_Descricao2 = "";
         lV43AmbienteTecnologico_Descricao3 = "";
         lV10TFAmbienteTecnologico_Descricao = "";
         lV14TFAmbienteTecnologico_AreaTrabalhoDes = "";
         A352AmbienteTecnologico_Descricao = "";
         A729AmbienteTecnologico_AreaTrabalhoDes = "";
         P00LW2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00LW2_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         P00LW2_A729AmbienteTecnologico_AreaTrabalhoDes = new String[] {""} ;
         P00LW2_n729AmbienteTecnologico_AreaTrabalhoDes = new bool[] {false} ;
         P00LW2_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         P00LW2_A351AmbienteTecnologico_Codigo = new int[1] ;
         AV21Option = "";
         P00LW3_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         P00LW3_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         P00LW3_A729AmbienteTecnologico_AreaTrabalhoDes = new String[] {""} ;
         P00LW3_n729AmbienteTecnologico_AreaTrabalhoDes = new bool[] {false} ;
         P00LW3_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         P00LW3_A351AmbienteTecnologico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptambientetecnologicofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LW2_A352AmbienteTecnologico_Descricao, P00LW2_A353AmbienteTecnologico_Ativo, P00LW2_A729AmbienteTecnologico_AreaTrabalhoDes, P00LW2_n729AmbienteTecnologico_AreaTrabalhoDes, P00LW2_A728AmbienteTecnologico_AreaTrabalhoCod, P00LW2_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               P00LW3_A728AmbienteTecnologico_AreaTrabalhoCod, P00LW3_A353AmbienteTecnologico_Ativo, P00LW3_A729AmbienteTecnologico_AreaTrabalhoDes, P00LW3_n729AmbienteTecnologico_AreaTrabalhoDes, P00LW3_A352AmbienteTecnologico_Descricao, P00LW3_A351AmbienteTecnologico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFAmbienteTecnologico_Ativo_Sel ;
      private int AV46GXV1 ;
      private int AV35AmbienteTecnologico_AreaTrabalhoCod ;
      private int AV12TFAmbienteTecnologico_AreaTrabalhoCod ;
      private int AV13TFAmbienteTecnologico_AreaTrabalhoCod_To ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int A351AmbienteTecnologico_Codigo ;
      private int AV20InsertIndex ;
      private long AV29count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool A353AmbienteTecnologico_Ativo ;
      private bool BRKLW2 ;
      private bool n729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool BRKLW4 ;
      private String AV28OptionIndexesJson ;
      private String AV23OptionsJson ;
      private String AV26OptionsDescJson ;
      private String AV19DDOName ;
      private String AV17SearchTxt ;
      private String AV18SearchTxtTo ;
      private String AV10TFAmbienteTecnologico_Descricao ;
      private String AV11TFAmbienteTecnologico_Descricao_Sel ;
      private String AV14TFAmbienteTecnologico_AreaTrabalhoDes ;
      private String AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV37AmbienteTecnologico_Descricao1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV40AmbienteTecnologico_Descricao2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV43AmbienteTecnologico_Descricao3 ;
      private String lV37AmbienteTecnologico_Descricao1 ;
      private String lV40AmbienteTecnologico_Descricao2 ;
      private String lV43AmbienteTecnologico_Descricao3 ;
      private String lV10TFAmbienteTecnologico_Descricao ;
      private String lV14TFAmbienteTecnologico_AreaTrabalhoDes ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A729AmbienteTecnologico_AreaTrabalhoDes ;
      private String AV21Option ;
      private IGxSession AV30Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00LW2_A352AmbienteTecnologico_Descricao ;
      private bool[] P00LW2_A353AmbienteTecnologico_Ativo ;
      private String[] P00LW2_A729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool[] P00LW2_n729AmbienteTecnologico_AreaTrabalhoDes ;
      private int[] P00LW2_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int[] P00LW2_A351AmbienteTecnologico_Codigo ;
      private int[] P00LW3_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private bool[] P00LW3_A353AmbienteTecnologico_Ativo ;
      private String[] P00LW3_A729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool[] P00LW3_n729AmbienteTecnologico_AreaTrabalhoDes ;
      private String[] P00LW3_A352AmbienteTecnologico_Descricao ;
      private int[] P00LW3_A351AmbienteTecnologico_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV33GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV34GridStateDynamicFilter ;
   }

   public class getpromptambientetecnologicofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LW2( IGxContext context ,
                                             int AV35AmbienteTecnologico_AreaTrabalhoCod ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37AmbienteTecnologico_Descricao1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             String AV40AmbienteTecnologico_Descricao2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             String AV43AmbienteTecnologico_Descricao3 ,
                                             String AV11TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV10TFAmbienteTecnologico_Descricao ,
                                             int AV12TFAmbienteTecnologico_AreaTrabalhoCod ,
                                             int AV13TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                             String AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                             String AV14TFAmbienteTecnologico_AreaTrabalhoDes ,
                                             short AV16TFAmbienteTecnologico_Ativo_Sel ,
                                             int A728AmbienteTecnologico_AreaTrabalhoCod ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A729AmbienteTecnologico_AreaTrabalhoDes ,
                                             bool A353AmbienteTecnologico_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[AmbienteTecnologico_Descricao], T1.[AmbienteTecnologico_Ativo], T2.[AreaTrabalho_Descricao] AS AmbienteTecnologico_AreaTrabalhoDes, T1.[AmbienteTecnologico_AreaTrabalhoCod] AS AmbienteTecnologico_AreaTrabalhoCod, T1.[AmbienteTecnologico_Codigo] FROM ([AmbienteTecnologico] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[AmbienteTecnologico_AreaTrabalhoCod])";
         if ( ! (0==AV35AmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV35AmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV35AmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37AmbienteTecnologico_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV37AmbienteTecnologico_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV37AmbienteTecnologico_Descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40AmbienteTecnologico_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV40AmbienteTecnologico_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV40AmbienteTecnologico_Descricao2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43AmbienteTecnologico_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV43AmbienteTecnologico_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV43AmbienteTecnologico_Descricao3)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFAmbienteTecnologico_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like @lV10TFAmbienteTecnologico_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like @lV10TFAmbienteTecnologico_Descricao)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAmbienteTecnologico_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] = @AV11TFAmbienteTecnologico_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] = @AV11TFAmbienteTecnologico_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV12TFAmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV12TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV12TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV13TFAmbienteTecnologico_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV13TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV13TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAmbienteTecnologico_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV14TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV14TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] = @AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV16TFAmbienteTecnologico_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
         }
         if ( AV16TFAmbienteTecnologico_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[AmbienteTecnologico_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00LW3( IGxContext context ,
                                             int AV35AmbienteTecnologico_AreaTrabalhoCod ,
                                             String AV36DynamicFiltersSelector1 ,
                                             String AV37AmbienteTecnologico_Descricao1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             String AV40AmbienteTecnologico_Descricao2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             String AV43AmbienteTecnologico_Descricao3 ,
                                             String AV11TFAmbienteTecnologico_Descricao_Sel ,
                                             String AV10TFAmbienteTecnologico_Descricao ,
                                             int AV12TFAmbienteTecnologico_AreaTrabalhoCod ,
                                             int AV13TFAmbienteTecnologico_AreaTrabalhoCod_To ,
                                             String AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel ,
                                             String AV14TFAmbienteTecnologico_AreaTrabalhoDes ,
                                             short AV16TFAmbienteTecnologico_Ativo_Sel ,
                                             int A728AmbienteTecnologico_AreaTrabalhoCod ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A729AmbienteTecnologico_AreaTrabalhoDes ,
                                             bool A353AmbienteTecnologico_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[AmbienteTecnologico_AreaTrabalhoCod] AS AmbienteTecnologico_AreaTrabalhoCod, T1.[AmbienteTecnologico_Ativo], T2.[AreaTrabalho_Descricao] AS AmbienteTecnologico_AreaTrabalhoDes, T1.[AmbienteTecnologico_Descricao], T1.[AmbienteTecnologico_Codigo] FROM ([AmbienteTecnologico] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[AmbienteTecnologico_AreaTrabalhoCod])";
         if ( ! (0==AV35AmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV35AmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] = @AV35AmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37AmbienteTecnologico_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV37AmbienteTecnologico_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV37AmbienteTecnologico_Descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40AmbienteTecnologico_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV40AmbienteTecnologico_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV40AmbienteTecnologico_Descricao2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "AMBIENTETECNOLOGICO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43AmbienteTecnologico_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like '%' + @lV43AmbienteTecnologico_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like '%' + @lV43AmbienteTecnologico_Descricao3)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAmbienteTecnologico_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFAmbienteTecnologico_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] like @lV10TFAmbienteTecnologico_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] like @lV10TFAmbienteTecnologico_Descricao)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFAmbienteTecnologico_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Descricao] = @AV11TFAmbienteTecnologico_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Descricao] = @AV11TFAmbienteTecnologico_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV12TFAmbienteTecnologico_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV12TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] >= @AV12TFAmbienteTecnologico_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV13TFAmbienteTecnologico_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV13TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_AreaTrabalhoCod] <= @AV13TFAmbienteTecnologico_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFAmbienteTecnologico_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV14TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV14TFAmbienteTecnologico_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] = @AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV16TFAmbienteTecnologico_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 1)";
            }
         }
         if ( AV16TFAmbienteTecnologico_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AmbienteTecnologico_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[AmbienteTecnologico_AreaTrabalhoCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LW2(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] );
               case 1 :
                     return conditional_P00LW3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (bool)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LW2 ;
          prmP00LW2 = new Object[] {
          new Object[] {"@AV35AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37AmbienteTecnologico_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV40AmbienteTecnologico_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV43AmbienteTecnologico_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV12TFAmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFAmbienteTecnologico_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFAmbienteTecnologico_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00LW3 ;
          prmP00LW3 = new Object[] {
          new Object[] {"@AV35AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37AmbienteTecnologico_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV40AmbienteTecnologico_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV43AmbienteTecnologico_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFAmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFAmbienteTecnologico_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV12TFAmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFAmbienteTecnologico_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFAmbienteTecnologico_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFAmbienteTecnologico_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LW2,100,0,true,false )
             ,new CursorDef("P00LW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LW3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptambientetecnologicofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptambientetecnologicofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptambientetecnologicofilterdata") )
          {
             return  ;
          }
          getpromptambientetecnologicofilterdata worker = new getpromptambientetecnologicofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
