/*
               File: WP_ImportarPFFS
        Description: Importar PF FS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 23:47:8.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_importarpffs : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_importarpffs( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_importarpffs( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSelcontratada_codigo = new GXCombobox();
         dynavContratoservicos_codigo = new GXCombobox();
         dynavSelcontadorfs_codigo = new GXCombobox();
         cmbavRegradivergencia = new GXCombobox();
         chkavFinal = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV113WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSELCONTRATADA_CODIGOD92( AV113WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATOSERVICOS_CODIGO") == 0 )
            {
               AV124SelContratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATOSERVICOS_CODIGOD92( AV124SelContratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELCONTADORFS_CODIGO") == 0 )
            {
               AV124SelContratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSELCONTADORFS_CODIGOD92( AV124SelContratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAD92( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTD92( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203182347822");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_importarpffs.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vARQUIVO", StringUtil.RTrim( AV7Arquivo));
         GxWebStd.gx_hidden_field( context, "vCOLDMNN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24ColDmnn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV116ColPFBFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV117ColPFLFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV121ColPFBFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV120ColPFLFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDATACNTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV127ColDataCntn), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV113WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV113WWPContext);
         }
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV134Blob);
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Height", StringUtil.RTrim( Confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Modal", StringUtil.BoolToStr( Confirmpanel_Modal));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttoncanceltext", StringUtil.RTrim( Confirmpanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW1_Target", StringUtil.RTrim( Innewwindow1_Target));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WED92( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTD92( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_importarpffs.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ImportarPFFS" ;
      }

      public override String GetPgmdesc( )
      {
         return "Importar PF FS" ;
      }

      protected void WBD90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_D92( true) ;
         }
         else
         {
            wb_table1_2_D92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_D92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOW1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContadorfs_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV130ContadorFS_Codigo), 6, 0, ",", "")), ((edtavContadorfs_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV130ContadorFS_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV130ContadorFS_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContadorfs_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContadorfs_codigo_Visible, edtavContadorfs_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV132Contratada_Codigo), 6, 0, ",", "")), ((edtavContratada_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV132Contratada_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV132Contratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratada_codigo_Visible, edtavContratada_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, StringUtil.RTrim( AV70FileName), StringUtil.RTrim( context.localUtil.Format( AV70FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "", "", "", edtavFilename_Visible, edtavFilename_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</p>") ;
         }
         wbLoad = true;
      }

      protected void STARTD92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Importar PF FS", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPD90( ) ;
      }

      protected void WSD92( )
      {
         STARTD92( ) ;
         EVTD92( ) ;
      }

      protected void EVTD92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11D92 */
                              E11D92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12D92 */
                              E12D92 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13D92 */
                                    E13D92 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14D92 */
                              E14D92 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WED92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAD92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavSelcontratada_codigo.Name = "vSELCONTRATADA_CODIGO";
            dynavSelcontratada_codigo.WebTags = "";
            dynavContratoservicos_codigo.Name = "vCONTRATOSERVICOS_CODIGO";
            dynavContratoservicos_codigo.WebTags = "";
            dynavSelcontadorfs_codigo.Name = "vSELCONTADORFS_CODIGO";
            dynavSelcontadorfs_codigo.WebTags = "";
            cmbavRegradivergencia.Name = "vREGRADIVERGENCIA";
            cmbavRegradivergencia.WebTags = "";
            cmbavRegradivergencia.addItem("C", "Indice de aceita��o de diverg�ncia no contrato", 0);
            cmbavRegradivergencia.addItem("M", "Aceitar menor valor de PF da FS", 0);
            if ( cmbavRegradivergencia.ItemCount > 0 )
            {
               AV131RegraDivergencia = cmbavRegradivergencia.getValidValue(AV131RegraDivergencia);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131RegraDivergencia", AV131RegraDivergencia);
            }
            chkavFinal.Name = "vFINAL";
            chkavFinal.WebTags = "";
            chkavFinal.Caption = "Contagem final para demandas com Diverg�ncia";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFinal_Internalname, "TitleCaption", chkavFinal.Caption);
            chkavFinal.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavSelcontratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTRATOSERVICOS_CODIGO_htmlD92( AV124SelContratada_Codigo) ;
         GXVvSELCONTADORFS_CODIGO_htmlD92( AV124SelContratada_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvSELCONTRATADA_CODIGOD92( wwpbaseobjects.SdtWWPContext AV113WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELCONTRATADA_CODIGO_dataD92( AV113WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELCONTRATADA_CODIGO_htmlD92( wwpbaseobjects.SdtWWPContext AV113WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvSELCONTRATADA_CODIGO_dataD92( AV113WWPContext) ;
         gxdynajaxindex = 1;
         dynavSelcontratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelcontratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSelcontratada_codigo.ItemCount > 0 )
         {
            AV124SelContratada_Codigo = (int)(NumberUtil.Val( dynavSelcontratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSELCONTRATADA_CODIGO_dataD92( wwpbaseobjects.SdtWWPContext AV113WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00D92 */
         pr_default.execute(0, new Object[] {AV113WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D92_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D92_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATOSERVICOS_CODIGOD92( int AV124SelContratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOSERVICOS_CODIGO_dataD92( AV124SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOSERVICOS_CODIGO_htmlD92( int AV124SelContratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOSERVICOS_CODIGO_dataD92( AV124SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContratoservicos_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratoservicos_codigo.ItemCount > 0 )
         {
            AV141Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141Contratoservicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATOSERVICOS_CODIGO_dataD92( int AV124SelContratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D93 */
         pr_default.execute(1, new Object[] {AV124SelContratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D93_A160ContratoServicos_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D93_A826ContratoServicos_ServicoSigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvSELCONTADORFS_CODIGOD92( int AV124SelContratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELCONTADORFS_CODIGO_dataD92( AV124SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELCONTADORFS_CODIGO_htmlD92( int AV124SelContratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSELCONTADORFS_CODIGO_dataD92( AV124SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavSelcontadorfs_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelcontadorfs_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSelcontadorfs_codigo.ItemCount > 0 )
         {
            AV125SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSELCONTADORFS_CODIGO_dataD92( int AV124SelContratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D94 */
         pr_default.execute(2, new Object[] {AV124SelContratada_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D94_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D94_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSelcontratada_codigo.ItemCount > 0 )
         {
            AV124SelContratada_Codigo = (int)(NumberUtil.Val( dynavSelcontratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0)));
         }
         if ( dynavContratoservicos_codigo.ItemCount > 0 )
         {
            AV141Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141Contratoservicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0)));
         }
         if ( dynavSelcontadorfs_codigo.ItemCount > 0 )
         {
            AV125SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0)));
         }
         if ( cmbavRegradivergencia.ItemCount > 0 )
         {
            AV131RegraDivergencia = cmbavRegradivergencia.getValidValue(AV131RegraDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131RegraDivergencia", AV131RegraDivergencia);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFD92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContadorfs_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContadorfs_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContadorfs_codigo_Enabled), 5, 0)));
         edtavContratada_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Enabled), 5, 0)));
      }

      protected void RFD92( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12D92 */
         E12D92 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14D92 */
            E14D92 ();
            WBD90( ) ;
         }
      }

      protected void STRUPD90( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContadorfs_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContadorfs_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContadorfs_codigo_Enabled), 5, 0)));
         edtavContratada_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11D92 */
         E11D92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvSELCONTRATADA_CODIGO_htmlD92( AV113WWPContext) ;
         GXVvCONTRATOSERVICOS_CODIGO_htmlD92( AV124SelContratada_Codigo) ;
         GXVvSELCONTADORFS_CODIGO_htmlD92( AV124SelContratada_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavSelcontratada_codigo.CurrentValue = cgiGet( dynavSelcontratada_codigo_Internalname);
            AV124SelContratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelcontratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0)));
            dynavContratoservicos_codigo.CurrentValue = cgiGet( dynavContratoservicos_codigo_Internalname);
            AV141Contratoservicos_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratoservicos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141Contratoservicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0)));
            dynavSelcontadorfs_codigo.CurrentValue = cgiGet( dynavSelcontadorfs_codigo_Internalname);
            AV125SelContadorFS_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelcontadorfs_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0)));
            cmbavRegradivergencia.CurrentValue = cgiGet( cmbavRegradivergencia_Internalname);
            AV131RegraDivergencia = cgiGet( cmbavRegradivergencia_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131RegraDivergencia", AV131RegraDivergencia);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data da Demanda"}), 1, "vDATADMN");
               GX_FocusControl = edtavDatadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV138DataDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV138DataDmn", context.localUtil.Format(AV138DataDmn, "99/99/99"));
            }
            else
            {
               AV138DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatadmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV138DataDmn", context.localUtil.Format(AV138DataDmn, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDataentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de Entrega"}), 1, "vDATAENTREGA");
               GX_FocusControl = edtavDataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV139DataEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV139DataEntrega", context.localUtil.Format(AV139DataEntrega, "99/99/99"));
            }
            else
            {
               AV139DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDataentrega_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV139DataEntrega", context.localUtil.Format(AV139DataEntrega, "99/99/99"));
            }
            AV140DemandaFM = cgiGet( edtavDemandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140DemandaFM", AV140DemandaFM);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRALINHA");
               GX_FocusControl = edtavPralinha_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV96PraLinha = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96PraLinha), 4, 0)));
            }
            else
            {
               AV96PraLinha = (short)(context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96PraLinha), 4, 0)));
            }
            AV23ColDmn = StringUtil.Upper( cgiGet( edtavColdmn_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ColDmn", AV23ColDmn);
            AV38ColPFBFS = StringUtil.Upper( cgiGet( edtavColpfbfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ColPFBFS", AV38ColPFBFS);
            AV45ColPFLFS = StringUtil.Upper( cgiGet( edtavColpflfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ColPFLFS", AV45ColPFLFS);
            AV128ColDataCnt = StringUtil.Upper( cgiGet( edtavColdatacnt_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128ColDataCnt", AV128ColDataCnt);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatacnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Cnt"}), 1, "vDATACNT");
               GX_FocusControl = edtavDatacnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV126DataCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126DataCnt", context.localUtil.Format(AV126DataCnt, "99/99/99"));
            }
            else
            {
               AV126DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatacnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126DataCnt", context.localUtil.Format(AV126DataCnt, "99/99/99"));
            }
            AV122Final = StringUtil.StrToBool( cgiGet( chkavFinal_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122Final", AV122Final);
            AV119ColPFBFM = StringUtil.Upper( cgiGet( edtavColpfbfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119ColPFBFM", AV119ColPFBFM);
            AV118ColPFLFM = StringUtil.Upper( cgiGet( edtavColpflfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118ColPFLFM", AV118ColPFLFM);
            AV134Blob = cgiGet( edtavBlob_Internalname);
            AV123Aba = cgiGet( edtavAba_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123Aba", AV123Aba);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContadorfs_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContadorfs_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTADORFS_CODIGO");
               GX_FocusControl = edtavContadorfs_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV130ContadorFS_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130ContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV130ContadorFS_Codigo), 6, 0)));
            }
            else
            {
               AV130ContadorFS_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContadorfs_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130ContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV130ContadorFS_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_CODIGO");
               GX_FocusControl = edtavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV132Contratada_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV132Contratada_Codigo), 6, 0)));
            }
            else
            {
               AV132Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV132Contratada_Codigo), 6, 0)));
            }
            AV70FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70FileName", AV70FileName);
            /* Read saved values. */
            AV7Arquivo = cgiGet( "vARQUIVO");
            AV24ColDmnn = (short)(context.localUtil.CToN( cgiGet( "vCOLDMNN"), ",", "."));
            AV116ColPFBFSn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFBFSN"), ",", "."));
            AV117ColPFLFSn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFLFSN"), ",", "."));
            AV121ColPFBFMn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFBFMN"), ",", "."));
            AV120ColPFLFMn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFLFMN"), ",", "."));
            AV127ColDataCntn = (short)(context.localUtil.CToN( cgiGet( "vCOLDATACNTN"), ",", "."));
            Confirmpanel_Width = cgiGet( "CONFIRMPANEL_Width");
            Confirmpanel_Height = cgiGet( "CONFIRMPANEL_Height");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
            Confirmpanel_Modal = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Modal"));
            Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Buttoncanceltext = cgiGet( "CONFIRMPANEL_Buttoncanceltext");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Innewwindow1_Target = cgiGet( "INNEWWINDOW1_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV134Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV134Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvSELCONTRATADA_CODIGO_htmlD92( AV113WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11D92 */
         E11D92 ();
         if (returnInSub) return;
      }

      protected void E11D92( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 18/03/2020 23:16", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV113WWPContext) ;
         AV19ColDem = 0;
         AV96PraLinha = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96PraLinha), 4, 0)));
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
         tblTblpffm_Visible = (AV122Final ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblpffm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblpffm_Visible), 5, 0)));
         edtavContadorfs_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContadorfs_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContadorfs_codigo_Visible), 5, 0)));
         edtavContratada_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Visible), 5, 0)));
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"if(nome.split('\\\\').length>1) nome = nome.split('\\\\')[nome.split('\\\\').length-1]; else nome = nome.split('/')[nome.split('/').length-1];";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         tblTbladmin_Visible = (AV113WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTbladmin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTbladmin_Visible), 5, 0)));
         if ( AV113WWPContext.gxTpr_Userehadministradorgam )
         {
            cmbavRegradivergencia.addItem("N", "N�o calcular diverg�ncia nem criar OS da FS", 0);
         }
      }

      protected void E12D92( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV113WWPContext) ;
         bttCarregar_Visible = (AV113WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCarregar_Visible), 5, 0)));
         AV19ColDem = 0;
         AV125SelContadorFS_Codigo = AV130ContadorFS_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0)));
         AV124SelContratada_Codigo = AV132Contratada_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV113WWPContext", AV113WWPContext);
         dynavSelcontadorfs_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontadorfs_codigo_Internalname, "Values", dynavSelcontadorfs_codigo.ToJavascriptSource());
         dynavSelcontratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontratada_codigo_Internalname, "Values", dynavSelcontratada_codigo.ToJavascriptSource());
      }

      public void GXEnter( )
      {
         /* Execute user event: E13D92 */
         E13D92 ();
         if (returnInSub) return;
      }

      protected void E13D92( )
      {
         /* Enter Routine */
         AV7Arquivo = AV134Blob;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Arquivo", AV7Arquivo);
         AV89Ok = true;
         if ( (0==AV132Contratada_Codigo) )
         {
            GX_msglist.addItem("Informe a F�brica de Software!");
            AV89Ok = false;
            GX_FocusControl = dynavSelcontratada_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV132Contratada_Codigo) )
         {
            GX_msglist.addItem("Informe o Servi�o da F�brica de Software!");
            AV89Ok = false;
            GX_FocusControl = dynavContratoservicos_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV130ContadorFS_Codigo) )
         {
            GX_msglist.addItem("Informe o Contador da F�brica de Software!");
            AV89Ok = false;
            GX_FocusControl = edtavContadorfs_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (DateTime.MinValue==AV126DataCnt) && String.IsNullOrEmpty(StringUtil.RTrim( AV128ColDataCnt)) )
         {
            GX_msglist.addItem("Informe coluna ou a data da Contagem!");
            AV89Ok = false;
            GX_FocusControl = edtavDatacnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ! (DateTime.MinValue==AV126DataCnt) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV128ColDataCnt)) )
         {
            GX_msglist.addItem("Informe apenas a coluna ou a data da Contagem!");
            AV89Ok = false;
            GX_FocusControl = edtavDatacnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV96PraLinha) )
         {
            GX_msglist.addItem("Informe a Primeira linha de dados!");
            AV89Ok = false;
            GX_FocusControl = edtavPralinha_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23ColDmn)) )
         {
            GX_msglist.addItem("Informe a coluna das OS");
            AV89Ok = false;
            GX_FocusControl = edtavColdmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38ColPFBFS)) || String.IsNullOrEmpty(StringUtil.RTrim( AV45ColPFLFS)) )
         {
            GX_msglist.addItem("Informe ambas colunas dos PF da FS!");
            AV89Ok = false;
            GX_FocusControl = edtavColpfbfs_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( StringUtil.StringSearch( AV7Arquivo, ".xlsx", 1) == 0 )
         {
            GX_msglist.addItem("Arquivo esperado para processar Excel com extens�o .xlsx!");
            AV89Ok = false;
         }
         if ( AV89Ok )
         {
            AV116ColPFBFSn = (short)(StringUtil.Asc( AV38ColPFBFS)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116ColPFBFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV116ColPFBFSn), 4, 0)));
            AV117ColPFLFSn = (short)(StringUtil.Asc( AV45ColPFLFS)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117ColPFLFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117ColPFLFSn), 4, 0)));
            AV24ColDmnn = (short)(StringUtil.Asc( AV23ColDmn)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ColDmnn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ColDmnn), 4, 0)));
            AV121ColPFBFMn = (short)(StringUtil.Asc( AV119ColPFBFM)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV121ColPFBFMn), 4, 0)));
            AV120ColPFLFMn = (short)(StringUtil.Asc( AV118ColPFLFM)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120ColPFLFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV120ColPFLFMn), 4, 0)));
            AV127ColDataCntn = (short)(StringUtil.Asc( AV128ColDataCnt)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127ColDataCntn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127ColDataCntn), 4, 0)));
            if ( AV113WWPContext.gxTpr_Userehadministradorgam && ! ( StringUtil.StrCmp(AV131RegraDivergencia, "N") == 0 ) )
            {
               Confirmpanel_Title = "Administrador";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel_Internalname, "Title", Confirmpanel_Title);
               Confirmpanel_Confirmtext = "Confirma usar esse c�lculo de diverg�ncia?";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel_Internalname, "ConfirmText", Confirmpanel_Confirmtext);
               this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
            }
            else
            {
               /* Execute user subroutine: 'IMPORTARCONTAGENS' */
               S112 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S112( )
      {
         /* 'IMPORTARCONTAGENS' Routine */
         Innewwindow1_Target = formatLink("aprc_importarpffs.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV7Arquivo)) + "," + UrlEncode(StringUtil.RTrim(AV123Aba)) + "," + UrlEncode("" +AV24ColDmnn) + "," + UrlEncode("" +AV96PraLinha) + "," + UrlEncode("" +AV116ColPFBFSn) + "," + UrlEncode("" +AV117ColPFLFSn) + "," + UrlEncode("" +AV121ColPFBFMn) + "," + UrlEncode("" +AV120ColPFLFMn) + "," + UrlEncode(StringUtil.BoolToStr(AV122Final)) + "," + UrlEncode("" +AV132Contratada_Codigo) + "," + UrlEncode("" +AV130ContadorFS_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV126DataCnt)) + "," + UrlEncode("" +AV127ColDataCntn) + "," + UrlEncode(StringUtil.RTrim(AV131RegraDivergencia)) + "," + UrlEncode(StringUtil.RTrim(AV70FileName)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV138DataDmn)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV139DataEntrega)) + "," + UrlEncode(StringUtil.RTrim(AV140DemandaFM)) + "," + UrlEncode("" +AV141Contratoservicos_Codigo);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow1_Internalname, "Target", Innewwindow1_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOW1Container", "OpenWindow", "", new Object[] {});
      }

      protected void nextLoad( )
      {
      }

      protected void E14D92( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_D92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicostitle_Internalname, "Importa��o dos Pontos de Fun��o da FS:", "", "", lblContratoservicostitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:46px")+"\">") ;
            wb_table2_13_D92( true) ;
         }
         else
         {
            wb_table2_13_D92( false) ;
         }
         return  ;
      }

      protected void wb_table2_13_D92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:26px")+"\" class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            wb_table3_92_D92( true) ;
         }
         else
         {
            wb_table3_92_D92( false) ;
         }
         return  ;
      }

      protected void wb_table3_92_D92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCarregar_Internalname, "", "Importar", bttCarregar_Jsonclick, 5, "Importar", "", StyleString, ClassString, bttCarregar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_D92e( true) ;
         }
         else
         {
            wb_table1_2_D92e( false) ;
         }
      }

      protected void wb_table3_92_D92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134Blob)) )
            {
               gxblobfileaux.Source = AV134Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV134Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV134Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV134Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV134Blob), context.PathToRelativeUrl( AV134Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV134Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 250, "px", 18, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "", "", "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock27_Internalname, "Nome da Aba:", "", "", lblTextblock27_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAba_Internalname, StringUtil.RTrim( AV123Aba), StringUtil.RTrim( context.localUtil.Format( AV123Aba, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAba_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_92_D92e( true) ;
         }
         else
         {
            wb_table3_92_D92e( false) ;
         }
      }

      protected void wb_table2_13_D92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblcast_Internalname, tblTblcast_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock29_Internalname, "F�brica de Software:", "", "", lblTextblock29_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"11\"  class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelcontratada_codigo, dynavSelcontratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0)), 1, dynavSelcontratada_codigo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e15d91_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            dynavSelcontratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV124SelContratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontratada_codigo_Internalname, "Values", (String)(dynavSelcontratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock39_Internalname, "Servi�o:", "", "", lblTextblock39_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"11\"  class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratoservicos_codigo, dynavContratoservicos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0)), 1, dynavContratoservicos_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            dynavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicos_codigo_Internalname, "Values", (String)(dynavContratoservicos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock35_Internalname, "Contador FS:", "", "", lblTextblock35_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"11\"  class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelcontadorfs_codigo, dynavSelcontadorfs_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0)), 1, dynavSelcontadorfs_codigo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e16d91_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            dynavSelcontadorfs_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontadorfs_codigo_Internalname, "Values", (String)(dynavSelcontadorfs_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock38_Internalname, "C�lculo da Diverg�ncia", "", "", lblTextblock38_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"11\"  style=\""+CSSHelper.Prettify( "width:100%")+"\" class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRegradivergencia, cmbavRegradivergencia_Internalname, StringUtil.RTrim( AV131RegraDivergencia), 1, cmbavRegradivergencia_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            cmbavRegradivergencia.CurrentValue = StringUtil.RTrim( AV131RegraDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRegradivergencia_Internalname, "Values", (String)(cmbavRegradivergencia.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            wb_table4_34_D92( true) ;
         }
         else
         {
            wb_table4_34_D92( false) ;
         }
         return  ;
      }

      protected void wb_table4_34_D92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock28_Internalname, "1� Linha �", "", "", lblTextblock28_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPralinha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV96PraLinha), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV96PraLinha), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPralinha_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock34_Internalname, "Colunas:  Demandas �", "", "", lblTextblock34_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdmn_Internalname, StringUtil.RTrim( AV23ColDmn), StringUtil.RTrim( context.localUtil.Format( AV23ColDmn, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdmn_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock30_Internalname, "PFB FS �", "", "", lblTextblock30_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfs_Internalname, StringUtil.RTrim( AV38ColPFBFS), StringUtil.RTrim( context.localUtil.Format( AV38ColPFBFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfs_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock31_Internalname, "PFL FS �", "", "", lblTextblock31_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfs_Internalname, StringUtil.RTrim( AV45ColPFLFS), StringUtil.RTrim( context.localUtil.Format( AV45ColPFLFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfs_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock36_Internalname, "Data da Contagem �", "", "", lblTextblock36_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdatacnt_Internalname, StringUtil.RTrim( AV128ColDataCnt), StringUtil.RTrim( context.localUtil.Format( AV128ColDataCnt, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdatacnt_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock37_Internalname, "Ou:", "", "", lblTextblock37_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatacnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatacnt_Internalname, context.localUtil.Format(AV126DataCnt, "99/99/99"), context.localUtil.Format( AV126DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatacnt_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            GxWebStd.gx_bitmap( context, edtavDatacnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\" colspan=\"12\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            wb_table5_72_D92( true) ;
         }
         else
         {
            wb_table5_72_D92( false) ;
         }
         return  ;
      }

      protected void wb_table5_72_D92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_13_D92e( true) ;
         }
         else
         {
            wb_table2_13_D92e( false) ;
         }
      }

      protected void wb_table5_72_D92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle")+"\" class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavFinal_Internalname, StringUtil.BoolToStr( AV122Final), "", "", 1, 1, "true", "Contagem final para demandas com Diverg�ncia", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(75, this, 'true', 'false');gx.ajax.executeCliEvent('e17d91_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle")+"\" class='DataContentCell'>") ;
            wb_table6_77_D92( true) ;
         }
         else
         {
            wb_table6_77_D92( false) ;
         }
         return  ;
      }

      protected void wb_table6_77_D92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_72_D92e( true) ;
         }
         else
         {
            wb_table5_72_D92e( false) ;
         }
      }

      protected void wb_table6_77_D92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblpffm_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblpffm_Internalname, tblTblpffm_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:59px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock33_Internalname, "PFB FM �", "", "", lblTextblock33_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfm_Internalname, StringUtil.RTrim( AV119ColPFBFM), StringUtil.RTrim( context.localUtil.Format( AV119ColPFBFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock32_Internalname, "PFL FM �", "", "", lblTextblock32_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfm_Internalname, StringUtil.RTrim( AV118ColPFLFM), StringUtil.RTrim( context.localUtil.Format( AV118ColPFLFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_77_D92e( true) ;
         }
         else
         {
            wb_table6_77_D92e( false) ;
         }
      }

      protected void wb_table4_34_D92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTbladmin_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTbladmin_Internalname, tblTbladmin_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Data da Demanda:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatadmn_Internalname, context.localUtil.Format(AV138DataDmn, "99/99/99"), context.localUtil.Format( AV138DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatadmn_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            GxWebStd.gx_bitmap( context, edtavDatadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Data de Entrega:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDataentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDataentrega_Internalname, context.localUtil.Format(AV139DataEntrega, "99/99/99"), context.localUtil.Format( AV139DataEntrega, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDataentrega_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            GxWebStd.gx_bitmap( context, edtavDataentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "N�  OS Interna:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDemandafm_Internalname, AV140DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV140DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDemandafm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_D92e( true) ;
         }
         else
         {
            wb_table4_34_D92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAD92( ) ;
         WSD92( ) ;
         WED92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203182347924");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_importarpffs.js", "?20203182347924");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblContratoservicostitle_Internalname = "CONTRATOSERVICOSTITLE";
         lblTextblock29_Internalname = "TEXTBLOCK29";
         dynavSelcontratada_codigo_Internalname = "vSELCONTRATADA_CODIGO";
         lblTextblock39_Internalname = "TEXTBLOCK39";
         dynavContratoservicos_codigo_Internalname = "vCONTRATOSERVICOS_CODIGO";
         lblTextblock35_Internalname = "TEXTBLOCK35";
         dynavSelcontadorfs_codigo_Internalname = "vSELCONTADORFS_CODIGO";
         lblTextblock38_Internalname = "TEXTBLOCK38";
         cmbavRegradivergencia_Internalname = "vREGRADIVERGENCIA";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavDatadmn_Internalname = "vDATADMN";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDataentrega_Internalname = "vDATAENTREGA";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavDemandafm_Internalname = "vDEMANDAFM";
         tblTbladmin_Internalname = "TBLADMIN";
         lblTextblock28_Internalname = "TEXTBLOCK28";
         edtavPralinha_Internalname = "vPRALINHA";
         lblTextblock34_Internalname = "TEXTBLOCK34";
         edtavColdmn_Internalname = "vCOLDMN";
         lblTextblock30_Internalname = "TEXTBLOCK30";
         edtavColpfbfs_Internalname = "vCOLPFBFS";
         lblTextblock31_Internalname = "TEXTBLOCK31";
         edtavColpflfs_Internalname = "vCOLPFLFS";
         lblTextblock36_Internalname = "TEXTBLOCK36";
         edtavColdatacnt_Internalname = "vCOLDATACNT";
         lblTextblock37_Internalname = "TEXTBLOCK37";
         edtavDatacnt_Internalname = "vDATACNT";
         chkavFinal_Internalname = "vFINAL";
         lblTextblock33_Internalname = "TEXTBLOCK33";
         edtavColpfbfm_Internalname = "vCOLPFBFM";
         lblTextblock32_Internalname = "TEXTBLOCK32";
         edtavColpflfm_Internalname = "vCOLPFLFM";
         tblTblpffm_Internalname = "TBLPFFM";
         tblTable2_Internalname = "TABLE2";
         tblTblcast_Internalname = "TBLCAST";
         edtavBlob_Internalname = "vBLOB";
         lblTextblock27_Internalname = "TEXTBLOCK27";
         edtavAba_Internalname = "vABA";
         tblTable3_Internalname = "TABLE3";
         bttCarregar_Internalname = "CARREGAR";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         tblTable1_Internalname = "TABLE1";
         Innewwindow1_Internalname = "INNEWWINDOW1";
         edtavContadorfs_codigo_Internalname = "vCONTADORFS_CODIGO";
         edtavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         edtavFilename_Internalname = "vFILENAME";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavDemandafm_Jsonclick = "";
         edtavDataentrega_Jsonclick = "";
         edtavDatadmn_Jsonclick = "";
         edtavColpflfm_Jsonclick = "";
         edtavColpfbfm_Jsonclick = "";
         edtavDatacnt_Jsonclick = "";
         edtavColdatacnt_Jsonclick = "";
         edtavColpflfs_Jsonclick = "";
         edtavColpfbfs_Jsonclick = "";
         edtavColdmn_Jsonclick = "";
         edtavPralinha_Jsonclick = "";
         cmbavRegradivergencia_Jsonclick = "";
         dynavSelcontadorfs_codigo_Jsonclick = "";
         dynavContratoservicos_codigo_Jsonclick = "";
         dynavSelcontratada_codigo_Jsonclick = "";
         edtavAba_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         bttCarregar_Visible = 1;
         tblTbladmin_Visible = 1;
         tblTblpffm_Visible = 1;
         chkavFinal.Caption = "";
         edtavFilename_Jsonclick = "";
         edtavFilename_Enabled = 1;
         edtavFilename_Visible = 1;
         edtavContratada_codigo_Jsonclick = "";
         edtavContratada_codigo_Enabled = 1;
         edtavContratada_codigo_Visible = 1;
         edtavContadorfs_codigo_Jsonclick = "";
         edtavContadorfs_codigo_Enabled = 1;
         edtavContadorfs_codigo_Visible = 1;
         Innewwindow1_Target = "";
         Confirmpanel_Confirmtype = "1";
         Confirmpanel_Buttoncanceltext = "Cancelar";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Sim";
         Confirmpanel_Confirmtext = "";
         Confirmpanel_Modal = Convert.ToBoolean( 0);
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "";
         Confirmpanel_Height = "50px";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Importar PF FS";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Selcontratada_codigo( GXCombobox dynGX_Parm1 ,
                                               GXCombobox dynGX_Parm2 ,
                                               GXCombobox dynGX_Parm3 )
      {
         dynavSelcontratada_codigo = dynGX_Parm1;
         AV124SelContratada_Codigo = (int)(NumberUtil.Val( dynavSelcontratada_codigo.CurrentValue, "."));
         dynavContratoservicos_codigo = dynGX_Parm2;
         AV141Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.CurrentValue, "."));
         dynavSelcontadorfs_codigo = dynGX_Parm3;
         AV125SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.CurrentValue, "."));
         GXVvCONTRATOSERVICOS_CODIGO_htmlD92( AV124SelContratada_Codigo) ;
         GXVvSELCONTADORFS_CODIGO_htmlD92( AV124SelContratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContratoservicos_codigo.ItemCount > 0 )
         {
            AV141Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0))), "."));
         }
         dynavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV141Contratoservicos_Codigo), 6, 0));
         isValidOutput.Add(dynavContratoservicos_codigo);
         if ( dynavSelcontadorfs_codigo.ItemCount > 0 )
         {
            AV125SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0))), "."));
         }
         dynavSelcontadorfs_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV125SelContadorFS_Codigo), 6, 0));
         isValidOutput.Add(dynavSelcontadorfs_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV130ContadorFS_Codigo',fld:'vCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV113WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'CARREGAR',prop:'Visible'},{av:'AV125SelContadorFS_Codigo',fld:'vSELCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV124SelContratada_Codigo',fld:'vSELCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSELCONTRATADA_CODIGO.CLICK","{handler:'E15D91',iparms:[{av:'AV124SelContratada_Codigo',fld:'vSELCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV132Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSELCONTADORFS_CODIGO.CLICK","{handler:'E16D91',iparms:[{av:'AV125SelContadorFS_Codigo',fld:'vSELCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV130ContadorFS_Codigo',fld:'vCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E13D92',iparms:[{av:'AV134Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV124SelContratada_Codigo',fld:'vSELCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV141Contratoservicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV132Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV130ContadorFS_Codigo',fld:'vCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV128ColDataCnt',fld:'vCOLDATACNT',pic:'@!',nv:''},{av:'AV126DataCnt',fld:'vDATACNT',pic:'',nv:''},{av:'AV96PraLinha',fld:'vPRALINHA',pic:'ZZZ9',nv:0},{av:'AV23ColDmn',fld:'vCOLDMN',pic:'@!',nv:''},{av:'AV45ColPFLFS',fld:'vCOLPFLFS',pic:'@!',nv:''},{av:'AV38ColPFBFS',fld:'vCOLPFBFS',pic:'@!',nv:''},{av:'AV119ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV118ColPFLFM',fld:'vCOLPFLFM',pic:'@!',nv:''},{av:'AV113WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV131RegraDivergencia',fld:'vREGRADIVERGENCIA',pic:'',nv:''},{av:'AV7Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV123Aba',fld:'vABA',pic:'',nv:''},{av:'AV24ColDmnn',fld:'vCOLDMNN',pic:'ZZZ9',nv:0},{av:'AV116ColPFBFSn',fld:'vCOLPFBFSN',pic:'ZZZ9',nv:0},{av:'AV117ColPFLFSn',fld:'vCOLPFLFSN',pic:'ZZZ9',nv:0},{av:'AV121ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV120ColPFLFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{av:'AV122Final',fld:'vFINAL',pic:'',nv:false},{av:'AV127ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'AV70FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV138DataDmn',fld:'vDATADMN',pic:'',nv:''},{av:'AV139DataEntrega',fld:'vDATAENTREGA',pic:'',nv:''},{av:'AV140DemandaFM',fld:'vDEMANDAFM',pic:'',nv:''}],oparms:[{av:'AV7Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV116ColPFBFSn',fld:'vCOLPFBFSN',pic:'ZZZ9',nv:0},{av:'AV117ColPFLFSn',fld:'vCOLPFLFSN',pic:'ZZZ9',nv:0},{av:'AV24ColDmnn',fld:'vCOLDMNN',pic:'ZZZ9',nv:0},{av:'AV121ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV120ColPFLFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{av:'AV127ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'Confirmpanel_Title',ctrl:'CONFIRMPANEL',prop:'Title'},{av:'Confirmpanel_Confirmtext',ctrl:'CONFIRMPANEL',prop:'ConfirmText'},{av:'Innewwindow1_Target',ctrl:'INNEWWINDOW1',prop:'Target'}]}");
         setEventMetadata("VFINAL.CLICK","{handler:'E17D91',iparms:[{av:'AV122Final',fld:'vFINAL',pic:'',nv:false}],oparms:[{av:'tblTblpffm_Visible',ctrl:'TBLPFFM',prop:'Visible'},{av:'AV119ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV118ColPFLFM',fld:'vCOLPFLFM',pic:'@!',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Confirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV113WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7Arquivo = "";
         GXCCtlgxBlob = "";
         AV134Blob = "";
         Confirmpanel_Width = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV70FileName = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV131RegraDivergencia = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00D92_A40Contratada_PessoaCod = new int[1] ;
         H00D92_A39Contratada_Codigo = new int[1] ;
         H00D92_A41Contratada_PessoaNom = new String[] {""} ;
         H00D92_n41Contratada_PessoaNom = new bool[] {false} ;
         H00D92_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00D92_A516Contratada_TipoFabrica = new String[] {""} ;
         H00D92_A43Contratada_Ativo = new bool[] {false} ;
         H00D93_A74Contrato_Codigo = new int[1] ;
         H00D93_A155Servico_Codigo = new int[1] ;
         H00D93_A160ContratoServicos_Codigo = new int[1] ;
         H00D93_A39Contratada_Codigo = new int[1] ;
         H00D93_A92Contrato_Ativo = new bool[] {false} ;
         H00D93_A826ContratoServicos_ServicoSigla = new String[] {""} ;
         H00D94_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00D94_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00D94_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00D94_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00D94_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00D94_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV138DataDmn = DateTime.MinValue;
         AV139DataEntrega = DateTime.MinValue;
         AV140DemandaFM = "";
         AV23ColDmn = "";
         AV38ColPFBFS = "";
         AV45ColPFLFS = "";
         AV128ColDataCnt = "";
         AV126DataCnt = DateTime.MinValue;
         AV119ColPFBFM = "";
         AV118ColPFLFM = "";
         AV123Aba = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblContratoservicostitle_Jsonclick = "";
         bttCarregar_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock27_Jsonclick = "";
         lblTextblock29_Jsonclick = "";
         lblTextblock39_Jsonclick = "";
         lblTextblock35_Jsonclick = "";
         lblTextblock38_Jsonclick = "";
         lblTextblock28_Jsonclick = "";
         lblTextblock34_Jsonclick = "";
         lblTextblock30_Jsonclick = "";
         lblTextblock31_Jsonclick = "";
         lblTextblock36_Jsonclick = "";
         lblTextblock37_Jsonclick = "";
         lblTextblock33_Jsonclick = "";
         lblTextblock32_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_importarpffs__default(),
            new Object[][] {
                new Object[] {
               H00D92_A40Contratada_PessoaCod, H00D92_A39Contratada_Codigo, H00D92_A41Contratada_PessoaNom, H00D92_n41Contratada_PessoaNom, H00D92_A52Contratada_AreaTrabalhoCod, H00D92_A516Contratada_TipoFabrica, H00D92_A43Contratada_Ativo
               }
               , new Object[] {
               H00D93_A74Contrato_Codigo, H00D93_A155Servico_Codigo, H00D93_A160ContratoServicos_Codigo, H00D93_A39Contratada_Codigo, H00D93_A92Contrato_Ativo, H00D93_A826ContratoServicos_ServicoSigla
               }
               , new Object[] {
               H00D94_A70ContratadaUsuario_UsuarioPessoaCod, H00D94_n70ContratadaUsuario_UsuarioPessoaCod, H00D94_A69ContratadaUsuario_UsuarioCod, H00D94_A71ContratadaUsuario_UsuarioPessoaNom, H00D94_n71ContratadaUsuario_UsuarioPessoaNom, H00D94_A66ContratadaUsuario_ContratadaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContadorfs_codigo_Enabled = 0;
         edtavContratada_codigo_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV24ColDmnn ;
      private short AV116ColPFBFSn ;
      private short AV117ColPFLFSn ;
      private short AV121ColPFBFMn ;
      private short AV120ColPFLFMn ;
      private short AV127ColDataCntn ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV96PraLinha ;
      private short AV19ColDem ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV124SelContratada_Codigo ;
      private int AV130ContadorFS_Codigo ;
      private int edtavContadorfs_codigo_Enabled ;
      private int edtavContadorfs_codigo_Visible ;
      private int AV132Contratada_Codigo ;
      private int edtavContratada_codigo_Enabled ;
      private int edtavContratada_codigo_Visible ;
      private int edtavFilename_Visible ;
      private int edtavFilename_Enabled ;
      private int gxdynajaxindex ;
      private int AV141Contratoservicos_Codigo ;
      private int AV125SelContadorFS_Codigo ;
      private int tblTblpffm_Visible ;
      private int tblTbladmin_Visible ;
      private int bttCarregar_Visible ;
      private int idxLst ;
      private String Confirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV7Arquivo ;
      private String GXCCtlgxBlob ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Height ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Buttoncanceltext ;
      private String Confirmpanel_Confirmtype ;
      private String Innewwindow1_Target ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavContadorfs_codigo_Internalname ;
      private String edtavContadorfs_codigo_Jsonclick ;
      private String edtavContratada_codigo_Internalname ;
      private String edtavContratada_codigo_Jsonclick ;
      private String edtavFilename_Internalname ;
      private String AV70FileName ;
      private String edtavFilename_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV131RegraDivergencia ;
      private String chkavFinal_Internalname ;
      private String dynavSelcontratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavContratoservicos_codigo_Internalname ;
      private String dynavSelcontadorfs_codigo_Internalname ;
      private String cmbavRegradivergencia_Internalname ;
      private String edtavDatadmn_Internalname ;
      private String edtavDataentrega_Internalname ;
      private String edtavDemandafm_Internalname ;
      private String edtavPralinha_Internalname ;
      private String AV23ColDmn ;
      private String edtavColdmn_Internalname ;
      private String AV38ColPFBFS ;
      private String edtavColpfbfs_Internalname ;
      private String AV45ColPFLFS ;
      private String edtavColpflfs_Internalname ;
      private String AV128ColDataCnt ;
      private String edtavColdatacnt_Internalname ;
      private String edtavDatacnt_Internalname ;
      private String AV119ColPFBFM ;
      private String edtavColpfbfm_Internalname ;
      private String AV118ColPFLFM ;
      private String edtavColpflfm_Internalname ;
      private String edtavBlob_Internalname ;
      private String AV123Aba ;
      private String edtavAba_Internalname ;
      private String tblTblpffm_Internalname ;
      private String tblTbladmin_Internalname ;
      private String bttCarregar_Internalname ;
      private String Confirmpanel_Internalname ;
      private String Innewwindow1_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblContratoservicostitle_Internalname ;
      private String lblContratoservicostitle_Jsonclick ;
      private String bttCarregar_Jsonclick ;
      private String tblTable3_Internalname ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock27_Internalname ;
      private String lblTextblock27_Jsonclick ;
      private String edtavAba_Jsonclick ;
      private String tblTblcast_Internalname ;
      private String lblTextblock29_Internalname ;
      private String lblTextblock29_Jsonclick ;
      private String dynavSelcontratada_codigo_Jsonclick ;
      private String lblTextblock39_Internalname ;
      private String lblTextblock39_Jsonclick ;
      private String dynavContratoservicos_codigo_Jsonclick ;
      private String lblTextblock35_Internalname ;
      private String lblTextblock35_Jsonclick ;
      private String dynavSelcontadorfs_codigo_Jsonclick ;
      private String lblTextblock38_Internalname ;
      private String lblTextblock38_Jsonclick ;
      private String cmbavRegradivergencia_Jsonclick ;
      private String lblTextblock28_Internalname ;
      private String lblTextblock28_Jsonclick ;
      private String edtavPralinha_Jsonclick ;
      private String lblTextblock34_Internalname ;
      private String lblTextblock34_Jsonclick ;
      private String edtavColdmn_Jsonclick ;
      private String lblTextblock30_Internalname ;
      private String lblTextblock30_Jsonclick ;
      private String edtavColpfbfs_Jsonclick ;
      private String lblTextblock31_Internalname ;
      private String lblTextblock31_Jsonclick ;
      private String edtavColpflfs_Jsonclick ;
      private String lblTextblock36_Internalname ;
      private String lblTextblock36_Jsonclick ;
      private String edtavColdatacnt_Jsonclick ;
      private String lblTextblock37_Internalname ;
      private String lblTextblock37_Jsonclick ;
      private String edtavDatacnt_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblock33_Internalname ;
      private String lblTextblock33_Jsonclick ;
      private String edtavColpfbfm_Jsonclick ;
      private String lblTextblock32_Internalname ;
      private String lblTextblock32_Jsonclick ;
      private String edtavColpflfm_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavDatadmn_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavDataentrega_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavDemandafm_Jsonclick ;
      private DateTime AV138DataDmn ;
      private DateTime AV139DataEntrega ;
      private DateTime AV126DataCnt ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Confirmpanel_Modal ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV122Final ;
      private bool returnInSub ;
      private bool AV89Ok ;
      private String AV140DemandaFM ;
      private String AV134Blob ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavSelcontratada_codigo ;
      private GXCombobox dynavContratoservicos_codigo ;
      private GXCombobox dynavSelcontadorfs_codigo ;
      private GXCombobox cmbavRegradivergencia ;
      private GXCheckbox chkavFinal ;
      private IDataStoreProvider pr_default ;
      private int[] H00D92_A40Contratada_PessoaCod ;
      private int[] H00D92_A39Contratada_Codigo ;
      private String[] H00D92_A41Contratada_PessoaNom ;
      private bool[] H00D92_n41Contratada_PessoaNom ;
      private int[] H00D92_A52Contratada_AreaTrabalhoCod ;
      private String[] H00D92_A516Contratada_TipoFabrica ;
      private bool[] H00D92_A43Contratada_Ativo ;
      private int[] H00D93_A74Contrato_Codigo ;
      private int[] H00D93_A155Servico_Codigo ;
      private int[] H00D93_A160ContratoServicos_Codigo ;
      private int[] H00D93_A39Contratada_Codigo ;
      private bool[] H00D93_A92Contrato_Ativo ;
      private String[] H00D93_A826ContratoServicos_ServicoSigla ;
      private int[] H00D94_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00D94_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00D94_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00D94_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00D94_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00D94_A66ContratadaUsuario_ContratadaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV113WWPContext ;
   }

   public class wp_importarpffs__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00D92 ;
          prmH00D92 = new Object[] {
          new Object[] {"@AV113WWP_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00D93 ;
          prmH00D93 = new Object[] {
          new Object[] {"@AV124SelContratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00D94 ;
          prmH00D94 = new Object[] {
          new Object[] {"@AV124SelContratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00D92", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_TipoFabrica], T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV113WWP_1Areatrabalho_codigo) AND (T1.[Contratada_TipoFabrica] = 'S') ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D92,0,0,true,false )
             ,new CursorDef("H00D93", "SELECT T3.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T3.[Contratada_Codigo], T3.[Contrato_Ativo], T2.[Servico_Sigla] AS ContratoServicos_ServicoSigla FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T3.[Contrato_Ativo] = 1) AND (T3.[Contratada_Codigo] = @AV124SelContratada_Codigo) ORDER BY [ContratoServicos_ServicoSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D93,0,0,true,false )
             ,new CursorDef("H00D94", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV124SelContratada_Codigo ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D94,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
