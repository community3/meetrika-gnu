/*
               File: WP_HomologarProposta
        Description: OS X
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:50:8.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_homologarproposta : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_homologarproposta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_homologarproposta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Proposta_OSCodigo ,
                           int aP1_UserId )
      {
         this.A1686Proposta_OSCodigo = aP0_Proposta_OSCodigo;
         this.AV13UserId = aP1_UserId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynProposta_OSServico = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PROPOSTA_OSSERVICO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLAPROPOSTA_OSSERVICOMV2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A1686Proposta_OSCodigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV13UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13UserId), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13UserId), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423501058");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_homologarproposta.aspx") + "?" + UrlEncode("" +A1686Proposta_OSCodigo) + "," + UrlEncode("" +AV13UserId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_ACAO", StringUtil.RTrim( A894LogResponsavel_Acao));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPRAZOENTREGA", context.localUtil.TToC( AV18PrazoEntrega, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV5Confirmado);
         GxWebStd.gx_hidden_field( context, "vDIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Dias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV22TipoDias));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "LOGRESPONSAVEL_OWNEREHCONTRATANTE", A1149LogResponsavel_OwnerEhContratante);
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_OBJETIVO", GetSecureSignedToken( "", A1690Proposta_Objetivo));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_ESFORCO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_OBJETIVO", GetSecureSignedToken( "", A1690Proposta_Objetivo));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_ESFORCO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_OBJETIVO", GetSecureSignedToken( "", A1690Proposta_Objetivo));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_ESFORCO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_OBJETIVO", GetSecureSignedToken( "", A1690Proposta_Objetivo));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_ESFORCO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_OBJETIVO", GetSecureSignedToken( "", A1690Proposta_Objetivo));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_ESFORCO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13UserId), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13UserId), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttoncanceltext", StringUtil.RTrim( Confirmpanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_HomologarProposta";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_homologarproposta:[SendSecurityCheck value for]"+"Proposta_PrazoDias:"+context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_homologarproposta.aspx") + "?" + UrlEncode("" +A1686Proposta_OSCodigo) + "," + UrlEncode("" +AV13UserId) ;
      }

      public override String GetPgmname( )
      {
         return "WP_HomologarProposta" ;
      }

      public override String GetPgmdesc( )
      {
         return "OS X" ;
      }

      protected void WBMV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_MV2( true) ;
         }
         else
         {
            wb_table1_2_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_OSCodigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1686Proposta_OSCodigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1686Proposta_OSCodigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_OSCodigo_Jsonclick, 0, "Attribute", "", "", "", edtProposta_OSCodigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_HomologarProposta.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A490ContagemResultado_ContratadaCod), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_ContratadaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultado_ContratadaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_HomologarProposta.htm");
         }
         wbLoad = true;
      }

      protected void STARTMV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "OS X", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMV0( ) ;
      }

      protected void WSMV2( )
      {
         STARTMV2( ) ;
         EVTMV2( ) ;
      }

      protected void EVTMV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MV2 */
                              E11MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12MV2 */
                              E12MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOBTNHMLEXC'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13MV2 */
                              E13MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOBTNINDEFERIR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14MV2 */
                              E14MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15MV2 */
                              E15MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16MV2 */
                              E16MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17MV2 */
                              E17MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDATAINICIO.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18MV2 */
                              E18MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDATAINICIO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19MV2 */
                              E19MV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynProposta_OSServico.Name = "PROPOSTA_OSSERVICO";
            dynProposta_OSServico.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSaldocontrato_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPROPOSTA_OSSERVICOMV2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROPOSTA_OSSERVICO_dataMV2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROPOSTA_OSSERVICO_htmlMV2( )
      {
         int gxdynajaxvalue ;
         GXDLAPROPOSTA_OSSERVICO_dataMV2( ) ;
         gxdynajaxindex = 1;
         dynProposta_OSServico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProposta_OSServico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynProposta_OSServico.ItemCount > 0 )
         {
            A1705Proposta_OSServico = (int)(NumberUtil.Val( dynProposta_OSServico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0))), "."));
            n1705Proposta_OSServico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
         }
      }

      protected void GXDLAPROPOSTA_OSSERVICO_dataMV2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00MV2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00MV2_A160ContratoServicos_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00MV2_A605Servico_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynProposta_OSServico.ItemCount > 0 )
         {
            A1705Proposta_OSServico = (int)(NumberUtil.Val( dynProposta_OSServico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0))), "."));
            n1705Proposta_OSServico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSaldocontrato_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_Enabled), 5, 0)));
         edtavProposta_prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProposta_prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProposta_prazo_Enabled), 5, 0)));
      }

      protected void RFMV2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E16MV2 */
         E16MV2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00MV3 */
            pr_default.execute(1, new Object[] {A1686Proposta_OSCodigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1704Proposta_CntSrvCod = H00MV3_A1704Proposta_CntSrvCod[0];
               A1702Proposta_Prazo = H00MV3_A1702Proposta_Prazo[0];
               A1793Proposta_PrazoDias = H00MV3_A1793Proposta_PrazoDias[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1793Proposta_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1793Proposta_PrazoDias), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
               n1793Proposta_PrazoDias = H00MV3_n1793Proposta_PrazoDias[0];
               A1688Proposta_Valor = H00MV3_A1688Proposta_Valor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1688Proposta_Valor", StringUtil.LTrim( StringUtil.Str( A1688Proposta_Valor, 15, 4)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")));
               A1703Proposta_Esforco = H00MV3_A1703Proposta_Esforco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1703Proposta_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A1703Proposta_Esforco), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_ESFORCO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")));
               A1690Proposta_Objetivo = H00MV3_A1690Proposta_Objetivo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1690Proposta_Objetivo", A1690Proposta_Objetivo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_OBJETIVO", GetSecureSignedToken( "", A1690Proposta_Objetivo));
               GXAPROPOSTA_OSSERVICO_htmlMV2( ) ;
               /* Execute user event: E17MV2 */
               E17MV2 ();
               pr_default.readNext(1);
            }
            pr_default.close(1);
            WBMV0( ) ;
         }
      }

      protected void STRUPMV0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavSaldocontrato_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_Enabled), 5, 0)));
         edtavProposta_prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavProposta_prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavProposta_prazo_Enabled), 5, 0)));
         GXAPROPOSTA_OSSERVICO_htmlMV2( ) ;
         /* Using cursor H00MV4 */
         pr_default.execute(2, new Object[] {n1705Proposta_OSServico, A1705Proposta_OSServico});
         A1713Proposta_OSUndCntCod = H00MV4_A1713Proposta_OSUndCntCod[0];
         n1713Proposta_OSUndCntCod = H00MV4_n1713Proposta_OSUndCntCod[0];
         A1721Proposta_OSPrzRsp = H00MV4_A1721Proposta_OSPrzRsp[0];
         n1721Proposta_OSPrzRsp = H00MV4_n1721Proposta_OSPrzRsp[0];
         A1707Proposta_OSPrzTpDias = H00MV4_A1707Proposta_OSPrzTpDias[0];
         n1707Proposta_OSPrzTpDias = H00MV4_n1707Proposta_OSPrzTpDias[0];
         pr_default.close(2);
         /* Using cursor H00MV5 */
         pr_default.execute(3, new Object[] {n1713Proposta_OSUndCntCod, A1713Proposta_OSUndCntCod});
         A1710Proposta_OSUndCntSgl = H00MV5_A1710Proposta_OSUndCntSgl[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
         n1710Proposta_OSUndCntSgl = H00MV5_n1710Proposta_OSUndCntSgl[0];
         pr_default.close(3);
         /* Using cursor H00MV6 */
         pr_default.execute(4, new Object[] {A1686Proposta_OSCodigo});
         A1764Proposta_OSDmn = H00MV6_A1764Proposta_OSDmn[0];
         n1764Proposta_OSDmn = H00MV6_n1764Proposta_OSDmn[0];
         A912ContagemResultado_HoraEntrega = H00MV6_A912ContagemResultado_HoraEntrega[0];
         n912ContagemResultado_HoraEntrega = H00MV6_n912ContagemResultado_HoraEntrega[0];
         A490ContagemResultado_ContratadaCod = H00MV6_A490ContagemResultado_ContratadaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
         n490ContagemResultado_ContratadaCod = H00MV6_n490ContagemResultado_ContratadaCod[0];
         A1705Proposta_OSServico = H00MV6_A1705Proposta_OSServico[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
         n1705Proposta_OSServico = H00MV6_n1705Proposta_OSServico[0];
         pr_default.close(4);
         /* Using cursor H00MV7 */
         pr_default.execute(5, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         A803ContagemResultado_ContratadaSigla = H00MV7_A803ContagemResultado_ContratadaSigla[0];
         n803ContagemResultado_ContratadaSigla = H00MV7_n803ContagemResultado_ContratadaSigla[0];
         pr_default.close(5);
         /* Using cursor H00MV8 */
         pr_default.execute(6, new Object[] {A1686Proposta_OSCodigo});
         A1627ContagemResultado_CntSrvVncCod = H00MV8_A1627ContagemResultado_CntSrvVncCod[0];
         n1627ContagemResultado_CntSrvVncCod = H00MV8_n1627ContagemResultado_CntSrvVncCod[0];
         pr_default.close(6);
         /* Using cursor H00MV9 */
         pr_default.execute(7, new Object[] {n1627ContagemResultado_CntSrvVncCod, A1627ContagemResultado_CntSrvVncCod});
         A1454ContratoServicos_PrazoTpDias = H00MV9_A1454ContratoServicos_PrazoTpDias[0];
         n1454ContratoServicos_PrazoTpDias = H00MV9_n1454ContratoServicos_PrazoTpDias[0];
         pr_default.close(7);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12MV2 */
         E12MV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1690Proposta_Objetivo = cgiGet( edtProposta_Objetivo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1690Proposta_Objetivo", A1690Proposta_Objetivo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_OBJETIVO", GetSecureSignedToken( "", A1690Proposta_Objetivo));
            dynProposta_OSServico.CurrentValue = cgiGet( dynProposta_OSServico_Internalname);
            A1705Proposta_OSServico = (int)(NumberUtil.Val( cgiGet( dynProposta_OSServico_Internalname), "."));
            n1705Proposta_OSServico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1705Proposta_OSServico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)));
            A1703Proposta_Esforco = (int)(context.localUtil.CToN( cgiGet( edtProposta_Esforco_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1703Proposta_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A1703Proposta_Esforco), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_ESFORCO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9")));
            A1710Proposta_OSUndCntSgl = StringUtil.Upper( cgiGet( edtProposta_OSUndCntSgl_Internalname));
            n1710Proposta_OSUndCntSgl = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1710Proposta_OSUndCntSgl", A1710Proposta_OSUndCntSgl);
            A1688Proposta_Valor = context.localUtil.CToN( cgiGet( edtProposta_Valor_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1688Proposta_Valor", StringUtil.LTrim( StringUtil.Str( A1688Proposta_Valor, 15, 4)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99")));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO");
               GX_FocusControl = edtavSaldocontrato_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19SaldoContrato = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19SaldoContrato", StringUtil.LTrim( StringUtil.Str( AV19SaldoContrato, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            else
            {
               AV19SaldoContrato = context.localUtil.CToN( cgiGet( edtavSaldocontrato_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19SaldoContrato", StringUtil.LTrim( StringUtil.Str( AV19SaldoContrato, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatainicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Inicio"}), 1, "vDATAINICIO");
               GX_FocusControl = edtavDatainicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24DataInicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DataInicio", context.localUtil.Format(AV24DataInicio, "99/99/99"));
            }
            else
            {
               AV24DataInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatainicio_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DataInicio", context.localUtil.Format(AV24DataInicio, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavProposta_prazo_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Prazo"}), 1, "vPROPOSTA_PRAZO");
               GX_FocusControl = edtavProposta_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20Proposta_Prazo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Proposta_Prazo", context.localUtil.Format(AV20Proposta_Prazo, "99/99/99"));
            }
            else
            {
               AV20Proposta_Prazo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavProposta_prazo_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Proposta_Prazo", context.localUtil.Format(AV20Proposta_Prazo, "99/99/99"));
            }
            A1793Proposta_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtProposta_PrazoDias_Internalname), ",", "."));
            n1793Proposta_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1793Proposta_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1793Proposta_PrazoDias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
            A490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_ContratadaCod_Internalname), ",", "."));
            n490ContagemResultado_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Buttoncanceltext = cgiGet( "CONFIRMPANEL_Buttoncanceltext");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Confirmpanel_Result = cgiGet( "CONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_HomologarProposta";
            A1793Proposta_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtProposta_PrazoDias_Internalname), ",", "."));
            n1793Proposta_PrazoDias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1793Proposta_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1793Proposta_PrazoDias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PROPOSTA_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_homologarproposta:[SecurityCheckFailed value for]"+"Proposta_PrazoDias:"+context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXAPROPOSTA_OSSERVICO_htmlMV2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12MV2 */
         E12MV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12MV2( )
      {
         /* Start Routine */
         edtProposta_OSCodigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProposta_OSCodigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProposta_OSCodigo_Visible), 5, 0)));
         edtContagemResultado_ContratadaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_ContratadaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_ContratadaCod_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         bttBtnbtnhmlexc_Visible = (AV6Context.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbtnhmlexc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnbtnhmlexc_Visible), 5, 0)));
         bttBtnbtnhmlnaoexc_Visible = (AV6Context.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbtnhmlnaoexc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnbtnhmlnaoexc_Visible), 5, 0)));
         bttBtnbtnindeferir_Visible = (AV6Context.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnbtnindeferir_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnbtnindeferir_Visible), 5, 0)));
      }

      protected void E13MV2( )
      {
         /* 'DobtnHmlExc' Routine */
         if ( AV24DataInicio < DateTimeUtil.ServerDate( context, "DEFAULT") )
         {
            GX_msglist.addItem("Data prevista de inicio incorreta!");
            GX_FocusControl = edtavDatainicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            GXt_dtime1 = DateTimeUtil.ResetTime( AV24DataInicio ) ;
            GXt_dtime2 = DateTimeUtil.ResetTime( AV20Proposta_Prazo ) ;
            new prc_executarpropostadaos(context ).execute( ref  A1686Proposta_OSCodigo,  A490ContagemResultado_ContratadaCod,  AV13UserId,  GXt_dtime1, ref  GXt_dtime2) ;
            AV20Proposta_Prazo = DateTimeUtil.ResetTime((DateTime)(GXt_dtime2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13UserId), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13UserId), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DataInicio", context.localUtil.Format(AV24DataInicio, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Proposta_Prazo", context.localUtil.Format(AV20Proposta_Prazo, "99/99/99"));
            /* Execute user subroutine: 'FECHAR' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E14MV2( )
      {
         /* 'DobtnIndeferir' Routine */
         AV15ContagemResultado.Load(A1686Proposta_OSCodigo);
         AV17StatusDmn = AV15ContagemResultado.gxTpr_Contagemresultado_statusdmn;
         AV15ContagemResultado.gxTpr_Contagemresultado_statusdmn = "J";
         /* Using cursor H00MV10 */
         pr_default.execute(8, new Object[] {A1686Proposta_OSCodigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A894LogResponsavel_Acao = H00MV10_A894LogResponsavel_Acao[0];
            A1797LogResponsavel_Codigo = H00MV10_A1797LogResponsavel_Codigo[0];
            A896LogResponsavel_Owner = H00MV10_A896LogResponsavel_Owner[0];
            A892LogResponsavel_DemandaCod = H00MV10_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00MV10_n892LogResponsavel_DemandaCod[0];
            GXt_boolean3 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
            if ( ! A1149LogResponsavel_OwnerEhContratante )
            {
               /* Using cursor H00MV11 */
               pr_default.execute(9, new Object[] {AV15ContagemResultado.gxTpr_Contagemresultado_contratadacod, A896LogResponsavel_Owner});
               while ( (pr_default.getStatus(9) != 101) )
               {
                  A69ContratadaUsuario_UsuarioCod = H00MV11_A69ContratadaUsuario_UsuarioCod[0];
                  A66ContratadaUsuario_ContratadaCod = H00MV11_A66ContratadaUsuario_ContratadaCod[0];
                  AV15ContagemResultado.gxTpr_Contagemresultado_responsavel = A896LogResponsavel_Owner;
                  AV16Achado = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(9);
               if ( AV16Achado )
               {
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
            }
            pr_default.readNext(8);
         }
         pr_default.close(8);
         AV15ContagemResultado.gxTpr_Contagemresultado_dataentrega = AV18PrazoEntrega;
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_contratadaorigemcod_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_contadorfscod_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_loteaceitecod_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_sistemacod_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_naocnfdmncod_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_servicoss_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_loteaceitecod_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_liqlogcod_SetNull();
         AV15ContagemResultado.gxTv_SdtContagemResultado_Contagemresultado_fncusrcod_SetNull();
         AV15ContagemResultado.Save();
         if ( AV15ContagemResultado.Success() )
         {
            GXt_int4 = AV15ContagemResultado.gxTpr_Contagemresultado_codigo;
            new prc_inslogresponsavel(context ).execute( ref  GXt_int4,  AV15ContagemResultado.gxTpr_Contagemresultado_responsavel,  "R",  "D",  AV13UserId,  0,  AV17StatusDmn,  "D",  "",  AV18PrazoEntrega,  true) ;
            AV15ContagemResultado.gxTpr_Contagemresultado_codigo = GXt_int4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13UserId), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13UserId), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            context.CommitDataStores( "WP_HomologarProposta");
            /* Execute user subroutine: 'FECHAR' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            context.RollbackDataStores( "WP_HomologarProposta");
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15ContagemResultado", AV15ContagemResultado);
      }

      protected void E15MV2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E16MV2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6Context) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Context", AV6Context);
      }

      protected void nextLoad( )
      {
      }

      protected void E17MV2( )
      {
         /* Load Routine */
         Form.Caption = "OS "+StringUtil.Trim( A1764Proposta_OSDmn);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         Dvpanel_tableattributes_Title = "Proposta "+StringUtil.Trim( A803ContagemResultado_ContratadaSigla);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         AV20Proposta_Prazo = A1702Proposta_Prazo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Proposta_Prazo", context.localUtil.Format(AV20Proposta_Prazo, "99/99/99"));
         AV21Dias = A1793Proposta_PrazoDias;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Dias), 4, 0)));
         AV18PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         if ( A1721Proposta_OSPrzRsp > 0 )
         {
            GXt_dtime2 = AV18PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV18PrazoEntrega,  A1721Proposta_OSPrzRsp,  A1707Proposta_OSPrzTpDias, out  GXt_dtime2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1721Proposta_OSPrzRsp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1721Proposta_OSPrzRsp), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
            AV18PrazoEntrega = GXt_dtime2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
         else
         {
            GXt_dtime2 = AV18PrazoEntrega;
            new prc_adddiasuteis(context ).execute(  AV18PrazoEntrega,  5,  A1707Proposta_OSPrzTpDias, out  GXt_dtime2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1707Proposta_OSPrzTpDias", A1707Proposta_OSPrzTpDias);
            AV18PrazoEntrega = GXt_dtime2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         }
         AV18PrazoEntrega = DateTimeUtil.TAdd( AV18PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         AV18PrazoEntrega = DateTimeUtil.TAdd( AV18PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PrazoEntrega", context.localUtil.TToC( AV18PrazoEntrega, 8, 5, 0, 3, "/", ":", " "));
         /* Using cursor H00MV12 */
         pr_default.execute(10, new Object[] {A1704Proposta_CntSrvCod});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A160ContratoServicos_Codigo = H00MV12_A160ContratoServicos_Codigo[0];
            A74Contrato_Codigo = H00MV12_A74Contrato_Codigo[0];
            AV22TipoDias = A1454ContratoServicos_PrazoTpDias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TipoDias", AV22TipoDias);
            /* Using cursor H00MV13 */
            pr_default.execute(11, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A1576SaldoContrato_Saldo = H00MV13_A1576SaldoContrato_Saldo[0];
               AV19SaldoContrato = A1576SaldoContrato_Saldo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19SaldoContrato", StringUtil.LTrim( StringUtil.Str( AV19SaldoContrato, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSALDOCONTRATO", GetSecureSignedToken( "", context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(11);
            }
            pr_default.close(11);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(10);
      }

      protected void E11MV2( )
      {
         /* Confirmpanel_Close Routine */
         if ( ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 ) || ( StringUtil.StrCmp(Confirmpanel_Result, "No") == 0 ) )
         {
            if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
            {
               AV15ContagemResultado.Load(A1686Proposta_OSCodigo);
               AV15ContagemResultado.gxTpr_Contagemresultado_semcusto = true;
               AV15ContagemResultado.Save();
            }
            new prc_contagemresultadoalterastatus(context ).execute( ref  A1686Proposta_OSCodigo,  "H",  0,  "Negada a execu��o da proposta.", ref  AV5Confirmado) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Confirmado", AV5Confirmado);
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15ContagemResultado", AV15ContagemResultado);
      }

      protected void E18MV2( )
      {
         /* Datainicio_Isvalid Routine */
         if ( ! (DateTime.MinValue==AV24DataInicio) && ( AV24DataInicio < DateTimeUtil.ServerDate( context, "DEFAULT") ) )
         {
            GX_msglist.addItem("Data prevista de inicio incorreta!");
            GX_FocusControl = edtavDatainicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            /* Execute user subroutine: 'PRAZO' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E19MV2( )
      {
         /* Datainicio_Click Routine */
         if ( ! (DateTime.MinValue==AV24DataInicio) && ( AV24DataInicio < DateTimeUtil.ServerDate( context, "DEFAULT") ) )
         {
            GX_msglist.addItem("Data prevista de inicio incorreta!");
            GX_FocusControl = edtavDatainicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            /* Execute user subroutine: 'PRAZO' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S122( )
      {
         /* 'PRAZO' Routine */
         GXt_dtime2 = DateTimeUtil.ResetTime( AV20Proposta_Prazo ) ;
         GXt_dtime1 = DateTimeUtil.ResetTime( AV24DataInicio ) ;
         new prc_adddiasuteis(context ).execute(  GXt_dtime1,  AV21Dias,  AV22TipoDias, out  GXt_dtime2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DataInicio", context.localUtil.Format(AV24DataInicio, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Dias), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TipoDias", AV22TipoDias);
         AV20Proposta_Prazo = DateTimeUtil.ResetTime(GXt_dtime2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Proposta_Prazo", context.localUtil.Format(AV20Proposta_Prazo, "99/99/99"));
      }

      protected void S112( )
      {
         /* 'FECHAR' Routine */
         lblTbjava_Caption = "<script language=\"javascript\" type=\"javascript\">parent.location.replace(parent.location.href); </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void wb_table1_2_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_MV2( true) ;
         }
         else
         {
            wb_table2_5_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_70_MV2( true) ;
         }
         else
         {
            wb_table3_70_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table3_70_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_MV2( true) ;
         }
         else
         {
            wb_table4_85_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MV2e( true) ;
         }
         else
         {
            wb_table1_2_MV2e( false) ;
         }
      }

      protected void wb_table4_85_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_MV2e( true) ;
         }
         else
         {
            wb_table4_85_MV2e( false) ;
         }
      }

      protected void wb_table3_70_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnDefault";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnhmlexc_Internalname, "", "Homologar e Executar", bttBtnbtnhmlexc_Jsonclick, 5, "Homologar e Executar", "", StyleString, ClassString, bttBtnbtnhmlexc_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBTNHMLEXC\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "BtnDefault";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnhmlnaoexc_Internalname, "", "Homologar e N�O Executar", bttBtnbtnhmlnaoexc_Jsonclick, 7, "Homologar e N�O Executar", "", StyleString, ClassString, bttBtnbtnhmlnaoexc_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e20mv1_client"+"'", TempTags, "", 2, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "btn btn-warning";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbtnindeferir_Internalname, "", "Indeferir e Retornar", bttBtnbtnindeferir_Jsonclick, 5, "Indeferir e Retornar", "", StyleString, ClassString, bttBtnbtnindeferir_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBTNINDEFERIR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_70_MV2e( true) ;
         }
         else
         {
            wb_table3_70_MV2e( false) ;
         }
      }

      protected void wb_table2_5_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_13_MV2( true) ;
         }
         else
         {
            wb_table5_13_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_MV2e( true) ;
         }
         else
         {
            wb_table2_5_MV2e( false) ;
         }
      }

      protected void wb_table5_13_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_16_MV2( true) ;
         }
         else
         {
            wb_table6_16_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table6_16_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table7_48_MV2( true) ;
         }
         else
         {
            wb_table7_48_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table7_48_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_MV2e( true) ;
         }
         else
         {
            wb_table5_13_MV2e( false) ;
         }
      }

      protected void wb_table7_48_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockdatainicio_Internalname, "Inicio", "", "", lblTextblockdatainicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatainicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatainicio_Internalname, context.localUtil.Format(AV24DataInicio, "99/99/99"), context.localUtil.Format( AV24DataInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+"EVDATAINICIO.CLICK."+"'", "", "", "", "", edtavDatainicio_Jsonclick, 5, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_HomologarProposta.htm");
            GxWebStd.gx_bitmap( context, edtavDatainicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_prazo_Internalname, "Prazo", "", "", lblTextblockproposta_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_58_MV2( true) ;
         }
         else
         {
            wb_table8_58_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table8_58_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_48_MV2e( true) ;
         }
         else
         {
            wb_table7_48_MV2e( false) ;
         }
      }

      protected void wb_table8_58_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedproposta_prazo_Internalname, tblTablemergedproposta_prazo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavProposta_prazo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavProposta_prazo_Internalname, context.localUtil.Format(AV20Proposta_Prazo, "99/99/99"), context.localUtil.Format( AV20Proposta_Prazo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavProposta_prazo_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtavProposta_prazo_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_HomologarProposta.htm");
            GxWebStd.gx_bitmap( context, edtavProposta_prazo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavProposta_prazo_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_prazodias_Internalname, "(", "", "", lblTextblockproposta_prazodias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_PrazoDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1793Proposta_PrazoDias), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1793Proposta_PrazoDias), "ZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_PrazoDias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellProposta_prazodias_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblProposta_prazodias_righttext_Internalname, "dias)", "", "", lblProposta_prazodias_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_58_MV2e( true) ;
         }
         else
         {
            wb_table8_58_MV2e( false) ;
         }
      }

      protected void wb_table6_16_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_objetivo_Internalname, "Descri��o", "", "", lblTextblockproposta_objetivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProposta_Objetivo_Internalname, A1690Proposta_Objetivo, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2048", -1, "", "", -1, true, "", "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_osservico_Internalname, "Servi�o", "", "", lblTextblockproposta_osservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProposta_OSServico, dynProposta_OSServico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0)), 1, dynProposta_OSServico_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_WP_HomologarProposta.htm");
            dynProposta_OSServico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1705Proposta_OSServico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProposta_OSServico_Internalname, "Values", (String)(dynProposta_OSServico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_esforco_Internalname, "Esforco", "", "", lblTextblockproposta_esforco_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table9_31_MV2( true) ;
         }
         else
         {
            wb_table9_31_MV2( false) ;
         }
         return  ;
      }

      protected void wb_table9_31_MV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockproposta_valor_Internalname, "Valor R$", "", "", lblTextblockproposta_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1688Proposta_Valor, 16, 4, ",", "")), context.localUtil.Format( A1688Proposta_Valor, "Z,ZZZ,ZZZ,ZZ9.99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_Internalname, "Saldo", "", "", lblTextblocksaldocontrato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_Internalname, StringUtil.LTrim( StringUtil.NToC( AV19SaldoContrato, 18, 5, ",", "")), ((edtavSaldocontrato_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV19SaldoContrato, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSaldocontrato_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_16_MV2e( true) ;
         }
         else
         {
            wb_table6_16_MV2e( false) ;
         }
      }

      protected void wb_table9_31_MV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedproposta_esforco_Internalname, tblTablemergedproposta_esforco_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_Esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1703Proposta_Esforco), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A1703Proposta_Esforco), "ZZZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_Esforco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProposta_OSUndCntSgl_Internalname, StringUtil.RTrim( A1710Proposta_OSUndCntSgl), StringUtil.RTrim( context.localUtil.Format( A1710Proposta_OSUndCntSgl, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProposta_OSUndCntSgl_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_WP_HomologarProposta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_31_MV2e( true) ;
         }
         else
         {
            wb_table9_31_MV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1686Proposta_OSCodigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1686Proposta_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1686Proposta_OSCodigo), 6, 0)));
         AV13UserId = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13UserId), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMV2( ) ;
         WSMV2( ) ;
         WEMV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423501226");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_homologarproposta.js", "?202032423501227");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockproposta_objetivo_Internalname = "TEXTBLOCKPROPOSTA_OBJETIVO";
         edtProposta_Objetivo_Internalname = "PROPOSTA_OBJETIVO";
         lblTextblockproposta_osservico_Internalname = "TEXTBLOCKPROPOSTA_OSSERVICO";
         dynProposta_OSServico_Internalname = "PROPOSTA_OSSERVICO";
         lblTextblockproposta_esforco_Internalname = "TEXTBLOCKPROPOSTA_ESFORCO";
         edtProposta_Esforco_Internalname = "PROPOSTA_ESFORCO";
         edtProposta_OSUndCntSgl_Internalname = "PROPOSTA_OSUNDCNTSGL";
         tblTablemergedproposta_esforco_Internalname = "TABLEMERGEDPROPOSTA_ESFORCO";
         lblTextblockproposta_valor_Internalname = "TEXTBLOCKPROPOSTA_VALOR";
         edtProposta_Valor_Internalname = "PROPOSTA_VALOR";
         lblTextblocksaldocontrato_Internalname = "TEXTBLOCKSALDOCONTRATO";
         edtavSaldocontrato_Internalname = "vSALDOCONTRATO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTextblockdatainicio_Internalname = "TEXTBLOCKDATAINICIO";
         edtavDatainicio_Internalname = "vDATAINICIO";
         lblTextblockproposta_prazo_Internalname = "TEXTBLOCKPROPOSTA_PRAZO";
         edtavProposta_prazo_Internalname = "vPROPOSTA_PRAZO";
         lblTextblockproposta_prazodias_Internalname = "TEXTBLOCKPROPOSTA_PRAZODIAS";
         edtProposta_PrazoDias_Internalname = "PROPOSTA_PRAZODIAS";
         lblProposta_prazodias_righttext_Internalname = "PROPOSTA_PRAZODIAS_RIGHTTEXT";
         cellProposta_prazodias_righttext_cell_Internalname = "PROPOSTA_PRAZODIAS_RIGHTTEXT_CELL";
         tblTablemergedproposta_prazo_Internalname = "TABLEMERGEDPROPOSTA_PRAZO";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnbtnhmlexc_Internalname = "BTNBTNHMLEXC";
         bttBtnbtnhmlnaoexc_Internalname = "BTNBTNHMLNAOEXC";
         bttBtnbtnindeferir_Internalname = "BTNBTNINDEFERIR";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         tblUsertable_Internalname = "USERTABLE";
         tblTablemain_Internalname = "TABLEMAIN";
         edtProposta_OSCodigo_Internalname = "PROPOSTA_OSCODIGO";
         edtContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtProposta_OSUndCntSgl_Jsonclick = "";
         edtProposta_Esforco_Jsonclick = "";
         edtavSaldocontrato_Jsonclick = "";
         edtavSaldocontrato_Enabled = 1;
         edtProposta_Valor_Jsonclick = "";
         dynProposta_OSServico_Jsonclick = "";
         edtProposta_PrazoDias_Jsonclick = "";
         edtavProposta_prazo_Jsonclick = "";
         edtavProposta_prazo_Enabled = 1;
         edtavDatainicio_Jsonclick = "";
         bttBtnbtnindeferir_Visible = 1;
         bttBtnbtnhmlnaoexc_Visible = 1;
         bttBtnbtnhmlexc_Visible = 1;
         lblTbjava_Visible = 1;
         lblTbjava_Caption = "tbJava";
         edtContagemResultado_ContratadaCod_Jsonclick = "";
         edtContagemResultado_ContratadaCod_Visible = 1;
         edtProposta_OSCodigo_Jsonclick = "";
         edtProposta_OSCodigo_Visible = 1;
         Confirmpanel_Confirmtype = "2";
         Confirmpanel_Buttoncanceltext = "Voltar";
         Confirmpanel_Buttonnotext = "Sem custo";
         Confirmpanel_Buttonyestext = "Com custo";
         Confirmpanel_Confirmtext = "Deseja cancelar a execu��o desta OS?";
         Confirmpanel_Icon = "2";
         Confirmpanel_Title = "Aten��o";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Proposta";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "OS X";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{av:'AV6Context',fld:'vCONTEXT',pic:'',nv:null}]}");
         setEventMetadata("'DOBTNHMLEXC'","{handler:'E13MV2',iparms:[{av:'AV24DataInicio',fld:'vDATAINICIO',pic:'',nv:''},{av:'A1686Proposta_OSCodigo',fld:'PROPOSTA_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV13UserId',fld:'vUSERID',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV20Proposta_Prazo',fld:'vPROPOSTA_PRAZO',pic:'',nv:''}],oparms:[{av:'AV20Proposta_Prazo',fld:'vPROPOSTA_PRAZO',pic:'',nv:''},{av:'A1686Proposta_OSCodigo',fld:'PROPOSTA_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("'DOBTNHMLNAOEXC'","{handler:'E20MV1',iparms:[],oparms:[]}");
         setEventMetadata("'DOBTNINDEFERIR'","{handler:'E14MV2',iparms:[{av:'A1686Proposta_OSCodigo',fld:'PROPOSTA_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV18PrazoEntrega',fld:'vPRAZOENTREGA',pic:'99/99/99 99:99',nv:''},{av:'AV13UserId',fld:'vUSERID',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV15ContagemResultado',fld:'vCONTAGEMRESULTADO',pic:'',nv:null},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15MV2',iparms:[],oparms:[]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11MV2',iparms:[{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'},{av:'A1686Proposta_OSCodigo',fld:'PROPOSTA_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5Confirmado',fld:'vCONFIRMADO',pic:'',nv:false}],oparms:[{av:'AV15ContagemResultado',fld:'vCONTAGEMRESULTADO',pic:'',nv:null},{av:'AV5Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'A1686Proposta_OSCodigo',fld:'PROPOSTA_OSCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VDATAINICIO.ISVALID","{handler:'E18MV2',iparms:[{av:'AV24DataInicio',fld:'vDATAINICIO',pic:'',nv:''},{av:'AV21Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV22TipoDias',fld:'vTIPODIAS',pic:'',nv:''}],oparms:[{av:'AV20Proposta_Prazo',fld:'vPROPOSTA_PRAZO',pic:'',nv:''}]}");
         setEventMetadata("VDATAINICIO.CLICK","{handler:'E19MV2',iparms:[{av:'AV24DataInicio',fld:'vDATAINICIO',pic:'',nv:''},{av:'AV21Dias',fld:'vDIAS',pic:'ZZZ9',nv:0},{av:'AV22TipoDias',fld:'vTIPODIAS',pic:'',nv:''}],oparms:[{av:'AV20Proposta_Prazo',fld:'vPROPOSTA_PRAZO',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Confirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A894LogResponsavel_Acao = "";
         AV18PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV22TipoDias = "";
         A1690Proposta_Objetivo = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00MV2_A155Servico_Codigo = new int[1] ;
         H00MV2_A160ContratoServicos_Codigo = new int[1] ;
         H00MV2_A605Servico_Sigla = new String[] {""} ;
         H00MV3_A602ContagemResultado_OSVinculada = new int[1] ;
         H00MV3_A1685Proposta_Codigo = new int[1] ;
         H00MV3_A1713Proposta_OSUndCntCod = new int[1] ;
         H00MV3_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         H00MV3_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         H00MV3_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         H00MV3_A1686Proposta_OSCodigo = new int[1] ;
         H00MV3_A1704Proposta_CntSrvCod = new int[1] ;
         H00MV3_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         H00MV3_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         H00MV3_A1764Proposta_OSDmn = new String[] {""} ;
         H00MV3_n1764Proposta_OSDmn = new bool[] {false} ;
         H00MV3_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00MV3_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00MV3_A1702Proposta_Prazo = new DateTime[] {DateTime.MinValue} ;
         H00MV3_A1721Proposta_OSPrzRsp = new short[1] ;
         H00MV3_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         H00MV3_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         H00MV3_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         H00MV3_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MV3_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00MV3_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00MV3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00MV3_A1793Proposta_PrazoDias = new short[1] ;
         H00MV3_n1793Proposta_PrazoDias = new bool[] {false} ;
         H00MV3_A1688Proposta_Valor = new decimal[1] ;
         H00MV3_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         H00MV3_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         H00MV3_A1703Proposta_Esforco = new int[1] ;
         H00MV3_A1705Proposta_OSServico = new int[1] ;
         H00MV3_n1705Proposta_OSServico = new bool[] {false} ;
         H00MV3_A1690Proposta_Objetivo = new String[] {""} ;
         A1702Proposta_Prazo = DateTime.MinValue;
         H00MV4_A1713Proposta_OSUndCntCod = new int[1] ;
         H00MV4_n1713Proposta_OSUndCntCod = new bool[] {false} ;
         H00MV4_A1721Proposta_OSPrzRsp = new short[1] ;
         H00MV4_n1721Proposta_OSPrzRsp = new bool[] {false} ;
         H00MV4_A1707Proposta_OSPrzTpDias = new String[] {""} ;
         H00MV4_n1707Proposta_OSPrzTpDias = new bool[] {false} ;
         A1707Proposta_OSPrzTpDias = "";
         H00MV5_A1710Proposta_OSUndCntSgl = new String[] {""} ;
         H00MV5_n1710Proposta_OSUndCntSgl = new bool[] {false} ;
         A1710Proposta_OSUndCntSgl = "";
         H00MV6_A1764Proposta_OSDmn = new String[] {""} ;
         H00MV6_n1764Proposta_OSDmn = new bool[] {false} ;
         H00MV6_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00MV6_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00MV6_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00MV6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00MV6_A1705Proposta_OSServico = new int[1] ;
         H00MV6_n1705Proposta_OSServico = new bool[] {false} ;
         A1764Proposta_OSDmn = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         H00MV7_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00MV7_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         A803ContagemResultado_ContratadaSigla = "";
         H00MV8_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         H00MV8_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         H00MV9_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         H00MV9_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         A1454ContratoServicos_PrazoTpDias = "";
         AV24DataInicio = DateTime.MinValue;
         AV20Proposta_Prazo = DateTime.MinValue;
         hsh = "";
         AV6Context = new wwpbaseobjects.SdtWWPContext(context);
         AV15ContagemResultado = new SdtContagemResultado(context);
         AV17StatusDmn = "";
         H00MV10_A894LogResponsavel_Acao = new String[] {""} ;
         H00MV10_A1797LogResponsavel_Codigo = new long[1] ;
         H00MV10_A896LogResponsavel_Owner = new int[1] ;
         H00MV10_A892LogResponsavel_DemandaCod = new int[1] ;
         H00MV10_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00MV11_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00MV11_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00MV12_A160ContratoServicos_Codigo = new int[1] ;
         H00MV12_A74Contrato_Codigo = new int[1] ;
         H00MV13_A1561SaldoContrato_Codigo = new int[1] ;
         H00MV13_A74Contrato_Codigo = new int[1] ;
         H00MV13_A1576SaldoContrato_Saldo = new decimal[1] ;
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnbtnhmlexc_Jsonclick = "";
         bttBtnbtnhmlnaoexc_Jsonclick = "";
         bttBtnbtnindeferir_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockdatainicio_Jsonclick = "";
         lblTextblockproposta_prazo_Jsonclick = "";
         lblTextblockproposta_prazodias_Jsonclick = "";
         lblProposta_prazodias_righttext_Jsonclick = "";
         lblTextblockproposta_objetivo_Jsonclick = "";
         lblTextblockproposta_osservico_Jsonclick = "";
         lblTextblockproposta_esforco_Jsonclick = "";
         lblTextblockproposta_valor_Jsonclick = "";
         lblTextblocksaldocontrato_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_homologarproposta__default(),
            new Object[][] {
                new Object[] {
               H00MV2_A155Servico_Codigo, H00MV2_A160ContratoServicos_Codigo, H00MV2_A605Servico_Sigla
               }
               , new Object[] {
               H00MV3_A602ContagemResultado_OSVinculada, H00MV3_A1685Proposta_Codigo, H00MV3_A1713Proposta_OSUndCntCod, H00MV3_n1713Proposta_OSUndCntCod, H00MV3_A1627ContagemResultado_CntSrvVncCod, H00MV3_n1627ContagemResultado_CntSrvVncCod, H00MV3_A1686Proposta_OSCodigo, H00MV3_A1704Proposta_CntSrvCod, H00MV3_A1454ContratoServicos_PrazoTpDias, H00MV3_n1454ContratoServicos_PrazoTpDias,
               H00MV3_A1764Proposta_OSDmn, H00MV3_n1764Proposta_OSDmn, H00MV3_A803ContagemResultado_ContratadaSigla, H00MV3_n803ContagemResultado_ContratadaSigla, H00MV3_A1702Proposta_Prazo, H00MV3_A1721Proposta_OSPrzRsp, H00MV3_n1721Proposta_OSPrzRsp, H00MV3_A1707Proposta_OSPrzTpDias, H00MV3_n1707Proposta_OSPrzTpDias, H00MV3_A912ContagemResultado_HoraEntrega,
               H00MV3_n912ContagemResultado_HoraEntrega, H00MV3_A490ContagemResultado_ContratadaCod, H00MV3_n490ContagemResultado_ContratadaCod, H00MV3_A1793Proposta_PrazoDias, H00MV3_n1793Proposta_PrazoDias, H00MV3_A1688Proposta_Valor, H00MV3_A1710Proposta_OSUndCntSgl, H00MV3_n1710Proposta_OSUndCntSgl, H00MV3_A1703Proposta_Esforco, H00MV3_A1705Proposta_OSServico,
               H00MV3_n1705Proposta_OSServico, H00MV3_A1690Proposta_Objetivo
               }
               , new Object[] {
               H00MV4_A1713Proposta_OSUndCntCod, H00MV4_n1713Proposta_OSUndCntCod, H00MV4_A1721Proposta_OSPrzRsp, H00MV4_n1721Proposta_OSPrzRsp, H00MV4_A1707Proposta_OSPrzTpDias, H00MV4_n1707Proposta_OSPrzTpDias
               }
               , new Object[] {
               H00MV5_A1710Proposta_OSUndCntSgl, H00MV5_n1710Proposta_OSUndCntSgl
               }
               , new Object[] {
               H00MV6_A1764Proposta_OSDmn, H00MV6_n1764Proposta_OSDmn, H00MV6_A912ContagemResultado_HoraEntrega, H00MV6_n912ContagemResultado_HoraEntrega, H00MV6_A490ContagemResultado_ContratadaCod, H00MV6_n490ContagemResultado_ContratadaCod, H00MV6_A1705Proposta_OSServico, H00MV6_n1705Proposta_OSServico
               }
               , new Object[] {
               H00MV7_A803ContagemResultado_ContratadaSigla, H00MV7_n803ContagemResultado_ContratadaSigla
               }
               , new Object[] {
               H00MV8_A1627ContagemResultado_CntSrvVncCod, H00MV8_n1627ContagemResultado_CntSrvVncCod
               }
               , new Object[] {
               H00MV9_A1454ContratoServicos_PrazoTpDias, H00MV9_n1454ContratoServicos_PrazoTpDias
               }
               , new Object[] {
               H00MV10_A894LogResponsavel_Acao, H00MV10_A1797LogResponsavel_Codigo, H00MV10_A896LogResponsavel_Owner, H00MV10_A892LogResponsavel_DemandaCod, H00MV10_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00MV11_A69ContratadaUsuario_UsuarioCod, H00MV11_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               H00MV12_A160ContratoServicos_Codigo, H00MV12_A74Contrato_Codigo
               }
               , new Object[] {
               H00MV13_A1561SaldoContrato_Codigo, H00MV13_A74Contrato_Codigo, H00MV13_A1576SaldoContrato_Saldo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSaldocontrato_Enabled = 0;
         edtavProposta_prazo_Enabled = 0;
      }

      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV21Dias ;
      private short A1793Proposta_PrazoDias ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A1721Proposta_OSPrzRsp ;
      private short nGXWrapped ;
      private int A1686Proposta_OSCodigo ;
      private int AV13UserId ;
      private int wcpOA1686Proposta_OSCodigo ;
      private int wcpOAV13UserId ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int A1703Proposta_Esforco ;
      private int edtProposta_OSCodigo_Visible ;
      private int A490ContagemResultado_ContratadaCod ;
      private int edtContagemResultado_ContratadaCod_Visible ;
      private int gxdynajaxindex ;
      private int A1705Proposta_OSServico ;
      private int edtavSaldocontrato_Enabled ;
      private int edtavProposta_prazo_Enabled ;
      private int A1704Proposta_CntSrvCod ;
      private int A1713Proposta_OSUndCntCod ;
      private int A1627ContagemResultado_CntSrvVncCod ;
      private int lblTbjava_Visible ;
      private int bttBtnbtnhmlexc_Visible ;
      private int bttBtnbtnhmlnaoexc_Visible ;
      private int bttBtnbtnindeferir_Visible ;
      private int GXt_int4 ;
      private int A160ContratoServicos_Codigo ;
      private int A74Contrato_Codigo ;
      private int idxLst ;
      private long A1797LogResponsavel_Codigo ;
      private decimal A1688Proposta_Valor ;
      private decimal AV19SaldoContrato ;
      private decimal A1576SaldoContrato_Saldo ;
      private String Confirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A894LogResponsavel_Acao ;
      private String AV22TipoDias ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Buttoncanceltext ;
      private String Confirmpanel_Confirmtype ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtProposta_OSCodigo_Internalname ;
      private String edtProposta_OSCodigo_Jsonclick ;
      private String edtContagemResultado_ContratadaCod_Internalname ;
      private String edtContagemResultado_ContratadaCod_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSaldocontrato_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavProposta_prazo_Internalname ;
      private String A1707Proposta_OSPrzTpDias ;
      private String A1710Proposta_OSUndCntSgl ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String edtProposta_Objetivo_Internalname ;
      private String dynProposta_OSServico_Internalname ;
      private String edtProposta_Esforco_Internalname ;
      private String edtProposta_OSUndCntSgl_Internalname ;
      private String edtProposta_Valor_Internalname ;
      private String edtavDatainicio_Internalname ;
      private String edtProposta_PrazoDias_Internalname ;
      private String hsh ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String bttBtnbtnhmlexc_Internalname ;
      private String bttBtnbtnhmlnaoexc_Internalname ;
      private String bttBtnbtnindeferir_Internalname ;
      private String AV17StatusDmn ;
      private String Dvpanel_tableattributes_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String tblUsertable_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnbtnhmlexc_Jsonclick ;
      private String bttBtnbtnhmlnaoexc_Jsonclick ;
      private String bttBtnbtnindeferir_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockdatainicio_Internalname ;
      private String lblTextblockdatainicio_Jsonclick ;
      private String edtavDatainicio_Jsonclick ;
      private String lblTextblockproposta_prazo_Internalname ;
      private String lblTextblockproposta_prazo_Jsonclick ;
      private String tblTablemergedproposta_prazo_Internalname ;
      private String edtavProposta_prazo_Jsonclick ;
      private String lblTextblockproposta_prazodias_Internalname ;
      private String lblTextblockproposta_prazodias_Jsonclick ;
      private String edtProposta_PrazoDias_Jsonclick ;
      private String cellProposta_prazodias_righttext_cell_Internalname ;
      private String lblProposta_prazodias_righttext_Internalname ;
      private String lblProposta_prazodias_righttext_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockproposta_objetivo_Internalname ;
      private String lblTextblockproposta_objetivo_Jsonclick ;
      private String lblTextblockproposta_osservico_Internalname ;
      private String lblTextblockproposta_osservico_Jsonclick ;
      private String dynProposta_OSServico_Jsonclick ;
      private String lblTextblockproposta_esforco_Internalname ;
      private String lblTextblockproposta_esforco_Jsonclick ;
      private String lblTextblockproposta_valor_Internalname ;
      private String lblTextblockproposta_valor_Jsonclick ;
      private String edtProposta_Valor_Jsonclick ;
      private String lblTextblocksaldocontrato_Internalname ;
      private String lblTextblocksaldocontrato_Jsonclick ;
      private String edtavSaldocontrato_Jsonclick ;
      private String tblTablemergedproposta_esforco_Internalname ;
      private String edtProposta_Esforco_Jsonclick ;
      private String edtProposta_OSUndCntSgl_Jsonclick ;
      private String Confirmpanel_Internalname ;
      private DateTime AV18PrazoEntrega ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime GXt_dtime2 ;
      private DateTime GXt_dtime1 ;
      private DateTime A1702Proposta_Prazo ;
      private DateTime AV24DataInicio ;
      private DateTime AV20Proposta_Prazo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV5Confirmado ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1705Proposta_OSServico ;
      private bool n1793Proposta_PrazoDias ;
      private bool n1713Proposta_OSUndCntCod ;
      private bool n1721Proposta_OSPrzRsp ;
      private bool n1707Proposta_OSPrzTpDias ;
      private bool n1710Proposta_OSUndCntSgl ;
      private bool n1764Proposta_OSDmn ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n1627ContagemResultado_CntSrvVncCod ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool returnInSub ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool GXt_boolean3 ;
      private bool AV16Achado ;
      private String A1690Proposta_Objetivo ;
      private String A1764Proposta_OSDmn ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynProposta_OSServico ;
      private IDataStoreProvider pr_default ;
      private int[] H00MV2_A155Servico_Codigo ;
      private int[] H00MV2_A160ContratoServicos_Codigo ;
      private String[] H00MV2_A605Servico_Sigla ;
      private int[] H00MV3_A602ContagemResultado_OSVinculada ;
      private int[] H00MV3_A1685Proposta_Codigo ;
      private int[] H00MV3_A1713Proposta_OSUndCntCod ;
      private bool[] H00MV3_n1713Proposta_OSUndCntCod ;
      private int[] H00MV3_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] H00MV3_n1627ContagemResultado_CntSrvVncCod ;
      private int[] H00MV3_A1686Proposta_OSCodigo ;
      private int[] H00MV3_A1704Proposta_CntSrvCod ;
      private String[] H00MV3_A1454ContratoServicos_PrazoTpDias ;
      private bool[] H00MV3_n1454ContratoServicos_PrazoTpDias ;
      private String[] H00MV3_A1764Proposta_OSDmn ;
      private bool[] H00MV3_n1764Proposta_OSDmn ;
      private String[] H00MV3_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00MV3_n803ContagemResultado_ContratadaSigla ;
      private DateTime[] H00MV3_A1702Proposta_Prazo ;
      private short[] H00MV3_A1721Proposta_OSPrzRsp ;
      private bool[] H00MV3_n1721Proposta_OSPrzRsp ;
      private String[] H00MV3_A1707Proposta_OSPrzTpDias ;
      private bool[] H00MV3_n1707Proposta_OSPrzTpDias ;
      private DateTime[] H00MV3_A912ContagemResultado_HoraEntrega ;
      private bool[] H00MV3_n912ContagemResultado_HoraEntrega ;
      private int[] H00MV3_A490ContagemResultado_ContratadaCod ;
      private bool[] H00MV3_n490ContagemResultado_ContratadaCod ;
      private short[] H00MV3_A1793Proposta_PrazoDias ;
      private bool[] H00MV3_n1793Proposta_PrazoDias ;
      private decimal[] H00MV3_A1688Proposta_Valor ;
      private String[] H00MV3_A1710Proposta_OSUndCntSgl ;
      private bool[] H00MV3_n1710Proposta_OSUndCntSgl ;
      private int[] H00MV3_A1703Proposta_Esforco ;
      private int[] H00MV3_A1705Proposta_OSServico ;
      private bool[] H00MV3_n1705Proposta_OSServico ;
      private String[] H00MV3_A1690Proposta_Objetivo ;
      private int[] H00MV4_A1713Proposta_OSUndCntCod ;
      private bool[] H00MV4_n1713Proposta_OSUndCntCod ;
      private short[] H00MV4_A1721Proposta_OSPrzRsp ;
      private bool[] H00MV4_n1721Proposta_OSPrzRsp ;
      private String[] H00MV4_A1707Proposta_OSPrzTpDias ;
      private bool[] H00MV4_n1707Proposta_OSPrzTpDias ;
      private String[] H00MV5_A1710Proposta_OSUndCntSgl ;
      private bool[] H00MV5_n1710Proposta_OSUndCntSgl ;
      private String[] H00MV6_A1764Proposta_OSDmn ;
      private bool[] H00MV6_n1764Proposta_OSDmn ;
      private DateTime[] H00MV6_A912ContagemResultado_HoraEntrega ;
      private bool[] H00MV6_n912ContagemResultado_HoraEntrega ;
      private int[] H00MV6_A490ContagemResultado_ContratadaCod ;
      private bool[] H00MV6_n490ContagemResultado_ContratadaCod ;
      private int[] H00MV6_A1705Proposta_OSServico ;
      private bool[] H00MV6_n1705Proposta_OSServico ;
      private String[] H00MV7_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00MV7_n803ContagemResultado_ContratadaSigla ;
      private int[] H00MV8_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] H00MV8_n1627ContagemResultado_CntSrvVncCod ;
      private String[] H00MV9_A1454ContratoServicos_PrazoTpDias ;
      private bool[] H00MV9_n1454ContratoServicos_PrazoTpDias ;
      private String[] H00MV10_A894LogResponsavel_Acao ;
      private long[] H00MV10_A1797LogResponsavel_Codigo ;
      private int[] H00MV10_A896LogResponsavel_Owner ;
      private int[] H00MV10_A892LogResponsavel_DemandaCod ;
      private bool[] H00MV10_n892LogResponsavel_DemandaCod ;
      private int[] H00MV11_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00MV11_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00MV12_A160ContratoServicos_Codigo ;
      private int[] H00MV12_A74Contrato_Codigo ;
      private int[] H00MV13_A1561SaldoContrato_Codigo ;
      private int[] H00MV13_A74Contrato_Codigo ;
      private decimal[] H00MV13_A1576SaldoContrato_Saldo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6Context ;
      private SdtContagemResultado AV15ContagemResultado ;
   }

   public class wp_homologarproposta__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MV2 ;
          prmH00MV2 = new Object[] {
          } ;
          Object[] prmH00MV3 ;
          prmH00MV3 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV4 ;
          prmH00MV4 = new Object[] {
          new Object[] {"@Proposta_OSServico",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV5 ;
          prmH00MV5 = new Object[] {
          new Object[] {"@Proposta_OSUndCntCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV6 ;
          prmH00MV6 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV7 ;
          prmH00MV7 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV8 ;
          prmH00MV8 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV9 ;
          prmH00MV9 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvVncCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV10 ;
          prmH00MV10 = new Object[] {
          new Object[] {"@Proposta_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV11 ;
          prmH00MV11 = new Object[] {
          new Object[] {"@AV15Cont_1Contagemresultado_c",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV12 ;
          prmH00MV12 = new Object[] {
          new Object[] {"@Proposta_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MV13 ;
          prmH00MV13 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MV2", "SELECT T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[Servico_Sigla] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV2,0,0,true,false )
             ,new CursorDef("H00MV3", "SELECT T6.[ContagemResultado_Codigo] AS ContagemResultado_OSVinculada, T1.[Proposta_Codigo], T4.[ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod, T6.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod, T1.[Proposta_OSCodigo] AS Proposta_OSCodigo, T1.[Proposta_CntSrvCod], T7.[ContratoServicos_PrazoTpDias], T2.[ContagemResultado_Demanda] AS Proposta_OSDmn, T3.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[Proposta_Prazo], T4.[ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, T4.[ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias, T2.[ContagemResultado_HoraEntrega], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[Proposta_PrazoDias], T1.[Proposta_Valor], T5.[UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl, T1.[Proposta_Esforco], T2.[ContagemResultado_CntSrvCod] AS Proposta_OSServico, T1.[Proposta_Objetivo] FROM ((((((dbo.[Proposta] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T5 WITH (NOLOCK) ON T5.[UnidadeMedicao_Codigo] = T4.[ContratoServicos_UnidadeContratada]) INNER JOIN [ContagemResultado] T6 WITH (NOLOCK) ON T6.[ContagemResultado_Codigo] = T1.[Proposta_OSCodigo]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T6.[ContagemResultado_CntSrvCod]) WHERE T1.[Proposta_OSCodigo] = @Proposta_OSCodigo ORDER BY T1.[Proposta_OSCodigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV3,100,0,true,false )
             ,new CursorDef("H00MV4", "SELECT [ContratoServicos_UnidadeContratada] AS Proposta_OSUndCntCod, [ContratoServicos_PrazoResposta] AS Proposta_OSPrzRsp, [ContratoServicos_PrazoTpDias] AS Proposta_OSPrzTpDias FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @Proposta_OSServico ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV4,1,0,true,false )
             ,new CursorDef("H00MV5", "SELECT [UnidadeMedicao_Sigla] AS Proposta_OSUndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @Proposta_OSUndCntCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV5,1,0,true,false )
             ,new CursorDef("H00MV6", "SELECT [ContagemResultado_Demanda] AS Proposta_OSDmn, [ContagemResultado_HoraEntrega], [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_CntSrvCod] AS Proposta_OSServico FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Proposta_OSCodigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV6,1,0,true,false )
             ,new CursorDef("H00MV7", "SELECT [Contratada_Sigla] AS ContagemResultado_ContratadaSigla FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV7,1,0,true,false )
             ,new CursorDef("H00MV8", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @Proposta_OSCodigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV8,1,0,true,false )
             ,new CursorDef("H00MV9", "SELECT [ContratoServicos_PrazoTpDias] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvVncCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV9,1,0,true,false )
             ,new CursorDef("H00MV10", "SELECT [LogResponsavel_Acao], [LogResponsavel_Codigo], [LogResponsavel_Owner], [LogResponsavel_DemandaCod] FROM [LogResponsavel] WITH (NOLOCK) WHERE ([LogResponsavel_DemandaCod] = @Proposta_OSCodigo) AND ([LogResponsavel_Acao] = 'E') ORDER BY [LogResponsavel_DemandaCod], [LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV10,100,0,true,false )
             ,new CursorDef("H00MV11", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV15Cont_1Contagemresultado_c and [ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV11,1,0,false,true )
             ,new CursorDef("H00MV12", "SELECT TOP 1 [ContratoServicos_Codigo], [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @Proposta_CntSrvCod ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV12,1,0,true,true )
             ,new CursorDef("H00MV13", "SELECT TOP 1 [SaldoContrato_Codigo], [Contrato_Codigo], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MV13,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(10) ;
                ((short[]) buf[15])[0] = rslt.getShort(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[19])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((int[]) buf[21])[0] = rslt.getInt(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((short[]) buf[23])[0] = rslt.getShort(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                ((String[]) buf[26])[0] = rslt.getString(17, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((int[]) buf[28])[0] = rslt.getInt(18) ;
                ((int[]) buf[29])[0] = rslt.getInt(19) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                ((String[]) buf[31])[0] = rslt.getLongVarchar(20) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
