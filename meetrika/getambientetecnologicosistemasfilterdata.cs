/*
               File: GetAmbienteTecnologicoSistemasFilterData
        Description: Get Ambiente Tecnologico Sistemas Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:2.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getambientetecnologicosistemasfilterdata : GXProcedure
   {
      public getambientetecnologicosistemasfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getambientetecnologicosistemasfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getambientetecnologicosistemasfilterdata objgetambientetecnologicosistemasfilterdata;
         objgetambientetecnologicosistemasfilterdata = new getambientetecnologicosistemasfilterdata();
         objgetambientetecnologicosistemasfilterdata.AV16DDOName = aP0_DDOName;
         objgetambientetecnologicosistemasfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetambientetecnologicosistemasfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetambientetecnologicosistemasfilterdata.AV20OptionsJson = "" ;
         objgetambientetecnologicosistemasfilterdata.AV23OptionsDescJson = "" ;
         objgetambientetecnologicosistemasfilterdata.AV25OptionIndexesJson = "" ;
         objgetambientetecnologicosistemasfilterdata.context.SetSubmitInitialConfig(context);
         objgetambientetecnologicosistemasfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetambientetecnologicosistemasfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getambientetecnologicosistemasfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_SISTEMA_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_SIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_SISTEMA_COORDENACAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_COORDENACAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("AmbienteTecnologicoSistemasGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "AmbienteTecnologicoSistemasGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("AmbienteTecnologicoSistemasGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "SISTEMA_ATIVO") == 0 )
            {
               AV32Sistema_Ativo = BooleanUtil.Val( AV30GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "SISTEMA_TIPO") == 0 )
            {
               AV33Sistema_Tipo = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA") == 0 )
            {
               AV10TFSistema_Sigla = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA_SEL") == 0 )
            {
               AV11TFSistema_Sigla_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO") == 0 )
            {
               AV12TFSistema_Coordenacao = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO_SEL") == 0 )
            {
               AV13TFSistema_Coordenacao_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&AMBIENTETECNOLOGICO_CODIGO") == 0 )
            {
               AV45AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 )
            {
               AV35Sistema_Sigla1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 )
            {
               AV36Sistema_Coordenacao1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 )
               {
                  AV39Sistema_Sigla2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 )
               {
                  AV40Sistema_Coordenacao2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 )
                  {
                     AV43Sistema_Sigla3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 )
                  {
                     AV44Sistema_Coordenacao3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSISTEMA_SIGLAOPTIONS' Routine */
         AV10TFSistema_Sigla = AV14SearchTxt;
         AV11TFSistema_Sigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35Sistema_Sigla1 ,
                                              AV36Sistema_Coordenacao1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39Sistema_Sigla2 ,
                                              AV40Sistema_Coordenacao2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43Sistema_Sigla3 ,
                                              AV44Sistema_Coordenacao3 ,
                                              AV11TFSistema_Sigla_Sel ,
                                              AV10TFSistema_Sigla ,
                                              AV13TFSistema_Coordenacao_Sel ,
                                              AV12TFSistema_Coordenacao ,
                                              A129Sistema_Sigla ,
                                              A513Sistema_Coordenacao ,
                                              A130Sistema_Ativo ,
                                              A699Sistema_Tipo ,
                                              AV45AmbienteTecnologico_Codigo ,
                                              A351AmbienteTecnologico_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV35Sistema_Sigla1 = StringUtil.PadR( StringUtil.RTrim( AV35Sistema_Sigla1), 25, "%");
         lV36Sistema_Coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV36Sistema_Coordenacao1), "%", "");
         lV39Sistema_Sigla2 = StringUtil.PadR( StringUtil.RTrim( AV39Sistema_Sigla2), 25, "%");
         lV40Sistema_Coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV40Sistema_Coordenacao2), "%", "");
         lV43Sistema_Sigla3 = StringUtil.PadR( StringUtil.RTrim( AV43Sistema_Sigla3), 25, "%");
         lV44Sistema_Coordenacao3 = StringUtil.Concat( StringUtil.RTrim( AV44Sistema_Coordenacao3), "%", "");
         lV10TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV10TFSistema_Sigla), 25, "%");
         lV12TFSistema_Coordenacao = StringUtil.Concat( StringUtil.RTrim( AV12TFSistema_Coordenacao), "%", "");
         /* Using cursor P00LY2 */
         pr_default.execute(0, new Object[] {AV45AmbienteTecnologico_Codigo, lV35Sistema_Sigla1, lV36Sistema_Coordenacao1, lV39Sistema_Sigla2, lV40Sistema_Coordenacao2, lV43Sistema_Sigla3, lV44Sistema_Coordenacao3, lV10TFSistema_Sigla, AV11TFSistema_Sigla_Sel, lV12TFSistema_Coordenacao, AV13TFSistema_Coordenacao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLY2 = false;
            A351AmbienteTecnologico_Codigo = P00LY2_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00LY2_n351AmbienteTecnologico_Codigo[0];
            A130Sistema_Ativo = P00LY2_A130Sistema_Ativo[0];
            A699Sistema_Tipo = P00LY2_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00LY2_n699Sistema_Tipo[0];
            A129Sistema_Sigla = P00LY2_A129Sistema_Sigla[0];
            A513Sistema_Coordenacao = P00LY2_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00LY2_n513Sistema_Coordenacao[0];
            A127Sistema_Codigo = P00LY2_A127Sistema_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00LY2_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) && ( StringUtil.StrCmp(P00LY2_A129Sistema_Sigla[0], A129Sistema_Sigla) == 0 ) )
            {
               BRKLY2 = false;
               A127Sistema_Codigo = P00LY2_A127Sistema_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKLY2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A129Sistema_Sigla)) )
            {
               AV18Option = A129Sistema_Sigla;
               AV21OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")));
               AV19Options.Add(AV18Option, 0);
               AV22OptionsDesc.Add(AV21OptionDesc, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLY2 )
            {
               BRKLY2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSISTEMA_COORDENACAOOPTIONS' Routine */
         AV12TFSistema_Coordenacao = AV14SearchTxt;
         AV13TFSistema_Coordenacao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35Sistema_Sigla1 ,
                                              AV36Sistema_Coordenacao1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39Sistema_Sigla2 ,
                                              AV40Sistema_Coordenacao2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43Sistema_Sigla3 ,
                                              AV44Sistema_Coordenacao3 ,
                                              AV11TFSistema_Sigla_Sel ,
                                              AV10TFSistema_Sigla ,
                                              AV13TFSistema_Coordenacao_Sel ,
                                              AV12TFSistema_Coordenacao ,
                                              A129Sistema_Sigla ,
                                              A513Sistema_Coordenacao ,
                                              A130Sistema_Ativo ,
                                              A699Sistema_Tipo ,
                                              AV45AmbienteTecnologico_Codigo ,
                                              A351AmbienteTecnologico_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV35Sistema_Sigla1 = StringUtil.PadR( StringUtil.RTrim( AV35Sistema_Sigla1), 25, "%");
         lV36Sistema_Coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV36Sistema_Coordenacao1), "%", "");
         lV39Sistema_Sigla2 = StringUtil.PadR( StringUtil.RTrim( AV39Sistema_Sigla2), 25, "%");
         lV40Sistema_Coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV40Sistema_Coordenacao2), "%", "");
         lV43Sistema_Sigla3 = StringUtil.PadR( StringUtil.RTrim( AV43Sistema_Sigla3), 25, "%");
         lV44Sistema_Coordenacao3 = StringUtil.Concat( StringUtil.RTrim( AV44Sistema_Coordenacao3), "%", "");
         lV10TFSistema_Sigla = StringUtil.PadR( StringUtil.RTrim( AV10TFSistema_Sigla), 25, "%");
         lV12TFSistema_Coordenacao = StringUtil.Concat( StringUtil.RTrim( AV12TFSistema_Coordenacao), "%", "");
         /* Using cursor P00LY3 */
         pr_default.execute(1, new Object[] {AV45AmbienteTecnologico_Codigo, lV35Sistema_Sigla1, lV36Sistema_Coordenacao1, lV39Sistema_Sigla2, lV40Sistema_Coordenacao2, lV43Sistema_Sigla3, lV44Sistema_Coordenacao3, lV10TFSistema_Sigla, AV11TFSistema_Sigla_Sel, lV12TFSistema_Coordenacao, AV13TFSistema_Coordenacao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKLY4 = false;
            A351AmbienteTecnologico_Codigo = P00LY3_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = P00LY3_n351AmbienteTecnologico_Codigo[0];
            A130Sistema_Ativo = P00LY3_A130Sistema_Ativo[0];
            A699Sistema_Tipo = P00LY3_A699Sistema_Tipo[0];
            n699Sistema_Tipo = P00LY3_n699Sistema_Tipo[0];
            A513Sistema_Coordenacao = P00LY3_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = P00LY3_n513Sistema_Coordenacao[0];
            A129Sistema_Sigla = P00LY3_A129Sistema_Sigla[0];
            A127Sistema_Codigo = P00LY3_A127Sistema_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00LY3_A351AmbienteTecnologico_Codigo[0] == A351AmbienteTecnologico_Codigo ) && ( StringUtil.StrCmp(P00LY3_A513Sistema_Coordenacao[0], A513Sistema_Coordenacao) == 0 ) )
            {
               BRKLY4 = false;
               A127Sistema_Codigo = P00LY3_A127Sistema_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKLY4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A513Sistema_Coordenacao)) )
            {
               AV18Option = A513Sistema_Coordenacao;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLY4 )
            {
               BRKLY4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV32Sistema_Ativo = true;
         AV33Sistema_Tipo = "A";
         AV10TFSistema_Sigla = "";
         AV11TFSistema_Sigla_Sel = "";
         AV12TFSistema_Coordenacao = "";
         AV13TFSistema_Coordenacao_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV35Sistema_Sigla1 = "";
         AV36Sistema_Coordenacao1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV39Sistema_Sigla2 = "";
         AV40Sistema_Coordenacao2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV43Sistema_Sigla3 = "";
         AV44Sistema_Coordenacao3 = "";
         scmdbuf = "";
         lV35Sistema_Sigla1 = "";
         lV36Sistema_Coordenacao1 = "";
         lV39Sistema_Sigla2 = "";
         lV40Sistema_Coordenacao2 = "";
         lV43Sistema_Sigla3 = "";
         lV44Sistema_Coordenacao3 = "";
         lV10TFSistema_Sigla = "";
         lV12TFSistema_Coordenacao = "";
         A129Sistema_Sigla = "";
         A513Sistema_Coordenacao = "";
         A699Sistema_Tipo = "";
         P00LY2_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00LY2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00LY2_A130Sistema_Ativo = new bool[] {false} ;
         P00LY2_A699Sistema_Tipo = new String[] {""} ;
         P00LY2_n699Sistema_Tipo = new bool[] {false} ;
         P00LY2_A129Sistema_Sigla = new String[] {""} ;
         P00LY2_A513Sistema_Coordenacao = new String[] {""} ;
         P00LY2_n513Sistema_Coordenacao = new bool[] {false} ;
         P00LY2_A127Sistema_Codigo = new int[1] ;
         AV18Option = "";
         AV21OptionDesc = "";
         P00LY3_A351AmbienteTecnologico_Codigo = new int[1] ;
         P00LY3_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         P00LY3_A130Sistema_Ativo = new bool[] {false} ;
         P00LY3_A699Sistema_Tipo = new String[] {""} ;
         P00LY3_n699Sistema_Tipo = new bool[] {false} ;
         P00LY3_A513Sistema_Coordenacao = new String[] {""} ;
         P00LY3_n513Sistema_Coordenacao = new bool[] {false} ;
         P00LY3_A129Sistema_Sigla = new String[] {""} ;
         P00LY3_A127Sistema_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getambientetecnologicosistemasfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LY2_A351AmbienteTecnologico_Codigo, P00LY2_n351AmbienteTecnologico_Codigo, P00LY2_A130Sistema_Ativo, P00LY2_A699Sistema_Tipo, P00LY2_n699Sistema_Tipo, P00LY2_A129Sistema_Sigla, P00LY2_A513Sistema_Coordenacao, P00LY2_n513Sistema_Coordenacao, P00LY2_A127Sistema_Codigo
               }
               , new Object[] {
               P00LY3_A351AmbienteTecnologico_Codigo, P00LY3_n351AmbienteTecnologico_Codigo, P00LY3_A130Sistema_Ativo, P00LY3_A699Sistema_Tipo, P00LY3_n699Sistema_Tipo, P00LY3_A513Sistema_Coordenacao, P00LY3_n513Sistema_Coordenacao, P00LY3_A129Sistema_Sigla, P00LY3_A127Sistema_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV48GXV1 ;
      private int AV45AmbienteTecnologico_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A127Sistema_Codigo ;
      private long AV26count ;
      private String AV33Sistema_Tipo ;
      private String AV10TFSistema_Sigla ;
      private String AV11TFSistema_Sigla_Sel ;
      private String AV35Sistema_Sigla1 ;
      private String AV39Sistema_Sigla2 ;
      private String AV43Sistema_Sigla3 ;
      private String scmdbuf ;
      private String lV35Sistema_Sigla1 ;
      private String lV39Sistema_Sigla2 ;
      private String lV43Sistema_Sigla3 ;
      private String lV10TFSistema_Sigla ;
      private String A129Sistema_Sigla ;
      private String A699Sistema_Tipo ;
      private bool returnInSub ;
      private bool AV32Sistema_Ativo ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool A130Sistema_Ativo ;
      private bool BRKLY2 ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n699Sistema_Tipo ;
      private bool n513Sistema_Coordenacao ;
      private bool BRKLY4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV12TFSistema_Coordenacao ;
      private String AV13TFSistema_Coordenacao_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV36Sistema_Coordenacao1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV40Sistema_Coordenacao2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV44Sistema_Coordenacao3 ;
      private String lV36Sistema_Coordenacao1 ;
      private String lV40Sistema_Coordenacao2 ;
      private String lV44Sistema_Coordenacao3 ;
      private String lV12TFSistema_Coordenacao ;
      private String A513Sistema_Coordenacao ;
      private String AV18Option ;
      private String AV21OptionDesc ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LY2_A351AmbienteTecnologico_Codigo ;
      private bool[] P00LY2_n351AmbienteTecnologico_Codigo ;
      private bool[] P00LY2_A130Sistema_Ativo ;
      private String[] P00LY2_A699Sistema_Tipo ;
      private bool[] P00LY2_n699Sistema_Tipo ;
      private String[] P00LY2_A129Sistema_Sigla ;
      private String[] P00LY2_A513Sistema_Coordenacao ;
      private bool[] P00LY2_n513Sistema_Coordenacao ;
      private int[] P00LY2_A127Sistema_Codigo ;
      private int[] P00LY3_A351AmbienteTecnologico_Codigo ;
      private bool[] P00LY3_n351AmbienteTecnologico_Codigo ;
      private bool[] P00LY3_A130Sistema_Ativo ;
      private String[] P00LY3_A699Sistema_Tipo ;
      private bool[] P00LY3_n699Sistema_Tipo ;
      private String[] P00LY3_A513Sistema_Coordenacao ;
      private bool[] P00LY3_n513Sistema_Coordenacao ;
      private String[] P00LY3_A129Sistema_Sigla ;
      private int[] P00LY3_A127Sistema_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getambientetecnologicosistemasfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LY2( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35Sistema_Sigla1 ,
                                             String AV36Sistema_Coordenacao1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             String AV39Sistema_Sigla2 ,
                                             String AV40Sistema_Coordenacao2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             String AV43Sistema_Sigla3 ,
                                             String AV44Sistema_Coordenacao3 ,
                                             String AV11TFSistema_Sigla_Sel ,
                                             String AV10TFSistema_Sigla ,
                                             String AV13TFSistema_Coordenacao_Sel ,
                                             String AV12TFSistema_Coordenacao ,
                                             String A129Sistema_Sigla ,
                                             String A513Sistema_Coordenacao ,
                                             bool A130Sistema_Ativo ,
                                             String A699Sistema_Tipo ,
                                             int AV45AmbienteTecnologico_Codigo ,
                                             int A351AmbienteTecnologico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [AmbienteTecnologico_Codigo], [Sistema_Ativo], [Sistema_Tipo], [Sistema_Sigla], [Sistema_Coordenacao], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([AmbienteTecnologico_Codigo] = @AV45AmbienteTecnologico_Codigo)";
         scmdbuf = scmdbuf + " and ([Sistema_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ([Sistema_Tipo] = 'A')";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Sistema_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV35Sistema_Sigla1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Sistema_Coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV36Sistema_Coordenacao1 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Sistema_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV39Sistema_Sigla2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Sistema_Coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV40Sistema_Coordenacao2 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Sistema_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV43Sistema_Sigla3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Sistema_Coordenacao3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV44Sistema_Coordenacao3 + '%')";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV10TFSistema_Sigla)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] = @AV11TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Coordenacao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistema_Coordenacao)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV12TFSistema_Coordenacao)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Coordenacao_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] = @AV13TFSistema_Coordenacao_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AmbienteTecnologico_Codigo], [Sistema_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00LY3( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             String AV35Sistema_Sigla1 ,
                                             String AV36Sistema_Coordenacao1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             String AV39Sistema_Sigla2 ,
                                             String AV40Sistema_Coordenacao2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             String AV43Sistema_Sigla3 ,
                                             String AV44Sistema_Coordenacao3 ,
                                             String AV11TFSistema_Sigla_Sel ,
                                             String AV10TFSistema_Sigla ,
                                             String AV13TFSistema_Coordenacao_Sel ,
                                             String AV12TFSistema_Coordenacao ,
                                             String A129Sistema_Sigla ,
                                             String A513Sistema_Coordenacao ,
                                             bool A130Sistema_Ativo ,
                                             String A699Sistema_Tipo ,
                                             int AV45AmbienteTecnologico_Codigo ,
                                             int A351AmbienteTecnologico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [AmbienteTecnologico_Codigo], [Sistema_Ativo], [Sistema_Tipo], [Sistema_Coordenacao], [Sistema_Sigla], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([AmbienteTecnologico_Codigo] = @AV45AmbienteTecnologico_Codigo)";
         scmdbuf = scmdbuf + " and ([Sistema_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ([Sistema_Tipo] = 'A')";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Sistema_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV35Sistema_Sigla1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Sistema_Coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV36Sistema_Coordenacao1 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Sistema_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV39Sistema_Sigla2 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Sistema_Coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV40Sistema_Coordenacao2 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Sistema_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV43Sistema_Sigla3 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44Sistema_Coordenacao3)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV44Sistema_Coordenacao3 + '%')";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFSistema_Sigla)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] like @lV10TFSistema_Sigla)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFSistema_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Sigla] = @AV11TFSistema_Sigla_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Coordenacao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFSistema_Coordenacao)) ) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] like @lV12TFSistema_Coordenacao)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFSistema_Coordenacao_Sel)) )
         {
            sWhereString = sWhereString + " and ([Sistema_Coordenacao] = @AV13TFSistema_Coordenacao_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AmbienteTecnologico_Codigo], [Sistema_Coordenacao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LY2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] );
               case 1 :
                     return conditional_P00LY3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LY2 ;
          prmP00LY2 = new Object[] {
          new Object[] {"@AV45AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35Sistema_Sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@lV36Sistema_Coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV39Sistema_Sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@lV40Sistema_Coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV43Sistema_Sigla3",SqlDbType.Char,25,0} ,
          new Object[] {"@lV44Sistema_Coordenacao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV11TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV12TFSistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFSistema_Coordenacao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00LY3 ;
          prmP00LY3 = new Object[] {
          new Object[] {"@AV45AmbienteTecnologico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV35Sistema_Sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@lV36Sistema_Coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV39Sistema_Sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@lV40Sistema_Coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV43Sistema_Sigla3",SqlDbType.Char,25,0} ,
          new Object[] {"@lV44Sistema_Coordenacao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV10TFSistema_Sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV11TFSistema_Sigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV12TFSistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFSistema_Coordenacao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LY2,100,0,true,false )
             ,new CursorDef("P00LY3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LY3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 25) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 25) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getambientetecnologicosistemasfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getambientetecnologicosistemasfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getambientetecnologicosistemasfilterdata") )
          {
             return  ;
          }
          getambientetecnologicosistemasfilterdata worker = new getambientetecnologicosistemasfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
